#!/usr/bin/env python

import gtk


class Gui:

    def on_fileQuit_activate(self, fileQuit, data=None):
        gtk.Widget.destroy(self.toplevel)

    def on_fileQuit_destroy_event(self, fileQuit, data=None):
        pass

    def on_fileQuit_delete_event(self, fileQuit, data=None):
        print 'delete event on file Quit'
        return False

    def on_file_activate(self, file, data=None):
        pass

    def on_edit_activate(self, edit, data=None):
        pass

    def on_button1_activate(self, button1, data=None):
        pass

    def on_button_clicked(self, button1, data=None):
        self.label = self.builder.get_object('label')
        if self.counter % 2 == 0:
            self.label.set_text('TEST')
        else:
            self.label.set_text('')
        self.counter = self.counter + 1

    def on_toplevel_delete_event(self, windows, data=None):
        return False

    def on_toplevel_destroy(self, window, data=None):
        print 'tutu'
        gtk.mainquit()

    def __init__(self):
        "The constructor"
        self.counter = 0
        self.builder = gtk.Builder()
        self.builder.add_from_file('Test.glade')
        self.builder.connect_signals(self)
        self.toplevel = self.builder.get_object('toplevel')
        self.toplevel.show_all()


if __name__ == '__main__':
    gui = Gui()
    gtk.main()
