{-# LANGUAGE GADTs #-}
module Hshac where
import Language
import Prelude hiding ((<$>), LT, GT)
import Text.PrettyPrint.ANSI.Leijen
import qualified Data.List as L
-- import Data.Ratio
 
-- TODO: Need to make sure that the saturation only happens when == is
-- present on the edge of a location.

(|>) :: a -> (a -> b) -> b
(|>) x f = f x

apply     :: (a -> a -> a) -> [a] -> Maybe a
apply f x = if (L.length x) Prelude.== 0
               then Nothing
               else Just (L.foldl' f (head x) x)

symbols       :: Symbol -> Doc
symbols (S x) = text x

getTerm                            :: String -> Term a -> Doc
getTerm _ TTrue                    = text "True"
getTerm _ TFalse                   = text "False"
getTerm n (T t)                    = text $ getVar t n
getTerm _ (TR t)                   = text (show (fromRational t :: Double))
getTerm n (Not t)                  = text "!" <+> getTerm n t |> parens
getTerm n (Min t1 t2)              = ((getTerm n t1 |> parens 
                                         <+> text "<=" <+> getTerm n t2 |> parens) 
                                       |> parens
                                       <+> text "?" <+> getTerm n t1 <+> text ":"  
                                       <+> getTerm n t2) |> parens
getTerm n (Max t1 t2)              = ((getTerm n t1 |> parens 
                                         <+> text ">=" <+> getTerm n t2 |> parens) 
                                       |> parens
                                       <+> text "?" <+> getTerm n t1 <+> text ":"  
                                       <+> getTerm n t2) |> parens
getTerm n (Leq t1 t2)              = getTerm n t1 <+> text "<=" <+> getTerm n t2 |> parens
getTerm n (Geq t1 t2)              = getTerm n t1 <+> text ">=" <+> getTerm n t2 |> parens
getTerm n (LT t1 t2)               = getTerm n t1 <+> text "<" <+> getTerm n t2 |> parens
getTerm n (GT t1 t2)               = getTerm n t1 <+> text ">" <+> getTerm n t2 |> parens
getTerm n (Eq t1 t2)               = getTerm n t1 <+> text "==" <+> getTerm n t2  |> parens
getTerm n (NEq t1 t2)              = getTerm n t1 <+> text "!=" <+> getTerm n t2 |> parens
getTerm n (And t1 t2)              = getTerm n t1 <+> text "&&" <+> getTerm n t2 |> parens

getHaName       :: Lit -> String
getHaName (L x) = x

getiVars                            :: String -> [ExternalDecl Input V] -> [Doc]
getiVars _ []                       = []
getiVars name ((InputVarDecl (S x)):xs) = text ("double " 
                                                ++ name 
                                                ++ "_" 
                                                ++ x ++ ";") : (getiVars name xs)
                                                
getoVars                                 :: String -> [ExternalDecl Output V] -> [Doc]
getoVars _ []                            = [] 
getoVars name (OutputVarDecl (S x) x2:xs) = text ("double " 
                                                     ++ name 
                                                     ++ "_" 
                                                     ++ x ++ " = " ++ (show (fromRational x2 :: Double))
                                                     ++ ";") : getoVars name xs
                                                     

getoEvs                                  :: String -> [ExternalDecl Output E] -> [Doc]
getoEvs _ []                             = [] 
getoEvs name ((OutputEventDecl x x2):xs) = text ("unsigned char " 
                                                 ++ name 
                                                 ++ "_" 
                                                 ++ x ++ " = ")
                                                 <+> getTerm name x2
                                                 <+> text ";" : (getoEvs name xs)
                                                 
data Type = U
          | D

makeExtern :: Type -> String -> String -> String -> String -> Doc
makeExtern Hshac.U iHa oHa ib ob = text ("extern unsigned char " ++ iHa ++ "_" ++ ib ++ ";")
                                   <$> text ("extern unsigned char " ++ oHa ++ "_" ++ ob ++ ";")
makeExtern Hshac.D iHa oHa ib ob = text ("extern double " ++ iHa ++ "_" ++ ib ++ ";")
                                   <$> text ("extern double " ++ oHa ++ "_" ++ ob ++ ";")
                                   

externBindIO :: String -> BindSeries -> String -> [Doc]
externBindIO iHa (LastBind ((InputVar (S bs1)) :< (OutputVar (S bs2)))) oHa = 
  [makeExtern Hshac.D iHa oHa bs1 bs2] 
externBindIO iHa (LastBind ((InputEvent bs1) :< (OutputEvent bs2))) oHa = 
  [makeExtern Hshac.U iHa oHa bs1 bs2]

externBindIO iHa (((InputVar (S bs11)) :< (OutputVar (S bs12))) :<< bs2) oHa = 
    makeExtern Hshac.D iHa oHa bs11 bs12
    : (externBindIO iHa bs2 oHa) 
externBindIO iHa (((InputEvent bs11) :< (OutputEvent bs12)) :<< bs2) oHa = 
    makeExtern Hshac.U iHa oHa bs11 bs12
    : (externBindIO iHa bs2 oHa) 

mkExtern :: BackEnd -> String -> String -> String -> String -> Doc
mkExtern be iHa oHa ib ob = 
  let 
      tt = 
        text (iHa ++ "_" ++ ib)
        <+> text "="
        <+> text (oHa ++ "_" ++ ob ++ ";")
  in case be of
       C -> tt
       Promela -> text "c_code {" <+> tt <+> text "}"

bindIO :: BackEnd -> String -> BindSeries -> String -> [Doc]
bindIO be iHa (LastBind ((InputVar (S bs1)) :< (OutputVar (S bs2)))) oHa = 
  [mkExtern be iHa oHa bs1 bs2] 
bindIO _ iHa (LastBind ((InputEvent bs1) :< (OutputEvent bs2))) oHa = 
  [mkExtern C iHa oHa bs1 bs2]

bindIO be iHa ((InputVar (S bs11) :< OutputVar (S bs12)) :<< bs2) oHa = 
    mkExtern be iHa oHa bs11 bs12
    : (bindIO be iHa bs2 oHa) 
bindIO be iHa ((InputEvent bs11 :< OutputEvent bs12) :<< bs2) oHa = 
    mkExtern C iHa oHa bs11 bs12
    : (bindIO be iHa bs2 oHa) 

mkBind                                                                            :: BindHa -> [Doc]
mkBind []                                                                        = [] 
mkBind (((Ha bs11 _ _ _ _ _ _ _ _),bs2,(Ha bs31 _ _ _ _ _ _ _ _)):o) = 
    externBindIO (getHaName bs11) bs2 (getHaName bs31) |> vcat |> align : mkBind o

makBind    :: BindHa -> BackEnd -> [Doc]
makBind [] _ = [] 
makBind (((Ha bs11 _ _ _ _ _ _ _ _),bs2,(Ha bs31 _ _ _ _ _ _ _ _)):o) be = 
           bindIO be (getHaName bs11) bs2 (getHaName bs31) |> vcat |> align : (makBind o be)

mkMain :: HaSeries -> BindHa -> [Doc] -> [Doc] -> Doc
mkMain hs bs decs calls = 
       let ns = zip (getNames hs) (getEdges hs)
           level = 2
           initStates = map (\(a, x, y) -> initStateValue a x y 
                                             |> vcat 
                                             |> align) (L.zip3 (getStates hs) (getNames hs) (getInitLoc hs))
                      |> vcat |> align |> indent level
           while = makeWhile ns level bs
           ret = text "return 0;" |> indent level
       in makeMain while ret initStates
       where
       makeMain :: Doc -> Doc -> Doc -> Doc
       makeMain while ret initStates = 
         headers 
         <$> decs |> vcat |> align
         <$> defines (getNames hs)
         <$> mkBind bs |> vcat |> align
         <$> text "int main (void){"
         <$> initStates
         <$> calls |> vcat |> align |> indent 2
         <$> while
         <$> ret
         <$> text "}"
                         
       defines :: [String] -> Doc
       defines ns = text "#define True 1" 
                    <$> text "#define False 0"
                    <$> text "extern void readInput();"
                    <$> text "extern void writeOutput();"
                    <$> map (\x -> text ("extern int " ++ x ++ "(int, int);")) ns
                        |> vcat |> align

       headers :: Doc
       headers = text "#include<stdint.h>" 
                 <$> text "#include<stdlib.h>" 
                 <$> text "#include<stdio.h>"
                 <$> text "#include<math.h>"
                 
          
getStates :: HaSeries -> [[Loc]]
getStates (LastHa (Ha _ x1 _ _ _ _ _ _ _)) = [x1]
getStates ((Ha _ x11 _ _ _ _ _ _ _) ::: x2) = x11 : getStates x2

getNames :: HaSeries -> [String]
getNames (LastHa (Ha (L x1) _ _ _ _ _ _ _ _)) = [x1]   
getNames ((Ha (L x11) _ _ _ _ _ _ _ _) ::: x2) = x11 : getNames x2

getInitLoc :: HaSeries -> [Loc]
getInitLoc (LastHa (Ha  _ _ x1 _ _ _ _ _ _)) = [x1]   
getInitLoc ((Ha _ _ x11 _ _ _ _ _ _) ::: x2) = x11 : getInitLoc x2

getEdges :: HaSeries -> [[Edge]] 
getEdges (LastHa (Ha _ _ _ x4  _ _ _ _ _)) = [x4]  
getEdges ((Ha _ _ _ x14 _ _ _ _ _) ::: x2) = x14 : getEdges x2


initStateValue                         :: [Loc] -> String -> Loc -> [Doc] 
initStateValue ls name (Loc l1 _ _ _) = 
  let index = case L.findIndex (\(Loc x1 _ _ _) -> x1 Prelude.== l1) ls of
                Just x -> x
                Nothing -> error "Location not found!"
  in
    text "int" <+> text (name ++ "_cstate =")
    <+> text (show index)
    <+> text ";" :
    [text "int" <+> text (name ++ "_pstate = -1;")]

makeWhile :: [(String, [Edge])] -> Int -> BindHa -> Doc
makeWhile name level bs = 
    let 
      calls = map (\(name, _) -> 
                    text "int"
                    <+> (name ++ "_rstate = " ++ name) |> text
                    <+> ("(" ++ name ++ "_cstate, " 
                         ++ name ++ "_pstate);") |> text
                    <$> text (name ++ "_pstate = " ++ name ++ "_cstate;")
                    <$> text (name ++ "_cstate = " ++ name ++ "_rstate;")) name 
              |> vcat 
              |> align

        -- XXX: This is where the external vars with bindings need to be
        -- updated.

      body = [text "readInput();", makBind bs C |> vcat |> align, calls] |> vcat

    in text "while(True) {" |> indent level
       <$> body |> indent (level * 2)
       <$> text "writeOutput();" |> indent (level * 2)
       <$> text "}" |> indent level

getMinMaxRational             :: Term Rational -> [Rational]
getMinMaxRational (Min x1 x2) = getMinMaxRational x1 ++ getMinMaxRational x2
getMinMaxRational (Max x1 x2) = getMinMaxRational x1 ++ getMinMaxRational x2
getMinMaxRational (TR x)      = [x]

getMinMaxTerm             :: (Term Bool) -> [Rational]
getMinMaxTerm TTrue       = []
getMinMaxTerm TFalse      = [] 
getMinMaxTerm (Not x)     = getMinMaxTerm x
getMinMaxTerm (Leq _ x2)  = getMinMaxRational x2
getMinMaxTerm (Geq _ x2)  = getMinMaxRational x2
getMinMaxTerm (LT _ x2)   = getMinMaxRational x2
getMinMaxTerm (GT _ x2)   = getMinMaxRational x2
getMinMaxTerm (Eq _ x2)   = getMinMaxRational x2
getMinMaxTerm (NEq _ x2)  = getMinMaxRational x2
getMinMaxTerm (And x1 x2) = getMinMaxTerm x1 ++ getMinMaxTerm x2
                            
getlbArb :: Invariant Lo -> Bool
getlbArb (InvariantLoc _ x2) = getlbArbE x2

                               
-- FIXME: This needs to be using bounds like in the python version.
-- TODO: LATER
getlbArbE :: Term Bool -> Bool
getlbArbE TTrue = False
getlbArbE TFalse = False
getlbArbE (Not x) = getlbArbE x
getlbArbE (Leq _ _) = False
getlbArbE (Geq _ _) = False
getlbArbE (LT _ _) = False
getlbArbE (GT _ _) = False
getlbArbE (Eq _ _) = True
getlbArbE (NEq _ _) = False
-- XXX: You do not have things like x >10 && x >20, but only x >10 && x <20.
getlbArbE (And _ _) = True 

getMinMax :: Invariant Lo -> (Maybe Rational, Maybe Rational)
getMinMax (InvariantLoc _ inv2) = let u = (L.nub . getMinMaxTerm) inv2
                                  in (apply Prelude.min u, apply Prelude.max u)

compileReactions                 :: HaSeries -> BindHa -> [(Doc, (Doc, Doc))]
compileReactions (ha1 ::: ha2) b = compileReaction ha1 : (compileReactions ha2 b)
compileReactions (LastHa (Ha name locs initl edges cVars iVars oVars iEvs oEvs)) _   = 
  [compileReaction (Ha name locs initl edges cVars iVars oVars iEvs oEvs)]

afcalls                  :: ArithExpr Rational -> String
afcalls (A _)            = ""
afcalls (AR _)           = ""
afcalls (Plus _ _)       = ""
afcalls (Abs _)          = ""
afcalls (Mult _ _)       = ""
afcalls (Negate _)       = ""
afcalls (Signum _)       = ""
afcalls (FromInteger _)  = ""
afcalls (FromRational _) = ""
afcalls (Exp _ _)        = ""
afcalls (Div _ _)        = ""
afcalls (AMin _ _)       = ""
afcalls (AMax _ _)       = ""
afcalls (FCall x1 x2 Nothing)    = 
        let dd = L.replicate (L.length x2) "double" |> L.intersperse ","
        in "extern void " ++ x1 ++ "(" ++ foldl (++) "" dd ++ ");"
afcalls (FCall x1 x2 (Just r))    = 
         let dd = L.replicate (L.length x2) "double" |> L.intersperse ","
         in "extern " ++ r ++ " " ++ x1 ++ "(" ++ foldl (++) "" dd ++ ");"

defExternFunCallsS :: [InternalSymbolDecl] -> [Doc]
defExternFunCallsS l = 
  let as = map (\(SymbolDecl _ x) -> x) l |> map afcalls |> L.nub |> map text
  in as

defExternFunCalls :: [Loc] -> [Edge] -> [Doc]
defExternFunCalls locs edges = 
    let ldefs = externFCalls locs 
        edefs = externFECalls edges 
  in (ldefs ++ edefs) |> L.nub |> map text
  where
  externFCalls        :: [Loc] -> [String]
  externFCalls []     = []
  externFCalls (Loc _ l2 _ (l41,l42):ls) = 
           let ocalls = ofcalls l2
               ucalls = (ufcalls l41) ++ (ufcalls l42)
           in (ocalls ++ ucalls) ++ externFCalls ls

  externFECalls                                       :: [Edge] -> [String]
  externFECalls []                                    = []
  externFECalls (Edge _ _ _ (e41,e42,e43) _:es) = 
    let ee1 = ufcalls e41
        ee2 = ufcalls e42
        ee3 = ufcalls e43
    in (ee1 ++ ee2 ++ ee3) ++ (externFECalls es)

  ufcalls                 :: [Update a] -> [String]
  ufcalls []              = []
  ufcalls ((_ := x2):xs)  = afcalls x2 : ufcalls xs
  ufcalls ((:!)_:xs)    = ufcalls xs

  ofcalls                   :: [Ode] -> [String]
  ofcalls []                = []
  ofcalls ((Ode _ x2 _):xs) = (afcalls x2) : (ofcalls xs)

getNFunCallInit                                  :: [InternalSymbolDecl] -> [InternalSymbolDecl]
getNFunCallInit []                               = []
getNFunCallInit ((SymbolDecl l (FCall _ _ _)):xs)  = (SymbolDecl l 0.0) : getNFunCallInit xs
getNFunCallInit (s@(SymbolDecl _ _):xs)          = s : getNFunCallInit xs

getFunCallInit                                     :: [InternalSymbolDecl] -> [InternalSymbolDecl]
getFunCallInit []                                  = []
getFunCallInit (s@(SymbolDecl _ (FCall _ _ _)):xs) = s : getFunCallInit xs
getFunCallInit ((SymbolDecl _ _):xs)               = getFunCallInit xs


-- Write this function, which produces "C" code
compileReaction :: Ha -> (Doc, (Doc, Doc))
compileReaction (Ha name locs _ edges cVars iVars oVars iEvs oEvs) =
  let 
      decfcalls = defExternFunCallsS cVars |> vcat |> align
      initvarsFunCall = getFunCallInit cVars |> declFunContVars |> vcat |> align
      initvarsNFunCall = getNFunCallInit cVars
  in 
  (headers <$> text "#define True 1" <$> text "#define False 0"
  <$> defExternFunCalls locs edges |> vcat |> align
  <$> getiVars (getHaName name) iVars |> vcat |> align
  <$> getoVars (getHaName name) oVars |> vcat |> align
  -- <$> getiEvents (getHaName name) iVars |> vcat |> align
  <$> getoEvs (getHaName name) oEvs |> vcat |> align
  <$> declEvents (events edges)
  <$> declContVars (initContValue initvarsNFunCall) (contVars initvarsNFunCall)
  <$> decStates locs
  <$> text ""
  <$> makeReaction, (decfcalls, initvarsFunCall))
        where
          declFunContVars :: [InternalSymbolDecl] -> [Doc]
          declFunContVars [] = []
          declFunContVars ((SymbolDecl (S _) s2):ss) = 
            let rhs = textArithExpr s2
            in rhs <+> text ";" : declFunContVars ss

          initContValue                             :: [InternalSymbolDecl] -> [(String, Doc)]
          initContValue []                          = []
          initContValue ((SymbolDecl (S x1) x2):xs) = (x1, textArithExpr x2) : initContValue xs

          headers :: Doc
          headers = text "#include<stdint.h>" 
                    <$> text "#include<stdlib.h>" 
                    <$> text "#include<stdio.h>"
                    <$> text "#include<math.h>"

          events :: [Edge] -> [Event]
          events []                          = []
          events ((Edge _ _ _ _ evs):edgess) = evs ++ events edgess

          declEvents :: [Event] -> Doc
          declEvents evs = 
              let xs = (L.intersperse "," . L.nub) $ map (\(Event x) -> getEvent x (getHaName name)) evs
              in if xs Prelude./= []
                 then 
                     text "extern unsigned char" 
                     <+> foldl (\a x -> a <+> text x) empty xs
                     <+> text ";//Events"
                 else empty

          contVars :: [InternalSymbolDecl] -> [String]
          contVars []                          = []
          contVars ((SymbolDecl (S x) _):cs) = 
              L.nub $ x : contVars cs

          getPossibleInitialValue :: String -> [(String, Doc)] -> Doc
          getPossibleInitialValue x ys = 
            let ms = L.find (\(y,_)  -> y Prelude.== x) ys
            in case ms of
                 Just (_, y) -> text " = " <+> y
                 Nothing -> empty

          declContVars :: [(String, Doc)] -> [String] -> Doc
          declContVars iivars vars = 
              let uvars = L.intersperse "," (map (\x -> x ++ "_u") vars)
                  ivars = L.intersperse "," (map (\x -> x ++ "_init") vars)
                  svars = L.intersperse "," (map (\x -> "slope_" ++ x) vars)
                  vvars = L.intersperse "," vars
              in 
                text "static double" 
                <+> foldl (\a x -> a <+> text x <+> getPossibleInitialValue x iivars) empty vvars
                <+> text "; //the continuous vars"
                <$> text "static double"
                <+> foldl (\a x -> a <+> text x) empty uvars
                <+> text "; // and their updates"
                <$> text "static double"
                <+> foldl (\a x -> a <+> text x) empty ivars
                <+> text "; // and their inits"
                <$> text "static double"
                <+> foldl (\a x -> a <+> text x) empty svars
                <+> text "; // and their slopes"
                <$> text "static unsigned char force_init_update;"
                <$> text "extern double d; // the time step"
                -- <$> text "static double slope; // the slope"

          decStates :: [Loc] -> Doc
          decStates x = 
                    let ls = L.intersperse "," $ declStates x
                    in
                      text "enum states"
                      <+> foldl (\a x -> a <+> text x) (text "{") ls
                      <+> text "}; // state declarations"

          declStates                             :: [Loc] -> [String] 
          declStates []                          = []
          declStates ((Loc (L locs1) _ _ _):locss) = locs1 : declStates locss


          getInitLocName       :: Lit -> String
          getInitLocName (L x) = x

          getInv                    :: Invariant a -> Doc
          getInv (InvariantEdge inv2) = getTerm (getHaName name) inv2
          getInv (InvariantLoc _ inv2) = getTerm (getHaName name) inv2

          getInvs              :: [Invariant a] -> [Doc]
          getInvs []           = []
          getInvs (invs:invss) = getInv invs : getInvs invss

          getNotEvents                    :: [Event] -> [Doc]
          getNotEvents []                 = []
          getNotEvents ((Event evs):evss) = text "(!" 
                                            <+> text (getEvent evs (getHaName name)) 
                                            <+> text ")" : getNotEvents evss

          textArithExpr                  :: (ArithExpr a) -> Doc
          textArithExpr (FCall fn x _)     = let 
                                              xx = map textArithExpr x |> L.intersperse (text ",") |> hcat |> parens
                                             in text fn <+> xx
          textArithExpr (AR x)           = text (show (fromRational x ::Double))
          textArithExpr (A x)            = text $ getVar x (getHaName name)
          textArithExpr (Plus x1 x2)     = (textArithExpr x1 <+> text "+" <+> textArithExpr x2) |> parens
          textArithExpr (Abs x)          = parens (textArithExpr x) |> (<+> text "abs")
          textArithExpr (Exp x y)        = text "pow (" <+> textArithExpr x |> parens <+> text ","
                                           <+> textArithExpr y |> parens <+> text ")"
          textArithExpr (Mult x1 x2)     = (textArithExpr x1 <+> text "*" <+> textArithExpr x2) |> parens
          textArithExpr (Negate x)       = (text "-" <+> textArithExpr x) |> parens
          textArithExpr (Signum _)       = error "Signum function in 'C' not implemented"
          textArithExpr (FromInteger x)  = text (show x)
          textArithExpr (FromRational x) = text (show (fromRational x :: Double))
          textArithExpr (Div x1 x2)      = (textArithExpr x1 <+> text "/" <+> textArithExpr x2) |> parens
          textArithExpr (AMin x1 x2)     = ((textArithExpr x1 |> parens 
                                               <+> text "<=" <+> textArithExpr x2 |> parens) 
                                              |> parens
                                              <+> text "?" <+> textArithExpr x1 <+> text ":"  
                                              <+> textArithExpr x2) |> parens
          textArithExpr (AMax x1 x2)     = ((textArithExpr x1 |> parens 
                                               <+> text ">=" <+> textArithExpr x2 |> parens) 
                                              |> parens
                                              <+> text "?" <+> textArithExpr x1 <+> text ":"  
                                              <+> textArithExpr x2) |> parens

          declOdes                                         :: [Ode] -> [Doc]
          declOdes []                                      = []
          declOdes ((Ode (DiffT (S odes1)) slope _):odess) = 
              let sl = textArithExpr slope
                  slx = "slope_"++odes1
              in (text "if ((pstate != cstate) || force_init_update)"
                  <+> text (odes1 ++ "_init") <+> text "=" <+> text odes1 <+> text ";"
                  <$> text slx <+> text "=" <+> sl <+> text ";"
                  <$> text (odes1 ++ "_u") 
                  <+> text "=" 
                  <+> (text (slx ++ " * d") |> parens) 
                  <+> text "+" 
                  <+> text odes1 
                  <+> text ";" ): declOdes odess

          buildSatBody             :: Symbol -> Rational -> Rational -> Doc
          buildSatBody (S x) minn maxx = text (x ++ "_u")
                                         <+> text "="
                                         <+> (text (x ++ "_u < ") <+> text (show (fromRational minn :: Double))
                                         <+> text (" && signbit(slope_"++ x) <+> (text ") && ") <+> text (x ++ "_init > ") 
                                         <+> text (show (fromRational minn :: Double))) |> parens
                                         <+> text " ? " <+> text (show (fromRational minn :: Double))
                                         <+> text ":" <+> text (x ++ "_u") <+> text ";"
                                         -- XXX: Now the max part
                                         <$> text (x ++ "_u")
                                         <+> text "="
                                         <+> (text (x ++ "_u > ") <+> text (show (fromRational maxx :: Double))
                                         -- <+> text " && !signbit(slope) && " <+> text (x ++ "_init < ")
                                         <+> text (" && !signbit(slope_" ++ x)<+> (text ") && ") <+> text (x ++ "_init < ") 
                                         <+> text (show (fromRational maxx :: Double))) |> parens
                                         <+> text " ? " <+> text (show (fromRational maxx :: Double))
                                         <+> text ":" <+> text (x ++ "_u") <+> text ";"

          getSaturationBody           :: [Ode] -> [Invariant Lo] -> [Doc]
          getSaturationBody []   []   = []
          getSaturationBody odes invs = 
            -- if L.length odes Prelude.== L.length invs
            -- then 
              map (\(Ode (DiffT o1) _ _) ->
                       case L.find (\(InvariantLoc x1 _) -> (getDiffOrEInE x1) Prelude.== o1) invs of
                         Just x ->
                           let (minn, maxx) = getMinMax x
                           in case (minn, maxx) of
                             (Just minn, Just maxx) -> 
                                 -- if getlbArb x
                                    -- then
                                    buildSatBody o1 minn maxx
                                    -- else empty
                             (_, _) -> empty
                         Nothing -> empty
                           -- error ("Continuous variable with no invariant: " ++ (show o1))
                     ) odes
            -- else error "Every evolving continuous variable does not have a location invariant"
            
          makeAssignmentStatements2     :: [Update Symbol] -> [Doc]
          makeAssignmentStatements2 []  = []
          makeAssignmentStatements2 ((ass1 := ass2):asss) = 
              symbols (getSym ass1 "")
              <+> text "="
              <+> textArithExpr ass2 <+> text ";" : (makeAssignmentStatements2 asss)

          makeAssignmentStatements     :: String -> [Update (External Output V)] -> [Doc]
          makeAssignmentStatements _ []  = []
          makeAssignmentStatements n ((ass1 := ass2):asss) = 
              text n <> text "_" <> symbols (getSym ass1 n)
              <+> text "="
              <+> textArithExpr ass2 <+> text ";" : (makeAssignmentStatements n asss)
              
          setIFalse   :: [ExternalDecl Input E] -> [Doc]
          setIFalse [] = []
          setIFalse ((InputEventDecl x):xs) = text ((getHaName name) ++ "_" ++ x) 
                                              <+> text " = False;"
                                              : setIFalse xs

          setOFalse   :: [ExternalDecl Output E] -> [Doc]
          setOFalse [] = []
          setOFalse ((OutputEventDecl x1 _):xs) = text ((getHaName name) ++ "_" ++ x1) 
                                                  <+> text " = False;"
                                                  : setOFalse xs

          makeIntralocationBody :: String -> [Invariant Lo] -> [Ode] -> [Update (External Output V)] ->
                                                                        [Update Symbol] -> Doc
          makeIntralocationBody n invs odes ass ass2 = 
              let 
              odeBody = declOdes odes |> vcat |> align
              saturationBody = getSaturationBody odes invs |> vcat |> align
              assignments2 = makeAssignmentStatements2 ass2 |> vcat |> align
              assignments = makeAssignmentStatements (getHaName name) ass |> vcat |> align
              in
                odeBody
                <$> text "/* Possible Saturation */"
                <$> saturationBody
                <$> setOFalse oEvs |> vcat |> align
                <$> setIFalse iEvs |> vcat |> align
                <$> text "cstate = " 
                <+> text n <+> text ";"
                <$> text "force_init_update = False;"
                <$> assignments2
                <$> assignments
                
          getUpdate         :: ([Update Symbol], [Update (External Output V)],
                                                 [Update (External Output E)]) -> [Doc]
          getUpdate (x,y,z) = (getUpdates x) ++ (getUpdates y) ++ (getUpdates z)

          getUpdates :: [Update a] -> [Doc]
          getUpdates []  = []
          getUpdates ((:!)(OutputEvent x) : upss) = 
            text (getHaName name ++ "_" ++ x)
            <+> text "= True;" : getUpdates upss
          getUpdates ((ups1 := ups2):upss) = 
              text (getIn ups1 (getHaName name))
              <+> text "="
              <+> textArithExpr ups2
              <+> text ";" : getUpdates upss

          getEvents        :: [Event] -> [Doc]
          getEvents []     = []
          getEvents ((Event x):xs) = text (getEvent x (getHaName name)) : getEvents xs

          getInterlocationBody                           :: [Edge] -> [(Doc, Doc)]
          getInterlocationBody []                        = []
          getInterlocationBody ((Edge (Loc (L sloc1) _ _ _) (Loc (L tloc1) _ _ _) linvs updates evs):edgess) = 
              let 
                  invs = (getInvs linvs ++ getEvents evs) |> L.intersperse (text " && ") |> fillCat |> align |> parens
                  ups = getUpdate updates |> vcat |> align
                  jstate = text "cstate = " <+> text tloc1 <+> text ";"
                  force_up = if sloc1 Prelude.== tloc1
                             then text "force_init_update = True;"
                             else text "force_init_update = False;"
              in (invs, (ups <$> jstate <$> force_up)  |> align) : getInterlocationBody edgess

          getLoc :: Int -> Loc -> Doc
          getLoc level (Loc (L ln) lodes linvs (sass, assignments)) = 
              let evs = events edges 
                        |> L.nubBy (\(Event x) (Event y) 
                                      -> (getEvent x (getHaName name)) Prelude.== (getEvent y (getHaName name)))
                  -- nevs = getNotEvents evs
                  nevs = []
                  invs = (getInvs linvs ++ nevs) |> L.intersperse (text " && ") |> fillCat |> align
                  intralocationbody = makeIntralocationBody ln linvs lodes assignments sass
                  -- XXX: Interlocation else if statements
                  medges = filter (\(Edge (Loc (L l1) _ _ _) _ _ _ _) -> l1 Prelude.== ln) edges
                  ibody = getInterlocationBody medges
              in
                (text "case (" <+> text ln <+> text "):") |> indent level
                -- XXX: All edges go here!
                <$> (text "if (True == False) {;}") |> indent (level * 2)
                <$> foldl (\a (i, b) -> 
                (text  "else if " <+> i <+> text "{") |> indent (level * 2)
                <$> b |> indent (level * 3)
                <$> text "}" |> indent (level * 2)
                <$> a) empty ibody
                <$> text "else if (" |> indent (level * 2)
                <+> invs
                <+> text ") {" |> indent (level * 2)
                <$> intralocationbody |> indent (level * 3)
                <$> text "}" |> indent (level * 2)
                <$> text "else {" |> indent (level * 2)
                <$> text ("fprintf(stderr, \"Unreachable state in: " ++ (getHaName name) ++"!\\n\");") |> indent (level * 3)
                <$> text "exit(1);" |> indent (level * 3)
                <$> text "}" |> indent (level * 2)
                <$> text "break;" |> indent (level * 2)

          makeLocs :: Int -> [Doc]
          -- XXX: This uses locs
          makeLocs level = map (getLoc level) locs

          makeReaction :: Doc
          makeReaction =
              let locName = getInitLocName name
                  level = 2
                  getLocs = makeLocs level |> vcat
              in 
                text "enum states"
                <+> text locName
                <+> text "(enum states cstate, enum states pstate){"
                <$> text "switch (cstate) {" |> indent level
                <$> getLocs
                <$> text "}" |> indent level
                <$> contUpdates |> indent level
                -- <$> oUpdates (getHaName name) oVars |> indent level
                <$> text "return cstate;" |> indent level
                <$> text "}"
                
          -- oUpdates              :: String -> [ExternalDecl Output V] -> Doc
          -- oUpdates _ []         = empty
          -- oUpdates n ((OutputVarDecl (S ovs1) _):ovss) = 
          --   text (n ++ "_" ++ ovs1) 
          --   <+> text ("= " ++ n ++ "_" ++ ovs1 ++ "_u;")
          --   <$> oUpdates n ovss 


          contUpdates :: Doc
          contUpdates = 
            map (\x -> text x <+> text "=" 
                       <+> text (x ++ "_u;")) (contVars cVars)
                |> vcat


