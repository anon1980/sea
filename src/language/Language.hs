{-# LANGUAGE GADTs #-}
{-# LANGUAGE DataKinds,  MultiParamTypeClasses,  FlexibleInstances, TypeFamilies, TypeOperators, 
  FunctionalDependencies #-}

module Language where

import Prelude hiding ((==),(/=), LT, GT, (<=), (>=), (<), (>), (&&), not)
import Data.Int
import Data.Word

newtype Lit = L String deriving (Show, Eq)

newtype Symbol = S String deriving (Show, Eq)

newtype InternalEvent = InternalEvent String deriving (Show, Eq)

class GetEvent a where
  getEvent :: a -> String -> String

data Event where
     Event :: GetEvent a => a -> Event
     
instance GetEvent InternalEvent where
  getEvent (InternalEvent x) _ = x

instance GetEvent (External Input E) where
  getEvent (InputEvent x) hname = hname ++ "_" ++ x
  
data Update a where
     (:=) :: (GetIn a) => a -> ArithExpr Rational -> Update a
     (:!) :: External Output E -> Update (External Output E)
     
class GetVar a where
  getVar :: a -> String -> String
  
instance GetVar Symbol where
  getVar (S x) _ = x 

instance GetVar (External Input V) where
  getVar (InputVar (S x)) name = name ++ "_" ++ x

data Term a where
     TTrue  :: Term Bool
     TFalse :: Term Bool
     T      :: (GetVar a) => a -> Term String
     TR     :: Rational -> Term Rational
     Not    :: Term Bool -> Term Bool
     Min    :: Term Rational -> Term Rational -> Term Rational
     Max    :: Term Rational -> Term Rational -> Term Rational
     Leq    :: Term String -> Term Rational -> Term Bool
     Geq    :: Term String -> Term Rational -> Term Bool
     LT     :: Term String -> Term Rational -> Term Bool
     GT     :: Term String -> Term Rational -> Term Bool
     Eq     :: Term String -> Term Rational -> Term Bool
     NEq    :: Term String -> Term Rational -> Term Bool
     And    :: Term Bool -> Term Bool -> Term Bool

class BoolEq a b | a -> b where
  (==) :: a -> b -> Term Bool
  (/=) :: a -> b -> Term Bool

instance BoolEq (Term String) (Term Rational) where
  x == y = Eq x y
  x /= y = NEq x y
  
class BoolEq a b => BoolOrd a b | a -> b where
  (<=) :: a -> b -> Term Bool
  (<)  :: a -> b -> Term Bool
  (>)  :: a -> b -> Term Bool
  (>=) :: a -> b -> Term Bool
  
class RationalOrd a where
  min :: a -> a -> a
  max :: a -> a -> a
  
instance RationalOrd (Term Rational) where
  min = Min 
  max = Max 
  
instance RationalOrd (ArithExpr Rational) where
  min = AMin 
  max = AMax

class BoolNot where
  not  :: Term Bool -> Term Bool

instance BoolOrd (Term String) (Term Rational) where
  x <= y = Leq x y
  x < y = LT x y
  x > y = GT x y
  x >= y = Geq x y
  
instance BoolNot where
  not = Not
  
class LogicalExpr where
  (&&) :: Term Bool -> Term Bool -> Term Bool
  
instance LogicalExpr  where
  x && y = And x y
  
newtype Diff = DiffT Symbol deriving (Show, Eq)

data ExternalDecl a b where
     InputVarDecl    :: Symbol -> ExternalDecl Input V
     OutputVarDecl   :: Symbol -> Rational -> ExternalDecl Output V
     InputEventDecl  :: String -> ExternalDecl Input E
     OutputEventDecl :: String -> Term Bool -> ExternalDecl Output E
     
data InternalSymbolDecl where
     SymbolDecl      :: Symbol -> ArithExpr Rational -> InternalSymbolDecl

data External a b where
     InputVar    :: Symbol -> External Input V
     OutputVar   :: Symbol -> External Output V
     InputEvent  :: String -> External Input E
     OutputEvent :: String -> External Output E

data ArithExpr a where
     A            :: (GetVar b) => b -> ArithExpr a
     AR           :: Rational -> ArithExpr Rational
     Plus         :: ArithExpr a -> ArithExpr a -> ArithExpr a
     Abs          :: ArithExpr a -> ArithExpr a
     Mult         :: ArithExpr a -> ArithExpr a -> ArithExpr a
     Negate       :: ArithExpr a -> ArithExpr a
     Signum       :: ArithExpr a -> ArithExpr a
     FromInteger  :: Integer -> ArithExpr a
     FromRational :: Rational -> ArithExpr a
     Exp          :: ArithExpr a -> ArithExpr a -> ArithExpr a
     Div          :: ArithExpr a -> ArithExpr a -> ArithExpr a
     AMin         :: ArithExpr a -> ArithExpr a -> ArithExpr a
     AMax         :: ArithExpr a -> ArithExpr a -> ArithExpr a
     FCall        :: String -> [ArithExpr a] -> Maybe String -> ArithExpr a

instance Num (ArithExpr a) where
  x + y = Plus x y
  x * y = Mult x y
  negate = Negate
  abs = Abs
  signum = Signum
  fromInteger = FromInteger
  
instance Fractional (ArithExpr a) where
  x / y = Div x y
  fromRational = FromRational
  
class Exponentiation where
  (^) :: ArithExpr a -> ArithExpr a -> ArithExpr a
  
instance Exponentiation  where
  x ^ y = Exp x y

-- This is the ODE data type 
data Ode where
     Ode :: Diff -> ArithExpr Rational -> Rational -> Ode
     
class GetIn a where
  getIn  :: a -> String -> String
  getSym :: a -> String -> Symbol

instance GetIn (External Output V) where
  getIn (OutputVar (S x)) name = name ++ "_" ++ x
  getSym (OutputVar (S x)) _ = S x

instance GetIn Symbol where
  getIn (S x) _ = x ++ "_u"
  getSym (S x) _ = S (x ++ "_u")

data Lo
data Ee

class DiffOrEInE a where
  getDiffOrEInE :: a -> Symbol
  isInputVar :: a -> Bool

instance DiffOrEInE Diff where
  getDiffOrEInE (DiffT x) = x
  isInputVar _ = False

instance DiffOrEInE (External Input V) where
  getDiffOrEInE (InputVar x) = x
  isInputVar _ = True

data Invariant a where
     InvariantLoc  :: (DiffOrEInE b) => b -> Term Bool -> Invariant Lo
     InvariantEdge :: Term Bool -> Invariant Ee

                     
type LocName = Lit
data Loc where
     Loc :: LocName -> [Ode] -> [Invariant Lo] -> 
               ([Update Symbol], [Update (External Output V)]) -> Loc


type SourceLoc = Loc
type DestLoc = Loc
data Edge where
     Edge :: SourceLoc -> DestLoc -> [Invariant Ee] 
                                   -> ([Update Symbol], [Update (External Output V)],
                                      [Update (External Output E)]) 
                                   -> [Event] -> Edge

data Input
data Output
data V
data E

data Bind where
     (:<) :: External Input a -> External Output a -> Bind
     
            
type InitialLoc = Loc
type HaName = Lit
data Ha where
     Ha :: HaName -> [Loc] -> InitialLoc 
                              -> [Edge] 
                              -> [InternalSymbolDecl]
                              -> [ExternalDecl Input V]
                              -> [ExternalDecl Output V]
                              -> [ExternalDecl Input E]
                              -> [ExternalDecl Output E]
                              -> Ha

data HaSeries         = LastHa Ha
                      | Ha ::: HaSeries

data BindSeries where
     LastBind  :: Bind -> BindSeries
     (:<<)     :: Bind -> BindSeries -> BindSeries

type BindHa = [(Ha, BindSeries, Ha)] 

-- The promela types
data PromelaTypes = Int
                  | Bit
                  | Bool
                  | Byte
                  | Pid
                  | Unsigned Integer deriving (Show, Eq)
                  
-- Should this have initial value?
data PromelaVar where
     IntVar   :: Symbol -> Int32 -> PromelaVar
     BoolVar  :: Symbol -> Bool -> PromelaVar
     ByteVar  :: Symbol -> Word8 -> PromelaVar
     ShortVar :: Symbol -> Int16 -> PromelaVar

type PromelaBind = [(Ha, External Output V, PromelaVar)]

data BackEnd = Promela
             | C deriving (Show, Eq)
