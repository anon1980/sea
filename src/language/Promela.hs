-- This file is the Promela backend generator
{-# LANGUAGE GADTs #-}
module Promela (compileReactions) where 
import Language
import Prelude hiding ((<$>), LT, GT)
import Text.PrettyPrint.ANSI.Leijen
import qualified Data.List as L
import Data.Char
import qualified Hshac as H

(|>) :: a -> (a -> b) -> b
(|>) x f = f x
-- All the include files
includes :: Doc
includes = text ("c_decl {\\#include<math.h>};") 
           <$> text "c_decl{extern double d;};"
           -- <$> text "c_decl {extern void writeOutput();};"

getSymbol :: Symbol -> Doc
getSymbol (S x) = text x


textArithExpr                    :: (ArithExpr a) -> String -> Doc
textArithExpr (FCall fn x _) n   = let 
                                     xx = map (\y -> textArithExpr y n) x |> L.intersperse (text ",") |> hcat |> parens
                                   in text fn <+> xx
textArithExpr (AR x) _           = text (show (fromRational x :: Double))
textArithExpr (A x) name         = text $ getVar x name
textArithExpr (Exp x1 x2) n      = text "pow (" <+> textArithExpr x1 n |> parens <+> text ","
                                                 <+> textArithExpr x2 n |> parens <+> text ")"
textArithExpr (Plus x1 x2) n     = (textArithExpr x1 n <+> text "+" <+> textArithExpr x2 n) |> parens
textArithExpr (Abs x) n          = parens (textArithExpr x n) |> (<+> text "abs")
textArithExpr (Mult x1 x2) n     = (textArithExpr x1 n <+> text "*" <+> textArithExpr x2 n) |> parens
textArithExpr (Negate x) n       = (text "-" <+> textArithExpr x n) |> parens
textArithExpr (Signum _) _       = error "Signum function in 'C' not implemented"
textArithExpr (FromInteger x) _  = text (show x)
textArithExpr (FromRational x) _ = text (show (fromRational x :: Double))
textArithExpr (Div x1 x2) n      = (textArithExpr x1 n <+> text "/" <+> textArithExpr x2 n) |> parens
textArithExpr (AMin x1 x2) n      = ((textArithExpr x1 n |> parens 
                                    <+> text "<=" <+> textArithExpr x2 n |> parens) 
                                  |> parens
                                  <+> text "?" <+> textArithExpr x1 n <+> text ":"  
                                  <+> textArithExpr x2 n) |> parens
textArithExpr (AMax x1 x2) n      = ((textArithExpr x1 n |> parens 
                                    <+> text ">=" <+> textArithExpr x2 n |> parens) 
                                  |> parens
                                  <+> text "?" <+> textArithExpr x1 n <+> text ":"  
                                  <+> textArithExpr x2 n) |> parens

getBoolTerm                 :: Term a -> String -> Doc
getBoolTerm TTrue _         = text "1"
getBoolTerm TFalse _        = text "0"
getBoolTerm (T tb) name     = text (getVar tb name)
getBoolTerm (TR tb) _       = (text (show (fromRational tb :: Double)))
getBoolTerm (Not tb) n      = (text "!" <+> (getBoolTerm tb n)) |> parens
getBoolTerm (Leq tb1 tb2) n = ((getBoolTerm tb1 n) <+> text "<=" <+> (getBoolTerm tb2 n)) |> parens
getBoolTerm (Geq tb1 tb2) n = ((getBoolTerm tb1 n) <+> text ">=" <+> (getBoolTerm tb2 n)) |> parens
getBoolTerm (LT tb1 tb2) n  = ((getBoolTerm tb1 n) <+> text "<" <+> (getBoolTerm tb2 n)) |> parens
getBoolTerm (GT tb1 tb2) n  = ((getBoolTerm tb1 n) <+> text ">" <+> (getBoolTerm tb2 n)) |> parens
getBoolTerm (Min t1 t2) n   = ((getBoolTerm t1 n |> parens 
                                    <+> text "<=" <+> getBoolTerm t2 n |> parens) 
                                  |> parens
                                  <+> text "?" <+> getBoolTerm t1 n <+> text ":"  
                                  <+> getBoolTerm t2 n) |> parens
getBoolTerm (Max t1 t2) n   = ((getBoolTerm t1 n |> parens 
                                    <+> text ">=" <+> getBoolTerm t2 n |> parens) 
                                  |> parens
                                  <+> text "?" <+> getBoolTerm t1 n <+> text ":"  
                                  <+> getBoolTerm t2 n) |> parens
getBoolTerm (Eq tb1 tb2) n  = ((getBoolTerm tb1 n) <+> text "==" <+> (getBoolTerm tb2 n)) |> parens
getBoolTerm (NEq tb1 tb2) n = ((getBoolTerm tb1 n) <+> text "!=" <+> (getBoolTerm tb2 n)) |> parens
getBoolTerm (And tb1 tb2) n = ((getBoolTerm tb1 n) <+> text "&&" <+> (getBoolTerm tb2 n)) |> parens

declPromelavars                               :: PromelaBind -> [Doc]
declPromelavars []                            = [] 
declPromelavars ((_,_,(IntVar x31 x32)):xs)   = (text "int"  
                                                 <+> getSymbol x31 
                                                 <+> text "="
                                                 <+> text (show x32) <+> text ";") : declPromelavars xs
declPromelavars ((_,_,(BoolVar x31 x32)):xs)  = (text "bool"  
                                                 <+> getSymbol x31 
                                                 <+> text "="
                                                 <+> text (show x32) <+> text ";") : declPromelavars xs
declPromelavars ((_,_,(ByteVar x31 x32)):xs)  = (text "unsigned char"  
                                                 <+> getSymbol x31 
                                                 <+> text "="
                                                 <+> text (show x32) <+> text ";") : declPromelavars xs
declPromelavars ((_,_,(ShortVar x31 x32)):xs) = (text "short"  
                                                 <+> getSymbol x31 
                                                 <+> text "="
                                                 <+> text (show x32) <+> text ";") : declPromelavars xs
                                                 
declTicks   :: [String] -> Doc
declTicks x = L.foldl' (\a x1 -> (text "bool" 
                                  <+> text (x1 ++ "_tick") 
                                  <+> text "= false;") 
                                 <$> a) empty x

haSeriesNames                                       :: HaSeries -> [String]
haSeriesNames (LastHa (Ha (L x1) _ _ _ _ _ _ _ _))  = [x1]
haSeriesNames ((Ha (L x11) _ _ _ _ _ _ _ _) ::: x2) = x11 : haSeriesNames x2

declStates   :: [String] -> Doc 
declStates x = L.foldl' (\a x1 -> (text "mtype" 
                                   <+> text (x1 ++ "_cstate") 
                                   <+> text "," 
                                   <+> text (x1 ++ "_pstate")
                                   <+> text ";"
                                   <$> text "bool"
                                   <+> text (x1 ++ "_force_init_update")
                                   <+> text ";\n")
                                  <$> a) empty x
                                  
getInitStates :: HaSeries -> [Doc]
getInitStates (LastHa (Ha (L x1) _ (Loc (L x31) _ _ _) _ _ _ _ _ _)) = [text (x1 ++ "_cstate = " 
                                                                              ++ x1 ++ "_" ++ x31 ++ ";")]
getInitStates ((Ha (L x11) _ (Loc (L x131) _ _ _) _ _ _ _ _ _) ::: x2) = 
 text (x11 ++ "_cstate = " 
               ++ x11 ++ "_" ++ x131 ++ ";") : getInitStates x2
               
buildInits :: [InternalSymbolDecl] -> [Doc]
buildInits []   = [] 
-- XXX: Why does giving "" to textArithExpr work, I dont know.
buildInits ((SymbolDecl _ init2):inits) = textArithExpr init2 ""<+> text ";" : buildInits inits

makeInit :: HaSeries -> BindHa -> PromelaBind -> [InternalSymbolDecl] -> Doc
makeInit has b pb inits = 
  let level = 2
      idecs = text "c_code {" <$> (buildInits inits |> vcat |> align) <$> text "};"
      pbinds = bindPromelaVars pb |> vcat |> align
      pbs = text "c_code {" <+> pbinds <+> text "};"
      initstates = getInitStates has |> vcat
  in text "init {"
     <$> text "d_step {" |> indent level
     -- <$> text "c_code {writeOutput();};" |> indent (level * 2)
     <$> idecs |> indent (level * 2)
     <$> pbs |> indent (level * 2)
     <$> H.makBind b Promela |> vcat |> align |> indent (level * 2)
     <$> initstates |> indent (level * 2)
     <$> text "}" |> indent level
     <$> text "atomic {" |> indent level
     <$> text "run main();" |> indent (level * 2)
     <$> foldl (\a x -> text ("run " ++ x ++ "();") <$> a) empty (haSeriesNames has)
         |> indent (level * 2)
     <$> text "}" |> indent level
     <$> text "}"


collectHaConts                                   :: HaSeries -> [Symbol]
collectHaConts (LastHa (Ha _ _ _ _ x5 _ _ _ _))  = map (\(SymbolDecl x _) -> x) x5
collectHaConts ((Ha _ _ _ _ x15 _ _ _ _) ::: x2) = map (\(SymbolDecl x _) -> x) x15 ++ (collectHaConts x2)

bindPromVars                                              :: Lit -> External Output V -> PromelaVar -> Doc
bindPromVars (L l) (OutputVar (S o)) (IntVar (S pv1) _)   = text ("now." ++ pv1 ++ " = (int)" ++ l ++ "_" ++ o ++ ";")
bindPromVars (L l) (OutputVar (S o)) (BoolVar (S pv1) _)  = text ("now." ++ pv1 ++ " = (bool)" ++ l ++ "_" ++ o ++ ";")
bindPromVars (L l) (OutputVar (S o)) (ByteVar (S pv1) _)  = text ("now." ++ pv1 
                                                                  ++ " = (unsigned char)" ++ l ++ "_" ++ o ++ ";")
bindPromVars (L l) (OutputVar (S o)) (ShortVar (S pv1) _) = text ("now." ++ pv1 ++ " = (short)" ++ l ++ "_" ++ o ++ ";") 

bindPromelaVars    :: PromelaBind -> [Doc]
bindPromelaVars [] = []
bindPromelaVars (((Ha x11 _ _ _ _ _ _ _ _),x2,x3):xs) = 
                   bindPromVars x11 x2 x3 : bindPromelaVars xs

makeMain :: HaSeries -> BindHa -> PromelaBind -> Doc
makeMain has b pb = 
    let level = 2
        hnames = haSeriesNames has
        haticks = map (\x -> text (x ++ "_tick")) hnames
                  |> L.intersperse (text " && ") |> fillCat |> align
        hatiflase = foldl (\a x -> text (x ++ "_tick = false;") <$> a) empty hnames
        -- XXX: Make the x = x_u for all HAs
        hconts = collectHaConts has |> map (\(S x) -> text (x ++ " = " ++ x ++ "_u;")) |> vcat |> align
        hcts = text "c_code {" <+> hconts <+> text "};"
        -- XXX: Build the binding between promela vars and HA vars
        pbinds = bindPromelaVars pb |> vcat |> align
        pbs = text "c_code {" <+> pbinds <+> text "};"
    in text "proctype main () {"
       <$> text "do" |> indent level
       <$> (text " :: " <+> haticks |> parens <+> text " -> ") |> indent (level * 2)
       <$> text "d_step {" |> indent (level * 3)
       -- <$> text "c_code {writeOutput();};" |> indent (level * 4)
       <$> hcts |> indent (level * 4)
       <$> pbs |> indent (level * 4)
       <$> H.makBind b Promela |> vcat |> align |> indent (level * 4)
       <$> hatiflase |> align |> indent (level * 4)
       <$> text "}" |> indent (level * 3)
       <$> text " :: else -> true" |> indent (level * 2)
       <$> text "od" |> indent level
       <$> text "}"

-- The compileReaction function
compileReactions :: HaSeries -> BindHa -> PromelaBind -> Doc
compileReactions hs b pb = 
     let 
         -- TODO: Check that cont vars are not shared between HAs
         -- First declare the list of promela vars
         pdecls = declPromelavars pb |> vcat |> align -- The promela var declarations
         incs = includes  -- The includes files
         lticks = declTicks $ haSeriesNames hs -- The delcaration of local ticks
         cpstates = declStates $ haSeriesNames hs
         (ptypes, inits) = compileReacs hs b pb |> unzip
         proctypes = ptypes |> vcat |> align  -- The proc declarations
     in pdecls <$> incs <$> lticks <$> cpstates <$> proctypes
        <$> makeMain hs b pb <$> makeInit hs b pb (inits |> L.concat)

compileReacs :: HaSeries -> BindHa -> PromelaBind -> [(Doc, [InternalSymbolDecl])]
compileReacs (LastHa hs) b pb = [compileReaction hs b pb] 
compileReacs (hs1 ::: hs2) b pb = (compileReaction hs1 b pb) : (compileReacs hs2 b pb)

declConst                           :: [InternalSymbolDecl] -> String -> Loc -> [Doc]
declConst [] _ _                    = []
declConst ((SymbolDecl (S x) i):xs) n st = 
      (text "c_decl { double"
      <+> text (x ++ "_u,")
      <+> text (x ++ "_slope,")
      <+> text (x ++ "_init,")
      <+> text (x ++ " = ")
      <+> textArithExpr i n
      <+> text ";};")
      : declConst xs n st
      
declOutPutVars                                    :: [ExternalDecl Output V] -> String -> [Doc]
declOutPutVars [] _                               = []
declOutPutVars ((OutputVarDecl (S ss1) ss2):sss) name = 
      (text "c_decl {double "
      <+> text (name ++ "_" ++ ss1)
      <+> text "="
      <+> text (show (fromRational ss2 :: Double))
      <+> text ";};"
      <$> text "c_track"
      <+> text ("\"&" ++ name ++ "_" ++ ss1 ++ "\"")
      <+> text "\"sizeof(double)\";")
      : declOutPutVars sss name
      
declInputVars                                :: [ExternalDecl Input V] -> String -> [Doc]
declInputVars []  _                          = []
declInputVars ((InputVarDecl (S eiv)):eivs) name = 
  (text "c_decl {double "
      <+> text (name ++ "_" ++ eiv)
      <+> text ";};"
      <$> text "c_track"
      <+> text ("\"&" ++ name ++ "_" ++ eiv ++ "\"")
      <+> text "\"sizeof(double)\";")
  : declInputVars eivs name

declOutputEvents :: [ExternalDecl Output E] -> String -> [Doc]
declOutputEvents []  _ = []
declOutputEvents ((OutputEventDecl eov1 eov2):eovs) name = 
  (text "bool"
  <+> text (name ++ "_" ++ eov1)
  <+> text "="
  <+> getBoolTerm eov2 name
  <+> text ";")
  : declOutputEvents eovs name
  
declInputEvents                                  :: [ExternalDecl Input E] -> String -> [Doc]
declInputEvents []  _                            = []
declInputEvents ((InputEventDecl eiv):eivs) name = 
  (text "bool"
  <+> text (name ++ "_" ++ eiv)
  <+> text ";")
  : declInputEvents eivs name

trackConsts                 :: [InternalSymbolDecl] -> String -> [Doc]
trackConsts [] _            = [] 
trackConsts ((SymbolDecl (S x) _):xs) name = (text "c_track" 
                                          <+> text ("\"&" ++ x ++ "\"") 
                                          <+> text "\"sizeof(double)\";"
                                          <$> text "c_track"
                                          <+> text ("\"&" ++ x ++ "_slope\"") 
                                          <+> text "\"sizeof(double)\";"
                                          <$> text "c_track"
                                          <+> text ("\"&" ++ x ++ "_u\"")
                                          <+> text "\"sizeof(double)\";"
                                          <$> text "c_track"
                                          <+> text ("\"&" ++ x ++ "_init\"")
                                          <+> text "\"sizeof(double)\";")
                                          : trackConsts xs name
declMtypes           :: [Loc] -> String -> [Doc]
declMtypes []   _    = []
declMtypes ((Loc (L locs1) _ _ _):locss) name = 
                     (text $ name ++ "_" ++ locs1)
                     : declMtypes locss name
                     
areEqual :: Loc -> Loc -> Bool
areEqual (Loc (L x1) _ _ _) (Loc (L y1) _ _ _) = x1 Prelude.== y1

compileReaction                                          :: Ha -> BindHa -> PromelaBind -> (Doc, [InternalSymbolDecl])
compileReaction (Ha (L name) locs start edges conts eivar eovar eie eov) _ _ = 
  let 
      decfcalls = H.defExternFunCallsS conts |> vcat |> align
      ecalls = H.defExternFunCalls locs edges |> vcat |> align |> (<$> decfcalls)
      dfcalls = text "c_decl {" <$> ecalls <$> text "};"
      initvarsFunCall = H.getFunCallInit conts 
      initvarsNFunCall = H.getNFunCallInit conts
      decls = declConst initvarsNFunCall name start |> vcat |> align
      tracks = trackConsts conts name |> vcat |> align
      outputvars = declOutPutVars eovar name |> vcat |> align
      inputvars = declInputVars eivar name |> vcat |> align
      outputevs = declOutputEvents eov name |> vcat |> align
      inputevs = declInputEvents eie name |> vcat |> align
      mtypes = text "mtype {" <+> declMtypes locs name |> L.intersperse (text ", ") 
               |> hcat <+> text "};" 
               |> align
  in ((dfcalls <$> decls <$> tracks <$> outputvars <$> inputvars
      <$> outputevs <$> inputevs <$> mtypes <$> makeReaction), initvarsFunCall)

  where

  makeReaction :: Doc
  makeReaction = 
      let 
          level = 2
          getLocs = makeLocs level |> vcat
      in 
         text ("proctype " ++ name ++ " () {")
         -- XXX: Everything goes in here
         <$> getLocs |> align
         <$> text "}" |> align
         
  makeLocs       :: Int -> [Doc]
  makeLocs level = map (makeLoc level) locs

  -- TODO: Need to fix this. First put the outputs and then remove the
  -- !events from intralocation transitions.
  makeLoc                                     :: Int -> Loc -> Doc
  makeLoc level ll@(Loc (L loc1) loc2 loc3 (internal, loc4)) = 
    let intra = makeIntra loc1 (level + 1) loc2 loc3 loc4 internal
        ibody = makeInter (level + 3) $ L.filter (\(Edge s _ _ _ _) -> areEqual s ll) edges
    in text (toUpper (head loc1) : L.tail loc1) <> text ":"
       <$> text "if" |> indent (level + 1)
       <$> foldl (\a (i, bod, d) -> 
       text  " :: " <+> i <+> text " -> " |> indent (level + 2)
       <$> text "d_step {" |> indent (level + 4)
       <$> bod |> indent (level + 5)
       <$> text "};" |> indent (level + 4)
       <$> d |> indent (level + 4)
       <$> a) empty ibody
       <$> text " :: else -> " |> indent (level + 2)
       <$> text "if" |> indent (level + 3)
       <$> intra |> indent (level + 4)
       <$> text " :: else -> true" |> indent (level + 3)
       <$> text "fi;" |> indent (level + 2) |> align
       <$> text "fi;" |> indent (level + 1) |> align

  getInvs :: [Invariant Lo] -> Symbol -> Doc
  getInvs invs ode = 
    let 
        is = L.find (\(InvariantLoc x _) -> (getDiffOrEInE x) Prelude.== ode 
                                            || isInputVar x) invs
    in case is of
         Just (InvariantLoc _ x2) -> getBoolTerm x2 name
         Nothing -> empty

  getNotExternalInputEvents          :: [ExternalDecl Input E] -> [Doc]
  getNotExternalInputEvents []       = []
  getNotExternalInputEvents ((InputEventDecl xs):xss) = 
    (text "!" <+> text (name ++ "_" ++ xs)) |> parens
    : getNotExternalInputEvents xss
    
  declInit                    :: [Symbol] -> [Doc]
  declInit []                 = []
  declInit ((S conts):contss) = (text (conts ++ "_init") <+> text "=" 
                                 <+> text conts <+> text ";")
                                : declInit contss

  buildSatBody             :: Symbol -> Rational -> Rational -> Doc
  buildSatBody (S x) minn maxx = text (x ++ "_u")
                                 <+> text "="
                                 <+> (text (x ++ "_u < ") <+> text (show (fromRational minn :: Double))
                                 <+> text (" && signbit(" ++ x ++ "_slope) && ") <+> text (x ++ "_init > ") 
                                 <+> text (show (fromRational minn :: Double))) |> parens
                                 <+> text " ? " <+> text (show (fromRational minn :: Double))
                                 <+> text ":" <+> text (x ++ "_u") <+> text ";"
                                 -- XXX: Now the max part
                                 <$> text (x ++ "_u")
                                 <+> text "="
                                 <+> (text (x ++ "_u > ") <+> text (show (fromRational maxx :: Double))
                                 <+> text (" && !signbit(" ++ x ++ "_slope) && ") <+> text (x ++ "_init < ")
                                 <+> text (show (fromRational maxx :: Double))) |> parens
                                 <+> text " ? " <+> text (show (fromRational maxx :: Double))
                                 <+> text ":" <+> text (x ++ "_u") <+> text ";"

  getSaturationBody           :: [Ode] -> [Invariant Lo] -> [Doc]
  getSaturationBody []   []   = []
  getSaturationBody odes invs = 
      map (\(Ode (DiffT o1) _ _) ->
               case L.find (\(InvariantLoc x1 _) -> (getDiffOrEInE x1) Prelude.== o1) invs of
                 Just x ->
                     let (minn, maxx) = H.getMinMax x
                     in case (minn, maxx) of
                          (Just minn, Just maxx) -> 
                              if H.getlbArb x
                              then buildSatBody o1 minn maxx
                              else empty
                          (_, _) -> empty
                 Nothing -> empty) odes


  declOdes                                         :: [Ode] -> [Doc]
  declOdes []                                      = []
  declOdes ((Ode (DiffT (S odes1)) slope _):odess) = 
      let sl = textArithExpr slope name
      in 
      -- (text ("if ((" ++ name ++ "_pstate != " ++ name ++ "_cstate) || " ++ name ++ "_force_init_update)")
      -- <+> text (odes1 ++ "_init") <+> text "=" <+> text odes1 <+> text ";"
      (text (odes1 ++ "_slope = ") <+> sl <+> text ";"
      <$> text (odes1 ++ "_u") 
      <+> text "=" 
      <+> ((text (odes1 ++ "_slope * d")) |> parens) 
      <+> text "+" 
      <+> text odes1 
      <+> text ";" ): declOdes odess

  makeAssignmentStatements     :: String -> [Update (External Output V)] -> [Doc]
  makeAssignmentStatements _ []  = []
  makeAssignmentStatements n ((ass1 := ass2):asss) = 
      text n <> text "_" <> getSymbol (getSym ass1 n)
      <+> text "="
      <+> textArithExpr ass2 name <+> text ";" : (makeAssignmentStatements n asss)
      
  makeAssignmentStatements2     :: [Update Symbol] -> [Doc]
  makeAssignmentStatements2 []  = []
  makeAssignmentStatements2 ((ass1 := ass2):asss) = 
      getSymbol (getSym ass1 "")
      <+> text "="
      <+> textArithExpr ass2 name <+> text ";" : (makeAssignmentStatements2 asss)

  setIFalse [] = []
  setIFalse ((InputEventDecl x):xs) = text (name ++ "_" ++ x) 
                                      <+> text " = false;"
                                      : setIFalse xs

  setOFalse   :: [ExternalDecl Output E] -> [Doc]
  setOFalse [] = []
  setOFalse ((OutputEventDecl x1 _):xs) = text (name ++ "_" ++ x1) 
                                          <+> text " = false;"
                                          : setOFalse xs

  makeIntra                             :: String -> Int -> [Ode] -> [Invariant Lo] -> [Update (External Output V)] ->
                                                                                       [Update Symbol] -> Doc
  makeIntra lname level odes invs ups iups = 
    let 
      -- XXX: Get the invs expression
      iis = (map (getInvs invs) $ map (\(Ode (DiffT odes1) _ _) -> odes1) odes)
      invvs = filter (\(InvariantLoc x _) -> isInputVar x) invs
      iiis = map (\(InvariantLoc _ x) -> getBoolTerm x name) invvs
      is = case iis of
             [] -> [text "1"] ++ iiis
             _  -> iis ++ iiis
      inv_cexpr = text "c_expr {"
                  <+> is
                  |> L.intersperse (text " && ") |> hcat
                  <+> text "}"
      -- XXX: Get the not of input events
      not_eie = getNotExternalInputEvents eie
      n_eie = case not_eie of
                  [] -> [text "true"]
                  _ -> not_eie
                  |> L.intersperse (text " && ") |> fillCat 
      inv = (inv_cexpr <+> text " && " 
            <+> n_eie <+> text " && " 
            <+> text (name ++ "_cstate ==")
            <+> (text (name ++ "_" ++ lname)))
            |> parens

      -- XXX: Get the ode c_code part
      inits = (text "c_code {") <+> declInit (map (\(Ode (DiffT odes1) _ _) -> odes1) odes) 
              |> fillCat <+> text "};"

      odeBody = declOdes odes |> vcat |> align
      satBody = getSaturationBody odes invs |> vcat |> align
      assignments = makeAssignmentStatements name ups |> vcat |> align
      assignments2 = makeAssignmentStatements2 iups |> vcat |> align

       in 
          (text "::" <+> inv <+> text "->")|> indent (level + 1)
          <$> text "if" |> indent (level + 2)
          <$> (text (":: " ++ name ++ "_pstate != " ++ name ++ "_cstate || " ++ name ++ "_force_init_update")
          <+> text "->") |> indent (level + 3)
          <$> inits |> indent (level + 4)
          <$> text ":: else -> true" |> indent (level + 3)
          <$> text "fi;" |> indent (level + 2)
          <$> text "d_step {" |> indent (level + 2)
          <$> text "c_code {" |> indent (level + 3)
          <$> odeBody |> indent (level + 4)
          <$> satBody |> indent (level + 4)
          <$> assignments2 |> indent (level + 4)
          <$> assignments |> indent (level + 4)
          <$> text "};" |> indent (level + 3)
          <$> text (name ++ "_pstate = " ++ name ++ "_cstate;") |> indent (level + 3)
          <$> text (name ++ "_cstate = " ++ name ++ "_" ++ lname ++ ";") 
          |> indent (level + 3) 
          <$> text (name ++ "_force_init_update = false;") 
          |> indent (level + 3)
          <$> setOFalse eov |> vcat |> indent (level + 3) |> align
          <$> setIFalse eie |> vcat |> indent (level + 3) |> align 
          <$> text (name ++ "_tick = true;") |> indent (level + 3)
          <$> text "};" |> indent (level + 2)
          -- XXX: This is where the do-od goes
          <$> text "do" |> indent (level + 2)
          <$> (text "::" <+> text (name ++ "_tick -> true")) |> indent (level + 3) |> align
          <$> (text "::" <+> text "else -> goto " <+> text (toUpper (lname !! 0) : L.tail lname)) 
              |> indent (level + 3) |> align
          <$> text "od;" |> indent (level + 2) |> align

  getInv                       :: Invariant a -> Doc
  getInv (InvariantEdge inv2)  = text "c_expr {" <+> getBoolTerm inv2 name <+> text "}"
  getInv (InvariantLoc _ inv2) = text "c_expr {" <+> getBoolTerm inv2 name <+> text "}"

  getIInvs              :: [Invariant a] -> [Doc]
  getIInvs []           = []
  getIInvs (invs:invss) = getInv invs : getIInvs invss

  makeInter                                                          :: Int -> [Edge] -> [(Doc, Doc, Doc)]
  makeInter _ []                                                     = []
  makeInter level ((Edge (Loc (L sloc1) _ _ _) (Loc (L tloc1) _ _ _) linvs updates evs):edgess) = 
      let 
      invs = (getIInvs linvs ++ getEvents evs ++ [text (name ++ "_cstate == " ++ name ++ "_" ++ sloc1)])
             |> L.intersperse (text " && ")
             |> fillCat |> align |> parens
      ups = (getUpdate updates |> vcat |> align) |> indent level
      jstatep = (text (name ++ "_pstate = ") <+>  text (name ++ "_cstate") <+> text ";")
                |> indent level
      jstate = (text (name ++ "_cstate = ") <+>  text (name ++ "_" ++ tloc1) <+> text ";")
               |> indent level
      force_up = if sloc1 Prelude.== tloc1
                 then text (name ++ "_force_init_update = true;") |> indent level
                 else text (name ++ "_force_init_update = false;") |> indent level
      tick_up = text (name ++ "_tick = true;") |> indent level
      dood = text "do"
             <$> text (" :: " ++ name ++ "_tick -> true")
             <$> text (" :: else -> goto ") <+> text (toUpper (tloc1 !! 0) : tail tloc1)
             <$> text "od;"
      in (invs, (ups <$> jstatep <$> jstate <$> force_up <$> tick_up) |> align, dood) 
             : makeInter level edgess

  getEvents        :: [Event] -> [Doc]
  getEvents []     = []
  getEvents ((Event x):xs) = text (getEvent x name) : getEvents xs

  getUpdate         :: ([Update Symbol], [Update (External Output V)], [Update (External Output E)]) -> [Doc]
  getUpdate (x,y,z) = (getUpdates x) ++ (getUpdates y) ++ (getUpdates z)

  getUpdates :: [Update a] -> [Doc]
  getUpdates []  = []
  getUpdates ((:!)(OutputEvent x) : upss) = 
            text (name ++ "_" ++ x)
            <+> text "= 1;" : getUpdates upss
  getUpdates ((ups1 := ups2):upss) = 
              text "c_code { "
              <+> text (getIn ups1 name)
              <+> text "="
              <+> textArithExpr ups2 name
              <+> text ";"
              <+> text "};"
              : getUpdates upss
