#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double Slow_v_i_0;
double Slow_v_i_1;
double Slow_v_i_2;
double Slow_voo = 0.0;
double Slow_state = 0.0;


static double  Slow_vx  =  0 ,  Slow_vy  =  0 ,  Slow_vz  =  0 ,  Slow_g  =  0 ,  Slow_v  =  0 ,  Slow_ft  =  0 ,  Slow_theta  =  0 ,  Slow_v_O  =  0 ; //the continuous vars
static double  Slow_vx_u , Slow_vy_u , Slow_vz_u , Slow_g_u , Slow_v_u , Slow_ft_u , Slow_theta_u , Slow_v_O_u ; // and their updates
static double  Slow_vx_init , Slow_vy_init , Slow_vz_init , Slow_g_init , Slow_v_init , Slow_ft_init , Slow_theta_init , Slow_v_O_init ; // and their inits
static double  slope_Slow_vx , slope_Slow_vy , slope_Slow_vz , slope_Slow_g , slope_Slow_v , slope_Slow_ft , slope_Slow_theta , slope_Slow_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { Slow_t1 , Slow_t2 , Slow_t3 , Slow_t4 }; // state declarations

enum states Slow (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( Slow_t1 ):
    if (True == False) {;}
    else if  (Slow_g > (44.5)) {
      Slow_vx_u = (0.3 * Slow_v) ;
      Slow_vy_u = 0 ;
      Slow_vz_u = (0.7 * Slow_v) ;
      Slow_g_u = ((((((((Slow_v_i_0 + (- ((Slow_vx + (- Slow_vy)) + Slow_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Slow_v_i_1 + (- ((Slow_vx + (- Slow_vy)) + Slow_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Slow_theta_u = (Slow_v / 30.0) ;
      Slow_v_O_u = (131.1 + (- (80.1 * pow ( ((Slow_v / 30.0)) , (0.5) )))) ;
      Slow_ft_u = f (Slow_theta,4.0e-2) ;
      cstate =  Slow_t2 ;
      force_init_update = False;
    }

    else if ( Slow_v <= (44.5)
               && Slow_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) Slow_vx_init = Slow_vx ;
      slope_Slow_vx = (Slow_vx * -8.7) ;
      Slow_vx_u = (slope_Slow_vx * d) + Slow_vx ;
      if ((pstate != cstate) || force_init_update) Slow_vy_init = Slow_vy ;
      slope_Slow_vy = (Slow_vy * -190.9) ;
      Slow_vy_u = (slope_Slow_vy * d) + Slow_vy ;
      if ((pstate != cstate) || force_init_update) Slow_vz_init = Slow_vz ;
      slope_Slow_vz = (Slow_vz * -190.4) ;
      Slow_vz_u = (slope_Slow_vz * d) + Slow_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Slow_t1 ;
      force_init_update = False;
      Slow_g_u = ((((((((Slow_v_i_0 + (- ((Slow_vx + (- Slow_vy)) + Slow_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Slow_v_i_1 + (- ((Slow_vx + (- Slow_vy)) + Slow_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Slow_v_u = ((Slow_vx + (- Slow_vy)) + Slow_vz) ;
      Slow_voo = ((Slow_vx + (- Slow_vy)) + Slow_vz) ;
      Slow_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow!\n");
      exit(1);
    }
    break;
  case ( Slow_t2 ):
    if (True == False) {;}
    else if  (Slow_v >= (44.5)) {
      Slow_vx_u = Slow_vx ;
      Slow_vy_u = Slow_vy ;
      Slow_vz_u = Slow_vz ;
      Slow_g_u = ((((((((Slow_v_i_0 + (- ((Slow_vx + (- Slow_vy)) + Slow_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Slow_v_i_1 + (- ((Slow_vx + (- Slow_vy)) + Slow_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  Slow_t3 ;
      force_init_update = False;
    }
    else if  (Slow_g <= (44.5)
               && Slow_v < (44.5)) {
      Slow_vx_u = Slow_vx ;
      Slow_vy_u = Slow_vy ;
      Slow_vz_u = Slow_vz ;
      Slow_g_u = ((((((((Slow_v_i_0 + (- ((Slow_vx + (- Slow_vy)) + Slow_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Slow_v_i_1 + (- ((Slow_vx + (- Slow_vy)) + Slow_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  Slow_t1 ;
      force_init_update = False;
    }

    else if ( Slow_v < (44.5)
               && Slow_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) Slow_vx_init = Slow_vx ;
      slope_Slow_vx = ((Slow_vx * -23.6) + (777200.0 * Slow_g)) ;
      Slow_vx_u = (slope_Slow_vx * d) + Slow_vx ;
      if ((pstate != cstate) || force_init_update) Slow_vy_init = Slow_vy ;
      slope_Slow_vy = ((Slow_vy * -45.5) + (58900.0 * Slow_g)) ;
      Slow_vy_u = (slope_Slow_vy * d) + Slow_vy ;
      if ((pstate != cstate) || force_init_update) Slow_vz_init = Slow_vz ;
      slope_Slow_vz = ((Slow_vz * -12.9) + (276600.0 * Slow_g)) ;
      Slow_vz_u = (slope_Slow_vz * d) + Slow_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Slow_t2 ;
      force_init_update = False;
      Slow_g_u = ((((((((Slow_v_i_0 + (- ((Slow_vx + (- Slow_vy)) + Slow_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Slow_v_i_1 + (- ((Slow_vx + (- Slow_vy)) + Slow_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Slow_v_u = ((Slow_vx + (- Slow_vy)) + Slow_vz) ;
      Slow_voo = ((Slow_vx + (- Slow_vy)) + Slow_vz) ;
      Slow_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow!\n");
      exit(1);
    }
    break;
  case ( Slow_t3 ):
    if (True == False) {;}
    else if  (Slow_v >= (131.1)) {
      Slow_vx_u = Slow_vx ;
      Slow_vy_u = Slow_vy ;
      Slow_vz_u = Slow_vz ;
      Slow_g_u = ((((((((Slow_v_i_0 + (- ((Slow_vx + (- Slow_vy)) + Slow_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Slow_v_i_1 + (- ((Slow_vx + (- Slow_vy)) + Slow_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  Slow_t4 ;
      force_init_update = False;
    }

    else if ( Slow_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) Slow_vx_init = Slow_vx ;
      slope_Slow_vx = (Slow_vx * -6.9) ;
      Slow_vx_u = (slope_Slow_vx * d) + Slow_vx ;
      if ((pstate != cstate) || force_init_update) Slow_vy_init = Slow_vy ;
      slope_Slow_vy = (Slow_vy * 75.9) ;
      Slow_vy_u = (slope_Slow_vy * d) + Slow_vy ;
      if ((pstate != cstate) || force_init_update) Slow_vz_init = Slow_vz ;
      slope_Slow_vz = (Slow_vz * 6826.5) ;
      Slow_vz_u = (slope_Slow_vz * d) + Slow_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Slow_t3 ;
      force_init_update = False;
      Slow_g_u = ((((((((Slow_v_i_0 + (- ((Slow_vx + (- Slow_vy)) + Slow_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Slow_v_i_1 + (- ((Slow_vx + (- Slow_vy)) + Slow_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Slow_v_u = ((Slow_vx + (- Slow_vy)) + Slow_vz) ;
      Slow_voo = ((Slow_vx + (- Slow_vy)) + Slow_vz) ;
      Slow_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow!\n");
      exit(1);
    }
    break;
  case ( Slow_t4 ):
    if (True == False) {;}
    else if  (Slow_v <= (30.0)) {
      Slow_vx_u = Slow_vx ;
      Slow_vy_u = Slow_vy ;
      Slow_vz_u = Slow_vz ;
      Slow_g_u = ((((((((Slow_v_i_0 + (- ((Slow_vx + (- Slow_vy)) + Slow_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Slow_v_i_1 + (- ((Slow_vx + (- Slow_vy)) + Slow_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  Slow_t1 ;
      force_init_update = False;
    }

    else if ( Slow_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) Slow_vx_init = Slow_vx ;
      slope_Slow_vx = (Slow_vx * -33.2) ;
      Slow_vx_u = (slope_Slow_vx * d) + Slow_vx ;
      if ((pstate != cstate) || force_init_update) Slow_vy_init = Slow_vy ;
      slope_Slow_vy = ((Slow_vy * 20.0) * Slow_ft) ;
      Slow_vy_u = (slope_Slow_vy * d) + Slow_vy ;
      if ((pstate != cstate) || force_init_update) Slow_vz_init = Slow_vz ;
      slope_Slow_vz = ((Slow_vz * 2.0) * Slow_ft) ;
      Slow_vz_u = (slope_Slow_vz * d) + Slow_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Slow_t4 ;
      force_init_update = False;
      Slow_g_u = ((((((((Slow_v_i_0 + (- ((Slow_vx + (- Slow_vy)) + Slow_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Slow_v_i_1 + (- ((Slow_vx + (- Slow_vy)) + Slow_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Slow_v_u = ((Slow_vx + (- Slow_vy)) + Slow_vz) ;
      Slow_voo = ((Slow_vx + (- Slow_vy)) + Slow_vz) ;
      Slow_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow!\n");
      exit(1);
    }
    break;
  }
  Slow_vx = Slow_vx_u;
  Slow_vy = Slow_vy_u;
  Slow_vz = Slow_vz_u;
  Slow_g = Slow_g_u;
  Slow_v = Slow_v_u;
  Slow_ft = Slow_ft_u;
  Slow_theta = Slow_theta_u;
  Slow_v_O = Slow_v_O_u;
  return cstate;
}