#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double BundleOfHis2_v_i_0;
double BundleOfHis2_v_i_1;
double BundleOfHis2_v_i_2;
double BundleOfHis2_v_i_3;
double BundleOfHis2_voo = 0.0;
double BundleOfHis2_state = 0.0;


static double  BundleOfHis2_vx  =  0 ,  BundleOfHis2_vy  =  0 ,  BundleOfHis2_vz  =  0 ,  BundleOfHis2_g  =  0 ,  BundleOfHis2_v  =  0 ,  BundleOfHis2_ft  =  0 ,  BundleOfHis2_theta  =  0 ,  BundleOfHis2_v_O  =  0 ; //the continuous vars
static double  BundleOfHis2_vx_u , BundleOfHis2_vy_u , BundleOfHis2_vz_u , BundleOfHis2_g_u , BundleOfHis2_v_u , BundleOfHis2_ft_u , BundleOfHis2_theta_u , BundleOfHis2_v_O_u ; // and their updates
static double  BundleOfHis2_vx_init , BundleOfHis2_vy_init , BundleOfHis2_vz_init , BundleOfHis2_g_init , BundleOfHis2_v_init , BundleOfHis2_ft_init , BundleOfHis2_theta_init , BundleOfHis2_v_O_init ; // and their inits
static double  slope_BundleOfHis2_vx , slope_BundleOfHis2_vy , slope_BundleOfHis2_vz , slope_BundleOfHis2_g , slope_BundleOfHis2_v , slope_BundleOfHis2_ft , slope_BundleOfHis2_theta , slope_BundleOfHis2_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { BundleOfHis2_t1 , BundleOfHis2_t2 , BundleOfHis2_t3 , BundleOfHis2_t4 }; // state declarations

enum states BundleOfHis2 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( BundleOfHis2_t1 ):
    if (True == False) {;}
    else if  (BundleOfHis2_g > (44.5)) {
      BundleOfHis2_vx_u = (0.3 * BundleOfHis2_v) ;
      BundleOfHis2_vy_u = 0 ;
      BundleOfHis2_vz_u = (0.7 * BundleOfHis2_v) ;
      BundleOfHis2_g_u = ((((((((BundleOfHis2_v_i_0 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis2_v_i_1 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((BundleOfHis2_v_i_2 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      BundleOfHis2_theta_u = (BundleOfHis2_v / 30.0) ;
      BundleOfHis2_v_O_u = (131.1 + (- (80.1 * pow ( ((BundleOfHis2_v / 30.0)) , (0.5) )))) ;
      BundleOfHis2_ft_u = f (BundleOfHis2_theta,4.0e-2) ;
      cstate =  BundleOfHis2_t2 ;
      force_init_update = False;
    }

    else if ( BundleOfHis2_v <= (44.5)
               && 
              BundleOfHis2_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis2_vx_init = BundleOfHis2_vx ;
      slope_BundleOfHis2_vx = (BundleOfHis2_vx * -8.7) ;
      BundleOfHis2_vx_u = (slope_BundleOfHis2_vx * d) + BundleOfHis2_vx ;
      if ((pstate != cstate) || force_init_update) BundleOfHis2_vy_init = BundleOfHis2_vy ;
      slope_BundleOfHis2_vy = (BundleOfHis2_vy * -190.9) ;
      BundleOfHis2_vy_u = (slope_BundleOfHis2_vy * d) + BundleOfHis2_vy ;
      if ((pstate != cstate) || force_init_update) BundleOfHis2_vz_init = BundleOfHis2_vz ;
      slope_BundleOfHis2_vz = (BundleOfHis2_vz * -190.4) ;
      BundleOfHis2_vz_u = (slope_BundleOfHis2_vz * d) + BundleOfHis2_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  BundleOfHis2_t1 ;
      force_init_update = False;
      BundleOfHis2_g_u = ((((((((BundleOfHis2_v_i_0 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis2_v_i_1 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((BundleOfHis2_v_i_2 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      BundleOfHis2_v_u = ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz) ;
      BundleOfHis2_voo = ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz) ;
      BundleOfHis2_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis2!\n");
      exit(1);
    }
    break;
  case ( BundleOfHis2_t2 ):
    if (True == False) {;}
    else if  (BundleOfHis2_v >= (44.5)) {
      BundleOfHis2_vx_u = BundleOfHis2_vx ;
      BundleOfHis2_vy_u = BundleOfHis2_vy ;
      BundleOfHis2_vz_u = BundleOfHis2_vz ;
      BundleOfHis2_g_u = ((((((((BundleOfHis2_v_i_0 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis2_v_i_1 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((BundleOfHis2_v_i_2 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      cstate =  BundleOfHis2_t3 ;
      force_init_update = False;
    }
    else if  (BundleOfHis2_g <= (44.5)
               && BundleOfHis2_v < (44.5)) {
      BundleOfHis2_vx_u = BundleOfHis2_vx ;
      BundleOfHis2_vy_u = BundleOfHis2_vy ;
      BundleOfHis2_vz_u = BundleOfHis2_vz ;
      BundleOfHis2_g_u = ((((((((BundleOfHis2_v_i_0 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis2_v_i_1 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((BundleOfHis2_v_i_2 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      cstate =  BundleOfHis2_t1 ;
      force_init_update = False;
    }

    else if ( BundleOfHis2_v < (44.5)
               && 
              BundleOfHis2_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis2_vx_init = BundleOfHis2_vx ;
      slope_BundleOfHis2_vx = ((BundleOfHis2_vx * -23.6) + (777200.0 * BundleOfHis2_g)) ;
      BundleOfHis2_vx_u = (slope_BundleOfHis2_vx * d) + BundleOfHis2_vx ;
      if ((pstate != cstate) || force_init_update) BundleOfHis2_vy_init = BundleOfHis2_vy ;
      slope_BundleOfHis2_vy = ((BundleOfHis2_vy * -45.5) + (58900.0 * BundleOfHis2_g)) ;
      BundleOfHis2_vy_u = (slope_BundleOfHis2_vy * d) + BundleOfHis2_vy ;
      if ((pstate != cstate) || force_init_update) BundleOfHis2_vz_init = BundleOfHis2_vz ;
      slope_BundleOfHis2_vz = ((BundleOfHis2_vz * -12.9) + (276600.0 * BundleOfHis2_g)) ;
      BundleOfHis2_vz_u = (slope_BundleOfHis2_vz * d) + BundleOfHis2_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  BundleOfHis2_t2 ;
      force_init_update = False;
      BundleOfHis2_g_u = ((((((((BundleOfHis2_v_i_0 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis2_v_i_1 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((BundleOfHis2_v_i_2 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      BundleOfHis2_v_u = ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz) ;
      BundleOfHis2_voo = ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz) ;
      BundleOfHis2_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis2!\n");
      exit(1);
    }
    break;
  case ( BundleOfHis2_t3 ):
    if (True == False) {;}
    else if  (BundleOfHis2_v >= (131.1)) {
      BundleOfHis2_vx_u = BundleOfHis2_vx ;
      BundleOfHis2_vy_u = BundleOfHis2_vy ;
      BundleOfHis2_vz_u = BundleOfHis2_vz ;
      BundleOfHis2_g_u = ((((((((BundleOfHis2_v_i_0 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis2_v_i_1 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((BundleOfHis2_v_i_2 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      cstate =  BundleOfHis2_t4 ;
      force_init_update = False;
    }

    else if ( BundleOfHis2_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis2_vx_init = BundleOfHis2_vx ;
      slope_BundleOfHis2_vx = (BundleOfHis2_vx * -6.9) ;
      BundleOfHis2_vx_u = (slope_BundleOfHis2_vx * d) + BundleOfHis2_vx ;
      if ((pstate != cstate) || force_init_update) BundleOfHis2_vy_init = BundleOfHis2_vy ;
      slope_BundleOfHis2_vy = (BundleOfHis2_vy * 75.9) ;
      BundleOfHis2_vy_u = (slope_BundleOfHis2_vy * d) + BundleOfHis2_vy ;
      if ((pstate != cstate) || force_init_update) BundleOfHis2_vz_init = BundleOfHis2_vz ;
      slope_BundleOfHis2_vz = (BundleOfHis2_vz * 6826.5) ;
      BundleOfHis2_vz_u = (slope_BundleOfHis2_vz * d) + BundleOfHis2_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  BundleOfHis2_t3 ;
      force_init_update = False;
      BundleOfHis2_g_u = ((((((((BundleOfHis2_v_i_0 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis2_v_i_1 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((BundleOfHis2_v_i_2 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      BundleOfHis2_v_u = ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz) ;
      BundleOfHis2_voo = ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz) ;
      BundleOfHis2_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis2!\n");
      exit(1);
    }
    break;
  case ( BundleOfHis2_t4 ):
    if (True == False) {;}
    else if  (BundleOfHis2_v <= (30.0)) {
      BundleOfHis2_vx_u = BundleOfHis2_vx ;
      BundleOfHis2_vy_u = BundleOfHis2_vy ;
      BundleOfHis2_vz_u = BundleOfHis2_vz ;
      BundleOfHis2_g_u = ((((((((BundleOfHis2_v_i_0 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis2_v_i_1 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((BundleOfHis2_v_i_2 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      cstate =  BundleOfHis2_t1 ;
      force_init_update = False;
    }

    else if ( BundleOfHis2_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis2_vx_init = BundleOfHis2_vx ;
      slope_BundleOfHis2_vx = (BundleOfHis2_vx * -33.2) ;
      BundleOfHis2_vx_u = (slope_BundleOfHis2_vx * d) + BundleOfHis2_vx ;
      if ((pstate != cstate) || force_init_update) BundleOfHis2_vy_init = BundleOfHis2_vy ;
      slope_BundleOfHis2_vy = ((BundleOfHis2_vy * 11.0) * BundleOfHis2_ft) ;
      BundleOfHis2_vy_u = (slope_BundleOfHis2_vy * d) + BundleOfHis2_vy ;
      if ((pstate != cstate) || force_init_update) BundleOfHis2_vz_init = BundleOfHis2_vz ;
      slope_BundleOfHis2_vz = ((BundleOfHis2_vz * 2.0) * BundleOfHis2_ft) ;
      BundleOfHis2_vz_u = (slope_BundleOfHis2_vz * d) + BundleOfHis2_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  BundleOfHis2_t4 ;
      force_init_update = False;
      BundleOfHis2_g_u = ((((((((BundleOfHis2_v_i_0 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis2_v_i_1 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((BundleOfHis2_v_i_2 + (- ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      BundleOfHis2_v_u = ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz) ;
      BundleOfHis2_voo = ((BundleOfHis2_vx + (- BundleOfHis2_vy)) + BundleOfHis2_vz) ;
      BundleOfHis2_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis2!\n");
      exit(1);
    }
    break;
  }
  BundleOfHis2_vx = BundleOfHis2_vx_u;
  BundleOfHis2_vy = BundleOfHis2_vy_u;
  BundleOfHis2_vz = BundleOfHis2_vz_u;
  BundleOfHis2_g = BundleOfHis2_g_u;
  BundleOfHis2_v = BundleOfHis2_v_u;
  BundleOfHis2_ft = BundleOfHis2_ft_u;
  BundleOfHis2_theta = BundleOfHis2_theta_u;
  BundleOfHis2_v_O = BundleOfHis2_v_O_u;
  return cstate;
}