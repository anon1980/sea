#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double RA_RA1_update_c1vd();
extern double RA_RA1_update_c2vd();
extern double RA_RA1_update_c1md();
extern double RA_RA1_update_c2md();
extern double RA_RA1_update_buffer_index(double,double,double,double);
extern double RA_RA1_update_latch1(double,double);
extern double RA_RA1_update_latch2(double,double);
extern double RA_RA1_update_ocell1(double,double);
extern double RA_RA1_update_ocell2(double,double);
double RA_RA1_cell1_v;
double RA_RA1_cell1_mode;
double RA_RA1_cell2_v;
double RA_RA1_cell2_mode;
double RA_RA1_cell1_v_replay = 0.0;
double RA_RA1_cell2_v_replay = 0.0;


static double  RA_RA1_k  =  0.0 ,  RA_RA1_cell1_mode_delayed  =  0.0 ,  RA_RA1_cell2_mode_delayed  =  0.0 ,  RA_RA1_from_cell  =  0.0 ,  RA_RA1_cell1_replay_latch  =  0.0 ,  RA_RA1_cell2_replay_latch  =  0.0 ,  RA_RA1_cell1_v_delayed  =  0.0 ,  RA_RA1_cell2_v_delayed  =  0.0 ,  RA_RA1_wasted  =  0.0 ; //the continuous vars
static double  RA_RA1_k_u , RA_RA1_cell1_mode_delayed_u , RA_RA1_cell2_mode_delayed_u , RA_RA1_from_cell_u , RA_RA1_cell1_replay_latch_u , RA_RA1_cell2_replay_latch_u , RA_RA1_cell1_v_delayed_u , RA_RA1_cell2_v_delayed_u , RA_RA1_wasted_u ; // and their updates
static double  RA_RA1_k_init , RA_RA1_cell1_mode_delayed_init , RA_RA1_cell2_mode_delayed_init , RA_RA1_from_cell_init , RA_RA1_cell1_replay_latch_init , RA_RA1_cell2_replay_latch_init , RA_RA1_cell1_v_delayed_init , RA_RA1_cell2_v_delayed_init , RA_RA1_wasted_init ; // and their inits
static double  slope_RA_RA1_k , slope_RA_RA1_cell1_mode_delayed , slope_RA_RA1_cell2_mode_delayed , slope_RA_RA1_from_cell , slope_RA_RA1_cell1_replay_latch , slope_RA_RA1_cell2_replay_latch , slope_RA_RA1_cell1_v_delayed , slope_RA_RA1_cell2_v_delayed , slope_RA_RA1_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { RA_RA1_idle , RA_RA1_annhilate , RA_RA1_previous_drection1 , RA_RA1_previous_direction2 , RA_RA1_wait_cell1 , RA_RA1_replay_cell1 , RA_RA1_replay_cell2 , RA_RA1_wait_cell2 }; // state declarations

enum states RA_RA1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( RA_RA1_idle ):
    if (True == False) {;}
    else if  (RA_RA1_cell2_mode == (2.0) && (RA_RA1_cell1_mode != (2.0))) {
      RA_RA1_k_u = 1 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
      cstate =  RA_RA1_previous_direction2 ;
      force_init_update = False;
    }
    else if  (RA_RA1_cell1_mode == (2.0) && (RA_RA1_cell2_mode != (2.0))) {
      RA_RA1_k_u = 1 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
      cstate =  RA_RA1_previous_drection1 ;
      force_init_update = False;
    }
    else if  (RA_RA1_cell1_mode == (2.0) && (RA_RA1_cell2_mode == (2.0))) {
      RA_RA1_k_u = 1 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
      cstate =  RA_RA1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RA_RA1_k_init = RA_RA1_k ;
      slope_RA_RA1_k = 1 ;
      RA_RA1_k_u = (slope_RA_RA1_k * d) + RA_RA1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RA_RA1_idle ;
      force_init_update = False;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell1_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RA_RA1!\n");
      exit(1);
    }
    break;
  case ( RA_RA1_annhilate ):
    if (True == False) {;}
    else if  (RA_RA1_cell1_mode != (2.0) && (RA_RA1_cell2_mode != (2.0))) {
      RA_RA1_k_u = 1 ;
      RA_RA1_from_cell_u = 0 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
      cstate =  RA_RA1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RA_RA1_k_init = RA_RA1_k ;
      slope_RA_RA1_k = 1 ;
      RA_RA1_k_u = (slope_RA_RA1_k * d) + RA_RA1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RA_RA1_annhilate ;
      force_init_update = False;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell1_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RA_RA1!\n");
      exit(1);
    }
    break;
  case ( RA_RA1_previous_drection1 ):
    if (True == False) {;}
    else if  (RA_RA1_from_cell == (1.0)) {
      RA_RA1_k_u = 1 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
      cstate =  RA_RA1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RA_RA1_from_cell == (0.0)) {
      RA_RA1_k_u = 1 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
      cstate =  RA_RA1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RA_RA1_from_cell == (2.0) && (RA_RA1_cell2_mode_delayed == (0.0))) {
      RA_RA1_k_u = 1 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
      cstate =  RA_RA1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RA_RA1_from_cell == (2.0) && (RA_RA1_cell2_mode_delayed != (0.0))) {
      RA_RA1_k_u = 1 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
      cstate =  RA_RA1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RA_RA1_k_init = RA_RA1_k ;
      slope_RA_RA1_k = 1 ;
      RA_RA1_k_u = (slope_RA_RA1_k * d) + RA_RA1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RA_RA1_previous_drection1 ;
      force_init_update = False;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell1_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RA_RA1!\n");
      exit(1);
    }
    break;
  case ( RA_RA1_previous_direction2 ):
    if (True == False) {;}
    else if  (RA_RA1_from_cell == (1.0) && (RA_RA1_cell1_mode_delayed != (0.0))) {
      RA_RA1_k_u = 1 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
      cstate =  RA_RA1_annhilate ;
      force_init_update = False;
    }
    else if  (RA_RA1_from_cell == (2.0)) {
      RA_RA1_k_u = 1 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
      cstate =  RA_RA1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (RA_RA1_from_cell == (0.0)) {
      RA_RA1_k_u = 1 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
      cstate =  RA_RA1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (RA_RA1_from_cell == (1.0) && (RA_RA1_cell1_mode_delayed == (0.0))) {
      RA_RA1_k_u = 1 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
      cstate =  RA_RA1_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RA_RA1_k_init = RA_RA1_k ;
      slope_RA_RA1_k = 1 ;
      RA_RA1_k_u = (slope_RA_RA1_k * d) + RA_RA1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RA_RA1_previous_direction2 ;
      force_init_update = False;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell1_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RA_RA1!\n");
      exit(1);
    }
    break;
  case ( RA_RA1_wait_cell1 ):
    if (True == False) {;}
    else if  (RA_RA1_cell2_mode == (2.0)) {
      RA_RA1_k_u = 1 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
      cstate =  RA_RA1_annhilate ;
      force_init_update = False;
    }
    else if  (RA_RA1_k >= (20.0)) {
      RA_RA1_from_cell_u = 1 ;
      RA_RA1_cell1_replay_latch_u = 1 ;
      RA_RA1_k_u = 1 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
      cstate =  RA_RA1_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RA_RA1_k_init = RA_RA1_k ;
      slope_RA_RA1_k = 1 ;
      RA_RA1_k_u = (slope_RA_RA1_k * d) + RA_RA1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RA_RA1_wait_cell1 ;
      force_init_update = False;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell1_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RA_RA1!\n");
      exit(1);
    }
    break;
  case ( RA_RA1_replay_cell1 ):
    if (True == False) {;}
    else if  (RA_RA1_cell1_mode == (2.0)) {
      RA_RA1_k_u = 1 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
      cstate =  RA_RA1_annhilate ;
      force_init_update = False;
    }
    else if  (RA_RA1_k >= (20.0)) {
      RA_RA1_from_cell_u = 2 ;
      RA_RA1_cell2_replay_latch_u = 1 ;
      RA_RA1_k_u = 1 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
      cstate =  RA_RA1_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RA_RA1_k_init = RA_RA1_k ;
      slope_RA_RA1_k = 1 ;
      RA_RA1_k_u = (slope_RA_RA1_k * d) + RA_RA1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RA_RA1_replay_cell1 ;
      force_init_update = False;
      RA_RA1_cell1_replay_latch_u = 1 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell1_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RA_RA1!\n");
      exit(1);
    }
    break;
  case ( RA_RA1_replay_cell2 ):
    if (True == False) {;}
    else if  (RA_RA1_k >= (10.0)) {
      RA_RA1_k_u = 1 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
      cstate =  RA_RA1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RA_RA1_k_init = RA_RA1_k ;
      slope_RA_RA1_k = 1 ;
      RA_RA1_k_u = (slope_RA_RA1_k * d) + RA_RA1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RA_RA1_replay_cell2 ;
      force_init_update = False;
      RA_RA1_cell2_replay_latch_u = 1 ;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell1_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RA_RA1!\n");
      exit(1);
    }
    break;
  case ( RA_RA1_wait_cell2 ):
    if (True == False) {;}
    else if  (RA_RA1_k >= (10.0)) {
      RA_RA1_k_u = 1 ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
      cstate =  RA_RA1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RA_RA1_k_init = RA_RA1_k ;
      slope_RA_RA1_k = 1 ;
      RA_RA1_k_u = (slope_RA_RA1_k * d) + RA_RA1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RA_RA1_wait_cell2 ;
      force_init_update = False;
      RA_RA1_cell1_v_delayed_u = RA_RA1_update_c1vd () ;
      RA_RA1_cell2_v_delayed_u = RA_RA1_update_c2vd () ;
      RA_RA1_cell1_mode_delayed_u = RA_RA1_update_c1md () ;
      RA_RA1_cell2_mode_delayed_u = RA_RA1_update_c2md () ;
      RA_RA1_wasted_u = RA_RA1_update_buffer_index (RA_RA1_cell1_v,RA_RA1_cell2_v,RA_RA1_cell1_mode,RA_RA1_cell2_mode) ;
      RA_RA1_cell1_replay_latch_u = RA_RA1_update_latch1 (RA_RA1_cell1_mode_delayed,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_replay_latch_u = RA_RA1_update_latch2 (RA_RA1_cell2_mode_delayed,RA_RA1_cell2_replay_latch_u) ;
      RA_RA1_cell1_v_replay = RA_RA1_update_ocell1 (RA_RA1_cell1_v_delayed_u,RA_RA1_cell1_replay_latch_u) ;
      RA_RA1_cell2_v_replay = RA_RA1_update_ocell2 (RA_RA1_cell2_v_delayed_u,RA_RA1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RA_RA1!\n");
      exit(1);
    }
    break;
  }
  RA_RA1_k = RA_RA1_k_u;
  RA_RA1_cell1_mode_delayed = RA_RA1_cell1_mode_delayed_u;
  RA_RA1_cell2_mode_delayed = RA_RA1_cell2_mode_delayed_u;
  RA_RA1_from_cell = RA_RA1_from_cell_u;
  RA_RA1_cell1_replay_latch = RA_RA1_cell1_replay_latch_u;
  RA_RA1_cell2_replay_latch = RA_RA1_cell2_replay_latch_u;
  RA_RA1_cell1_v_delayed = RA_RA1_cell1_v_delayed_u;
  RA_RA1_cell2_v_delayed = RA_RA1_cell2_v_delayed_u;
  RA_RA1_wasted = RA_RA1_wasted_u;
  return cstate;
}