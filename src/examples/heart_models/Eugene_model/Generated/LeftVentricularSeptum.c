#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double LeftVentricularSeptum_v_i_0;
double LeftVentricularSeptum_v_i_1;
double LeftVentricularSeptum_v_i_2;
double LeftVentricularSeptum_voo = 0.0;
double LeftVentricularSeptum_state = 0.0;


static double  LeftVentricularSeptum_vx  =  0 ,  LeftVentricularSeptum_vy  =  0 ,  LeftVentricularSeptum_vz  =  0 ,  LeftVentricularSeptum_g  =  0 ,  LeftVentricularSeptum_v  =  0 ,  LeftVentricularSeptum_ft  =  0 ,  LeftVentricularSeptum_theta  =  0 ,  LeftVentricularSeptum_v_O  =  0 ; //the continuous vars
static double  LeftVentricularSeptum_vx_u , LeftVentricularSeptum_vy_u , LeftVentricularSeptum_vz_u , LeftVentricularSeptum_g_u , LeftVentricularSeptum_v_u , LeftVentricularSeptum_ft_u , LeftVentricularSeptum_theta_u , LeftVentricularSeptum_v_O_u ; // and their updates
static double  LeftVentricularSeptum_vx_init , LeftVentricularSeptum_vy_init , LeftVentricularSeptum_vz_init , LeftVentricularSeptum_g_init , LeftVentricularSeptum_v_init , LeftVentricularSeptum_ft_init , LeftVentricularSeptum_theta_init , LeftVentricularSeptum_v_O_init ; // and their inits
static double  slope_LeftVentricularSeptum_vx , slope_LeftVentricularSeptum_vy , slope_LeftVentricularSeptum_vz , slope_LeftVentricularSeptum_g , slope_LeftVentricularSeptum_v , slope_LeftVentricularSeptum_ft , slope_LeftVentricularSeptum_theta , slope_LeftVentricularSeptum_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { LeftVentricularSeptum_t1 , LeftVentricularSeptum_t2 , LeftVentricularSeptum_t3 , LeftVentricularSeptum_t4 }; // state declarations

enum states LeftVentricularSeptum (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( LeftVentricularSeptum_t1 ):
    if (True == False) {;}
    else if  (LeftVentricularSeptum_g > (44.5)) {
      LeftVentricularSeptum_vx_u = (0.3 * LeftVentricularSeptum_v) ;
      LeftVentricularSeptum_vy_u = 0 ;
      LeftVentricularSeptum_vz_u = (0.7 * LeftVentricularSeptum_v) ;
      LeftVentricularSeptum_g_u = ((((((((LeftVentricularSeptum_v_i_0 + (- ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularSeptum_v_i_1 + (- ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftVentricularSeptum_theta_u = (LeftVentricularSeptum_v / 30.0) ;
      LeftVentricularSeptum_v_O_u = (131.1 + (- (80.1 * pow ( ((LeftVentricularSeptum_v / 30.0)) , (0.5) )))) ;
      LeftVentricularSeptum_ft_u = f (LeftVentricularSeptum_theta,4.0e-2) ;
      cstate =  LeftVentricularSeptum_t2 ;
      force_init_update = False;
    }

    else if ( LeftVentricularSeptum_v <= (44.5)
               && 
              LeftVentricularSeptum_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum_vx_init = LeftVentricularSeptum_vx ;
      slope_LeftVentricularSeptum_vx = (LeftVentricularSeptum_vx * -8.7) ;
      LeftVentricularSeptum_vx_u = (slope_LeftVentricularSeptum_vx * d) + LeftVentricularSeptum_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum_vy_init = LeftVentricularSeptum_vy ;
      slope_LeftVentricularSeptum_vy = (LeftVentricularSeptum_vy * -190.9) ;
      LeftVentricularSeptum_vy_u = (slope_LeftVentricularSeptum_vy * d) + LeftVentricularSeptum_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum_vz_init = LeftVentricularSeptum_vz ;
      slope_LeftVentricularSeptum_vz = (LeftVentricularSeptum_vz * -190.4) ;
      LeftVentricularSeptum_vz_u = (slope_LeftVentricularSeptum_vz * d) + LeftVentricularSeptum_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricularSeptum_t1 ;
      force_init_update = False;
      LeftVentricularSeptum_g_u = ((((((((LeftVentricularSeptum_v_i_0 + (- ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularSeptum_v_i_1 + (- ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftVentricularSeptum_v_u = ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz) ;
      LeftVentricularSeptum_voo = ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz) ;
      LeftVentricularSeptum_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularSeptum!\n");
      exit(1);
    }
    break;
  case ( LeftVentricularSeptum_t2 ):
    if (True == False) {;}
    else if  (LeftVentricularSeptum_v >= (44.5)) {
      LeftVentricularSeptum_vx_u = LeftVentricularSeptum_vx ;
      LeftVentricularSeptum_vy_u = LeftVentricularSeptum_vy ;
      LeftVentricularSeptum_vz_u = LeftVentricularSeptum_vz ;
      LeftVentricularSeptum_g_u = ((((((((LeftVentricularSeptum_v_i_0 + (- ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularSeptum_v_i_1 + (- ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftVentricularSeptum_t3 ;
      force_init_update = False;
    }
    else if  (LeftVentricularSeptum_g <= (44.5)
               && 
              LeftVentricularSeptum_v < (44.5)) {
      LeftVentricularSeptum_vx_u = LeftVentricularSeptum_vx ;
      LeftVentricularSeptum_vy_u = LeftVentricularSeptum_vy ;
      LeftVentricularSeptum_vz_u = LeftVentricularSeptum_vz ;
      LeftVentricularSeptum_g_u = ((((((((LeftVentricularSeptum_v_i_0 + (- ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularSeptum_v_i_1 + (- ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftVentricularSeptum_t1 ;
      force_init_update = False;
    }

    else if ( LeftVentricularSeptum_v < (44.5)
               && 
              LeftVentricularSeptum_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum_vx_init = LeftVentricularSeptum_vx ;
      slope_LeftVentricularSeptum_vx = ((LeftVentricularSeptum_vx * -23.6) + (777200.0 * LeftVentricularSeptum_g)) ;
      LeftVentricularSeptum_vx_u = (slope_LeftVentricularSeptum_vx * d) + LeftVentricularSeptum_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum_vy_init = LeftVentricularSeptum_vy ;
      slope_LeftVentricularSeptum_vy = ((LeftVentricularSeptum_vy * -45.5) + (58900.0 * LeftVentricularSeptum_g)) ;
      LeftVentricularSeptum_vy_u = (slope_LeftVentricularSeptum_vy * d) + LeftVentricularSeptum_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum_vz_init = LeftVentricularSeptum_vz ;
      slope_LeftVentricularSeptum_vz = ((LeftVentricularSeptum_vz * -12.9) + (276600.0 * LeftVentricularSeptum_g)) ;
      LeftVentricularSeptum_vz_u = (slope_LeftVentricularSeptum_vz * d) + LeftVentricularSeptum_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricularSeptum_t2 ;
      force_init_update = False;
      LeftVentricularSeptum_g_u = ((((((((LeftVentricularSeptum_v_i_0 + (- ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularSeptum_v_i_1 + (- ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftVentricularSeptum_v_u = ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz) ;
      LeftVentricularSeptum_voo = ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz) ;
      LeftVentricularSeptum_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularSeptum!\n");
      exit(1);
    }
    break;
  case ( LeftVentricularSeptum_t3 ):
    if (True == False) {;}
    else if  (LeftVentricularSeptum_v >= (131.1)) {
      LeftVentricularSeptum_vx_u = LeftVentricularSeptum_vx ;
      LeftVentricularSeptum_vy_u = LeftVentricularSeptum_vy ;
      LeftVentricularSeptum_vz_u = LeftVentricularSeptum_vz ;
      LeftVentricularSeptum_g_u = ((((((((LeftVentricularSeptum_v_i_0 + (- ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularSeptum_v_i_1 + (- ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftVentricularSeptum_t4 ;
      force_init_update = False;
    }

    else if ( LeftVentricularSeptum_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum_vx_init = LeftVentricularSeptum_vx ;
      slope_LeftVentricularSeptum_vx = (LeftVentricularSeptum_vx * -6.9) ;
      LeftVentricularSeptum_vx_u = (slope_LeftVentricularSeptum_vx * d) + LeftVentricularSeptum_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum_vy_init = LeftVentricularSeptum_vy ;
      slope_LeftVentricularSeptum_vy = (LeftVentricularSeptum_vy * 75.9) ;
      LeftVentricularSeptum_vy_u = (slope_LeftVentricularSeptum_vy * d) + LeftVentricularSeptum_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum_vz_init = LeftVentricularSeptum_vz ;
      slope_LeftVentricularSeptum_vz = (LeftVentricularSeptum_vz * 6826.5) ;
      LeftVentricularSeptum_vz_u = (slope_LeftVentricularSeptum_vz * d) + LeftVentricularSeptum_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricularSeptum_t3 ;
      force_init_update = False;
      LeftVentricularSeptum_g_u = ((((((((LeftVentricularSeptum_v_i_0 + (- ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularSeptum_v_i_1 + (- ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftVentricularSeptum_v_u = ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz) ;
      LeftVentricularSeptum_voo = ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz) ;
      LeftVentricularSeptum_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularSeptum!\n");
      exit(1);
    }
    break;
  case ( LeftVentricularSeptum_t4 ):
    if (True == False) {;}
    else if  (LeftVentricularSeptum_v <= (30.0)) {
      LeftVentricularSeptum_vx_u = LeftVentricularSeptum_vx ;
      LeftVentricularSeptum_vy_u = LeftVentricularSeptum_vy ;
      LeftVentricularSeptum_vz_u = LeftVentricularSeptum_vz ;
      LeftVentricularSeptum_g_u = ((((((((LeftVentricularSeptum_v_i_0 + (- ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularSeptum_v_i_1 + (- ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftVentricularSeptum_t1 ;
      force_init_update = False;
    }

    else if ( LeftVentricularSeptum_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum_vx_init = LeftVentricularSeptum_vx ;
      slope_LeftVentricularSeptum_vx = (LeftVentricularSeptum_vx * -33.2) ;
      LeftVentricularSeptum_vx_u = (slope_LeftVentricularSeptum_vx * d) + LeftVentricularSeptum_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum_vy_init = LeftVentricularSeptum_vy ;
      slope_LeftVentricularSeptum_vy = ((LeftVentricularSeptum_vy * 11.0) * LeftVentricularSeptum_ft) ;
      LeftVentricularSeptum_vy_u = (slope_LeftVentricularSeptum_vy * d) + LeftVentricularSeptum_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum_vz_init = LeftVentricularSeptum_vz ;
      slope_LeftVentricularSeptum_vz = ((LeftVentricularSeptum_vz * 2.0) * LeftVentricularSeptum_ft) ;
      LeftVentricularSeptum_vz_u = (slope_LeftVentricularSeptum_vz * d) + LeftVentricularSeptum_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricularSeptum_t4 ;
      force_init_update = False;
      LeftVentricularSeptum_g_u = ((((((((LeftVentricularSeptum_v_i_0 + (- ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularSeptum_v_i_1 + (- ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftVentricularSeptum_v_u = ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz) ;
      LeftVentricularSeptum_voo = ((LeftVentricularSeptum_vx + (- LeftVentricularSeptum_vy)) + LeftVentricularSeptum_vz) ;
      LeftVentricularSeptum_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularSeptum!\n");
      exit(1);
    }
    break;
  }
  LeftVentricularSeptum_vx = LeftVentricularSeptum_vx_u;
  LeftVentricularSeptum_vy = LeftVentricularSeptum_vy_u;
  LeftVentricularSeptum_vz = LeftVentricularSeptum_vz_u;
  LeftVentricularSeptum_g = LeftVentricularSeptum_g_u;
  LeftVentricularSeptum_v = LeftVentricularSeptum_v_u;
  LeftVentricularSeptum_ft = LeftVentricularSeptum_ft_u;
  LeftVentricularSeptum_theta = LeftVentricularSeptum_theta_u;
  LeftVentricularSeptum_v_O = LeftVentricularSeptum_v_O_u;
  return cstate;
}