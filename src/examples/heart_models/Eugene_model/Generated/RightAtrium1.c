#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double RightAtrium1_v_i_0;
double RightAtrium1_v_i_1;
double RightAtrium1_v_i_2;
double RightAtrium1_voo = 0.0;
double RightAtrium1_state = 0.0;


static double  RightAtrium1_vx  =  0 ,  RightAtrium1_vy  =  0 ,  RightAtrium1_vz  =  0 ,  RightAtrium1_g  =  0 ,  RightAtrium1_v  =  0 ,  RightAtrium1_ft  =  0 ,  RightAtrium1_theta  =  0 ,  RightAtrium1_v_O  =  0 ; //the continuous vars
static double  RightAtrium1_vx_u , RightAtrium1_vy_u , RightAtrium1_vz_u , RightAtrium1_g_u , RightAtrium1_v_u , RightAtrium1_ft_u , RightAtrium1_theta_u , RightAtrium1_v_O_u ; // and their updates
static double  RightAtrium1_vx_init , RightAtrium1_vy_init , RightAtrium1_vz_init , RightAtrium1_g_init , RightAtrium1_v_init , RightAtrium1_ft_init , RightAtrium1_theta_init , RightAtrium1_v_O_init ; // and their inits
static double  slope_RightAtrium1_vx , slope_RightAtrium1_vy , slope_RightAtrium1_vz , slope_RightAtrium1_g , slope_RightAtrium1_v , slope_RightAtrium1_ft , slope_RightAtrium1_theta , slope_RightAtrium1_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { RightAtrium1_t1 , RightAtrium1_t2 , RightAtrium1_t3 , RightAtrium1_t4 }; // state declarations

enum states RightAtrium1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( RightAtrium1_t1 ):
    if (True == False) {;}
    else if  (RightAtrium1_g > (44.5)) {
      RightAtrium1_vx_u = (0.3 * RightAtrium1_v) ;
      RightAtrium1_vy_u = 0 ;
      RightAtrium1_vz_u = (0.7 * RightAtrium1_v) ;
      RightAtrium1_g_u = ((((((((RightAtrium1_v_i_0 + (- ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightAtrium1_v_i_1 + (- ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightAtrium1_theta_u = (RightAtrium1_v / 30.0) ;
      RightAtrium1_v_O_u = (131.1 + (- (80.1 * pow ( ((RightAtrium1_v / 30.0)) , (0.5) )))) ;
      RightAtrium1_ft_u = f (RightAtrium1_theta,4.0e-2) ;
      cstate =  RightAtrium1_t2 ;
      force_init_update = False;
    }

    else if ( RightAtrium1_v <= (44.5)
               && 
              RightAtrium1_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) RightAtrium1_vx_init = RightAtrium1_vx ;
      slope_RightAtrium1_vx = (RightAtrium1_vx * -8.7) ;
      RightAtrium1_vx_u = (slope_RightAtrium1_vx * d) + RightAtrium1_vx ;
      if ((pstate != cstate) || force_init_update) RightAtrium1_vy_init = RightAtrium1_vy ;
      slope_RightAtrium1_vy = (RightAtrium1_vy * -190.9) ;
      RightAtrium1_vy_u = (slope_RightAtrium1_vy * d) + RightAtrium1_vy ;
      if ((pstate != cstate) || force_init_update) RightAtrium1_vz_init = RightAtrium1_vz ;
      slope_RightAtrium1_vz = (RightAtrium1_vz * -190.4) ;
      RightAtrium1_vz_u = (slope_RightAtrium1_vz * d) + RightAtrium1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightAtrium1_t1 ;
      force_init_update = False;
      RightAtrium1_g_u = ((((((((RightAtrium1_v_i_0 + (- ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightAtrium1_v_i_1 + (- ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightAtrium1_v_u = ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz) ;
      RightAtrium1_voo = ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz) ;
      RightAtrium1_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightAtrium1!\n");
      exit(1);
    }
    break;
  case ( RightAtrium1_t2 ):
    if (True == False) {;}
    else if  (RightAtrium1_v >= (44.5)) {
      RightAtrium1_vx_u = RightAtrium1_vx ;
      RightAtrium1_vy_u = RightAtrium1_vy ;
      RightAtrium1_vz_u = RightAtrium1_vz ;
      RightAtrium1_g_u = ((((((((RightAtrium1_v_i_0 + (- ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightAtrium1_v_i_1 + (- ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  RightAtrium1_t3 ;
      force_init_update = False;
    }
    else if  (RightAtrium1_g <= (44.5)
               && RightAtrium1_v < (44.5)) {
      RightAtrium1_vx_u = RightAtrium1_vx ;
      RightAtrium1_vy_u = RightAtrium1_vy ;
      RightAtrium1_vz_u = RightAtrium1_vz ;
      RightAtrium1_g_u = ((((((((RightAtrium1_v_i_0 + (- ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightAtrium1_v_i_1 + (- ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  RightAtrium1_t1 ;
      force_init_update = False;
    }

    else if ( RightAtrium1_v < (44.5)
               && 
              RightAtrium1_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) RightAtrium1_vx_init = RightAtrium1_vx ;
      slope_RightAtrium1_vx = ((RightAtrium1_vx * -23.6) + (777200.0 * RightAtrium1_g)) ;
      RightAtrium1_vx_u = (slope_RightAtrium1_vx * d) + RightAtrium1_vx ;
      if ((pstate != cstate) || force_init_update) RightAtrium1_vy_init = RightAtrium1_vy ;
      slope_RightAtrium1_vy = ((RightAtrium1_vy * -45.5) + (58900.0 * RightAtrium1_g)) ;
      RightAtrium1_vy_u = (slope_RightAtrium1_vy * d) + RightAtrium1_vy ;
      if ((pstate != cstate) || force_init_update) RightAtrium1_vz_init = RightAtrium1_vz ;
      slope_RightAtrium1_vz = ((RightAtrium1_vz * -12.9) + (276600.0 * RightAtrium1_g)) ;
      RightAtrium1_vz_u = (slope_RightAtrium1_vz * d) + RightAtrium1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightAtrium1_t2 ;
      force_init_update = False;
      RightAtrium1_g_u = ((((((((RightAtrium1_v_i_0 + (- ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightAtrium1_v_i_1 + (- ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightAtrium1_v_u = ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz) ;
      RightAtrium1_voo = ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz) ;
      RightAtrium1_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightAtrium1!\n");
      exit(1);
    }
    break;
  case ( RightAtrium1_t3 ):
    if (True == False) {;}
    else if  (RightAtrium1_v >= (131.1)) {
      RightAtrium1_vx_u = RightAtrium1_vx ;
      RightAtrium1_vy_u = RightAtrium1_vy ;
      RightAtrium1_vz_u = RightAtrium1_vz ;
      RightAtrium1_g_u = ((((((((RightAtrium1_v_i_0 + (- ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightAtrium1_v_i_1 + (- ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  RightAtrium1_t4 ;
      force_init_update = False;
    }

    else if ( RightAtrium1_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) RightAtrium1_vx_init = RightAtrium1_vx ;
      slope_RightAtrium1_vx = (RightAtrium1_vx * -6.9) ;
      RightAtrium1_vx_u = (slope_RightAtrium1_vx * d) + RightAtrium1_vx ;
      if ((pstate != cstate) || force_init_update) RightAtrium1_vy_init = RightAtrium1_vy ;
      slope_RightAtrium1_vy = (RightAtrium1_vy * 75.9) ;
      RightAtrium1_vy_u = (slope_RightAtrium1_vy * d) + RightAtrium1_vy ;
      if ((pstate != cstate) || force_init_update) RightAtrium1_vz_init = RightAtrium1_vz ;
      slope_RightAtrium1_vz = (RightAtrium1_vz * 6826.5) ;
      RightAtrium1_vz_u = (slope_RightAtrium1_vz * d) + RightAtrium1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightAtrium1_t3 ;
      force_init_update = False;
      RightAtrium1_g_u = ((((((((RightAtrium1_v_i_0 + (- ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightAtrium1_v_i_1 + (- ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightAtrium1_v_u = ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz) ;
      RightAtrium1_voo = ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz) ;
      RightAtrium1_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightAtrium1!\n");
      exit(1);
    }
    break;
  case ( RightAtrium1_t4 ):
    if (True == False) {;}
    else if  (RightAtrium1_v <= (30.0)) {
      RightAtrium1_vx_u = RightAtrium1_vx ;
      RightAtrium1_vy_u = RightAtrium1_vy ;
      RightAtrium1_vz_u = RightAtrium1_vz ;
      RightAtrium1_g_u = ((((((((RightAtrium1_v_i_0 + (- ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightAtrium1_v_i_1 + (- ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  RightAtrium1_t1 ;
      force_init_update = False;
    }

    else if ( RightAtrium1_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) RightAtrium1_vx_init = RightAtrium1_vx ;
      slope_RightAtrium1_vx = (RightAtrium1_vx * -33.2) ;
      RightAtrium1_vx_u = (slope_RightAtrium1_vx * d) + RightAtrium1_vx ;
      if ((pstate != cstate) || force_init_update) RightAtrium1_vy_init = RightAtrium1_vy ;
      slope_RightAtrium1_vy = ((RightAtrium1_vy * 20.0) * RightAtrium1_ft) ;
      RightAtrium1_vy_u = (slope_RightAtrium1_vy * d) + RightAtrium1_vy ;
      if ((pstate != cstate) || force_init_update) RightAtrium1_vz_init = RightAtrium1_vz ;
      slope_RightAtrium1_vz = ((RightAtrium1_vz * 2.0) * RightAtrium1_ft) ;
      RightAtrium1_vz_u = (slope_RightAtrium1_vz * d) + RightAtrium1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightAtrium1_t4 ;
      force_init_update = False;
      RightAtrium1_g_u = ((((((((RightAtrium1_v_i_0 + (- ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightAtrium1_v_i_1 + (- ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightAtrium1_v_u = ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz) ;
      RightAtrium1_voo = ((RightAtrium1_vx + (- RightAtrium1_vy)) + RightAtrium1_vz) ;
      RightAtrium1_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightAtrium1!\n");
      exit(1);
    }
    break;
  }
  RightAtrium1_vx = RightAtrium1_vx_u;
  RightAtrium1_vy = RightAtrium1_vy_u;
  RightAtrium1_vz = RightAtrium1_vz_u;
  RightAtrium1_g = RightAtrium1_g_u;
  RightAtrium1_v = RightAtrium1_v_u;
  RightAtrium1_ft = RightAtrium1_ft_u;
  RightAtrium1_theta = RightAtrium1_theta_u;
  RightAtrium1_v_O = RightAtrium1_v_O_u;
  return cstate;
}