#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double CristaTerminalis1_v_i_0;
double CristaTerminalis1_v_i_1;
double CristaTerminalis1_voo = 0.0;
double CristaTerminalis1_state = 0.0;


static double  CristaTerminalis1_vx  =  0 ,  CristaTerminalis1_vy  =  0 ,  CristaTerminalis1_vz  =  0 ,  CristaTerminalis1_g  =  0 ,  CristaTerminalis1_v  =  0 ,  CristaTerminalis1_ft  =  0 ,  CristaTerminalis1_theta  =  0 ,  CristaTerminalis1_v_O  =  0 ; //the continuous vars
static double  CristaTerminalis1_vx_u , CristaTerminalis1_vy_u , CristaTerminalis1_vz_u , CristaTerminalis1_g_u , CristaTerminalis1_v_u , CristaTerminalis1_ft_u , CristaTerminalis1_theta_u , CristaTerminalis1_v_O_u ; // and their updates
static double  CristaTerminalis1_vx_init , CristaTerminalis1_vy_init , CristaTerminalis1_vz_init , CristaTerminalis1_g_init , CristaTerminalis1_v_init , CristaTerminalis1_ft_init , CristaTerminalis1_theta_init , CristaTerminalis1_v_O_init ; // and their inits
static double  slope_CristaTerminalis1_vx , slope_CristaTerminalis1_vy , slope_CristaTerminalis1_vz , slope_CristaTerminalis1_g , slope_CristaTerminalis1_v , slope_CristaTerminalis1_ft , slope_CristaTerminalis1_theta , slope_CristaTerminalis1_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { CristaTerminalis1_t1 , CristaTerminalis1_t2 , CristaTerminalis1_t3 , CristaTerminalis1_t4 }; // state declarations

enum states CristaTerminalis1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( CristaTerminalis1_t1 ):
    if (True == False) {;}
    else if  (CristaTerminalis1_g > (44.5)) {
      CristaTerminalis1_vx_u = (0.3 * CristaTerminalis1_v) ;
      CristaTerminalis1_vy_u = 0 ;
      CristaTerminalis1_vz_u = (0.7 * CristaTerminalis1_v) ;
      CristaTerminalis1_g_u = ((((((((CristaTerminalis1_v_i_0 + (- ((CristaTerminalis1_vx + (- CristaTerminalis1_vy)) + CristaTerminalis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      CristaTerminalis1_theta_u = (CristaTerminalis1_v / 30.0) ;
      CristaTerminalis1_v_O_u = (131.1 + (- (80.1 * pow ( ((CristaTerminalis1_v / 30.0)) , (0.5) )))) ;
      CristaTerminalis1_ft_u = f (CristaTerminalis1_theta,4.0e-2) ;
      cstate =  CristaTerminalis1_t2 ;
      force_init_update = False;
    }

    else if ( CristaTerminalis1_v <= (44.5)
               && 
              CristaTerminalis1_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) CristaTerminalis1_vx_init = CristaTerminalis1_vx ;
      slope_CristaTerminalis1_vx = (CristaTerminalis1_vx * -8.7) ;
      CristaTerminalis1_vx_u = (slope_CristaTerminalis1_vx * d) + CristaTerminalis1_vx ;
      if ((pstate != cstate) || force_init_update) CristaTerminalis1_vy_init = CristaTerminalis1_vy ;
      slope_CristaTerminalis1_vy = (CristaTerminalis1_vy * -190.9) ;
      CristaTerminalis1_vy_u = (slope_CristaTerminalis1_vy * d) + CristaTerminalis1_vy ;
      if ((pstate != cstate) || force_init_update) CristaTerminalis1_vz_init = CristaTerminalis1_vz ;
      slope_CristaTerminalis1_vz = (CristaTerminalis1_vz * -190.4) ;
      CristaTerminalis1_vz_u = (slope_CristaTerminalis1_vz * d) + CristaTerminalis1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  CristaTerminalis1_t1 ;
      force_init_update = False;
      CristaTerminalis1_g_u = ((((((((CristaTerminalis1_v_i_0 + (- ((CristaTerminalis1_vx + (- CristaTerminalis1_vy)) + CristaTerminalis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      CristaTerminalis1_v_u = ((CristaTerminalis1_vx + (- CristaTerminalis1_vy)) + CristaTerminalis1_vz) ;
      CristaTerminalis1_voo = ((CristaTerminalis1_vx + (- CristaTerminalis1_vy)) + CristaTerminalis1_vz) ;
      CristaTerminalis1_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CristaTerminalis1!\n");
      exit(1);
    }
    break;
  case ( CristaTerminalis1_t2 ):
    if (True == False) {;}
    else if  (CristaTerminalis1_v >= (44.5)) {
      CristaTerminalis1_vx_u = CristaTerminalis1_vx ;
      CristaTerminalis1_vy_u = CristaTerminalis1_vy ;
      CristaTerminalis1_vz_u = CristaTerminalis1_vz ;
      CristaTerminalis1_g_u = ((((((((CristaTerminalis1_v_i_0 + (- ((CristaTerminalis1_vx + (- CristaTerminalis1_vy)) + CristaTerminalis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  CristaTerminalis1_t3 ;
      force_init_update = False;
    }
    else if  (CristaTerminalis1_g <= (44.5)
               && 
              CristaTerminalis1_v < (44.5)) {
      CristaTerminalis1_vx_u = CristaTerminalis1_vx ;
      CristaTerminalis1_vy_u = CristaTerminalis1_vy ;
      CristaTerminalis1_vz_u = CristaTerminalis1_vz ;
      CristaTerminalis1_g_u = ((((((((CristaTerminalis1_v_i_0 + (- ((CristaTerminalis1_vx + (- CristaTerminalis1_vy)) + CristaTerminalis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  CristaTerminalis1_t1 ;
      force_init_update = False;
    }

    else if ( CristaTerminalis1_v < (44.5)
               && 
              CristaTerminalis1_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) CristaTerminalis1_vx_init = CristaTerminalis1_vx ;
      slope_CristaTerminalis1_vx = ((CristaTerminalis1_vx * -23.6) + (777200.0 * CristaTerminalis1_g)) ;
      CristaTerminalis1_vx_u = (slope_CristaTerminalis1_vx * d) + CristaTerminalis1_vx ;
      if ((pstate != cstate) || force_init_update) CristaTerminalis1_vy_init = CristaTerminalis1_vy ;
      slope_CristaTerminalis1_vy = ((CristaTerminalis1_vy * -45.5) + (58900.0 * CristaTerminalis1_g)) ;
      CristaTerminalis1_vy_u = (slope_CristaTerminalis1_vy * d) + CristaTerminalis1_vy ;
      if ((pstate != cstate) || force_init_update) CristaTerminalis1_vz_init = CristaTerminalis1_vz ;
      slope_CristaTerminalis1_vz = ((CristaTerminalis1_vz * -12.9) + (276600.0 * CristaTerminalis1_g)) ;
      CristaTerminalis1_vz_u = (slope_CristaTerminalis1_vz * d) + CristaTerminalis1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  CristaTerminalis1_t2 ;
      force_init_update = False;
      CristaTerminalis1_g_u = ((((((((CristaTerminalis1_v_i_0 + (- ((CristaTerminalis1_vx + (- CristaTerminalis1_vy)) + CristaTerminalis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      CristaTerminalis1_v_u = ((CristaTerminalis1_vx + (- CristaTerminalis1_vy)) + CristaTerminalis1_vz) ;
      CristaTerminalis1_voo = ((CristaTerminalis1_vx + (- CristaTerminalis1_vy)) + CristaTerminalis1_vz) ;
      CristaTerminalis1_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CristaTerminalis1!\n");
      exit(1);
    }
    break;
  case ( CristaTerminalis1_t3 ):
    if (True == False) {;}
    else if  (CristaTerminalis1_v >= (131.1)) {
      CristaTerminalis1_vx_u = CristaTerminalis1_vx ;
      CristaTerminalis1_vy_u = CristaTerminalis1_vy ;
      CristaTerminalis1_vz_u = CristaTerminalis1_vz ;
      CristaTerminalis1_g_u = ((((((((CristaTerminalis1_v_i_0 + (- ((CristaTerminalis1_vx + (- CristaTerminalis1_vy)) + CristaTerminalis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  CristaTerminalis1_t4 ;
      force_init_update = False;
    }

    else if ( CristaTerminalis1_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) CristaTerminalis1_vx_init = CristaTerminalis1_vx ;
      slope_CristaTerminalis1_vx = (CristaTerminalis1_vx * -6.9) ;
      CristaTerminalis1_vx_u = (slope_CristaTerminalis1_vx * d) + CristaTerminalis1_vx ;
      if ((pstate != cstate) || force_init_update) CristaTerminalis1_vy_init = CristaTerminalis1_vy ;
      slope_CristaTerminalis1_vy = (CristaTerminalis1_vy * 75.9) ;
      CristaTerminalis1_vy_u = (slope_CristaTerminalis1_vy * d) + CristaTerminalis1_vy ;
      if ((pstate != cstate) || force_init_update) CristaTerminalis1_vz_init = CristaTerminalis1_vz ;
      slope_CristaTerminalis1_vz = (CristaTerminalis1_vz * 6826.5) ;
      CristaTerminalis1_vz_u = (slope_CristaTerminalis1_vz * d) + CristaTerminalis1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  CristaTerminalis1_t3 ;
      force_init_update = False;
      CristaTerminalis1_g_u = ((((((((CristaTerminalis1_v_i_0 + (- ((CristaTerminalis1_vx + (- CristaTerminalis1_vy)) + CristaTerminalis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      CristaTerminalis1_v_u = ((CristaTerminalis1_vx + (- CristaTerminalis1_vy)) + CristaTerminalis1_vz) ;
      CristaTerminalis1_voo = ((CristaTerminalis1_vx + (- CristaTerminalis1_vy)) + CristaTerminalis1_vz) ;
      CristaTerminalis1_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CristaTerminalis1!\n");
      exit(1);
    }
    break;
  case ( CristaTerminalis1_t4 ):
    if (True == False) {;}
    else if  (CristaTerminalis1_v <= (30.0)) {
      CristaTerminalis1_vx_u = CristaTerminalis1_vx ;
      CristaTerminalis1_vy_u = CristaTerminalis1_vy ;
      CristaTerminalis1_vz_u = CristaTerminalis1_vz ;
      CristaTerminalis1_g_u = ((((((((CristaTerminalis1_v_i_0 + (- ((CristaTerminalis1_vx + (- CristaTerminalis1_vy)) + CristaTerminalis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  CristaTerminalis1_t1 ;
      force_init_update = False;
    }

    else if ( CristaTerminalis1_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) CristaTerminalis1_vx_init = CristaTerminalis1_vx ;
      slope_CristaTerminalis1_vx = (CristaTerminalis1_vx * -33.2) ;
      CristaTerminalis1_vx_u = (slope_CristaTerminalis1_vx * d) + CristaTerminalis1_vx ;
      if ((pstate != cstate) || force_init_update) CristaTerminalis1_vy_init = CristaTerminalis1_vy ;
      slope_CristaTerminalis1_vy = ((CristaTerminalis1_vy * 20.0) * CristaTerminalis1_ft) ;
      CristaTerminalis1_vy_u = (slope_CristaTerminalis1_vy * d) + CristaTerminalis1_vy ;
      if ((pstate != cstate) || force_init_update) CristaTerminalis1_vz_init = CristaTerminalis1_vz ;
      slope_CristaTerminalis1_vz = ((CristaTerminalis1_vz * 2.0) * CristaTerminalis1_ft) ;
      CristaTerminalis1_vz_u = (slope_CristaTerminalis1_vz * d) + CristaTerminalis1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  CristaTerminalis1_t4 ;
      force_init_update = False;
      CristaTerminalis1_g_u = ((((((((CristaTerminalis1_v_i_0 + (- ((CristaTerminalis1_vx + (- CristaTerminalis1_vy)) + CristaTerminalis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      CristaTerminalis1_v_u = ((CristaTerminalis1_vx + (- CristaTerminalis1_vy)) + CristaTerminalis1_vz) ;
      CristaTerminalis1_voo = ((CristaTerminalis1_vx + (- CristaTerminalis1_vy)) + CristaTerminalis1_vz) ;
      CristaTerminalis1_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CristaTerminalis1!\n");
      exit(1);
    }
    break;
  }
  CristaTerminalis1_vx = CristaTerminalis1_vx_u;
  CristaTerminalis1_vy = CristaTerminalis1_vy_u;
  CristaTerminalis1_vz = CristaTerminalis1_vz_u;
  CristaTerminalis1_g = CristaTerminalis1_g_u;
  CristaTerminalis1_v = CristaTerminalis1_v_u;
  CristaTerminalis1_ft = CristaTerminalis1_ft_u;
  CristaTerminalis1_theta = CristaTerminalis1_theta_u;
  CristaTerminalis1_v_O = CristaTerminalis1_v_O_u;
  return cstate;
}