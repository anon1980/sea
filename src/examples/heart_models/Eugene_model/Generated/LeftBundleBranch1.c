#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double LeftBundleBranch1_v_i_0;
double LeftBundleBranch1_v_i_1;
double LeftBundleBranch1_v_i_2;
double LeftBundleBranch1_voo = 0.0;
double LeftBundleBranch1_state = 0.0;


static double  LeftBundleBranch1_vx  =  0 ,  LeftBundleBranch1_vy  =  0 ,  LeftBundleBranch1_vz  =  0 ,  LeftBundleBranch1_g  =  0 ,  LeftBundleBranch1_v  =  0 ,  LeftBundleBranch1_ft  =  0 ,  LeftBundleBranch1_theta  =  0 ,  LeftBundleBranch1_v_O  =  0 ; //the continuous vars
static double  LeftBundleBranch1_vx_u , LeftBundleBranch1_vy_u , LeftBundleBranch1_vz_u , LeftBundleBranch1_g_u , LeftBundleBranch1_v_u , LeftBundleBranch1_ft_u , LeftBundleBranch1_theta_u , LeftBundleBranch1_v_O_u ; // and their updates
static double  LeftBundleBranch1_vx_init , LeftBundleBranch1_vy_init , LeftBundleBranch1_vz_init , LeftBundleBranch1_g_init , LeftBundleBranch1_v_init , LeftBundleBranch1_ft_init , LeftBundleBranch1_theta_init , LeftBundleBranch1_v_O_init ; // and their inits
static double  slope_LeftBundleBranch1_vx , slope_LeftBundleBranch1_vy , slope_LeftBundleBranch1_vz , slope_LeftBundleBranch1_g , slope_LeftBundleBranch1_v , slope_LeftBundleBranch1_ft , slope_LeftBundleBranch1_theta , slope_LeftBundleBranch1_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { LeftBundleBranch1_t1 , LeftBundleBranch1_t2 , LeftBundleBranch1_t3 , LeftBundleBranch1_t4 }; // state declarations

enum states LeftBundleBranch1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( LeftBundleBranch1_t1 ):
    if (True == False) {;}
    else if  (LeftBundleBranch1_g > (44.5)) {
      LeftBundleBranch1_vx_u = (0.3 * LeftBundleBranch1_v) ;
      LeftBundleBranch1_vy_u = 0 ;
      LeftBundleBranch1_vz_u = (0.7 * LeftBundleBranch1_v) ;
      LeftBundleBranch1_g_u = ((((((((LeftBundleBranch1_v_i_0 + (- ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftBundleBranch1_v_i_1 + (- ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftBundleBranch1_theta_u = (LeftBundleBranch1_v / 30.0) ;
      LeftBundleBranch1_v_O_u = (131.1 + (- (80.1 * pow ( ((LeftBundleBranch1_v / 30.0)) , (0.5) )))) ;
      LeftBundleBranch1_ft_u = f (LeftBundleBranch1_theta,4.0e-2) ;
      cstate =  LeftBundleBranch1_t2 ;
      force_init_update = False;
    }

    else if ( LeftBundleBranch1_v <= (44.5)
               && 
              LeftBundleBranch1_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) LeftBundleBranch1_vx_init = LeftBundleBranch1_vx ;
      slope_LeftBundleBranch1_vx = (LeftBundleBranch1_vx * -8.7) ;
      LeftBundleBranch1_vx_u = (slope_LeftBundleBranch1_vx * d) + LeftBundleBranch1_vx ;
      if ((pstate != cstate) || force_init_update) LeftBundleBranch1_vy_init = LeftBundleBranch1_vy ;
      slope_LeftBundleBranch1_vy = (LeftBundleBranch1_vy * -190.9) ;
      LeftBundleBranch1_vy_u = (slope_LeftBundleBranch1_vy * d) + LeftBundleBranch1_vy ;
      if ((pstate != cstate) || force_init_update) LeftBundleBranch1_vz_init = LeftBundleBranch1_vz ;
      slope_LeftBundleBranch1_vz = (LeftBundleBranch1_vz * -190.4) ;
      LeftBundleBranch1_vz_u = (slope_LeftBundleBranch1_vz * d) + LeftBundleBranch1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftBundleBranch1_t1 ;
      force_init_update = False;
      LeftBundleBranch1_g_u = ((((((((LeftBundleBranch1_v_i_0 + (- ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftBundleBranch1_v_i_1 + (- ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftBundleBranch1_v_u = ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz) ;
      LeftBundleBranch1_voo = ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz) ;
      LeftBundleBranch1_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftBundleBranch1!\n");
      exit(1);
    }
    break;
  case ( LeftBundleBranch1_t2 ):
    if (True == False) {;}
    else if  (LeftBundleBranch1_v >= (44.5)) {
      LeftBundleBranch1_vx_u = LeftBundleBranch1_vx ;
      LeftBundleBranch1_vy_u = LeftBundleBranch1_vy ;
      LeftBundleBranch1_vz_u = LeftBundleBranch1_vz ;
      LeftBundleBranch1_g_u = ((((((((LeftBundleBranch1_v_i_0 + (- ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftBundleBranch1_v_i_1 + (- ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftBundleBranch1_t3 ;
      force_init_update = False;
    }
    else if  (LeftBundleBranch1_g <= (44.5)
               && 
              LeftBundleBranch1_v < (44.5)) {
      LeftBundleBranch1_vx_u = LeftBundleBranch1_vx ;
      LeftBundleBranch1_vy_u = LeftBundleBranch1_vy ;
      LeftBundleBranch1_vz_u = LeftBundleBranch1_vz ;
      LeftBundleBranch1_g_u = ((((((((LeftBundleBranch1_v_i_0 + (- ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftBundleBranch1_v_i_1 + (- ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftBundleBranch1_t1 ;
      force_init_update = False;
    }

    else if ( LeftBundleBranch1_v < (44.5)
               && 
              LeftBundleBranch1_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftBundleBranch1_vx_init = LeftBundleBranch1_vx ;
      slope_LeftBundleBranch1_vx = ((LeftBundleBranch1_vx * -23.6) + (777200.0 * LeftBundleBranch1_g)) ;
      LeftBundleBranch1_vx_u = (slope_LeftBundleBranch1_vx * d) + LeftBundleBranch1_vx ;
      if ((pstate != cstate) || force_init_update) LeftBundleBranch1_vy_init = LeftBundleBranch1_vy ;
      slope_LeftBundleBranch1_vy = ((LeftBundleBranch1_vy * -45.5) + (58900.0 * LeftBundleBranch1_g)) ;
      LeftBundleBranch1_vy_u = (slope_LeftBundleBranch1_vy * d) + LeftBundleBranch1_vy ;
      if ((pstate != cstate) || force_init_update) LeftBundleBranch1_vz_init = LeftBundleBranch1_vz ;
      slope_LeftBundleBranch1_vz = ((LeftBundleBranch1_vz * -12.9) + (276600.0 * LeftBundleBranch1_g)) ;
      LeftBundleBranch1_vz_u = (slope_LeftBundleBranch1_vz * d) + LeftBundleBranch1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftBundleBranch1_t2 ;
      force_init_update = False;
      LeftBundleBranch1_g_u = ((((((((LeftBundleBranch1_v_i_0 + (- ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftBundleBranch1_v_i_1 + (- ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftBundleBranch1_v_u = ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz) ;
      LeftBundleBranch1_voo = ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz) ;
      LeftBundleBranch1_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftBundleBranch1!\n");
      exit(1);
    }
    break;
  case ( LeftBundleBranch1_t3 ):
    if (True == False) {;}
    else if  (LeftBundleBranch1_v >= (131.1)) {
      LeftBundleBranch1_vx_u = LeftBundleBranch1_vx ;
      LeftBundleBranch1_vy_u = LeftBundleBranch1_vy ;
      LeftBundleBranch1_vz_u = LeftBundleBranch1_vz ;
      LeftBundleBranch1_g_u = ((((((((LeftBundleBranch1_v_i_0 + (- ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftBundleBranch1_v_i_1 + (- ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftBundleBranch1_t4 ;
      force_init_update = False;
    }

    else if ( LeftBundleBranch1_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) LeftBundleBranch1_vx_init = LeftBundleBranch1_vx ;
      slope_LeftBundleBranch1_vx = (LeftBundleBranch1_vx * -6.9) ;
      LeftBundleBranch1_vx_u = (slope_LeftBundleBranch1_vx * d) + LeftBundleBranch1_vx ;
      if ((pstate != cstate) || force_init_update) LeftBundleBranch1_vy_init = LeftBundleBranch1_vy ;
      slope_LeftBundleBranch1_vy = (LeftBundleBranch1_vy * 75.9) ;
      LeftBundleBranch1_vy_u = (slope_LeftBundleBranch1_vy * d) + LeftBundleBranch1_vy ;
      if ((pstate != cstate) || force_init_update) LeftBundleBranch1_vz_init = LeftBundleBranch1_vz ;
      slope_LeftBundleBranch1_vz = (LeftBundleBranch1_vz * 6826.5) ;
      LeftBundleBranch1_vz_u = (slope_LeftBundleBranch1_vz * d) + LeftBundleBranch1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftBundleBranch1_t3 ;
      force_init_update = False;
      LeftBundleBranch1_g_u = ((((((((LeftBundleBranch1_v_i_0 + (- ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftBundleBranch1_v_i_1 + (- ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftBundleBranch1_v_u = ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz) ;
      LeftBundleBranch1_voo = ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz) ;
      LeftBundleBranch1_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftBundleBranch1!\n");
      exit(1);
    }
    break;
  case ( LeftBundleBranch1_t4 ):
    if (True == False) {;}
    else if  (LeftBundleBranch1_v <= (30.0)) {
      LeftBundleBranch1_vx_u = LeftBundleBranch1_vx ;
      LeftBundleBranch1_vy_u = LeftBundleBranch1_vy ;
      LeftBundleBranch1_vz_u = LeftBundleBranch1_vz ;
      LeftBundleBranch1_g_u = ((((((((LeftBundleBranch1_v_i_0 + (- ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftBundleBranch1_v_i_1 + (- ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftBundleBranch1_t1 ;
      force_init_update = False;
    }

    else if ( LeftBundleBranch1_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftBundleBranch1_vx_init = LeftBundleBranch1_vx ;
      slope_LeftBundleBranch1_vx = (LeftBundleBranch1_vx * -33.2) ;
      LeftBundleBranch1_vx_u = (slope_LeftBundleBranch1_vx * d) + LeftBundleBranch1_vx ;
      if ((pstate != cstate) || force_init_update) LeftBundleBranch1_vy_init = LeftBundleBranch1_vy ;
      slope_LeftBundleBranch1_vy = ((LeftBundleBranch1_vy * 11.0) * LeftBundleBranch1_ft) ;
      LeftBundleBranch1_vy_u = (slope_LeftBundleBranch1_vy * d) + LeftBundleBranch1_vy ;
      if ((pstate != cstate) || force_init_update) LeftBundleBranch1_vz_init = LeftBundleBranch1_vz ;
      slope_LeftBundleBranch1_vz = ((LeftBundleBranch1_vz * 2.0) * LeftBundleBranch1_ft) ;
      LeftBundleBranch1_vz_u = (slope_LeftBundleBranch1_vz * d) + LeftBundleBranch1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftBundleBranch1_t4 ;
      force_init_update = False;
      LeftBundleBranch1_g_u = ((((((((LeftBundleBranch1_v_i_0 + (- ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftBundleBranch1_v_i_1 + (- ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftBundleBranch1_v_u = ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz) ;
      LeftBundleBranch1_voo = ((LeftBundleBranch1_vx + (- LeftBundleBranch1_vy)) + LeftBundleBranch1_vz) ;
      LeftBundleBranch1_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftBundleBranch1!\n");
      exit(1);
    }
    break;
  }
  LeftBundleBranch1_vx = LeftBundleBranch1_vx_u;
  LeftBundleBranch1_vy = LeftBundleBranch1_vy_u;
  LeftBundleBranch1_vz = LeftBundleBranch1_vz_u;
  LeftBundleBranch1_g = LeftBundleBranch1_g_u;
  LeftBundleBranch1_v = LeftBundleBranch1_v_u;
  LeftBundleBranch1_ft = LeftBundleBranch1_ft_u;
  LeftBundleBranch1_theta = LeftBundleBranch1_theta_u;
  LeftBundleBranch1_v_O = LeftBundleBranch1_v_O_u;
  return cstate;
}