#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double Fast_v_i_0;
double Fast_v_i_1;
double Fast_v_i_2;
double Fast_voo = 0.0;
double Fast_state = 0.0;


static double  Fast_vx  =  0 ,  Fast_vy  =  0 ,  Fast_vz  =  0 ,  Fast_g  =  0 ,  Fast_v  =  0 ,  Fast_ft  =  0 ,  Fast_theta  =  0 ,  Fast_v_O  =  0 ; //the continuous vars
static double  Fast_vx_u , Fast_vy_u , Fast_vz_u , Fast_g_u , Fast_v_u , Fast_ft_u , Fast_theta_u , Fast_v_O_u ; // and their updates
static double  Fast_vx_init , Fast_vy_init , Fast_vz_init , Fast_g_init , Fast_v_init , Fast_ft_init , Fast_theta_init , Fast_v_O_init ; // and their inits
static double  slope_Fast_vx , slope_Fast_vy , slope_Fast_vz , slope_Fast_g , slope_Fast_v , slope_Fast_ft , slope_Fast_theta , slope_Fast_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { Fast_t1 , Fast_t2 , Fast_t3 , Fast_t4 }; // state declarations

enum states Fast (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( Fast_t1 ):
    if (True == False) {;}
    else if  (Fast_g > (44.5)) {
      Fast_vx_u = (0.3 * Fast_v) ;
      Fast_vy_u = 0 ;
      Fast_vz_u = (0.7 * Fast_v) ;
      Fast_g_u = ((((((((Fast_v_i_0 + (- ((Fast_vx + (- Fast_vy)) + Fast_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Fast_v_i_1 + (- ((Fast_vx + (- Fast_vy)) + Fast_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Fast_theta_u = (Fast_v / 30.0) ;
      Fast_v_O_u = (131.1 + (- (80.1 * pow ( ((Fast_v / 30.0)) , (0.5) )))) ;
      Fast_ft_u = f (Fast_theta,4.0e-2) ;
      cstate =  Fast_t2 ;
      force_init_update = False;
    }

    else if ( Fast_v <= (44.5)
               && Fast_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) Fast_vx_init = Fast_vx ;
      slope_Fast_vx = (Fast_vx * -8.7) ;
      Fast_vx_u = (slope_Fast_vx * d) + Fast_vx ;
      if ((pstate != cstate) || force_init_update) Fast_vy_init = Fast_vy ;
      slope_Fast_vy = (Fast_vy * -190.9) ;
      Fast_vy_u = (slope_Fast_vy * d) + Fast_vy ;
      if ((pstate != cstate) || force_init_update) Fast_vz_init = Fast_vz ;
      slope_Fast_vz = (Fast_vz * -190.4) ;
      Fast_vz_u = (slope_Fast_vz * d) + Fast_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Fast_t1 ;
      force_init_update = False;
      Fast_g_u = ((((((((Fast_v_i_0 + (- ((Fast_vx + (- Fast_vy)) + Fast_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Fast_v_i_1 + (- ((Fast_vx + (- Fast_vy)) + Fast_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Fast_v_u = ((Fast_vx + (- Fast_vy)) + Fast_vz) ;
      Fast_voo = ((Fast_vx + (- Fast_vy)) + Fast_vz) ;
      Fast_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast!\n");
      exit(1);
    }
    break;
  case ( Fast_t2 ):
    if (True == False) {;}
    else if  (Fast_v >= (44.5)) {
      Fast_vx_u = Fast_vx ;
      Fast_vy_u = Fast_vy ;
      Fast_vz_u = Fast_vz ;
      Fast_g_u = ((((((((Fast_v_i_0 + (- ((Fast_vx + (- Fast_vy)) + Fast_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Fast_v_i_1 + (- ((Fast_vx + (- Fast_vy)) + Fast_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  Fast_t3 ;
      force_init_update = False;
    }
    else if  (Fast_g <= (44.5)
               && Fast_v < (44.5)) {
      Fast_vx_u = Fast_vx ;
      Fast_vy_u = Fast_vy ;
      Fast_vz_u = Fast_vz ;
      Fast_g_u = ((((((((Fast_v_i_0 + (- ((Fast_vx + (- Fast_vy)) + Fast_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Fast_v_i_1 + (- ((Fast_vx + (- Fast_vy)) + Fast_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  Fast_t1 ;
      force_init_update = False;
    }

    else if ( Fast_v < (44.5)
               && Fast_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) Fast_vx_init = Fast_vx ;
      slope_Fast_vx = ((Fast_vx * -23.6) + (777200.0 * Fast_g)) ;
      Fast_vx_u = (slope_Fast_vx * d) + Fast_vx ;
      if ((pstate != cstate) || force_init_update) Fast_vy_init = Fast_vy ;
      slope_Fast_vy = ((Fast_vy * -45.5) + (58900.0 * Fast_g)) ;
      Fast_vy_u = (slope_Fast_vy * d) + Fast_vy ;
      if ((pstate != cstate) || force_init_update) Fast_vz_init = Fast_vz ;
      slope_Fast_vz = ((Fast_vz * -12.9) + (276600.0 * Fast_g)) ;
      Fast_vz_u = (slope_Fast_vz * d) + Fast_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Fast_t2 ;
      force_init_update = False;
      Fast_g_u = ((((((((Fast_v_i_0 + (- ((Fast_vx + (- Fast_vy)) + Fast_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Fast_v_i_1 + (- ((Fast_vx + (- Fast_vy)) + Fast_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Fast_v_u = ((Fast_vx + (- Fast_vy)) + Fast_vz) ;
      Fast_voo = ((Fast_vx + (- Fast_vy)) + Fast_vz) ;
      Fast_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast!\n");
      exit(1);
    }
    break;
  case ( Fast_t3 ):
    if (True == False) {;}
    else if  (Fast_v >= (131.1)) {
      Fast_vx_u = Fast_vx ;
      Fast_vy_u = Fast_vy ;
      Fast_vz_u = Fast_vz ;
      Fast_g_u = ((((((((Fast_v_i_0 + (- ((Fast_vx + (- Fast_vy)) + Fast_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Fast_v_i_1 + (- ((Fast_vx + (- Fast_vy)) + Fast_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  Fast_t4 ;
      force_init_update = False;
    }

    else if ( Fast_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) Fast_vx_init = Fast_vx ;
      slope_Fast_vx = (Fast_vx * -6.9) ;
      Fast_vx_u = (slope_Fast_vx * d) + Fast_vx ;
      if ((pstate != cstate) || force_init_update) Fast_vy_init = Fast_vy ;
      slope_Fast_vy = (Fast_vy * 75.9) ;
      Fast_vy_u = (slope_Fast_vy * d) + Fast_vy ;
      if ((pstate != cstate) || force_init_update) Fast_vz_init = Fast_vz ;
      slope_Fast_vz = (Fast_vz * 6826.5) ;
      Fast_vz_u = (slope_Fast_vz * d) + Fast_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Fast_t3 ;
      force_init_update = False;
      Fast_g_u = ((((((((Fast_v_i_0 + (- ((Fast_vx + (- Fast_vy)) + Fast_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Fast_v_i_1 + (- ((Fast_vx + (- Fast_vy)) + Fast_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Fast_v_u = ((Fast_vx + (- Fast_vy)) + Fast_vz) ;
      Fast_voo = ((Fast_vx + (- Fast_vy)) + Fast_vz) ;
      Fast_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast!\n");
      exit(1);
    }
    break;
  case ( Fast_t4 ):
    if (True == False) {;}
    else if  (Fast_v <= (30.0)) {
      Fast_vx_u = Fast_vx ;
      Fast_vy_u = Fast_vy ;
      Fast_vz_u = Fast_vz ;
      Fast_g_u = ((((((((Fast_v_i_0 + (- ((Fast_vx + (- Fast_vy)) + Fast_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Fast_v_i_1 + (- ((Fast_vx + (- Fast_vy)) + Fast_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  Fast_t1 ;
      force_init_update = False;
    }

    else if ( Fast_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) Fast_vx_init = Fast_vx ;
      slope_Fast_vx = (Fast_vx * -33.2) ;
      Fast_vx_u = (slope_Fast_vx * d) + Fast_vx ;
      if ((pstate != cstate) || force_init_update) Fast_vy_init = Fast_vy ;
      slope_Fast_vy = ((Fast_vy * 18.0) * Fast_ft) ;
      Fast_vy_u = (slope_Fast_vy * d) + Fast_vy ;
      if ((pstate != cstate) || force_init_update) Fast_vz_init = Fast_vz ;
      slope_Fast_vz = ((Fast_vz * 2.0) * Fast_ft) ;
      Fast_vz_u = (slope_Fast_vz * d) + Fast_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Fast_t4 ;
      force_init_update = False;
      Fast_g_u = ((((((((Fast_v_i_0 + (- ((Fast_vx + (- Fast_vy)) + Fast_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Fast_v_i_1 + (- ((Fast_vx + (- Fast_vy)) + Fast_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Fast_v_u = ((Fast_vx + (- Fast_vy)) + Fast_vz) ;
      Fast_voo = ((Fast_vx + (- Fast_vy)) + Fast_vz) ;
      Fast_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast!\n");
      exit(1);
    }
    break;
  }
  Fast_vx = Fast_vx_u;
  Fast_vy = Fast_vy_u;
  Fast_vz = Fast_vz_u;
  Fast_g = Fast_g_u;
  Fast_v = Fast_v_u;
  Fast_ft = Fast_ft_u;
  Fast_theta = Fast_theta_u;
  Fast_v_O = Fast_v_O_u;
  return cstate;
}