#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double LeftVentricle1_v_i_0;
double LeftVentricle1_v_i_1;
double LeftVentricle1_voo = 0.0;
double LeftVentricle1_state = 0.0;


static double  LeftVentricle1_vx  =  0 ,  LeftVentricle1_vy  =  0 ,  LeftVentricle1_vz  =  0 ,  LeftVentricle1_g  =  0 ,  LeftVentricle1_v  =  0 ,  LeftVentricle1_ft  =  0 ,  LeftVentricle1_theta  =  0 ,  LeftVentricle1_v_O  =  0 ; //the continuous vars
static double  LeftVentricle1_vx_u , LeftVentricle1_vy_u , LeftVentricle1_vz_u , LeftVentricle1_g_u , LeftVentricle1_v_u , LeftVentricle1_ft_u , LeftVentricle1_theta_u , LeftVentricle1_v_O_u ; // and their updates
static double  LeftVentricle1_vx_init , LeftVentricle1_vy_init , LeftVentricle1_vz_init , LeftVentricle1_g_init , LeftVentricle1_v_init , LeftVentricle1_ft_init , LeftVentricle1_theta_init , LeftVentricle1_v_O_init ; // and their inits
static double  slope_LeftVentricle1_vx , slope_LeftVentricle1_vy , slope_LeftVentricle1_vz , slope_LeftVentricle1_g , slope_LeftVentricle1_v , slope_LeftVentricle1_ft , slope_LeftVentricle1_theta , slope_LeftVentricle1_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { LeftVentricle1_t1 , LeftVentricle1_t2 , LeftVentricle1_t3 , LeftVentricle1_t4 }; // state declarations

enum states LeftVentricle1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( LeftVentricle1_t1 ):
    if (True == False) {;}
    else if  (LeftVentricle1_g > (44.5)) {
      LeftVentricle1_vx_u = (0.3 * LeftVentricle1_v) ;
      LeftVentricle1_vy_u = 0 ;
      LeftVentricle1_vz_u = (0.7 * LeftVentricle1_v) ;
      LeftVentricle1_g_u = ((((((((LeftVentricle1_v_i_0 + (- ((LeftVentricle1_vx + (- LeftVentricle1_vy)) + LeftVentricle1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      LeftVentricle1_theta_u = (LeftVentricle1_v / 30.0) ;
      LeftVentricle1_v_O_u = (131.1 + (- (80.1 * pow ( ((LeftVentricle1_v / 30.0)) , (0.5) )))) ;
      LeftVentricle1_ft_u = f (LeftVentricle1_theta,4.0e-2) ;
      cstate =  LeftVentricle1_t2 ;
      force_init_update = False;
    }

    else if ( LeftVentricle1_v <= (44.5)
               && 
              LeftVentricle1_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricle1_vx_init = LeftVentricle1_vx ;
      slope_LeftVentricle1_vx = (LeftVentricle1_vx * -8.7) ;
      LeftVentricle1_vx_u = (slope_LeftVentricle1_vx * d) + LeftVentricle1_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricle1_vy_init = LeftVentricle1_vy ;
      slope_LeftVentricle1_vy = (LeftVentricle1_vy * -190.9) ;
      LeftVentricle1_vy_u = (slope_LeftVentricle1_vy * d) + LeftVentricle1_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricle1_vz_init = LeftVentricle1_vz ;
      slope_LeftVentricle1_vz = (LeftVentricle1_vz * -190.4) ;
      LeftVentricle1_vz_u = (slope_LeftVentricle1_vz * d) + LeftVentricle1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricle1_t1 ;
      force_init_update = False;
      LeftVentricle1_g_u = ((((((((LeftVentricle1_v_i_0 + (- ((LeftVentricle1_vx + (- LeftVentricle1_vy)) + LeftVentricle1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      LeftVentricle1_v_u = ((LeftVentricle1_vx + (- LeftVentricle1_vy)) + LeftVentricle1_vz) ;
      LeftVentricle1_voo = ((LeftVentricle1_vx + (- LeftVentricle1_vy)) + LeftVentricle1_vz) ;
      LeftVentricle1_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricle1!\n");
      exit(1);
    }
    break;
  case ( LeftVentricle1_t2 ):
    if (True == False) {;}
    else if  (LeftVentricle1_v >= (44.5)) {
      LeftVentricle1_vx_u = LeftVentricle1_vx ;
      LeftVentricle1_vy_u = LeftVentricle1_vy ;
      LeftVentricle1_vz_u = LeftVentricle1_vz ;
      LeftVentricle1_g_u = ((((((((LeftVentricle1_v_i_0 + (- ((LeftVentricle1_vx + (- LeftVentricle1_vy)) + LeftVentricle1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  LeftVentricle1_t3 ;
      force_init_update = False;
    }
    else if  (LeftVentricle1_g <= (44.5)
               && LeftVentricle1_v < (44.5)) {
      LeftVentricle1_vx_u = LeftVentricle1_vx ;
      LeftVentricle1_vy_u = LeftVentricle1_vy ;
      LeftVentricle1_vz_u = LeftVentricle1_vz ;
      LeftVentricle1_g_u = ((((((((LeftVentricle1_v_i_0 + (- ((LeftVentricle1_vx + (- LeftVentricle1_vy)) + LeftVentricle1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  LeftVentricle1_t1 ;
      force_init_update = False;
    }

    else if ( LeftVentricle1_v < (44.5)
               && 
              LeftVentricle1_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricle1_vx_init = LeftVentricle1_vx ;
      slope_LeftVentricle1_vx = ((LeftVentricle1_vx * -23.6) + (777200.0 * LeftVentricle1_g)) ;
      LeftVentricle1_vx_u = (slope_LeftVentricle1_vx * d) + LeftVentricle1_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricle1_vy_init = LeftVentricle1_vy ;
      slope_LeftVentricle1_vy = ((LeftVentricle1_vy * -45.5) + (58900.0 * LeftVentricle1_g)) ;
      LeftVentricle1_vy_u = (slope_LeftVentricle1_vy * d) + LeftVentricle1_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricle1_vz_init = LeftVentricle1_vz ;
      slope_LeftVentricle1_vz = ((LeftVentricle1_vz * -12.9) + (276600.0 * LeftVentricle1_g)) ;
      LeftVentricle1_vz_u = (slope_LeftVentricle1_vz * d) + LeftVentricle1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricle1_t2 ;
      force_init_update = False;
      LeftVentricle1_g_u = ((((((((LeftVentricle1_v_i_0 + (- ((LeftVentricle1_vx + (- LeftVentricle1_vy)) + LeftVentricle1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      LeftVentricle1_v_u = ((LeftVentricle1_vx + (- LeftVentricle1_vy)) + LeftVentricle1_vz) ;
      LeftVentricle1_voo = ((LeftVentricle1_vx + (- LeftVentricle1_vy)) + LeftVentricle1_vz) ;
      LeftVentricle1_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricle1!\n");
      exit(1);
    }
    break;
  case ( LeftVentricle1_t3 ):
    if (True == False) {;}
    else if  (LeftVentricle1_v >= (131.1)) {
      LeftVentricle1_vx_u = LeftVentricle1_vx ;
      LeftVentricle1_vy_u = LeftVentricle1_vy ;
      LeftVentricle1_vz_u = LeftVentricle1_vz ;
      LeftVentricle1_g_u = ((((((((LeftVentricle1_v_i_0 + (- ((LeftVentricle1_vx + (- LeftVentricle1_vy)) + LeftVentricle1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  LeftVentricle1_t4 ;
      force_init_update = False;
    }

    else if ( LeftVentricle1_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricle1_vx_init = LeftVentricle1_vx ;
      slope_LeftVentricle1_vx = (LeftVentricle1_vx * -6.9) ;
      LeftVentricle1_vx_u = (slope_LeftVentricle1_vx * d) + LeftVentricle1_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricle1_vy_init = LeftVentricle1_vy ;
      slope_LeftVentricle1_vy = (LeftVentricle1_vy * 75.9) ;
      LeftVentricle1_vy_u = (slope_LeftVentricle1_vy * d) + LeftVentricle1_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricle1_vz_init = LeftVentricle1_vz ;
      slope_LeftVentricle1_vz = (LeftVentricle1_vz * 6826.5) ;
      LeftVentricle1_vz_u = (slope_LeftVentricle1_vz * d) + LeftVentricle1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricle1_t3 ;
      force_init_update = False;
      LeftVentricle1_g_u = ((((((((LeftVentricle1_v_i_0 + (- ((LeftVentricle1_vx + (- LeftVentricle1_vy)) + LeftVentricle1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      LeftVentricle1_v_u = ((LeftVentricle1_vx + (- LeftVentricle1_vy)) + LeftVentricle1_vz) ;
      LeftVentricle1_voo = ((LeftVentricle1_vx + (- LeftVentricle1_vy)) + LeftVentricle1_vz) ;
      LeftVentricle1_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricle1!\n");
      exit(1);
    }
    break;
  case ( LeftVentricle1_t4 ):
    if (True == False) {;}
    else if  (LeftVentricle1_v <= (30.0)) {
      LeftVentricle1_vx_u = LeftVentricle1_vx ;
      LeftVentricle1_vy_u = LeftVentricle1_vy ;
      LeftVentricle1_vz_u = LeftVentricle1_vz ;
      LeftVentricle1_g_u = ((((((((LeftVentricle1_v_i_0 + (- ((LeftVentricle1_vx + (- LeftVentricle1_vy)) + LeftVentricle1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  LeftVentricle1_t1 ;
      force_init_update = False;
    }

    else if ( LeftVentricle1_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricle1_vx_init = LeftVentricle1_vx ;
      slope_LeftVentricle1_vx = (LeftVentricle1_vx * -33.2) ;
      LeftVentricle1_vx_u = (slope_LeftVentricle1_vx * d) + LeftVentricle1_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricle1_vy_init = LeftVentricle1_vy ;
      slope_LeftVentricle1_vy = ((LeftVentricle1_vy * 11.0) * LeftVentricle1_ft) ;
      LeftVentricle1_vy_u = (slope_LeftVentricle1_vy * d) + LeftVentricle1_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricle1_vz_init = LeftVentricle1_vz ;
      slope_LeftVentricle1_vz = ((LeftVentricle1_vz * 2.0) * LeftVentricle1_ft) ;
      LeftVentricle1_vz_u = (slope_LeftVentricle1_vz * d) + LeftVentricle1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricle1_t4 ;
      force_init_update = False;
      LeftVentricle1_g_u = ((((((((LeftVentricle1_v_i_0 + (- ((LeftVentricle1_vx + (- LeftVentricle1_vy)) + LeftVentricle1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      LeftVentricle1_v_u = ((LeftVentricle1_vx + (- LeftVentricle1_vy)) + LeftVentricle1_vz) ;
      LeftVentricle1_voo = ((LeftVentricle1_vx + (- LeftVentricle1_vy)) + LeftVentricle1_vz) ;
      LeftVentricle1_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricle1!\n");
      exit(1);
    }
    break;
  }
  LeftVentricle1_vx = LeftVentricle1_vx_u;
  LeftVentricle1_vy = LeftVentricle1_vy_u;
  LeftVentricle1_vz = LeftVentricle1_vz_u;
  LeftVentricle1_g = LeftVentricle1_g_u;
  LeftVentricle1_v = LeftVentricle1_v_u;
  LeftVentricle1_ft = LeftVentricle1_ft_u;
  LeftVentricle1_theta = LeftVentricle1_theta_u;
  LeftVentricle1_v_O = LeftVentricle1_v_O_u;
  return cstate;
}