#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double LVS1_CSLV_update_c1vd();
extern double LVS1_CSLV_update_c2vd();
extern double LVS1_CSLV_update_c1md();
extern double LVS1_CSLV_update_c2md();
extern double LVS1_CSLV_update_buffer_index(double,double,double,double);
extern double LVS1_CSLV_update_latch1(double,double);
extern double LVS1_CSLV_update_latch2(double,double);
extern double LVS1_CSLV_update_ocell1(double,double);
extern double LVS1_CSLV_update_ocell2(double,double);
double LVS1_CSLV_cell1_v;
double LVS1_CSLV_cell1_mode;
double LVS1_CSLV_cell2_v;
double LVS1_CSLV_cell2_mode;
double LVS1_CSLV_cell1_v_replay = 0.0;
double LVS1_CSLV_cell2_v_replay = 0.0;


static double  LVS1_CSLV_k  =  0.0 ,  LVS1_CSLV_cell1_mode_delayed  =  0.0 ,  LVS1_CSLV_cell2_mode_delayed  =  0.0 ,  LVS1_CSLV_from_cell  =  0.0 ,  LVS1_CSLV_cell1_replay_latch  =  0.0 ,  LVS1_CSLV_cell2_replay_latch  =  0.0 ,  LVS1_CSLV_cell1_v_delayed  =  0.0 ,  LVS1_CSLV_cell2_v_delayed  =  0.0 ,  LVS1_CSLV_wasted  =  0.0 ; //the continuous vars
static double  LVS1_CSLV_k_u , LVS1_CSLV_cell1_mode_delayed_u , LVS1_CSLV_cell2_mode_delayed_u , LVS1_CSLV_from_cell_u , LVS1_CSLV_cell1_replay_latch_u , LVS1_CSLV_cell2_replay_latch_u , LVS1_CSLV_cell1_v_delayed_u , LVS1_CSLV_cell2_v_delayed_u , LVS1_CSLV_wasted_u ; // and their updates
static double  LVS1_CSLV_k_init , LVS1_CSLV_cell1_mode_delayed_init , LVS1_CSLV_cell2_mode_delayed_init , LVS1_CSLV_from_cell_init , LVS1_CSLV_cell1_replay_latch_init , LVS1_CSLV_cell2_replay_latch_init , LVS1_CSLV_cell1_v_delayed_init , LVS1_CSLV_cell2_v_delayed_init , LVS1_CSLV_wasted_init ; // and their inits
static double  slope_LVS1_CSLV_k , slope_LVS1_CSLV_cell1_mode_delayed , slope_LVS1_CSLV_cell2_mode_delayed , slope_LVS1_CSLV_from_cell , slope_LVS1_CSLV_cell1_replay_latch , slope_LVS1_CSLV_cell2_replay_latch , slope_LVS1_CSLV_cell1_v_delayed , slope_LVS1_CSLV_cell2_v_delayed , slope_LVS1_CSLV_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { LVS1_CSLV_idle , LVS1_CSLV_annhilate , LVS1_CSLV_previous_drection1 , LVS1_CSLV_previous_direction2 , LVS1_CSLV_wait_cell1 , LVS1_CSLV_replay_cell1 , LVS1_CSLV_replay_cell2 , LVS1_CSLV_wait_cell2 }; // state declarations

enum states LVS1_CSLV (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( LVS1_CSLV_idle ):
    if (True == False) {;}
    else if  (LVS1_CSLV_cell2_mode == (2.0) && (LVS1_CSLV_cell1_mode != (2.0))) {
      LVS1_CSLV_k_u = 1 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
      cstate =  LVS1_CSLV_previous_direction2 ;
      force_init_update = False;
    }
    else if  (LVS1_CSLV_cell1_mode == (2.0) && (LVS1_CSLV_cell2_mode != (2.0))) {
      LVS1_CSLV_k_u = 1 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
      cstate =  LVS1_CSLV_previous_drection1 ;
      force_init_update = False;
    }
    else if  (LVS1_CSLV_cell1_mode == (2.0) && (LVS1_CSLV_cell2_mode == (2.0))) {
      LVS1_CSLV_k_u = 1 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
      cstate =  LVS1_CSLV_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVS1_CSLV_k_init = LVS1_CSLV_k ;
      slope_LVS1_CSLV_k = 1 ;
      LVS1_CSLV_k_u = (slope_LVS1_CSLV_k * d) + LVS1_CSLV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVS1_CSLV_idle ;
      force_init_update = False;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell1_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVS1_CSLV!\n");
      exit(1);
    }
    break;
  case ( LVS1_CSLV_annhilate ):
    if (True == False) {;}
    else if  (LVS1_CSLV_cell1_mode != (2.0) && (LVS1_CSLV_cell2_mode != (2.0))) {
      LVS1_CSLV_k_u = 1 ;
      LVS1_CSLV_from_cell_u = 0 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
      cstate =  LVS1_CSLV_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVS1_CSLV_k_init = LVS1_CSLV_k ;
      slope_LVS1_CSLV_k = 1 ;
      LVS1_CSLV_k_u = (slope_LVS1_CSLV_k * d) + LVS1_CSLV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVS1_CSLV_annhilate ;
      force_init_update = False;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell1_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVS1_CSLV!\n");
      exit(1);
    }
    break;
  case ( LVS1_CSLV_previous_drection1 ):
    if (True == False) {;}
    else if  (LVS1_CSLV_from_cell == (1.0)) {
      LVS1_CSLV_k_u = 1 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
      cstate =  LVS1_CSLV_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LVS1_CSLV_from_cell == (0.0)) {
      LVS1_CSLV_k_u = 1 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
      cstate =  LVS1_CSLV_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LVS1_CSLV_from_cell == (2.0) && (LVS1_CSLV_cell2_mode_delayed == (0.0))) {
      LVS1_CSLV_k_u = 1 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
      cstate =  LVS1_CSLV_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LVS1_CSLV_from_cell == (2.0) && (LVS1_CSLV_cell2_mode_delayed != (0.0))) {
      LVS1_CSLV_k_u = 1 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
      cstate =  LVS1_CSLV_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVS1_CSLV_k_init = LVS1_CSLV_k ;
      slope_LVS1_CSLV_k = 1 ;
      LVS1_CSLV_k_u = (slope_LVS1_CSLV_k * d) + LVS1_CSLV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVS1_CSLV_previous_drection1 ;
      force_init_update = False;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell1_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVS1_CSLV!\n");
      exit(1);
    }
    break;
  case ( LVS1_CSLV_previous_direction2 ):
    if (True == False) {;}
    else if  (LVS1_CSLV_from_cell == (1.0) && (LVS1_CSLV_cell1_mode_delayed != (0.0))) {
      LVS1_CSLV_k_u = 1 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
      cstate =  LVS1_CSLV_annhilate ;
      force_init_update = False;
    }
    else if  (LVS1_CSLV_from_cell == (2.0)) {
      LVS1_CSLV_k_u = 1 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
      cstate =  LVS1_CSLV_replay_cell1 ;
      force_init_update = False;
    }
    else if  (LVS1_CSLV_from_cell == (0.0)) {
      LVS1_CSLV_k_u = 1 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
      cstate =  LVS1_CSLV_replay_cell1 ;
      force_init_update = False;
    }
    else if  (LVS1_CSLV_from_cell == (1.0) && (LVS1_CSLV_cell1_mode_delayed == (0.0))) {
      LVS1_CSLV_k_u = 1 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
      cstate =  LVS1_CSLV_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVS1_CSLV_k_init = LVS1_CSLV_k ;
      slope_LVS1_CSLV_k = 1 ;
      LVS1_CSLV_k_u = (slope_LVS1_CSLV_k * d) + LVS1_CSLV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVS1_CSLV_previous_direction2 ;
      force_init_update = False;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell1_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVS1_CSLV!\n");
      exit(1);
    }
    break;
  case ( LVS1_CSLV_wait_cell1 ):
    if (True == False) {;}
    else if  (LVS1_CSLV_cell2_mode == (2.0)) {
      LVS1_CSLV_k_u = 1 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
      cstate =  LVS1_CSLV_annhilate ;
      force_init_update = False;
    }
    else if  (LVS1_CSLV_k >= (20.0)) {
      LVS1_CSLV_from_cell_u = 1 ;
      LVS1_CSLV_cell1_replay_latch_u = 1 ;
      LVS1_CSLV_k_u = 1 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
      cstate =  LVS1_CSLV_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVS1_CSLV_k_init = LVS1_CSLV_k ;
      slope_LVS1_CSLV_k = 1 ;
      LVS1_CSLV_k_u = (slope_LVS1_CSLV_k * d) + LVS1_CSLV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVS1_CSLV_wait_cell1 ;
      force_init_update = False;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell1_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVS1_CSLV!\n");
      exit(1);
    }
    break;
  case ( LVS1_CSLV_replay_cell1 ):
    if (True == False) {;}
    else if  (LVS1_CSLV_cell1_mode == (2.0)) {
      LVS1_CSLV_k_u = 1 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
      cstate =  LVS1_CSLV_annhilate ;
      force_init_update = False;
    }
    else if  (LVS1_CSLV_k >= (20.0)) {
      LVS1_CSLV_from_cell_u = 2 ;
      LVS1_CSLV_cell2_replay_latch_u = 1 ;
      LVS1_CSLV_k_u = 1 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
      cstate =  LVS1_CSLV_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVS1_CSLV_k_init = LVS1_CSLV_k ;
      slope_LVS1_CSLV_k = 1 ;
      LVS1_CSLV_k_u = (slope_LVS1_CSLV_k * d) + LVS1_CSLV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVS1_CSLV_replay_cell1 ;
      force_init_update = False;
      LVS1_CSLV_cell1_replay_latch_u = 1 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell1_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVS1_CSLV!\n");
      exit(1);
    }
    break;
  case ( LVS1_CSLV_replay_cell2 ):
    if (True == False) {;}
    else if  (LVS1_CSLV_k >= (10.0)) {
      LVS1_CSLV_k_u = 1 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
      cstate =  LVS1_CSLV_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVS1_CSLV_k_init = LVS1_CSLV_k ;
      slope_LVS1_CSLV_k = 1 ;
      LVS1_CSLV_k_u = (slope_LVS1_CSLV_k * d) + LVS1_CSLV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVS1_CSLV_replay_cell2 ;
      force_init_update = False;
      LVS1_CSLV_cell2_replay_latch_u = 1 ;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell1_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVS1_CSLV!\n");
      exit(1);
    }
    break;
  case ( LVS1_CSLV_wait_cell2 ):
    if (True == False) {;}
    else if  (LVS1_CSLV_k >= (10.0)) {
      LVS1_CSLV_k_u = 1 ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
      cstate =  LVS1_CSLV_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVS1_CSLV_k_init = LVS1_CSLV_k ;
      slope_LVS1_CSLV_k = 1 ;
      LVS1_CSLV_k_u = (slope_LVS1_CSLV_k * d) + LVS1_CSLV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVS1_CSLV_wait_cell2 ;
      force_init_update = False;
      LVS1_CSLV_cell1_v_delayed_u = LVS1_CSLV_update_c1vd () ;
      LVS1_CSLV_cell2_v_delayed_u = LVS1_CSLV_update_c2vd () ;
      LVS1_CSLV_cell1_mode_delayed_u = LVS1_CSLV_update_c1md () ;
      LVS1_CSLV_cell2_mode_delayed_u = LVS1_CSLV_update_c2md () ;
      LVS1_CSLV_wasted_u = LVS1_CSLV_update_buffer_index (LVS1_CSLV_cell1_v,LVS1_CSLV_cell2_v,LVS1_CSLV_cell1_mode,LVS1_CSLV_cell2_mode) ;
      LVS1_CSLV_cell1_replay_latch_u = LVS1_CSLV_update_latch1 (LVS1_CSLV_cell1_mode_delayed,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_replay_latch_u = LVS1_CSLV_update_latch2 (LVS1_CSLV_cell2_mode_delayed,LVS1_CSLV_cell2_replay_latch_u) ;
      LVS1_CSLV_cell1_v_replay = LVS1_CSLV_update_ocell1 (LVS1_CSLV_cell1_v_delayed_u,LVS1_CSLV_cell1_replay_latch_u) ;
      LVS1_CSLV_cell2_v_replay = LVS1_CSLV_update_ocell2 (LVS1_CSLV_cell2_v_delayed_u,LVS1_CSLV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVS1_CSLV!\n");
      exit(1);
    }
    break;
  }
  LVS1_CSLV_k = LVS1_CSLV_k_u;
  LVS1_CSLV_cell1_mode_delayed = LVS1_CSLV_cell1_mode_delayed_u;
  LVS1_CSLV_cell2_mode_delayed = LVS1_CSLV_cell2_mode_delayed_u;
  LVS1_CSLV_from_cell = LVS1_CSLV_from_cell_u;
  LVS1_CSLV_cell1_replay_latch = LVS1_CSLV_cell1_replay_latch_u;
  LVS1_CSLV_cell2_replay_latch = LVS1_CSLV_cell2_replay_latch_u;
  LVS1_CSLV_cell1_v_delayed = LVS1_CSLV_cell1_v_delayed_u;
  LVS1_CSLV_cell2_v_delayed = LVS1_CSLV_cell2_v_delayed_u;
  LVS1_CSLV_wasted = LVS1_CSLV_wasted_u;
  return cstate;
}