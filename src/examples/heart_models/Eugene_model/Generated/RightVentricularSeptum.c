#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double RightVentricularSeptum_v_i_0;
double RightVentricularSeptum_v_i_1;
double RightVentricularSeptum_v_i_2;
double RightVentricularSeptum_voo = 0.0;
double RightVentricularSeptum_state = 0.0;


static double  RightVentricularSeptum_vx  =  0 ,  RightVentricularSeptum_vy  =  0 ,  RightVentricularSeptum_vz  =  0 ,  RightVentricularSeptum_g  =  0 ,  RightVentricularSeptum_v  =  0 ,  RightVentricularSeptum_ft  =  0 ,  RightVentricularSeptum_theta  =  0 ,  RightVentricularSeptum_v_O  =  0 ; //the continuous vars
static double  RightVentricularSeptum_vx_u , RightVentricularSeptum_vy_u , RightVentricularSeptum_vz_u , RightVentricularSeptum_g_u , RightVentricularSeptum_v_u , RightVentricularSeptum_ft_u , RightVentricularSeptum_theta_u , RightVentricularSeptum_v_O_u ; // and their updates
static double  RightVentricularSeptum_vx_init , RightVentricularSeptum_vy_init , RightVentricularSeptum_vz_init , RightVentricularSeptum_g_init , RightVentricularSeptum_v_init , RightVentricularSeptum_ft_init , RightVentricularSeptum_theta_init , RightVentricularSeptum_v_O_init ; // and their inits
static double  slope_RightVentricularSeptum_vx , slope_RightVentricularSeptum_vy , slope_RightVentricularSeptum_vz , slope_RightVentricularSeptum_g , slope_RightVentricularSeptum_v , slope_RightVentricularSeptum_ft , slope_RightVentricularSeptum_theta , slope_RightVentricularSeptum_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { RightVentricularSeptum_t1 , RightVentricularSeptum_t2 , RightVentricularSeptum_t3 , RightVentricularSeptum_t4 }; // state declarations

enum states RightVentricularSeptum (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( RightVentricularSeptum_t1 ):
    if (True == False) {;}
    else if  (RightVentricularSeptum_g > (44.5)) {
      RightVentricularSeptum_vx_u = (0.3 * RightVentricularSeptum_v) ;
      RightVentricularSeptum_vy_u = 0 ;
      RightVentricularSeptum_vz_u = (0.7 * RightVentricularSeptum_v) ;
      RightVentricularSeptum_g_u = ((((((((RightVentricularSeptum_v_i_0 + (- ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricularSeptum_v_i_1 + (- ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightVentricularSeptum_theta_u = (RightVentricularSeptum_v / 30.0) ;
      RightVentricularSeptum_v_O_u = (131.1 + (- (80.1 * pow ( ((RightVentricularSeptum_v / 30.0)) , (0.5) )))) ;
      RightVentricularSeptum_ft_u = f (RightVentricularSeptum_theta,4.0e-2) ;
      cstate =  RightVentricularSeptum_t2 ;
      force_init_update = False;
    }

    else if ( RightVentricularSeptum_v <= (44.5)
               && 
              RightVentricularSeptum_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum_vx_init = RightVentricularSeptum_vx ;
      slope_RightVentricularSeptum_vx = (RightVentricularSeptum_vx * -8.7) ;
      RightVentricularSeptum_vx_u = (slope_RightVentricularSeptum_vx * d) + RightVentricularSeptum_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum_vy_init = RightVentricularSeptum_vy ;
      slope_RightVentricularSeptum_vy = (RightVentricularSeptum_vy * -190.9) ;
      RightVentricularSeptum_vy_u = (slope_RightVentricularSeptum_vy * d) + RightVentricularSeptum_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum_vz_init = RightVentricularSeptum_vz ;
      slope_RightVentricularSeptum_vz = (RightVentricularSeptum_vz * -190.4) ;
      RightVentricularSeptum_vz_u = (slope_RightVentricularSeptum_vz * d) + RightVentricularSeptum_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricularSeptum_t1 ;
      force_init_update = False;
      RightVentricularSeptum_g_u = ((((((((RightVentricularSeptum_v_i_0 + (- ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricularSeptum_v_i_1 + (- ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightVentricularSeptum_v_u = ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz) ;
      RightVentricularSeptum_voo = ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz) ;
      RightVentricularSeptum_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularSeptum!\n");
      exit(1);
    }
    break;
  case ( RightVentricularSeptum_t2 ):
    if (True == False) {;}
    else if  (RightVentricularSeptum_v >= (44.5)) {
      RightVentricularSeptum_vx_u = RightVentricularSeptum_vx ;
      RightVentricularSeptum_vy_u = RightVentricularSeptum_vy ;
      RightVentricularSeptum_vz_u = RightVentricularSeptum_vz ;
      RightVentricularSeptum_g_u = ((((((((RightVentricularSeptum_v_i_0 + (- ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricularSeptum_v_i_1 + (- ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  RightVentricularSeptum_t3 ;
      force_init_update = False;
    }
    else if  (RightVentricularSeptum_g <= (44.5)
               && 
              RightVentricularSeptum_v < (44.5)) {
      RightVentricularSeptum_vx_u = RightVentricularSeptum_vx ;
      RightVentricularSeptum_vy_u = RightVentricularSeptum_vy ;
      RightVentricularSeptum_vz_u = RightVentricularSeptum_vz ;
      RightVentricularSeptum_g_u = ((((((((RightVentricularSeptum_v_i_0 + (- ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricularSeptum_v_i_1 + (- ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  RightVentricularSeptum_t1 ;
      force_init_update = False;
    }

    else if ( RightVentricularSeptum_v < (44.5)
               && 
              RightVentricularSeptum_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum_vx_init = RightVentricularSeptum_vx ;
      slope_RightVentricularSeptum_vx = ((RightVentricularSeptum_vx * -23.6) + (777200.0 * RightVentricularSeptum_g)) ;
      RightVentricularSeptum_vx_u = (slope_RightVentricularSeptum_vx * d) + RightVentricularSeptum_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum_vy_init = RightVentricularSeptum_vy ;
      slope_RightVentricularSeptum_vy = ((RightVentricularSeptum_vy * -45.5) + (58900.0 * RightVentricularSeptum_g)) ;
      RightVentricularSeptum_vy_u = (slope_RightVentricularSeptum_vy * d) + RightVentricularSeptum_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum_vz_init = RightVentricularSeptum_vz ;
      slope_RightVentricularSeptum_vz = ((RightVentricularSeptum_vz * -12.9) + (276600.0 * RightVentricularSeptum_g)) ;
      RightVentricularSeptum_vz_u = (slope_RightVentricularSeptum_vz * d) + RightVentricularSeptum_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricularSeptum_t2 ;
      force_init_update = False;
      RightVentricularSeptum_g_u = ((((((((RightVentricularSeptum_v_i_0 + (- ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricularSeptum_v_i_1 + (- ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightVentricularSeptum_v_u = ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz) ;
      RightVentricularSeptum_voo = ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz) ;
      RightVentricularSeptum_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularSeptum!\n");
      exit(1);
    }
    break;
  case ( RightVentricularSeptum_t3 ):
    if (True == False) {;}
    else if  (RightVentricularSeptum_v >= (131.1)) {
      RightVentricularSeptum_vx_u = RightVentricularSeptum_vx ;
      RightVentricularSeptum_vy_u = RightVentricularSeptum_vy ;
      RightVentricularSeptum_vz_u = RightVentricularSeptum_vz ;
      RightVentricularSeptum_g_u = ((((((((RightVentricularSeptum_v_i_0 + (- ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricularSeptum_v_i_1 + (- ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  RightVentricularSeptum_t4 ;
      force_init_update = False;
    }

    else if ( RightVentricularSeptum_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum_vx_init = RightVentricularSeptum_vx ;
      slope_RightVentricularSeptum_vx = (RightVentricularSeptum_vx * -6.9) ;
      RightVentricularSeptum_vx_u = (slope_RightVentricularSeptum_vx * d) + RightVentricularSeptum_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum_vy_init = RightVentricularSeptum_vy ;
      slope_RightVentricularSeptum_vy = (RightVentricularSeptum_vy * 75.9) ;
      RightVentricularSeptum_vy_u = (slope_RightVentricularSeptum_vy * d) + RightVentricularSeptum_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum_vz_init = RightVentricularSeptum_vz ;
      slope_RightVentricularSeptum_vz = (RightVentricularSeptum_vz * 6826.5) ;
      RightVentricularSeptum_vz_u = (slope_RightVentricularSeptum_vz * d) + RightVentricularSeptum_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricularSeptum_t3 ;
      force_init_update = False;
      RightVentricularSeptum_g_u = ((((((((RightVentricularSeptum_v_i_0 + (- ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricularSeptum_v_i_1 + (- ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightVentricularSeptum_v_u = ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz) ;
      RightVentricularSeptum_voo = ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz) ;
      RightVentricularSeptum_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularSeptum!\n");
      exit(1);
    }
    break;
  case ( RightVentricularSeptum_t4 ):
    if (True == False) {;}
    else if  (RightVentricularSeptum_v <= (30.0)) {
      RightVentricularSeptum_vx_u = RightVentricularSeptum_vx ;
      RightVentricularSeptum_vy_u = RightVentricularSeptum_vy ;
      RightVentricularSeptum_vz_u = RightVentricularSeptum_vz ;
      RightVentricularSeptum_g_u = ((((((((RightVentricularSeptum_v_i_0 + (- ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricularSeptum_v_i_1 + (- ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  RightVentricularSeptum_t1 ;
      force_init_update = False;
    }

    else if ( RightVentricularSeptum_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum_vx_init = RightVentricularSeptum_vx ;
      slope_RightVentricularSeptum_vx = (RightVentricularSeptum_vx * -33.2) ;
      RightVentricularSeptum_vx_u = (slope_RightVentricularSeptum_vx * d) + RightVentricularSeptum_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum_vy_init = RightVentricularSeptum_vy ;
      slope_RightVentricularSeptum_vy = ((RightVentricularSeptum_vy * 11.0) * RightVentricularSeptum_ft) ;
      RightVentricularSeptum_vy_u = (slope_RightVentricularSeptum_vy * d) + RightVentricularSeptum_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum_vz_init = RightVentricularSeptum_vz ;
      slope_RightVentricularSeptum_vz = ((RightVentricularSeptum_vz * 2.0) * RightVentricularSeptum_ft) ;
      RightVentricularSeptum_vz_u = (slope_RightVentricularSeptum_vz * d) + RightVentricularSeptum_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricularSeptum_t4 ;
      force_init_update = False;
      RightVentricularSeptum_g_u = ((((((((RightVentricularSeptum_v_i_0 + (- ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricularSeptum_v_i_1 + (- ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightVentricularSeptum_v_u = ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz) ;
      RightVentricularSeptum_voo = ((RightVentricularSeptum_vx + (- RightVentricularSeptum_vy)) + RightVentricularSeptum_vz) ;
      RightVentricularSeptum_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularSeptum!\n");
      exit(1);
    }
    break;
  }
  RightVentricularSeptum_vx = RightVentricularSeptum_vx_u;
  RightVentricularSeptum_vy = RightVentricularSeptum_vy_u;
  RightVentricularSeptum_vz = RightVentricularSeptum_vz_u;
  RightVentricularSeptum_g = RightVentricularSeptum_g_u;
  RightVentricularSeptum_v = RightVentricularSeptum_v_u;
  RightVentricularSeptum_ft = RightVentricularSeptum_ft_u;
  RightVentricularSeptum_theta = RightVentricularSeptum_theta_u;
  RightVentricularSeptum_v_O = RightVentricularSeptum_v_O_u;
  return cstate;
}