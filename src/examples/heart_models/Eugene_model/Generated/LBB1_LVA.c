#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double LBB1_LVA_update_c1vd();
extern double LBB1_LVA_update_c2vd();
extern double LBB1_LVA_update_c1md();
extern double LBB1_LVA_update_c2md();
extern double LBB1_LVA_update_buffer_index(double,double,double,double);
extern double LBB1_LVA_update_latch1(double,double);
extern double LBB1_LVA_update_latch2(double,double);
extern double LBB1_LVA_update_ocell1(double,double);
extern double LBB1_LVA_update_ocell2(double,double);
double LBB1_LVA_cell1_v;
double LBB1_LVA_cell1_mode;
double LBB1_LVA_cell2_v;
double LBB1_LVA_cell2_mode;
double LBB1_LVA_cell1_v_replay = 0.0;
double LBB1_LVA_cell2_v_replay = 0.0;


static double  LBB1_LVA_k  =  0.0 ,  LBB1_LVA_cell1_mode_delayed  =  0.0 ,  LBB1_LVA_cell2_mode_delayed  =  0.0 ,  LBB1_LVA_from_cell  =  0.0 ,  LBB1_LVA_cell1_replay_latch  =  0.0 ,  LBB1_LVA_cell2_replay_latch  =  0.0 ,  LBB1_LVA_cell1_v_delayed  =  0.0 ,  LBB1_LVA_cell2_v_delayed  =  0.0 ,  LBB1_LVA_wasted  =  0.0 ; //the continuous vars
static double  LBB1_LVA_k_u , LBB1_LVA_cell1_mode_delayed_u , LBB1_LVA_cell2_mode_delayed_u , LBB1_LVA_from_cell_u , LBB1_LVA_cell1_replay_latch_u , LBB1_LVA_cell2_replay_latch_u , LBB1_LVA_cell1_v_delayed_u , LBB1_LVA_cell2_v_delayed_u , LBB1_LVA_wasted_u ; // and their updates
static double  LBB1_LVA_k_init , LBB1_LVA_cell1_mode_delayed_init , LBB1_LVA_cell2_mode_delayed_init , LBB1_LVA_from_cell_init , LBB1_LVA_cell1_replay_latch_init , LBB1_LVA_cell2_replay_latch_init , LBB1_LVA_cell1_v_delayed_init , LBB1_LVA_cell2_v_delayed_init , LBB1_LVA_wasted_init ; // and their inits
static double  slope_LBB1_LVA_k , slope_LBB1_LVA_cell1_mode_delayed , slope_LBB1_LVA_cell2_mode_delayed , slope_LBB1_LVA_from_cell , slope_LBB1_LVA_cell1_replay_latch , slope_LBB1_LVA_cell2_replay_latch , slope_LBB1_LVA_cell1_v_delayed , slope_LBB1_LVA_cell2_v_delayed , slope_LBB1_LVA_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { LBB1_LVA_idle , LBB1_LVA_annhilate , LBB1_LVA_previous_drection1 , LBB1_LVA_previous_direction2 , LBB1_LVA_wait_cell1 , LBB1_LVA_replay_cell1 , LBB1_LVA_replay_cell2 , LBB1_LVA_wait_cell2 }; // state declarations

enum states LBB1_LVA (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( LBB1_LVA_idle ):
    if (True == False) {;}
    else if  (LBB1_LVA_cell2_mode == (2.0) && (LBB1_LVA_cell1_mode != (2.0))) {
      LBB1_LVA_k_u = 1 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
      cstate =  LBB1_LVA_previous_direction2 ;
      force_init_update = False;
    }
    else if  (LBB1_LVA_cell1_mode == (2.0) && (LBB1_LVA_cell2_mode != (2.0))) {
      LBB1_LVA_k_u = 1 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
      cstate =  LBB1_LVA_previous_drection1 ;
      force_init_update = False;
    }
    else if  (LBB1_LVA_cell1_mode == (2.0) && (LBB1_LVA_cell2_mode == (2.0))) {
      LBB1_LVA_k_u = 1 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
      cstate =  LBB1_LVA_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LBB1_LVA_k_init = LBB1_LVA_k ;
      slope_LBB1_LVA_k = 1 ;
      LBB1_LVA_k_u = (slope_LBB1_LVA_k * d) + LBB1_LVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LBB1_LVA_idle ;
      force_init_update = False;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell1_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB1_LVA!\n");
      exit(1);
    }
    break;
  case ( LBB1_LVA_annhilate ):
    if (True == False) {;}
    else if  (LBB1_LVA_cell1_mode != (2.0) && (LBB1_LVA_cell2_mode != (2.0))) {
      LBB1_LVA_k_u = 1 ;
      LBB1_LVA_from_cell_u = 0 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
      cstate =  LBB1_LVA_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LBB1_LVA_k_init = LBB1_LVA_k ;
      slope_LBB1_LVA_k = 1 ;
      LBB1_LVA_k_u = (slope_LBB1_LVA_k * d) + LBB1_LVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LBB1_LVA_annhilate ;
      force_init_update = False;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell1_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB1_LVA!\n");
      exit(1);
    }
    break;
  case ( LBB1_LVA_previous_drection1 ):
    if (True == False) {;}
    else if  (LBB1_LVA_from_cell == (1.0)) {
      LBB1_LVA_k_u = 1 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
      cstate =  LBB1_LVA_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LBB1_LVA_from_cell == (0.0)) {
      LBB1_LVA_k_u = 1 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
      cstate =  LBB1_LVA_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LBB1_LVA_from_cell == (2.0) && (LBB1_LVA_cell2_mode_delayed == (0.0))) {
      LBB1_LVA_k_u = 1 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
      cstate =  LBB1_LVA_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LBB1_LVA_from_cell == (2.0) && (LBB1_LVA_cell2_mode_delayed != (0.0))) {
      LBB1_LVA_k_u = 1 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
      cstate =  LBB1_LVA_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LBB1_LVA_k_init = LBB1_LVA_k ;
      slope_LBB1_LVA_k = 1 ;
      LBB1_LVA_k_u = (slope_LBB1_LVA_k * d) + LBB1_LVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LBB1_LVA_previous_drection1 ;
      force_init_update = False;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell1_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB1_LVA!\n");
      exit(1);
    }
    break;
  case ( LBB1_LVA_previous_direction2 ):
    if (True == False) {;}
    else if  (LBB1_LVA_from_cell == (1.0) && (LBB1_LVA_cell1_mode_delayed != (0.0))) {
      LBB1_LVA_k_u = 1 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
      cstate =  LBB1_LVA_annhilate ;
      force_init_update = False;
    }
    else if  (LBB1_LVA_from_cell == (2.0)) {
      LBB1_LVA_k_u = 1 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
      cstate =  LBB1_LVA_replay_cell1 ;
      force_init_update = False;
    }
    else if  (LBB1_LVA_from_cell == (0.0)) {
      LBB1_LVA_k_u = 1 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
      cstate =  LBB1_LVA_replay_cell1 ;
      force_init_update = False;
    }
    else if  (LBB1_LVA_from_cell == (1.0) && (LBB1_LVA_cell1_mode_delayed == (0.0))) {
      LBB1_LVA_k_u = 1 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
      cstate =  LBB1_LVA_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LBB1_LVA_k_init = LBB1_LVA_k ;
      slope_LBB1_LVA_k = 1 ;
      LBB1_LVA_k_u = (slope_LBB1_LVA_k * d) + LBB1_LVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LBB1_LVA_previous_direction2 ;
      force_init_update = False;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell1_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB1_LVA!\n");
      exit(1);
    }
    break;
  case ( LBB1_LVA_wait_cell1 ):
    if (True == False) {;}
    else if  (LBB1_LVA_cell2_mode == (2.0)) {
      LBB1_LVA_k_u = 1 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
      cstate =  LBB1_LVA_annhilate ;
      force_init_update = False;
    }
    else if  (LBB1_LVA_k >= (5.0)) {
      LBB1_LVA_from_cell_u = 1 ;
      LBB1_LVA_cell1_replay_latch_u = 1 ;
      LBB1_LVA_k_u = 1 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
      cstate =  LBB1_LVA_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LBB1_LVA_k_init = LBB1_LVA_k ;
      slope_LBB1_LVA_k = 1 ;
      LBB1_LVA_k_u = (slope_LBB1_LVA_k * d) + LBB1_LVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LBB1_LVA_wait_cell1 ;
      force_init_update = False;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell1_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB1_LVA!\n");
      exit(1);
    }
    break;
  case ( LBB1_LVA_replay_cell1 ):
    if (True == False) {;}
    else if  (LBB1_LVA_cell1_mode == (2.0)) {
      LBB1_LVA_k_u = 1 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
      cstate =  LBB1_LVA_annhilate ;
      force_init_update = False;
    }
    else if  (LBB1_LVA_k >= (5.0)) {
      LBB1_LVA_from_cell_u = 2 ;
      LBB1_LVA_cell2_replay_latch_u = 1 ;
      LBB1_LVA_k_u = 1 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
      cstate =  LBB1_LVA_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LBB1_LVA_k_init = LBB1_LVA_k ;
      slope_LBB1_LVA_k = 1 ;
      LBB1_LVA_k_u = (slope_LBB1_LVA_k * d) + LBB1_LVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LBB1_LVA_replay_cell1 ;
      force_init_update = False;
      LBB1_LVA_cell1_replay_latch_u = 1 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell1_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB1_LVA!\n");
      exit(1);
    }
    break;
  case ( LBB1_LVA_replay_cell2 ):
    if (True == False) {;}
    else if  (LBB1_LVA_k >= (10.0)) {
      LBB1_LVA_k_u = 1 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
      cstate =  LBB1_LVA_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LBB1_LVA_k_init = LBB1_LVA_k ;
      slope_LBB1_LVA_k = 1 ;
      LBB1_LVA_k_u = (slope_LBB1_LVA_k * d) + LBB1_LVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LBB1_LVA_replay_cell2 ;
      force_init_update = False;
      LBB1_LVA_cell2_replay_latch_u = 1 ;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell1_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB1_LVA!\n");
      exit(1);
    }
    break;
  case ( LBB1_LVA_wait_cell2 ):
    if (True == False) {;}
    else if  (LBB1_LVA_k >= (10.0)) {
      LBB1_LVA_k_u = 1 ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
      cstate =  LBB1_LVA_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LBB1_LVA_k_init = LBB1_LVA_k ;
      slope_LBB1_LVA_k = 1 ;
      LBB1_LVA_k_u = (slope_LBB1_LVA_k * d) + LBB1_LVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LBB1_LVA_wait_cell2 ;
      force_init_update = False;
      LBB1_LVA_cell1_v_delayed_u = LBB1_LVA_update_c1vd () ;
      LBB1_LVA_cell2_v_delayed_u = LBB1_LVA_update_c2vd () ;
      LBB1_LVA_cell1_mode_delayed_u = LBB1_LVA_update_c1md () ;
      LBB1_LVA_cell2_mode_delayed_u = LBB1_LVA_update_c2md () ;
      LBB1_LVA_wasted_u = LBB1_LVA_update_buffer_index (LBB1_LVA_cell1_v,LBB1_LVA_cell2_v,LBB1_LVA_cell1_mode,LBB1_LVA_cell2_mode) ;
      LBB1_LVA_cell1_replay_latch_u = LBB1_LVA_update_latch1 (LBB1_LVA_cell1_mode_delayed,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_replay_latch_u = LBB1_LVA_update_latch2 (LBB1_LVA_cell2_mode_delayed,LBB1_LVA_cell2_replay_latch_u) ;
      LBB1_LVA_cell1_v_replay = LBB1_LVA_update_ocell1 (LBB1_LVA_cell1_v_delayed_u,LBB1_LVA_cell1_replay_latch_u) ;
      LBB1_LVA_cell2_v_replay = LBB1_LVA_update_ocell2 (LBB1_LVA_cell2_v_delayed_u,LBB1_LVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB1_LVA!\n");
      exit(1);
    }
    break;
  }
  LBB1_LVA_k = LBB1_LVA_k_u;
  LBB1_LVA_cell1_mode_delayed = LBB1_LVA_cell1_mode_delayed_u;
  LBB1_LVA_cell2_mode_delayed = LBB1_LVA_cell2_mode_delayed_u;
  LBB1_LVA_from_cell = LBB1_LVA_from_cell_u;
  LBB1_LVA_cell1_replay_latch = LBB1_LVA_cell1_replay_latch_u;
  LBB1_LVA_cell2_replay_latch = LBB1_LVA_cell2_replay_latch_u;
  LBB1_LVA_cell1_v_delayed = LBB1_LVA_cell1_v_delayed_u;
  LBB1_LVA_cell2_v_delayed = LBB1_LVA_cell2_v_delayed_u;
  LBB1_LVA_wasted = LBB1_LVA_wasted_u;
  return cstate;
}