#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double AtrioventricularNode_v_i_0;
double AtrioventricularNode_v_i_1;
double AtrioventricularNode_v_i_2;
double AtrioventricularNode_v_i_3;
double AtrioventricularNode_voo = 0.0;
double AtrioventricularNode_state = 0.0;


static double  AtrioventricularNode_vx  =  0 ,  AtrioventricularNode_vy  =  0 ,  AtrioventricularNode_vz  =  0 ,  AtrioventricularNode_g  =  0 ,  AtrioventricularNode_v  =  0 ,  AtrioventricularNode_ft  =  0 ,  AtrioventricularNode_theta  =  0 ,  AtrioventricularNode_v_O  =  0 ; //the continuous vars
static double  AtrioventricularNode_vx_u , AtrioventricularNode_vy_u , AtrioventricularNode_vz_u , AtrioventricularNode_g_u , AtrioventricularNode_v_u , AtrioventricularNode_ft_u , AtrioventricularNode_theta_u , AtrioventricularNode_v_O_u ; // and their updates
static double  AtrioventricularNode_vx_init , AtrioventricularNode_vy_init , AtrioventricularNode_vz_init , AtrioventricularNode_g_init , AtrioventricularNode_v_init , AtrioventricularNode_ft_init , AtrioventricularNode_theta_init , AtrioventricularNode_v_O_init ; // and their inits
static double  slope_AtrioventricularNode_vx , slope_AtrioventricularNode_vy , slope_AtrioventricularNode_vz , slope_AtrioventricularNode_g , slope_AtrioventricularNode_v , slope_AtrioventricularNode_ft , slope_AtrioventricularNode_theta , slope_AtrioventricularNode_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { AtrioventricularNode_t1 , AtrioventricularNode_t2 , AtrioventricularNode_t3 , AtrioventricularNode_t4 }; // state declarations

enum states AtrioventricularNode (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( AtrioventricularNode_t1 ):
    if (True == False) {;}
    else if  (AtrioventricularNode_g > (44.5)) {
      AtrioventricularNode_vx_u = (0.3 * AtrioventricularNode_v) ;
      AtrioventricularNode_vy_u = 0 ;
      AtrioventricularNode_vz_u = (0.7 * AtrioventricularNode_v) ;
      AtrioventricularNode_g_u = ((((((((AtrioventricularNode_v_i_0 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((AtrioventricularNode_v_i_1 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((AtrioventricularNode_v_i_2 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      AtrioventricularNode_theta_u = (AtrioventricularNode_v / 30.0) ;
      AtrioventricularNode_v_O_u = (131.1 + (- (80.1 * pow ( ((AtrioventricularNode_v / 30.0)) , (0.5) )))) ;
      AtrioventricularNode_ft_u = f (AtrioventricularNode_theta,4.0e-2) ;
      cstate =  AtrioventricularNode_t2 ;
      force_init_update = False;
    }

    else if ( AtrioventricularNode_v <= (44.5)
               && 
              AtrioventricularNode_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) AtrioventricularNode_vx_init = AtrioventricularNode_vx ;
      slope_AtrioventricularNode_vx = (AtrioventricularNode_vx * -8.7) ;
      AtrioventricularNode_vx_u = (slope_AtrioventricularNode_vx * d) + AtrioventricularNode_vx ;
      if ((pstate != cstate) || force_init_update) AtrioventricularNode_vy_init = AtrioventricularNode_vy ;
      slope_AtrioventricularNode_vy = (AtrioventricularNode_vy * -190.9) ;
      AtrioventricularNode_vy_u = (slope_AtrioventricularNode_vy * d) + AtrioventricularNode_vy ;
      if ((pstate != cstate) || force_init_update) AtrioventricularNode_vz_init = AtrioventricularNode_vz ;
      slope_AtrioventricularNode_vz = (AtrioventricularNode_vz * -190.4) ;
      AtrioventricularNode_vz_u = (slope_AtrioventricularNode_vz * d) + AtrioventricularNode_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  AtrioventricularNode_t1 ;
      force_init_update = False;
      AtrioventricularNode_g_u = ((((((((AtrioventricularNode_v_i_0 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((AtrioventricularNode_v_i_1 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((AtrioventricularNode_v_i_2 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      AtrioventricularNode_v_u = ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz) ;
      AtrioventricularNode_voo = ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz) ;
      AtrioventricularNode_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: AtrioventricularNode!\n");
      exit(1);
    }
    break;
  case ( AtrioventricularNode_t2 ):
    if (True == False) {;}
    else if  (AtrioventricularNode_v >= (44.5)) {
      AtrioventricularNode_vx_u = AtrioventricularNode_vx ;
      AtrioventricularNode_vy_u = AtrioventricularNode_vy ;
      AtrioventricularNode_vz_u = AtrioventricularNode_vz ;
      AtrioventricularNode_g_u = ((((((((AtrioventricularNode_v_i_0 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((AtrioventricularNode_v_i_1 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((AtrioventricularNode_v_i_2 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      cstate =  AtrioventricularNode_t3 ;
      force_init_update = False;
    }
    else if  (AtrioventricularNode_g <= (44.5)
               && 
              AtrioventricularNode_v < (44.5)) {
      AtrioventricularNode_vx_u = AtrioventricularNode_vx ;
      AtrioventricularNode_vy_u = AtrioventricularNode_vy ;
      AtrioventricularNode_vz_u = AtrioventricularNode_vz ;
      AtrioventricularNode_g_u = ((((((((AtrioventricularNode_v_i_0 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((AtrioventricularNode_v_i_1 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((AtrioventricularNode_v_i_2 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      cstate =  AtrioventricularNode_t1 ;
      force_init_update = False;
    }

    else if ( AtrioventricularNode_v < (44.5)
               && 
              AtrioventricularNode_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) AtrioventricularNode_vx_init = AtrioventricularNode_vx ;
      slope_AtrioventricularNode_vx = ((AtrioventricularNode_vx * -23.6) + (777200.0 * AtrioventricularNode_g)) ;
      AtrioventricularNode_vx_u = (slope_AtrioventricularNode_vx * d) + AtrioventricularNode_vx ;
      if ((pstate != cstate) || force_init_update) AtrioventricularNode_vy_init = AtrioventricularNode_vy ;
      slope_AtrioventricularNode_vy = ((AtrioventricularNode_vy * -45.5) + (58900.0 * AtrioventricularNode_g)) ;
      AtrioventricularNode_vy_u = (slope_AtrioventricularNode_vy * d) + AtrioventricularNode_vy ;
      if ((pstate != cstate) || force_init_update) AtrioventricularNode_vz_init = AtrioventricularNode_vz ;
      slope_AtrioventricularNode_vz = ((AtrioventricularNode_vz * -12.9) + (276600.0 * AtrioventricularNode_g)) ;
      AtrioventricularNode_vz_u = (slope_AtrioventricularNode_vz * d) + AtrioventricularNode_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  AtrioventricularNode_t2 ;
      force_init_update = False;
      AtrioventricularNode_g_u = ((((((((AtrioventricularNode_v_i_0 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((AtrioventricularNode_v_i_1 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((AtrioventricularNode_v_i_2 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      AtrioventricularNode_v_u = ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz) ;
      AtrioventricularNode_voo = ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz) ;
      AtrioventricularNode_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: AtrioventricularNode!\n");
      exit(1);
    }
    break;
  case ( AtrioventricularNode_t3 ):
    if (True == False) {;}
    else if  (AtrioventricularNode_v >= (131.1)) {
      AtrioventricularNode_vx_u = AtrioventricularNode_vx ;
      AtrioventricularNode_vy_u = AtrioventricularNode_vy ;
      AtrioventricularNode_vz_u = AtrioventricularNode_vz ;
      AtrioventricularNode_g_u = ((((((((AtrioventricularNode_v_i_0 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((AtrioventricularNode_v_i_1 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((AtrioventricularNode_v_i_2 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      cstate =  AtrioventricularNode_t4 ;
      force_init_update = False;
    }

    else if ( AtrioventricularNode_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) AtrioventricularNode_vx_init = AtrioventricularNode_vx ;
      slope_AtrioventricularNode_vx = (AtrioventricularNode_vx * -6.9) ;
      AtrioventricularNode_vx_u = (slope_AtrioventricularNode_vx * d) + AtrioventricularNode_vx ;
      if ((pstate != cstate) || force_init_update) AtrioventricularNode_vy_init = AtrioventricularNode_vy ;
      slope_AtrioventricularNode_vy = (AtrioventricularNode_vy * 75.9) ;
      AtrioventricularNode_vy_u = (slope_AtrioventricularNode_vy * d) + AtrioventricularNode_vy ;
      if ((pstate != cstate) || force_init_update) AtrioventricularNode_vz_init = AtrioventricularNode_vz ;
      slope_AtrioventricularNode_vz = (AtrioventricularNode_vz * 6826.5) ;
      AtrioventricularNode_vz_u = (slope_AtrioventricularNode_vz * d) + AtrioventricularNode_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  AtrioventricularNode_t3 ;
      force_init_update = False;
      AtrioventricularNode_g_u = ((((((((AtrioventricularNode_v_i_0 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((AtrioventricularNode_v_i_1 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((AtrioventricularNode_v_i_2 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      AtrioventricularNode_v_u = ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz) ;
      AtrioventricularNode_voo = ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz) ;
      AtrioventricularNode_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: AtrioventricularNode!\n");
      exit(1);
    }
    break;
  case ( AtrioventricularNode_t4 ):
    if (True == False) {;}
    else if  (AtrioventricularNode_v <= (30.0)) {
      AtrioventricularNode_vx_u = AtrioventricularNode_vx ;
      AtrioventricularNode_vy_u = AtrioventricularNode_vy ;
      AtrioventricularNode_vz_u = AtrioventricularNode_vz ;
      AtrioventricularNode_g_u = ((((((((AtrioventricularNode_v_i_0 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((AtrioventricularNode_v_i_1 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((AtrioventricularNode_v_i_2 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      cstate =  AtrioventricularNode_t1 ;
      force_init_update = False;
    }

    else if ( AtrioventricularNode_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) AtrioventricularNode_vx_init = AtrioventricularNode_vx ;
      slope_AtrioventricularNode_vx = (AtrioventricularNode_vx * -33.2) ;
      AtrioventricularNode_vx_u = (slope_AtrioventricularNode_vx * d) + AtrioventricularNode_vx ;
      if ((pstate != cstate) || force_init_update) AtrioventricularNode_vy_init = AtrioventricularNode_vy ;
      slope_AtrioventricularNode_vy = ((AtrioventricularNode_vy * 20.0) * AtrioventricularNode_ft) ;
      AtrioventricularNode_vy_u = (slope_AtrioventricularNode_vy * d) + AtrioventricularNode_vy ;
      if ((pstate != cstate) || force_init_update) AtrioventricularNode_vz_init = AtrioventricularNode_vz ;
      slope_AtrioventricularNode_vz = ((AtrioventricularNode_vz * 2.0) * AtrioventricularNode_ft) ;
      AtrioventricularNode_vz_u = (slope_AtrioventricularNode_vz * d) + AtrioventricularNode_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  AtrioventricularNode_t4 ;
      force_init_update = False;
      AtrioventricularNode_g_u = ((((((((AtrioventricularNode_v_i_0 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((AtrioventricularNode_v_i_1 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((AtrioventricularNode_v_i_2 + (- ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      AtrioventricularNode_v_u = ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz) ;
      AtrioventricularNode_voo = ((AtrioventricularNode_vx + (- AtrioventricularNode_vy)) + AtrioventricularNode_vz) ;
      AtrioventricularNode_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: AtrioventricularNode!\n");
      exit(1);
    }
    break;
  }
  AtrioventricularNode_vx = AtrioventricularNode_vx_u;
  AtrioventricularNode_vy = AtrioventricularNode_vy_u;
  AtrioventricularNode_vz = AtrioventricularNode_vz_u;
  AtrioventricularNode_g = AtrioventricularNode_g_u;
  AtrioventricularNode_v = AtrioventricularNode_v_u;
  AtrioventricularNode_ft = AtrioventricularNode_ft_u;
  AtrioventricularNode_theta = AtrioventricularNode_theta_u;
  AtrioventricularNode_v_O = AtrioventricularNode_v_O_u;
  return cstate;
}