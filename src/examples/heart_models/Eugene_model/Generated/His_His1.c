#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double His_His1_update_c1vd();
extern double His_His1_update_c2vd();
extern double His_His1_update_c1md();
extern double His_His1_update_c2md();
extern double His_His1_update_buffer_index(double,double,double,double);
extern double His_His1_update_latch1(double,double);
extern double His_His1_update_latch2(double,double);
extern double His_His1_update_ocell1(double,double);
extern double His_His1_update_ocell2(double,double);
double His_His1_cell1_v;
double His_His1_cell1_mode;
double His_His1_cell2_v;
double His_His1_cell2_mode;
double His_His1_cell1_v_replay = 0.0;
double His_His1_cell2_v_replay = 0.0;


static double  His_His1_k  =  0.0 ,  His_His1_cell1_mode_delayed  =  0.0 ,  His_His1_cell2_mode_delayed  =  0.0 ,  His_His1_from_cell  =  0.0 ,  His_His1_cell1_replay_latch  =  0.0 ,  His_His1_cell2_replay_latch  =  0.0 ,  His_His1_cell1_v_delayed  =  0.0 ,  His_His1_cell2_v_delayed  =  0.0 ,  His_His1_wasted  =  0.0 ; //the continuous vars
static double  His_His1_k_u , His_His1_cell1_mode_delayed_u , His_His1_cell2_mode_delayed_u , His_His1_from_cell_u , His_His1_cell1_replay_latch_u , His_His1_cell2_replay_latch_u , His_His1_cell1_v_delayed_u , His_His1_cell2_v_delayed_u , His_His1_wasted_u ; // and their updates
static double  His_His1_k_init , His_His1_cell1_mode_delayed_init , His_His1_cell2_mode_delayed_init , His_His1_from_cell_init , His_His1_cell1_replay_latch_init , His_His1_cell2_replay_latch_init , His_His1_cell1_v_delayed_init , His_His1_cell2_v_delayed_init , His_His1_wasted_init ; // and their inits
static double  slope_His_His1_k , slope_His_His1_cell1_mode_delayed , slope_His_His1_cell2_mode_delayed , slope_His_His1_from_cell , slope_His_His1_cell1_replay_latch , slope_His_His1_cell2_replay_latch , slope_His_His1_cell1_v_delayed , slope_His_His1_cell2_v_delayed , slope_His_His1_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { His_His1_idle , His_His1_annhilate , His_His1_previous_drection1 , His_His1_previous_direction2 , His_His1_wait_cell1 , His_His1_replay_cell1 , His_His1_replay_cell2 , His_His1_wait_cell2 }; // state declarations

enum states His_His1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( His_His1_idle ):
    if (True == False) {;}
    else if  (His_His1_cell2_mode == (2.0) && (His_His1_cell1_mode != (2.0))) {
      His_His1_k_u = 1 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
      cstate =  His_His1_previous_direction2 ;
      force_init_update = False;
    }
    else if  (His_His1_cell1_mode == (2.0) && (His_His1_cell2_mode != (2.0))) {
      His_His1_k_u = 1 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
      cstate =  His_His1_previous_drection1 ;
      force_init_update = False;
    }
    else if  (His_His1_cell1_mode == (2.0) && (His_His1_cell2_mode == (2.0))) {
      His_His1_k_u = 1 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
      cstate =  His_His1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His_His1_k_init = His_His1_k ;
      slope_His_His1_k = 1 ;
      His_His1_k_u = (slope_His_His1_k * d) + His_His1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His_His1_idle ;
      force_init_update = False;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell1_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His_His1!\n");
      exit(1);
    }
    break;
  case ( His_His1_annhilate ):
    if (True == False) {;}
    else if  (His_His1_cell1_mode != (2.0) && (His_His1_cell2_mode != (2.0))) {
      His_His1_k_u = 1 ;
      His_His1_from_cell_u = 0 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
      cstate =  His_His1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His_His1_k_init = His_His1_k ;
      slope_His_His1_k = 1 ;
      His_His1_k_u = (slope_His_His1_k * d) + His_His1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His_His1_annhilate ;
      force_init_update = False;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell1_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His_His1!\n");
      exit(1);
    }
    break;
  case ( His_His1_previous_drection1 ):
    if (True == False) {;}
    else if  (His_His1_from_cell == (1.0)) {
      His_His1_k_u = 1 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
      cstate =  His_His1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (His_His1_from_cell == (0.0)) {
      His_His1_k_u = 1 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
      cstate =  His_His1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (His_His1_from_cell == (2.0) && (His_His1_cell2_mode_delayed == (0.0))) {
      His_His1_k_u = 1 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
      cstate =  His_His1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (His_His1_from_cell == (2.0) && (His_His1_cell2_mode_delayed != (0.0))) {
      His_His1_k_u = 1 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
      cstate =  His_His1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His_His1_k_init = His_His1_k ;
      slope_His_His1_k = 1 ;
      His_His1_k_u = (slope_His_His1_k * d) + His_His1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His_His1_previous_drection1 ;
      force_init_update = False;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell1_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His_His1!\n");
      exit(1);
    }
    break;
  case ( His_His1_previous_direction2 ):
    if (True == False) {;}
    else if  (His_His1_from_cell == (1.0) && (His_His1_cell1_mode_delayed != (0.0))) {
      His_His1_k_u = 1 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
      cstate =  His_His1_annhilate ;
      force_init_update = False;
    }
    else if  (His_His1_from_cell == (2.0)) {
      His_His1_k_u = 1 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
      cstate =  His_His1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (His_His1_from_cell == (0.0)) {
      His_His1_k_u = 1 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
      cstate =  His_His1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (His_His1_from_cell == (1.0) && (His_His1_cell1_mode_delayed == (0.0))) {
      His_His1_k_u = 1 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
      cstate =  His_His1_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His_His1_k_init = His_His1_k ;
      slope_His_His1_k = 1 ;
      His_His1_k_u = (slope_His_His1_k * d) + His_His1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His_His1_previous_direction2 ;
      force_init_update = False;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell1_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His_His1!\n");
      exit(1);
    }
    break;
  case ( His_His1_wait_cell1 ):
    if (True == False) {;}
    else if  (His_His1_cell2_mode == (2.0)) {
      His_His1_k_u = 1 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
      cstate =  His_His1_annhilate ;
      force_init_update = False;
    }
    else if  (His_His1_k >= (20.0)) {
      His_His1_from_cell_u = 1 ;
      His_His1_cell1_replay_latch_u = 1 ;
      His_His1_k_u = 1 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
      cstate =  His_His1_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His_His1_k_init = His_His1_k ;
      slope_His_His1_k = 1 ;
      His_His1_k_u = (slope_His_His1_k * d) + His_His1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His_His1_wait_cell1 ;
      force_init_update = False;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell1_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His_His1!\n");
      exit(1);
    }
    break;
  case ( His_His1_replay_cell1 ):
    if (True == False) {;}
    else if  (His_His1_cell1_mode == (2.0)) {
      His_His1_k_u = 1 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
      cstate =  His_His1_annhilate ;
      force_init_update = False;
    }
    else if  (His_His1_k >= (20.0)) {
      His_His1_from_cell_u = 2 ;
      His_His1_cell2_replay_latch_u = 1 ;
      His_His1_k_u = 1 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
      cstate =  His_His1_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His_His1_k_init = His_His1_k ;
      slope_His_His1_k = 1 ;
      His_His1_k_u = (slope_His_His1_k * d) + His_His1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His_His1_replay_cell1 ;
      force_init_update = False;
      His_His1_cell1_replay_latch_u = 1 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell1_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His_His1!\n");
      exit(1);
    }
    break;
  case ( His_His1_replay_cell2 ):
    if (True == False) {;}
    else if  (His_His1_k >= (10.0)) {
      His_His1_k_u = 1 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
      cstate =  His_His1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His_His1_k_init = His_His1_k ;
      slope_His_His1_k = 1 ;
      His_His1_k_u = (slope_His_His1_k * d) + His_His1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His_His1_replay_cell2 ;
      force_init_update = False;
      His_His1_cell2_replay_latch_u = 1 ;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell1_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His_His1!\n");
      exit(1);
    }
    break;
  case ( His_His1_wait_cell2 ):
    if (True == False) {;}
    else if  (His_His1_k >= (10.0)) {
      His_His1_k_u = 1 ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
      cstate =  His_His1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His_His1_k_init = His_His1_k ;
      slope_His_His1_k = 1 ;
      His_His1_k_u = (slope_His_His1_k * d) + His_His1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His_His1_wait_cell2 ;
      force_init_update = False;
      His_His1_cell1_v_delayed_u = His_His1_update_c1vd () ;
      His_His1_cell2_v_delayed_u = His_His1_update_c2vd () ;
      His_His1_cell1_mode_delayed_u = His_His1_update_c1md () ;
      His_His1_cell2_mode_delayed_u = His_His1_update_c2md () ;
      His_His1_wasted_u = His_His1_update_buffer_index (His_His1_cell1_v,His_His1_cell2_v,His_His1_cell1_mode,His_His1_cell2_mode) ;
      His_His1_cell1_replay_latch_u = His_His1_update_latch1 (His_His1_cell1_mode_delayed,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_replay_latch_u = His_His1_update_latch2 (His_His1_cell2_mode_delayed,His_His1_cell2_replay_latch_u) ;
      His_His1_cell1_v_replay = His_His1_update_ocell1 (His_His1_cell1_v_delayed_u,His_His1_cell1_replay_latch_u) ;
      His_His1_cell2_v_replay = His_His1_update_ocell2 (His_His1_cell2_v_delayed_u,His_His1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His_His1!\n");
      exit(1);
    }
    break;
  }
  His_His1_k = His_His1_k_u;
  His_His1_cell1_mode_delayed = His_His1_cell1_mode_delayed_u;
  His_His1_cell2_mode_delayed = His_His1_cell2_mode_delayed_u;
  His_His1_from_cell = His_His1_from_cell_u;
  His_His1_cell1_replay_latch = His_His1_cell1_replay_latch_u;
  His_His1_cell2_replay_latch = His_His1_cell2_replay_latch_u;
  His_His1_cell1_v_delayed = His_His1_cell1_v_delayed_u;
  His_His1_cell2_v_delayed = His_His1_cell2_v_delayed_u;
  His_His1_wasted = His_His1_wasted_u;
  return cstate;
}