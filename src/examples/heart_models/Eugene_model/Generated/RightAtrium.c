#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double RightAtrium_v_i_0;
double RightAtrium_v_i_1;
double RightAtrium_v_i_2;
double RightAtrium_v_i_3;
double RightAtrium_voo = 0.0;
double RightAtrium_state = 0.0;


static double  RightAtrium_vx  =  0 ,  RightAtrium_vy  =  0 ,  RightAtrium_vz  =  0 ,  RightAtrium_g  =  0 ,  RightAtrium_v  =  0 ,  RightAtrium_ft  =  0 ,  RightAtrium_theta  =  0 ,  RightAtrium_v_O  =  0 ; //the continuous vars
static double  RightAtrium_vx_u , RightAtrium_vy_u , RightAtrium_vz_u , RightAtrium_g_u , RightAtrium_v_u , RightAtrium_ft_u , RightAtrium_theta_u , RightAtrium_v_O_u ; // and their updates
static double  RightAtrium_vx_init , RightAtrium_vy_init , RightAtrium_vz_init , RightAtrium_g_init , RightAtrium_v_init , RightAtrium_ft_init , RightAtrium_theta_init , RightAtrium_v_O_init ; // and their inits
static double  slope_RightAtrium_vx , slope_RightAtrium_vy , slope_RightAtrium_vz , slope_RightAtrium_g , slope_RightAtrium_v , slope_RightAtrium_ft , slope_RightAtrium_theta , slope_RightAtrium_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { RightAtrium_t1 , RightAtrium_t2 , RightAtrium_t3 , RightAtrium_t4 }; // state declarations

enum states RightAtrium (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( RightAtrium_t1 ):
    if (True == False) {;}
    else if  (RightAtrium_g > (44.5)) {
      RightAtrium_vx_u = (0.3 * RightAtrium_v) ;
      RightAtrium_vy_u = 0 ;
      RightAtrium_vz_u = (0.7 * RightAtrium_v) ;
      RightAtrium_g_u = ((((((((RightAtrium_v_i_0 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightAtrium_v_i_1 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightAtrium_v_i_2 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0))) + 0) + 0) ;
      RightAtrium_theta_u = (RightAtrium_v / 30.0) ;
      RightAtrium_v_O_u = (131.1 + (- (80.1 * pow ( ((RightAtrium_v / 30.0)) , (0.5) )))) ;
      RightAtrium_ft_u = f (RightAtrium_theta,4.0e-2) ;
      cstate =  RightAtrium_t2 ;
      force_init_update = False;
    }

    else if ( RightAtrium_v <= (44.5)
               && 
              RightAtrium_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) RightAtrium_vx_init = RightAtrium_vx ;
      slope_RightAtrium_vx = (RightAtrium_vx * -8.7) ;
      RightAtrium_vx_u = (slope_RightAtrium_vx * d) + RightAtrium_vx ;
      if ((pstate != cstate) || force_init_update) RightAtrium_vy_init = RightAtrium_vy ;
      slope_RightAtrium_vy = (RightAtrium_vy * -190.9) ;
      RightAtrium_vy_u = (slope_RightAtrium_vy * d) + RightAtrium_vy ;
      if ((pstate != cstate) || force_init_update) RightAtrium_vz_init = RightAtrium_vz ;
      slope_RightAtrium_vz = (RightAtrium_vz * -190.4) ;
      RightAtrium_vz_u = (slope_RightAtrium_vz * d) + RightAtrium_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightAtrium_t1 ;
      force_init_update = False;
      RightAtrium_g_u = ((((((((RightAtrium_v_i_0 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightAtrium_v_i_1 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightAtrium_v_i_2 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0))) + 0) + 0) ;
      RightAtrium_v_u = ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz) ;
      RightAtrium_voo = ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz) ;
      RightAtrium_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightAtrium!\n");
      exit(1);
    }
    break;
  case ( RightAtrium_t2 ):
    if (True == False) {;}
    else if  (RightAtrium_v >= (44.5)) {
      RightAtrium_vx_u = RightAtrium_vx ;
      RightAtrium_vy_u = RightAtrium_vy ;
      RightAtrium_vz_u = RightAtrium_vz ;
      RightAtrium_g_u = ((((((((RightAtrium_v_i_0 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightAtrium_v_i_1 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightAtrium_v_i_2 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0))) + 0) + 0) ;
      cstate =  RightAtrium_t3 ;
      force_init_update = False;
    }
    else if  (RightAtrium_g <= (44.5)
               && RightAtrium_v < (44.5)) {
      RightAtrium_vx_u = RightAtrium_vx ;
      RightAtrium_vy_u = RightAtrium_vy ;
      RightAtrium_vz_u = RightAtrium_vz ;
      RightAtrium_g_u = ((((((((RightAtrium_v_i_0 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightAtrium_v_i_1 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightAtrium_v_i_2 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0))) + 0) + 0) ;
      cstate =  RightAtrium_t1 ;
      force_init_update = False;
    }

    else if ( RightAtrium_v < (44.5)
               && 
              RightAtrium_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) RightAtrium_vx_init = RightAtrium_vx ;
      slope_RightAtrium_vx = ((RightAtrium_vx * -23.6) + (777200.0 * RightAtrium_g)) ;
      RightAtrium_vx_u = (slope_RightAtrium_vx * d) + RightAtrium_vx ;
      if ((pstate != cstate) || force_init_update) RightAtrium_vy_init = RightAtrium_vy ;
      slope_RightAtrium_vy = ((RightAtrium_vy * -45.5) + (58900.0 * RightAtrium_g)) ;
      RightAtrium_vy_u = (slope_RightAtrium_vy * d) + RightAtrium_vy ;
      if ((pstate != cstate) || force_init_update) RightAtrium_vz_init = RightAtrium_vz ;
      slope_RightAtrium_vz = ((RightAtrium_vz * -12.9) + (276600.0 * RightAtrium_g)) ;
      RightAtrium_vz_u = (slope_RightAtrium_vz * d) + RightAtrium_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightAtrium_t2 ;
      force_init_update = False;
      RightAtrium_g_u = ((((((((RightAtrium_v_i_0 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightAtrium_v_i_1 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightAtrium_v_i_2 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0))) + 0) + 0) ;
      RightAtrium_v_u = ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz) ;
      RightAtrium_voo = ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz) ;
      RightAtrium_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightAtrium!\n");
      exit(1);
    }
    break;
  case ( RightAtrium_t3 ):
    if (True == False) {;}
    else if  (RightAtrium_v >= (131.1)) {
      RightAtrium_vx_u = RightAtrium_vx ;
      RightAtrium_vy_u = RightAtrium_vy ;
      RightAtrium_vz_u = RightAtrium_vz ;
      RightAtrium_g_u = ((((((((RightAtrium_v_i_0 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightAtrium_v_i_1 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightAtrium_v_i_2 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0))) + 0) + 0) ;
      cstate =  RightAtrium_t4 ;
      force_init_update = False;
    }

    else if ( RightAtrium_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) RightAtrium_vx_init = RightAtrium_vx ;
      slope_RightAtrium_vx = (RightAtrium_vx * -6.9) ;
      RightAtrium_vx_u = (slope_RightAtrium_vx * d) + RightAtrium_vx ;
      if ((pstate != cstate) || force_init_update) RightAtrium_vy_init = RightAtrium_vy ;
      slope_RightAtrium_vy = (RightAtrium_vy * 75.9) ;
      RightAtrium_vy_u = (slope_RightAtrium_vy * d) + RightAtrium_vy ;
      if ((pstate != cstate) || force_init_update) RightAtrium_vz_init = RightAtrium_vz ;
      slope_RightAtrium_vz = (RightAtrium_vz * 6826.5) ;
      RightAtrium_vz_u = (slope_RightAtrium_vz * d) + RightAtrium_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightAtrium_t3 ;
      force_init_update = False;
      RightAtrium_g_u = ((((((((RightAtrium_v_i_0 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightAtrium_v_i_1 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightAtrium_v_i_2 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0))) + 0) + 0) ;
      RightAtrium_v_u = ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz) ;
      RightAtrium_voo = ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz) ;
      RightAtrium_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightAtrium!\n");
      exit(1);
    }
    break;
  case ( RightAtrium_t4 ):
    if (True == False) {;}
    else if  (RightAtrium_v <= (30.0)) {
      RightAtrium_vx_u = RightAtrium_vx ;
      RightAtrium_vy_u = RightAtrium_vy ;
      RightAtrium_vz_u = RightAtrium_vz ;
      RightAtrium_g_u = ((((((((RightAtrium_v_i_0 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightAtrium_v_i_1 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightAtrium_v_i_2 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0))) + 0) + 0) ;
      cstate =  RightAtrium_t1 ;
      force_init_update = False;
    }

    else if ( RightAtrium_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) RightAtrium_vx_init = RightAtrium_vx ;
      slope_RightAtrium_vx = (RightAtrium_vx * -33.2) ;
      RightAtrium_vx_u = (slope_RightAtrium_vx * d) + RightAtrium_vx ;
      if ((pstate != cstate) || force_init_update) RightAtrium_vy_init = RightAtrium_vy ;
      slope_RightAtrium_vy = ((RightAtrium_vy * 20.0) * RightAtrium_ft) ;
      RightAtrium_vy_u = (slope_RightAtrium_vy * d) + RightAtrium_vy ;
      if ((pstate != cstate) || force_init_update) RightAtrium_vz_init = RightAtrium_vz ;
      slope_RightAtrium_vz = ((RightAtrium_vz * 2.0) * RightAtrium_ft) ;
      RightAtrium_vz_u = (slope_RightAtrium_vz * d) + RightAtrium_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightAtrium_t4 ;
      force_init_update = False;
      RightAtrium_g_u = ((((((((RightAtrium_v_i_0 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightAtrium_v_i_1 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightAtrium_v_i_2 + (- ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0))) + 0) + 0) ;
      RightAtrium_v_u = ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz) ;
      RightAtrium_voo = ((RightAtrium_vx + (- RightAtrium_vy)) + RightAtrium_vz) ;
      RightAtrium_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightAtrium!\n");
      exit(1);
    }
    break;
  }
  RightAtrium_vx = RightAtrium_vx_u;
  RightAtrium_vy = RightAtrium_vy_u;
  RightAtrium_vz = RightAtrium_vz_u;
  RightAtrium_g = RightAtrium_g_u;
  RightAtrium_v = RightAtrium_v_u;
  RightAtrium_ft = RightAtrium_ft_u;
  RightAtrium_theta = RightAtrium_theta_u;
  RightAtrium_v_O = RightAtrium_v_O_u;
  return cstate;
}