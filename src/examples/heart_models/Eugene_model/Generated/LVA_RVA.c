#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double LVA_RVA_update_c1vd();
extern double LVA_RVA_update_c2vd();
extern double LVA_RVA_update_c1md();
extern double LVA_RVA_update_c2md();
extern double LVA_RVA_update_buffer_index(double,double,double,double);
extern double LVA_RVA_update_latch1(double,double);
extern double LVA_RVA_update_latch2(double,double);
extern double LVA_RVA_update_ocell1(double,double);
extern double LVA_RVA_update_ocell2(double,double);
double LVA_RVA_cell1_v;
double LVA_RVA_cell1_mode;
double LVA_RVA_cell2_v;
double LVA_RVA_cell2_mode;
double LVA_RVA_cell1_v_replay = 0.0;
double LVA_RVA_cell2_v_replay = 0.0;


static double  LVA_RVA_k  =  0.0 ,  LVA_RVA_cell1_mode_delayed  =  0.0 ,  LVA_RVA_cell2_mode_delayed  =  0.0 ,  LVA_RVA_from_cell  =  0.0 ,  LVA_RVA_cell1_replay_latch  =  0.0 ,  LVA_RVA_cell2_replay_latch  =  0.0 ,  LVA_RVA_cell1_v_delayed  =  0.0 ,  LVA_RVA_cell2_v_delayed  =  0.0 ,  LVA_RVA_wasted  =  0.0 ; //the continuous vars
static double  LVA_RVA_k_u , LVA_RVA_cell1_mode_delayed_u , LVA_RVA_cell2_mode_delayed_u , LVA_RVA_from_cell_u , LVA_RVA_cell1_replay_latch_u , LVA_RVA_cell2_replay_latch_u , LVA_RVA_cell1_v_delayed_u , LVA_RVA_cell2_v_delayed_u , LVA_RVA_wasted_u ; // and their updates
static double  LVA_RVA_k_init , LVA_RVA_cell1_mode_delayed_init , LVA_RVA_cell2_mode_delayed_init , LVA_RVA_from_cell_init , LVA_RVA_cell1_replay_latch_init , LVA_RVA_cell2_replay_latch_init , LVA_RVA_cell1_v_delayed_init , LVA_RVA_cell2_v_delayed_init , LVA_RVA_wasted_init ; // and their inits
static double  slope_LVA_RVA_k , slope_LVA_RVA_cell1_mode_delayed , slope_LVA_RVA_cell2_mode_delayed , slope_LVA_RVA_from_cell , slope_LVA_RVA_cell1_replay_latch , slope_LVA_RVA_cell2_replay_latch , slope_LVA_RVA_cell1_v_delayed , slope_LVA_RVA_cell2_v_delayed , slope_LVA_RVA_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { LVA_RVA_idle , LVA_RVA_annhilate , LVA_RVA_previous_drection1 , LVA_RVA_previous_direction2 , LVA_RVA_wait_cell1 , LVA_RVA_replay_cell1 , LVA_RVA_replay_cell2 , LVA_RVA_wait_cell2 }; // state declarations

enum states LVA_RVA (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( LVA_RVA_idle ):
    if (True == False) {;}
    else if  (LVA_RVA_cell2_mode == (2.0) && (LVA_RVA_cell1_mode != (2.0))) {
      LVA_RVA_k_u = 1 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
      cstate =  LVA_RVA_previous_direction2 ;
      force_init_update = False;
    }
    else if  (LVA_RVA_cell1_mode == (2.0) && (LVA_RVA_cell2_mode != (2.0))) {
      LVA_RVA_k_u = 1 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
      cstate =  LVA_RVA_previous_drection1 ;
      force_init_update = False;
    }
    else if  (LVA_RVA_cell1_mode == (2.0) && (LVA_RVA_cell2_mode == (2.0))) {
      LVA_RVA_k_u = 1 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
      cstate =  LVA_RVA_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_RVA_k_init = LVA_RVA_k ;
      slope_LVA_RVA_k = 1 ;
      LVA_RVA_k_u = (slope_LVA_RVA_k * d) + LVA_RVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_RVA_idle ;
      force_init_update = False;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell1_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_RVA!\n");
      exit(1);
    }
    break;
  case ( LVA_RVA_annhilate ):
    if (True == False) {;}
    else if  (LVA_RVA_cell1_mode != (2.0) && (LVA_RVA_cell2_mode != (2.0))) {
      LVA_RVA_k_u = 1 ;
      LVA_RVA_from_cell_u = 0 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
      cstate =  LVA_RVA_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_RVA_k_init = LVA_RVA_k ;
      slope_LVA_RVA_k = 1 ;
      LVA_RVA_k_u = (slope_LVA_RVA_k * d) + LVA_RVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_RVA_annhilate ;
      force_init_update = False;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell1_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_RVA!\n");
      exit(1);
    }
    break;
  case ( LVA_RVA_previous_drection1 ):
    if (True == False) {;}
    else if  (LVA_RVA_from_cell == (1.0)) {
      LVA_RVA_k_u = 1 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
      cstate =  LVA_RVA_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LVA_RVA_from_cell == (0.0)) {
      LVA_RVA_k_u = 1 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
      cstate =  LVA_RVA_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LVA_RVA_from_cell == (2.0) && (LVA_RVA_cell2_mode_delayed == (0.0))) {
      LVA_RVA_k_u = 1 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
      cstate =  LVA_RVA_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LVA_RVA_from_cell == (2.0) && (LVA_RVA_cell2_mode_delayed != (0.0))) {
      LVA_RVA_k_u = 1 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
      cstate =  LVA_RVA_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_RVA_k_init = LVA_RVA_k ;
      slope_LVA_RVA_k = 1 ;
      LVA_RVA_k_u = (slope_LVA_RVA_k * d) + LVA_RVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_RVA_previous_drection1 ;
      force_init_update = False;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell1_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_RVA!\n");
      exit(1);
    }
    break;
  case ( LVA_RVA_previous_direction2 ):
    if (True == False) {;}
    else if  (LVA_RVA_from_cell == (1.0) && (LVA_RVA_cell1_mode_delayed != (0.0))) {
      LVA_RVA_k_u = 1 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
      cstate =  LVA_RVA_annhilate ;
      force_init_update = False;
    }
    else if  (LVA_RVA_from_cell == (2.0)) {
      LVA_RVA_k_u = 1 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
      cstate =  LVA_RVA_replay_cell1 ;
      force_init_update = False;
    }
    else if  (LVA_RVA_from_cell == (0.0)) {
      LVA_RVA_k_u = 1 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
      cstate =  LVA_RVA_replay_cell1 ;
      force_init_update = False;
    }
    else if  (LVA_RVA_from_cell == (1.0) && (LVA_RVA_cell1_mode_delayed == (0.0))) {
      LVA_RVA_k_u = 1 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
      cstate =  LVA_RVA_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_RVA_k_init = LVA_RVA_k ;
      slope_LVA_RVA_k = 1 ;
      LVA_RVA_k_u = (slope_LVA_RVA_k * d) + LVA_RVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_RVA_previous_direction2 ;
      force_init_update = False;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell1_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_RVA!\n");
      exit(1);
    }
    break;
  case ( LVA_RVA_wait_cell1 ):
    if (True == False) {;}
    else if  (LVA_RVA_cell2_mode == (2.0)) {
      LVA_RVA_k_u = 1 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
      cstate =  LVA_RVA_annhilate ;
      force_init_update = False;
    }
    else if  (LVA_RVA_k >= (5.0)) {
      LVA_RVA_from_cell_u = 1 ;
      LVA_RVA_cell1_replay_latch_u = 1 ;
      LVA_RVA_k_u = 1 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
      cstate =  LVA_RVA_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_RVA_k_init = LVA_RVA_k ;
      slope_LVA_RVA_k = 1 ;
      LVA_RVA_k_u = (slope_LVA_RVA_k * d) + LVA_RVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_RVA_wait_cell1 ;
      force_init_update = False;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell1_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_RVA!\n");
      exit(1);
    }
    break;
  case ( LVA_RVA_replay_cell1 ):
    if (True == False) {;}
    else if  (LVA_RVA_cell1_mode == (2.0)) {
      LVA_RVA_k_u = 1 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
      cstate =  LVA_RVA_annhilate ;
      force_init_update = False;
    }
    else if  (LVA_RVA_k >= (5.0)) {
      LVA_RVA_from_cell_u = 2 ;
      LVA_RVA_cell2_replay_latch_u = 1 ;
      LVA_RVA_k_u = 1 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
      cstate =  LVA_RVA_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_RVA_k_init = LVA_RVA_k ;
      slope_LVA_RVA_k = 1 ;
      LVA_RVA_k_u = (slope_LVA_RVA_k * d) + LVA_RVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_RVA_replay_cell1 ;
      force_init_update = False;
      LVA_RVA_cell1_replay_latch_u = 1 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell1_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_RVA!\n");
      exit(1);
    }
    break;
  case ( LVA_RVA_replay_cell2 ):
    if (True == False) {;}
    else if  (LVA_RVA_k >= (10.0)) {
      LVA_RVA_k_u = 1 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
      cstate =  LVA_RVA_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_RVA_k_init = LVA_RVA_k ;
      slope_LVA_RVA_k = 1 ;
      LVA_RVA_k_u = (slope_LVA_RVA_k * d) + LVA_RVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_RVA_replay_cell2 ;
      force_init_update = False;
      LVA_RVA_cell2_replay_latch_u = 1 ;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell1_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_RVA!\n");
      exit(1);
    }
    break;
  case ( LVA_RVA_wait_cell2 ):
    if (True == False) {;}
    else if  (LVA_RVA_k >= (10.0)) {
      LVA_RVA_k_u = 1 ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
      cstate =  LVA_RVA_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_RVA_k_init = LVA_RVA_k ;
      slope_LVA_RVA_k = 1 ;
      LVA_RVA_k_u = (slope_LVA_RVA_k * d) + LVA_RVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_RVA_wait_cell2 ;
      force_init_update = False;
      LVA_RVA_cell1_v_delayed_u = LVA_RVA_update_c1vd () ;
      LVA_RVA_cell2_v_delayed_u = LVA_RVA_update_c2vd () ;
      LVA_RVA_cell1_mode_delayed_u = LVA_RVA_update_c1md () ;
      LVA_RVA_cell2_mode_delayed_u = LVA_RVA_update_c2md () ;
      LVA_RVA_wasted_u = LVA_RVA_update_buffer_index (LVA_RVA_cell1_v,LVA_RVA_cell2_v,LVA_RVA_cell1_mode,LVA_RVA_cell2_mode) ;
      LVA_RVA_cell1_replay_latch_u = LVA_RVA_update_latch1 (LVA_RVA_cell1_mode_delayed,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_replay_latch_u = LVA_RVA_update_latch2 (LVA_RVA_cell2_mode_delayed,LVA_RVA_cell2_replay_latch_u) ;
      LVA_RVA_cell1_v_replay = LVA_RVA_update_ocell1 (LVA_RVA_cell1_v_delayed_u,LVA_RVA_cell1_replay_latch_u) ;
      LVA_RVA_cell2_v_replay = LVA_RVA_update_ocell2 (LVA_RVA_cell2_v_delayed_u,LVA_RVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_RVA!\n");
      exit(1);
    }
    break;
  }
  LVA_RVA_k = LVA_RVA_k_u;
  LVA_RVA_cell1_mode_delayed = LVA_RVA_cell1_mode_delayed_u;
  LVA_RVA_cell2_mode_delayed = LVA_RVA_cell2_mode_delayed_u;
  LVA_RVA_from_cell = LVA_RVA_from_cell_u;
  LVA_RVA_cell1_replay_latch = LVA_RVA_cell1_replay_latch_u;
  LVA_RVA_cell2_replay_latch = LVA_RVA_cell2_replay_latch_u;
  LVA_RVA_cell1_v_delayed = LVA_RVA_cell1_v_delayed_u;
  LVA_RVA_cell2_v_delayed = LVA_RVA_cell2_v_delayed_u;
  LVA_RVA_wasted = LVA_RVA_wasted_u;
  return cstate;
}