#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double RightVentricle1_v_i_0;
double RightVentricle1_v_i_1;
double RightVentricle1_voo = 0.0;
double RightVentricle1_state = 0.0;


static double  RightVentricle1_vx  =  0 ,  RightVentricle1_vy  =  0 ,  RightVentricle1_vz  =  0 ,  RightVentricle1_g  =  0 ,  RightVentricle1_v  =  0 ,  RightVentricle1_ft  =  0 ,  RightVentricle1_theta  =  0 ,  RightVentricle1_v_O  =  0 ; //the continuous vars
static double  RightVentricle1_vx_u , RightVentricle1_vy_u , RightVentricle1_vz_u , RightVentricle1_g_u , RightVentricle1_v_u , RightVentricle1_ft_u , RightVentricle1_theta_u , RightVentricle1_v_O_u ; // and their updates
static double  RightVentricle1_vx_init , RightVentricle1_vy_init , RightVentricle1_vz_init , RightVentricle1_g_init , RightVentricle1_v_init , RightVentricle1_ft_init , RightVentricle1_theta_init , RightVentricle1_v_O_init ; // and their inits
static double  slope_RightVentricle1_vx , slope_RightVentricle1_vy , slope_RightVentricle1_vz , slope_RightVentricle1_g , slope_RightVentricle1_v , slope_RightVentricle1_ft , slope_RightVentricle1_theta , slope_RightVentricle1_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { RightVentricle1_t1 , RightVentricle1_t2 , RightVentricle1_t3 , RightVentricle1_t4 }; // state declarations

enum states RightVentricle1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( RightVentricle1_t1 ):
    if (True == False) {;}
    else if  (RightVentricle1_g > (44.5)) {
      RightVentricle1_vx_u = (0.3 * RightVentricle1_v) ;
      RightVentricle1_vy_u = 0 ;
      RightVentricle1_vz_u = (0.7 * RightVentricle1_v) ;
      RightVentricle1_g_u = ((((((((RightVentricle1_v_i_0 + (- ((RightVentricle1_vx + (- RightVentricle1_vy)) + RightVentricle1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      RightVentricle1_theta_u = (RightVentricle1_v / 30.0) ;
      RightVentricle1_v_O_u = (131.1 + (- (80.1 * pow ( ((RightVentricle1_v / 30.0)) , (0.5) )))) ;
      RightVentricle1_ft_u = f (RightVentricle1_theta,4.0e-2) ;
      cstate =  RightVentricle1_t2 ;
      force_init_update = False;
    }

    else if ( RightVentricle1_v <= (44.5)
               && 
              RightVentricle1_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricle1_vx_init = RightVentricle1_vx ;
      slope_RightVentricle1_vx = (RightVentricle1_vx * -8.7) ;
      RightVentricle1_vx_u = (slope_RightVentricle1_vx * d) + RightVentricle1_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricle1_vy_init = RightVentricle1_vy ;
      slope_RightVentricle1_vy = (RightVentricle1_vy * -190.9) ;
      RightVentricle1_vy_u = (slope_RightVentricle1_vy * d) + RightVentricle1_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricle1_vz_init = RightVentricle1_vz ;
      slope_RightVentricle1_vz = (RightVentricle1_vz * -190.4) ;
      RightVentricle1_vz_u = (slope_RightVentricle1_vz * d) + RightVentricle1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricle1_t1 ;
      force_init_update = False;
      RightVentricle1_g_u = ((((((((RightVentricle1_v_i_0 + (- ((RightVentricle1_vx + (- RightVentricle1_vy)) + RightVentricle1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      RightVentricle1_v_u = ((RightVentricle1_vx + (- RightVentricle1_vy)) + RightVentricle1_vz) ;
      RightVentricle1_voo = ((RightVentricle1_vx + (- RightVentricle1_vy)) + RightVentricle1_vz) ;
      RightVentricle1_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricle1!\n");
      exit(1);
    }
    break;
  case ( RightVentricle1_t2 ):
    if (True == False) {;}
    else if  (RightVentricle1_v >= (44.5)) {
      RightVentricle1_vx_u = RightVentricle1_vx ;
      RightVentricle1_vy_u = RightVentricle1_vy ;
      RightVentricle1_vz_u = RightVentricle1_vz ;
      RightVentricle1_g_u = ((((((((RightVentricle1_v_i_0 + (- ((RightVentricle1_vx + (- RightVentricle1_vy)) + RightVentricle1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  RightVentricle1_t3 ;
      force_init_update = False;
    }
    else if  (RightVentricle1_g <= (44.5)
               && 
              RightVentricle1_v < (44.5)) {
      RightVentricle1_vx_u = RightVentricle1_vx ;
      RightVentricle1_vy_u = RightVentricle1_vy ;
      RightVentricle1_vz_u = RightVentricle1_vz ;
      RightVentricle1_g_u = ((((((((RightVentricle1_v_i_0 + (- ((RightVentricle1_vx + (- RightVentricle1_vy)) + RightVentricle1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  RightVentricle1_t1 ;
      force_init_update = False;
    }

    else if ( RightVentricle1_v < (44.5)
               && 
              RightVentricle1_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricle1_vx_init = RightVentricle1_vx ;
      slope_RightVentricle1_vx = ((RightVentricle1_vx * -23.6) + (777200.0 * RightVentricle1_g)) ;
      RightVentricle1_vx_u = (slope_RightVentricle1_vx * d) + RightVentricle1_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricle1_vy_init = RightVentricle1_vy ;
      slope_RightVentricle1_vy = ((RightVentricle1_vy * -45.5) + (58900.0 * RightVentricle1_g)) ;
      RightVentricle1_vy_u = (slope_RightVentricle1_vy * d) + RightVentricle1_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricle1_vz_init = RightVentricle1_vz ;
      slope_RightVentricle1_vz = ((RightVentricle1_vz * -12.9) + (276600.0 * RightVentricle1_g)) ;
      RightVentricle1_vz_u = (slope_RightVentricle1_vz * d) + RightVentricle1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricle1_t2 ;
      force_init_update = False;
      RightVentricle1_g_u = ((((((((RightVentricle1_v_i_0 + (- ((RightVentricle1_vx + (- RightVentricle1_vy)) + RightVentricle1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      RightVentricle1_v_u = ((RightVentricle1_vx + (- RightVentricle1_vy)) + RightVentricle1_vz) ;
      RightVentricle1_voo = ((RightVentricle1_vx + (- RightVentricle1_vy)) + RightVentricle1_vz) ;
      RightVentricle1_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricle1!\n");
      exit(1);
    }
    break;
  case ( RightVentricle1_t3 ):
    if (True == False) {;}
    else if  (RightVentricle1_v >= (131.1)) {
      RightVentricle1_vx_u = RightVentricle1_vx ;
      RightVentricle1_vy_u = RightVentricle1_vy ;
      RightVentricle1_vz_u = RightVentricle1_vz ;
      RightVentricle1_g_u = ((((((((RightVentricle1_v_i_0 + (- ((RightVentricle1_vx + (- RightVentricle1_vy)) + RightVentricle1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  RightVentricle1_t4 ;
      force_init_update = False;
    }

    else if ( RightVentricle1_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricle1_vx_init = RightVentricle1_vx ;
      slope_RightVentricle1_vx = (RightVentricle1_vx * -6.9) ;
      RightVentricle1_vx_u = (slope_RightVentricle1_vx * d) + RightVentricle1_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricle1_vy_init = RightVentricle1_vy ;
      slope_RightVentricle1_vy = (RightVentricle1_vy * 75.9) ;
      RightVentricle1_vy_u = (slope_RightVentricle1_vy * d) + RightVentricle1_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricle1_vz_init = RightVentricle1_vz ;
      slope_RightVentricle1_vz = (RightVentricle1_vz * 6826.5) ;
      RightVentricle1_vz_u = (slope_RightVentricle1_vz * d) + RightVentricle1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricle1_t3 ;
      force_init_update = False;
      RightVentricle1_g_u = ((((((((RightVentricle1_v_i_0 + (- ((RightVentricle1_vx + (- RightVentricle1_vy)) + RightVentricle1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      RightVentricle1_v_u = ((RightVentricle1_vx + (- RightVentricle1_vy)) + RightVentricle1_vz) ;
      RightVentricle1_voo = ((RightVentricle1_vx + (- RightVentricle1_vy)) + RightVentricle1_vz) ;
      RightVentricle1_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricle1!\n");
      exit(1);
    }
    break;
  case ( RightVentricle1_t4 ):
    if (True == False) {;}
    else if  (RightVentricle1_v <= (30.0)) {
      RightVentricle1_vx_u = RightVentricle1_vx ;
      RightVentricle1_vy_u = RightVentricle1_vy ;
      RightVentricle1_vz_u = RightVentricle1_vz ;
      RightVentricle1_g_u = ((((((((RightVentricle1_v_i_0 + (- ((RightVentricle1_vx + (- RightVentricle1_vy)) + RightVentricle1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  RightVentricle1_t1 ;
      force_init_update = False;
    }

    else if ( RightVentricle1_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricle1_vx_init = RightVentricle1_vx ;
      slope_RightVentricle1_vx = (RightVentricle1_vx * -33.2) ;
      RightVentricle1_vx_u = (slope_RightVentricle1_vx * d) + RightVentricle1_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricle1_vy_init = RightVentricle1_vy ;
      slope_RightVentricle1_vy = ((RightVentricle1_vy * 11.0) * RightVentricle1_ft) ;
      RightVentricle1_vy_u = (slope_RightVentricle1_vy * d) + RightVentricle1_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricle1_vz_init = RightVentricle1_vz ;
      slope_RightVentricle1_vz = ((RightVentricle1_vz * 2.0) * RightVentricle1_ft) ;
      RightVentricle1_vz_u = (slope_RightVentricle1_vz * d) + RightVentricle1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricle1_t4 ;
      force_init_update = False;
      RightVentricle1_g_u = ((((((((RightVentricle1_v_i_0 + (- ((RightVentricle1_vx + (- RightVentricle1_vy)) + RightVentricle1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      RightVentricle1_v_u = ((RightVentricle1_vx + (- RightVentricle1_vy)) + RightVentricle1_vz) ;
      RightVentricle1_voo = ((RightVentricle1_vx + (- RightVentricle1_vy)) + RightVentricle1_vz) ;
      RightVentricle1_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricle1!\n");
      exit(1);
    }
    break;
  }
  RightVentricle1_vx = RightVentricle1_vx_u;
  RightVentricle1_vy = RightVentricle1_vy_u;
  RightVentricle1_vz = RightVentricle1_vz_u;
  RightVentricle1_g = RightVentricle1_g_u;
  RightVentricle1_v = RightVentricle1_v_u;
  RightVentricle1_ft = RightVentricle1_ft_u;
  RightVentricle1_theta = RightVentricle1_theta_u;
  RightVentricle1_v_O = RightVentricle1_v_O_u;
  return cstate;
}