#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double RV_RV1_update_c1vd();
extern double RV_RV1_update_c2vd();
extern double RV_RV1_update_c1md();
extern double RV_RV1_update_c2md();
extern double RV_RV1_update_buffer_index(double,double,double,double);
extern double RV_RV1_update_latch1(double,double);
extern double RV_RV1_update_latch2(double,double);
extern double RV_RV1_update_ocell1(double,double);
extern double RV_RV1_update_ocell2(double,double);
double RV_RV1_cell1_v;
double RV_RV1_cell1_mode;
double RV_RV1_cell2_v;
double RV_RV1_cell2_mode;
double RV_RV1_cell1_v_replay = 0.0;
double RV_RV1_cell2_v_replay = 0.0;


static double  RV_RV1_k  =  0.0 ,  RV_RV1_cell1_mode_delayed  =  0.0 ,  RV_RV1_cell2_mode_delayed  =  0.0 ,  RV_RV1_from_cell  =  0.0 ,  RV_RV1_cell1_replay_latch  =  0.0 ,  RV_RV1_cell2_replay_latch  =  0.0 ,  RV_RV1_cell1_v_delayed  =  0.0 ,  RV_RV1_cell2_v_delayed  =  0.0 ,  RV_RV1_wasted  =  0.0 ; //the continuous vars
static double  RV_RV1_k_u , RV_RV1_cell1_mode_delayed_u , RV_RV1_cell2_mode_delayed_u , RV_RV1_from_cell_u , RV_RV1_cell1_replay_latch_u , RV_RV1_cell2_replay_latch_u , RV_RV1_cell1_v_delayed_u , RV_RV1_cell2_v_delayed_u , RV_RV1_wasted_u ; // and their updates
static double  RV_RV1_k_init , RV_RV1_cell1_mode_delayed_init , RV_RV1_cell2_mode_delayed_init , RV_RV1_from_cell_init , RV_RV1_cell1_replay_latch_init , RV_RV1_cell2_replay_latch_init , RV_RV1_cell1_v_delayed_init , RV_RV1_cell2_v_delayed_init , RV_RV1_wasted_init ; // and their inits
static double  slope_RV_RV1_k , slope_RV_RV1_cell1_mode_delayed , slope_RV_RV1_cell2_mode_delayed , slope_RV_RV1_from_cell , slope_RV_RV1_cell1_replay_latch , slope_RV_RV1_cell2_replay_latch , slope_RV_RV1_cell1_v_delayed , slope_RV_RV1_cell2_v_delayed , slope_RV_RV1_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { RV_RV1_idle , RV_RV1_annhilate , RV_RV1_previous_drection1 , RV_RV1_previous_direction2 , RV_RV1_wait_cell1 , RV_RV1_replay_cell1 , RV_RV1_replay_cell2 , RV_RV1_wait_cell2 }; // state declarations

enum states RV_RV1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( RV_RV1_idle ):
    if (True == False) {;}
    else if  (RV_RV1_cell2_mode == (2.0) && (RV_RV1_cell1_mode != (2.0))) {
      RV_RV1_k_u = 1 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
      cstate =  RV_RV1_previous_direction2 ;
      force_init_update = False;
    }
    else if  (RV_RV1_cell1_mode == (2.0) && (RV_RV1_cell2_mode != (2.0))) {
      RV_RV1_k_u = 1 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
      cstate =  RV_RV1_previous_drection1 ;
      force_init_update = False;
    }
    else if  (RV_RV1_cell1_mode == (2.0) && (RV_RV1_cell2_mode == (2.0))) {
      RV_RV1_k_u = 1 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
      cstate =  RV_RV1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RV_RV1_k_init = RV_RV1_k ;
      slope_RV_RV1_k = 1 ;
      RV_RV1_k_u = (slope_RV_RV1_k * d) + RV_RV1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RV_RV1_idle ;
      force_init_update = False;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell1_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RV_RV1!\n");
      exit(1);
    }
    break;
  case ( RV_RV1_annhilate ):
    if (True == False) {;}
    else if  (RV_RV1_cell1_mode != (2.0) && (RV_RV1_cell2_mode != (2.0))) {
      RV_RV1_k_u = 1 ;
      RV_RV1_from_cell_u = 0 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
      cstate =  RV_RV1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RV_RV1_k_init = RV_RV1_k ;
      slope_RV_RV1_k = 1 ;
      RV_RV1_k_u = (slope_RV_RV1_k * d) + RV_RV1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RV_RV1_annhilate ;
      force_init_update = False;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell1_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RV_RV1!\n");
      exit(1);
    }
    break;
  case ( RV_RV1_previous_drection1 ):
    if (True == False) {;}
    else if  (RV_RV1_from_cell == (1.0)) {
      RV_RV1_k_u = 1 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
      cstate =  RV_RV1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RV_RV1_from_cell == (0.0)) {
      RV_RV1_k_u = 1 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
      cstate =  RV_RV1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RV_RV1_from_cell == (2.0) && (RV_RV1_cell2_mode_delayed == (0.0))) {
      RV_RV1_k_u = 1 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
      cstate =  RV_RV1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RV_RV1_from_cell == (2.0) && (RV_RV1_cell2_mode_delayed != (0.0))) {
      RV_RV1_k_u = 1 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
      cstate =  RV_RV1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RV_RV1_k_init = RV_RV1_k ;
      slope_RV_RV1_k = 1 ;
      RV_RV1_k_u = (slope_RV_RV1_k * d) + RV_RV1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RV_RV1_previous_drection1 ;
      force_init_update = False;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell1_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RV_RV1!\n");
      exit(1);
    }
    break;
  case ( RV_RV1_previous_direction2 ):
    if (True == False) {;}
    else if  (RV_RV1_from_cell == (1.0) && (RV_RV1_cell1_mode_delayed != (0.0))) {
      RV_RV1_k_u = 1 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
      cstate =  RV_RV1_annhilate ;
      force_init_update = False;
    }
    else if  (RV_RV1_from_cell == (2.0)) {
      RV_RV1_k_u = 1 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
      cstate =  RV_RV1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (RV_RV1_from_cell == (0.0)) {
      RV_RV1_k_u = 1 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
      cstate =  RV_RV1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (RV_RV1_from_cell == (1.0) && (RV_RV1_cell1_mode_delayed == (0.0))) {
      RV_RV1_k_u = 1 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
      cstate =  RV_RV1_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RV_RV1_k_init = RV_RV1_k ;
      slope_RV_RV1_k = 1 ;
      RV_RV1_k_u = (slope_RV_RV1_k * d) + RV_RV1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RV_RV1_previous_direction2 ;
      force_init_update = False;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell1_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RV_RV1!\n");
      exit(1);
    }
    break;
  case ( RV_RV1_wait_cell1 ):
    if (True == False) {;}
    else if  (RV_RV1_cell2_mode == (2.0)) {
      RV_RV1_k_u = 1 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
      cstate =  RV_RV1_annhilate ;
      force_init_update = False;
    }
    else if  (RV_RV1_k >= (20.0)) {
      RV_RV1_from_cell_u = 1 ;
      RV_RV1_cell1_replay_latch_u = 1 ;
      RV_RV1_k_u = 1 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
      cstate =  RV_RV1_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RV_RV1_k_init = RV_RV1_k ;
      slope_RV_RV1_k = 1 ;
      RV_RV1_k_u = (slope_RV_RV1_k * d) + RV_RV1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RV_RV1_wait_cell1 ;
      force_init_update = False;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell1_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RV_RV1!\n");
      exit(1);
    }
    break;
  case ( RV_RV1_replay_cell1 ):
    if (True == False) {;}
    else if  (RV_RV1_cell1_mode == (2.0)) {
      RV_RV1_k_u = 1 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
      cstate =  RV_RV1_annhilate ;
      force_init_update = False;
    }
    else if  (RV_RV1_k >= (20.0)) {
      RV_RV1_from_cell_u = 2 ;
      RV_RV1_cell2_replay_latch_u = 1 ;
      RV_RV1_k_u = 1 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
      cstate =  RV_RV1_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RV_RV1_k_init = RV_RV1_k ;
      slope_RV_RV1_k = 1 ;
      RV_RV1_k_u = (slope_RV_RV1_k * d) + RV_RV1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RV_RV1_replay_cell1 ;
      force_init_update = False;
      RV_RV1_cell1_replay_latch_u = 1 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell1_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RV_RV1!\n");
      exit(1);
    }
    break;
  case ( RV_RV1_replay_cell2 ):
    if (True == False) {;}
    else if  (RV_RV1_k >= (10.0)) {
      RV_RV1_k_u = 1 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
      cstate =  RV_RV1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RV_RV1_k_init = RV_RV1_k ;
      slope_RV_RV1_k = 1 ;
      RV_RV1_k_u = (slope_RV_RV1_k * d) + RV_RV1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RV_RV1_replay_cell2 ;
      force_init_update = False;
      RV_RV1_cell2_replay_latch_u = 1 ;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell1_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RV_RV1!\n");
      exit(1);
    }
    break;
  case ( RV_RV1_wait_cell2 ):
    if (True == False) {;}
    else if  (RV_RV1_k >= (10.0)) {
      RV_RV1_k_u = 1 ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
      cstate =  RV_RV1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RV_RV1_k_init = RV_RV1_k ;
      slope_RV_RV1_k = 1 ;
      RV_RV1_k_u = (slope_RV_RV1_k * d) + RV_RV1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RV_RV1_wait_cell2 ;
      force_init_update = False;
      RV_RV1_cell1_v_delayed_u = RV_RV1_update_c1vd () ;
      RV_RV1_cell2_v_delayed_u = RV_RV1_update_c2vd () ;
      RV_RV1_cell1_mode_delayed_u = RV_RV1_update_c1md () ;
      RV_RV1_cell2_mode_delayed_u = RV_RV1_update_c2md () ;
      RV_RV1_wasted_u = RV_RV1_update_buffer_index (RV_RV1_cell1_v,RV_RV1_cell2_v,RV_RV1_cell1_mode,RV_RV1_cell2_mode) ;
      RV_RV1_cell1_replay_latch_u = RV_RV1_update_latch1 (RV_RV1_cell1_mode_delayed,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_replay_latch_u = RV_RV1_update_latch2 (RV_RV1_cell2_mode_delayed,RV_RV1_cell2_replay_latch_u) ;
      RV_RV1_cell1_v_replay = RV_RV1_update_ocell1 (RV_RV1_cell1_v_delayed_u,RV_RV1_cell1_replay_latch_u) ;
      RV_RV1_cell2_v_replay = RV_RV1_update_ocell2 (RV_RV1_cell2_v_delayed_u,RV_RV1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RV_RV1!\n");
      exit(1);
    }
    break;
  }
  RV_RV1_k = RV_RV1_k_u;
  RV_RV1_cell1_mode_delayed = RV_RV1_cell1_mode_delayed_u;
  RV_RV1_cell2_mode_delayed = RV_RV1_cell2_mode_delayed_u;
  RV_RV1_from_cell = RV_RV1_from_cell_u;
  RV_RV1_cell1_replay_latch = RV_RV1_cell1_replay_latch_u;
  RV_RV1_cell2_replay_latch = RV_RV1_cell2_replay_latch_u;
  RV_RV1_cell1_v_delayed = RV_RV1_cell1_v_delayed_u;
  RV_RV1_cell2_v_delayed = RV_RV1_cell2_v_delayed_u;
  RV_RV1_wasted = RV_RV1_wasted_u;
  return cstate;
}