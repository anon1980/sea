#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double RBB1_RVA_update_c1vd();
extern double RBB1_RVA_update_c2vd();
extern double RBB1_RVA_update_c1md();
extern double RBB1_RVA_update_c2md();
extern double RBB1_RVA_update_buffer_index(double,double,double,double);
extern double RBB1_RVA_update_latch1(double,double);
extern double RBB1_RVA_update_latch2(double,double);
extern double RBB1_RVA_update_ocell1(double,double);
extern double RBB1_RVA_update_ocell2(double,double);
double RBB1_RVA_cell1_v;
double RBB1_RVA_cell1_mode;
double RBB1_RVA_cell2_v;
double RBB1_RVA_cell2_mode;
double RBB1_RVA_cell1_v_replay = 0.0;
double RBB1_RVA_cell2_v_replay = 0.0;


static double  RBB1_RVA_k  =  0.0 ,  RBB1_RVA_cell1_mode_delayed  =  0.0 ,  RBB1_RVA_cell2_mode_delayed  =  0.0 ,  RBB1_RVA_from_cell  =  0.0 ,  RBB1_RVA_cell1_replay_latch  =  0.0 ,  RBB1_RVA_cell2_replay_latch  =  0.0 ,  RBB1_RVA_cell1_v_delayed  =  0.0 ,  RBB1_RVA_cell2_v_delayed  =  0.0 ,  RBB1_RVA_wasted  =  0.0 ; //the continuous vars
static double  RBB1_RVA_k_u , RBB1_RVA_cell1_mode_delayed_u , RBB1_RVA_cell2_mode_delayed_u , RBB1_RVA_from_cell_u , RBB1_RVA_cell1_replay_latch_u , RBB1_RVA_cell2_replay_latch_u , RBB1_RVA_cell1_v_delayed_u , RBB1_RVA_cell2_v_delayed_u , RBB1_RVA_wasted_u ; // and their updates
static double  RBB1_RVA_k_init , RBB1_RVA_cell1_mode_delayed_init , RBB1_RVA_cell2_mode_delayed_init , RBB1_RVA_from_cell_init , RBB1_RVA_cell1_replay_latch_init , RBB1_RVA_cell2_replay_latch_init , RBB1_RVA_cell1_v_delayed_init , RBB1_RVA_cell2_v_delayed_init , RBB1_RVA_wasted_init ; // and their inits
static double  slope_RBB1_RVA_k , slope_RBB1_RVA_cell1_mode_delayed , slope_RBB1_RVA_cell2_mode_delayed , slope_RBB1_RVA_from_cell , slope_RBB1_RVA_cell1_replay_latch , slope_RBB1_RVA_cell2_replay_latch , slope_RBB1_RVA_cell1_v_delayed , slope_RBB1_RVA_cell2_v_delayed , slope_RBB1_RVA_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { RBB1_RVA_idle , RBB1_RVA_annhilate , RBB1_RVA_previous_drection1 , RBB1_RVA_previous_direction2 , RBB1_RVA_wait_cell1 , RBB1_RVA_replay_cell1 , RBB1_RVA_replay_cell2 , RBB1_RVA_wait_cell2 }; // state declarations

enum states RBB1_RVA (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( RBB1_RVA_idle ):
    if (True == False) {;}
    else if  (RBB1_RVA_cell2_mode == (2.0) && (RBB1_RVA_cell1_mode != (2.0))) {
      RBB1_RVA_k_u = 1 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
      cstate =  RBB1_RVA_previous_direction2 ;
      force_init_update = False;
    }
    else if  (RBB1_RVA_cell1_mode == (2.0) && (RBB1_RVA_cell2_mode != (2.0))) {
      RBB1_RVA_k_u = 1 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
      cstate =  RBB1_RVA_previous_drection1 ;
      force_init_update = False;
    }
    else if  (RBB1_RVA_cell1_mode == (2.0) && (RBB1_RVA_cell2_mode == (2.0))) {
      RBB1_RVA_k_u = 1 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
      cstate =  RBB1_RVA_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RBB1_RVA_k_init = RBB1_RVA_k ;
      slope_RBB1_RVA_k = 1 ;
      RBB1_RVA_k_u = (slope_RBB1_RVA_k * d) + RBB1_RVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RBB1_RVA_idle ;
      force_init_update = False;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell1_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB1_RVA!\n");
      exit(1);
    }
    break;
  case ( RBB1_RVA_annhilate ):
    if (True == False) {;}
    else if  (RBB1_RVA_cell1_mode != (2.0) && (RBB1_RVA_cell2_mode != (2.0))) {
      RBB1_RVA_k_u = 1 ;
      RBB1_RVA_from_cell_u = 0 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
      cstate =  RBB1_RVA_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RBB1_RVA_k_init = RBB1_RVA_k ;
      slope_RBB1_RVA_k = 1 ;
      RBB1_RVA_k_u = (slope_RBB1_RVA_k * d) + RBB1_RVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RBB1_RVA_annhilate ;
      force_init_update = False;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell1_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB1_RVA!\n");
      exit(1);
    }
    break;
  case ( RBB1_RVA_previous_drection1 ):
    if (True == False) {;}
    else if  (RBB1_RVA_from_cell == (1.0)) {
      RBB1_RVA_k_u = 1 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
      cstate =  RBB1_RVA_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RBB1_RVA_from_cell == (0.0)) {
      RBB1_RVA_k_u = 1 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
      cstate =  RBB1_RVA_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RBB1_RVA_from_cell == (2.0) && (RBB1_RVA_cell2_mode_delayed == (0.0))) {
      RBB1_RVA_k_u = 1 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
      cstate =  RBB1_RVA_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RBB1_RVA_from_cell == (2.0) && (RBB1_RVA_cell2_mode_delayed != (0.0))) {
      RBB1_RVA_k_u = 1 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
      cstate =  RBB1_RVA_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RBB1_RVA_k_init = RBB1_RVA_k ;
      slope_RBB1_RVA_k = 1 ;
      RBB1_RVA_k_u = (slope_RBB1_RVA_k * d) + RBB1_RVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RBB1_RVA_previous_drection1 ;
      force_init_update = False;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell1_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB1_RVA!\n");
      exit(1);
    }
    break;
  case ( RBB1_RVA_previous_direction2 ):
    if (True == False) {;}
    else if  (RBB1_RVA_from_cell == (1.0) && (RBB1_RVA_cell1_mode_delayed != (0.0))) {
      RBB1_RVA_k_u = 1 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
      cstate =  RBB1_RVA_annhilate ;
      force_init_update = False;
    }
    else if  (RBB1_RVA_from_cell == (2.0)) {
      RBB1_RVA_k_u = 1 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
      cstate =  RBB1_RVA_replay_cell1 ;
      force_init_update = False;
    }
    else if  (RBB1_RVA_from_cell == (0.0)) {
      RBB1_RVA_k_u = 1 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
      cstate =  RBB1_RVA_replay_cell1 ;
      force_init_update = False;
    }
    else if  (RBB1_RVA_from_cell == (1.0) && (RBB1_RVA_cell1_mode_delayed == (0.0))) {
      RBB1_RVA_k_u = 1 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
      cstate =  RBB1_RVA_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RBB1_RVA_k_init = RBB1_RVA_k ;
      slope_RBB1_RVA_k = 1 ;
      RBB1_RVA_k_u = (slope_RBB1_RVA_k * d) + RBB1_RVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RBB1_RVA_previous_direction2 ;
      force_init_update = False;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell1_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB1_RVA!\n");
      exit(1);
    }
    break;
  case ( RBB1_RVA_wait_cell1 ):
    if (True == False) {;}
    else if  (RBB1_RVA_cell2_mode == (2.0)) {
      RBB1_RVA_k_u = 1 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
      cstate =  RBB1_RVA_annhilate ;
      force_init_update = False;
    }
    else if  (RBB1_RVA_k >= (5.0)) {
      RBB1_RVA_from_cell_u = 1 ;
      RBB1_RVA_cell1_replay_latch_u = 1 ;
      RBB1_RVA_k_u = 1 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
      cstate =  RBB1_RVA_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RBB1_RVA_k_init = RBB1_RVA_k ;
      slope_RBB1_RVA_k = 1 ;
      RBB1_RVA_k_u = (slope_RBB1_RVA_k * d) + RBB1_RVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RBB1_RVA_wait_cell1 ;
      force_init_update = False;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell1_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB1_RVA!\n");
      exit(1);
    }
    break;
  case ( RBB1_RVA_replay_cell1 ):
    if (True == False) {;}
    else if  (RBB1_RVA_cell1_mode == (2.0)) {
      RBB1_RVA_k_u = 1 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
      cstate =  RBB1_RVA_annhilate ;
      force_init_update = False;
    }
    else if  (RBB1_RVA_k >= (5.0)) {
      RBB1_RVA_from_cell_u = 2 ;
      RBB1_RVA_cell2_replay_latch_u = 1 ;
      RBB1_RVA_k_u = 1 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
      cstate =  RBB1_RVA_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RBB1_RVA_k_init = RBB1_RVA_k ;
      slope_RBB1_RVA_k = 1 ;
      RBB1_RVA_k_u = (slope_RBB1_RVA_k * d) + RBB1_RVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RBB1_RVA_replay_cell1 ;
      force_init_update = False;
      RBB1_RVA_cell1_replay_latch_u = 1 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell1_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB1_RVA!\n");
      exit(1);
    }
    break;
  case ( RBB1_RVA_replay_cell2 ):
    if (True == False) {;}
    else if  (RBB1_RVA_k >= (10.0)) {
      RBB1_RVA_k_u = 1 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
      cstate =  RBB1_RVA_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RBB1_RVA_k_init = RBB1_RVA_k ;
      slope_RBB1_RVA_k = 1 ;
      RBB1_RVA_k_u = (slope_RBB1_RVA_k * d) + RBB1_RVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RBB1_RVA_replay_cell2 ;
      force_init_update = False;
      RBB1_RVA_cell2_replay_latch_u = 1 ;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell1_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB1_RVA!\n");
      exit(1);
    }
    break;
  case ( RBB1_RVA_wait_cell2 ):
    if (True == False) {;}
    else if  (RBB1_RVA_k >= (10.0)) {
      RBB1_RVA_k_u = 1 ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
      cstate =  RBB1_RVA_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RBB1_RVA_k_init = RBB1_RVA_k ;
      slope_RBB1_RVA_k = 1 ;
      RBB1_RVA_k_u = (slope_RBB1_RVA_k * d) + RBB1_RVA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RBB1_RVA_wait_cell2 ;
      force_init_update = False;
      RBB1_RVA_cell1_v_delayed_u = RBB1_RVA_update_c1vd () ;
      RBB1_RVA_cell2_v_delayed_u = RBB1_RVA_update_c2vd () ;
      RBB1_RVA_cell1_mode_delayed_u = RBB1_RVA_update_c1md () ;
      RBB1_RVA_cell2_mode_delayed_u = RBB1_RVA_update_c2md () ;
      RBB1_RVA_wasted_u = RBB1_RVA_update_buffer_index (RBB1_RVA_cell1_v,RBB1_RVA_cell2_v,RBB1_RVA_cell1_mode,RBB1_RVA_cell2_mode) ;
      RBB1_RVA_cell1_replay_latch_u = RBB1_RVA_update_latch1 (RBB1_RVA_cell1_mode_delayed,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_replay_latch_u = RBB1_RVA_update_latch2 (RBB1_RVA_cell2_mode_delayed,RBB1_RVA_cell2_replay_latch_u) ;
      RBB1_RVA_cell1_v_replay = RBB1_RVA_update_ocell1 (RBB1_RVA_cell1_v_delayed_u,RBB1_RVA_cell1_replay_latch_u) ;
      RBB1_RVA_cell2_v_replay = RBB1_RVA_update_ocell2 (RBB1_RVA_cell2_v_delayed_u,RBB1_RVA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB1_RVA!\n");
      exit(1);
    }
    break;
  }
  RBB1_RVA_k = RBB1_RVA_k_u;
  RBB1_RVA_cell1_mode_delayed = RBB1_RVA_cell1_mode_delayed_u;
  RBB1_RVA_cell2_mode_delayed = RBB1_RVA_cell2_mode_delayed_u;
  RBB1_RVA_from_cell = RBB1_RVA_from_cell_u;
  RBB1_RVA_cell1_replay_latch = RBB1_RVA_cell1_replay_latch_u;
  RBB1_RVA_cell2_replay_latch = RBB1_RVA_cell2_replay_latch_u;
  RBB1_RVA_cell1_v_delayed = RBB1_RVA_cell1_v_delayed_u;
  RBB1_RVA_cell2_v_delayed = RBB1_RVA_cell2_v_delayed_u;
  RBB1_RVA_wasted = RBB1_RVA_wasted_u;
  return cstate;
}