#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double BB_LA_update_c1vd();
extern double BB_LA_update_c2vd();
extern double BB_LA_update_c1md();
extern double BB_LA_update_c2md();
extern double BB_LA_update_buffer_index(double,double,double,double);
extern double BB_LA_update_latch1(double,double);
extern double BB_LA_update_latch2(double,double);
extern double BB_LA_update_ocell1(double,double);
extern double BB_LA_update_ocell2(double,double);
double BB_LA_cell1_v;
double BB_LA_cell1_mode;
double BB_LA_cell2_v;
double BB_LA_cell2_mode;
double BB_LA_cell1_v_replay = 0.0;
double BB_LA_cell2_v_replay = 0.0;


static double  BB_LA_k  =  0.0 ,  BB_LA_cell1_mode_delayed  =  0.0 ,  BB_LA_cell2_mode_delayed  =  0.0 ,  BB_LA_from_cell  =  0.0 ,  BB_LA_cell1_replay_latch  =  0.0 ,  BB_LA_cell2_replay_latch  =  0.0 ,  BB_LA_cell1_v_delayed  =  0.0 ,  BB_LA_cell2_v_delayed  =  0.0 ,  BB_LA_wasted  =  0.0 ; //the continuous vars
static double  BB_LA_k_u , BB_LA_cell1_mode_delayed_u , BB_LA_cell2_mode_delayed_u , BB_LA_from_cell_u , BB_LA_cell1_replay_latch_u , BB_LA_cell2_replay_latch_u , BB_LA_cell1_v_delayed_u , BB_LA_cell2_v_delayed_u , BB_LA_wasted_u ; // and their updates
static double  BB_LA_k_init , BB_LA_cell1_mode_delayed_init , BB_LA_cell2_mode_delayed_init , BB_LA_from_cell_init , BB_LA_cell1_replay_latch_init , BB_LA_cell2_replay_latch_init , BB_LA_cell1_v_delayed_init , BB_LA_cell2_v_delayed_init , BB_LA_wasted_init ; // and their inits
static double  slope_BB_LA_k , slope_BB_LA_cell1_mode_delayed , slope_BB_LA_cell2_mode_delayed , slope_BB_LA_from_cell , slope_BB_LA_cell1_replay_latch , slope_BB_LA_cell2_replay_latch , slope_BB_LA_cell1_v_delayed , slope_BB_LA_cell2_v_delayed , slope_BB_LA_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { BB_LA_idle , BB_LA_annhilate , BB_LA_previous_drection1 , BB_LA_previous_direction2 , BB_LA_wait_cell1 , BB_LA_replay_cell1 , BB_LA_replay_cell2 , BB_LA_wait_cell2 }; // state declarations

enum states BB_LA (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( BB_LA_idle ):
    if (True == False) {;}
    else if  (BB_LA_cell2_mode == (2.0) && (BB_LA_cell1_mode != (2.0))) {
      BB_LA_k_u = 1 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
      cstate =  BB_LA_previous_direction2 ;
      force_init_update = False;
    }
    else if  (BB_LA_cell1_mode == (2.0) && (BB_LA_cell2_mode != (2.0))) {
      BB_LA_k_u = 1 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
      cstate =  BB_LA_previous_drection1 ;
      force_init_update = False;
    }
    else if  (BB_LA_cell1_mode == (2.0) && (BB_LA_cell2_mode == (2.0))) {
      BB_LA_k_u = 1 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
      cstate =  BB_LA_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) BB_LA_k_init = BB_LA_k ;
      slope_BB_LA_k = 1 ;
      BB_LA_k_u = (slope_BB_LA_k * d) + BB_LA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  BB_LA_idle ;
      force_init_update = False;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell1_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BB_LA!\n");
      exit(1);
    }
    break;
  case ( BB_LA_annhilate ):
    if (True == False) {;}
    else if  (BB_LA_cell1_mode != (2.0) && (BB_LA_cell2_mode != (2.0))) {
      BB_LA_k_u = 1 ;
      BB_LA_from_cell_u = 0 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
      cstate =  BB_LA_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) BB_LA_k_init = BB_LA_k ;
      slope_BB_LA_k = 1 ;
      BB_LA_k_u = (slope_BB_LA_k * d) + BB_LA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  BB_LA_annhilate ;
      force_init_update = False;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell1_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BB_LA!\n");
      exit(1);
    }
    break;
  case ( BB_LA_previous_drection1 ):
    if (True == False) {;}
    else if  (BB_LA_from_cell == (1.0)) {
      BB_LA_k_u = 1 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
      cstate =  BB_LA_wait_cell1 ;
      force_init_update = False;
    }
    else if  (BB_LA_from_cell == (0.0)) {
      BB_LA_k_u = 1 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
      cstate =  BB_LA_wait_cell1 ;
      force_init_update = False;
    }
    else if  (BB_LA_from_cell == (2.0) && (BB_LA_cell2_mode_delayed == (0.0))) {
      BB_LA_k_u = 1 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
      cstate =  BB_LA_wait_cell1 ;
      force_init_update = False;
    }
    else if  (BB_LA_from_cell == (2.0) && (BB_LA_cell2_mode_delayed != (0.0))) {
      BB_LA_k_u = 1 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
      cstate =  BB_LA_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) BB_LA_k_init = BB_LA_k ;
      slope_BB_LA_k = 1 ;
      BB_LA_k_u = (slope_BB_LA_k * d) + BB_LA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  BB_LA_previous_drection1 ;
      force_init_update = False;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell1_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BB_LA!\n");
      exit(1);
    }
    break;
  case ( BB_LA_previous_direction2 ):
    if (True == False) {;}
    else if  (BB_LA_from_cell == (1.0) && (BB_LA_cell1_mode_delayed != (0.0))) {
      BB_LA_k_u = 1 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
      cstate =  BB_LA_annhilate ;
      force_init_update = False;
    }
    else if  (BB_LA_from_cell == (2.0)) {
      BB_LA_k_u = 1 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
      cstate =  BB_LA_replay_cell1 ;
      force_init_update = False;
    }
    else if  (BB_LA_from_cell == (0.0)) {
      BB_LA_k_u = 1 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
      cstate =  BB_LA_replay_cell1 ;
      force_init_update = False;
    }
    else if  (BB_LA_from_cell == (1.0) && (BB_LA_cell1_mode_delayed == (0.0))) {
      BB_LA_k_u = 1 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
      cstate =  BB_LA_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) BB_LA_k_init = BB_LA_k ;
      slope_BB_LA_k = 1 ;
      BB_LA_k_u = (slope_BB_LA_k * d) + BB_LA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  BB_LA_previous_direction2 ;
      force_init_update = False;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell1_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BB_LA!\n");
      exit(1);
    }
    break;
  case ( BB_LA_wait_cell1 ):
    if (True == False) {;}
    else if  (BB_LA_cell2_mode == (2.0)) {
      BB_LA_k_u = 1 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
      cstate =  BB_LA_annhilate ;
      force_init_update = False;
    }
    else if  (BB_LA_k >= (30.0)) {
      BB_LA_from_cell_u = 1 ;
      BB_LA_cell1_replay_latch_u = 1 ;
      BB_LA_k_u = 1 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
      cstate =  BB_LA_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) BB_LA_k_init = BB_LA_k ;
      slope_BB_LA_k = 1 ;
      BB_LA_k_u = (slope_BB_LA_k * d) + BB_LA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  BB_LA_wait_cell1 ;
      force_init_update = False;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell1_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BB_LA!\n");
      exit(1);
    }
    break;
  case ( BB_LA_replay_cell1 ):
    if (True == False) {;}
    else if  (BB_LA_cell1_mode == (2.0)) {
      BB_LA_k_u = 1 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
      cstate =  BB_LA_annhilate ;
      force_init_update = False;
    }
    else if  (BB_LA_k >= (30.0)) {
      BB_LA_from_cell_u = 2 ;
      BB_LA_cell2_replay_latch_u = 1 ;
      BB_LA_k_u = 1 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
      cstate =  BB_LA_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) BB_LA_k_init = BB_LA_k ;
      slope_BB_LA_k = 1 ;
      BB_LA_k_u = (slope_BB_LA_k * d) + BB_LA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  BB_LA_replay_cell1 ;
      force_init_update = False;
      BB_LA_cell1_replay_latch_u = 1 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell1_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BB_LA!\n");
      exit(1);
    }
    break;
  case ( BB_LA_replay_cell2 ):
    if (True == False) {;}
    else if  (BB_LA_k >= (10.0)) {
      BB_LA_k_u = 1 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
      cstate =  BB_LA_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) BB_LA_k_init = BB_LA_k ;
      slope_BB_LA_k = 1 ;
      BB_LA_k_u = (slope_BB_LA_k * d) + BB_LA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  BB_LA_replay_cell2 ;
      force_init_update = False;
      BB_LA_cell2_replay_latch_u = 1 ;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell1_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BB_LA!\n");
      exit(1);
    }
    break;
  case ( BB_LA_wait_cell2 ):
    if (True == False) {;}
    else if  (BB_LA_k >= (10.0)) {
      BB_LA_k_u = 1 ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
      cstate =  BB_LA_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) BB_LA_k_init = BB_LA_k ;
      slope_BB_LA_k = 1 ;
      BB_LA_k_u = (slope_BB_LA_k * d) + BB_LA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  BB_LA_wait_cell2 ;
      force_init_update = False;
      BB_LA_cell1_v_delayed_u = BB_LA_update_c1vd () ;
      BB_LA_cell2_v_delayed_u = BB_LA_update_c2vd () ;
      BB_LA_cell1_mode_delayed_u = BB_LA_update_c1md () ;
      BB_LA_cell2_mode_delayed_u = BB_LA_update_c2md () ;
      BB_LA_wasted_u = BB_LA_update_buffer_index (BB_LA_cell1_v,BB_LA_cell2_v,BB_LA_cell1_mode,BB_LA_cell2_mode) ;
      BB_LA_cell1_replay_latch_u = BB_LA_update_latch1 (BB_LA_cell1_mode_delayed,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_replay_latch_u = BB_LA_update_latch2 (BB_LA_cell2_mode_delayed,BB_LA_cell2_replay_latch_u) ;
      BB_LA_cell1_v_replay = BB_LA_update_ocell1 (BB_LA_cell1_v_delayed_u,BB_LA_cell1_replay_latch_u) ;
      BB_LA_cell2_v_replay = BB_LA_update_ocell2 (BB_LA_cell2_v_delayed_u,BB_LA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BB_LA!\n");
      exit(1);
    }
    break;
  }
  BB_LA_k = BB_LA_k_u;
  BB_LA_cell1_mode_delayed = BB_LA_cell1_mode_delayed_u;
  BB_LA_cell2_mode_delayed = BB_LA_cell2_mode_delayed_u;
  BB_LA_from_cell = BB_LA_from_cell_u;
  BB_LA_cell1_replay_latch = BB_LA_cell1_replay_latch_u;
  BB_LA_cell2_replay_latch = BB_LA_cell2_replay_latch_u;
  BB_LA_cell1_v_delayed = BB_LA_cell1_v_delayed_u;
  BB_LA_cell2_v_delayed = BB_LA_cell2_v_delayed_u;
  BB_LA_wasted = BB_LA_wasted_u;
  return cstate;
}