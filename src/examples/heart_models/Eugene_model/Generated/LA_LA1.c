#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double LA_LA1_update_c1vd();
extern double LA_LA1_update_c2vd();
extern double LA_LA1_update_c1md();
extern double LA_LA1_update_c2md();
extern double LA_LA1_update_buffer_index(double,double,double,double);
extern double LA_LA1_update_latch1(double,double);
extern double LA_LA1_update_latch2(double,double);
extern double LA_LA1_update_ocell1(double,double);
extern double LA_LA1_update_ocell2(double,double);
double LA_LA1_cell1_v;
double LA_LA1_cell1_mode;
double LA_LA1_cell2_v;
double LA_LA1_cell2_mode;
double LA_LA1_cell1_v_replay = 0.0;
double LA_LA1_cell2_v_replay = 0.0;


static double  LA_LA1_k  =  0.0 ,  LA_LA1_cell1_mode_delayed  =  0.0 ,  LA_LA1_cell2_mode_delayed  =  0.0 ,  LA_LA1_from_cell  =  0.0 ,  LA_LA1_cell1_replay_latch  =  0.0 ,  LA_LA1_cell2_replay_latch  =  0.0 ,  LA_LA1_cell1_v_delayed  =  0.0 ,  LA_LA1_cell2_v_delayed  =  0.0 ,  LA_LA1_wasted  =  0.0 ; //the continuous vars
static double  LA_LA1_k_u , LA_LA1_cell1_mode_delayed_u , LA_LA1_cell2_mode_delayed_u , LA_LA1_from_cell_u , LA_LA1_cell1_replay_latch_u , LA_LA1_cell2_replay_latch_u , LA_LA1_cell1_v_delayed_u , LA_LA1_cell2_v_delayed_u , LA_LA1_wasted_u ; // and their updates
static double  LA_LA1_k_init , LA_LA1_cell1_mode_delayed_init , LA_LA1_cell2_mode_delayed_init , LA_LA1_from_cell_init , LA_LA1_cell1_replay_latch_init , LA_LA1_cell2_replay_latch_init , LA_LA1_cell1_v_delayed_init , LA_LA1_cell2_v_delayed_init , LA_LA1_wasted_init ; // and their inits
static double  slope_LA_LA1_k , slope_LA_LA1_cell1_mode_delayed , slope_LA_LA1_cell2_mode_delayed , slope_LA_LA1_from_cell , slope_LA_LA1_cell1_replay_latch , slope_LA_LA1_cell2_replay_latch , slope_LA_LA1_cell1_v_delayed , slope_LA_LA1_cell2_v_delayed , slope_LA_LA1_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { LA_LA1_idle , LA_LA1_annhilate , LA_LA1_previous_drection1 , LA_LA1_previous_direction2 , LA_LA1_wait_cell1 , LA_LA1_replay_cell1 , LA_LA1_replay_cell2 , LA_LA1_wait_cell2 }; // state declarations

enum states LA_LA1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( LA_LA1_idle ):
    if (True == False) {;}
    else if  (LA_LA1_cell2_mode == (2.0) && (LA_LA1_cell1_mode != (2.0))) {
      LA_LA1_k_u = 1 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
      cstate =  LA_LA1_previous_direction2 ;
      force_init_update = False;
    }
    else if  (LA_LA1_cell1_mode == (2.0) && (LA_LA1_cell2_mode != (2.0))) {
      LA_LA1_k_u = 1 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
      cstate =  LA_LA1_previous_drection1 ;
      force_init_update = False;
    }
    else if  (LA_LA1_cell1_mode == (2.0) && (LA_LA1_cell2_mode == (2.0))) {
      LA_LA1_k_u = 1 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
      cstate =  LA_LA1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LA_LA1_k_init = LA_LA1_k ;
      slope_LA_LA1_k = 1 ;
      LA_LA1_k_u = (slope_LA_LA1_k * d) + LA_LA1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LA_LA1_idle ;
      force_init_update = False;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell1_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LA_LA1!\n");
      exit(1);
    }
    break;
  case ( LA_LA1_annhilate ):
    if (True == False) {;}
    else if  (LA_LA1_cell1_mode != (2.0) && (LA_LA1_cell2_mode != (2.0))) {
      LA_LA1_k_u = 1 ;
      LA_LA1_from_cell_u = 0 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
      cstate =  LA_LA1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LA_LA1_k_init = LA_LA1_k ;
      slope_LA_LA1_k = 1 ;
      LA_LA1_k_u = (slope_LA_LA1_k * d) + LA_LA1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LA_LA1_annhilate ;
      force_init_update = False;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell1_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LA_LA1!\n");
      exit(1);
    }
    break;
  case ( LA_LA1_previous_drection1 ):
    if (True == False) {;}
    else if  (LA_LA1_from_cell == (1.0)) {
      LA_LA1_k_u = 1 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
      cstate =  LA_LA1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LA_LA1_from_cell == (0.0)) {
      LA_LA1_k_u = 1 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
      cstate =  LA_LA1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LA_LA1_from_cell == (2.0) && (LA_LA1_cell2_mode_delayed == (0.0))) {
      LA_LA1_k_u = 1 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
      cstate =  LA_LA1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LA_LA1_from_cell == (2.0) && (LA_LA1_cell2_mode_delayed != (0.0))) {
      LA_LA1_k_u = 1 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
      cstate =  LA_LA1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LA_LA1_k_init = LA_LA1_k ;
      slope_LA_LA1_k = 1 ;
      LA_LA1_k_u = (slope_LA_LA1_k * d) + LA_LA1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LA_LA1_previous_drection1 ;
      force_init_update = False;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell1_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LA_LA1!\n");
      exit(1);
    }
    break;
  case ( LA_LA1_previous_direction2 ):
    if (True == False) {;}
    else if  (LA_LA1_from_cell == (1.0) && (LA_LA1_cell1_mode_delayed != (0.0))) {
      LA_LA1_k_u = 1 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
      cstate =  LA_LA1_annhilate ;
      force_init_update = False;
    }
    else if  (LA_LA1_from_cell == (2.0)) {
      LA_LA1_k_u = 1 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
      cstate =  LA_LA1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (LA_LA1_from_cell == (0.0)) {
      LA_LA1_k_u = 1 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
      cstate =  LA_LA1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (LA_LA1_from_cell == (1.0) && (LA_LA1_cell1_mode_delayed == (0.0))) {
      LA_LA1_k_u = 1 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
      cstate =  LA_LA1_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LA_LA1_k_init = LA_LA1_k ;
      slope_LA_LA1_k = 1 ;
      LA_LA1_k_u = (slope_LA_LA1_k * d) + LA_LA1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LA_LA1_previous_direction2 ;
      force_init_update = False;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell1_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LA_LA1!\n");
      exit(1);
    }
    break;
  case ( LA_LA1_wait_cell1 ):
    if (True == False) {;}
    else if  (LA_LA1_cell2_mode == (2.0)) {
      LA_LA1_k_u = 1 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
      cstate =  LA_LA1_annhilate ;
      force_init_update = False;
    }
    else if  (LA_LA1_k >= (40.0)) {
      LA_LA1_from_cell_u = 1 ;
      LA_LA1_cell1_replay_latch_u = 1 ;
      LA_LA1_k_u = 1 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
      cstate =  LA_LA1_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LA_LA1_k_init = LA_LA1_k ;
      slope_LA_LA1_k = 1 ;
      LA_LA1_k_u = (slope_LA_LA1_k * d) + LA_LA1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LA_LA1_wait_cell1 ;
      force_init_update = False;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell1_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LA_LA1!\n");
      exit(1);
    }
    break;
  case ( LA_LA1_replay_cell1 ):
    if (True == False) {;}
    else if  (LA_LA1_cell1_mode == (2.0)) {
      LA_LA1_k_u = 1 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
      cstate =  LA_LA1_annhilate ;
      force_init_update = False;
    }
    else if  (LA_LA1_k >= (40.0)) {
      LA_LA1_from_cell_u = 2 ;
      LA_LA1_cell2_replay_latch_u = 1 ;
      LA_LA1_k_u = 1 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
      cstate =  LA_LA1_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LA_LA1_k_init = LA_LA1_k ;
      slope_LA_LA1_k = 1 ;
      LA_LA1_k_u = (slope_LA_LA1_k * d) + LA_LA1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LA_LA1_replay_cell1 ;
      force_init_update = False;
      LA_LA1_cell1_replay_latch_u = 1 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell1_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LA_LA1!\n");
      exit(1);
    }
    break;
  case ( LA_LA1_replay_cell2 ):
    if (True == False) {;}
    else if  (LA_LA1_k >= (10.0)) {
      LA_LA1_k_u = 1 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
      cstate =  LA_LA1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LA_LA1_k_init = LA_LA1_k ;
      slope_LA_LA1_k = 1 ;
      LA_LA1_k_u = (slope_LA_LA1_k * d) + LA_LA1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LA_LA1_replay_cell2 ;
      force_init_update = False;
      LA_LA1_cell2_replay_latch_u = 1 ;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell1_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LA_LA1!\n");
      exit(1);
    }
    break;
  case ( LA_LA1_wait_cell2 ):
    if (True == False) {;}
    else if  (LA_LA1_k >= (10.0)) {
      LA_LA1_k_u = 1 ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
      cstate =  LA_LA1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LA_LA1_k_init = LA_LA1_k ;
      slope_LA_LA1_k = 1 ;
      LA_LA1_k_u = (slope_LA_LA1_k * d) + LA_LA1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LA_LA1_wait_cell2 ;
      force_init_update = False;
      LA_LA1_cell1_v_delayed_u = LA_LA1_update_c1vd () ;
      LA_LA1_cell2_v_delayed_u = LA_LA1_update_c2vd () ;
      LA_LA1_cell1_mode_delayed_u = LA_LA1_update_c1md () ;
      LA_LA1_cell2_mode_delayed_u = LA_LA1_update_c2md () ;
      LA_LA1_wasted_u = LA_LA1_update_buffer_index (LA_LA1_cell1_v,LA_LA1_cell2_v,LA_LA1_cell1_mode,LA_LA1_cell2_mode) ;
      LA_LA1_cell1_replay_latch_u = LA_LA1_update_latch1 (LA_LA1_cell1_mode_delayed,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_replay_latch_u = LA_LA1_update_latch2 (LA_LA1_cell2_mode_delayed,LA_LA1_cell2_replay_latch_u) ;
      LA_LA1_cell1_v_replay = LA_LA1_update_ocell1 (LA_LA1_cell1_v_delayed_u,LA_LA1_cell1_replay_latch_u) ;
      LA_LA1_cell2_v_replay = LA_LA1_update_ocell2 (LA_LA1_cell2_v_delayed_u,LA_LA1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LA_LA1!\n");
      exit(1);
    }
    break;
  }
  LA_LA1_k = LA_LA1_k_u;
  LA_LA1_cell1_mode_delayed = LA_LA1_cell1_mode_delayed_u;
  LA_LA1_cell2_mode_delayed = LA_LA1_cell2_mode_delayed_u;
  LA_LA1_from_cell = LA_LA1_from_cell_u;
  LA_LA1_cell1_replay_latch = LA_LA1_cell1_replay_latch_u;
  LA_LA1_cell2_replay_latch = LA_LA1_cell2_replay_latch_u;
  LA_LA1_cell1_v_delayed = LA_LA1_cell1_v_delayed_u;
  LA_LA1_cell2_v_delayed = LA_LA1_cell2_v_delayed_u;
  LA_LA1_wasted = LA_LA1_wasted_u;
  return cstate;
}