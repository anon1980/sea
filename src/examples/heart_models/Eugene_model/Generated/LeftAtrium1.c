#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double LeftAtrium1_v_i_0;
double LeftAtrium1_v_i_1;
double LeftAtrium1_voo = 0.0;
double LeftAtrium1_state = 0.0;


static double  LeftAtrium1_vx  =  0 ,  LeftAtrium1_vy  =  0 ,  LeftAtrium1_vz  =  0 ,  LeftAtrium1_g  =  0 ,  LeftAtrium1_v  =  0 ,  LeftAtrium1_ft  =  0 ,  LeftAtrium1_theta  =  0 ,  LeftAtrium1_v_O  =  0 ; //the continuous vars
static double  LeftAtrium1_vx_u , LeftAtrium1_vy_u , LeftAtrium1_vz_u , LeftAtrium1_g_u , LeftAtrium1_v_u , LeftAtrium1_ft_u , LeftAtrium1_theta_u , LeftAtrium1_v_O_u ; // and their updates
static double  LeftAtrium1_vx_init , LeftAtrium1_vy_init , LeftAtrium1_vz_init , LeftAtrium1_g_init , LeftAtrium1_v_init , LeftAtrium1_ft_init , LeftAtrium1_theta_init , LeftAtrium1_v_O_init ; // and their inits
static double  slope_LeftAtrium1_vx , slope_LeftAtrium1_vy , slope_LeftAtrium1_vz , slope_LeftAtrium1_g , slope_LeftAtrium1_v , slope_LeftAtrium1_ft , slope_LeftAtrium1_theta , slope_LeftAtrium1_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { LeftAtrium1_t1 , LeftAtrium1_t2 , LeftAtrium1_t3 , LeftAtrium1_t4 }; // state declarations

enum states LeftAtrium1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( LeftAtrium1_t1 ):
    if (True == False) {;}
    else if  (LeftAtrium1_g > (44.5)) {
      LeftAtrium1_vx_u = (0.3 * LeftAtrium1_v) ;
      LeftAtrium1_vy_u = 0 ;
      LeftAtrium1_vz_u = (0.7 * LeftAtrium1_v) ;
      LeftAtrium1_g_u = ((((((((LeftAtrium1_v_i_0 + (- ((LeftAtrium1_vx + (- LeftAtrium1_vy)) + LeftAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      LeftAtrium1_theta_u = (LeftAtrium1_v / 30.0) ;
      LeftAtrium1_v_O_u = (131.1 + (- (80.1 * pow ( ((LeftAtrium1_v / 30.0)) , (0.5) )))) ;
      LeftAtrium1_ft_u = f (LeftAtrium1_theta,4.0e-2) ;
      cstate =  LeftAtrium1_t2 ;
      force_init_update = False;
    }

    else if ( LeftAtrium1_v <= (44.5)
               && 
              LeftAtrium1_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) LeftAtrium1_vx_init = LeftAtrium1_vx ;
      slope_LeftAtrium1_vx = (LeftAtrium1_vx * -8.7) ;
      LeftAtrium1_vx_u = (slope_LeftAtrium1_vx * d) + LeftAtrium1_vx ;
      if ((pstate != cstate) || force_init_update) LeftAtrium1_vy_init = LeftAtrium1_vy ;
      slope_LeftAtrium1_vy = (LeftAtrium1_vy * -190.9) ;
      LeftAtrium1_vy_u = (slope_LeftAtrium1_vy * d) + LeftAtrium1_vy ;
      if ((pstate != cstate) || force_init_update) LeftAtrium1_vz_init = LeftAtrium1_vz ;
      slope_LeftAtrium1_vz = (LeftAtrium1_vz * -190.4) ;
      LeftAtrium1_vz_u = (slope_LeftAtrium1_vz * d) + LeftAtrium1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftAtrium1_t1 ;
      force_init_update = False;
      LeftAtrium1_g_u = ((((((((LeftAtrium1_v_i_0 + (- ((LeftAtrium1_vx + (- LeftAtrium1_vy)) + LeftAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      LeftAtrium1_v_u = ((LeftAtrium1_vx + (- LeftAtrium1_vy)) + LeftAtrium1_vz) ;
      LeftAtrium1_voo = ((LeftAtrium1_vx + (- LeftAtrium1_vy)) + LeftAtrium1_vz) ;
      LeftAtrium1_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftAtrium1!\n");
      exit(1);
    }
    break;
  case ( LeftAtrium1_t2 ):
    if (True == False) {;}
    else if  (LeftAtrium1_v >= (44.5)) {
      LeftAtrium1_vx_u = LeftAtrium1_vx ;
      LeftAtrium1_vy_u = LeftAtrium1_vy ;
      LeftAtrium1_vz_u = LeftAtrium1_vz ;
      LeftAtrium1_g_u = ((((((((LeftAtrium1_v_i_0 + (- ((LeftAtrium1_vx + (- LeftAtrium1_vy)) + LeftAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  LeftAtrium1_t3 ;
      force_init_update = False;
    }
    else if  (LeftAtrium1_g <= (44.5)
               && LeftAtrium1_v < (44.5)) {
      LeftAtrium1_vx_u = LeftAtrium1_vx ;
      LeftAtrium1_vy_u = LeftAtrium1_vy ;
      LeftAtrium1_vz_u = LeftAtrium1_vz ;
      LeftAtrium1_g_u = ((((((((LeftAtrium1_v_i_0 + (- ((LeftAtrium1_vx + (- LeftAtrium1_vy)) + LeftAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  LeftAtrium1_t1 ;
      force_init_update = False;
    }

    else if ( LeftAtrium1_v < (44.5)
               && 
              LeftAtrium1_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftAtrium1_vx_init = LeftAtrium1_vx ;
      slope_LeftAtrium1_vx = ((LeftAtrium1_vx * -23.6) + (777200.0 * LeftAtrium1_g)) ;
      LeftAtrium1_vx_u = (slope_LeftAtrium1_vx * d) + LeftAtrium1_vx ;
      if ((pstate != cstate) || force_init_update) LeftAtrium1_vy_init = LeftAtrium1_vy ;
      slope_LeftAtrium1_vy = ((LeftAtrium1_vy * -45.5) + (58900.0 * LeftAtrium1_g)) ;
      LeftAtrium1_vy_u = (slope_LeftAtrium1_vy * d) + LeftAtrium1_vy ;
      if ((pstate != cstate) || force_init_update) LeftAtrium1_vz_init = LeftAtrium1_vz ;
      slope_LeftAtrium1_vz = ((LeftAtrium1_vz * -12.9) + (276600.0 * LeftAtrium1_g)) ;
      LeftAtrium1_vz_u = (slope_LeftAtrium1_vz * d) + LeftAtrium1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftAtrium1_t2 ;
      force_init_update = False;
      LeftAtrium1_g_u = ((((((((LeftAtrium1_v_i_0 + (- ((LeftAtrium1_vx + (- LeftAtrium1_vy)) + LeftAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      LeftAtrium1_v_u = ((LeftAtrium1_vx + (- LeftAtrium1_vy)) + LeftAtrium1_vz) ;
      LeftAtrium1_voo = ((LeftAtrium1_vx + (- LeftAtrium1_vy)) + LeftAtrium1_vz) ;
      LeftAtrium1_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftAtrium1!\n");
      exit(1);
    }
    break;
  case ( LeftAtrium1_t3 ):
    if (True == False) {;}
    else if  (LeftAtrium1_v >= (131.1)) {
      LeftAtrium1_vx_u = LeftAtrium1_vx ;
      LeftAtrium1_vy_u = LeftAtrium1_vy ;
      LeftAtrium1_vz_u = LeftAtrium1_vz ;
      LeftAtrium1_g_u = ((((((((LeftAtrium1_v_i_0 + (- ((LeftAtrium1_vx + (- LeftAtrium1_vy)) + LeftAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  LeftAtrium1_t4 ;
      force_init_update = False;
    }

    else if ( LeftAtrium1_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) LeftAtrium1_vx_init = LeftAtrium1_vx ;
      slope_LeftAtrium1_vx = (LeftAtrium1_vx * -6.9) ;
      LeftAtrium1_vx_u = (slope_LeftAtrium1_vx * d) + LeftAtrium1_vx ;
      if ((pstate != cstate) || force_init_update) LeftAtrium1_vy_init = LeftAtrium1_vy ;
      slope_LeftAtrium1_vy = (LeftAtrium1_vy * 75.9) ;
      LeftAtrium1_vy_u = (slope_LeftAtrium1_vy * d) + LeftAtrium1_vy ;
      if ((pstate != cstate) || force_init_update) LeftAtrium1_vz_init = LeftAtrium1_vz ;
      slope_LeftAtrium1_vz = (LeftAtrium1_vz * 6826.5) ;
      LeftAtrium1_vz_u = (slope_LeftAtrium1_vz * d) + LeftAtrium1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftAtrium1_t3 ;
      force_init_update = False;
      LeftAtrium1_g_u = ((((((((LeftAtrium1_v_i_0 + (- ((LeftAtrium1_vx + (- LeftAtrium1_vy)) + LeftAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      LeftAtrium1_v_u = ((LeftAtrium1_vx + (- LeftAtrium1_vy)) + LeftAtrium1_vz) ;
      LeftAtrium1_voo = ((LeftAtrium1_vx + (- LeftAtrium1_vy)) + LeftAtrium1_vz) ;
      LeftAtrium1_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftAtrium1!\n");
      exit(1);
    }
    break;
  case ( LeftAtrium1_t4 ):
    if (True == False) {;}
    else if  (LeftAtrium1_v <= (30.0)) {
      LeftAtrium1_vx_u = LeftAtrium1_vx ;
      LeftAtrium1_vy_u = LeftAtrium1_vy ;
      LeftAtrium1_vz_u = LeftAtrium1_vz ;
      LeftAtrium1_g_u = ((((((((LeftAtrium1_v_i_0 + (- ((LeftAtrium1_vx + (- LeftAtrium1_vy)) + LeftAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  LeftAtrium1_t1 ;
      force_init_update = False;
    }

    else if ( LeftAtrium1_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftAtrium1_vx_init = LeftAtrium1_vx ;
      slope_LeftAtrium1_vx = (LeftAtrium1_vx * -33.2) ;
      LeftAtrium1_vx_u = (slope_LeftAtrium1_vx * d) + LeftAtrium1_vx ;
      if ((pstate != cstate) || force_init_update) LeftAtrium1_vy_init = LeftAtrium1_vy ;
      slope_LeftAtrium1_vy = ((LeftAtrium1_vy * 20.0) * LeftAtrium1_ft) ;
      LeftAtrium1_vy_u = (slope_LeftAtrium1_vy * d) + LeftAtrium1_vy ;
      if ((pstate != cstate) || force_init_update) LeftAtrium1_vz_init = LeftAtrium1_vz ;
      slope_LeftAtrium1_vz = ((LeftAtrium1_vz * 2.0) * LeftAtrium1_ft) ;
      LeftAtrium1_vz_u = (slope_LeftAtrium1_vz * d) + LeftAtrium1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftAtrium1_t4 ;
      force_init_update = False;
      LeftAtrium1_g_u = ((((((((LeftAtrium1_v_i_0 + (- ((LeftAtrium1_vx + (- LeftAtrium1_vy)) + LeftAtrium1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      LeftAtrium1_v_u = ((LeftAtrium1_vx + (- LeftAtrium1_vy)) + LeftAtrium1_vz) ;
      LeftAtrium1_voo = ((LeftAtrium1_vx + (- LeftAtrium1_vy)) + LeftAtrium1_vz) ;
      LeftAtrium1_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftAtrium1!\n");
      exit(1);
    }
    break;
  }
  LeftAtrium1_vx = LeftAtrium1_vx_u;
  LeftAtrium1_vy = LeftAtrium1_vy_u;
  LeftAtrium1_vz = LeftAtrium1_vz_u;
  LeftAtrium1_g = LeftAtrium1_g_u;
  LeftAtrium1_v = LeftAtrium1_v_u;
  LeftAtrium1_ft = LeftAtrium1_ft_u;
  LeftAtrium1_theta = LeftAtrium1_theta_u;
  LeftAtrium1_v_O = LeftAtrium1_v_O_u;
  return cstate;
}