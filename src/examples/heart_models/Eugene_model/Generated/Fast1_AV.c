#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double Fast1_AV_update_c1vd();
extern double Fast1_AV_update_c2vd();
extern double Fast1_AV_update_c1md();
extern double Fast1_AV_update_c2md();
extern double Fast1_AV_update_buffer_index(double,double,double,double);
extern double Fast1_AV_update_latch1(double,double);
extern double Fast1_AV_update_latch2(double,double);
extern double Fast1_AV_update_ocell1(double,double);
extern double Fast1_AV_update_ocell2(double,double);
double Fast1_AV_cell1_v;
double Fast1_AV_cell1_mode;
double Fast1_AV_cell2_v;
double Fast1_AV_cell2_mode;
double Fast1_AV_cell1_v_replay = 0.0;
double Fast1_AV_cell2_v_replay = 0.0;


static double  Fast1_AV_k  =  0.0 ,  Fast1_AV_cell1_mode_delayed  =  0.0 ,  Fast1_AV_cell2_mode_delayed  =  0.0 ,  Fast1_AV_from_cell  =  0.0 ,  Fast1_AV_cell1_replay_latch  =  0.0 ,  Fast1_AV_cell2_replay_latch  =  0.0 ,  Fast1_AV_cell1_v_delayed  =  0.0 ,  Fast1_AV_cell2_v_delayed  =  0.0 ,  Fast1_AV_wasted  =  0.0 ; //the continuous vars
static double  Fast1_AV_k_u , Fast1_AV_cell1_mode_delayed_u , Fast1_AV_cell2_mode_delayed_u , Fast1_AV_from_cell_u , Fast1_AV_cell1_replay_latch_u , Fast1_AV_cell2_replay_latch_u , Fast1_AV_cell1_v_delayed_u , Fast1_AV_cell2_v_delayed_u , Fast1_AV_wasted_u ; // and their updates
static double  Fast1_AV_k_init , Fast1_AV_cell1_mode_delayed_init , Fast1_AV_cell2_mode_delayed_init , Fast1_AV_from_cell_init , Fast1_AV_cell1_replay_latch_init , Fast1_AV_cell2_replay_latch_init , Fast1_AV_cell1_v_delayed_init , Fast1_AV_cell2_v_delayed_init , Fast1_AV_wasted_init ; // and their inits
static double  slope_Fast1_AV_k , slope_Fast1_AV_cell1_mode_delayed , slope_Fast1_AV_cell2_mode_delayed , slope_Fast1_AV_from_cell , slope_Fast1_AV_cell1_replay_latch , slope_Fast1_AV_cell2_replay_latch , slope_Fast1_AV_cell1_v_delayed , slope_Fast1_AV_cell2_v_delayed , slope_Fast1_AV_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { Fast1_AV_idle , Fast1_AV_annhilate , Fast1_AV_previous_drection1 , Fast1_AV_previous_direction2 , Fast1_AV_wait_cell1 , Fast1_AV_replay_cell1 , Fast1_AV_replay_cell2 , Fast1_AV_wait_cell2 }; // state declarations

enum states Fast1_AV (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( Fast1_AV_idle ):
    if (True == False) {;}
    else if  (Fast1_AV_cell2_mode == (2.0) && (Fast1_AV_cell1_mode != (2.0))) {
      Fast1_AV_k_u = 1 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
      cstate =  Fast1_AV_previous_direction2 ;
      force_init_update = False;
    }
    else if  (Fast1_AV_cell1_mode == (2.0) && (Fast1_AV_cell2_mode != (2.0))) {
      Fast1_AV_k_u = 1 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
      cstate =  Fast1_AV_previous_drection1 ;
      force_init_update = False;
    }
    else if  (Fast1_AV_cell1_mode == (2.0) && (Fast1_AV_cell2_mode == (2.0))) {
      Fast1_AV_k_u = 1 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
      cstate =  Fast1_AV_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Fast1_AV_k_init = Fast1_AV_k ;
      slope_Fast1_AV_k = 1 ;
      Fast1_AV_k_u = (slope_Fast1_AV_k * d) + Fast1_AV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Fast1_AV_idle ;
      force_init_update = False;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell1_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast1_AV!\n");
      exit(1);
    }
    break;
  case ( Fast1_AV_annhilate ):
    if (True == False) {;}
    else if  (Fast1_AV_cell1_mode != (2.0) && (Fast1_AV_cell2_mode != (2.0))) {
      Fast1_AV_k_u = 1 ;
      Fast1_AV_from_cell_u = 0 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
      cstate =  Fast1_AV_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Fast1_AV_k_init = Fast1_AV_k ;
      slope_Fast1_AV_k = 1 ;
      Fast1_AV_k_u = (slope_Fast1_AV_k * d) + Fast1_AV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Fast1_AV_annhilate ;
      force_init_update = False;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell1_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast1_AV!\n");
      exit(1);
    }
    break;
  case ( Fast1_AV_previous_drection1 ):
    if (True == False) {;}
    else if  (Fast1_AV_from_cell == (1.0)) {
      Fast1_AV_k_u = 1 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
      cstate =  Fast1_AV_wait_cell1 ;
      force_init_update = False;
    }
    else if  (Fast1_AV_from_cell == (0.0)) {
      Fast1_AV_k_u = 1 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
      cstate =  Fast1_AV_wait_cell1 ;
      force_init_update = False;
    }
    else if  (Fast1_AV_from_cell == (2.0) && (Fast1_AV_cell2_mode_delayed == (0.0))) {
      Fast1_AV_k_u = 1 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
      cstate =  Fast1_AV_wait_cell1 ;
      force_init_update = False;
    }
    else if  (Fast1_AV_from_cell == (2.0) && (Fast1_AV_cell2_mode_delayed != (0.0))) {
      Fast1_AV_k_u = 1 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
      cstate =  Fast1_AV_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Fast1_AV_k_init = Fast1_AV_k ;
      slope_Fast1_AV_k = 1 ;
      Fast1_AV_k_u = (slope_Fast1_AV_k * d) + Fast1_AV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Fast1_AV_previous_drection1 ;
      force_init_update = False;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell1_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast1_AV!\n");
      exit(1);
    }
    break;
  case ( Fast1_AV_previous_direction2 ):
    if (True == False) {;}
    else if  (Fast1_AV_from_cell == (1.0) && (Fast1_AV_cell1_mode_delayed != (0.0))) {
      Fast1_AV_k_u = 1 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
      cstate =  Fast1_AV_annhilate ;
      force_init_update = False;
    }
    else if  (Fast1_AV_from_cell == (2.0)) {
      Fast1_AV_k_u = 1 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
      cstate =  Fast1_AV_replay_cell1 ;
      force_init_update = False;
    }
    else if  (Fast1_AV_from_cell == (0.0)) {
      Fast1_AV_k_u = 1 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
      cstate =  Fast1_AV_replay_cell1 ;
      force_init_update = False;
    }
    else if  (Fast1_AV_from_cell == (1.0) && (Fast1_AV_cell1_mode_delayed == (0.0))) {
      Fast1_AV_k_u = 1 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
      cstate =  Fast1_AV_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Fast1_AV_k_init = Fast1_AV_k ;
      slope_Fast1_AV_k = 1 ;
      Fast1_AV_k_u = (slope_Fast1_AV_k * d) + Fast1_AV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Fast1_AV_previous_direction2 ;
      force_init_update = False;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell1_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast1_AV!\n");
      exit(1);
    }
    break;
  case ( Fast1_AV_wait_cell1 ):
    if (True == False) {;}
    else if  (Fast1_AV_cell2_mode == (2.0)) {
      Fast1_AV_k_u = 1 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
      cstate =  Fast1_AV_annhilate ;
      force_init_update = False;
    }
    else if  (Fast1_AV_k >= (10.0)) {
      Fast1_AV_from_cell_u = 1 ;
      Fast1_AV_cell1_replay_latch_u = 1 ;
      Fast1_AV_k_u = 1 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
      cstate =  Fast1_AV_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Fast1_AV_k_init = Fast1_AV_k ;
      slope_Fast1_AV_k = 1 ;
      Fast1_AV_k_u = (slope_Fast1_AV_k * d) + Fast1_AV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Fast1_AV_wait_cell1 ;
      force_init_update = False;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell1_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast1_AV!\n");
      exit(1);
    }
    break;
  case ( Fast1_AV_replay_cell1 ):
    if (True == False) {;}
    else if  (Fast1_AV_cell1_mode == (2.0)) {
      Fast1_AV_k_u = 1 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
      cstate =  Fast1_AV_annhilate ;
      force_init_update = False;
    }
    else if  (Fast1_AV_k >= (10.0)) {
      Fast1_AV_from_cell_u = 2 ;
      Fast1_AV_cell2_replay_latch_u = 1 ;
      Fast1_AV_k_u = 1 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
      cstate =  Fast1_AV_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Fast1_AV_k_init = Fast1_AV_k ;
      slope_Fast1_AV_k = 1 ;
      Fast1_AV_k_u = (slope_Fast1_AV_k * d) + Fast1_AV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Fast1_AV_replay_cell1 ;
      force_init_update = False;
      Fast1_AV_cell1_replay_latch_u = 1 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell1_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast1_AV!\n");
      exit(1);
    }
    break;
  case ( Fast1_AV_replay_cell2 ):
    if (True == False) {;}
    else if  (Fast1_AV_k >= (10.0)) {
      Fast1_AV_k_u = 1 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
      cstate =  Fast1_AV_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Fast1_AV_k_init = Fast1_AV_k ;
      slope_Fast1_AV_k = 1 ;
      Fast1_AV_k_u = (slope_Fast1_AV_k * d) + Fast1_AV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Fast1_AV_replay_cell2 ;
      force_init_update = False;
      Fast1_AV_cell2_replay_latch_u = 1 ;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell1_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast1_AV!\n");
      exit(1);
    }
    break;
  case ( Fast1_AV_wait_cell2 ):
    if (True == False) {;}
    else if  (Fast1_AV_k >= (10.0)) {
      Fast1_AV_k_u = 1 ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
      cstate =  Fast1_AV_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Fast1_AV_k_init = Fast1_AV_k ;
      slope_Fast1_AV_k = 1 ;
      Fast1_AV_k_u = (slope_Fast1_AV_k * d) + Fast1_AV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Fast1_AV_wait_cell2 ;
      force_init_update = False;
      Fast1_AV_cell1_v_delayed_u = Fast1_AV_update_c1vd () ;
      Fast1_AV_cell2_v_delayed_u = Fast1_AV_update_c2vd () ;
      Fast1_AV_cell1_mode_delayed_u = Fast1_AV_update_c1md () ;
      Fast1_AV_cell2_mode_delayed_u = Fast1_AV_update_c2md () ;
      Fast1_AV_wasted_u = Fast1_AV_update_buffer_index (Fast1_AV_cell1_v,Fast1_AV_cell2_v,Fast1_AV_cell1_mode,Fast1_AV_cell2_mode) ;
      Fast1_AV_cell1_replay_latch_u = Fast1_AV_update_latch1 (Fast1_AV_cell1_mode_delayed,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_replay_latch_u = Fast1_AV_update_latch2 (Fast1_AV_cell2_mode_delayed,Fast1_AV_cell2_replay_latch_u) ;
      Fast1_AV_cell1_v_replay = Fast1_AV_update_ocell1 (Fast1_AV_cell1_v_delayed_u,Fast1_AV_cell1_replay_latch_u) ;
      Fast1_AV_cell2_v_replay = Fast1_AV_update_ocell2 (Fast1_AV_cell2_v_delayed_u,Fast1_AV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast1_AV!\n");
      exit(1);
    }
    break;
  }
  Fast1_AV_k = Fast1_AV_k_u;
  Fast1_AV_cell1_mode_delayed = Fast1_AV_cell1_mode_delayed_u;
  Fast1_AV_cell2_mode_delayed = Fast1_AV_cell2_mode_delayed_u;
  Fast1_AV_from_cell = Fast1_AV_from_cell_u;
  Fast1_AV_cell1_replay_latch = Fast1_AV_cell1_replay_latch_u;
  Fast1_AV_cell2_replay_latch = Fast1_AV_cell2_replay_latch_u;
  Fast1_AV_cell1_v_delayed = Fast1_AV_cell1_v_delayed_u;
  Fast1_AV_cell2_v_delayed = Fast1_AV_cell2_v_delayed_u;
  Fast1_AV_wasted = Fast1_AV_wasted_u;
  return cstate;
}