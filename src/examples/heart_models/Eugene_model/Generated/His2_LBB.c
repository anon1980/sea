#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double His2_LBB_update_c1vd();
extern double His2_LBB_update_c2vd();
extern double His2_LBB_update_c1md();
extern double His2_LBB_update_c2md();
extern double His2_LBB_update_buffer_index(double,double,double,double);
extern double His2_LBB_update_latch1(double,double);
extern double His2_LBB_update_latch2(double,double);
extern double His2_LBB_update_ocell1(double,double);
extern double His2_LBB_update_ocell2(double,double);
double His2_LBB_cell1_v;
double His2_LBB_cell1_mode;
double His2_LBB_cell2_v;
double His2_LBB_cell2_mode;
double His2_LBB_cell1_v_replay = 0.0;
double His2_LBB_cell2_v_replay = 0.0;


static double  His2_LBB_k  =  0.0 ,  His2_LBB_cell1_mode_delayed  =  0.0 ,  His2_LBB_cell2_mode_delayed  =  0.0 ,  His2_LBB_from_cell  =  0.0 ,  His2_LBB_cell1_replay_latch  =  0.0 ,  His2_LBB_cell2_replay_latch  =  0.0 ,  His2_LBB_cell1_v_delayed  =  0.0 ,  His2_LBB_cell2_v_delayed  =  0.0 ,  His2_LBB_wasted  =  0.0 ; //the continuous vars
static double  His2_LBB_k_u , His2_LBB_cell1_mode_delayed_u , His2_LBB_cell2_mode_delayed_u , His2_LBB_from_cell_u , His2_LBB_cell1_replay_latch_u , His2_LBB_cell2_replay_latch_u , His2_LBB_cell1_v_delayed_u , His2_LBB_cell2_v_delayed_u , His2_LBB_wasted_u ; // and their updates
static double  His2_LBB_k_init , His2_LBB_cell1_mode_delayed_init , His2_LBB_cell2_mode_delayed_init , His2_LBB_from_cell_init , His2_LBB_cell1_replay_latch_init , His2_LBB_cell2_replay_latch_init , His2_LBB_cell1_v_delayed_init , His2_LBB_cell2_v_delayed_init , His2_LBB_wasted_init ; // and their inits
static double  slope_His2_LBB_k , slope_His2_LBB_cell1_mode_delayed , slope_His2_LBB_cell2_mode_delayed , slope_His2_LBB_from_cell , slope_His2_LBB_cell1_replay_latch , slope_His2_LBB_cell2_replay_latch , slope_His2_LBB_cell1_v_delayed , slope_His2_LBB_cell2_v_delayed , slope_His2_LBB_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { His2_LBB_idle , His2_LBB_annhilate , His2_LBB_previous_drection1 , His2_LBB_previous_direction2 , His2_LBB_wait_cell1 , His2_LBB_replay_cell1 , His2_LBB_replay_cell2 , His2_LBB_wait_cell2 }; // state declarations

enum states His2_LBB (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( His2_LBB_idle ):
    if (True == False) {;}
    else if  (His2_LBB_cell2_mode == (2.0) && (His2_LBB_cell1_mode != (2.0))) {
      His2_LBB_k_u = 1 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
      cstate =  His2_LBB_previous_direction2 ;
      force_init_update = False;
    }
    else if  (His2_LBB_cell1_mode == (2.0) && (His2_LBB_cell2_mode != (2.0))) {
      His2_LBB_k_u = 1 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
      cstate =  His2_LBB_previous_drection1 ;
      force_init_update = False;
    }
    else if  (His2_LBB_cell1_mode == (2.0) && (His2_LBB_cell2_mode == (2.0))) {
      His2_LBB_k_u = 1 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
      cstate =  His2_LBB_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His2_LBB_k_init = His2_LBB_k ;
      slope_His2_LBB_k = 1 ;
      His2_LBB_k_u = (slope_His2_LBB_k * d) + His2_LBB_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His2_LBB_idle ;
      force_init_update = False;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell1_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_LBB!\n");
      exit(1);
    }
    break;
  case ( His2_LBB_annhilate ):
    if (True == False) {;}
    else if  (His2_LBB_cell1_mode != (2.0) && (His2_LBB_cell2_mode != (2.0))) {
      His2_LBB_k_u = 1 ;
      His2_LBB_from_cell_u = 0 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
      cstate =  His2_LBB_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His2_LBB_k_init = His2_LBB_k ;
      slope_His2_LBB_k = 1 ;
      His2_LBB_k_u = (slope_His2_LBB_k * d) + His2_LBB_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His2_LBB_annhilate ;
      force_init_update = False;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell1_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_LBB!\n");
      exit(1);
    }
    break;
  case ( His2_LBB_previous_drection1 ):
    if (True == False) {;}
    else if  (His2_LBB_from_cell == (1.0)) {
      His2_LBB_k_u = 1 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
      cstate =  His2_LBB_wait_cell1 ;
      force_init_update = False;
    }
    else if  (His2_LBB_from_cell == (0.0)) {
      His2_LBB_k_u = 1 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
      cstate =  His2_LBB_wait_cell1 ;
      force_init_update = False;
    }
    else if  (His2_LBB_from_cell == (2.0) && (His2_LBB_cell2_mode_delayed == (0.0))) {
      His2_LBB_k_u = 1 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
      cstate =  His2_LBB_wait_cell1 ;
      force_init_update = False;
    }
    else if  (His2_LBB_from_cell == (2.0) && (His2_LBB_cell2_mode_delayed != (0.0))) {
      His2_LBB_k_u = 1 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
      cstate =  His2_LBB_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His2_LBB_k_init = His2_LBB_k ;
      slope_His2_LBB_k = 1 ;
      His2_LBB_k_u = (slope_His2_LBB_k * d) + His2_LBB_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His2_LBB_previous_drection1 ;
      force_init_update = False;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell1_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_LBB!\n");
      exit(1);
    }
    break;
  case ( His2_LBB_previous_direction2 ):
    if (True == False) {;}
    else if  (His2_LBB_from_cell == (1.0) && (His2_LBB_cell1_mode_delayed != (0.0))) {
      His2_LBB_k_u = 1 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
      cstate =  His2_LBB_annhilate ;
      force_init_update = False;
    }
    else if  (His2_LBB_from_cell == (2.0)) {
      His2_LBB_k_u = 1 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
      cstate =  His2_LBB_replay_cell1 ;
      force_init_update = False;
    }
    else if  (His2_LBB_from_cell == (0.0)) {
      His2_LBB_k_u = 1 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
      cstate =  His2_LBB_replay_cell1 ;
      force_init_update = False;
    }
    else if  (His2_LBB_from_cell == (1.0) && (His2_LBB_cell1_mode_delayed == (0.0))) {
      His2_LBB_k_u = 1 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
      cstate =  His2_LBB_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His2_LBB_k_init = His2_LBB_k ;
      slope_His2_LBB_k = 1 ;
      His2_LBB_k_u = (slope_His2_LBB_k * d) + His2_LBB_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His2_LBB_previous_direction2 ;
      force_init_update = False;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell1_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_LBB!\n");
      exit(1);
    }
    break;
  case ( His2_LBB_wait_cell1 ):
    if (True == False) {;}
    else if  (His2_LBB_cell2_mode == (2.0)) {
      His2_LBB_k_u = 1 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
      cstate =  His2_LBB_annhilate ;
      force_init_update = False;
    }
    else if  (His2_LBB_k >= (20.0)) {
      His2_LBB_from_cell_u = 1 ;
      His2_LBB_cell1_replay_latch_u = 1 ;
      His2_LBB_k_u = 1 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
      cstate =  His2_LBB_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His2_LBB_k_init = His2_LBB_k ;
      slope_His2_LBB_k = 1 ;
      His2_LBB_k_u = (slope_His2_LBB_k * d) + His2_LBB_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His2_LBB_wait_cell1 ;
      force_init_update = False;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell1_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_LBB!\n");
      exit(1);
    }
    break;
  case ( His2_LBB_replay_cell1 ):
    if (True == False) {;}
    else if  (His2_LBB_cell1_mode == (2.0)) {
      His2_LBB_k_u = 1 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
      cstate =  His2_LBB_annhilate ;
      force_init_update = False;
    }
    else if  (His2_LBB_k >= (20.0)) {
      His2_LBB_from_cell_u = 2 ;
      His2_LBB_cell2_replay_latch_u = 1 ;
      His2_LBB_k_u = 1 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
      cstate =  His2_LBB_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His2_LBB_k_init = His2_LBB_k ;
      slope_His2_LBB_k = 1 ;
      His2_LBB_k_u = (slope_His2_LBB_k * d) + His2_LBB_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His2_LBB_replay_cell1 ;
      force_init_update = False;
      His2_LBB_cell1_replay_latch_u = 1 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell1_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_LBB!\n");
      exit(1);
    }
    break;
  case ( His2_LBB_replay_cell2 ):
    if (True == False) {;}
    else if  (His2_LBB_k >= (10.0)) {
      His2_LBB_k_u = 1 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
      cstate =  His2_LBB_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His2_LBB_k_init = His2_LBB_k ;
      slope_His2_LBB_k = 1 ;
      His2_LBB_k_u = (slope_His2_LBB_k * d) + His2_LBB_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His2_LBB_replay_cell2 ;
      force_init_update = False;
      His2_LBB_cell2_replay_latch_u = 1 ;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell1_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_LBB!\n");
      exit(1);
    }
    break;
  case ( His2_LBB_wait_cell2 ):
    if (True == False) {;}
    else if  (His2_LBB_k >= (10.0)) {
      His2_LBB_k_u = 1 ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
      cstate =  His2_LBB_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His2_LBB_k_init = His2_LBB_k ;
      slope_His2_LBB_k = 1 ;
      His2_LBB_k_u = (slope_His2_LBB_k * d) + His2_LBB_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His2_LBB_wait_cell2 ;
      force_init_update = False;
      His2_LBB_cell1_v_delayed_u = His2_LBB_update_c1vd () ;
      His2_LBB_cell2_v_delayed_u = His2_LBB_update_c2vd () ;
      His2_LBB_cell1_mode_delayed_u = His2_LBB_update_c1md () ;
      His2_LBB_cell2_mode_delayed_u = His2_LBB_update_c2md () ;
      His2_LBB_wasted_u = His2_LBB_update_buffer_index (His2_LBB_cell1_v,His2_LBB_cell2_v,His2_LBB_cell1_mode,His2_LBB_cell2_mode) ;
      His2_LBB_cell1_replay_latch_u = His2_LBB_update_latch1 (His2_LBB_cell1_mode_delayed,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_replay_latch_u = His2_LBB_update_latch2 (His2_LBB_cell2_mode_delayed,His2_LBB_cell2_replay_latch_u) ;
      His2_LBB_cell1_v_replay = His2_LBB_update_ocell1 (His2_LBB_cell1_v_delayed_u,His2_LBB_cell1_replay_latch_u) ;
      His2_LBB_cell2_v_replay = His2_LBB_update_ocell2 (His2_LBB_cell2_v_delayed_u,His2_LBB_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_LBB!\n");
      exit(1);
    }
    break;
  }
  His2_LBB_k = His2_LBB_k_u;
  His2_LBB_cell1_mode_delayed = His2_LBB_cell1_mode_delayed_u;
  His2_LBB_cell2_mode_delayed = His2_LBB_cell2_mode_delayed_u;
  His2_LBB_from_cell = His2_LBB_from_cell_u;
  His2_LBB_cell1_replay_latch = His2_LBB_cell1_replay_latch_u;
  His2_LBB_cell2_replay_latch = His2_LBB_cell2_replay_latch_u;
  His2_LBB_cell1_v_delayed = His2_LBB_cell1_v_delayed_u;
  His2_LBB_cell2_v_delayed = His2_LBB_cell2_v_delayed_u;
  His2_LBB_wasted = His2_LBB_wasted_u;
  return cstate;
}