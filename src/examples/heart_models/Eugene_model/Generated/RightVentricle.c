#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double RightVentricle_v_i_0;
double RightVentricle_v_i_1;
double RightVentricle_v_i_2;
double RightVentricle_v_i_3;
double RightVentricle_voo = 0.0;
double RightVentricle_state = 0.0;


static double  RightVentricle_vx  =  0 ,  RightVentricle_vy  =  0 ,  RightVentricle_vz  =  0 ,  RightVentricle_g  =  0 ,  RightVentricle_v  =  0 ,  RightVentricle_ft  =  0 ,  RightVentricle_theta  =  0 ,  RightVentricle_v_O  =  0 ; //the continuous vars
static double  RightVentricle_vx_u , RightVentricle_vy_u , RightVentricle_vz_u , RightVentricle_g_u , RightVentricle_v_u , RightVentricle_ft_u , RightVentricle_theta_u , RightVentricle_v_O_u ; // and their updates
static double  RightVentricle_vx_init , RightVentricle_vy_init , RightVentricle_vz_init , RightVentricle_g_init , RightVentricle_v_init , RightVentricle_ft_init , RightVentricle_theta_init , RightVentricle_v_O_init ; // and their inits
static double  slope_RightVentricle_vx , slope_RightVentricle_vy , slope_RightVentricle_vz , slope_RightVentricle_g , slope_RightVentricle_v , slope_RightVentricle_ft , slope_RightVentricle_theta , slope_RightVentricle_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { RightVentricle_t1 , RightVentricle_t2 , RightVentricle_t3 , RightVentricle_t4 }; // state declarations

enum states RightVentricle (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( RightVentricle_t1 ):
    if (True == False) {;}
    else if  (RightVentricle_g > (44.5)) {
      RightVentricle_vx_u = (0.3 * RightVentricle_v) ;
      RightVentricle_vy_u = 0 ;
      RightVentricle_vz_u = (0.7 * RightVentricle_v) ;
      RightVentricle_g_u = ((((((((RightVentricle_v_i_0 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricle_v_i_1 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricle_v_i_2 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0))) + 0) + 0) ;
      RightVentricle_theta_u = (RightVentricle_v / 30.0) ;
      RightVentricle_v_O_u = (131.1 + (- (80.1 * pow ( ((RightVentricle_v / 30.0)) , (0.5) )))) ;
      RightVentricle_ft_u = f (RightVentricle_theta,4.0e-2) ;
      cstate =  RightVentricle_t2 ;
      force_init_update = False;
    }

    else if ( RightVentricle_v <= (44.5)
               && 
              RightVentricle_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricle_vx_init = RightVentricle_vx ;
      slope_RightVentricle_vx = (RightVentricle_vx * -8.7) ;
      RightVentricle_vx_u = (slope_RightVentricle_vx * d) + RightVentricle_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricle_vy_init = RightVentricle_vy ;
      slope_RightVentricle_vy = (RightVentricle_vy * -190.9) ;
      RightVentricle_vy_u = (slope_RightVentricle_vy * d) + RightVentricle_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricle_vz_init = RightVentricle_vz ;
      slope_RightVentricle_vz = (RightVentricle_vz * -190.4) ;
      RightVentricle_vz_u = (slope_RightVentricle_vz * d) + RightVentricle_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricle_t1 ;
      force_init_update = False;
      RightVentricle_g_u = ((((((((RightVentricle_v_i_0 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricle_v_i_1 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricle_v_i_2 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0))) + 0) + 0) ;
      RightVentricle_v_u = ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz) ;
      RightVentricle_voo = ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz) ;
      RightVentricle_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricle!\n");
      exit(1);
    }
    break;
  case ( RightVentricle_t2 ):
    if (True == False) {;}
    else if  (RightVentricle_v >= (44.5)) {
      RightVentricle_vx_u = RightVentricle_vx ;
      RightVentricle_vy_u = RightVentricle_vy ;
      RightVentricle_vz_u = RightVentricle_vz ;
      RightVentricle_g_u = ((((((((RightVentricle_v_i_0 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricle_v_i_1 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricle_v_i_2 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0))) + 0) + 0) ;
      cstate =  RightVentricle_t3 ;
      force_init_update = False;
    }
    else if  (RightVentricle_g <= (44.5)
               && RightVentricle_v < (44.5)) {
      RightVentricle_vx_u = RightVentricle_vx ;
      RightVentricle_vy_u = RightVentricle_vy ;
      RightVentricle_vz_u = RightVentricle_vz ;
      RightVentricle_g_u = ((((((((RightVentricle_v_i_0 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricle_v_i_1 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricle_v_i_2 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0))) + 0) + 0) ;
      cstate =  RightVentricle_t1 ;
      force_init_update = False;
    }

    else if ( RightVentricle_v < (44.5)
               && 
              RightVentricle_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricle_vx_init = RightVentricle_vx ;
      slope_RightVentricle_vx = ((RightVentricle_vx * -23.6) + (777200.0 * RightVentricle_g)) ;
      RightVentricle_vx_u = (slope_RightVentricle_vx * d) + RightVentricle_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricle_vy_init = RightVentricle_vy ;
      slope_RightVentricle_vy = ((RightVentricle_vy * -45.5) + (58900.0 * RightVentricle_g)) ;
      RightVentricle_vy_u = (slope_RightVentricle_vy * d) + RightVentricle_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricle_vz_init = RightVentricle_vz ;
      slope_RightVentricle_vz = ((RightVentricle_vz * -12.9) + (276600.0 * RightVentricle_g)) ;
      RightVentricle_vz_u = (slope_RightVentricle_vz * d) + RightVentricle_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricle_t2 ;
      force_init_update = False;
      RightVentricle_g_u = ((((((((RightVentricle_v_i_0 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricle_v_i_1 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricle_v_i_2 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0))) + 0) + 0) ;
      RightVentricle_v_u = ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz) ;
      RightVentricle_voo = ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz) ;
      RightVentricle_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricle!\n");
      exit(1);
    }
    break;
  case ( RightVentricle_t3 ):
    if (True == False) {;}
    else if  (RightVentricle_v >= (131.1)) {
      RightVentricle_vx_u = RightVentricle_vx ;
      RightVentricle_vy_u = RightVentricle_vy ;
      RightVentricle_vz_u = RightVentricle_vz ;
      RightVentricle_g_u = ((((((((RightVentricle_v_i_0 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricle_v_i_1 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricle_v_i_2 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0))) + 0) + 0) ;
      cstate =  RightVentricle_t4 ;
      force_init_update = False;
    }

    else if ( RightVentricle_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricle_vx_init = RightVentricle_vx ;
      slope_RightVentricle_vx = (RightVentricle_vx * -6.9) ;
      RightVentricle_vx_u = (slope_RightVentricle_vx * d) + RightVentricle_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricle_vy_init = RightVentricle_vy ;
      slope_RightVentricle_vy = (RightVentricle_vy * 75.9) ;
      RightVentricle_vy_u = (slope_RightVentricle_vy * d) + RightVentricle_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricle_vz_init = RightVentricle_vz ;
      slope_RightVentricle_vz = (RightVentricle_vz * 6826.5) ;
      RightVentricle_vz_u = (slope_RightVentricle_vz * d) + RightVentricle_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricle_t3 ;
      force_init_update = False;
      RightVentricle_g_u = ((((((((RightVentricle_v_i_0 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricle_v_i_1 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricle_v_i_2 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0))) + 0) + 0) ;
      RightVentricle_v_u = ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz) ;
      RightVentricle_voo = ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz) ;
      RightVentricle_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricle!\n");
      exit(1);
    }
    break;
  case ( RightVentricle_t4 ):
    if (True == False) {;}
    else if  (RightVentricle_v <= (30.0)) {
      RightVentricle_vx_u = RightVentricle_vx ;
      RightVentricle_vy_u = RightVentricle_vy ;
      RightVentricle_vz_u = RightVentricle_vz ;
      RightVentricle_g_u = ((((((((RightVentricle_v_i_0 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricle_v_i_1 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricle_v_i_2 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0))) + 0) + 0) ;
      cstate =  RightVentricle_t1 ;
      force_init_update = False;
    }

    else if ( RightVentricle_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricle_vx_init = RightVentricle_vx ;
      slope_RightVentricle_vx = (RightVentricle_vx * -33.2) ;
      RightVentricle_vx_u = (slope_RightVentricle_vx * d) + RightVentricle_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricle_vy_init = RightVentricle_vy ;
      slope_RightVentricle_vy = ((RightVentricle_vy * 11.0) * RightVentricle_ft) ;
      RightVentricle_vy_u = (slope_RightVentricle_vy * d) + RightVentricle_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricle_vz_init = RightVentricle_vz ;
      slope_RightVentricle_vz = ((RightVentricle_vz * 2.0) * RightVentricle_ft) ;
      RightVentricle_vz_u = (slope_RightVentricle_vz * d) + RightVentricle_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricle_t4 ;
      force_init_update = False;
      RightVentricle_g_u = ((((((((RightVentricle_v_i_0 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricle_v_i_1 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricle_v_i_2 + (- ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0))) + 0) + 0) ;
      RightVentricle_v_u = ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz) ;
      RightVentricle_voo = ((RightVentricle_vx + (- RightVentricle_vy)) + RightVentricle_vz) ;
      RightVentricle_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricle!\n");
      exit(1);
    }
    break;
  }
  RightVentricle_vx = RightVentricle_vx_u;
  RightVentricle_vy = RightVentricle_vy_u;
  RightVentricle_vz = RightVentricle_vz_u;
  RightVentricle_g = RightVentricle_g_u;
  RightVentricle_v = RightVentricle_v_u;
  RightVentricle_ft = RightVentricle_ft_u;
  RightVentricle_theta = RightVentricle_theta_u;
  RightVentricle_v_O = RightVentricle_v_O_u;
  return cstate;
}