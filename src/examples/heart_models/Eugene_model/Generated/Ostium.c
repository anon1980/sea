#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double Ostium_v_i_0;
double Ostium_v_i_1;
double Ostium_v_i_2;
double Ostium_v_i_3;
double Ostium_voo = 0.0;
double Ostium_state = 0.0;


static double  Ostium_vx  =  0 ,  Ostium_vy  =  0 ,  Ostium_vz  =  0 ,  Ostium_g  =  0 ,  Ostium_v  =  0 ,  Ostium_ft  =  0 ,  Ostium_theta  =  0 ,  Ostium_v_O  =  0 ; //the continuous vars
static double  Ostium_vx_u , Ostium_vy_u , Ostium_vz_u , Ostium_g_u , Ostium_v_u , Ostium_ft_u , Ostium_theta_u , Ostium_v_O_u ; // and their updates
static double  Ostium_vx_init , Ostium_vy_init , Ostium_vz_init , Ostium_g_init , Ostium_v_init , Ostium_ft_init , Ostium_theta_init , Ostium_v_O_init ; // and their inits
static double  slope_Ostium_vx , slope_Ostium_vy , slope_Ostium_vz , slope_Ostium_g , slope_Ostium_v , slope_Ostium_ft , slope_Ostium_theta , slope_Ostium_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { Ostium_t1 , Ostium_t2 , Ostium_t3 , Ostium_t4 }; // state declarations

enum states Ostium (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( Ostium_t1 ):
    if (True == False) {;}
    else if  (Ostium_g > (44.5)) {
      Ostium_vx_u = (0.3 * Ostium_v) ;
      Ostium_vy_u = 0 ;
      Ostium_vz_u = (0.7 * Ostium_v) ;
      Ostium_g_u = ((((((((Ostium_v_i_0 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Ostium_v_i_1 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((Ostium_v_i_2 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      Ostium_theta_u = (Ostium_v / 30.0) ;
      Ostium_v_O_u = (131.1 + (- (80.1 * pow ( ((Ostium_v / 30.0)) , (0.5) )))) ;
      Ostium_ft_u = f (Ostium_theta,4.0e-2) ;
      cstate =  Ostium_t2 ;
      force_init_update = False;
    }

    else if ( Ostium_v <= (44.5)
               && Ostium_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) Ostium_vx_init = Ostium_vx ;
      slope_Ostium_vx = (Ostium_vx * -8.7) ;
      Ostium_vx_u = (slope_Ostium_vx * d) + Ostium_vx ;
      if ((pstate != cstate) || force_init_update) Ostium_vy_init = Ostium_vy ;
      slope_Ostium_vy = (Ostium_vy * -190.9) ;
      Ostium_vy_u = (slope_Ostium_vy * d) + Ostium_vy ;
      if ((pstate != cstate) || force_init_update) Ostium_vz_init = Ostium_vz ;
      slope_Ostium_vz = (Ostium_vz * -190.4) ;
      Ostium_vz_u = (slope_Ostium_vz * d) + Ostium_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Ostium_t1 ;
      force_init_update = False;
      Ostium_g_u = ((((((((Ostium_v_i_0 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Ostium_v_i_1 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((Ostium_v_i_2 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      Ostium_v_u = ((Ostium_vx + (- Ostium_vy)) + Ostium_vz) ;
      Ostium_voo = ((Ostium_vx + (- Ostium_vy)) + Ostium_vz) ;
      Ostium_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Ostium!\n");
      exit(1);
    }
    break;
  case ( Ostium_t2 ):
    if (True == False) {;}
    else if  (Ostium_v >= (44.5)) {
      Ostium_vx_u = Ostium_vx ;
      Ostium_vy_u = Ostium_vy ;
      Ostium_vz_u = Ostium_vz ;
      Ostium_g_u = ((((((((Ostium_v_i_0 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Ostium_v_i_1 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((Ostium_v_i_2 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      cstate =  Ostium_t3 ;
      force_init_update = False;
    }
    else if  (Ostium_g <= (44.5)
               && Ostium_v < (44.5)) {
      Ostium_vx_u = Ostium_vx ;
      Ostium_vy_u = Ostium_vy ;
      Ostium_vz_u = Ostium_vz ;
      Ostium_g_u = ((((((((Ostium_v_i_0 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Ostium_v_i_1 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((Ostium_v_i_2 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      cstate =  Ostium_t1 ;
      force_init_update = False;
    }

    else if ( Ostium_v < (44.5)
               && Ostium_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) Ostium_vx_init = Ostium_vx ;
      slope_Ostium_vx = ((Ostium_vx * -23.6) + (777200.0 * Ostium_g)) ;
      Ostium_vx_u = (slope_Ostium_vx * d) + Ostium_vx ;
      if ((pstate != cstate) || force_init_update) Ostium_vy_init = Ostium_vy ;
      slope_Ostium_vy = ((Ostium_vy * -45.5) + (58900.0 * Ostium_g)) ;
      Ostium_vy_u = (slope_Ostium_vy * d) + Ostium_vy ;
      if ((pstate != cstate) || force_init_update) Ostium_vz_init = Ostium_vz ;
      slope_Ostium_vz = ((Ostium_vz * -12.9) + (276600.0 * Ostium_g)) ;
      Ostium_vz_u = (slope_Ostium_vz * d) + Ostium_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Ostium_t2 ;
      force_init_update = False;
      Ostium_g_u = ((((((((Ostium_v_i_0 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Ostium_v_i_1 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((Ostium_v_i_2 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      Ostium_v_u = ((Ostium_vx + (- Ostium_vy)) + Ostium_vz) ;
      Ostium_voo = ((Ostium_vx + (- Ostium_vy)) + Ostium_vz) ;
      Ostium_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Ostium!\n");
      exit(1);
    }
    break;
  case ( Ostium_t3 ):
    if (True == False) {;}
    else if  (Ostium_v >= (131.1)) {
      Ostium_vx_u = Ostium_vx ;
      Ostium_vy_u = Ostium_vy ;
      Ostium_vz_u = Ostium_vz ;
      Ostium_g_u = ((((((((Ostium_v_i_0 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Ostium_v_i_1 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((Ostium_v_i_2 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      cstate =  Ostium_t4 ;
      force_init_update = False;
    }

    else if ( Ostium_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) Ostium_vx_init = Ostium_vx ;
      slope_Ostium_vx = (Ostium_vx * -6.9) ;
      Ostium_vx_u = (slope_Ostium_vx * d) + Ostium_vx ;
      if ((pstate != cstate) || force_init_update) Ostium_vy_init = Ostium_vy ;
      slope_Ostium_vy = (Ostium_vy * 75.9) ;
      Ostium_vy_u = (slope_Ostium_vy * d) + Ostium_vy ;
      if ((pstate != cstate) || force_init_update) Ostium_vz_init = Ostium_vz ;
      slope_Ostium_vz = (Ostium_vz * 6826.5) ;
      Ostium_vz_u = (slope_Ostium_vz * d) + Ostium_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Ostium_t3 ;
      force_init_update = False;
      Ostium_g_u = ((((((((Ostium_v_i_0 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Ostium_v_i_1 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((Ostium_v_i_2 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      Ostium_v_u = ((Ostium_vx + (- Ostium_vy)) + Ostium_vz) ;
      Ostium_voo = ((Ostium_vx + (- Ostium_vy)) + Ostium_vz) ;
      Ostium_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Ostium!\n");
      exit(1);
    }
    break;
  case ( Ostium_t4 ):
    if (True == False) {;}
    else if  (Ostium_v <= (30.0)) {
      Ostium_vx_u = Ostium_vx ;
      Ostium_vy_u = Ostium_vy ;
      Ostium_vz_u = Ostium_vz ;
      Ostium_g_u = ((((((((Ostium_v_i_0 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Ostium_v_i_1 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((Ostium_v_i_2 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      cstate =  Ostium_t1 ;
      force_init_update = False;
    }

    else if ( Ostium_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) Ostium_vx_init = Ostium_vx ;
      slope_Ostium_vx = (Ostium_vx * -33.2) ;
      Ostium_vx_u = (slope_Ostium_vx * d) + Ostium_vx ;
      if ((pstate != cstate) || force_init_update) Ostium_vy_init = Ostium_vy ;
      slope_Ostium_vy = ((Ostium_vy * 20.0) * Ostium_ft) ;
      Ostium_vy_u = (slope_Ostium_vy * d) + Ostium_vy ;
      if ((pstate != cstate) || force_init_update) Ostium_vz_init = Ostium_vz ;
      slope_Ostium_vz = ((Ostium_vz * 2.0) * Ostium_ft) ;
      Ostium_vz_u = (slope_Ostium_vz * d) + Ostium_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Ostium_t4 ;
      force_init_update = False;
      Ostium_g_u = ((((((((Ostium_v_i_0 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Ostium_v_i_1 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((Ostium_v_i_2 + (- ((Ostium_vx + (- Ostium_vy)) + Ostium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) ;
      Ostium_v_u = ((Ostium_vx + (- Ostium_vy)) + Ostium_vz) ;
      Ostium_voo = ((Ostium_vx + (- Ostium_vy)) + Ostium_vz) ;
      Ostium_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Ostium!\n");
      exit(1);
    }
    break;
  }
  Ostium_vx = Ostium_vx_u;
  Ostium_vy = Ostium_vy_u;
  Ostium_vz = Ostium_vz_u;
  Ostium_g = Ostium_g_u;
  Ostium_v = Ostium_v_u;
  Ostium_ft = Ostium_ft_u;
  Ostium_theta = Ostium_theta_u;
  Ostium_v_O = Ostium_v_O_u;
  return cstate;
}