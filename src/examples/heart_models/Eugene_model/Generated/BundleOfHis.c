#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double BundleOfHis_v_i_0;
double BundleOfHis_v_i_1;
double BundleOfHis_v_i_2;
double BundleOfHis_voo = 0.0;
double BundleOfHis_state = 0.0;


static double  BundleOfHis_vx  =  0 ,  BundleOfHis_vy  =  0 ,  BundleOfHis_vz  =  0 ,  BundleOfHis_g  =  0 ,  BundleOfHis_v  =  0 ,  BundleOfHis_ft  =  0 ,  BundleOfHis_theta  =  0 ,  BundleOfHis_v_O  =  0 ; //the continuous vars
static double  BundleOfHis_vx_u , BundleOfHis_vy_u , BundleOfHis_vz_u , BundleOfHis_g_u , BundleOfHis_v_u , BundleOfHis_ft_u , BundleOfHis_theta_u , BundleOfHis_v_O_u ; // and their updates
static double  BundleOfHis_vx_init , BundleOfHis_vy_init , BundleOfHis_vz_init , BundleOfHis_g_init , BundleOfHis_v_init , BundleOfHis_ft_init , BundleOfHis_theta_init , BundleOfHis_v_O_init ; // and their inits
static double  slope_BundleOfHis_vx , slope_BundleOfHis_vy , slope_BundleOfHis_vz , slope_BundleOfHis_g , slope_BundleOfHis_v , slope_BundleOfHis_ft , slope_BundleOfHis_theta , slope_BundleOfHis_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { BundleOfHis_t1 , BundleOfHis_t2 , BundleOfHis_t3 , BundleOfHis_t4 }; // state declarations

enum states BundleOfHis (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( BundleOfHis_t1 ):
    if (True == False) {;}
    else if  (BundleOfHis_g > (44.5)) {
      BundleOfHis_vx_u = (0.3 * BundleOfHis_v) ;
      BundleOfHis_vy_u = 0 ;
      BundleOfHis_vz_u = (0.7 * BundleOfHis_v) ;
      BundleOfHis_g_u = ((((((((BundleOfHis_v_i_0 + (- ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis_v_i_1 + (- ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      BundleOfHis_theta_u = (BundleOfHis_v / 30.0) ;
      BundleOfHis_v_O_u = (131.1 + (- (80.1 * pow ( ((BundleOfHis_v / 30.0)) , (0.5) )))) ;
      BundleOfHis_ft_u = f (BundleOfHis_theta,4.0e-2) ;
      cstate =  BundleOfHis_t2 ;
      force_init_update = False;
    }

    else if ( BundleOfHis_v <= (44.5)
               && 
              BundleOfHis_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis_vx_init = BundleOfHis_vx ;
      slope_BundleOfHis_vx = (BundleOfHis_vx * -8.7) ;
      BundleOfHis_vx_u = (slope_BundleOfHis_vx * d) + BundleOfHis_vx ;
      if ((pstate != cstate) || force_init_update) BundleOfHis_vy_init = BundleOfHis_vy ;
      slope_BundleOfHis_vy = (BundleOfHis_vy * -190.9) ;
      BundleOfHis_vy_u = (slope_BundleOfHis_vy * d) + BundleOfHis_vy ;
      if ((pstate != cstate) || force_init_update) BundleOfHis_vz_init = BundleOfHis_vz ;
      slope_BundleOfHis_vz = (BundleOfHis_vz * -190.4) ;
      BundleOfHis_vz_u = (slope_BundleOfHis_vz * d) + BundleOfHis_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  BundleOfHis_t1 ;
      force_init_update = False;
      BundleOfHis_g_u = ((((((((BundleOfHis_v_i_0 + (- ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis_v_i_1 + (- ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      BundleOfHis_v_u = ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz) ;
      BundleOfHis_voo = ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz) ;
      BundleOfHis_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis!\n");
      exit(1);
    }
    break;
  case ( BundleOfHis_t2 ):
    if (True == False) {;}
    else if  (BundleOfHis_v >= (44.5)) {
      BundleOfHis_vx_u = BundleOfHis_vx ;
      BundleOfHis_vy_u = BundleOfHis_vy ;
      BundleOfHis_vz_u = BundleOfHis_vz ;
      BundleOfHis_g_u = ((((((((BundleOfHis_v_i_0 + (- ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis_v_i_1 + (- ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  BundleOfHis_t3 ;
      force_init_update = False;
    }
    else if  (BundleOfHis_g <= (44.5)
               && BundleOfHis_v < (44.5)) {
      BundleOfHis_vx_u = BundleOfHis_vx ;
      BundleOfHis_vy_u = BundleOfHis_vy ;
      BundleOfHis_vz_u = BundleOfHis_vz ;
      BundleOfHis_g_u = ((((((((BundleOfHis_v_i_0 + (- ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis_v_i_1 + (- ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  BundleOfHis_t1 ;
      force_init_update = False;
    }

    else if ( BundleOfHis_v < (44.5)
               && 
              BundleOfHis_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis_vx_init = BundleOfHis_vx ;
      slope_BundleOfHis_vx = ((BundleOfHis_vx * -23.6) + (777200.0 * BundleOfHis_g)) ;
      BundleOfHis_vx_u = (slope_BundleOfHis_vx * d) + BundleOfHis_vx ;
      if ((pstate != cstate) || force_init_update) BundleOfHis_vy_init = BundleOfHis_vy ;
      slope_BundleOfHis_vy = ((BundleOfHis_vy * -45.5) + (58900.0 * BundleOfHis_g)) ;
      BundleOfHis_vy_u = (slope_BundleOfHis_vy * d) + BundleOfHis_vy ;
      if ((pstate != cstate) || force_init_update) BundleOfHis_vz_init = BundleOfHis_vz ;
      slope_BundleOfHis_vz = ((BundleOfHis_vz * -12.9) + (276600.0 * BundleOfHis_g)) ;
      BundleOfHis_vz_u = (slope_BundleOfHis_vz * d) + BundleOfHis_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  BundleOfHis_t2 ;
      force_init_update = False;
      BundleOfHis_g_u = ((((((((BundleOfHis_v_i_0 + (- ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis_v_i_1 + (- ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      BundleOfHis_v_u = ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz) ;
      BundleOfHis_voo = ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz) ;
      BundleOfHis_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis!\n");
      exit(1);
    }
    break;
  case ( BundleOfHis_t3 ):
    if (True == False) {;}
    else if  (BundleOfHis_v >= (131.1)) {
      BundleOfHis_vx_u = BundleOfHis_vx ;
      BundleOfHis_vy_u = BundleOfHis_vy ;
      BundleOfHis_vz_u = BundleOfHis_vz ;
      BundleOfHis_g_u = ((((((((BundleOfHis_v_i_0 + (- ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis_v_i_1 + (- ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  BundleOfHis_t4 ;
      force_init_update = False;
    }

    else if ( BundleOfHis_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis_vx_init = BundleOfHis_vx ;
      slope_BundleOfHis_vx = (BundleOfHis_vx * -6.9) ;
      BundleOfHis_vx_u = (slope_BundleOfHis_vx * d) + BundleOfHis_vx ;
      if ((pstate != cstate) || force_init_update) BundleOfHis_vy_init = BundleOfHis_vy ;
      slope_BundleOfHis_vy = (BundleOfHis_vy * 75.9) ;
      BundleOfHis_vy_u = (slope_BundleOfHis_vy * d) + BundleOfHis_vy ;
      if ((pstate != cstate) || force_init_update) BundleOfHis_vz_init = BundleOfHis_vz ;
      slope_BundleOfHis_vz = (BundleOfHis_vz * 6826.5) ;
      BundleOfHis_vz_u = (slope_BundleOfHis_vz * d) + BundleOfHis_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  BundleOfHis_t3 ;
      force_init_update = False;
      BundleOfHis_g_u = ((((((((BundleOfHis_v_i_0 + (- ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis_v_i_1 + (- ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      BundleOfHis_v_u = ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz) ;
      BundleOfHis_voo = ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz) ;
      BundleOfHis_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis!\n");
      exit(1);
    }
    break;
  case ( BundleOfHis_t4 ):
    if (True == False) {;}
    else if  (BundleOfHis_v <= (30.0)) {
      BundleOfHis_vx_u = BundleOfHis_vx ;
      BundleOfHis_vy_u = BundleOfHis_vy ;
      BundleOfHis_vz_u = BundleOfHis_vz ;
      BundleOfHis_g_u = ((((((((BundleOfHis_v_i_0 + (- ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis_v_i_1 + (- ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  BundleOfHis_t1 ;
      force_init_update = False;
    }

    else if ( BundleOfHis_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis_vx_init = BundleOfHis_vx ;
      slope_BundleOfHis_vx = (BundleOfHis_vx * -33.2) ;
      BundleOfHis_vx_u = (slope_BundleOfHis_vx * d) + BundleOfHis_vx ;
      if ((pstate != cstate) || force_init_update) BundleOfHis_vy_init = BundleOfHis_vy ;
      slope_BundleOfHis_vy = ((BundleOfHis_vy * 11.0) * BundleOfHis_ft) ;
      BundleOfHis_vy_u = (slope_BundleOfHis_vy * d) + BundleOfHis_vy ;
      if ((pstate != cstate) || force_init_update) BundleOfHis_vz_init = BundleOfHis_vz ;
      slope_BundleOfHis_vz = ((BundleOfHis_vz * 2.0) * BundleOfHis_ft) ;
      BundleOfHis_vz_u = (slope_BundleOfHis_vz * d) + BundleOfHis_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  BundleOfHis_t4 ;
      force_init_update = False;
      BundleOfHis_g_u = ((((((((BundleOfHis_v_i_0 + (- ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis_v_i_1 + (- ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      BundleOfHis_v_u = ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz) ;
      BundleOfHis_voo = ((BundleOfHis_vx + (- BundleOfHis_vy)) + BundleOfHis_vz) ;
      BundleOfHis_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis!\n");
      exit(1);
    }
    break;
  }
  BundleOfHis_vx = BundleOfHis_vx_u;
  BundleOfHis_vy = BundleOfHis_vy_u;
  BundleOfHis_vz = BundleOfHis_vz_u;
  BundleOfHis_g = BundleOfHis_g_u;
  BundleOfHis_v = BundleOfHis_v_u;
  BundleOfHis_ft = BundleOfHis_ft_u;
  BundleOfHis_theta = BundleOfHis_theta_u;
  BundleOfHis_v_O = BundleOfHis_v_O_u;
  return cstate;
}