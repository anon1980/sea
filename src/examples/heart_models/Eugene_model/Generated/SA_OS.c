#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double SA_OS_update_c1vd();
extern double SA_OS_update_c2vd();
extern double SA_OS_update_c1md();
extern double SA_OS_update_c2md();
extern double SA_OS_update_buffer_index(double,double,double,double);
extern double SA_OS_update_latch1(double,double);
extern double SA_OS_update_latch2(double,double);
extern double SA_OS_update_ocell1(double,double);
extern double SA_OS_update_ocell2(double,double);
double SA_OS_cell1_v;
double SA_OS_cell1_mode;
double SA_OS_cell2_v;
double SA_OS_cell2_mode;
double SA_OS_cell1_v_replay = 0.0;
double SA_OS_cell2_v_replay = 0.0;


static double  SA_OS_k  =  0.0 ,  SA_OS_cell1_mode_delayed  =  0.0 ,  SA_OS_cell2_mode_delayed  =  0.0 ,  SA_OS_from_cell  =  0.0 ,  SA_OS_cell1_replay_latch  =  0.0 ,  SA_OS_cell2_replay_latch  =  0.0 ,  SA_OS_cell1_v_delayed  =  0.0 ,  SA_OS_cell2_v_delayed  =  0.0 ,  SA_OS_wasted  =  0.0 ; //the continuous vars
static double  SA_OS_k_u , SA_OS_cell1_mode_delayed_u , SA_OS_cell2_mode_delayed_u , SA_OS_from_cell_u , SA_OS_cell1_replay_latch_u , SA_OS_cell2_replay_latch_u , SA_OS_cell1_v_delayed_u , SA_OS_cell2_v_delayed_u , SA_OS_wasted_u ; // and their updates
static double  SA_OS_k_init , SA_OS_cell1_mode_delayed_init , SA_OS_cell2_mode_delayed_init , SA_OS_from_cell_init , SA_OS_cell1_replay_latch_init , SA_OS_cell2_replay_latch_init , SA_OS_cell1_v_delayed_init , SA_OS_cell2_v_delayed_init , SA_OS_wasted_init ; // and their inits
static double  slope_SA_OS_k , slope_SA_OS_cell1_mode_delayed , slope_SA_OS_cell2_mode_delayed , slope_SA_OS_from_cell , slope_SA_OS_cell1_replay_latch , slope_SA_OS_cell2_replay_latch , slope_SA_OS_cell1_v_delayed , slope_SA_OS_cell2_v_delayed , slope_SA_OS_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { SA_OS_idle , SA_OS_annhilate , SA_OS_previous_drection1 , SA_OS_previous_direction2 , SA_OS_wait_cell1 , SA_OS_replay_cell1 , SA_OS_replay_cell2 , SA_OS_wait_cell2 }; // state declarations

enum states SA_OS (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( SA_OS_idle ):
    if (True == False) {;}
    else if  (SA_OS_cell2_mode == (2.0) && (SA_OS_cell1_mode != (2.0))) {
      SA_OS_k_u = 1 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
      cstate =  SA_OS_previous_direction2 ;
      force_init_update = False;
    }
    else if  (SA_OS_cell1_mode == (2.0) && (SA_OS_cell2_mode != (2.0))) {
      SA_OS_k_u = 1 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
      cstate =  SA_OS_previous_drection1 ;
      force_init_update = False;
    }
    else if  (SA_OS_cell1_mode == (2.0) && (SA_OS_cell2_mode == (2.0))) {
      SA_OS_k_u = 1 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
      cstate =  SA_OS_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) SA_OS_k_init = SA_OS_k ;
      slope_SA_OS_k = 1 ;
      SA_OS_k_u = (slope_SA_OS_k * d) + SA_OS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  SA_OS_idle ;
      force_init_update = False;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell1_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_OS!\n");
      exit(1);
    }
    break;
  case ( SA_OS_annhilate ):
    if (True == False) {;}
    else if  (SA_OS_cell1_mode != (2.0) && (SA_OS_cell2_mode != (2.0))) {
      SA_OS_k_u = 1 ;
      SA_OS_from_cell_u = 0 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
      cstate =  SA_OS_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) SA_OS_k_init = SA_OS_k ;
      slope_SA_OS_k = 1 ;
      SA_OS_k_u = (slope_SA_OS_k * d) + SA_OS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  SA_OS_annhilate ;
      force_init_update = False;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell1_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_OS!\n");
      exit(1);
    }
    break;
  case ( SA_OS_previous_drection1 ):
    if (True == False) {;}
    else if  (SA_OS_from_cell == (1.0)) {
      SA_OS_k_u = 1 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
      cstate =  SA_OS_wait_cell1 ;
      force_init_update = False;
    }
    else if  (SA_OS_from_cell == (0.0)) {
      SA_OS_k_u = 1 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
      cstate =  SA_OS_wait_cell1 ;
      force_init_update = False;
    }
    else if  (SA_OS_from_cell == (2.0) && (SA_OS_cell2_mode_delayed == (0.0))) {
      SA_OS_k_u = 1 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
      cstate =  SA_OS_wait_cell1 ;
      force_init_update = False;
    }
    else if  (SA_OS_from_cell == (2.0) && (SA_OS_cell2_mode_delayed != (0.0))) {
      SA_OS_k_u = 1 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
      cstate =  SA_OS_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) SA_OS_k_init = SA_OS_k ;
      slope_SA_OS_k = 1 ;
      SA_OS_k_u = (slope_SA_OS_k * d) + SA_OS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  SA_OS_previous_drection1 ;
      force_init_update = False;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell1_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_OS!\n");
      exit(1);
    }
    break;
  case ( SA_OS_previous_direction2 ):
    if (True == False) {;}
    else if  (SA_OS_from_cell == (1.0) && (SA_OS_cell1_mode_delayed != (0.0))) {
      SA_OS_k_u = 1 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
      cstate =  SA_OS_annhilate ;
      force_init_update = False;
    }
    else if  (SA_OS_from_cell == (2.0)) {
      SA_OS_k_u = 1 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
      cstate =  SA_OS_replay_cell1 ;
      force_init_update = False;
    }
    else if  (SA_OS_from_cell == (0.0)) {
      SA_OS_k_u = 1 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
      cstate =  SA_OS_replay_cell1 ;
      force_init_update = False;
    }
    else if  (SA_OS_from_cell == (1.0) && (SA_OS_cell1_mode_delayed == (0.0))) {
      SA_OS_k_u = 1 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
      cstate =  SA_OS_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) SA_OS_k_init = SA_OS_k ;
      slope_SA_OS_k = 1 ;
      SA_OS_k_u = (slope_SA_OS_k * d) + SA_OS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  SA_OS_previous_direction2 ;
      force_init_update = False;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell1_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_OS!\n");
      exit(1);
    }
    break;
  case ( SA_OS_wait_cell1 ):
    if (True == False) {;}
    else if  (SA_OS_cell2_mode == (2.0)) {
      SA_OS_k_u = 1 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
      cstate =  SA_OS_annhilate ;
      force_init_update = False;
    }
    else if  (SA_OS_k >= (20.0)) {
      SA_OS_from_cell_u = 1 ;
      SA_OS_cell1_replay_latch_u = 1 ;
      SA_OS_k_u = 1 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
      cstate =  SA_OS_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) SA_OS_k_init = SA_OS_k ;
      slope_SA_OS_k = 1 ;
      SA_OS_k_u = (slope_SA_OS_k * d) + SA_OS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  SA_OS_wait_cell1 ;
      force_init_update = False;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell1_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_OS!\n");
      exit(1);
    }
    break;
  case ( SA_OS_replay_cell1 ):
    if (True == False) {;}
    else if  (SA_OS_cell1_mode == (2.0)) {
      SA_OS_k_u = 1 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
      cstate =  SA_OS_annhilate ;
      force_init_update = False;
    }
    else if  (SA_OS_k >= (20.0)) {
      SA_OS_from_cell_u = 2 ;
      SA_OS_cell2_replay_latch_u = 1 ;
      SA_OS_k_u = 1 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
      cstate =  SA_OS_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) SA_OS_k_init = SA_OS_k ;
      slope_SA_OS_k = 1 ;
      SA_OS_k_u = (slope_SA_OS_k * d) + SA_OS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  SA_OS_replay_cell1 ;
      force_init_update = False;
      SA_OS_cell1_replay_latch_u = 1 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell1_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_OS!\n");
      exit(1);
    }
    break;
  case ( SA_OS_replay_cell2 ):
    if (True == False) {;}
    else if  (SA_OS_k >= (10.0)) {
      SA_OS_k_u = 1 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
      cstate =  SA_OS_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) SA_OS_k_init = SA_OS_k ;
      slope_SA_OS_k = 1 ;
      SA_OS_k_u = (slope_SA_OS_k * d) + SA_OS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  SA_OS_replay_cell2 ;
      force_init_update = False;
      SA_OS_cell2_replay_latch_u = 1 ;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell1_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_OS!\n");
      exit(1);
    }
    break;
  case ( SA_OS_wait_cell2 ):
    if (True == False) {;}
    else if  (SA_OS_k >= (10.0)) {
      SA_OS_k_u = 1 ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
      cstate =  SA_OS_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) SA_OS_k_init = SA_OS_k ;
      slope_SA_OS_k = 1 ;
      SA_OS_k_u = (slope_SA_OS_k * d) + SA_OS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  SA_OS_wait_cell2 ;
      force_init_update = False;
      SA_OS_cell1_v_delayed_u = SA_OS_update_c1vd () ;
      SA_OS_cell2_v_delayed_u = SA_OS_update_c2vd () ;
      SA_OS_cell1_mode_delayed_u = SA_OS_update_c1md () ;
      SA_OS_cell2_mode_delayed_u = SA_OS_update_c2md () ;
      SA_OS_wasted_u = SA_OS_update_buffer_index (SA_OS_cell1_v,SA_OS_cell2_v,SA_OS_cell1_mode,SA_OS_cell2_mode) ;
      SA_OS_cell1_replay_latch_u = SA_OS_update_latch1 (SA_OS_cell1_mode_delayed,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_replay_latch_u = SA_OS_update_latch2 (SA_OS_cell2_mode_delayed,SA_OS_cell2_replay_latch_u) ;
      SA_OS_cell1_v_replay = SA_OS_update_ocell1 (SA_OS_cell1_v_delayed_u,SA_OS_cell1_replay_latch_u) ;
      SA_OS_cell2_v_replay = SA_OS_update_ocell2 (SA_OS_cell2_v_delayed_u,SA_OS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_OS!\n");
      exit(1);
    }
    break;
  }
  SA_OS_k = SA_OS_k_u;
  SA_OS_cell1_mode_delayed = SA_OS_cell1_mode_delayed_u;
  SA_OS_cell2_mode_delayed = SA_OS_cell2_mode_delayed_u;
  SA_OS_from_cell = SA_OS_from_cell_u;
  SA_OS_cell1_replay_latch = SA_OS_cell1_replay_latch_u;
  SA_OS_cell2_replay_latch = SA_OS_cell2_replay_latch_u;
  SA_OS_cell1_v_delayed = SA_OS_cell1_v_delayed_u;
  SA_OS_cell2_v_delayed = SA_OS_cell2_v_delayed_u;
  SA_OS_wasted = SA_OS_wasted_u;
  return cstate;
}