#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double LVA_LVS_update_c1vd();
extern double LVA_LVS_update_c2vd();
extern double LVA_LVS_update_c1md();
extern double LVA_LVS_update_c2md();
extern double LVA_LVS_update_buffer_index(double,double,double,double);
extern double LVA_LVS_update_latch1(double,double);
extern double LVA_LVS_update_latch2(double,double);
extern double LVA_LVS_update_ocell1(double,double);
extern double LVA_LVS_update_ocell2(double,double);
double LVA_LVS_cell1_v;
double LVA_LVS_cell1_mode;
double LVA_LVS_cell2_v;
double LVA_LVS_cell2_mode;
double LVA_LVS_cell1_v_replay = 0.0;
double LVA_LVS_cell2_v_replay = 0.0;


static double  LVA_LVS_k  =  0.0 ,  LVA_LVS_cell1_mode_delayed  =  0.0 ,  LVA_LVS_cell2_mode_delayed  =  0.0 ,  LVA_LVS_from_cell  =  0.0 ,  LVA_LVS_cell1_replay_latch  =  0.0 ,  LVA_LVS_cell2_replay_latch  =  0.0 ,  LVA_LVS_cell1_v_delayed  =  0.0 ,  LVA_LVS_cell2_v_delayed  =  0.0 ,  LVA_LVS_wasted  =  0.0 ; //the continuous vars
static double  LVA_LVS_k_u , LVA_LVS_cell1_mode_delayed_u , LVA_LVS_cell2_mode_delayed_u , LVA_LVS_from_cell_u , LVA_LVS_cell1_replay_latch_u , LVA_LVS_cell2_replay_latch_u , LVA_LVS_cell1_v_delayed_u , LVA_LVS_cell2_v_delayed_u , LVA_LVS_wasted_u ; // and their updates
static double  LVA_LVS_k_init , LVA_LVS_cell1_mode_delayed_init , LVA_LVS_cell2_mode_delayed_init , LVA_LVS_from_cell_init , LVA_LVS_cell1_replay_latch_init , LVA_LVS_cell2_replay_latch_init , LVA_LVS_cell1_v_delayed_init , LVA_LVS_cell2_v_delayed_init , LVA_LVS_wasted_init ; // and their inits
static double  slope_LVA_LVS_k , slope_LVA_LVS_cell1_mode_delayed , slope_LVA_LVS_cell2_mode_delayed , slope_LVA_LVS_from_cell , slope_LVA_LVS_cell1_replay_latch , slope_LVA_LVS_cell2_replay_latch , slope_LVA_LVS_cell1_v_delayed , slope_LVA_LVS_cell2_v_delayed , slope_LVA_LVS_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { LVA_LVS_idle , LVA_LVS_annhilate , LVA_LVS_previous_drection1 , LVA_LVS_previous_direction2 , LVA_LVS_wait_cell1 , LVA_LVS_replay_cell1 , LVA_LVS_replay_cell2 , LVA_LVS_wait_cell2 }; // state declarations

enum states LVA_LVS (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( LVA_LVS_idle ):
    if (True == False) {;}
    else if  (LVA_LVS_cell2_mode == (2.0) && (LVA_LVS_cell1_mode != (2.0))) {
      LVA_LVS_k_u = 1 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
      cstate =  LVA_LVS_previous_direction2 ;
      force_init_update = False;
    }
    else if  (LVA_LVS_cell1_mode == (2.0) && (LVA_LVS_cell2_mode != (2.0))) {
      LVA_LVS_k_u = 1 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
      cstate =  LVA_LVS_previous_drection1 ;
      force_init_update = False;
    }
    else if  (LVA_LVS_cell1_mode == (2.0) && (LVA_LVS_cell2_mode == (2.0))) {
      LVA_LVS_k_u = 1 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
      cstate =  LVA_LVS_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_LVS_k_init = LVA_LVS_k ;
      slope_LVA_LVS_k = 1 ;
      LVA_LVS_k_u = (slope_LVA_LVS_k * d) + LVA_LVS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_LVS_idle ;
      force_init_update = False;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell1_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_LVS!\n");
      exit(1);
    }
    break;
  case ( LVA_LVS_annhilate ):
    if (True == False) {;}
    else if  (LVA_LVS_cell1_mode != (2.0) && (LVA_LVS_cell2_mode != (2.0))) {
      LVA_LVS_k_u = 1 ;
      LVA_LVS_from_cell_u = 0 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
      cstate =  LVA_LVS_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_LVS_k_init = LVA_LVS_k ;
      slope_LVA_LVS_k = 1 ;
      LVA_LVS_k_u = (slope_LVA_LVS_k * d) + LVA_LVS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_LVS_annhilate ;
      force_init_update = False;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell1_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_LVS!\n");
      exit(1);
    }
    break;
  case ( LVA_LVS_previous_drection1 ):
    if (True == False) {;}
    else if  (LVA_LVS_from_cell == (1.0)) {
      LVA_LVS_k_u = 1 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
      cstate =  LVA_LVS_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LVA_LVS_from_cell == (0.0)) {
      LVA_LVS_k_u = 1 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
      cstate =  LVA_LVS_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LVA_LVS_from_cell == (2.0) && (LVA_LVS_cell2_mode_delayed == (0.0))) {
      LVA_LVS_k_u = 1 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
      cstate =  LVA_LVS_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LVA_LVS_from_cell == (2.0) && (LVA_LVS_cell2_mode_delayed != (0.0))) {
      LVA_LVS_k_u = 1 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
      cstate =  LVA_LVS_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_LVS_k_init = LVA_LVS_k ;
      slope_LVA_LVS_k = 1 ;
      LVA_LVS_k_u = (slope_LVA_LVS_k * d) + LVA_LVS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_LVS_previous_drection1 ;
      force_init_update = False;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell1_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_LVS!\n");
      exit(1);
    }
    break;
  case ( LVA_LVS_previous_direction2 ):
    if (True == False) {;}
    else if  (LVA_LVS_from_cell == (1.0) && (LVA_LVS_cell1_mode_delayed != (0.0))) {
      LVA_LVS_k_u = 1 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
      cstate =  LVA_LVS_annhilate ;
      force_init_update = False;
    }
    else if  (LVA_LVS_from_cell == (2.0)) {
      LVA_LVS_k_u = 1 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
      cstate =  LVA_LVS_replay_cell1 ;
      force_init_update = False;
    }
    else if  (LVA_LVS_from_cell == (0.0)) {
      LVA_LVS_k_u = 1 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
      cstate =  LVA_LVS_replay_cell1 ;
      force_init_update = False;
    }
    else if  (LVA_LVS_from_cell == (1.0) && (LVA_LVS_cell1_mode_delayed == (0.0))) {
      LVA_LVS_k_u = 1 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
      cstate =  LVA_LVS_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_LVS_k_init = LVA_LVS_k ;
      slope_LVA_LVS_k = 1 ;
      LVA_LVS_k_u = (slope_LVA_LVS_k * d) + LVA_LVS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_LVS_previous_direction2 ;
      force_init_update = False;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell1_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_LVS!\n");
      exit(1);
    }
    break;
  case ( LVA_LVS_wait_cell1 ):
    if (True == False) {;}
    else if  (LVA_LVS_cell2_mode == (2.0)) {
      LVA_LVS_k_u = 1 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
      cstate =  LVA_LVS_annhilate ;
      force_init_update = False;
    }
    else if  (LVA_LVS_k >= (15.0)) {
      LVA_LVS_from_cell_u = 1 ;
      LVA_LVS_cell1_replay_latch_u = 1 ;
      LVA_LVS_k_u = 1 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
      cstate =  LVA_LVS_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_LVS_k_init = LVA_LVS_k ;
      slope_LVA_LVS_k = 1 ;
      LVA_LVS_k_u = (slope_LVA_LVS_k * d) + LVA_LVS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_LVS_wait_cell1 ;
      force_init_update = False;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell1_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_LVS!\n");
      exit(1);
    }
    break;
  case ( LVA_LVS_replay_cell1 ):
    if (True == False) {;}
    else if  (LVA_LVS_cell1_mode == (2.0)) {
      LVA_LVS_k_u = 1 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
      cstate =  LVA_LVS_annhilate ;
      force_init_update = False;
    }
    else if  (LVA_LVS_k >= (15.0)) {
      LVA_LVS_from_cell_u = 2 ;
      LVA_LVS_cell2_replay_latch_u = 1 ;
      LVA_LVS_k_u = 1 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
      cstate =  LVA_LVS_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_LVS_k_init = LVA_LVS_k ;
      slope_LVA_LVS_k = 1 ;
      LVA_LVS_k_u = (slope_LVA_LVS_k * d) + LVA_LVS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_LVS_replay_cell1 ;
      force_init_update = False;
      LVA_LVS_cell1_replay_latch_u = 1 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell1_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_LVS!\n");
      exit(1);
    }
    break;
  case ( LVA_LVS_replay_cell2 ):
    if (True == False) {;}
    else if  (LVA_LVS_k >= (10.0)) {
      LVA_LVS_k_u = 1 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
      cstate =  LVA_LVS_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_LVS_k_init = LVA_LVS_k ;
      slope_LVA_LVS_k = 1 ;
      LVA_LVS_k_u = (slope_LVA_LVS_k * d) + LVA_LVS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_LVS_replay_cell2 ;
      force_init_update = False;
      LVA_LVS_cell2_replay_latch_u = 1 ;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell1_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_LVS!\n");
      exit(1);
    }
    break;
  case ( LVA_LVS_wait_cell2 ):
    if (True == False) {;}
    else if  (LVA_LVS_k >= (10.0)) {
      LVA_LVS_k_u = 1 ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
      cstate =  LVA_LVS_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_LVS_k_init = LVA_LVS_k ;
      slope_LVA_LVS_k = 1 ;
      LVA_LVS_k_u = (slope_LVA_LVS_k * d) + LVA_LVS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_LVS_wait_cell2 ;
      force_init_update = False;
      LVA_LVS_cell1_v_delayed_u = LVA_LVS_update_c1vd () ;
      LVA_LVS_cell2_v_delayed_u = LVA_LVS_update_c2vd () ;
      LVA_LVS_cell1_mode_delayed_u = LVA_LVS_update_c1md () ;
      LVA_LVS_cell2_mode_delayed_u = LVA_LVS_update_c2md () ;
      LVA_LVS_wasted_u = LVA_LVS_update_buffer_index (LVA_LVS_cell1_v,LVA_LVS_cell2_v,LVA_LVS_cell1_mode,LVA_LVS_cell2_mode) ;
      LVA_LVS_cell1_replay_latch_u = LVA_LVS_update_latch1 (LVA_LVS_cell1_mode_delayed,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_replay_latch_u = LVA_LVS_update_latch2 (LVA_LVS_cell2_mode_delayed,LVA_LVS_cell2_replay_latch_u) ;
      LVA_LVS_cell1_v_replay = LVA_LVS_update_ocell1 (LVA_LVS_cell1_v_delayed_u,LVA_LVS_cell1_replay_latch_u) ;
      LVA_LVS_cell2_v_replay = LVA_LVS_update_ocell2 (LVA_LVS_cell2_v_delayed_u,LVA_LVS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_LVS!\n");
      exit(1);
    }
    break;
  }
  LVA_LVS_k = LVA_LVS_k_u;
  LVA_LVS_cell1_mode_delayed = LVA_LVS_cell1_mode_delayed_u;
  LVA_LVS_cell2_mode_delayed = LVA_LVS_cell2_mode_delayed_u;
  LVA_LVS_from_cell = LVA_LVS_from_cell_u;
  LVA_LVS_cell1_replay_latch = LVA_LVS_cell1_replay_latch_u;
  LVA_LVS_cell2_replay_latch = LVA_LVS_cell2_replay_latch_u;
  LVA_LVS_cell1_v_delayed = LVA_LVS_cell1_v_delayed_u;
  LVA_LVS_cell2_v_delayed = LVA_LVS_cell2_v_delayed_u;
  LVA_LVS_wasted = LVA_LVS_wasted_u;
  return cstate;
}