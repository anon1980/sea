#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double OS_Fast_update_c1vd();
extern double OS_Fast_update_c2vd();
extern double OS_Fast_update_c1md();
extern double OS_Fast_update_c2md();
extern double OS_Fast_update_buffer_index(double,double,double,double);
extern double OS_Fast_update_latch1(double,double);
extern double OS_Fast_update_latch2(double,double);
extern double OS_Fast_update_ocell1(double,double);
extern double OS_Fast_update_ocell2(double,double);
double OS_Fast_cell1_v;
double OS_Fast_cell1_mode;
double OS_Fast_cell2_v;
double OS_Fast_cell2_mode;
double OS_Fast_cell1_v_replay = 0.0;
double OS_Fast_cell2_v_replay = 0.0;


static double  OS_Fast_k  =  0.0 ,  OS_Fast_cell1_mode_delayed  =  0.0 ,  OS_Fast_cell2_mode_delayed  =  0.0 ,  OS_Fast_from_cell  =  0.0 ,  OS_Fast_cell1_replay_latch  =  0.0 ,  OS_Fast_cell2_replay_latch  =  0.0 ,  OS_Fast_cell1_v_delayed  =  0.0 ,  OS_Fast_cell2_v_delayed  =  0.0 ,  OS_Fast_wasted  =  0.0 ; //the continuous vars
static double  OS_Fast_k_u , OS_Fast_cell1_mode_delayed_u , OS_Fast_cell2_mode_delayed_u , OS_Fast_from_cell_u , OS_Fast_cell1_replay_latch_u , OS_Fast_cell2_replay_latch_u , OS_Fast_cell1_v_delayed_u , OS_Fast_cell2_v_delayed_u , OS_Fast_wasted_u ; // and their updates
static double  OS_Fast_k_init , OS_Fast_cell1_mode_delayed_init , OS_Fast_cell2_mode_delayed_init , OS_Fast_from_cell_init , OS_Fast_cell1_replay_latch_init , OS_Fast_cell2_replay_latch_init , OS_Fast_cell1_v_delayed_init , OS_Fast_cell2_v_delayed_init , OS_Fast_wasted_init ; // and their inits
static double  slope_OS_Fast_k , slope_OS_Fast_cell1_mode_delayed , slope_OS_Fast_cell2_mode_delayed , slope_OS_Fast_from_cell , slope_OS_Fast_cell1_replay_latch , slope_OS_Fast_cell2_replay_latch , slope_OS_Fast_cell1_v_delayed , slope_OS_Fast_cell2_v_delayed , slope_OS_Fast_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { OS_Fast_idle , OS_Fast_annhilate , OS_Fast_previous_drection1 , OS_Fast_previous_direction2 , OS_Fast_wait_cell1 , OS_Fast_replay_cell1 , OS_Fast_replay_cell2 , OS_Fast_wait_cell2 }; // state declarations

enum states OS_Fast (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( OS_Fast_idle ):
    if (True == False) {;}
    else if  (OS_Fast_cell2_mode == (2.0) && (OS_Fast_cell1_mode != (2.0))) {
      OS_Fast_k_u = 1 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
      cstate =  OS_Fast_previous_direction2 ;
      force_init_update = False;
    }
    else if  (OS_Fast_cell1_mode == (2.0) && (OS_Fast_cell2_mode != (2.0))) {
      OS_Fast_k_u = 1 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
      cstate =  OS_Fast_previous_drection1 ;
      force_init_update = False;
    }
    else if  (OS_Fast_cell1_mode == (2.0) && (OS_Fast_cell2_mode == (2.0))) {
      OS_Fast_k_u = 1 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
      cstate =  OS_Fast_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) OS_Fast_k_init = OS_Fast_k ;
      slope_OS_Fast_k = 1 ;
      OS_Fast_k_u = (slope_OS_Fast_k * d) + OS_Fast_k ;
      /* Possible Saturation */
      
      
      
      cstate =  OS_Fast_idle ;
      force_init_update = False;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell1_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Fast!\n");
      exit(1);
    }
    break;
  case ( OS_Fast_annhilate ):
    if (True == False) {;}
    else if  (OS_Fast_cell1_mode != (2.0) && (OS_Fast_cell2_mode != (2.0))) {
      OS_Fast_k_u = 1 ;
      OS_Fast_from_cell_u = 0 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
      cstate =  OS_Fast_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) OS_Fast_k_init = OS_Fast_k ;
      slope_OS_Fast_k = 1 ;
      OS_Fast_k_u = (slope_OS_Fast_k * d) + OS_Fast_k ;
      /* Possible Saturation */
      
      
      
      cstate =  OS_Fast_annhilate ;
      force_init_update = False;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell1_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Fast!\n");
      exit(1);
    }
    break;
  case ( OS_Fast_previous_drection1 ):
    if (True == False) {;}
    else if  (OS_Fast_from_cell == (1.0)) {
      OS_Fast_k_u = 1 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
      cstate =  OS_Fast_wait_cell1 ;
      force_init_update = False;
    }
    else if  (OS_Fast_from_cell == (0.0)) {
      OS_Fast_k_u = 1 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
      cstate =  OS_Fast_wait_cell1 ;
      force_init_update = False;
    }
    else if  (OS_Fast_from_cell == (2.0) && (OS_Fast_cell2_mode_delayed == (0.0))) {
      OS_Fast_k_u = 1 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
      cstate =  OS_Fast_wait_cell1 ;
      force_init_update = False;
    }
    else if  (OS_Fast_from_cell == (2.0) && (OS_Fast_cell2_mode_delayed != (0.0))) {
      OS_Fast_k_u = 1 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
      cstate =  OS_Fast_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) OS_Fast_k_init = OS_Fast_k ;
      slope_OS_Fast_k = 1 ;
      OS_Fast_k_u = (slope_OS_Fast_k * d) + OS_Fast_k ;
      /* Possible Saturation */
      
      
      
      cstate =  OS_Fast_previous_drection1 ;
      force_init_update = False;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell1_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Fast!\n");
      exit(1);
    }
    break;
  case ( OS_Fast_previous_direction2 ):
    if (True == False) {;}
    else if  (OS_Fast_from_cell == (1.0) && (OS_Fast_cell1_mode_delayed != (0.0))) {
      OS_Fast_k_u = 1 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
      cstate =  OS_Fast_annhilate ;
      force_init_update = False;
    }
    else if  (OS_Fast_from_cell == (2.0)) {
      OS_Fast_k_u = 1 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
      cstate =  OS_Fast_replay_cell1 ;
      force_init_update = False;
    }
    else if  (OS_Fast_from_cell == (0.0)) {
      OS_Fast_k_u = 1 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
      cstate =  OS_Fast_replay_cell1 ;
      force_init_update = False;
    }
    else if  (OS_Fast_from_cell == (1.0) && (OS_Fast_cell1_mode_delayed == (0.0))) {
      OS_Fast_k_u = 1 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
      cstate =  OS_Fast_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) OS_Fast_k_init = OS_Fast_k ;
      slope_OS_Fast_k = 1 ;
      OS_Fast_k_u = (slope_OS_Fast_k * d) + OS_Fast_k ;
      /* Possible Saturation */
      
      
      
      cstate =  OS_Fast_previous_direction2 ;
      force_init_update = False;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell1_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Fast!\n");
      exit(1);
    }
    break;
  case ( OS_Fast_wait_cell1 ):
    if (True == False) {;}
    else if  (OS_Fast_cell2_mode == (2.0)) {
      OS_Fast_k_u = 1 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
      cstate =  OS_Fast_annhilate ;
      force_init_update = False;
    }
    else if  (OS_Fast_k >= (20.0)) {
      OS_Fast_from_cell_u = 1 ;
      OS_Fast_cell1_replay_latch_u = 1 ;
      OS_Fast_k_u = 1 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
      cstate =  OS_Fast_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) OS_Fast_k_init = OS_Fast_k ;
      slope_OS_Fast_k = 1 ;
      OS_Fast_k_u = (slope_OS_Fast_k * d) + OS_Fast_k ;
      /* Possible Saturation */
      
      
      
      cstate =  OS_Fast_wait_cell1 ;
      force_init_update = False;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell1_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Fast!\n");
      exit(1);
    }
    break;
  case ( OS_Fast_replay_cell1 ):
    if (True == False) {;}
    else if  (OS_Fast_cell1_mode == (2.0)) {
      OS_Fast_k_u = 1 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
      cstate =  OS_Fast_annhilate ;
      force_init_update = False;
    }
    else if  (OS_Fast_k >= (20.0)) {
      OS_Fast_from_cell_u = 2 ;
      OS_Fast_cell2_replay_latch_u = 1 ;
      OS_Fast_k_u = 1 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
      cstate =  OS_Fast_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) OS_Fast_k_init = OS_Fast_k ;
      slope_OS_Fast_k = 1 ;
      OS_Fast_k_u = (slope_OS_Fast_k * d) + OS_Fast_k ;
      /* Possible Saturation */
      
      
      
      cstate =  OS_Fast_replay_cell1 ;
      force_init_update = False;
      OS_Fast_cell1_replay_latch_u = 1 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell1_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Fast!\n");
      exit(1);
    }
    break;
  case ( OS_Fast_replay_cell2 ):
    if (True == False) {;}
    else if  (OS_Fast_k >= (10.0)) {
      OS_Fast_k_u = 1 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
      cstate =  OS_Fast_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) OS_Fast_k_init = OS_Fast_k ;
      slope_OS_Fast_k = 1 ;
      OS_Fast_k_u = (slope_OS_Fast_k * d) + OS_Fast_k ;
      /* Possible Saturation */
      
      
      
      cstate =  OS_Fast_replay_cell2 ;
      force_init_update = False;
      OS_Fast_cell2_replay_latch_u = 1 ;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell1_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Fast!\n");
      exit(1);
    }
    break;
  case ( OS_Fast_wait_cell2 ):
    if (True == False) {;}
    else if  (OS_Fast_k >= (10.0)) {
      OS_Fast_k_u = 1 ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
      cstate =  OS_Fast_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) OS_Fast_k_init = OS_Fast_k ;
      slope_OS_Fast_k = 1 ;
      OS_Fast_k_u = (slope_OS_Fast_k * d) + OS_Fast_k ;
      /* Possible Saturation */
      
      
      
      cstate =  OS_Fast_wait_cell2 ;
      force_init_update = False;
      OS_Fast_cell1_v_delayed_u = OS_Fast_update_c1vd () ;
      OS_Fast_cell2_v_delayed_u = OS_Fast_update_c2vd () ;
      OS_Fast_cell1_mode_delayed_u = OS_Fast_update_c1md () ;
      OS_Fast_cell2_mode_delayed_u = OS_Fast_update_c2md () ;
      OS_Fast_wasted_u = OS_Fast_update_buffer_index (OS_Fast_cell1_v,OS_Fast_cell2_v,OS_Fast_cell1_mode,OS_Fast_cell2_mode) ;
      OS_Fast_cell1_replay_latch_u = OS_Fast_update_latch1 (OS_Fast_cell1_mode_delayed,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_replay_latch_u = OS_Fast_update_latch2 (OS_Fast_cell2_mode_delayed,OS_Fast_cell2_replay_latch_u) ;
      OS_Fast_cell1_v_replay = OS_Fast_update_ocell1 (OS_Fast_cell1_v_delayed_u,OS_Fast_cell1_replay_latch_u) ;
      OS_Fast_cell2_v_replay = OS_Fast_update_ocell2 (OS_Fast_cell2_v_delayed_u,OS_Fast_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Fast!\n");
      exit(1);
    }
    break;
  }
  OS_Fast_k = OS_Fast_k_u;
  OS_Fast_cell1_mode_delayed = OS_Fast_cell1_mode_delayed_u;
  OS_Fast_cell2_mode_delayed = OS_Fast_cell2_mode_delayed_u;
  OS_Fast_from_cell = OS_Fast_from_cell_u;
  OS_Fast_cell1_replay_latch = OS_Fast_cell1_replay_latch_u;
  OS_Fast_cell2_replay_latch = OS_Fast_cell2_replay_latch_u;
  OS_Fast_cell1_v_delayed = OS_Fast_cell1_v_delayed_u;
  OS_Fast_cell2_v_delayed = OS_Fast_cell2_v_delayed_u;
  OS_Fast_wasted = OS_Fast_wasted_u;
  return cstate;
}