#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double CoronarySinus_v_i_0;
double CoronarySinus_v_i_1;
double CoronarySinus_voo = 0.0;
double CoronarySinus_state = 0.0;


static double  CoronarySinus_vx  =  0 ,  CoronarySinus_vy  =  0 ,  CoronarySinus_vz  =  0 ,  CoronarySinus_g  =  0 ,  CoronarySinus_v  =  0 ,  CoronarySinus_ft  =  0 ,  CoronarySinus_theta  =  0 ,  CoronarySinus_v_O  =  0 ; //the continuous vars
static double  CoronarySinus_vx_u , CoronarySinus_vy_u , CoronarySinus_vz_u , CoronarySinus_g_u , CoronarySinus_v_u , CoronarySinus_ft_u , CoronarySinus_theta_u , CoronarySinus_v_O_u ; // and their updates
static double  CoronarySinus_vx_init , CoronarySinus_vy_init , CoronarySinus_vz_init , CoronarySinus_g_init , CoronarySinus_v_init , CoronarySinus_ft_init , CoronarySinus_theta_init , CoronarySinus_v_O_init ; // and their inits
static double  slope_CoronarySinus_vx , slope_CoronarySinus_vy , slope_CoronarySinus_vz , slope_CoronarySinus_g , slope_CoronarySinus_v , slope_CoronarySinus_ft , slope_CoronarySinus_theta , slope_CoronarySinus_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { CoronarySinus_t1 , CoronarySinus_t2 , CoronarySinus_t3 , CoronarySinus_t4 }; // state declarations

enum states CoronarySinus (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( CoronarySinus_t1 ):
    if (True == False) {;}
    else if  (CoronarySinus_g > (44.5)) {
      CoronarySinus_vx_u = (0.3 * CoronarySinus_v) ;
      CoronarySinus_vy_u = 0 ;
      CoronarySinus_vz_u = (0.7 * CoronarySinus_v) ;
      CoronarySinus_g_u = ((((((((CoronarySinus_v_i_0 + (- ((CoronarySinus_vx + (- CoronarySinus_vy)) + CoronarySinus_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      CoronarySinus_theta_u = (CoronarySinus_v / 30.0) ;
      CoronarySinus_v_O_u = (131.1 + (- (80.1 * pow ( ((CoronarySinus_v / 30.0)) , (0.5) )))) ;
      CoronarySinus_ft_u = f (CoronarySinus_theta,4.0e-2) ;
      cstate =  CoronarySinus_t2 ;
      force_init_update = False;
    }

    else if ( CoronarySinus_v <= (44.5)
               && 
              CoronarySinus_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) CoronarySinus_vx_init = CoronarySinus_vx ;
      slope_CoronarySinus_vx = (CoronarySinus_vx * -8.7) ;
      CoronarySinus_vx_u = (slope_CoronarySinus_vx * d) + CoronarySinus_vx ;
      if ((pstate != cstate) || force_init_update) CoronarySinus_vy_init = CoronarySinus_vy ;
      slope_CoronarySinus_vy = (CoronarySinus_vy * -190.9) ;
      CoronarySinus_vy_u = (slope_CoronarySinus_vy * d) + CoronarySinus_vy ;
      if ((pstate != cstate) || force_init_update) CoronarySinus_vz_init = CoronarySinus_vz ;
      slope_CoronarySinus_vz = (CoronarySinus_vz * -190.4) ;
      CoronarySinus_vz_u = (slope_CoronarySinus_vz * d) + CoronarySinus_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  CoronarySinus_t1 ;
      force_init_update = False;
      CoronarySinus_g_u = ((((((((CoronarySinus_v_i_0 + (- ((CoronarySinus_vx + (- CoronarySinus_vy)) + CoronarySinus_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      CoronarySinus_v_u = ((CoronarySinus_vx + (- CoronarySinus_vy)) + CoronarySinus_vz) ;
      CoronarySinus_voo = ((CoronarySinus_vx + (- CoronarySinus_vy)) + CoronarySinus_vz) ;
      CoronarySinus_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CoronarySinus!\n");
      exit(1);
    }
    break;
  case ( CoronarySinus_t2 ):
    if (True == False) {;}
    else if  (CoronarySinus_v >= (44.5)) {
      CoronarySinus_vx_u = CoronarySinus_vx ;
      CoronarySinus_vy_u = CoronarySinus_vy ;
      CoronarySinus_vz_u = CoronarySinus_vz ;
      CoronarySinus_g_u = ((((((((CoronarySinus_v_i_0 + (- ((CoronarySinus_vx + (- CoronarySinus_vy)) + CoronarySinus_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  CoronarySinus_t3 ;
      force_init_update = False;
    }
    else if  (CoronarySinus_g <= (44.5)
               && CoronarySinus_v < (44.5)) {
      CoronarySinus_vx_u = CoronarySinus_vx ;
      CoronarySinus_vy_u = CoronarySinus_vy ;
      CoronarySinus_vz_u = CoronarySinus_vz ;
      CoronarySinus_g_u = ((((((((CoronarySinus_v_i_0 + (- ((CoronarySinus_vx + (- CoronarySinus_vy)) + CoronarySinus_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  CoronarySinus_t1 ;
      force_init_update = False;
    }

    else if ( CoronarySinus_v < (44.5)
               && 
              CoronarySinus_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) CoronarySinus_vx_init = CoronarySinus_vx ;
      slope_CoronarySinus_vx = ((CoronarySinus_vx * -23.6) + (777200.0 * CoronarySinus_g)) ;
      CoronarySinus_vx_u = (slope_CoronarySinus_vx * d) + CoronarySinus_vx ;
      if ((pstate != cstate) || force_init_update) CoronarySinus_vy_init = CoronarySinus_vy ;
      slope_CoronarySinus_vy = ((CoronarySinus_vy * -45.5) + (58900.0 * CoronarySinus_g)) ;
      CoronarySinus_vy_u = (slope_CoronarySinus_vy * d) + CoronarySinus_vy ;
      if ((pstate != cstate) || force_init_update) CoronarySinus_vz_init = CoronarySinus_vz ;
      slope_CoronarySinus_vz = ((CoronarySinus_vz * -12.9) + (276600.0 * CoronarySinus_g)) ;
      CoronarySinus_vz_u = (slope_CoronarySinus_vz * d) + CoronarySinus_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  CoronarySinus_t2 ;
      force_init_update = False;
      CoronarySinus_g_u = ((((((((CoronarySinus_v_i_0 + (- ((CoronarySinus_vx + (- CoronarySinus_vy)) + CoronarySinus_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      CoronarySinus_v_u = ((CoronarySinus_vx + (- CoronarySinus_vy)) + CoronarySinus_vz) ;
      CoronarySinus_voo = ((CoronarySinus_vx + (- CoronarySinus_vy)) + CoronarySinus_vz) ;
      CoronarySinus_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CoronarySinus!\n");
      exit(1);
    }
    break;
  case ( CoronarySinus_t3 ):
    if (True == False) {;}
    else if  (CoronarySinus_v >= (131.1)) {
      CoronarySinus_vx_u = CoronarySinus_vx ;
      CoronarySinus_vy_u = CoronarySinus_vy ;
      CoronarySinus_vz_u = CoronarySinus_vz ;
      CoronarySinus_g_u = ((((((((CoronarySinus_v_i_0 + (- ((CoronarySinus_vx + (- CoronarySinus_vy)) + CoronarySinus_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  CoronarySinus_t4 ;
      force_init_update = False;
    }

    else if ( CoronarySinus_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) CoronarySinus_vx_init = CoronarySinus_vx ;
      slope_CoronarySinus_vx = (CoronarySinus_vx * -6.9) ;
      CoronarySinus_vx_u = (slope_CoronarySinus_vx * d) + CoronarySinus_vx ;
      if ((pstate != cstate) || force_init_update) CoronarySinus_vy_init = CoronarySinus_vy ;
      slope_CoronarySinus_vy = (CoronarySinus_vy * 75.9) ;
      CoronarySinus_vy_u = (slope_CoronarySinus_vy * d) + CoronarySinus_vy ;
      if ((pstate != cstate) || force_init_update) CoronarySinus_vz_init = CoronarySinus_vz ;
      slope_CoronarySinus_vz = (CoronarySinus_vz * 6826.5) ;
      CoronarySinus_vz_u = (slope_CoronarySinus_vz * d) + CoronarySinus_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  CoronarySinus_t3 ;
      force_init_update = False;
      CoronarySinus_g_u = ((((((((CoronarySinus_v_i_0 + (- ((CoronarySinus_vx + (- CoronarySinus_vy)) + CoronarySinus_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      CoronarySinus_v_u = ((CoronarySinus_vx + (- CoronarySinus_vy)) + CoronarySinus_vz) ;
      CoronarySinus_voo = ((CoronarySinus_vx + (- CoronarySinus_vy)) + CoronarySinus_vz) ;
      CoronarySinus_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CoronarySinus!\n");
      exit(1);
    }
    break;
  case ( CoronarySinus_t4 ):
    if (True == False) {;}
    else if  (CoronarySinus_v <= (30.0)) {
      CoronarySinus_vx_u = CoronarySinus_vx ;
      CoronarySinus_vy_u = CoronarySinus_vy ;
      CoronarySinus_vz_u = CoronarySinus_vz ;
      CoronarySinus_g_u = ((((((((CoronarySinus_v_i_0 + (- ((CoronarySinus_vx + (- CoronarySinus_vy)) + CoronarySinus_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  CoronarySinus_t1 ;
      force_init_update = False;
    }

    else if ( CoronarySinus_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) CoronarySinus_vx_init = CoronarySinus_vx ;
      slope_CoronarySinus_vx = (CoronarySinus_vx * -33.2) ;
      CoronarySinus_vx_u = (slope_CoronarySinus_vx * d) + CoronarySinus_vx ;
      if ((pstate != cstate) || force_init_update) CoronarySinus_vy_init = CoronarySinus_vy ;
      slope_CoronarySinus_vy = ((CoronarySinus_vy * 20.0) * CoronarySinus_ft) ;
      CoronarySinus_vy_u = (slope_CoronarySinus_vy * d) + CoronarySinus_vy ;
      if ((pstate != cstate) || force_init_update) CoronarySinus_vz_init = CoronarySinus_vz ;
      slope_CoronarySinus_vz = ((CoronarySinus_vz * 2.0) * CoronarySinus_ft) ;
      CoronarySinus_vz_u = (slope_CoronarySinus_vz * d) + CoronarySinus_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  CoronarySinus_t4 ;
      force_init_update = False;
      CoronarySinus_g_u = ((((((((CoronarySinus_v_i_0 + (- ((CoronarySinus_vx + (- CoronarySinus_vy)) + CoronarySinus_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      CoronarySinus_v_u = ((CoronarySinus_vx + (- CoronarySinus_vy)) + CoronarySinus_vz) ;
      CoronarySinus_voo = ((CoronarySinus_vx + (- CoronarySinus_vy)) + CoronarySinus_vz) ;
      CoronarySinus_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CoronarySinus!\n");
      exit(1);
    }
    break;
  }
  CoronarySinus_vx = CoronarySinus_vx_u;
  CoronarySinus_vy = CoronarySinus_vy_u;
  CoronarySinus_vz = CoronarySinus_vz_u;
  CoronarySinus_g = CoronarySinus_g_u;
  CoronarySinus_v = CoronarySinus_v_u;
  CoronarySinus_ft = CoronarySinus_ft_u;
  CoronarySinus_theta = CoronarySinus_theta_u;
  CoronarySinus_v_O = CoronarySinus_v_O_u;
  return cstate;
}