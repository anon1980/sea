#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double CristaTerminalis_v_i_0;
double CristaTerminalis_v_i_1;
double CristaTerminalis_v_i_2;
double CristaTerminalis_voo = 0.0;
double CristaTerminalis_state = 0.0;


static double  CristaTerminalis_vx  =  0 ,  CristaTerminalis_vy  =  0 ,  CristaTerminalis_vz  =  0 ,  CristaTerminalis_g  =  0 ,  CristaTerminalis_v  =  0 ,  CristaTerminalis_ft  =  0 ,  CristaTerminalis_theta  =  0 ,  CristaTerminalis_v_O  =  0 ; //the continuous vars
static double  CristaTerminalis_vx_u , CristaTerminalis_vy_u , CristaTerminalis_vz_u , CristaTerminalis_g_u , CristaTerminalis_v_u , CristaTerminalis_ft_u , CristaTerminalis_theta_u , CristaTerminalis_v_O_u ; // and their updates
static double  CristaTerminalis_vx_init , CristaTerminalis_vy_init , CristaTerminalis_vz_init , CristaTerminalis_g_init , CristaTerminalis_v_init , CristaTerminalis_ft_init , CristaTerminalis_theta_init , CristaTerminalis_v_O_init ; // and their inits
static double  slope_CristaTerminalis_vx , slope_CristaTerminalis_vy , slope_CristaTerminalis_vz , slope_CristaTerminalis_g , slope_CristaTerminalis_v , slope_CristaTerminalis_ft , slope_CristaTerminalis_theta , slope_CristaTerminalis_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { CristaTerminalis_t1 , CristaTerminalis_t2 , CristaTerminalis_t3 , CristaTerminalis_t4 }; // state declarations

enum states CristaTerminalis (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( CristaTerminalis_t1 ):
    if (True == False) {;}
    else if  (CristaTerminalis_g > (44.5)) {
      CristaTerminalis_vx_u = (0.3 * CristaTerminalis_v) ;
      CristaTerminalis_vy_u = 0 ;
      CristaTerminalis_vz_u = (0.7 * CristaTerminalis_v) ;
      CristaTerminalis_g_u = ((((((((CristaTerminalis_v_i_0 + (- ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((CristaTerminalis_v_i_1 + (- ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      CristaTerminalis_theta_u = (CristaTerminalis_v / 30.0) ;
      CristaTerminalis_v_O_u = (131.1 + (- (80.1 * pow ( ((CristaTerminalis_v / 30.0)) , (0.5) )))) ;
      CristaTerminalis_ft_u = f (CristaTerminalis_theta,4.0e-2) ;
      cstate =  CristaTerminalis_t2 ;
      force_init_update = False;
    }

    else if ( CristaTerminalis_v <= (44.5)
               && 
              CristaTerminalis_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) CristaTerminalis_vx_init = CristaTerminalis_vx ;
      slope_CristaTerminalis_vx = (CristaTerminalis_vx * -8.7) ;
      CristaTerminalis_vx_u = (slope_CristaTerminalis_vx * d) + CristaTerminalis_vx ;
      if ((pstate != cstate) || force_init_update) CristaTerminalis_vy_init = CristaTerminalis_vy ;
      slope_CristaTerminalis_vy = (CristaTerminalis_vy * -190.9) ;
      CristaTerminalis_vy_u = (slope_CristaTerminalis_vy * d) + CristaTerminalis_vy ;
      if ((pstate != cstate) || force_init_update) CristaTerminalis_vz_init = CristaTerminalis_vz ;
      slope_CristaTerminalis_vz = (CristaTerminalis_vz * -190.4) ;
      CristaTerminalis_vz_u = (slope_CristaTerminalis_vz * d) + CristaTerminalis_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  CristaTerminalis_t1 ;
      force_init_update = False;
      CristaTerminalis_g_u = ((((((((CristaTerminalis_v_i_0 + (- ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((CristaTerminalis_v_i_1 + (- ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      CristaTerminalis_v_u = ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz) ;
      CristaTerminalis_voo = ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz) ;
      CristaTerminalis_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CristaTerminalis!\n");
      exit(1);
    }
    break;
  case ( CristaTerminalis_t2 ):
    if (True == False) {;}
    else if  (CristaTerminalis_v >= (44.5)) {
      CristaTerminalis_vx_u = CristaTerminalis_vx ;
      CristaTerminalis_vy_u = CristaTerminalis_vy ;
      CristaTerminalis_vz_u = CristaTerminalis_vz ;
      CristaTerminalis_g_u = ((((((((CristaTerminalis_v_i_0 + (- ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((CristaTerminalis_v_i_1 + (- ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  CristaTerminalis_t3 ;
      force_init_update = False;
    }
    else if  (CristaTerminalis_g <= (44.5)
               && 
              CristaTerminalis_v < (44.5)) {
      CristaTerminalis_vx_u = CristaTerminalis_vx ;
      CristaTerminalis_vy_u = CristaTerminalis_vy ;
      CristaTerminalis_vz_u = CristaTerminalis_vz ;
      CristaTerminalis_g_u = ((((((((CristaTerminalis_v_i_0 + (- ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((CristaTerminalis_v_i_1 + (- ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  CristaTerminalis_t1 ;
      force_init_update = False;
    }

    else if ( CristaTerminalis_v < (44.5)
               && 
              CristaTerminalis_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) CristaTerminalis_vx_init = CristaTerminalis_vx ;
      slope_CristaTerminalis_vx = ((CristaTerminalis_vx * -23.6) + (777200.0 * CristaTerminalis_g)) ;
      CristaTerminalis_vx_u = (slope_CristaTerminalis_vx * d) + CristaTerminalis_vx ;
      if ((pstate != cstate) || force_init_update) CristaTerminalis_vy_init = CristaTerminalis_vy ;
      slope_CristaTerminalis_vy = ((CristaTerminalis_vy * -45.5) + (58900.0 * CristaTerminalis_g)) ;
      CristaTerminalis_vy_u = (slope_CristaTerminalis_vy * d) + CristaTerminalis_vy ;
      if ((pstate != cstate) || force_init_update) CristaTerminalis_vz_init = CristaTerminalis_vz ;
      slope_CristaTerminalis_vz = ((CristaTerminalis_vz * -12.9) + (276600.0 * CristaTerminalis_g)) ;
      CristaTerminalis_vz_u = (slope_CristaTerminalis_vz * d) + CristaTerminalis_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  CristaTerminalis_t2 ;
      force_init_update = False;
      CristaTerminalis_g_u = ((((((((CristaTerminalis_v_i_0 + (- ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((CristaTerminalis_v_i_1 + (- ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      CristaTerminalis_v_u = ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz) ;
      CristaTerminalis_voo = ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz) ;
      CristaTerminalis_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CristaTerminalis!\n");
      exit(1);
    }
    break;
  case ( CristaTerminalis_t3 ):
    if (True == False) {;}
    else if  (CristaTerminalis_v >= (131.1)) {
      CristaTerminalis_vx_u = CristaTerminalis_vx ;
      CristaTerminalis_vy_u = CristaTerminalis_vy ;
      CristaTerminalis_vz_u = CristaTerminalis_vz ;
      CristaTerminalis_g_u = ((((((((CristaTerminalis_v_i_0 + (- ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((CristaTerminalis_v_i_1 + (- ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  CristaTerminalis_t4 ;
      force_init_update = False;
    }

    else if ( CristaTerminalis_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) CristaTerminalis_vx_init = CristaTerminalis_vx ;
      slope_CristaTerminalis_vx = (CristaTerminalis_vx * -6.9) ;
      CristaTerminalis_vx_u = (slope_CristaTerminalis_vx * d) + CristaTerminalis_vx ;
      if ((pstate != cstate) || force_init_update) CristaTerminalis_vy_init = CristaTerminalis_vy ;
      slope_CristaTerminalis_vy = (CristaTerminalis_vy * 75.9) ;
      CristaTerminalis_vy_u = (slope_CristaTerminalis_vy * d) + CristaTerminalis_vy ;
      if ((pstate != cstate) || force_init_update) CristaTerminalis_vz_init = CristaTerminalis_vz ;
      slope_CristaTerminalis_vz = (CristaTerminalis_vz * 6826.5) ;
      CristaTerminalis_vz_u = (slope_CristaTerminalis_vz * d) + CristaTerminalis_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  CristaTerminalis_t3 ;
      force_init_update = False;
      CristaTerminalis_g_u = ((((((((CristaTerminalis_v_i_0 + (- ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((CristaTerminalis_v_i_1 + (- ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      CristaTerminalis_v_u = ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz) ;
      CristaTerminalis_voo = ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz) ;
      CristaTerminalis_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CristaTerminalis!\n");
      exit(1);
    }
    break;
  case ( CristaTerminalis_t4 ):
    if (True == False) {;}
    else if  (CristaTerminalis_v <= (30.0)) {
      CristaTerminalis_vx_u = CristaTerminalis_vx ;
      CristaTerminalis_vy_u = CristaTerminalis_vy ;
      CristaTerminalis_vz_u = CristaTerminalis_vz ;
      CristaTerminalis_g_u = ((((((((CristaTerminalis_v_i_0 + (- ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((CristaTerminalis_v_i_1 + (- ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  CristaTerminalis_t1 ;
      force_init_update = False;
    }

    else if ( CristaTerminalis_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) CristaTerminalis_vx_init = CristaTerminalis_vx ;
      slope_CristaTerminalis_vx = (CristaTerminalis_vx * -33.2) ;
      CristaTerminalis_vx_u = (slope_CristaTerminalis_vx * d) + CristaTerminalis_vx ;
      if ((pstate != cstate) || force_init_update) CristaTerminalis_vy_init = CristaTerminalis_vy ;
      slope_CristaTerminalis_vy = ((CristaTerminalis_vy * 20.0) * CristaTerminalis_ft) ;
      CristaTerminalis_vy_u = (slope_CristaTerminalis_vy * d) + CristaTerminalis_vy ;
      if ((pstate != cstate) || force_init_update) CristaTerminalis_vz_init = CristaTerminalis_vz ;
      slope_CristaTerminalis_vz = ((CristaTerminalis_vz * 2.0) * CristaTerminalis_ft) ;
      CristaTerminalis_vz_u = (slope_CristaTerminalis_vz * d) + CristaTerminalis_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  CristaTerminalis_t4 ;
      force_init_update = False;
      CristaTerminalis_g_u = ((((((((CristaTerminalis_v_i_0 + (- ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((CristaTerminalis_v_i_1 + (- ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      CristaTerminalis_v_u = ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz) ;
      CristaTerminalis_voo = ((CristaTerminalis_vx + (- CristaTerminalis_vy)) + CristaTerminalis_vz) ;
      CristaTerminalis_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CristaTerminalis!\n");
      exit(1);
    }
    break;
  }
  CristaTerminalis_vx = CristaTerminalis_vx_u;
  CristaTerminalis_vy = CristaTerminalis_vy_u;
  CristaTerminalis_vz = CristaTerminalis_vz_u;
  CristaTerminalis_g = CristaTerminalis_g_u;
  CristaTerminalis_v = CristaTerminalis_v_u;
  CristaTerminalis_ft = CristaTerminalis_ft_u;
  CristaTerminalis_theta = CristaTerminalis_theta_u;
  CristaTerminalis_v_O = CristaTerminalis_v_O_u;
  return cstate;
}