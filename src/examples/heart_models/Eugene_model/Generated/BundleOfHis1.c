#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double BundleOfHis1_v_i_0;
double BundleOfHis1_v_i_1;
double BundleOfHis1_v_i_2;
double BundleOfHis1_voo = 0.0;
double BundleOfHis1_state = 0.0;


static double  BundleOfHis1_vx  =  0 ,  BundleOfHis1_vy  =  0 ,  BundleOfHis1_vz  =  0 ,  BundleOfHis1_g  =  0 ,  BundleOfHis1_v  =  0 ,  BundleOfHis1_ft  =  0 ,  BundleOfHis1_theta  =  0 ,  BundleOfHis1_v_O  =  0 ; //the continuous vars
static double  BundleOfHis1_vx_u , BundleOfHis1_vy_u , BundleOfHis1_vz_u , BundleOfHis1_g_u , BundleOfHis1_v_u , BundleOfHis1_ft_u , BundleOfHis1_theta_u , BundleOfHis1_v_O_u ; // and their updates
static double  BundleOfHis1_vx_init , BundleOfHis1_vy_init , BundleOfHis1_vz_init , BundleOfHis1_g_init , BundleOfHis1_v_init , BundleOfHis1_ft_init , BundleOfHis1_theta_init , BundleOfHis1_v_O_init ; // and their inits
static double  slope_BundleOfHis1_vx , slope_BundleOfHis1_vy , slope_BundleOfHis1_vz , slope_BundleOfHis1_g , slope_BundleOfHis1_v , slope_BundleOfHis1_ft , slope_BundleOfHis1_theta , slope_BundleOfHis1_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { BundleOfHis1_t1 , BundleOfHis1_t2 , BundleOfHis1_t3 , BundleOfHis1_t4 }; // state declarations

enum states BundleOfHis1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( BundleOfHis1_t1 ):
    if (True == False) {;}
    else if  (BundleOfHis1_g > (44.5)) {
      BundleOfHis1_vx_u = (0.3 * BundleOfHis1_v) ;
      BundleOfHis1_vy_u = 0 ;
      BundleOfHis1_vz_u = (0.7 * BundleOfHis1_v) ;
      BundleOfHis1_g_u = ((((((((BundleOfHis1_v_i_0 + (- ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis1_v_i_1 + (- ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      BundleOfHis1_theta_u = (BundleOfHis1_v / 30.0) ;
      BundleOfHis1_v_O_u = (131.1 + (- (80.1 * pow ( ((BundleOfHis1_v / 30.0)) , (0.5) )))) ;
      BundleOfHis1_ft_u = f (BundleOfHis1_theta,4.0e-2) ;
      cstate =  BundleOfHis1_t2 ;
      force_init_update = False;
    }

    else if ( BundleOfHis1_v <= (44.5)
               && 
              BundleOfHis1_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis1_vx_init = BundleOfHis1_vx ;
      slope_BundleOfHis1_vx = (BundleOfHis1_vx * -8.7) ;
      BundleOfHis1_vx_u = (slope_BundleOfHis1_vx * d) + BundleOfHis1_vx ;
      if ((pstate != cstate) || force_init_update) BundleOfHis1_vy_init = BundleOfHis1_vy ;
      slope_BundleOfHis1_vy = (BundleOfHis1_vy * -190.9) ;
      BundleOfHis1_vy_u = (slope_BundleOfHis1_vy * d) + BundleOfHis1_vy ;
      if ((pstate != cstate) || force_init_update) BundleOfHis1_vz_init = BundleOfHis1_vz ;
      slope_BundleOfHis1_vz = (BundleOfHis1_vz * -190.4) ;
      BundleOfHis1_vz_u = (slope_BundleOfHis1_vz * d) + BundleOfHis1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  BundleOfHis1_t1 ;
      force_init_update = False;
      BundleOfHis1_g_u = ((((((((BundleOfHis1_v_i_0 + (- ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis1_v_i_1 + (- ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      BundleOfHis1_v_u = ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz) ;
      BundleOfHis1_voo = ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz) ;
      BundleOfHis1_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis1!\n");
      exit(1);
    }
    break;
  case ( BundleOfHis1_t2 ):
    if (True == False) {;}
    else if  (BundleOfHis1_v >= (44.5)) {
      BundleOfHis1_vx_u = BundleOfHis1_vx ;
      BundleOfHis1_vy_u = BundleOfHis1_vy ;
      BundleOfHis1_vz_u = BundleOfHis1_vz ;
      BundleOfHis1_g_u = ((((((((BundleOfHis1_v_i_0 + (- ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis1_v_i_1 + (- ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  BundleOfHis1_t3 ;
      force_init_update = False;
    }
    else if  (BundleOfHis1_g <= (44.5)
               && BundleOfHis1_v < (44.5)) {
      BundleOfHis1_vx_u = BundleOfHis1_vx ;
      BundleOfHis1_vy_u = BundleOfHis1_vy ;
      BundleOfHis1_vz_u = BundleOfHis1_vz ;
      BundleOfHis1_g_u = ((((((((BundleOfHis1_v_i_0 + (- ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis1_v_i_1 + (- ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  BundleOfHis1_t1 ;
      force_init_update = False;
    }

    else if ( BundleOfHis1_v < (44.5)
               && 
              BundleOfHis1_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis1_vx_init = BundleOfHis1_vx ;
      slope_BundleOfHis1_vx = ((BundleOfHis1_vx * -23.6) + (777200.0 * BundleOfHis1_g)) ;
      BundleOfHis1_vx_u = (slope_BundleOfHis1_vx * d) + BundleOfHis1_vx ;
      if ((pstate != cstate) || force_init_update) BundleOfHis1_vy_init = BundleOfHis1_vy ;
      slope_BundleOfHis1_vy = ((BundleOfHis1_vy * -45.5) + (58900.0 * BundleOfHis1_g)) ;
      BundleOfHis1_vy_u = (slope_BundleOfHis1_vy * d) + BundleOfHis1_vy ;
      if ((pstate != cstate) || force_init_update) BundleOfHis1_vz_init = BundleOfHis1_vz ;
      slope_BundleOfHis1_vz = ((BundleOfHis1_vz * -12.9) + (276600.0 * BundleOfHis1_g)) ;
      BundleOfHis1_vz_u = (slope_BundleOfHis1_vz * d) + BundleOfHis1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  BundleOfHis1_t2 ;
      force_init_update = False;
      BundleOfHis1_g_u = ((((((((BundleOfHis1_v_i_0 + (- ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis1_v_i_1 + (- ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      BundleOfHis1_v_u = ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz) ;
      BundleOfHis1_voo = ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz) ;
      BundleOfHis1_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis1!\n");
      exit(1);
    }
    break;
  case ( BundleOfHis1_t3 ):
    if (True == False) {;}
    else if  (BundleOfHis1_v >= (131.1)) {
      BundleOfHis1_vx_u = BundleOfHis1_vx ;
      BundleOfHis1_vy_u = BundleOfHis1_vy ;
      BundleOfHis1_vz_u = BundleOfHis1_vz ;
      BundleOfHis1_g_u = ((((((((BundleOfHis1_v_i_0 + (- ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis1_v_i_1 + (- ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  BundleOfHis1_t4 ;
      force_init_update = False;
    }

    else if ( BundleOfHis1_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis1_vx_init = BundleOfHis1_vx ;
      slope_BundleOfHis1_vx = (BundleOfHis1_vx * -6.9) ;
      BundleOfHis1_vx_u = (slope_BundleOfHis1_vx * d) + BundleOfHis1_vx ;
      if ((pstate != cstate) || force_init_update) BundleOfHis1_vy_init = BundleOfHis1_vy ;
      slope_BundleOfHis1_vy = (BundleOfHis1_vy * 75.9) ;
      BundleOfHis1_vy_u = (slope_BundleOfHis1_vy * d) + BundleOfHis1_vy ;
      if ((pstate != cstate) || force_init_update) BundleOfHis1_vz_init = BundleOfHis1_vz ;
      slope_BundleOfHis1_vz = (BundleOfHis1_vz * 6826.5) ;
      BundleOfHis1_vz_u = (slope_BundleOfHis1_vz * d) + BundleOfHis1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  BundleOfHis1_t3 ;
      force_init_update = False;
      BundleOfHis1_g_u = ((((((((BundleOfHis1_v_i_0 + (- ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis1_v_i_1 + (- ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      BundleOfHis1_v_u = ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz) ;
      BundleOfHis1_voo = ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz) ;
      BundleOfHis1_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis1!\n");
      exit(1);
    }
    break;
  case ( BundleOfHis1_t4 ):
    if (True == False) {;}
    else if  (BundleOfHis1_v <= (30.0)) {
      BundleOfHis1_vx_u = BundleOfHis1_vx ;
      BundleOfHis1_vy_u = BundleOfHis1_vy ;
      BundleOfHis1_vz_u = BundleOfHis1_vz ;
      BundleOfHis1_g_u = ((((((((BundleOfHis1_v_i_0 + (- ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis1_v_i_1 + (- ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  BundleOfHis1_t1 ;
      force_init_update = False;
    }

    else if ( BundleOfHis1_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis1_vx_init = BundleOfHis1_vx ;
      slope_BundleOfHis1_vx = (BundleOfHis1_vx * -33.2) ;
      BundleOfHis1_vx_u = (slope_BundleOfHis1_vx * d) + BundleOfHis1_vx ;
      if ((pstate != cstate) || force_init_update) BundleOfHis1_vy_init = BundleOfHis1_vy ;
      slope_BundleOfHis1_vy = ((BundleOfHis1_vy * 11.0) * BundleOfHis1_ft) ;
      BundleOfHis1_vy_u = (slope_BundleOfHis1_vy * d) + BundleOfHis1_vy ;
      if ((pstate != cstate) || force_init_update) BundleOfHis1_vz_init = BundleOfHis1_vz ;
      slope_BundleOfHis1_vz = ((BundleOfHis1_vz * 2.0) * BundleOfHis1_ft) ;
      BundleOfHis1_vz_u = (slope_BundleOfHis1_vz * d) + BundleOfHis1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  BundleOfHis1_t4 ;
      force_init_update = False;
      BundleOfHis1_g_u = ((((((((BundleOfHis1_v_i_0 + (- ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BundleOfHis1_v_i_1 + (- ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      BundleOfHis1_v_u = ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz) ;
      BundleOfHis1_voo = ((BundleOfHis1_vx + (- BundleOfHis1_vy)) + BundleOfHis1_vz) ;
      BundleOfHis1_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis1!\n");
      exit(1);
    }
    break;
  }
  BundleOfHis1_vx = BundleOfHis1_vx_u;
  BundleOfHis1_vy = BundleOfHis1_vy_u;
  BundleOfHis1_vz = BundleOfHis1_vz_u;
  BundleOfHis1_g = BundleOfHis1_g_u;
  BundleOfHis1_v = BundleOfHis1_v_u;
  BundleOfHis1_ft = BundleOfHis1_ft_u;
  BundleOfHis1_theta = BundleOfHis1_theta_u;
  BundleOfHis1_v_O = BundleOfHis1_v_O_u;
  return cstate;
}