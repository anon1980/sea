#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double Slow1_AV_update_c1vd();
extern double Slow1_AV_update_c2vd();
extern double Slow1_AV_update_c1md();
extern double Slow1_AV_update_c2md();
extern double Slow1_AV_update_buffer_index(double,double,double,double);
extern double Slow1_AV_update_latch1(double,double);
extern double Slow1_AV_update_latch2(double,double);
extern double Slow1_AV_update_ocell1(double,double);
extern double Slow1_AV_update_ocell2(double,double);
double Slow1_AV_cell1_v;
double Slow1_AV_cell1_mode;
double Slow1_AV_cell2_v;
double Slow1_AV_cell2_mode;
double Slow1_AV_cell1_v_replay = 0.0;
double Slow1_AV_cell2_v_replay = 0.0;


static double  Slow1_AV_k  =  0.0 ,  Slow1_AV_cell1_mode_delayed  =  0.0 ,  Slow1_AV_cell2_mode_delayed  =  0.0 ,  Slow1_AV_from_cell  =  0.0 ,  Slow1_AV_cell1_replay_latch  =  0.0 ,  Slow1_AV_cell2_replay_latch  =  0.0 ,  Slow1_AV_cell1_v_delayed  =  0.0 ,  Slow1_AV_cell2_v_delayed  =  0.0 ,  Slow1_AV_wasted  =  0.0 ; //the continuous vars
static double  Slow1_AV_k_u , Slow1_AV_cell1_mode_delayed_u , Slow1_AV_cell2_mode_delayed_u , Slow1_AV_from_cell_u , Slow1_AV_cell1_replay_latch_u , Slow1_AV_cell2_replay_latch_u , Slow1_AV_cell1_v_delayed_u , Slow1_AV_cell2_v_delayed_u , Slow1_AV_wasted_u ; // and their updates
static double  Slow1_AV_k_init , Slow1_AV_cell1_mode_delayed_init , Slow1_AV_cell2_mode_delayed_init , Slow1_AV_from_cell_init , Slow1_AV_cell1_replay_latch_init , Slow1_AV_cell2_replay_latch_init , Slow1_AV_cell1_v_delayed_init , Slow1_AV_cell2_v_delayed_init , Slow1_AV_wasted_init ; // and their inits
static double  slope_Slow1_AV_k , slope_Slow1_AV_cell1_mode_delayed , slope_Slow1_AV_cell2_mode_delayed , slope_Slow1_AV_from_cell , slope_Slow1_AV_cell1_replay_latch , slope_Slow1_AV_cell2_replay_latch , slope_Slow1_AV_cell1_v_delayed , slope_Slow1_AV_cell2_v_delayed , slope_Slow1_AV_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { Slow1_AV_idle , Slow1_AV_annhilate , Slow1_AV_previous_drection1 , Slow1_AV_previous_direction2 , Slow1_AV_wait_cell1 , Slow1_AV_replay_cell1 , Slow1_AV_replay_cell2 , Slow1_AV_wait_cell2 }; // state declarations

enum states Slow1_AV (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( Slow1_AV_idle ):
    if (True == False) {;}
    else if  (Slow1_AV_cell2_mode == (2.0) && (Slow1_AV_cell1_mode != (2.0))) {
      Slow1_AV_k_u = 1 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
      cstate =  Slow1_AV_previous_direction2 ;
      force_init_update = False;
    }
    else if  (Slow1_AV_cell1_mode == (2.0) && (Slow1_AV_cell2_mode != (2.0))) {
      Slow1_AV_k_u = 1 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
      cstate =  Slow1_AV_previous_drection1 ;
      force_init_update = False;
    }
    else if  (Slow1_AV_cell1_mode == (2.0) && (Slow1_AV_cell2_mode == (2.0))) {
      Slow1_AV_k_u = 1 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
      cstate =  Slow1_AV_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Slow1_AV_k_init = Slow1_AV_k ;
      slope_Slow1_AV_k = 1 ;
      Slow1_AV_k_u = (slope_Slow1_AV_k * d) + Slow1_AV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Slow1_AV_idle ;
      force_init_update = False;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell1_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow1_AV!\n");
      exit(1);
    }
    break;
  case ( Slow1_AV_annhilate ):
    if (True == False) {;}
    else if  (Slow1_AV_cell1_mode != (2.0) && (Slow1_AV_cell2_mode != (2.0))) {
      Slow1_AV_k_u = 1 ;
      Slow1_AV_from_cell_u = 0 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
      cstate =  Slow1_AV_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Slow1_AV_k_init = Slow1_AV_k ;
      slope_Slow1_AV_k = 1 ;
      Slow1_AV_k_u = (slope_Slow1_AV_k * d) + Slow1_AV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Slow1_AV_annhilate ;
      force_init_update = False;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell1_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow1_AV!\n");
      exit(1);
    }
    break;
  case ( Slow1_AV_previous_drection1 ):
    if (True == False) {;}
    else if  (Slow1_AV_from_cell == (1.0)) {
      Slow1_AV_k_u = 1 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
      cstate =  Slow1_AV_wait_cell1 ;
      force_init_update = False;
    }
    else if  (Slow1_AV_from_cell == (0.0)) {
      Slow1_AV_k_u = 1 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
      cstate =  Slow1_AV_wait_cell1 ;
      force_init_update = False;
    }
    else if  (Slow1_AV_from_cell == (2.0) && (Slow1_AV_cell2_mode_delayed == (0.0))) {
      Slow1_AV_k_u = 1 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
      cstate =  Slow1_AV_wait_cell1 ;
      force_init_update = False;
    }
    else if  (Slow1_AV_from_cell == (2.0) && (Slow1_AV_cell2_mode_delayed != (0.0))) {
      Slow1_AV_k_u = 1 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
      cstate =  Slow1_AV_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Slow1_AV_k_init = Slow1_AV_k ;
      slope_Slow1_AV_k = 1 ;
      Slow1_AV_k_u = (slope_Slow1_AV_k * d) + Slow1_AV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Slow1_AV_previous_drection1 ;
      force_init_update = False;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell1_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow1_AV!\n");
      exit(1);
    }
    break;
  case ( Slow1_AV_previous_direction2 ):
    if (True == False) {;}
    else if  (Slow1_AV_from_cell == (1.0) && (Slow1_AV_cell1_mode_delayed != (0.0))) {
      Slow1_AV_k_u = 1 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
      cstate =  Slow1_AV_annhilate ;
      force_init_update = False;
    }
    else if  (Slow1_AV_from_cell == (2.0)) {
      Slow1_AV_k_u = 1 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
      cstate =  Slow1_AV_replay_cell1 ;
      force_init_update = False;
    }
    else if  (Slow1_AV_from_cell == (0.0)) {
      Slow1_AV_k_u = 1 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
      cstate =  Slow1_AV_replay_cell1 ;
      force_init_update = False;
    }
    else if  (Slow1_AV_from_cell == (1.0) && (Slow1_AV_cell1_mode_delayed == (0.0))) {
      Slow1_AV_k_u = 1 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
      cstate =  Slow1_AV_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Slow1_AV_k_init = Slow1_AV_k ;
      slope_Slow1_AV_k = 1 ;
      Slow1_AV_k_u = (slope_Slow1_AV_k * d) + Slow1_AV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Slow1_AV_previous_direction2 ;
      force_init_update = False;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell1_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow1_AV!\n");
      exit(1);
    }
    break;
  case ( Slow1_AV_wait_cell1 ):
    if (True == False) {;}
    else if  (Slow1_AV_cell2_mode == (2.0)) {
      Slow1_AV_k_u = 1 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
      cstate =  Slow1_AV_annhilate ;
      force_init_update = False;
    }
    else if  (Slow1_AV_k >= (15.0)) {
      Slow1_AV_from_cell_u = 1 ;
      Slow1_AV_cell1_replay_latch_u = 1 ;
      Slow1_AV_k_u = 1 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
      cstate =  Slow1_AV_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Slow1_AV_k_init = Slow1_AV_k ;
      slope_Slow1_AV_k = 1 ;
      Slow1_AV_k_u = (slope_Slow1_AV_k * d) + Slow1_AV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Slow1_AV_wait_cell1 ;
      force_init_update = False;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell1_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow1_AV!\n");
      exit(1);
    }
    break;
  case ( Slow1_AV_replay_cell1 ):
    if (True == False) {;}
    else if  (Slow1_AV_cell1_mode == (2.0)) {
      Slow1_AV_k_u = 1 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
      cstate =  Slow1_AV_annhilate ;
      force_init_update = False;
    }
    else if  (Slow1_AV_k >= (15.0)) {
      Slow1_AV_from_cell_u = 2 ;
      Slow1_AV_cell2_replay_latch_u = 1 ;
      Slow1_AV_k_u = 1 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
      cstate =  Slow1_AV_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Slow1_AV_k_init = Slow1_AV_k ;
      slope_Slow1_AV_k = 1 ;
      Slow1_AV_k_u = (slope_Slow1_AV_k * d) + Slow1_AV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Slow1_AV_replay_cell1 ;
      force_init_update = False;
      Slow1_AV_cell1_replay_latch_u = 1 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell1_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow1_AV!\n");
      exit(1);
    }
    break;
  case ( Slow1_AV_replay_cell2 ):
    if (True == False) {;}
    else if  (Slow1_AV_k >= (10.0)) {
      Slow1_AV_k_u = 1 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
      cstate =  Slow1_AV_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Slow1_AV_k_init = Slow1_AV_k ;
      slope_Slow1_AV_k = 1 ;
      Slow1_AV_k_u = (slope_Slow1_AV_k * d) + Slow1_AV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Slow1_AV_replay_cell2 ;
      force_init_update = False;
      Slow1_AV_cell2_replay_latch_u = 1 ;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell1_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow1_AV!\n");
      exit(1);
    }
    break;
  case ( Slow1_AV_wait_cell2 ):
    if (True == False) {;}
    else if  (Slow1_AV_k >= (10.0)) {
      Slow1_AV_k_u = 1 ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
      cstate =  Slow1_AV_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Slow1_AV_k_init = Slow1_AV_k ;
      slope_Slow1_AV_k = 1 ;
      Slow1_AV_k_u = (slope_Slow1_AV_k * d) + Slow1_AV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Slow1_AV_wait_cell2 ;
      force_init_update = False;
      Slow1_AV_cell1_v_delayed_u = Slow1_AV_update_c1vd () ;
      Slow1_AV_cell2_v_delayed_u = Slow1_AV_update_c2vd () ;
      Slow1_AV_cell1_mode_delayed_u = Slow1_AV_update_c1md () ;
      Slow1_AV_cell2_mode_delayed_u = Slow1_AV_update_c2md () ;
      Slow1_AV_wasted_u = Slow1_AV_update_buffer_index (Slow1_AV_cell1_v,Slow1_AV_cell2_v,Slow1_AV_cell1_mode,Slow1_AV_cell2_mode) ;
      Slow1_AV_cell1_replay_latch_u = Slow1_AV_update_latch1 (Slow1_AV_cell1_mode_delayed,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_replay_latch_u = Slow1_AV_update_latch2 (Slow1_AV_cell2_mode_delayed,Slow1_AV_cell2_replay_latch_u) ;
      Slow1_AV_cell1_v_replay = Slow1_AV_update_ocell1 (Slow1_AV_cell1_v_delayed_u,Slow1_AV_cell1_replay_latch_u) ;
      Slow1_AV_cell2_v_replay = Slow1_AV_update_ocell2 (Slow1_AV_cell2_v_delayed_u,Slow1_AV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow1_AV!\n");
      exit(1);
    }
    break;
  }
  Slow1_AV_k = Slow1_AV_k_u;
  Slow1_AV_cell1_mode_delayed = Slow1_AV_cell1_mode_delayed_u;
  Slow1_AV_cell2_mode_delayed = Slow1_AV_cell2_mode_delayed_u;
  Slow1_AV_from_cell = Slow1_AV_from_cell_u;
  Slow1_AV_cell1_replay_latch = Slow1_AV_cell1_replay_latch_u;
  Slow1_AV_cell2_replay_latch = Slow1_AV_cell2_replay_latch_u;
  Slow1_AV_cell1_v_delayed = Slow1_AV_cell1_v_delayed_u;
  Slow1_AV_cell2_v_delayed = Slow1_AV_cell2_v_delayed_u;
  Slow1_AV_wasted = Slow1_AV_wasted_u;
  return cstate;
}