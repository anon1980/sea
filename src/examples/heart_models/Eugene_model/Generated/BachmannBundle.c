#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double BachmannBundle_v_i_0;
double BachmannBundle_v_i_1;
double BachmannBundle_v_i_2;
double BachmannBundle_voo = 0.0;
double BachmannBundle_state = 0.0;


static double  BachmannBundle_vx  =  0 ,  BachmannBundle_vy  =  0 ,  BachmannBundle_vz  =  0 ,  BachmannBundle_g  =  0 ,  BachmannBundle_v  =  0 ,  BachmannBundle_ft  =  0 ,  BachmannBundle_theta  =  0 ,  BachmannBundle_v_O  =  0 ; //the continuous vars
static double  BachmannBundle_vx_u , BachmannBundle_vy_u , BachmannBundle_vz_u , BachmannBundle_g_u , BachmannBundle_v_u , BachmannBundle_ft_u , BachmannBundle_theta_u , BachmannBundle_v_O_u ; // and their updates
static double  BachmannBundle_vx_init , BachmannBundle_vy_init , BachmannBundle_vz_init , BachmannBundle_g_init , BachmannBundle_v_init , BachmannBundle_ft_init , BachmannBundle_theta_init , BachmannBundle_v_O_init ; // and their inits
static double  slope_BachmannBundle_vx , slope_BachmannBundle_vy , slope_BachmannBundle_vz , slope_BachmannBundle_g , slope_BachmannBundle_v , slope_BachmannBundle_ft , slope_BachmannBundle_theta , slope_BachmannBundle_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { BachmannBundle_t1 , BachmannBundle_t2 , BachmannBundle_t3 , BachmannBundle_t4 }; // state declarations

enum states BachmannBundle (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( BachmannBundle_t1 ):
    if (True == False) {;}
    else if  (BachmannBundle_g > (44.5)) {
      BachmannBundle_vx_u = (0.3 * BachmannBundle_v) ;
      BachmannBundle_vy_u = 0 ;
      BachmannBundle_vz_u = (0.7 * BachmannBundle_v) ;
      BachmannBundle_g_u = ((((((((BachmannBundle_v_i_0 + (- ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BachmannBundle_v_i_1 + (- ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      BachmannBundle_theta_u = (BachmannBundle_v / 30.0) ;
      BachmannBundle_v_O_u = (131.1 + (- (80.1 * pow ( ((BachmannBundle_v / 30.0)) , (0.5) )))) ;
      BachmannBundle_ft_u = f (BachmannBundle_theta,4.0e-2) ;
      cstate =  BachmannBundle_t2 ;
      force_init_update = False;
    }

    else if ( BachmannBundle_v <= (44.5)
               && 
              BachmannBundle_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) BachmannBundle_vx_init = BachmannBundle_vx ;
      slope_BachmannBundle_vx = (BachmannBundle_vx * -8.7) ;
      BachmannBundle_vx_u = (slope_BachmannBundle_vx * d) + BachmannBundle_vx ;
      if ((pstate != cstate) || force_init_update) BachmannBundle_vy_init = BachmannBundle_vy ;
      slope_BachmannBundle_vy = (BachmannBundle_vy * -190.9) ;
      BachmannBundle_vy_u = (slope_BachmannBundle_vy * d) + BachmannBundle_vy ;
      if ((pstate != cstate) || force_init_update) BachmannBundle_vz_init = BachmannBundle_vz ;
      slope_BachmannBundle_vz = (BachmannBundle_vz * -190.4) ;
      BachmannBundle_vz_u = (slope_BachmannBundle_vz * d) + BachmannBundle_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  BachmannBundle_t1 ;
      force_init_update = False;
      BachmannBundle_g_u = ((((((((BachmannBundle_v_i_0 + (- ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BachmannBundle_v_i_1 + (- ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      BachmannBundle_v_u = ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz) ;
      BachmannBundle_voo = ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz) ;
      BachmannBundle_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BachmannBundle!\n");
      exit(1);
    }
    break;
  case ( BachmannBundle_t2 ):
    if (True == False) {;}
    else if  (BachmannBundle_v >= (44.5)) {
      BachmannBundle_vx_u = BachmannBundle_vx ;
      BachmannBundle_vy_u = BachmannBundle_vy ;
      BachmannBundle_vz_u = BachmannBundle_vz ;
      BachmannBundle_g_u = ((((((((BachmannBundle_v_i_0 + (- ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BachmannBundle_v_i_1 + (- ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  BachmannBundle_t3 ;
      force_init_update = False;
    }
    else if  (BachmannBundle_g <= (44.5)
               && BachmannBundle_v < (44.5)) {
      BachmannBundle_vx_u = BachmannBundle_vx ;
      BachmannBundle_vy_u = BachmannBundle_vy ;
      BachmannBundle_vz_u = BachmannBundle_vz ;
      BachmannBundle_g_u = ((((((((BachmannBundle_v_i_0 + (- ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BachmannBundle_v_i_1 + (- ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  BachmannBundle_t1 ;
      force_init_update = False;
    }

    else if ( BachmannBundle_v < (44.5)
               && 
              BachmannBundle_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) BachmannBundle_vx_init = BachmannBundle_vx ;
      slope_BachmannBundle_vx = ((BachmannBundle_vx * -23.6) + (777200.0 * BachmannBundle_g)) ;
      BachmannBundle_vx_u = (slope_BachmannBundle_vx * d) + BachmannBundle_vx ;
      if ((pstate != cstate) || force_init_update) BachmannBundle_vy_init = BachmannBundle_vy ;
      slope_BachmannBundle_vy = ((BachmannBundle_vy * -45.5) + (58900.0 * BachmannBundle_g)) ;
      BachmannBundle_vy_u = (slope_BachmannBundle_vy * d) + BachmannBundle_vy ;
      if ((pstate != cstate) || force_init_update) BachmannBundle_vz_init = BachmannBundle_vz ;
      slope_BachmannBundle_vz = ((BachmannBundle_vz * -12.9) + (276600.0 * BachmannBundle_g)) ;
      BachmannBundle_vz_u = (slope_BachmannBundle_vz * d) + BachmannBundle_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  BachmannBundle_t2 ;
      force_init_update = False;
      BachmannBundle_g_u = ((((((((BachmannBundle_v_i_0 + (- ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BachmannBundle_v_i_1 + (- ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      BachmannBundle_v_u = ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz) ;
      BachmannBundle_voo = ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz) ;
      BachmannBundle_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BachmannBundle!\n");
      exit(1);
    }
    break;
  case ( BachmannBundle_t3 ):
    if (True == False) {;}
    else if  (BachmannBundle_v >= (131.1)) {
      BachmannBundle_vx_u = BachmannBundle_vx ;
      BachmannBundle_vy_u = BachmannBundle_vy ;
      BachmannBundle_vz_u = BachmannBundle_vz ;
      BachmannBundle_g_u = ((((((((BachmannBundle_v_i_0 + (- ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BachmannBundle_v_i_1 + (- ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  BachmannBundle_t4 ;
      force_init_update = False;
    }

    else if ( BachmannBundle_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) BachmannBundle_vx_init = BachmannBundle_vx ;
      slope_BachmannBundle_vx = (BachmannBundle_vx * -6.9) ;
      BachmannBundle_vx_u = (slope_BachmannBundle_vx * d) + BachmannBundle_vx ;
      if ((pstate != cstate) || force_init_update) BachmannBundle_vy_init = BachmannBundle_vy ;
      slope_BachmannBundle_vy = (BachmannBundle_vy * 75.9) ;
      BachmannBundle_vy_u = (slope_BachmannBundle_vy * d) + BachmannBundle_vy ;
      if ((pstate != cstate) || force_init_update) BachmannBundle_vz_init = BachmannBundle_vz ;
      slope_BachmannBundle_vz = (BachmannBundle_vz * 6826.5) ;
      BachmannBundle_vz_u = (slope_BachmannBundle_vz * d) + BachmannBundle_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  BachmannBundle_t3 ;
      force_init_update = False;
      BachmannBundle_g_u = ((((((((BachmannBundle_v_i_0 + (- ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BachmannBundle_v_i_1 + (- ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      BachmannBundle_v_u = ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz) ;
      BachmannBundle_voo = ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz) ;
      BachmannBundle_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BachmannBundle!\n");
      exit(1);
    }
    break;
  case ( BachmannBundle_t4 ):
    if (True == False) {;}
    else if  (BachmannBundle_v <= (30.0)) {
      BachmannBundle_vx_u = BachmannBundle_vx ;
      BachmannBundle_vy_u = BachmannBundle_vy ;
      BachmannBundle_vz_u = BachmannBundle_vz ;
      BachmannBundle_g_u = ((((((((BachmannBundle_v_i_0 + (- ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BachmannBundle_v_i_1 + (- ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  BachmannBundle_t1 ;
      force_init_update = False;
    }

    else if ( BachmannBundle_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) BachmannBundle_vx_init = BachmannBundle_vx ;
      slope_BachmannBundle_vx = (BachmannBundle_vx * -33.2) ;
      BachmannBundle_vx_u = (slope_BachmannBundle_vx * d) + BachmannBundle_vx ;
      if ((pstate != cstate) || force_init_update) BachmannBundle_vy_init = BachmannBundle_vy ;
      slope_BachmannBundle_vy = ((BachmannBundle_vy * 20.0) * BachmannBundle_ft) ;
      BachmannBundle_vy_u = (slope_BachmannBundle_vy * d) + BachmannBundle_vy ;
      if ((pstate != cstate) || force_init_update) BachmannBundle_vz_init = BachmannBundle_vz ;
      slope_BachmannBundle_vz = ((BachmannBundle_vz * 2.0) * BachmannBundle_ft) ;
      BachmannBundle_vz_u = (slope_BachmannBundle_vz * d) + BachmannBundle_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  BachmannBundle_t4 ;
      force_init_update = False;
      BachmannBundle_g_u = ((((((((BachmannBundle_v_i_0 + (- ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((BachmannBundle_v_i_1 + (- ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      BachmannBundle_v_u = ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz) ;
      BachmannBundle_voo = ((BachmannBundle_vx + (- BachmannBundle_vy)) + BachmannBundle_vz) ;
      BachmannBundle_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BachmannBundle!\n");
      exit(1);
    }
    break;
  }
  BachmannBundle_vx = BachmannBundle_vx_u;
  BachmannBundle_vy = BachmannBundle_vy_u;
  BachmannBundle_vz = BachmannBundle_vz_u;
  BachmannBundle_g = BachmannBundle_g_u;
  BachmannBundle_v = BachmannBundle_v_u;
  BachmannBundle_ft = BachmannBundle_ft_u;
  BachmannBundle_theta = BachmannBundle_theta_u;
  BachmannBundle_v_O = BachmannBundle_v_O_u;
  return cstate;
}