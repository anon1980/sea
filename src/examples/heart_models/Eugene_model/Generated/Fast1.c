#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double Fast1_v_i_0;
double Fast1_v_i_1;
double Fast1_v_i_2;
double Fast1_voo = 0.0;
double Fast1_state = 0.0;


static double  Fast1_vx  =  0 ,  Fast1_vy  =  0 ,  Fast1_vz  =  0 ,  Fast1_g  =  0 ,  Fast1_v  =  0 ,  Fast1_ft  =  0 ,  Fast1_theta  =  0 ,  Fast1_v_O  =  0 ; //the continuous vars
static double  Fast1_vx_u , Fast1_vy_u , Fast1_vz_u , Fast1_g_u , Fast1_v_u , Fast1_ft_u , Fast1_theta_u , Fast1_v_O_u ; // and their updates
static double  Fast1_vx_init , Fast1_vy_init , Fast1_vz_init , Fast1_g_init , Fast1_v_init , Fast1_ft_init , Fast1_theta_init , Fast1_v_O_init ; // and their inits
static double  slope_Fast1_vx , slope_Fast1_vy , slope_Fast1_vz , slope_Fast1_g , slope_Fast1_v , slope_Fast1_ft , slope_Fast1_theta , slope_Fast1_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { Fast1_t1 , Fast1_t2 , Fast1_t3 , Fast1_t4 }; // state declarations

enum states Fast1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( Fast1_t1 ):
    if (True == False) {;}
    else if  (Fast1_g > (44.5)) {
      Fast1_vx_u = (0.3 * Fast1_v) ;
      Fast1_vy_u = 0 ;
      Fast1_vz_u = (0.7 * Fast1_v) ;
      Fast1_g_u = ((((((((Fast1_v_i_0 + (- ((Fast1_vx + (- Fast1_vy)) + Fast1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Fast1_v_i_1 + (- ((Fast1_vx + (- Fast1_vy)) + Fast1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Fast1_theta_u = (Fast1_v / 30.0) ;
      Fast1_v_O_u = (131.1 + (- (80.1 * pow ( ((Fast1_v / 30.0)) , (0.5) )))) ;
      Fast1_ft_u = f (Fast1_theta,4.0e-2) ;
      cstate =  Fast1_t2 ;
      force_init_update = False;
    }

    else if ( Fast1_v <= (44.5)
               && Fast1_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) Fast1_vx_init = Fast1_vx ;
      slope_Fast1_vx = (Fast1_vx * -8.7) ;
      Fast1_vx_u = (slope_Fast1_vx * d) + Fast1_vx ;
      if ((pstate != cstate) || force_init_update) Fast1_vy_init = Fast1_vy ;
      slope_Fast1_vy = (Fast1_vy * -190.9) ;
      Fast1_vy_u = (slope_Fast1_vy * d) + Fast1_vy ;
      if ((pstate != cstate) || force_init_update) Fast1_vz_init = Fast1_vz ;
      slope_Fast1_vz = (Fast1_vz * -190.4) ;
      Fast1_vz_u = (slope_Fast1_vz * d) + Fast1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Fast1_t1 ;
      force_init_update = False;
      Fast1_g_u = ((((((((Fast1_v_i_0 + (- ((Fast1_vx + (- Fast1_vy)) + Fast1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Fast1_v_i_1 + (- ((Fast1_vx + (- Fast1_vy)) + Fast1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Fast1_v_u = ((Fast1_vx + (- Fast1_vy)) + Fast1_vz) ;
      Fast1_voo = ((Fast1_vx + (- Fast1_vy)) + Fast1_vz) ;
      Fast1_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast1!\n");
      exit(1);
    }
    break;
  case ( Fast1_t2 ):
    if (True == False) {;}
    else if  (Fast1_v >= (44.5)) {
      Fast1_vx_u = Fast1_vx ;
      Fast1_vy_u = Fast1_vy ;
      Fast1_vz_u = Fast1_vz ;
      Fast1_g_u = ((((((((Fast1_v_i_0 + (- ((Fast1_vx + (- Fast1_vy)) + Fast1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Fast1_v_i_1 + (- ((Fast1_vx + (- Fast1_vy)) + Fast1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  Fast1_t3 ;
      force_init_update = False;
    }
    else if  (Fast1_g <= (44.5)
               && Fast1_v < (44.5)) {
      Fast1_vx_u = Fast1_vx ;
      Fast1_vy_u = Fast1_vy ;
      Fast1_vz_u = Fast1_vz ;
      Fast1_g_u = ((((((((Fast1_v_i_0 + (- ((Fast1_vx + (- Fast1_vy)) + Fast1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Fast1_v_i_1 + (- ((Fast1_vx + (- Fast1_vy)) + Fast1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  Fast1_t1 ;
      force_init_update = False;
    }

    else if ( Fast1_v < (44.5)
               && Fast1_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) Fast1_vx_init = Fast1_vx ;
      slope_Fast1_vx = ((Fast1_vx * -23.6) + (777200.0 * Fast1_g)) ;
      Fast1_vx_u = (slope_Fast1_vx * d) + Fast1_vx ;
      if ((pstate != cstate) || force_init_update) Fast1_vy_init = Fast1_vy ;
      slope_Fast1_vy = ((Fast1_vy * -45.5) + (58900.0 * Fast1_g)) ;
      Fast1_vy_u = (slope_Fast1_vy * d) + Fast1_vy ;
      if ((pstate != cstate) || force_init_update) Fast1_vz_init = Fast1_vz ;
      slope_Fast1_vz = ((Fast1_vz * -12.9) + (276600.0 * Fast1_g)) ;
      Fast1_vz_u = (slope_Fast1_vz * d) + Fast1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Fast1_t2 ;
      force_init_update = False;
      Fast1_g_u = ((((((((Fast1_v_i_0 + (- ((Fast1_vx + (- Fast1_vy)) + Fast1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Fast1_v_i_1 + (- ((Fast1_vx + (- Fast1_vy)) + Fast1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Fast1_v_u = ((Fast1_vx + (- Fast1_vy)) + Fast1_vz) ;
      Fast1_voo = ((Fast1_vx + (- Fast1_vy)) + Fast1_vz) ;
      Fast1_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast1!\n");
      exit(1);
    }
    break;
  case ( Fast1_t3 ):
    if (True == False) {;}
    else if  (Fast1_v >= (131.1)) {
      Fast1_vx_u = Fast1_vx ;
      Fast1_vy_u = Fast1_vy ;
      Fast1_vz_u = Fast1_vz ;
      Fast1_g_u = ((((((((Fast1_v_i_0 + (- ((Fast1_vx + (- Fast1_vy)) + Fast1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Fast1_v_i_1 + (- ((Fast1_vx + (- Fast1_vy)) + Fast1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  Fast1_t4 ;
      force_init_update = False;
    }

    else if ( Fast1_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) Fast1_vx_init = Fast1_vx ;
      slope_Fast1_vx = (Fast1_vx * -6.9) ;
      Fast1_vx_u = (slope_Fast1_vx * d) + Fast1_vx ;
      if ((pstate != cstate) || force_init_update) Fast1_vy_init = Fast1_vy ;
      slope_Fast1_vy = (Fast1_vy * 75.9) ;
      Fast1_vy_u = (slope_Fast1_vy * d) + Fast1_vy ;
      if ((pstate != cstate) || force_init_update) Fast1_vz_init = Fast1_vz ;
      slope_Fast1_vz = (Fast1_vz * 6826.5) ;
      Fast1_vz_u = (slope_Fast1_vz * d) + Fast1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Fast1_t3 ;
      force_init_update = False;
      Fast1_g_u = ((((((((Fast1_v_i_0 + (- ((Fast1_vx + (- Fast1_vy)) + Fast1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Fast1_v_i_1 + (- ((Fast1_vx + (- Fast1_vy)) + Fast1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Fast1_v_u = ((Fast1_vx + (- Fast1_vy)) + Fast1_vz) ;
      Fast1_voo = ((Fast1_vx + (- Fast1_vy)) + Fast1_vz) ;
      Fast1_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast1!\n");
      exit(1);
    }
    break;
  case ( Fast1_t4 ):
    if (True == False) {;}
    else if  (Fast1_v <= (30.0)) {
      Fast1_vx_u = Fast1_vx ;
      Fast1_vy_u = Fast1_vy ;
      Fast1_vz_u = Fast1_vz ;
      Fast1_g_u = ((((((((Fast1_v_i_0 + (- ((Fast1_vx + (- Fast1_vy)) + Fast1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Fast1_v_i_1 + (- ((Fast1_vx + (- Fast1_vy)) + Fast1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  Fast1_t1 ;
      force_init_update = False;
    }

    else if ( Fast1_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) Fast1_vx_init = Fast1_vx ;
      slope_Fast1_vx = (Fast1_vx * -33.2) ;
      Fast1_vx_u = (slope_Fast1_vx * d) + Fast1_vx ;
      if ((pstate != cstate) || force_init_update) Fast1_vy_init = Fast1_vy ;
      slope_Fast1_vy = ((Fast1_vy * 18.0) * Fast1_ft) ;
      Fast1_vy_u = (slope_Fast1_vy * d) + Fast1_vy ;
      if ((pstate != cstate) || force_init_update) Fast1_vz_init = Fast1_vz ;
      slope_Fast1_vz = ((Fast1_vz * 2.0) * Fast1_ft) ;
      Fast1_vz_u = (slope_Fast1_vz * d) + Fast1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Fast1_t4 ;
      force_init_update = False;
      Fast1_g_u = ((((((((Fast1_v_i_0 + (- ((Fast1_vx + (- Fast1_vy)) + Fast1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Fast1_v_i_1 + (- ((Fast1_vx + (- Fast1_vy)) + Fast1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Fast1_v_u = ((Fast1_vx + (- Fast1_vy)) + Fast1_vz) ;
      Fast1_voo = ((Fast1_vx + (- Fast1_vy)) + Fast1_vz) ;
      Fast1_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast1!\n");
      exit(1);
    }
    break;
  }
  Fast1_vx = Fast1_vx_u;
  Fast1_vy = Fast1_vy_u;
  Fast1_vz = Fast1_vz_u;
  Fast1_g = Fast1_g_u;
  Fast1_v = Fast1_v_u;
  Fast1_ft = Fast1_ft_u;
  Fast1_theta = Fast1_theta_u;
  Fast1_v_O = Fast1_v_O_u;
  return cstate;
}