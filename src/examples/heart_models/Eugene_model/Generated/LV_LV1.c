#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double LV_LV1_update_c1vd();
extern double LV_LV1_update_c2vd();
extern double LV_LV1_update_c1md();
extern double LV_LV1_update_c2md();
extern double LV_LV1_update_buffer_index(double,double,double,double);
extern double LV_LV1_update_latch1(double,double);
extern double LV_LV1_update_latch2(double,double);
extern double LV_LV1_update_ocell1(double,double);
extern double LV_LV1_update_ocell2(double,double);
double LV_LV1_cell1_v;
double LV_LV1_cell1_mode;
double LV_LV1_cell2_v;
double LV_LV1_cell2_mode;
double LV_LV1_cell1_v_replay = 0.0;
double LV_LV1_cell2_v_replay = 0.0;


static double  LV_LV1_k  =  0.0 ,  LV_LV1_cell1_mode_delayed  =  0.0 ,  LV_LV1_cell2_mode_delayed  =  0.0 ,  LV_LV1_from_cell  =  0.0 ,  LV_LV1_cell1_replay_latch  =  0.0 ,  LV_LV1_cell2_replay_latch  =  0.0 ,  LV_LV1_cell1_v_delayed  =  0.0 ,  LV_LV1_cell2_v_delayed  =  0.0 ,  LV_LV1_wasted  =  0.0 ; //the continuous vars
static double  LV_LV1_k_u , LV_LV1_cell1_mode_delayed_u , LV_LV1_cell2_mode_delayed_u , LV_LV1_from_cell_u , LV_LV1_cell1_replay_latch_u , LV_LV1_cell2_replay_latch_u , LV_LV1_cell1_v_delayed_u , LV_LV1_cell2_v_delayed_u , LV_LV1_wasted_u ; // and their updates
static double  LV_LV1_k_init , LV_LV1_cell1_mode_delayed_init , LV_LV1_cell2_mode_delayed_init , LV_LV1_from_cell_init , LV_LV1_cell1_replay_latch_init , LV_LV1_cell2_replay_latch_init , LV_LV1_cell1_v_delayed_init , LV_LV1_cell2_v_delayed_init , LV_LV1_wasted_init ; // and their inits
static double  slope_LV_LV1_k , slope_LV_LV1_cell1_mode_delayed , slope_LV_LV1_cell2_mode_delayed , slope_LV_LV1_from_cell , slope_LV_LV1_cell1_replay_latch , slope_LV_LV1_cell2_replay_latch , slope_LV_LV1_cell1_v_delayed , slope_LV_LV1_cell2_v_delayed , slope_LV_LV1_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { LV_LV1_idle , LV_LV1_annhilate , LV_LV1_previous_drection1 , LV_LV1_previous_direction2 , LV_LV1_wait_cell1 , LV_LV1_replay_cell1 , LV_LV1_replay_cell2 , LV_LV1_wait_cell2 }; // state declarations

enum states LV_LV1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( LV_LV1_idle ):
    if (True == False) {;}
    else if  (LV_LV1_cell2_mode == (2.0) && (LV_LV1_cell1_mode != (2.0))) {
      LV_LV1_k_u = 1 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
      cstate =  LV_LV1_previous_direction2 ;
      force_init_update = False;
    }
    else if  (LV_LV1_cell1_mode == (2.0) && (LV_LV1_cell2_mode != (2.0))) {
      LV_LV1_k_u = 1 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
      cstate =  LV_LV1_previous_drection1 ;
      force_init_update = False;
    }
    else if  (LV_LV1_cell1_mode == (2.0) && (LV_LV1_cell2_mode == (2.0))) {
      LV_LV1_k_u = 1 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
      cstate =  LV_LV1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LV_LV1_k_init = LV_LV1_k ;
      slope_LV_LV1_k = 1 ;
      LV_LV1_k_u = (slope_LV_LV1_k * d) + LV_LV1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LV_LV1_idle ;
      force_init_update = False;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell1_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LV_LV1!\n");
      exit(1);
    }
    break;
  case ( LV_LV1_annhilate ):
    if (True == False) {;}
    else if  (LV_LV1_cell1_mode != (2.0) && (LV_LV1_cell2_mode != (2.0))) {
      LV_LV1_k_u = 1 ;
      LV_LV1_from_cell_u = 0 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
      cstate =  LV_LV1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LV_LV1_k_init = LV_LV1_k ;
      slope_LV_LV1_k = 1 ;
      LV_LV1_k_u = (slope_LV_LV1_k * d) + LV_LV1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LV_LV1_annhilate ;
      force_init_update = False;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell1_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LV_LV1!\n");
      exit(1);
    }
    break;
  case ( LV_LV1_previous_drection1 ):
    if (True == False) {;}
    else if  (LV_LV1_from_cell == (1.0)) {
      LV_LV1_k_u = 1 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
      cstate =  LV_LV1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LV_LV1_from_cell == (0.0)) {
      LV_LV1_k_u = 1 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
      cstate =  LV_LV1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LV_LV1_from_cell == (2.0) && (LV_LV1_cell2_mode_delayed == (0.0))) {
      LV_LV1_k_u = 1 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
      cstate =  LV_LV1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LV_LV1_from_cell == (2.0) && (LV_LV1_cell2_mode_delayed != (0.0))) {
      LV_LV1_k_u = 1 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
      cstate =  LV_LV1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LV_LV1_k_init = LV_LV1_k ;
      slope_LV_LV1_k = 1 ;
      LV_LV1_k_u = (slope_LV_LV1_k * d) + LV_LV1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LV_LV1_previous_drection1 ;
      force_init_update = False;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell1_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LV_LV1!\n");
      exit(1);
    }
    break;
  case ( LV_LV1_previous_direction2 ):
    if (True == False) {;}
    else if  (LV_LV1_from_cell == (1.0) && (LV_LV1_cell1_mode_delayed != (0.0))) {
      LV_LV1_k_u = 1 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
      cstate =  LV_LV1_annhilate ;
      force_init_update = False;
    }
    else if  (LV_LV1_from_cell == (2.0)) {
      LV_LV1_k_u = 1 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
      cstate =  LV_LV1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (LV_LV1_from_cell == (0.0)) {
      LV_LV1_k_u = 1 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
      cstate =  LV_LV1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (LV_LV1_from_cell == (1.0) && (LV_LV1_cell1_mode_delayed == (0.0))) {
      LV_LV1_k_u = 1 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
      cstate =  LV_LV1_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LV_LV1_k_init = LV_LV1_k ;
      slope_LV_LV1_k = 1 ;
      LV_LV1_k_u = (slope_LV_LV1_k * d) + LV_LV1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LV_LV1_previous_direction2 ;
      force_init_update = False;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell1_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LV_LV1!\n");
      exit(1);
    }
    break;
  case ( LV_LV1_wait_cell1 ):
    if (True == False) {;}
    else if  (LV_LV1_cell2_mode == (2.0)) {
      LV_LV1_k_u = 1 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
      cstate =  LV_LV1_annhilate ;
      force_init_update = False;
    }
    else if  (LV_LV1_k >= (30.0)) {
      LV_LV1_from_cell_u = 1 ;
      LV_LV1_cell1_replay_latch_u = 1 ;
      LV_LV1_k_u = 1 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
      cstate =  LV_LV1_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LV_LV1_k_init = LV_LV1_k ;
      slope_LV_LV1_k = 1 ;
      LV_LV1_k_u = (slope_LV_LV1_k * d) + LV_LV1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LV_LV1_wait_cell1 ;
      force_init_update = False;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell1_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LV_LV1!\n");
      exit(1);
    }
    break;
  case ( LV_LV1_replay_cell1 ):
    if (True == False) {;}
    else if  (LV_LV1_cell1_mode == (2.0)) {
      LV_LV1_k_u = 1 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
      cstate =  LV_LV1_annhilate ;
      force_init_update = False;
    }
    else if  (LV_LV1_k >= (30.0)) {
      LV_LV1_from_cell_u = 2 ;
      LV_LV1_cell2_replay_latch_u = 1 ;
      LV_LV1_k_u = 1 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
      cstate =  LV_LV1_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LV_LV1_k_init = LV_LV1_k ;
      slope_LV_LV1_k = 1 ;
      LV_LV1_k_u = (slope_LV_LV1_k * d) + LV_LV1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LV_LV1_replay_cell1 ;
      force_init_update = False;
      LV_LV1_cell1_replay_latch_u = 1 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell1_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LV_LV1!\n");
      exit(1);
    }
    break;
  case ( LV_LV1_replay_cell2 ):
    if (True == False) {;}
    else if  (LV_LV1_k >= (10.0)) {
      LV_LV1_k_u = 1 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
      cstate =  LV_LV1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LV_LV1_k_init = LV_LV1_k ;
      slope_LV_LV1_k = 1 ;
      LV_LV1_k_u = (slope_LV_LV1_k * d) + LV_LV1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LV_LV1_replay_cell2 ;
      force_init_update = False;
      LV_LV1_cell2_replay_latch_u = 1 ;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell1_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LV_LV1!\n");
      exit(1);
    }
    break;
  case ( LV_LV1_wait_cell2 ):
    if (True == False) {;}
    else if  (LV_LV1_k >= (10.0)) {
      LV_LV1_k_u = 1 ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
      cstate =  LV_LV1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LV_LV1_k_init = LV_LV1_k ;
      slope_LV_LV1_k = 1 ;
      LV_LV1_k_u = (slope_LV_LV1_k * d) + LV_LV1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LV_LV1_wait_cell2 ;
      force_init_update = False;
      LV_LV1_cell1_v_delayed_u = LV_LV1_update_c1vd () ;
      LV_LV1_cell2_v_delayed_u = LV_LV1_update_c2vd () ;
      LV_LV1_cell1_mode_delayed_u = LV_LV1_update_c1md () ;
      LV_LV1_cell2_mode_delayed_u = LV_LV1_update_c2md () ;
      LV_LV1_wasted_u = LV_LV1_update_buffer_index (LV_LV1_cell1_v,LV_LV1_cell2_v,LV_LV1_cell1_mode,LV_LV1_cell2_mode) ;
      LV_LV1_cell1_replay_latch_u = LV_LV1_update_latch1 (LV_LV1_cell1_mode_delayed,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_replay_latch_u = LV_LV1_update_latch2 (LV_LV1_cell2_mode_delayed,LV_LV1_cell2_replay_latch_u) ;
      LV_LV1_cell1_v_replay = LV_LV1_update_ocell1 (LV_LV1_cell1_v_delayed_u,LV_LV1_cell1_replay_latch_u) ;
      LV_LV1_cell2_v_replay = LV_LV1_update_ocell2 (LV_LV1_cell2_v_delayed_u,LV_LV1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LV_LV1!\n");
      exit(1);
    }
    break;
  }
  LV_LV1_k = LV_LV1_k_u;
  LV_LV1_cell1_mode_delayed = LV_LV1_cell1_mode_delayed_u;
  LV_LV1_cell2_mode_delayed = LV_LV1_cell2_mode_delayed_u;
  LV_LV1_from_cell = LV_LV1_from_cell_u;
  LV_LV1_cell1_replay_latch = LV_LV1_cell1_replay_latch_u;
  LV_LV1_cell2_replay_latch = LV_LV1_cell2_replay_latch_u;
  LV_LV1_cell1_v_delayed = LV_LV1_cell1_v_delayed_u;
  LV_LV1_cell2_v_delayed = LV_LV1_cell2_v_delayed_u;
  LV_LV1_wasted = LV_LV1_wasted_u;
  return cstate;
}