#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double LVA_LV_update_c1vd();
extern double LVA_LV_update_c2vd();
extern double LVA_LV_update_c1md();
extern double LVA_LV_update_c2md();
extern double LVA_LV_update_buffer_index(double,double,double,double);
extern double LVA_LV_update_latch1(double,double);
extern double LVA_LV_update_latch2(double,double);
extern double LVA_LV_update_ocell1(double,double);
extern double LVA_LV_update_ocell2(double,double);
double LVA_LV_cell1_v;
double LVA_LV_cell1_mode;
double LVA_LV_cell2_v;
double LVA_LV_cell2_mode;
double LVA_LV_cell1_v_replay = 0.0;
double LVA_LV_cell2_v_replay = 0.0;


static double  LVA_LV_k  =  0.0 ,  LVA_LV_cell1_mode_delayed  =  0.0 ,  LVA_LV_cell2_mode_delayed  =  0.0 ,  LVA_LV_from_cell  =  0.0 ,  LVA_LV_cell1_replay_latch  =  0.0 ,  LVA_LV_cell2_replay_latch  =  0.0 ,  LVA_LV_cell1_v_delayed  =  0.0 ,  LVA_LV_cell2_v_delayed  =  0.0 ,  LVA_LV_wasted  =  0.0 ; //the continuous vars
static double  LVA_LV_k_u , LVA_LV_cell1_mode_delayed_u , LVA_LV_cell2_mode_delayed_u , LVA_LV_from_cell_u , LVA_LV_cell1_replay_latch_u , LVA_LV_cell2_replay_latch_u , LVA_LV_cell1_v_delayed_u , LVA_LV_cell2_v_delayed_u , LVA_LV_wasted_u ; // and their updates
static double  LVA_LV_k_init , LVA_LV_cell1_mode_delayed_init , LVA_LV_cell2_mode_delayed_init , LVA_LV_from_cell_init , LVA_LV_cell1_replay_latch_init , LVA_LV_cell2_replay_latch_init , LVA_LV_cell1_v_delayed_init , LVA_LV_cell2_v_delayed_init , LVA_LV_wasted_init ; // and their inits
static double  slope_LVA_LV_k , slope_LVA_LV_cell1_mode_delayed , slope_LVA_LV_cell2_mode_delayed , slope_LVA_LV_from_cell , slope_LVA_LV_cell1_replay_latch , slope_LVA_LV_cell2_replay_latch , slope_LVA_LV_cell1_v_delayed , slope_LVA_LV_cell2_v_delayed , slope_LVA_LV_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { LVA_LV_idle , LVA_LV_annhilate , LVA_LV_previous_drection1 , LVA_LV_previous_direction2 , LVA_LV_wait_cell1 , LVA_LV_replay_cell1 , LVA_LV_replay_cell2 , LVA_LV_wait_cell2 }; // state declarations

enum states LVA_LV (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( LVA_LV_idle ):
    if (True == False) {;}
    else if  (LVA_LV_cell2_mode == (2.0) && (LVA_LV_cell1_mode != (2.0))) {
      LVA_LV_k_u = 1 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
      cstate =  LVA_LV_previous_direction2 ;
      force_init_update = False;
    }
    else if  (LVA_LV_cell1_mode == (2.0) && (LVA_LV_cell2_mode != (2.0))) {
      LVA_LV_k_u = 1 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
      cstate =  LVA_LV_previous_drection1 ;
      force_init_update = False;
    }
    else if  (LVA_LV_cell1_mode == (2.0) && (LVA_LV_cell2_mode == (2.0))) {
      LVA_LV_k_u = 1 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
      cstate =  LVA_LV_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_LV_k_init = LVA_LV_k ;
      slope_LVA_LV_k = 1 ;
      LVA_LV_k_u = (slope_LVA_LV_k * d) + LVA_LV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_LV_idle ;
      force_init_update = False;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell1_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_LV!\n");
      exit(1);
    }
    break;
  case ( LVA_LV_annhilate ):
    if (True == False) {;}
    else if  (LVA_LV_cell1_mode != (2.0) && (LVA_LV_cell2_mode != (2.0))) {
      LVA_LV_k_u = 1 ;
      LVA_LV_from_cell_u = 0 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
      cstate =  LVA_LV_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_LV_k_init = LVA_LV_k ;
      slope_LVA_LV_k = 1 ;
      LVA_LV_k_u = (slope_LVA_LV_k * d) + LVA_LV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_LV_annhilate ;
      force_init_update = False;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell1_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_LV!\n");
      exit(1);
    }
    break;
  case ( LVA_LV_previous_drection1 ):
    if (True == False) {;}
    else if  (LVA_LV_from_cell == (1.0)) {
      LVA_LV_k_u = 1 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
      cstate =  LVA_LV_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LVA_LV_from_cell == (0.0)) {
      LVA_LV_k_u = 1 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
      cstate =  LVA_LV_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LVA_LV_from_cell == (2.0) && (LVA_LV_cell2_mode_delayed == (0.0))) {
      LVA_LV_k_u = 1 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
      cstate =  LVA_LV_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LVA_LV_from_cell == (2.0) && (LVA_LV_cell2_mode_delayed != (0.0))) {
      LVA_LV_k_u = 1 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
      cstate =  LVA_LV_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_LV_k_init = LVA_LV_k ;
      slope_LVA_LV_k = 1 ;
      LVA_LV_k_u = (slope_LVA_LV_k * d) + LVA_LV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_LV_previous_drection1 ;
      force_init_update = False;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell1_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_LV!\n");
      exit(1);
    }
    break;
  case ( LVA_LV_previous_direction2 ):
    if (True == False) {;}
    else if  (LVA_LV_from_cell == (1.0) && (LVA_LV_cell1_mode_delayed != (0.0))) {
      LVA_LV_k_u = 1 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
      cstate =  LVA_LV_annhilate ;
      force_init_update = False;
    }
    else if  (LVA_LV_from_cell == (2.0)) {
      LVA_LV_k_u = 1 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
      cstate =  LVA_LV_replay_cell1 ;
      force_init_update = False;
    }
    else if  (LVA_LV_from_cell == (0.0)) {
      LVA_LV_k_u = 1 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
      cstate =  LVA_LV_replay_cell1 ;
      force_init_update = False;
    }
    else if  (LVA_LV_from_cell == (1.0) && (LVA_LV_cell1_mode_delayed == (0.0))) {
      LVA_LV_k_u = 1 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
      cstate =  LVA_LV_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_LV_k_init = LVA_LV_k ;
      slope_LVA_LV_k = 1 ;
      LVA_LV_k_u = (slope_LVA_LV_k * d) + LVA_LV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_LV_previous_direction2 ;
      force_init_update = False;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell1_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_LV!\n");
      exit(1);
    }
    break;
  case ( LVA_LV_wait_cell1 ):
    if (True == False) {;}
    else if  (LVA_LV_cell2_mode == (2.0)) {
      LVA_LV_k_u = 1 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
      cstate =  LVA_LV_annhilate ;
      force_init_update = False;
    }
    else if  (LVA_LV_k >= (20.0)) {
      LVA_LV_from_cell_u = 1 ;
      LVA_LV_cell1_replay_latch_u = 1 ;
      LVA_LV_k_u = 1 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
      cstate =  LVA_LV_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_LV_k_init = LVA_LV_k ;
      slope_LVA_LV_k = 1 ;
      LVA_LV_k_u = (slope_LVA_LV_k * d) + LVA_LV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_LV_wait_cell1 ;
      force_init_update = False;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell1_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_LV!\n");
      exit(1);
    }
    break;
  case ( LVA_LV_replay_cell1 ):
    if (True == False) {;}
    else if  (LVA_LV_cell1_mode == (2.0)) {
      LVA_LV_k_u = 1 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
      cstate =  LVA_LV_annhilate ;
      force_init_update = False;
    }
    else if  (LVA_LV_k >= (20.0)) {
      LVA_LV_from_cell_u = 2 ;
      LVA_LV_cell2_replay_latch_u = 1 ;
      LVA_LV_k_u = 1 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
      cstate =  LVA_LV_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_LV_k_init = LVA_LV_k ;
      slope_LVA_LV_k = 1 ;
      LVA_LV_k_u = (slope_LVA_LV_k * d) + LVA_LV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_LV_replay_cell1 ;
      force_init_update = False;
      LVA_LV_cell1_replay_latch_u = 1 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell1_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_LV!\n");
      exit(1);
    }
    break;
  case ( LVA_LV_replay_cell2 ):
    if (True == False) {;}
    else if  (LVA_LV_k >= (10.0)) {
      LVA_LV_k_u = 1 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
      cstate =  LVA_LV_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_LV_k_init = LVA_LV_k ;
      slope_LVA_LV_k = 1 ;
      LVA_LV_k_u = (slope_LVA_LV_k * d) + LVA_LV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_LV_replay_cell2 ;
      force_init_update = False;
      LVA_LV_cell2_replay_latch_u = 1 ;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell1_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_LV!\n");
      exit(1);
    }
    break;
  case ( LVA_LV_wait_cell2 ):
    if (True == False) {;}
    else if  (LVA_LV_k >= (10.0)) {
      LVA_LV_k_u = 1 ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
      cstate =  LVA_LV_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_LV_k_init = LVA_LV_k ;
      slope_LVA_LV_k = 1 ;
      LVA_LV_k_u = (slope_LVA_LV_k * d) + LVA_LV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVA_LV_wait_cell2 ;
      force_init_update = False;
      LVA_LV_cell1_v_delayed_u = LVA_LV_update_c1vd () ;
      LVA_LV_cell2_v_delayed_u = LVA_LV_update_c2vd () ;
      LVA_LV_cell1_mode_delayed_u = LVA_LV_update_c1md () ;
      LVA_LV_cell2_mode_delayed_u = LVA_LV_update_c2md () ;
      LVA_LV_wasted_u = LVA_LV_update_buffer_index (LVA_LV_cell1_v,LVA_LV_cell2_v,LVA_LV_cell1_mode,LVA_LV_cell2_mode) ;
      LVA_LV_cell1_replay_latch_u = LVA_LV_update_latch1 (LVA_LV_cell1_mode_delayed,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_replay_latch_u = LVA_LV_update_latch2 (LVA_LV_cell2_mode_delayed,LVA_LV_cell2_replay_latch_u) ;
      LVA_LV_cell1_v_replay = LVA_LV_update_ocell1 (LVA_LV_cell1_v_delayed_u,LVA_LV_cell1_replay_latch_u) ;
      LVA_LV_cell2_v_replay = LVA_LV_update_ocell2 (LVA_LV_cell2_v_delayed_u,LVA_LV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_LV!\n");
      exit(1);
    }
    break;
  }
  LVA_LV_k = LVA_LV_k_u;
  LVA_LV_cell1_mode_delayed = LVA_LV_cell1_mode_delayed_u;
  LVA_LV_cell2_mode_delayed = LVA_LV_cell2_mode_delayed_u;
  LVA_LV_from_cell = LVA_LV_from_cell_u;
  LVA_LV_cell1_replay_latch = LVA_LV_cell1_replay_latch_u;
  LVA_LV_cell2_replay_latch = LVA_LV_cell2_replay_latch_u;
  LVA_LV_cell1_v_delayed = LVA_LV_cell1_v_delayed_u;
  LVA_LV_cell2_v_delayed = LVA_LV_cell2_v_delayed_u;
  LVA_LV_wasted = LVA_LV_wasted_u;
  return cstate;
}