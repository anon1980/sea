#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double RightVentricularSeptum1_v_i_0;
double RightVentricularSeptum1_v_i_1;
double RightVentricularSeptum1_voo = 0.0;
double RightVentricularSeptum1_state = 0.0;


static double  RightVentricularSeptum1_vx  =  0 ,  RightVentricularSeptum1_vy  =  0 ,  RightVentricularSeptum1_vz  =  0 ,  RightVentricularSeptum1_g  =  0 ,  RightVentricularSeptum1_v  =  0 ,  RightVentricularSeptum1_ft  =  0 ,  RightVentricularSeptum1_theta  =  0 ,  RightVentricularSeptum1_v_O  =  0 ; //the continuous vars
static double  RightVentricularSeptum1_vx_u , RightVentricularSeptum1_vy_u , RightVentricularSeptum1_vz_u , RightVentricularSeptum1_g_u , RightVentricularSeptum1_v_u , RightVentricularSeptum1_ft_u , RightVentricularSeptum1_theta_u , RightVentricularSeptum1_v_O_u ; // and their updates
static double  RightVentricularSeptum1_vx_init , RightVentricularSeptum1_vy_init , RightVentricularSeptum1_vz_init , RightVentricularSeptum1_g_init , RightVentricularSeptum1_v_init , RightVentricularSeptum1_ft_init , RightVentricularSeptum1_theta_init , RightVentricularSeptum1_v_O_init ; // and their inits
static double  slope_RightVentricularSeptum1_vx , slope_RightVentricularSeptum1_vy , slope_RightVentricularSeptum1_vz , slope_RightVentricularSeptum1_g , slope_RightVentricularSeptum1_v , slope_RightVentricularSeptum1_ft , slope_RightVentricularSeptum1_theta , slope_RightVentricularSeptum1_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { RightVentricularSeptum1_t1 , RightVentricularSeptum1_t2 , RightVentricularSeptum1_t3 , RightVentricularSeptum1_t4 }; // state declarations

enum states RightVentricularSeptum1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( RightVentricularSeptum1_t1 ):
    if (True == False) {;}
    else if  (RightVentricularSeptum1_g > (44.5)) {
      RightVentricularSeptum1_vx_u = (0.3 * RightVentricularSeptum1_v) ;
      RightVentricularSeptum1_vy_u = 0 ;
      RightVentricularSeptum1_vz_u = (0.7 * RightVentricularSeptum1_v) ;
      RightVentricularSeptum1_g_u = ((((((((RightVentricularSeptum1_v_i_0 + (- ((RightVentricularSeptum1_vx + (- RightVentricularSeptum1_vy)) + RightVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      RightVentricularSeptum1_theta_u = (RightVentricularSeptum1_v / 30.0) ;
      RightVentricularSeptum1_v_O_u = (131.1 + (- (80.1 * pow ( ((RightVentricularSeptum1_v / 30.0)) , (0.5) )))) ;
      RightVentricularSeptum1_ft_u = f (RightVentricularSeptum1_theta,4.0e-2) ;
      cstate =  RightVentricularSeptum1_t2 ;
      force_init_update = False;
    }

    else if ( RightVentricularSeptum1_v <= (44.5)
               && 
              RightVentricularSeptum1_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum1_vx_init = RightVentricularSeptum1_vx ;
      slope_RightVentricularSeptum1_vx = (RightVentricularSeptum1_vx * -8.7) ;
      RightVentricularSeptum1_vx_u = (slope_RightVentricularSeptum1_vx * d) + RightVentricularSeptum1_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum1_vy_init = RightVentricularSeptum1_vy ;
      slope_RightVentricularSeptum1_vy = (RightVentricularSeptum1_vy * -190.9) ;
      RightVentricularSeptum1_vy_u = (slope_RightVentricularSeptum1_vy * d) + RightVentricularSeptum1_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum1_vz_init = RightVentricularSeptum1_vz ;
      slope_RightVentricularSeptum1_vz = (RightVentricularSeptum1_vz * -190.4) ;
      RightVentricularSeptum1_vz_u = (slope_RightVentricularSeptum1_vz * d) + RightVentricularSeptum1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricularSeptum1_t1 ;
      force_init_update = False;
      RightVentricularSeptum1_g_u = ((((((((RightVentricularSeptum1_v_i_0 + (- ((RightVentricularSeptum1_vx + (- RightVentricularSeptum1_vy)) + RightVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      RightVentricularSeptum1_v_u = ((RightVentricularSeptum1_vx + (- RightVentricularSeptum1_vy)) + RightVentricularSeptum1_vz) ;
      RightVentricularSeptum1_voo = ((RightVentricularSeptum1_vx + (- RightVentricularSeptum1_vy)) + RightVentricularSeptum1_vz) ;
      RightVentricularSeptum1_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularSeptum1!\n");
      exit(1);
    }
    break;
  case ( RightVentricularSeptum1_t2 ):
    if (True == False) {;}
    else if  (RightVentricularSeptum1_v >= (44.5)) {
      RightVentricularSeptum1_vx_u = RightVentricularSeptum1_vx ;
      RightVentricularSeptum1_vy_u = RightVentricularSeptum1_vy ;
      RightVentricularSeptum1_vz_u = RightVentricularSeptum1_vz ;
      RightVentricularSeptum1_g_u = ((((((((RightVentricularSeptum1_v_i_0 + (- ((RightVentricularSeptum1_vx + (- RightVentricularSeptum1_vy)) + RightVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  RightVentricularSeptum1_t3 ;
      force_init_update = False;
    }
    else if  (RightVentricularSeptum1_g <= (44.5)
               && 
              RightVentricularSeptum1_v < (44.5)) {
      RightVentricularSeptum1_vx_u = RightVentricularSeptum1_vx ;
      RightVentricularSeptum1_vy_u = RightVentricularSeptum1_vy ;
      RightVentricularSeptum1_vz_u = RightVentricularSeptum1_vz ;
      RightVentricularSeptum1_g_u = ((((((((RightVentricularSeptum1_v_i_0 + (- ((RightVentricularSeptum1_vx + (- RightVentricularSeptum1_vy)) + RightVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  RightVentricularSeptum1_t1 ;
      force_init_update = False;
    }

    else if ( RightVentricularSeptum1_v < (44.5)
               && 
              RightVentricularSeptum1_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum1_vx_init = RightVentricularSeptum1_vx ;
      slope_RightVentricularSeptum1_vx = ((RightVentricularSeptum1_vx * -23.6) + (777200.0 * RightVentricularSeptum1_g)) ;
      RightVentricularSeptum1_vx_u = (slope_RightVentricularSeptum1_vx * d) + RightVentricularSeptum1_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum1_vy_init = RightVentricularSeptum1_vy ;
      slope_RightVentricularSeptum1_vy = ((RightVentricularSeptum1_vy * -45.5) + (58900.0 * RightVentricularSeptum1_g)) ;
      RightVentricularSeptum1_vy_u = (slope_RightVentricularSeptum1_vy * d) + RightVentricularSeptum1_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum1_vz_init = RightVentricularSeptum1_vz ;
      slope_RightVentricularSeptum1_vz = ((RightVentricularSeptum1_vz * -12.9) + (276600.0 * RightVentricularSeptum1_g)) ;
      RightVentricularSeptum1_vz_u = (slope_RightVentricularSeptum1_vz * d) + RightVentricularSeptum1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricularSeptum1_t2 ;
      force_init_update = False;
      RightVentricularSeptum1_g_u = ((((((((RightVentricularSeptum1_v_i_0 + (- ((RightVentricularSeptum1_vx + (- RightVentricularSeptum1_vy)) + RightVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      RightVentricularSeptum1_v_u = ((RightVentricularSeptum1_vx + (- RightVentricularSeptum1_vy)) + RightVentricularSeptum1_vz) ;
      RightVentricularSeptum1_voo = ((RightVentricularSeptum1_vx + (- RightVentricularSeptum1_vy)) + RightVentricularSeptum1_vz) ;
      RightVentricularSeptum1_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularSeptum1!\n");
      exit(1);
    }
    break;
  case ( RightVentricularSeptum1_t3 ):
    if (True == False) {;}
    else if  (RightVentricularSeptum1_v >= (131.1)) {
      RightVentricularSeptum1_vx_u = RightVentricularSeptum1_vx ;
      RightVentricularSeptum1_vy_u = RightVentricularSeptum1_vy ;
      RightVentricularSeptum1_vz_u = RightVentricularSeptum1_vz ;
      RightVentricularSeptum1_g_u = ((((((((RightVentricularSeptum1_v_i_0 + (- ((RightVentricularSeptum1_vx + (- RightVentricularSeptum1_vy)) + RightVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  RightVentricularSeptum1_t4 ;
      force_init_update = False;
    }

    else if ( RightVentricularSeptum1_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum1_vx_init = RightVentricularSeptum1_vx ;
      slope_RightVentricularSeptum1_vx = (RightVentricularSeptum1_vx * -6.9) ;
      RightVentricularSeptum1_vx_u = (slope_RightVentricularSeptum1_vx * d) + RightVentricularSeptum1_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum1_vy_init = RightVentricularSeptum1_vy ;
      slope_RightVentricularSeptum1_vy = (RightVentricularSeptum1_vy * 75.9) ;
      RightVentricularSeptum1_vy_u = (slope_RightVentricularSeptum1_vy * d) + RightVentricularSeptum1_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum1_vz_init = RightVentricularSeptum1_vz ;
      slope_RightVentricularSeptum1_vz = (RightVentricularSeptum1_vz * 6826.5) ;
      RightVentricularSeptum1_vz_u = (slope_RightVentricularSeptum1_vz * d) + RightVentricularSeptum1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricularSeptum1_t3 ;
      force_init_update = False;
      RightVentricularSeptum1_g_u = ((((((((RightVentricularSeptum1_v_i_0 + (- ((RightVentricularSeptum1_vx + (- RightVentricularSeptum1_vy)) + RightVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      RightVentricularSeptum1_v_u = ((RightVentricularSeptum1_vx + (- RightVentricularSeptum1_vy)) + RightVentricularSeptum1_vz) ;
      RightVentricularSeptum1_voo = ((RightVentricularSeptum1_vx + (- RightVentricularSeptum1_vy)) + RightVentricularSeptum1_vz) ;
      RightVentricularSeptum1_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularSeptum1!\n");
      exit(1);
    }
    break;
  case ( RightVentricularSeptum1_t4 ):
    if (True == False) {;}
    else if  (RightVentricularSeptum1_v <= (30.0)) {
      RightVentricularSeptum1_vx_u = RightVentricularSeptum1_vx ;
      RightVentricularSeptum1_vy_u = RightVentricularSeptum1_vy ;
      RightVentricularSeptum1_vz_u = RightVentricularSeptum1_vz ;
      RightVentricularSeptum1_g_u = ((((((((RightVentricularSeptum1_v_i_0 + (- ((RightVentricularSeptum1_vx + (- RightVentricularSeptum1_vy)) + RightVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  RightVentricularSeptum1_t1 ;
      force_init_update = False;
    }

    else if ( RightVentricularSeptum1_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum1_vx_init = RightVentricularSeptum1_vx ;
      slope_RightVentricularSeptum1_vx = (RightVentricularSeptum1_vx * -33.2) ;
      RightVentricularSeptum1_vx_u = (slope_RightVentricularSeptum1_vx * d) + RightVentricularSeptum1_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum1_vy_init = RightVentricularSeptum1_vy ;
      slope_RightVentricularSeptum1_vy = ((RightVentricularSeptum1_vy * 11.0) * RightVentricularSeptum1_ft) ;
      RightVentricularSeptum1_vy_u = (slope_RightVentricularSeptum1_vy * d) + RightVentricularSeptum1_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum1_vz_init = RightVentricularSeptum1_vz ;
      slope_RightVentricularSeptum1_vz = ((RightVentricularSeptum1_vz * 2.0) * RightVentricularSeptum1_ft) ;
      RightVentricularSeptum1_vz_u = (slope_RightVentricularSeptum1_vz * d) + RightVentricularSeptum1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricularSeptum1_t4 ;
      force_init_update = False;
      RightVentricularSeptum1_g_u = ((((((((RightVentricularSeptum1_v_i_0 + (- ((RightVentricularSeptum1_vx + (- RightVentricularSeptum1_vy)) + RightVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      RightVentricularSeptum1_v_u = ((RightVentricularSeptum1_vx + (- RightVentricularSeptum1_vy)) + RightVentricularSeptum1_vz) ;
      RightVentricularSeptum1_voo = ((RightVentricularSeptum1_vx + (- RightVentricularSeptum1_vy)) + RightVentricularSeptum1_vz) ;
      RightVentricularSeptum1_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularSeptum1!\n");
      exit(1);
    }
    break;
  }
  RightVentricularSeptum1_vx = RightVentricularSeptum1_vx_u;
  RightVentricularSeptum1_vy = RightVentricularSeptum1_vy_u;
  RightVentricularSeptum1_vz = RightVentricularSeptum1_vz_u;
  RightVentricularSeptum1_g = RightVentricularSeptum1_g_u;
  RightVentricularSeptum1_v = RightVentricularSeptum1_v_u;
  RightVentricularSeptum1_ft = RightVentricularSeptum1_ft_u;
  RightVentricularSeptum1_theta = RightVentricularSeptum1_theta_u;
  RightVentricularSeptum1_v_O = RightVentricularSeptum1_v_O_u;
  return cstate;
}