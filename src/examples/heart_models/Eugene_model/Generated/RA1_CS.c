#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double RA1_CS_update_c1vd();
extern double RA1_CS_update_c2vd();
extern double RA1_CS_update_c1md();
extern double RA1_CS_update_c2md();
extern double RA1_CS_update_buffer_index(double,double,double,double);
extern double RA1_CS_update_latch1(double,double);
extern double RA1_CS_update_latch2(double,double);
extern double RA1_CS_update_ocell1(double,double);
extern double RA1_CS_update_ocell2(double,double);
double RA1_CS_cell1_v;
double RA1_CS_cell1_mode;
double RA1_CS_cell2_v;
double RA1_CS_cell2_mode;
double RA1_CS_cell1_v_replay = 0.0;
double RA1_CS_cell2_v_replay = 0.0;


static double  RA1_CS_k  =  0.0 ,  RA1_CS_cell1_mode_delayed  =  0.0 ,  RA1_CS_cell2_mode_delayed  =  0.0 ,  RA1_CS_from_cell  =  0.0 ,  RA1_CS_cell1_replay_latch  =  0.0 ,  RA1_CS_cell2_replay_latch  =  0.0 ,  RA1_CS_cell1_v_delayed  =  0.0 ,  RA1_CS_cell2_v_delayed  =  0.0 ,  RA1_CS_wasted  =  0.0 ; //the continuous vars
static double  RA1_CS_k_u , RA1_CS_cell1_mode_delayed_u , RA1_CS_cell2_mode_delayed_u , RA1_CS_from_cell_u , RA1_CS_cell1_replay_latch_u , RA1_CS_cell2_replay_latch_u , RA1_CS_cell1_v_delayed_u , RA1_CS_cell2_v_delayed_u , RA1_CS_wasted_u ; // and their updates
static double  RA1_CS_k_init , RA1_CS_cell1_mode_delayed_init , RA1_CS_cell2_mode_delayed_init , RA1_CS_from_cell_init , RA1_CS_cell1_replay_latch_init , RA1_CS_cell2_replay_latch_init , RA1_CS_cell1_v_delayed_init , RA1_CS_cell2_v_delayed_init , RA1_CS_wasted_init ; // and their inits
static double  slope_RA1_CS_k , slope_RA1_CS_cell1_mode_delayed , slope_RA1_CS_cell2_mode_delayed , slope_RA1_CS_from_cell , slope_RA1_CS_cell1_replay_latch , slope_RA1_CS_cell2_replay_latch , slope_RA1_CS_cell1_v_delayed , slope_RA1_CS_cell2_v_delayed , slope_RA1_CS_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { RA1_CS_idle , RA1_CS_annhilate , RA1_CS_previous_drection1 , RA1_CS_previous_direction2 , RA1_CS_wait_cell1 , RA1_CS_replay_cell1 , RA1_CS_replay_cell2 , RA1_CS_wait_cell2 }; // state declarations

enum states RA1_CS (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( RA1_CS_idle ):
    if (True == False) {;}
    else if  (RA1_CS_cell2_mode == (2.0) && (RA1_CS_cell1_mode != (2.0))) {
      RA1_CS_k_u = 1 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
      cstate =  RA1_CS_previous_direction2 ;
      force_init_update = False;
    }
    else if  (RA1_CS_cell1_mode == (2.0) && (RA1_CS_cell2_mode != (2.0))) {
      RA1_CS_k_u = 1 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
      cstate =  RA1_CS_previous_drection1 ;
      force_init_update = False;
    }
    else if  (RA1_CS_cell1_mode == (2.0) && (RA1_CS_cell2_mode == (2.0))) {
      RA1_CS_k_u = 1 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
      cstate =  RA1_CS_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RA1_CS_k_init = RA1_CS_k ;
      slope_RA1_CS_k = 1 ;
      RA1_CS_k_u = (slope_RA1_CS_k * d) + RA1_CS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RA1_CS_idle ;
      force_init_update = False;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell1_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RA1_CS!\n");
      exit(1);
    }
    break;
  case ( RA1_CS_annhilate ):
    if (True == False) {;}
    else if  (RA1_CS_cell1_mode != (2.0) && (RA1_CS_cell2_mode != (2.0))) {
      RA1_CS_k_u = 1 ;
      RA1_CS_from_cell_u = 0 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
      cstate =  RA1_CS_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RA1_CS_k_init = RA1_CS_k ;
      slope_RA1_CS_k = 1 ;
      RA1_CS_k_u = (slope_RA1_CS_k * d) + RA1_CS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RA1_CS_annhilate ;
      force_init_update = False;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell1_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RA1_CS!\n");
      exit(1);
    }
    break;
  case ( RA1_CS_previous_drection1 ):
    if (True == False) {;}
    else if  (RA1_CS_from_cell == (1.0)) {
      RA1_CS_k_u = 1 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
      cstate =  RA1_CS_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RA1_CS_from_cell == (0.0)) {
      RA1_CS_k_u = 1 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
      cstate =  RA1_CS_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RA1_CS_from_cell == (2.0) && (RA1_CS_cell2_mode_delayed == (0.0))) {
      RA1_CS_k_u = 1 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
      cstate =  RA1_CS_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RA1_CS_from_cell == (2.0) && (RA1_CS_cell2_mode_delayed != (0.0))) {
      RA1_CS_k_u = 1 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
      cstate =  RA1_CS_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RA1_CS_k_init = RA1_CS_k ;
      slope_RA1_CS_k = 1 ;
      RA1_CS_k_u = (slope_RA1_CS_k * d) + RA1_CS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RA1_CS_previous_drection1 ;
      force_init_update = False;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell1_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RA1_CS!\n");
      exit(1);
    }
    break;
  case ( RA1_CS_previous_direction2 ):
    if (True == False) {;}
    else if  (RA1_CS_from_cell == (1.0) && (RA1_CS_cell1_mode_delayed != (0.0))) {
      RA1_CS_k_u = 1 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
      cstate =  RA1_CS_annhilate ;
      force_init_update = False;
    }
    else if  (RA1_CS_from_cell == (2.0)) {
      RA1_CS_k_u = 1 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
      cstate =  RA1_CS_replay_cell1 ;
      force_init_update = False;
    }
    else if  (RA1_CS_from_cell == (0.0)) {
      RA1_CS_k_u = 1 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
      cstate =  RA1_CS_replay_cell1 ;
      force_init_update = False;
    }
    else if  (RA1_CS_from_cell == (1.0) && (RA1_CS_cell1_mode_delayed == (0.0))) {
      RA1_CS_k_u = 1 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
      cstate =  RA1_CS_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RA1_CS_k_init = RA1_CS_k ;
      slope_RA1_CS_k = 1 ;
      RA1_CS_k_u = (slope_RA1_CS_k * d) + RA1_CS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RA1_CS_previous_direction2 ;
      force_init_update = False;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell1_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RA1_CS!\n");
      exit(1);
    }
    break;
  case ( RA1_CS_wait_cell1 ):
    if (True == False) {;}
    else if  (RA1_CS_cell2_mode == (2.0)) {
      RA1_CS_k_u = 1 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
      cstate =  RA1_CS_annhilate ;
      force_init_update = False;
    }
    else if  (RA1_CS_k >= (20.0)) {
      RA1_CS_from_cell_u = 1 ;
      RA1_CS_cell1_replay_latch_u = 1 ;
      RA1_CS_k_u = 1 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
      cstate =  RA1_CS_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RA1_CS_k_init = RA1_CS_k ;
      slope_RA1_CS_k = 1 ;
      RA1_CS_k_u = (slope_RA1_CS_k * d) + RA1_CS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RA1_CS_wait_cell1 ;
      force_init_update = False;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell1_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RA1_CS!\n");
      exit(1);
    }
    break;
  case ( RA1_CS_replay_cell1 ):
    if (True == False) {;}
    else if  (RA1_CS_cell1_mode == (2.0)) {
      RA1_CS_k_u = 1 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
      cstate =  RA1_CS_annhilate ;
      force_init_update = False;
    }
    else if  (RA1_CS_k >= (20.0)) {
      RA1_CS_from_cell_u = 2 ;
      RA1_CS_cell2_replay_latch_u = 1 ;
      RA1_CS_k_u = 1 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
      cstate =  RA1_CS_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RA1_CS_k_init = RA1_CS_k ;
      slope_RA1_CS_k = 1 ;
      RA1_CS_k_u = (slope_RA1_CS_k * d) + RA1_CS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RA1_CS_replay_cell1 ;
      force_init_update = False;
      RA1_CS_cell1_replay_latch_u = 1 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell1_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RA1_CS!\n");
      exit(1);
    }
    break;
  case ( RA1_CS_replay_cell2 ):
    if (True == False) {;}
    else if  (RA1_CS_k >= (10.0)) {
      RA1_CS_k_u = 1 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
      cstate =  RA1_CS_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RA1_CS_k_init = RA1_CS_k ;
      slope_RA1_CS_k = 1 ;
      RA1_CS_k_u = (slope_RA1_CS_k * d) + RA1_CS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RA1_CS_replay_cell2 ;
      force_init_update = False;
      RA1_CS_cell2_replay_latch_u = 1 ;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell1_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RA1_CS!\n");
      exit(1);
    }
    break;
  case ( RA1_CS_wait_cell2 ):
    if (True == False) {;}
    else if  (RA1_CS_k >= (10.0)) {
      RA1_CS_k_u = 1 ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
      cstate =  RA1_CS_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RA1_CS_k_init = RA1_CS_k ;
      slope_RA1_CS_k = 1 ;
      RA1_CS_k_u = (slope_RA1_CS_k * d) + RA1_CS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RA1_CS_wait_cell2 ;
      force_init_update = False;
      RA1_CS_cell1_v_delayed_u = RA1_CS_update_c1vd () ;
      RA1_CS_cell2_v_delayed_u = RA1_CS_update_c2vd () ;
      RA1_CS_cell1_mode_delayed_u = RA1_CS_update_c1md () ;
      RA1_CS_cell2_mode_delayed_u = RA1_CS_update_c2md () ;
      RA1_CS_wasted_u = RA1_CS_update_buffer_index (RA1_CS_cell1_v,RA1_CS_cell2_v,RA1_CS_cell1_mode,RA1_CS_cell2_mode) ;
      RA1_CS_cell1_replay_latch_u = RA1_CS_update_latch1 (RA1_CS_cell1_mode_delayed,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_replay_latch_u = RA1_CS_update_latch2 (RA1_CS_cell2_mode_delayed,RA1_CS_cell2_replay_latch_u) ;
      RA1_CS_cell1_v_replay = RA1_CS_update_ocell1 (RA1_CS_cell1_v_delayed_u,RA1_CS_cell1_replay_latch_u) ;
      RA1_CS_cell2_v_replay = RA1_CS_update_ocell2 (RA1_CS_cell2_v_delayed_u,RA1_CS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RA1_CS!\n");
      exit(1);
    }
    break;
  }
  RA1_CS_k = RA1_CS_k_u;
  RA1_CS_cell1_mode_delayed = RA1_CS_cell1_mode_delayed_u;
  RA1_CS_cell2_mode_delayed = RA1_CS_cell2_mode_delayed_u;
  RA1_CS_from_cell = RA1_CS_from_cell_u;
  RA1_CS_cell1_replay_latch = RA1_CS_cell1_replay_latch_u;
  RA1_CS_cell2_replay_latch = RA1_CS_cell2_replay_latch_u;
  RA1_CS_cell1_v_delayed = RA1_CS_cell1_v_delayed_u;
  RA1_CS_cell2_v_delayed = RA1_CS_cell2_v_delayed_u;
  RA1_CS_wasted = RA1_CS_wasted_u;
  return cstate;
}