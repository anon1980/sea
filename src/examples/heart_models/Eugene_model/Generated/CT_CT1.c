#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double CT_CT1_update_c1vd();
extern double CT_CT1_update_c2vd();
extern double CT_CT1_update_c1md();
extern double CT_CT1_update_c2md();
extern double CT_CT1_update_buffer_index(double,double,double,double);
extern double CT_CT1_update_latch1(double,double);
extern double CT_CT1_update_latch2(double,double);
extern double CT_CT1_update_ocell1(double,double);
extern double CT_CT1_update_ocell2(double,double);
double CT_CT1_cell1_v;
double CT_CT1_cell1_mode;
double CT_CT1_cell2_v;
double CT_CT1_cell2_mode;
double CT_CT1_cell1_v_replay = 0.0;
double CT_CT1_cell2_v_replay = 0.0;


static double  CT_CT1_k  =  0.0 ,  CT_CT1_cell1_mode_delayed  =  0.0 ,  CT_CT1_cell2_mode_delayed  =  0.0 ,  CT_CT1_from_cell  =  0.0 ,  CT_CT1_cell1_replay_latch  =  0.0 ,  CT_CT1_cell2_replay_latch  =  0.0 ,  CT_CT1_cell1_v_delayed  =  0.0 ,  CT_CT1_cell2_v_delayed  =  0.0 ,  CT_CT1_wasted  =  0.0 ; //the continuous vars
static double  CT_CT1_k_u , CT_CT1_cell1_mode_delayed_u , CT_CT1_cell2_mode_delayed_u , CT_CT1_from_cell_u , CT_CT1_cell1_replay_latch_u , CT_CT1_cell2_replay_latch_u , CT_CT1_cell1_v_delayed_u , CT_CT1_cell2_v_delayed_u , CT_CT1_wasted_u ; // and their updates
static double  CT_CT1_k_init , CT_CT1_cell1_mode_delayed_init , CT_CT1_cell2_mode_delayed_init , CT_CT1_from_cell_init , CT_CT1_cell1_replay_latch_init , CT_CT1_cell2_replay_latch_init , CT_CT1_cell1_v_delayed_init , CT_CT1_cell2_v_delayed_init , CT_CT1_wasted_init ; // and their inits
static double  slope_CT_CT1_k , slope_CT_CT1_cell1_mode_delayed , slope_CT_CT1_cell2_mode_delayed , slope_CT_CT1_from_cell , slope_CT_CT1_cell1_replay_latch , slope_CT_CT1_cell2_replay_latch , slope_CT_CT1_cell1_v_delayed , slope_CT_CT1_cell2_v_delayed , slope_CT_CT1_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { CT_CT1_idle , CT_CT1_annhilate , CT_CT1_previous_drection1 , CT_CT1_previous_direction2 , CT_CT1_wait_cell1 , CT_CT1_replay_cell1 , CT_CT1_replay_cell2 , CT_CT1_wait_cell2 }; // state declarations

enum states CT_CT1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( CT_CT1_idle ):
    if (True == False) {;}
    else if  (CT_CT1_cell2_mode == (2.0) && (CT_CT1_cell1_mode != (2.0))) {
      CT_CT1_k_u = 1 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
      cstate =  CT_CT1_previous_direction2 ;
      force_init_update = False;
    }
    else if  (CT_CT1_cell1_mode == (2.0) && (CT_CT1_cell2_mode != (2.0))) {
      CT_CT1_k_u = 1 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
      cstate =  CT_CT1_previous_drection1 ;
      force_init_update = False;
    }
    else if  (CT_CT1_cell1_mode == (2.0) && (CT_CT1_cell2_mode == (2.0))) {
      CT_CT1_k_u = 1 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
      cstate =  CT_CT1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) CT_CT1_k_init = CT_CT1_k ;
      slope_CT_CT1_k = 1 ;
      CT_CT1_k_u = (slope_CT_CT1_k * d) + CT_CT1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  CT_CT1_idle ;
      force_init_update = False;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell1_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CT_CT1!\n");
      exit(1);
    }
    break;
  case ( CT_CT1_annhilate ):
    if (True == False) {;}
    else if  (CT_CT1_cell1_mode != (2.0) && (CT_CT1_cell2_mode != (2.0))) {
      CT_CT1_k_u = 1 ;
      CT_CT1_from_cell_u = 0 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
      cstate =  CT_CT1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) CT_CT1_k_init = CT_CT1_k ;
      slope_CT_CT1_k = 1 ;
      CT_CT1_k_u = (slope_CT_CT1_k * d) + CT_CT1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  CT_CT1_annhilate ;
      force_init_update = False;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell1_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CT_CT1!\n");
      exit(1);
    }
    break;
  case ( CT_CT1_previous_drection1 ):
    if (True == False) {;}
    else if  (CT_CT1_from_cell == (1.0)) {
      CT_CT1_k_u = 1 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
      cstate =  CT_CT1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (CT_CT1_from_cell == (0.0)) {
      CT_CT1_k_u = 1 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
      cstate =  CT_CT1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (CT_CT1_from_cell == (2.0) && (CT_CT1_cell2_mode_delayed == (0.0))) {
      CT_CT1_k_u = 1 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
      cstate =  CT_CT1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (CT_CT1_from_cell == (2.0) && (CT_CT1_cell2_mode_delayed != (0.0))) {
      CT_CT1_k_u = 1 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
      cstate =  CT_CT1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) CT_CT1_k_init = CT_CT1_k ;
      slope_CT_CT1_k = 1 ;
      CT_CT1_k_u = (slope_CT_CT1_k * d) + CT_CT1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  CT_CT1_previous_drection1 ;
      force_init_update = False;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell1_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CT_CT1!\n");
      exit(1);
    }
    break;
  case ( CT_CT1_previous_direction2 ):
    if (True == False) {;}
    else if  (CT_CT1_from_cell == (1.0) && (CT_CT1_cell1_mode_delayed != (0.0))) {
      CT_CT1_k_u = 1 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
      cstate =  CT_CT1_annhilate ;
      force_init_update = False;
    }
    else if  (CT_CT1_from_cell == (2.0)) {
      CT_CT1_k_u = 1 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
      cstate =  CT_CT1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (CT_CT1_from_cell == (0.0)) {
      CT_CT1_k_u = 1 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
      cstate =  CT_CT1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (CT_CT1_from_cell == (1.0) && (CT_CT1_cell1_mode_delayed == (0.0))) {
      CT_CT1_k_u = 1 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
      cstate =  CT_CT1_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) CT_CT1_k_init = CT_CT1_k ;
      slope_CT_CT1_k = 1 ;
      CT_CT1_k_u = (slope_CT_CT1_k * d) + CT_CT1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  CT_CT1_previous_direction2 ;
      force_init_update = False;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell1_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CT_CT1!\n");
      exit(1);
    }
    break;
  case ( CT_CT1_wait_cell1 ):
    if (True == False) {;}
    else if  (CT_CT1_cell2_mode == (2.0)) {
      CT_CT1_k_u = 1 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
      cstate =  CT_CT1_annhilate ;
      force_init_update = False;
    }
    else if  (CT_CT1_k >= (20.0)) {
      CT_CT1_from_cell_u = 1 ;
      CT_CT1_cell1_replay_latch_u = 1 ;
      CT_CT1_k_u = 1 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
      cstate =  CT_CT1_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) CT_CT1_k_init = CT_CT1_k ;
      slope_CT_CT1_k = 1 ;
      CT_CT1_k_u = (slope_CT_CT1_k * d) + CT_CT1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  CT_CT1_wait_cell1 ;
      force_init_update = False;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell1_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CT_CT1!\n");
      exit(1);
    }
    break;
  case ( CT_CT1_replay_cell1 ):
    if (True == False) {;}
    else if  (CT_CT1_cell1_mode == (2.0)) {
      CT_CT1_k_u = 1 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
      cstate =  CT_CT1_annhilate ;
      force_init_update = False;
    }
    else if  (CT_CT1_k >= (20.0)) {
      CT_CT1_from_cell_u = 2 ;
      CT_CT1_cell2_replay_latch_u = 1 ;
      CT_CT1_k_u = 1 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
      cstate =  CT_CT1_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) CT_CT1_k_init = CT_CT1_k ;
      slope_CT_CT1_k = 1 ;
      CT_CT1_k_u = (slope_CT_CT1_k * d) + CT_CT1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  CT_CT1_replay_cell1 ;
      force_init_update = False;
      CT_CT1_cell1_replay_latch_u = 1 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell1_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CT_CT1!\n");
      exit(1);
    }
    break;
  case ( CT_CT1_replay_cell2 ):
    if (True == False) {;}
    else if  (CT_CT1_k >= (10.0)) {
      CT_CT1_k_u = 1 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
      cstate =  CT_CT1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) CT_CT1_k_init = CT_CT1_k ;
      slope_CT_CT1_k = 1 ;
      CT_CT1_k_u = (slope_CT_CT1_k * d) + CT_CT1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  CT_CT1_replay_cell2 ;
      force_init_update = False;
      CT_CT1_cell2_replay_latch_u = 1 ;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell1_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CT_CT1!\n");
      exit(1);
    }
    break;
  case ( CT_CT1_wait_cell2 ):
    if (True == False) {;}
    else if  (CT_CT1_k >= (10.0)) {
      CT_CT1_k_u = 1 ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
      cstate =  CT_CT1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) CT_CT1_k_init = CT_CT1_k ;
      slope_CT_CT1_k = 1 ;
      CT_CT1_k_u = (slope_CT_CT1_k * d) + CT_CT1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  CT_CT1_wait_cell2 ;
      force_init_update = False;
      CT_CT1_cell1_v_delayed_u = CT_CT1_update_c1vd () ;
      CT_CT1_cell2_v_delayed_u = CT_CT1_update_c2vd () ;
      CT_CT1_cell1_mode_delayed_u = CT_CT1_update_c1md () ;
      CT_CT1_cell2_mode_delayed_u = CT_CT1_update_c2md () ;
      CT_CT1_wasted_u = CT_CT1_update_buffer_index (CT_CT1_cell1_v,CT_CT1_cell2_v,CT_CT1_cell1_mode,CT_CT1_cell2_mode) ;
      CT_CT1_cell1_replay_latch_u = CT_CT1_update_latch1 (CT_CT1_cell1_mode_delayed,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_replay_latch_u = CT_CT1_update_latch2 (CT_CT1_cell2_mode_delayed,CT_CT1_cell2_replay_latch_u) ;
      CT_CT1_cell1_v_replay = CT_CT1_update_ocell1 (CT_CT1_cell1_v_delayed_u,CT_CT1_cell1_replay_latch_u) ;
      CT_CT1_cell2_v_replay = CT_CT1_update_ocell2 (CT_CT1_cell2_v_delayed_u,CT_CT1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CT_CT1!\n");
      exit(1);
    }
    break;
  }
  CT_CT1_k = CT_CT1_k_u;
  CT_CT1_cell1_mode_delayed = CT_CT1_cell1_mode_delayed_u;
  CT_CT1_cell2_mode_delayed = CT_CT1_cell2_mode_delayed_u;
  CT_CT1_from_cell = CT_CT1_from_cell_u;
  CT_CT1_cell1_replay_latch = CT_CT1_cell1_replay_latch_u;
  CT_CT1_cell2_replay_latch = CT_CT1_cell2_replay_latch_u;
  CT_CT1_cell1_v_delayed = CT_CT1_cell1_v_delayed_u;
  CT_CT1_cell2_v_delayed = CT_CT1_cell2_v_delayed_u;
  CT_CT1_wasted = CT_CT1_wasted_u;
  return cstate;
}