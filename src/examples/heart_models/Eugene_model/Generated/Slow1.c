#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double Slow1_v_i_0;
double Slow1_v_i_1;
double Slow1_v_i_2;
double Slow1_voo = 0.0;
double Slow1_state = 0.0;


static double  Slow1_vx  =  0 ,  Slow1_vy  =  0 ,  Slow1_vz  =  0 ,  Slow1_g  =  0 ,  Slow1_v  =  0 ,  Slow1_ft  =  0 ,  Slow1_theta  =  0 ,  Slow1_v_O  =  0 ; //the continuous vars
static double  Slow1_vx_u , Slow1_vy_u , Slow1_vz_u , Slow1_g_u , Slow1_v_u , Slow1_ft_u , Slow1_theta_u , Slow1_v_O_u ; // and their updates
static double  Slow1_vx_init , Slow1_vy_init , Slow1_vz_init , Slow1_g_init , Slow1_v_init , Slow1_ft_init , Slow1_theta_init , Slow1_v_O_init ; // and their inits
static double  slope_Slow1_vx , slope_Slow1_vy , slope_Slow1_vz , slope_Slow1_g , slope_Slow1_v , slope_Slow1_ft , slope_Slow1_theta , slope_Slow1_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { Slow1_t1 , Slow1_t2 , Slow1_t3 , Slow1_t4 }; // state declarations

enum states Slow1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( Slow1_t1 ):
    if (True == False) {;}
    else if  (Slow1_g > (44.5)) {
      Slow1_vx_u = (0.3 * Slow1_v) ;
      Slow1_vy_u = 0 ;
      Slow1_vz_u = (0.7 * Slow1_v) ;
      Slow1_g_u = ((((((((Slow1_v_i_0 + (- ((Slow1_vx + (- Slow1_vy)) + Slow1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Slow1_v_i_1 + (- ((Slow1_vx + (- Slow1_vy)) + Slow1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Slow1_theta_u = (Slow1_v / 30.0) ;
      Slow1_v_O_u = (131.1 + (- (80.1 * pow ( ((Slow1_v / 30.0)) , (0.5) )))) ;
      Slow1_ft_u = f (Slow1_theta,4.0e-2) ;
      cstate =  Slow1_t2 ;
      force_init_update = False;
    }

    else if ( Slow1_v <= (44.5)
               && Slow1_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) Slow1_vx_init = Slow1_vx ;
      slope_Slow1_vx = (Slow1_vx * -8.7) ;
      Slow1_vx_u = (slope_Slow1_vx * d) + Slow1_vx ;
      if ((pstate != cstate) || force_init_update) Slow1_vy_init = Slow1_vy ;
      slope_Slow1_vy = (Slow1_vy * -190.9) ;
      Slow1_vy_u = (slope_Slow1_vy * d) + Slow1_vy ;
      if ((pstate != cstate) || force_init_update) Slow1_vz_init = Slow1_vz ;
      slope_Slow1_vz = (Slow1_vz * -190.4) ;
      Slow1_vz_u = (slope_Slow1_vz * d) + Slow1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Slow1_t1 ;
      force_init_update = False;
      Slow1_g_u = ((((((((Slow1_v_i_0 + (- ((Slow1_vx + (- Slow1_vy)) + Slow1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Slow1_v_i_1 + (- ((Slow1_vx + (- Slow1_vy)) + Slow1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Slow1_v_u = ((Slow1_vx + (- Slow1_vy)) + Slow1_vz) ;
      Slow1_voo = ((Slow1_vx + (- Slow1_vy)) + Slow1_vz) ;
      Slow1_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow1!\n");
      exit(1);
    }
    break;
  case ( Slow1_t2 ):
    if (True == False) {;}
    else if  (Slow1_v >= (44.5)) {
      Slow1_vx_u = Slow1_vx ;
      Slow1_vy_u = Slow1_vy ;
      Slow1_vz_u = Slow1_vz ;
      Slow1_g_u = ((((((((Slow1_v_i_0 + (- ((Slow1_vx + (- Slow1_vy)) + Slow1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Slow1_v_i_1 + (- ((Slow1_vx + (- Slow1_vy)) + Slow1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  Slow1_t3 ;
      force_init_update = False;
    }
    else if  (Slow1_g <= (44.5)
               && Slow1_v < (44.5)) {
      Slow1_vx_u = Slow1_vx ;
      Slow1_vy_u = Slow1_vy ;
      Slow1_vz_u = Slow1_vz ;
      Slow1_g_u = ((((((((Slow1_v_i_0 + (- ((Slow1_vx + (- Slow1_vy)) + Slow1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Slow1_v_i_1 + (- ((Slow1_vx + (- Slow1_vy)) + Slow1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  Slow1_t1 ;
      force_init_update = False;
    }

    else if ( Slow1_v < (44.5)
               && Slow1_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) Slow1_vx_init = Slow1_vx ;
      slope_Slow1_vx = ((Slow1_vx * -23.6) + (777200.0 * Slow1_g)) ;
      Slow1_vx_u = (slope_Slow1_vx * d) + Slow1_vx ;
      if ((pstate != cstate) || force_init_update) Slow1_vy_init = Slow1_vy ;
      slope_Slow1_vy = ((Slow1_vy * -45.5) + (58900.0 * Slow1_g)) ;
      Slow1_vy_u = (slope_Slow1_vy * d) + Slow1_vy ;
      if ((pstate != cstate) || force_init_update) Slow1_vz_init = Slow1_vz ;
      slope_Slow1_vz = ((Slow1_vz * -12.9) + (276600.0 * Slow1_g)) ;
      Slow1_vz_u = (slope_Slow1_vz * d) + Slow1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Slow1_t2 ;
      force_init_update = False;
      Slow1_g_u = ((((((((Slow1_v_i_0 + (- ((Slow1_vx + (- Slow1_vy)) + Slow1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Slow1_v_i_1 + (- ((Slow1_vx + (- Slow1_vy)) + Slow1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Slow1_v_u = ((Slow1_vx + (- Slow1_vy)) + Slow1_vz) ;
      Slow1_voo = ((Slow1_vx + (- Slow1_vy)) + Slow1_vz) ;
      Slow1_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow1!\n");
      exit(1);
    }
    break;
  case ( Slow1_t3 ):
    if (True == False) {;}
    else if  (Slow1_v >= (131.1)) {
      Slow1_vx_u = Slow1_vx ;
      Slow1_vy_u = Slow1_vy ;
      Slow1_vz_u = Slow1_vz ;
      Slow1_g_u = ((((((((Slow1_v_i_0 + (- ((Slow1_vx + (- Slow1_vy)) + Slow1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Slow1_v_i_1 + (- ((Slow1_vx + (- Slow1_vy)) + Slow1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  Slow1_t4 ;
      force_init_update = False;
    }

    else if ( Slow1_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) Slow1_vx_init = Slow1_vx ;
      slope_Slow1_vx = (Slow1_vx * -6.9) ;
      Slow1_vx_u = (slope_Slow1_vx * d) + Slow1_vx ;
      if ((pstate != cstate) || force_init_update) Slow1_vy_init = Slow1_vy ;
      slope_Slow1_vy = (Slow1_vy * 75.9) ;
      Slow1_vy_u = (slope_Slow1_vy * d) + Slow1_vy ;
      if ((pstate != cstate) || force_init_update) Slow1_vz_init = Slow1_vz ;
      slope_Slow1_vz = (Slow1_vz * 6826.5) ;
      Slow1_vz_u = (slope_Slow1_vz * d) + Slow1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Slow1_t3 ;
      force_init_update = False;
      Slow1_g_u = ((((((((Slow1_v_i_0 + (- ((Slow1_vx + (- Slow1_vy)) + Slow1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Slow1_v_i_1 + (- ((Slow1_vx + (- Slow1_vy)) + Slow1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Slow1_v_u = ((Slow1_vx + (- Slow1_vy)) + Slow1_vz) ;
      Slow1_voo = ((Slow1_vx + (- Slow1_vy)) + Slow1_vz) ;
      Slow1_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow1!\n");
      exit(1);
    }
    break;
  case ( Slow1_t4 ):
    if (True == False) {;}
    else if  (Slow1_v <= (30.0)) {
      Slow1_vx_u = Slow1_vx ;
      Slow1_vy_u = Slow1_vy ;
      Slow1_vz_u = Slow1_vz ;
      Slow1_g_u = ((((((((Slow1_v_i_0 + (- ((Slow1_vx + (- Slow1_vy)) + Slow1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Slow1_v_i_1 + (- ((Slow1_vx + (- Slow1_vy)) + Slow1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  Slow1_t1 ;
      force_init_update = False;
    }

    else if ( Slow1_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) Slow1_vx_init = Slow1_vx ;
      slope_Slow1_vx = (Slow1_vx * -33.2) ;
      Slow1_vx_u = (slope_Slow1_vx * d) + Slow1_vx ;
      if ((pstate != cstate) || force_init_update) Slow1_vy_init = Slow1_vy ;
      slope_Slow1_vy = ((Slow1_vy * 20.0) * Slow1_ft) ;
      Slow1_vy_u = (slope_Slow1_vy * d) + Slow1_vy ;
      if ((pstate != cstate) || force_init_update) Slow1_vz_init = Slow1_vz ;
      slope_Slow1_vz = ((Slow1_vz * 2.0) * Slow1_ft) ;
      Slow1_vz_u = (slope_Slow1_vz * d) + Slow1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  Slow1_t4 ;
      force_init_update = False;
      Slow1_g_u = ((((((((Slow1_v_i_0 + (- ((Slow1_vx + (- Slow1_vy)) + Slow1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((Slow1_v_i_1 + (- ((Slow1_vx + (- Slow1_vy)) + Slow1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      Slow1_v_u = ((Slow1_vx + (- Slow1_vy)) + Slow1_vz) ;
      Slow1_voo = ((Slow1_vx + (- Slow1_vy)) + Slow1_vz) ;
      Slow1_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow1!\n");
      exit(1);
    }
    break;
  }
  Slow1_vx = Slow1_vx_u;
  Slow1_vy = Slow1_vy_u;
  Slow1_vz = Slow1_vz_u;
  Slow1_g = Slow1_g_u;
  Slow1_v = Slow1_v_u;
  Slow1_ft = Slow1_ft_u;
  Slow1_theta = Slow1_theta_u;
  Slow1_v_O = Slow1_v_O_u;
  return cstate;
}