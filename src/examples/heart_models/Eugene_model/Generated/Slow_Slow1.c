#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double Slow_Slow1_update_c1vd();
extern double Slow_Slow1_update_c2vd();
extern double Slow_Slow1_update_c1md();
extern double Slow_Slow1_update_c2md();
extern double Slow_Slow1_update_buffer_index(double,double,double,double);
extern double Slow_Slow1_update_latch1(double,double);
extern double Slow_Slow1_update_latch2(double,double);
extern double Slow_Slow1_update_ocell1(double,double);
extern double Slow_Slow1_update_ocell2(double,double);
double Slow_Slow1_cell1_v;
double Slow_Slow1_cell1_mode;
double Slow_Slow1_cell2_v;
double Slow_Slow1_cell2_mode;
double Slow_Slow1_cell1_v_replay = 0.0;
double Slow_Slow1_cell2_v_replay = 0.0;


static double  Slow_Slow1_k  =  0.0 ,  Slow_Slow1_cell1_mode_delayed  =  0.0 ,  Slow_Slow1_cell2_mode_delayed  =  0.0 ,  Slow_Slow1_from_cell  =  0.0 ,  Slow_Slow1_cell1_replay_latch  =  0.0 ,  Slow_Slow1_cell2_replay_latch  =  0.0 ,  Slow_Slow1_cell1_v_delayed  =  0.0 ,  Slow_Slow1_cell2_v_delayed  =  0.0 ,  Slow_Slow1_wasted  =  0.0 ; //the continuous vars
static double  Slow_Slow1_k_u , Slow_Slow1_cell1_mode_delayed_u , Slow_Slow1_cell2_mode_delayed_u , Slow_Slow1_from_cell_u , Slow_Slow1_cell1_replay_latch_u , Slow_Slow1_cell2_replay_latch_u , Slow_Slow1_cell1_v_delayed_u , Slow_Slow1_cell2_v_delayed_u , Slow_Slow1_wasted_u ; // and their updates
static double  Slow_Slow1_k_init , Slow_Slow1_cell1_mode_delayed_init , Slow_Slow1_cell2_mode_delayed_init , Slow_Slow1_from_cell_init , Slow_Slow1_cell1_replay_latch_init , Slow_Slow1_cell2_replay_latch_init , Slow_Slow1_cell1_v_delayed_init , Slow_Slow1_cell2_v_delayed_init , Slow_Slow1_wasted_init ; // and their inits
static double  slope_Slow_Slow1_k , slope_Slow_Slow1_cell1_mode_delayed , slope_Slow_Slow1_cell2_mode_delayed , slope_Slow_Slow1_from_cell , slope_Slow_Slow1_cell1_replay_latch , slope_Slow_Slow1_cell2_replay_latch , slope_Slow_Slow1_cell1_v_delayed , slope_Slow_Slow1_cell2_v_delayed , slope_Slow_Slow1_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { Slow_Slow1_idle , Slow_Slow1_annhilate , Slow_Slow1_previous_drection1 , Slow_Slow1_previous_direction2 , Slow_Slow1_wait_cell1 , Slow_Slow1_replay_cell1 , Slow_Slow1_replay_cell2 , Slow_Slow1_wait_cell2 }; // state declarations

enum states Slow_Slow1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( Slow_Slow1_idle ):
    if (True == False) {;}
    else if  (Slow_Slow1_cell2_mode == (2.0) && (Slow_Slow1_cell1_mode != (2.0))) {
      Slow_Slow1_k_u = 1 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
      cstate =  Slow_Slow1_previous_direction2 ;
      force_init_update = False;
    }
    else if  (Slow_Slow1_cell1_mode == (2.0) && (Slow_Slow1_cell2_mode != (2.0))) {
      Slow_Slow1_k_u = 1 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
      cstate =  Slow_Slow1_previous_drection1 ;
      force_init_update = False;
    }
    else if  (Slow_Slow1_cell1_mode == (2.0) && (Slow_Slow1_cell2_mode == (2.0))) {
      Slow_Slow1_k_u = 1 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
      cstate =  Slow_Slow1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Slow_Slow1_k_init = Slow_Slow1_k ;
      slope_Slow_Slow1_k = 1 ;
      Slow_Slow1_k_u = (slope_Slow_Slow1_k * d) + Slow_Slow1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Slow_Slow1_idle ;
      force_init_update = False;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell1_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow_Slow1!\n");
      exit(1);
    }
    break;
  case ( Slow_Slow1_annhilate ):
    if (True == False) {;}
    else if  (Slow_Slow1_cell1_mode != (2.0) && (Slow_Slow1_cell2_mode != (2.0))) {
      Slow_Slow1_k_u = 1 ;
      Slow_Slow1_from_cell_u = 0 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
      cstate =  Slow_Slow1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Slow_Slow1_k_init = Slow_Slow1_k ;
      slope_Slow_Slow1_k = 1 ;
      Slow_Slow1_k_u = (slope_Slow_Slow1_k * d) + Slow_Slow1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Slow_Slow1_annhilate ;
      force_init_update = False;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell1_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow_Slow1!\n");
      exit(1);
    }
    break;
  case ( Slow_Slow1_previous_drection1 ):
    if (True == False) {;}
    else if  (Slow_Slow1_from_cell == (1.0)) {
      Slow_Slow1_k_u = 1 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
      cstate =  Slow_Slow1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (Slow_Slow1_from_cell == (0.0)) {
      Slow_Slow1_k_u = 1 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
      cstate =  Slow_Slow1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (Slow_Slow1_from_cell == (2.0) && (Slow_Slow1_cell2_mode_delayed == (0.0))) {
      Slow_Slow1_k_u = 1 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
      cstate =  Slow_Slow1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (Slow_Slow1_from_cell == (2.0) && (Slow_Slow1_cell2_mode_delayed != (0.0))) {
      Slow_Slow1_k_u = 1 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
      cstate =  Slow_Slow1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Slow_Slow1_k_init = Slow_Slow1_k ;
      slope_Slow_Slow1_k = 1 ;
      Slow_Slow1_k_u = (slope_Slow_Slow1_k * d) + Slow_Slow1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Slow_Slow1_previous_drection1 ;
      force_init_update = False;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell1_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow_Slow1!\n");
      exit(1);
    }
    break;
  case ( Slow_Slow1_previous_direction2 ):
    if (True == False) {;}
    else if  (Slow_Slow1_from_cell == (1.0) && (Slow_Slow1_cell1_mode_delayed != (0.0))) {
      Slow_Slow1_k_u = 1 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
      cstate =  Slow_Slow1_annhilate ;
      force_init_update = False;
    }
    else if  (Slow_Slow1_from_cell == (2.0)) {
      Slow_Slow1_k_u = 1 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
      cstate =  Slow_Slow1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (Slow_Slow1_from_cell == (0.0)) {
      Slow_Slow1_k_u = 1 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
      cstate =  Slow_Slow1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (Slow_Slow1_from_cell == (1.0) && (Slow_Slow1_cell1_mode_delayed == (0.0))) {
      Slow_Slow1_k_u = 1 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
      cstate =  Slow_Slow1_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Slow_Slow1_k_init = Slow_Slow1_k ;
      slope_Slow_Slow1_k = 1 ;
      Slow_Slow1_k_u = (slope_Slow_Slow1_k * d) + Slow_Slow1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Slow_Slow1_previous_direction2 ;
      force_init_update = False;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell1_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow_Slow1!\n");
      exit(1);
    }
    break;
  case ( Slow_Slow1_wait_cell1 ):
    if (True == False) {;}
    else if  (Slow_Slow1_cell2_mode == (2.0)) {
      Slow_Slow1_k_u = 1 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
      cstate =  Slow_Slow1_annhilate ;
      force_init_update = False;
    }
    else if  (Slow_Slow1_k >= (30.0)) {
      Slow_Slow1_from_cell_u = 1 ;
      Slow_Slow1_cell1_replay_latch_u = 1 ;
      Slow_Slow1_k_u = 1 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
      cstate =  Slow_Slow1_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Slow_Slow1_k_init = Slow_Slow1_k ;
      slope_Slow_Slow1_k = 1 ;
      Slow_Slow1_k_u = (slope_Slow_Slow1_k * d) + Slow_Slow1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Slow_Slow1_wait_cell1 ;
      force_init_update = False;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell1_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow_Slow1!\n");
      exit(1);
    }
    break;
  case ( Slow_Slow1_replay_cell1 ):
    if (True == False) {;}
    else if  (Slow_Slow1_cell1_mode == (2.0)) {
      Slow_Slow1_k_u = 1 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
      cstate =  Slow_Slow1_annhilate ;
      force_init_update = False;
    }
    else if  (Slow_Slow1_k >= (30.0)) {
      Slow_Slow1_from_cell_u = 2 ;
      Slow_Slow1_cell2_replay_latch_u = 1 ;
      Slow_Slow1_k_u = 1 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
      cstate =  Slow_Slow1_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Slow_Slow1_k_init = Slow_Slow1_k ;
      slope_Slow_Slow1_k = 1 ;
      Slow_Slow1_k_u = (slope_Slow_Slow1_k * d) + Slow_Slow1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Slow_Slow1_replay_cell1 ;
      force_init_update = False;
      Slow_Slow1_cell1_replay_latch_u = 1 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell1_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow_Slow1!\n");
      exit(1);
    }
    break;
  case ( Slow_Slow1_replay_cell2 ):
    if (True == False) {;}
    else if  (Slow_Slow1_k >= (10.0)) {
      Slow_Slow1_k_u = 1 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
      cstate =  Slow_Slow1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Slow_Slow1_k_init = Slow_Slow1_k ;
      slope_Slow_Slow1_k = 1 ;
      Slow_Slow1_k_u = (slope_Slow_Slow1_k * d) + Slow_Slow1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Slow_Slow1_replay_cell2 ;
      force_init_update = False;
      Slow_Slow1_cell2_replay_latch_u = 1 ;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell1_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow_Slow1!\n");
      exit(1);
    }
    break;
  case ( Slow_Slow1_wait_cell2 ):
    if (True == False) {;}
    else if  (Slow_Slow1_k >= (10.0)) {
      Slow_Slow1_k_u = 1 ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
      cstate =  Slow_Slow1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Slow_Slow1_k_init = Slow_Slow1_k ;
      slope_Slow_Slow1_k = 1 ;
      Slow_Slow1_k_u = (slope_Slow_Slow1_k * d) + Slow_Slow1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Slow_Slow1_wait_cell2 ;
      force_init_update = False;
      Slow_Slow1_cell1_v_delayed_u = Slow_Slow1_update_c1vd () ;
      Slow_Slow1_cell2_v_delayed_u = Slow_Slow1_update_c2vd () ;
      Slow_Slow1_cell1_mode_delayed_u = Slow_Slow1_update_c1md () ;
      Slow_Slow1_cell2_mode_delayed_u = Slow_Slow1_update_c2md () ;
      Slow_Slow1_wasted_u = Slow_Slow1_update_buffer_index (Slow_Slow1_cell1_v,Slow_Slow1_cell2_v,Slow_Slow1_cell1_mode,Slow_Slow1_cell2_mode) ;
      Slow_Slow1_cell1_replay_latch_u = Slow_Slow1_update_latch1 (Slow_Slow1_cell1_mode_delayed,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_replay_latch_u = Slow_Slow1_update_latch2 (Slow_Slow1_cell2_mode_delayed,Slow_Slow1_cell2_replay_latch_u) ;
      Slow_Slow1_cell1_v_replay = Slow_Slow1_update_ocell1 (Slow_Slow1_cell1_v_delayed_u,Slow_Slow1_cell1_replay_latch_u) ;
      Slow_Slow1_cell2_v_replay = Slow_Slow1_update_ocell2 (Slow_Slow1_cell2_v_delayed_u,Slow_Slow1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow_Slow1!\n");
      exit(1);
    }
    break;
  }
  Slow_Slow1_k = Slow_Slow1_k_u;
  Slow_Slow1_cell1_mode_delayed = Slow_Slow1_cell1_mode_delayed_u;
  Slow_Slow1_cell2_mode_delayed = Slow_Slow1_cell2_mode_delayed_u;
  Slow_Slow1_from_cell = Slow_Slow1_from_cell_u;
  Slow_Slow1_cell1_replay_latch = Slow_Slow1_cell1_replay_latch_u;
  Slow_Slow1_cell2_replay_latch = Slow_Slow1_cell2_replay_latch_u;
  Slow_Slow1_cell1_v_delayed = Slow_Slow1_cell1_v_delayed_u;
  Slow_Slow1_cell2_v_delayed = Slow_Slow1_cell2_v_delayed_u;
  Slow_Slow1_wasted = Slow_Slow1_wasted_u;
  return cstate;
}