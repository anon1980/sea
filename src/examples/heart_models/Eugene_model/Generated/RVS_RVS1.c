#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double RVS_RVS1_update_c1vd();
extern double RVS_RVS1_update_c2vd();
extern double RVS_RVS1_update_c1md();
extern double RVS_RVS1_update_c2md();
extern double RVS_RVS1_update_buffer_index(double,double,double,double);
extern double RVS_RVS1_update_latch1(double,double);
extern double RVS_RVS1_update_latch2(double,double);
extern double RVS_RVS1_update_ocell1(double,double);
extern double RVS_RVS1_update_ocell2(double,double);
double RVS_RVS1_cell1_v;
double RVS_RVS1_cell1_mode;
double RVS_RVS1_cell2_v;
double RVS_RVS1_cell2_mode;
double RVS_RVS1_cell1_v_replay = 0.0;
double RVS_RVS1_cell2_v_replay = 0.0;


static double  RVS_RVS1_k  =  0.0 ,  RVS_RVS1_cell1_mode_delayed  =  0.0 ,  RVS_RVS1_cell2_mode_delayed  =  0.0 ,  RVS_RVS1_from_cell  =  0.0 ,  RVS_RVS1_cell1_replay_latch  =  0.0 ,  RVS_RVS1_cell2_replay_latch  =  0.0 ,  RVS_RVS1_cell1_v_delayed  =  0.0 ,  RVS_RVS1_cell2_v_delayed  =  0.0 ,  RVS_RVS1_wasted  =  0.0 ; //the continuous vars
static double  RVS_RVS1_k_u , RVS_RVS1_cell1_mode_delayed_u , RVS_RVS1_cell2_mode_delayed_u , RVS_RVS1_from_cell_u , RVS_RVS1_cell1_replay_latch_u , RVS_RVS1_cell2_replay_latch_u , RVS_RVS1_cell1_v_delayed_u , RVS_RVS1_cell2_v_delayed_u , RVS_RVS1_wasted_u ; // and their updates
static double  RVS_RVS1_k_init , RVS_RVS1_cell1_mode_delayed_init , RVS_RVS1_cell2_mode_delayed_init , RVS_RVS1_from_cell_init , RVS_RVS1_cell1_replay_latch_init , RVS_RVS1_cell2_replay_latch_init , RVS_RVS1_cell1_v_delayed_init , RVS_RVS1_cell2_v_delayed_init , RVS_RVS1_wasted_init ; // and their inits
static double  slope_RVS_RVS1_k , slope_RVS_RVS1_cell1_mode_delayed , slope_RVS_RVS1_cell2_mode_delayed , slope_RVS_RVS1_from_cell , slope_RVS_RVS1_cell1_replay_latch , slope_RVS_RVS1_cell2_replay_latch , slope_RVS_RVS1_cell1_v_delayed , slope_RVS_RVS1_cell2_v_delayed , slope_RVS_RVS1_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { RVS_RVS1_idle , RVS_RVS1_annhilate , RVS_RVS1_previous_drection1 , RVS_RVS1_previous_direction2 , RVS_RVS1_wait_cell1 , RVS_RVS1_replay_cell1 , RVS_RVS1_replay_cell2 , RVS_RVS1_wait_cell2 }; // state declarations

enum states RVS_RVS1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( RVS_RVS1_idle ):
    if (True == False) {;}
    else if  (RVS_RVS1_cell2_mode == (2.0) && (RVS_RVS1_cell1_mode != (2.0))) {
      RVS_RVS1_k_u = 1 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
      cstate =  RVS_RVS1_previous_direction2 ;
      force_init_update = False;
    }
    else if  (RVS_RVS1_cell1_mode == (2.0) && (RVS_RVS1_cell2_mode != (2.0))) {
      RVS_RVS1_k_u = 1 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
      cstate =  RVS_RVS1_previous_drection1 ;
      force_init_update = False;
    }
    else if  (RVS_RVS1_cell1_mode == (2.0) && (RVS_RVS1_cell2_mode == (2.0))) {
      RVS_RVS1_k_u = 1 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
      cstate =  RVS_RVS1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVS_RVS1_k_init = RVS_RVS1_k ;
      slope_RVS_RVS1_k = 1 ;
      RVS_RVS1_k_u = (slope_RVS_RVS1_k * d) + RVS_RVS1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVS_RVS1_idle ;
      force_init_update = False;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell1_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVS_RVS1!\n");
      exit(1);
    }
    break;
  case ( RVS_RVS1_annhilate ):
    if (True == False) {;}
    else if  (RVS_RVS1_cell1_mode != (2.0) && (RVS_RVS1_cell2_mode != (2.0))) {
      RVS_RVS1_k_u = 1 ;
      RVS_RVS1_from_cell_u = 0 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
      cstate =  RVS_RVS1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVS_RVS1_k_init = RVS_RVS1_k ;
      slope_RVS_RVS1_k = 1 ;
      RVS_RVS1_k_u = (slope_RVS_RVS1_k * d) + RVS_RVS1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVS_RVS1_annhilate ;
      force_init_update = False;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell1_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVS_RVS1!\n");
      exit(1);
    }
    break;
  case ( RVS_RVS1_previous_drection1 ):
    if (True == False) {;}
    else if  (RVS_RVS1_from_cell == (1.0)) {
      RVS_RVS1_k_u = 1 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
      cstate =  RVS_RVS1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RVS_RVS1_from_cell == (0.0)) {
      RVS_RVS1_k_u = 1 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
      cstate =  RVS_RVS1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RVS_RVS1_from_cell == (2.0) && (RVS_RVS1_cell2_mode_delayed == (0.0))) {
      RVS_RVS1_k_u = 1 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
      cstate =  RVS_RVS1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RVS_RVS1_from_cell == (2.0) && (RVS_RVS1_cell2_mode_delayed != (0.0))) {
      RVS_RVS1_k_u = 1 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
      cstate =  RVS_RVS1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVS_RVS1_k_init = RVS_RVS1_k ;
      slope_RVS_RVS1_k = 1 ;
      RVS_RVS1_k_u = (slope_RVS_RVS1_k * d) + RVS_RVS1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVS_RVS1_previous_drection1 ;
      force_init_update = False;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell1_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVS_RVS1!\n");
      exit(1);
    }
    break;
  case ( RVS_RVS1_previous_direction2 ):
    if (True == False) {;}
    else if  (RVS_RVS1_from_cell == (1.0) && (RVS_RVS1_cell1_mode_delayed != (0.0))) {
      RVS_RVS1_k_u = 1 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
      cstate =  RVS_RVS1_annhilate ;
      force_init_update = False;
    }
    else if  (RVS_RVS1_from_cell == (2.0)) {
      RVS_RVS1_k_u = 1 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
      cstate =  RVS_RVS1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (RVS_RVS1_from_cell == (0.0)) {
      RVS_RVS1_k_u = 1 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
      cstate =  RVS_RVS1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (RVS_RVS1_from_cell == (1.0) && (RVS_RVS1_cell1_mode_delayed == (0.0))) {
      RVS_RVS1_k_u = 1 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
      cstate =  RVS_RVS1_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVS_RVS1_k_init = RVS_RVS1_k ;
      slope_RVS_RVS1_k = 1 ;
      RVS_RVS1_k_u = (slope_RVS_RVS1_k * d) + RVS_RVS1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVS_RVS1_previous_direction2 ;
      force_init_update = False;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell1_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVS_RVS1!\n");
      exit(1);
    }
    break;
  case ( RVS_RVS1_wait_cell1 ):
    if (True == False) {;}
    else if  (RVS_RVS1_cell2_mode == (2.0)) {
      RVS_RVS1_k_u = 1 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
      cstate =  RVS_RVS1_annhilate ;
      force_init_update = False;
    }
    else if  (RVS_RVS1_k >= (20.0)) {
      RVS_RVS1_from_cell_u = 1 ;
      RVS_RVS1_cell1_replay_latch_u = 1 ;
      RVS_RVS1_k_u = 1 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
      cstate =  RVS_RVS1_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVS_RVS1_k_init = RVS_RVS1_k ;
      slope_RVS_RVS1_k = 1 ;
      RVS_RVS1_k_u = (slope_RVS_RVS1_k * d) + RVS_RVS1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVS_RVS1_wait_cell1 ;
      force_init_update = False;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell1_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVS_RVS1!\n");
      exit(1);
    }
    break;
  case ( RVS_RVS1_replay_cell1 ):
    if (True == False) {;}
    else if  (RVS_RVS1_cell1_mode == (2.0)) {
      RVS_RVS1_k_u = 1 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
      cstate =  RVS_RVS1_annhilate ;
      force_init_update = False;
    }
    else if  (RVS_RVS1_k >= (20.0)) {
      RVS_RVS1_from_cell_u = 2 ;
      RVS_RVS1_cell2_replay_latch_u = 1 ;
      RVS_RVS1_k_u = 1 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
      cstate =  RVS_RVS1_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVS_RVS1_k_init = RVS_RVS1_k ;
      slope_RVS_RVS1_k = 1 ;
      RVS_RVS1_k_u = (slope_RVS_RVS1_k * d) + RVS_RVS1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVS_RVS1_replay_cell1 ;
      force_init_update = False;
      RVS_RVS1_cell1_replay_latch_u = 1 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell1_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVS_RVS1!\n");
      exit(1);
    }
    break;
  case ( RVS_RVS1_replay_cell2 ):
    if (True == False) {;}
    else if  (RVS_RVS1_k >= (10.0)) {
      RVS_RVS1_k_u = 1 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
      cstate =  RVS_RVS1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVS_RVS1_k_init = RVS_RVS1_k ;
      slope_RVS_RVS1_k = 1 ;
      RVS_RVS1_k_u = (slope_RVS_RVS1_k * d) + RVS_RVS1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVS_RVS1_replay_cell2 ;
      force_init_update = False;
      RVS_RVS1_cell2_replay_latch_u = 1 ;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell1_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVS_RVS1!\n");
      exit(1);
    }
    break;
  case ( RVS_RVS1_wait_cell2 ):
    if (True == False) {;}
    else if  (RVS_RVS1_k >= (10.0)) {
      RVS_RVS1_k_u = 1 ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
      cstate =  RVS_RVS1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVS_RVS1_k_init = RVS_RVS1_k ;
      slope_RVS_RVS1_k = 1 ;
      RVS_RVS1_k_u = (slope_RVS_RVS1_k * d) + RVS_RVS1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVS_RVS1_wait_cell2 ;
      force_init_update = False;
      RVS_RVS1_cell1_v_delayed_u = RVS_RVS1_update_c1vd () ;
      RVS_RVS1_cell2_v_delayed_u = RVS_RVS1_update_c2vd () ;
      RVS_RVS1_cell1_mode_delayed_u = RVS_RVS1_update_c1md () ;
      RVS_RVS1_cell2_mode_delayed_u = RVS_RVS1_update_c2md () ;
      RVS_RVS1_wasted_u = RVS_RVS1_update_buffer_index (RVS_RVS1_cell1_v,RVS_RVS1_cell2_v,RVS_RVS1_cell1_mode,RVS_RVS1_cell2_mode) ;
      RVS_RVS1_cell1_replay_latch_u = RVS_RVS1_update_latch1 (RVS_RVS1_cell1_mode_delayed,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_replay_latch_u = RVS_RVS1_update_latch2 (RVS_RVS1_cell2_mode_delayed,RVS_RVS1_cell2_replay_latch_u) ;
      RVS_RVS1_cell1_v_replay = RVS_RVS1_update_ocell1 (RVS_RVS1_cell1_v_delayed_u,RVS_RVS1_cell1_replay_latch_u) ;
      RVS_RVS1_cell2_v_replay = RVS_RVS1_update_ocell2 (RVS_RVS1_cell2_v_delayed_u,RVS_RVS1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVS_RVS1!\n");
      exit(1);
    }
    break;
  }
  RVS_RVS1_k = RVS_RVS1_k_u;
  RVS_RVS1_cell1_mode_delayed = RVS_RVS1_cell1_mode_delayed_u;
  RVS_RVS1_cell2_mode_delayed = RVS_RVS1_cell2_mode_delayed_u;
  RVS_RVS1_from_cell = RVS_RVS1_from_cell_u;
  RVS_RVS1_cell1_replay_latch = RVS_RVS1_cell1_replay_latch_u;
  RVS_RVS1_cell2_replay_latch = RVS_RVS1_cell2_replay_latch_u;
  RVS_RVS1_cell1_v_delayed = RVS_RVS1_cell1_v_delayed_u;
  RVS_RVS1_cell2_v_delayed = RVS_RVS1_cell2_v_delayed_u;
  RVS_RVS1_wasted = RVS_RVS1_wasted_u;
  return cstate;
}