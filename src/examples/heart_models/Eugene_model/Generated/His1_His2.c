#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double His1_His2_update_c1vd();
extern double His1_His2_update_c2vd();
extern double His1_His2_update_c1md();
extern double His1_His2_update_c2md();
extern double His1_His2_update_buffer_index(double,double,double,double);
extern double His1_His2_update_latch1(double,double);
extern double His1_His2_update_latch2(double,double);
extern double His1_His2_update_ocell1(double,double);
extern double His1_His2_update_ocell2(double,double);
double His1_His2_cell1_v;
double His1_His2_cell1_mode;
double His1_His2_cell2_v;
double His1_His2_cell2_mode;
double His1_His2_cell1_v_replay = 0.0;
double His1_His2_cell2_v_replay = 0.0;


static double  His1_His2_k  =  0.0 ,  His1_His2_cell1_mode_delayed  =  0.0 ,  His1_His2_cell2_mode_delayed  =  0.0 ,  His1_His2_from_cell  =  0.0 ,  His1_His2_cell1_replay_latch  =  0.0 ,  His1_His2_cell2_replay_latch  =  0.0 ,  His1_His2_cell1_v_delayed  =  0.0 ,  His1_His2_cell2_v_delayed  =  0.0 ,  His1_His2_wasted  =  0.0 ; //the continuous vars
static double  His1_His2_k_u , His1_His2_cell1_mode_delayed_u , His1_His2_cell2_mode_delayed_u , His1_His2_from_cell_u , His1_His2_cell1_replay_latch_u , His1_His2_cell2_replay_latch_u , His1_His2_cell1_v_delayed_u , His1_His2_cell2_v_delayed_u , His1_His2_wasted_u ; // and their updates
static double  His1_His2_k_init , His1_His2_cell1_mode_delayed_init , His1_His2_cell2_mode_delayed_init , His1_His2_from_cell_init , His1_His2_cell1_replay_latch_init , His1_His2_cell2_replay_latch_init , His1_His2_cell1_v_delayed_init , His1_His2_cell2_v_delayed_init , His1_His2_wasted_init ; // and their inits
static double  slope_His1_His2_k , slope_His1_His2_cell1_mode_delayed , slope_His1_His2_cell2_mode_delayed , slope_His1_His2_from_cell , slope_His1_His2_cell1_replay_latch , slope_His1_His2_cell2_replay_latch , slope_His1_His2_cell1_v_delayed , slope_His1_His2_cell2_v_delayed , slope_His1_His2_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { His1_His2_idle , His1_His2_annhilate , His1_His2_previous_drection1 , His1_His2_previous_direction2 , His1_His2_wait_cell1 , His1_His2_replay_cell1 , His1_His2_replay_cell2 , His1_His2_wait_cell2 }; // state declarations

enum states His1_His2 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( His1_His2_idle ):
    if (True == False) {;}
    else if  (His1_His2_cell2_mode == (2.0) && (His1_His2_cell1_mode != (2.0))) {
      His1_His2_k_u = 1 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
      cstate =  His1_His2_previous_direction2 ;
      force_init_update = False;
    }
    else if  (His1_His2_cell1_mode == (2.0) && (His1_His2_cell2_mode != (2.0))) {
      His1_His2_k_u = 1 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
      cstate =  His1_His2_previous_drection1 ;
      force_init_update = False;
    }
    else if  (His1_His2_cell1_mode == (2.0) && (His1_His2_cell2_mode == (2.0))) {
      His1_His2_k_u = 1 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
      cstate =  His1_His2_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His1_His2_k_init = His1_His2_k ;
      slope_His1_His2_k = 1 ;
      His1_His2_k_u = (slope_His1_His2_k * d) + His1_His2_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His1_His2_idle ;
      force_init_update = False;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell1_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His1_His2!\n");
      exit(1);
    }
    break;
  case ( His1_His2_annhilate ):
    if (True == False) {;}
    else if  (His1_His2_cell1_mode != (2.0) && (His1_His2_cell2_mode != (2.0))) {
      His1_His2_k_u = 1 ;
      His1_His2_from_cell_u = 0 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
      cstate =  His1_His2_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His1_His2_k_init = His1_His2_k ;
      slope_His1_His2_k = 1 ;
      His1_His2_k_u = (slope_His1_His2_k * d) + His1_His2_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His1_His2_annhilate ;
      force_init_update = False;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell1_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His1_His2!\n");
      exit(1);
    }
    break;
  case ( His1_His2_previous_drection1 ):
    if (True == False) {;}
    else if  (His1_His2_from_cell == (1.0)) {
      His1_His2_k_u = 1 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
      cstate =  His1_His2_wait_cell1 ;
      force_init_update = False;
    }
    else if  (His1_His2_from_cell == (0.0)) {
      His1_His2_k_u = 1 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
      cstate =  His1_His2_wait_cell1 ;
      force_init_update = False;
    }
    else if  (His1_His2_from_cell == (2.0) && (His1_His2_cell2_mode_delayed == (0.0))) {
      His1_His2_k_u = 1 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
      cstate =  His1_His2_wait_cell1 ;
      force_init_update = False;
    }
    else if  (His1_His2_from_cell == (2.0) && (His1_His2_cell2_mode_delayed != (0.0))) {
      His1_His2_k_u = 1 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
      cstate =  His1_His2_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His1_His2_k_init = His1_His2_k ;
      slope_His1_His2_k = 1 ;
      His1_His2_k_u = (slope_His1_His2_k * d) + His1_His2_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His1_His2_previous_drection1 ;
      force_init_update = False;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell1_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His1_His2!\n");
      exit(1);
    }
    break;
  case ( His1_His2_previous_direction2 ):
    if (True == False) {;}
    else if  (His1_His2_from_cell == (1.0) && (His1_His2_cell1_mode_delayed != (0.0))) {
      His1_His2_k_u = 1 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
      cstate =  His1_His2_annhilate ;
      force_init_update = False;
    }
    else if  (His1_His2_from_cell == (2.0)) {
      His1_His2_k_u = 1 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
      cstate =  His1_His2_replay_cell1 ;
      force_init_update = False;
    }
    else if  (His1_His2_from_cell == (0.0)) {
      His1_His2_k_u = 1 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
      cstate =  His1_His2_replay_cell1 ;
      force_init_update = False;
    }
    else if  (His1_His2_from_cell == (1.0) && (His1_His2_cell1_mode_delayed == (0.0))) {
      His1_His2_k_u = 1 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
      cstate =  His1_His2_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His1_His2_k_init = His1_His2_k ;
      slope_His1_His2_k = 1 ;
      His1_His2_k_u = (slope_His1_His2_k * d) + His1_His2_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His1_His2_previous_direction2 ;
      force_init_update = False;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell1_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His1_His2!\n");
      exit(1);
    }
    break;
  case ( His1_His2_wait_cell1 ):
    if (True == False) {;}
    else if  (His1_His2_cell2_mode == (2.0)) {
      His1_His2_k_u = 1 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
      cstate =  His1_His2_annhilate ;
      force_init_update = False;
    }
    else if  (His1_His2_k >= (20.0)) {
      His1_His2_from_cell_u = 1 ;
      His1_His2_cell1_replay_latch_u = 1 ;
      His1_His2_k_u = 1 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
      cstate =  His1_His2_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His1_His2_k_init = His1_His2_k ;
      slope_His1_His2_k = 1 ;
      His1_His2_k_u = (slope_His1_His2_k * d) + His1_His2_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His1_His2_wait_cell1 ;
      force_init_update = False;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell1_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His1_His2!\n");
      exit(1);
    }
    break;
  case ( His1_His2_replay_cell1 ):
    if (True == False) {;}
    else if  (His1_His2_cell1_mode == (2.0)) {
      His1_His2_k_u = 1 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
      cstate =  His1_His2_annhilate ;
      force_init_update = False;
    }
    else if  (His1_His2_k >= (20.0)) {
      His1_His2_from_cell_u = 2 ;
      His1_His2_cell2_replay_latch_u = 1 ;
      His1_His2_k_u = 1 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
      cstate =  His1_His2_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His1_His2_k_init = His1_His2_k ;
      slope_His1_His2_k = 1 ;
      His1_His2_k_u = (slope_His1_His2_k * d) + His1_His2_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His1_His2_replay_cell1 ;
      force_init_update = False;
      His1_His2_cell1_replay_latch_u = 1 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell1_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His1_His2!\n");
      exit(1);
    }
    break;
  case ( His1_His2_replay_cell2 ):
    if (True == False) {;}
    else if  (His1_His2_k >= (10.0)) {
      His1_His2_k_u = 1 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
      cstate =  His1_His2_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His1_His2_k_init = His1_His2_k ;
      slope_His1_His2_k = 1 ;
      His1_His2_k_u = (slope_His1_His2_k * d) + His1_His2_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His1_His2_replay_cell2 ;
      force_init_update = False;
      His1_His2_cell2_replay_latch_u = 1 ;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell1_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His1_His2!\n");
      exit(1);
    }
    break;
  case ( His1_His2_wait_cell2 ):
    if (True == False) {;}
    else if  (His1_His2_k >= (10.0)) {
      His1_His2_k_u = 1 ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
      cstate =  His1_His2_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His1_His2_k_init = His1_His2_k ;
      slope_His1_His2_k = 1 ;
      His1_His2_k_u = (slope_His1_His2_k * d) + His1_His2_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His1_His2_wait_cell2 ;
      force_init_update = False;
      His1_His2_cell1_v_delayed_u = His1_His2_update_c1vd () ;
      His1_His2_cell2_v_delayed_u = His1_His2_update_c2vd () ;
      His1_His2_cell1_mode_delayed_u = His1_His2_update_c1md () ;
      His1_His2_cell2_mode_delayed_u = His1_His2_update_c2md () ;
      His1_His2_wasted_u = His1_His2_update_buffer_index (His1_His2_cell1_v,His1_His2_cell2_v,His1_His2_cell1_mode,His1_His2_cell2_mode) ;
      His1_His2_cell1_replay_latch_u = His1_His2_update_latch1 (His1_His2_cell1_mode_delayed,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_replay_latch_u = His1_His2_update_latch2 (His1_His2_cell2_mode_delayed,His1_His2_cell2_replay_latch_u) ;
      His1_His2_cell1_v_replay = His1_His2_update_ocell1 (His1_His2_cell1_v_delayed_u,His1_His2_cell1_replay_latch_u) ;
      His1_His2_cell2_v_replay = His1_His2_update_ocell2 (His1_His2_cell2_v_delayed_u,His1_His2_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His1_His2!\n");
      exit(1);
    }
    break;
  }
  His1_His2_k = His1_His2_k_u;
  His1_His2_cell1_mode_delayed = His1_His2_cell1_mode_delayed_u;
  His1_His2_cell2_mode_delayed = His1_His2_cell2_mode_delayed_u;
  His1_His2_from_cell = His1_His2_from_cell_u;
  His1_His2_cell1_replay_latch = His1_His2_cell1_replay_latch_u;
  His1_His2_cell2_replay_latch = His1_His2_cell2_replay_latch_u;
  His1_His2_cell1_v_delayed = His1_His2_cell1_v_delayed_u;
  His1_His2_cell2_v_delayed = His1_His2_cell2_v_delayed_u;
  His1_His2_wasted = His1_His2_wasted_u;
  return cstate;
}