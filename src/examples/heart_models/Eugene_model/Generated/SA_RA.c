#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double SA_RA_update_c1vd();
extern double SA_RA_update_c2vd();
extern double SA_RA_update_c1md();
extern double SA_RA_update_c2md();
extern double SA_RA_update_buffer_index(double,double,double,double);
extern double SA_RA_update_latch1(double,double);
extern double SA_RA_update_latch2(double,double);
extern double SA_RA_update_ocell1(double,double);
extern double SA_RA_update_ocell2(double,double);
double SA_RA_cell1_v;
double SA_RA_cell1_mode;
double SA_RA_cell2_v;
double SA_RA_cell2_mode;
double SA_RA_cell1_v_replay = 0.0;
double SA_RA_cell2_v_replay = 0.0;


static double  SA_RA_k  =  0.0 ,  SA_RA_cell1_mode_delayed  =  0.0 ,  SA_RA_cell2_mode_delayed  =  0.0 ,  SA_RA_from_cell  =  0.0 ,  SA_RA_cell1_replay_latch  =  0.0 ,  SA_RA_cell2_replay_latch  =  0.0 ,  SA_RA_cell1_v_delayed  =  0.0 ,  SA_RA_cell2_v_delayed  =  0.0 ,  SA_RA_wasted  =  0.0 ; //the continuous vars
static double  SA_RA_k_u , SA_RA_cell1_mode_delayed_u , SA_RA_cell2_mode_delayed_u , SA_RA_from_cell_u , SA_RA_cell1_replay_latch_u , SA_RA_cell2_replay_latch_u , SA_RA_cell1_v_delayed_u , SA_RA_cell2_v_delayed_u , SA_RA_wasted_u ; // and their updates
static double  SA_RA_k_init , SA_RA_cell1_mode_delayed_init , SA_RA_cell2_mode_delayed_init , SA_RA_from_cell_init , SA_RA_cell1_replay_latch_init , SA_RA_cell2_replay_latch_init , SA_RA_cell1_v_delayed_init , SA_RA_cell2_v_delayed_init , SA_RA_wasted_init ; // and their inits
static double  slope_SA_RA_k , slope_SA_RA_cell1_mode_delayed , slope_SA_RA_cell2_mode_delayed , slope_SA_RA_from_cell , slope_SA_RA_cell1_replay_latch , slope_SA_RA_cell2_replay_latch , slope_SA_RA_cell1_v_delayed , slope_SA_RA_cell2_v_delayed , slope_SA_RA_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { SA_RA_idle , SA_RA_annhilate , SA_RA_previous_drection1 , SA_RA_previous_direction2 , SA_RA_wait_cell1 , SA_RA_replay_cell1 , SA_RA_replay_cell2 , SA_RA_wait_cell2 }; // state declarations

enum states SA_RA (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( SA_RA_idle ):
    if (True == False) {;}
    else if  (SA_RA_cell2_mode == (2.0) && (SA_RA_cell1_mode != (2.0))) {
      SA_RA_k_u = 1 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
      cstate =  SA_RA_previous_direction2 ;
      force_init_update = False;
    }
    else if  (SA_RA_cell1_mode == (2.0) && (SA_RA_cell2_mode != (2.0))) {
      SA_RA_k_u = 1 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
      cstate =  SA_RA_previous_drection1 ;
      force_init_update = False;
    }
    else if  (SA_RA_cell1_mode == (2.0) && (SA_RA_cell2_mode == (2.0))) {
      SA_RA_k_u = 1 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
      cstate =  SA_RA_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) SA_RA_k_init = SA_RA_k ;
      slope_SA_RA_k = 1 ;
      SA_RA_k_u = (slope_SA_RA_k * d) + SA_RA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  SA_RA_idle ;
      force_init_update = False;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell1_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_RA!\n");
      exit(1);
    }
    break;
  case ( SA_RA_annhilate ):
    if (True == False) {;}
    else if  (SA_RA_cell1_mode != (2.0) && (SA_RA_cell2_mode != (2.0))) {
      SA_RA_k_u = 1 ;
      SA_RA_from_cell_u = 0 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
      cstate =  SA_RA_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) SA_RA_k_init = SA_RA_k ;
      slope_SA_RA_k = 1 ;
      SA_RA_k_u = (slope_SA_RA_k * d) + SA_RA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  SA_RA_annhilate ;
      force_init_update = False;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell1_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_RA!\n");
      exit(1);
    }
    break;
  case ( SA_RA_previous_drection1 ):
    if (True == False) {;}
    else if  (SA_RA_from_cell == (1.0)) {
      SA_RA_k_u = 1 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
      cstate =  SA_RA_wait_cell1 ;
      force_init_update = False;
    }
    else if  (SA_RA_from_cell == (0.0)) {
      SA_RA_k_u = 1 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
      cstate =  SA_RA_wait_cell1 ;
      force_init_update = False;
    }
    else if  (SA_RA_from_cell == (2.0) && (SA_RA_cell2_mode_delayed == (0.0))) {
      SA_RA_k_u = 1 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
      cstate =  SA_RA_wait_cell1 ;
      force_init_update = False;
    }
    else if  (SA_RA_from_cell == (2.0) && (SA_RA_cell2_mode_delayed != (0.0))) {
      SA_RA_k_u = 1 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
      cstate =  SA_RA_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) SA_RA_k_init = SA_RA_k ;
      slope_SA_RA_k = 1 ;
      SA_RA_k_u = (slope_SA_RA_k * d) + SA_RA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  SA_RA_previous_drection1 ;
      force_init_update = False;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell1_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_RA!\n");
      exit(1);
    }
    break;
  case ( SA_RA_previous_direction2 ):
    if (True == False) {;}
    else if  (SA_RA_from_cell == (1.0) && (SA_RA_cell1_mode_delayed != (0.0))) {
      SA_RA_k_u = 1 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
      cstate =  SA_RA_annhilate ;
      force_init_update = False;
    }
    else if  (SA_RA_from_cell == (2.0)) {
      SA_RA_k_u = 1 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
      cstate =  SA_RA_replay_cell1 ;
      force_init_update = False;
    }
    else if  (SA_RA_from_cell == (0.0)) {
      SA_RA_k_u = 1 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
      cstate =  SA_RA_replay_cell1 ;
      force_init_update = False;
    }
    else if  (SA_RA_from_cell == (1.0) && (SA_RA_cell1_mode_delayed == (0.0))) {
      SA_RA_k_u = 1 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
      cstate =  SA_RA_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) SA_RA_k_init = SA_RA_k ;
      slope_SA_RA_k = 1 ;
      SA_RA_k_u = (slope_SA_RA_k * d) + SA_RA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  SA_RA_previous_direction2 ;
      force_init_update = False;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell1_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_RA!\n");
      exit(1);
    }
    break;
  case ( SA_RA_wait_cell1 ):
    if (True == False) {;}
    else if  (SA_RA_cell2_mode == (2.0)) {
      SA_RA_k_u = 1 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
      cstate =  SA_RA_annhilate ;
      force_init_update = False;
    }
    else if  (SA_RA_k >= (20.0)) {
      SA_RA_from_cell_u = 1 ;
      SA_RA_cell1_replay_latch_u = 1 ;
      SA_RA_k_u = 1 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
      cstate =  SA_RA_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) SA_RA_k_init = SA_RA_k ;
      slope_SA_RA_k = 1 ;
      SA_RA_k_u = (slope_SA_RA_k * d) + SA_RA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  SA_RA_wait_cell1 ;
      force_init_update = False;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell1_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_RA!\n");
      exit(1);
    }
    break;
  case ( SA_RA_replay_cell1 ):
    if (True == False) {;}
    else if  (SA_RA_cell1_mode == (2.0)) {
      SA_RA_k_u = 1 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
      cstate =  SA_RA_annhilate ;
      force_init_update = False;
    }
    else if  (SA_RA_k >= (20.0)) {
      SA_RA_from_cell_u = 2 ;
      SA_RA_cell2_replay_latch_u = 1 ;
      SA_RA_k_u = 1 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
      cstate =  SA_RA_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) SA_RA_k_init = SA_RA_k ;
      slope_SA_RA_k = 1 ;
      SA_RA_k_u = (slope_SA_RA_k * d) + SA_RA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  SA_RA_replay_cell1 ;
      force_init_update = False;
      SA_RA_cell1_replay_latch_u = 1 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell1_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_RA!\n");
      exit(1);
    }
    break;
  case ( SA_RA_replay_cell2 ):
    if (True == False) {;}
    else if  (SA_RA_k >= (10.0)) {
      SA_RA_k_u = 1 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
      cstate =  SA_RA_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) SA_RA_k_init = SA_RA_k ;
      slope_SA_RA_k = 1 ;
      SA_RA_k_u = (slope_SA_RA_k * d) + SA_RA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  SA_RA_replay_cell2 ;
      force_init_update = False;
      SA_RA_cell2_replay_latch_u = 1 ;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell1_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_RA!\n");
      exit(1);
    }
    break;
  case ( SA_RA_wait_cell2 ):
    if (True == False) {;}
    else if  (SA_RA_k >= (10.0)) {
      SA_RA_k_u = 1 ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
      cstate =  SA_RA_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) SA_RA_k_init = SA_RA_k ;
      slope_SA_RA_k = 1 ;
      SA_RA_k_u = (slope_SA_RA_k * d) + SA_RA_k ;
      /* Possible Saturation */
      
      
      
      cstate =  SA_RA_wait_cell2 ;
      force_init_update = False;
      SA_RA_cell1_v_delayed_u = SA_RA_update_c1vd () ;
      SA_RA_cell2_v_delayed_u = SA_RA_update_c2vd () ;
      SA_RA_cell1_mode_delayed_u = SA_RA_update_c1md () ;
      SA_RA_cell2_mode_delayed_u = SA_RA_update_c2md () ;
      SA_RA_wasted_u = SA_RA_update_buffer_index (SA_RA_cell1_v,SA_RA_cell2_v,SA_RA_cell1_mode,SA_RA_cell2_mode) ;
      SA_RA_cell1_replay_latch_u = SA_RA_update_latch1 (SA_RA_cell1_mode_delayed,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_replay_latch_u = SA_RA_update_latch2 (SA_RA_cell2_mode_delayed,SA_RA_cell2_replay_latch_u) ;
      SA_RA_cell1_v_replay = SA_RA_update_ocell1 (SA_RA_cell1_v_delayed_u,SA_RA_cell1_replay_latch_u) ;
      SA_RA_cell2_v_replay = SA_RA_update_ocell2 (SA_RA_cell2_v_delayed_u,SA_RA_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_RA!\n");
      exit(1);
    }
    break;
  }
  SA_RA_k = SA_RA_k_u;
  SA_RA_cell1_mode_delayed = SA_RA_cell1_mode_delayed_u;
  SA_RA_cell2_mode_delayed = SA_RA_cell2_mode_delayed_u;
  SA_RA_from_cell = SA_RA_from_cell_u;
  SA_RA_cell1_replay_latch = SA_RA_cell1_replay_latch_u;
  SA_RA_cell2_replay_latch = SA_RA_cell2_replay_latch_u;
  SA_RA_cell1_v_delayed = SA_RA_cell1_v_delayed_u;
  SA_RA_cell2_v_delayed = SA_RA_cell2_v_delayed_u;
  SA_RA_wasted = SA_RA_wasted_u;
  return cstate;
}