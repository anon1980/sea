#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double RightBundleBranch1_v_i_0;
double RightBundleBranch1_v_i_1;
double RightBundleBranch1_v_i_2;
double RightBundleBranch1_voo = 0.0;
double RightBundleBranch1_state = 0.0;


static double  RightBundleBranch1_vx  =  0 ,  RightBundleBranch1_vy  =  0 ,  RightBundleBranch1_vz  =  0 ,  RightBundleBranch1_g  =  0 ,  RightBundleBranch1_v  =  0 ,  RightBundleBranch1_ft  =  0 ,  RightBundleBranch1_theta  =  0 ,  RightBundleBranch1_v_O  =  0 ; //the continuous vars
static double  RightBundleBranch1_vx_u , RightBundleBranch1_vy_u , RightBundleBranch1_vz_u , RightBundleBranch1_g_u , RightBundleBranch1_v_u , RightBundleBranch1_ft_u , RightBundleBranch1_theta_u , RightBundleBranch1_v_O_u ; // and their updates
static double  RightBundleBranch1_vx_init , RightBundleBranch1_vy_init , RightBundleBranch1_vz_init , RightBundleBranch1_g_init , RightBundleBranch1_v_init , RightBundleBranch1_ft_init , RightBundleBranch1_theta_init , RightBundleBranch1_v_O_init ; // and their inits
static double  slope_RightBundleBranch1_vx , slope_RightBundleBranch1_vy , slope_RightBundleBranch1_vz , slope_RightBundleBranch1_g , slope_RightBundleBranch1_v , slope_RightBundleBranch1_ft , slope_RightBundleBranch1_theta , slope_RightBundleBranch1_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { RightBundleBranch1_t1 , RightBundleBranch1_t2 , RightBundleBranch1_t3 , RightBundleBranch1_t4 }; // state declarations

enum states RightBundleBranch1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( RightBundleBranch1_t1 ):
    if (True == False) {;}
    else if  (RightBundleBranch1_g > (44.5)) {
      RightBundleBranch1_vx_u = (0.3 * RightBundleBranch1_v) ;
      RightBundleBranch1_vy_u = 0 ;
      RightBundleBranch1_vz_u = (0.7 * RightBundleBranch1_v) ;
      RightBundleBranch1_g_u = ((((((((RightBundleBranch1_v_i_0 + (- ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightBundleBranch1_v_i_1 + (- ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightBundleBranch1_theta_u = (RightBundleBranch1_v / 30.0) ;
      RightBundleBranch1_v_O_u = (131.1 + (- (80.1 * pow ( ((RightBundleBranch1_v / 30.0)) , (0.5) )))) ;
      RightBundleBranch1_ft_u = f (RightBundleBranch1_theta,4.0e-2) ;
      cstate =  RightBundleBranch1_t2 ;
      force_init_update = False;
    }

    else if ( RightBundleBranch1_v <= (44.5)
               && 
              RightBundleBranch1_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) RightBundleBranch1_vx_init = RightBundleBranch1_vx ;
      slope_RightBundleBranch1_vx = (RightBundleBranch1_vx * -8.7) ;
      RightBundleBranch1_vx_u = (slope_RightBundleBranch1_vx * d) + RightBundleBranch1_vx ;
      if ((pstate != cstate) || force_init_update) RightBundleBranch1_vy_init = RightBundleBranch1_vy ;
      slope_RightBundleBranch1_vy = (RightBundleBranch1_vy * -190.9) ;
      RightBundleBranch1_vy_u = (slope_RightBundleBranch1_vy * d) + RightBundleBranch1_vy ;
      if ((pstate != cstate) || force_init_update) RightBundleBranch1_vz_init = RightBundleBranch1_vz ;
      slope_RightBundleBranch1_vz = (RightBundleBranch1_vz * -190.4) ;
      RightBundleBranch1_vz_u = (slope_RightBundleBranch1_vz * d) + RightBundleBranch1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightBundleBranch1_t1 ;
      force_init_update = False;
      RightBundleBranch1_g_u = ((((((((RightBundleBranch1_v_i_0 + (- ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightBundleBranch1_v_i_1 + (- ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightBundleBranch1_v_u = ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz) ;
      RightBundleBranch1_voo = ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz) ;
      RightBundleBranch1_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightBundleBranch1!\n");
      exit(1);
    }
    break;
  case ( RightBundleBranch1_t2 ):
    if (True == False) {;}
    else if  (RightBundleBranch1_v >= (44.5)) {
      RightBundleBranch1_vx_u = RightBundleBranch1_vx ;
      RightBundleBranch1_vy_u = RightBundleBranch1_vy ;
      RightBundleBranch1_vz_u = RightBundleBranch1_vz ;
      RightBundleBranch1_g_u = ((((((((RightBundleBranch1_v_i_0 + (- ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightBundleBranch1_v_i_1 + (- ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  RightBundleBranch1_t3 ;
      force_init_update = False;
    }
    else if  (RightBundleBranch1_g <= (44.5)
               && 
              RightBundleBranch1_v < (44.5)) {
      RightBundleBranch1_vx_u = RightBundleBranch1_vx ;
      RightBundleBranch1_vy_u = RightBundleBranch1_vy ;
      RightBundleBranch1_vz_u = RightBundleBranch1_vz ;
      RightBundleBranch1_g_u = ((((((((RightBundleBranch1_v_i_0 + (- ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightBundleBranch1_v_i_1 + (- ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  RightBundleBranch1_t1 ;
      force_init_update = False;
    }

    else if ( RightBundleBranch1_v < (44.5)
               && 
              RightBundleBranch1_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) RightBundleBranch1_vx_init = RightBundleBranch1_vx ;
      slope_RightBundleBranch1_vx = ((RightBundleBranch1_vx * -23.6) + (777200.0 * RightBundleBranch1_g)) ;
      RightBundleBranch1_vx_u = (slope_RightBundleBranch1_vx * d) + RightBundleBranch1_vx ;
      if ((pstate != cstate) || force_init_update) RightBundleBranch1_vy_init = RightBundleBranch1_vy ;
      slope_RightBundleBranch1_vy = ((RightBundleBranch1_vy * -45.5) + (58900.0 * RightBundleBranch1_g)) ;
      RightBundleBranch1_vy_u = (slope_RightBundleBranch1_vy * d) + RightBundleBranch1_vy ;
      if ((pstate != cstate) || force_init_update) RightBundleBranch1_vz_init = RightBundleBranch1_vz ;
      slope_RightBundleBranch1_vz = ((RightBundleBranch1_vz * -12.9) + (276600.0 * RightBundleBranch1_g)) ;
      RightBundleBranch1_vz_u = (slope_RightBundleBranch1_vz * d) + RightBundleBranch1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightBundleBranch1_t2 ;
      force_init_update = False;
      RightBundleBranch1_g_u = ((((((((RightBundleBranch1_v_i_0 + (- ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightBundleBranch1_v_i_1 + (- ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightBundleBranch1_v_u = ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz) ;
      RightBundleBranch1_voo = ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz) ;
      RightBundleBranch1_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightBundleBranch1!\n");
      exit(1);
    }
    break;
  case ( RightBundleBranch1_t3 ):
    if (True == False) {;}
    else if  (RightBundleBranch1_v >= (131.1)) {
      RightBundleBranch1_vx_u = RightBundleBranch1_vx ;
      RightBundleBranch1_vy_u = RightBundleBranch1_vy ;
      RightBundleBranch1_vz_u = RightBundleBranch1_vz ;
      RightBundleBranch1_g_u = ((((((((RightBundleBranch1_v_i_0 + (- ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightBundleBranch1_v_i_1 + (- ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  RightBundleBranch1_t4 ;
      force_init_update = False;
    }

    else if ( RightBundleBranch1_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) RightBundleBranch1_vx_init = RightBundleBranch1_vx ;
      slope_RightBundleBranch1_vx = (RightBundleBranch1_vx * -6.9) ;
      RightBundleBranch1_vx_u = (slope_RightBundleBranch1_vx * d) + RightBundleBranch1_vx ;
      if ((pstate != cstate) || force_init_update) RightBundleBranch1_vy_init = RightBundleBranch1_vy ;
      slope_RightBundleBranch1_vy = (RightBundleBranch1_vy * 75.9) ;
      RightBundleBranch1_vy_u = (slope_RightBundleBranch1_vy * d) + RightBundleBranch1_vy ;
      if ((pstate != cstate) || force_init_update) RightBundleBranch1_vz_init = RightBundleBranch1_vz ;
      slope_RightBundleBranch1_vz = (RightBundleBranch1_vz * 6826.5) ;
      RightBundleBranch1_vz_u = (slope_RightBundleBranch1_vz * d) + RightBundleBranch1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightBundleBranch1_t3 ;
      force_init_update = False;
      RightBundleBranch1_g_u = ((((((((RightBundleBranch1_v_i_0 + (- ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightBundleBranch1_v_i_1 + (- ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightBundleBranch1_v_u = ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz) ;
      RightBundleBranch1_voo = ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz) ;
      RightBundleBranch1_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightBundleBranch1!\n");
      exit(1);
    }
    break;
  case ( RightBundleBranch1_t4 ):
    if (True == False) {;}
    else if  (RightBundleBranch1_v <= (30.0)) {
      RightBundleBranch1_vx_u = RightBundleBranch1_vx ;
      RightBundleBranch1_vy_u = RightBundleBranch1_vy ;
      RightBundleBranch1_vz_u = RightBundleBranch1_vz ;
      RightBundleBranch1_g_u = ((((((((RightBundleBranch1_v_i_0 + (- ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightBundleBranch1_v_i_1 + (- ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  RightBundleBranch1_t1 ;
      force_init_update = False;
    }

    else if ( RightBundleBranch1_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) RightBundleBranch1_vx_init = RightBundleBranch1_vx ;
      slope_RightBundleBranch1_vx = (RightBundleBranch1_vx * -33.2) ;
      RightBundleBranch1_vx_u = (slope_RightBundleBranch1_vx * d) + RightBundleBranch1_vx ;
      if ((pstate != cstate) || force_init_update) RightBundleBranch1_vy_init = RightBundleBranch1_vy ;
      slope_RightBundleBranch1_vy = ((RightBundleBranch1_vy * 11.0) * RightBundleBranch1_ft) ;
      RightBundleBranch1_vy_u = (slope_RightBundleBranch1_vy * d) + RightBundleBranch1_vy ;
      if ((pstate != cstate) || force_init_update) RightBundleBranch1_vz_init = RightBundleBranch1_vz ;
      slope_RightBundleBranch1_vz = ((RightBundleBranch1_vz * 2.0) * RightBundleBranch1_ft) ;
      RightBundleBranch1_vz_u = (slope_RightBundleBranch1_vz * d) + RightBundleBranch1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightBundleBranch1_t4 ;
      force_init_update = False;
      RightBundleBranch1_g_u = ((((((((RightBundleBranch1_v_i_0 + (- ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightBundleBranch1_v_i_1 + (- ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightBundleBranch1_v_u = ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz) ;
      RightBundleBranch1_voo = ((RightBundleBranch1_vx + (- RightBundleBranch1_vy)) + RightBundleBranch1_vz) ;
      RightBundleBranch1_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightBundleBranch1!\n");
      exit(1);
    }
    break;
  }
  RightBundleBranch1_vx = RightBundleBranch1_vx_u;
  RightBundleBranch1_vy = RightBundleBranch1_vy_u;
  RightBundleBranch1_vz = RightBundleBranch1_vz_u;
  RightBundleBranch1_g = RightBundleBranch1_g_u;
  RightBundleBranch1_v = RightBundleBranch1_v_u;
  RightBundleBranch1_ft = RightBundleBranch1_ft_u;
  RightBundleBranch1_theta = RightBundleBranch1_theta_u;
  RightBundleBranch1_v_O = RightBundleBranch1_v_O_u;
  return cstate;
}