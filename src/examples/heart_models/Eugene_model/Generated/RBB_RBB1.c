#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double RBB_RBB1_update_c1vd();
extern double RBB_RBB1_update_c2vd();
extern double RBB_RBB1_update_c1md();
extern double RBB_RBB1_update_c2md();
extern double RBB_RBB1_update_buffer_index(double,double,double,double);
extern double RBB_RBB1_update_latch1(double,double);
extern double RBB_RBB1_update_latch2(double,double);
extern double RBB_RBB1_update_ocell1(double,double);
extern double RBB_RBB1_update_ocell2(double,double);
double RBB_RBB1_cell1_v;
double RBB_RBB1_cell1_mode;
double RBB_RBB1_cell2_v;
double RBB_RBB1_cell2_mode;
double RBB_RBB1_cell1_v_replay = 0.0;
double RBB_RBB1_cell2_v_replay = 0.0;


static double  RBB_RBB1_k  =  0.0 ,  RBB_RBB1_cell1_mode_delayed  =  0.0 ,  RBB_RBB1_cell2_mode_delayed  =  0.0 ,  RBB_RBB1_from_cell  =  0.0 ,  RBB_RBB1_cell1_replay_latch  =  0.0 ,  RBB_RBB1_cell2_replay_latch  =  0.0 ,  RBB_RBB1_cell1_v_delayed  =  0.0 ,  RBB_RBB1_cell2_v_delayed  =  0.0 ,  RBB_RBB1_wasted  =  0.0 ; //the continuous vars
static double  RBB_RBB1_k_u , RBB_RBB1_cell1_mode_delayed_u , RBB_RBB1_cell2_mode_delayed_u , RBB_RBB1_from_cell_u , RBB_RBB1_cell1_replay_latch_u , RBB_RBB1_cell2_replay_latch_u , RBB_RBB1_cell1_v_delayed_u , RBB_RBB1_cell2_v_delayed_u , RBB_RBB1_wasted_u ; // and their updates
static double  RBB_RBB1_k_init , RBB_RBB1_cell1_mode_delayed_init , RBB_RBB1_cell2_mode_delayed_init , RBB_RBB1_from_cell_init , RBB_RBB1_cell1_replay_latch_init , RBB_RBB1_cell2_replay_latch_init , RBB_RBB1_cell1_v_delayed_init , RBB_RBB1_cell2_v_delayed_init , RBB_RBB1_wasted_init ; // and their inits
static double  slope_RBB_RBB1_k , slope_RBB_RBB1_cell1_mode_delayed , slope_RBB_RBB1_cell2_mode_delayed , slope_RBB_RBB1_from_cell , slope_RBB_RBB1_cell1_replay_latch , slope_RBB_RBB1_cell2_replay_latch , slope_RBB_RBB1_cell1_v_delayed , slope_RBB_RBB1_cell2_v_delayed , slope_RBB_RBB1_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { RBB_RBB1_idle , RBB_RBB1_annhilate , RBB_RBB1_previous_drection1 , RBB_RBB1_previous_direction2 , RBB_RBB1_wait_cell1 , RBB_RBB1_replay_cell1 , RBB_RBB1_replay_cell2 , RBB_RBB1_wait_cell2 }; // state declarations

enum states RBB_RBB1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( RBB_RBB1_idle ):
    if (True == False) {;}
    else if  (RBB_RBB1_cell2_mode == (2.0) && (RBB_RBB1_cell1_mode != (2.0))) {
      RBB_RBB1_k_u = 1 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
      cstate =  RBB_RBB1_previous_direction2 ;
      force_init_update = False;
    }
    else if  (RBB_RBB1_cell1_mode == (2.0) && (RBB_RBB1_cell2_mode != (2.0))) {
      RBB_RBB1_k_u = 1 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
      cstate =  RBB_RBB1_previous_drection1 ;
      force_init_update = False;
    }
    else if  (RBB_RBB1_cell1_mode == (2.0) && (RBB_RBB1_cell2_mode == (2.0))) {
      RBB_RBB1_k_u = 1 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
      cstate =  RBB_RBB1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RBB_RBB1_k_init = RBB_RBB1_k ;
      slope_RBB_RBB1_k = 1 ;
      RBB_RBB1_k_u = (slope_RBB_RBB1_k * d) + RBB_RBB1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RBB_RBB1_idle ;
      force_init_update = False;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell1_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB_RBB1!\n");
      exit(1);
    }
    break;
  case ( RBB_RBB1_annhilate ):
    if (True == False) {;}
    else if  (RBB_RBB1_cell1_mode != (2.0) && (RBB_RBB1_cell2_mode != (2.0))) {
      RBB_RBB1_k_u = 1 ;
      RBB_RBB1_from_cell_u = 0 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
      cstate =  RBB_RBB1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RBB_RBB1_k_init = RBB_RBB1_k ;
      slope_RBB_RBB1_k = 1 ;
      RBB_RBB1_k_u = (slope_RBB_RBB1_k * d) + RBB_RBB1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RBB_RBB1_annhilate ;
      force_init_update = False;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell1_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB_RBB1!\n");
      exit(1);
    }
    break;
  case ( RBB_RBB1_previous_drection1 ):
    if (True == False) {;}
    else if  (RBB_RBB1_from_cell == (1.0)) {
      RBB_RBB1_k_u = 1 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
      cstate =  RBB_RBB1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RBB_RBB1_from_cell == (0.0)) {
      RBB_RBB1_k_u = 1 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
      cstate =  RBB_RBB1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RBB_RBB1_from_cell == (2.0) && (RBB_RBB1_cell2_mode_delayed == (0.0))) {
      RBB_RBB1_k_u = 1 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
      cstate =  RBB_RBB1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RBB_RBB1_from_cell == (2.0) && (RBB_RBB1_cell2_mode_delayed != (0.0))) {
      RBB_RBB1_k_u = 1 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
      cstate =  RBB_RBB1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RBB_RBB1_k_init = RBB_RBB1_k ;
      slope_RBB_RBB1_k = 1 ;
      RBB_RBB1_k_u = (slope_RBB_RBB1_k * d) + RBB_RBB1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RBB_RBB1_previous_drection1 ;
      force_init_update = False;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell1_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB_RBB1!\n");
      exit(1);
    }
    break;
  case ( RBB_RBB1_previous_direction2 ):
    if (True == False) {;}
    else if  (RBB_RBB1_from_cell == (1.0) && (RBB_RBB1_cell1_mode_delayed != (0.0))) {
      RBB_RBB1_k_u = 1 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
      cstate =  RBB_RBB1_annhilate ;
      force_init_update = False;
    }
    else if  (RBB_RBB1_from_cell == (2.0)) {
      RBB_RBB1_k_u = 1 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
      cstate =  RBB_RBB1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (RBB_RBB1_from_cell == (0.0)) {
      RBB_RBB1_k_u = 1 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
      cstate =  RBB_RBB1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (RBB_RBB1_from_cell == (1.0) && (RBB_RBB1_cell1_mode_delayed == (0.0))) {
      RBB_RBB1_k_u = 1 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
      cstate =  RBB_RBB1_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RBB_RBB1_k_init = RBB_RBB1_k ;
      slope_RBB_RBB1_k = 1 ;
      RBB_RBB1_k_u = (slope_RBB_RBB1_k * d) + RBB_RBB1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RBB_RBB1_previous_direction2 ;
      force_init_update = False;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell1_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB_RBB1!\n");
      exit(1);
    }
    break;
  case ( RBB_RBB1_wait_cell1 ):
    if (True == False) {;}
    else if  (RBB_RBB1_cell2_mode == (2.0)) {
      RBB_RBB1_k_u = 1 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
      cstate =  RBB_RBB1_annhilate ;
      force_init_update = False;
    }
    else if  (RBB_RBB1_k >= (5.0)) {
      RBB_RBB1_from_cell_u = 1 ;
      RBB_RBB1_cell1_replay_latch_u = 1 ;
      RBB_RBB1_k_u = 1 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
      cstate =  RBB_RBB1_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RBB_RBB1_k_init = RBB_RBB1_k ;
      slope_RBB_RBB1_k = 1 ;
      RBB_RBB1_k_u = (slope_RBB_RBB1_k * d) + RBB_RBB1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RBB_RBB1_wait_cell1 ;
      force_init_update = False;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell1_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB_RBB1!\n");
      exit(1);
    }
    break;
  case ( RBB_RBB1_replay_cell1 ):
    if (True == False) {;}
    else if  (RBB_RBB1_cell1_mode == (2.0)) {
      RBB_RBB1_k_u = 1 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
      cstate =  RBB_RBB1_annhilate ;
      force_init_update = False;
    }
    else if  (RBB_RBB1_k >= (5.0)) {
      RBB_RBB1_from_cell_u = 2 ;
      RBB_RBB1_cell2_replay_latch_u = 1 ;
      RBB_RBB1_k_u = 1 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
      cstate =  RBB_RBB1_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RBB_RBB1_k_init = RBB_RBB1_k ;
      slope_RBB_RBB1_k = 1 ;
      RBB_RBB1_k_u = (slope_RBB_RBB1_k * d) + RBB_RBB1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RBB_RBB1_replay_cell1 ;
      force_init_update = False;
      RBB_RBB1_cell1_replay_latch_u = 1 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell1_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB_RBB1!\n");
      exit(1);
    }
    break;
  case ( RBB_RBB1_replay_cell2 ):
    if (True == False) {;}
    else if  (RBB_RBB1_k >= (10.0)) {
      RBB_RBB1_k_u = 1 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
      cstate =  RBB_RBB1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RBB_RBB1_k_init = RBB_RBB1_k ;
      slope_RBB_RBB1_k = 1 ;
      RBB_RBB1_k_u = (slope_RBB_RBB1_k * d) + RBB_RBB1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RBB_RBB1_replay_cell2 ;
      force_init_update = False;
      RBB_RBB1_cell2_replay_latch_u = 1 ;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell1_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB_RBB1!\n");
      exit(1);
    }
    break;
  case ( RBB_RBB1_wait_cell2 ):
    if (True == False) {;}
    else if  (RBB_RBB1_k >= (10.0)) {
      RBB_RBB1_k_u = 1 ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
      cstate =  RBB_RBB1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RBB_RBB1_k_init = RBB_RBB1_k ;
      slope_RBB_RBB1_k = 1 ;
      RBB_RBB1_k_u = (slope_RBB_RBB1_k * d) + RBB_RBB1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RBB_RBB1_wait_cell2 ;
      force_init_update = False;
      RBB_RBB1_cell1_v_delayed_u = RBB_RBB1_update_c1vd () ;
      RBB_RBB1_cell2_v_delayed_u = RBB_RBB1_update_c2vd () ;
      RBB_RBB1_cell1_mode_delayed_u = RBB_RBB1_update_c1md () ;
      RBB_RBB1_cell2_mode_delayed_u = RBB_RBB1_update_c2md () ;
      RBB_RBB1_wasted_u = RBB_RBB1_update_buffer_index (RBB_RBB1_cell1_v,RBB_RBB1_cell2_v,RBB_RBB1_cell1_mode,RBB_RBB1_cell2_mode) ;
      RBB_RBB1_cell1_replay_latch_u = RBB_RBB1_update_latch1 (RBB_RBB1_cell1_mode_delayed,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_replay_latch_u = RBB_RBB1_update_latch2 (RBB_RBB1_cell2_mode_delayed,RBB_RBB1_cell2_replay_latch_u) ;
      RBB_RBB1_cell1_v_replay = RBB_RBB1_update_ocell1 (RBB_RBB1_cell1_v_delayed_u,RBB_RBB1_cell1_replay_latch_u) ;
      RBB_RBB1_cell2_v_replay = RBB_RBB1_update_ocell2 (RBB_RBB1_cell2_v_delayed_u,RBB_RBB1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB_RBB1!\n");
      exit(1);
    }
    break;
  }
  RBB_RBB1_k = RBB_RBB1_k_u;
  RBB_RBB1_cell1_mode_delayed = RBB_RBB1_cell1_mode_delayed_u;
  RBB_RBB1_cell2_mode_delayed = RBB_RBB1_cell2_mode_delayed_u;
  RBB_RBB1_from_cell = RBB_RBB1_from_cell_u;
  RBB_RBB1_cell1_replay_latch = RBB_RBB1_cell1_replay_latch_u;
  RBB_RBB1_cell2_replay_latch = RBB_RBB1_cell2_replay_latch_u;
  RBB_RBB1_cell1_v_delayed = RBB_RBB1_cell1_v_delayed_u;
  RBB_RBB1_cell2_v_delayed = RBB_RBB1_cell2_v_delayed_u;
  RBB_RBB1_wasted = RBB_RBB1_wasted_u;
  return cstate;
}