#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double OS_Slow_update_c1vd();
extern double OS_Slow_update_c2vd();
extern double OS_Slow_update_c1md();
extern double OS_Slow_update_c2md();
extern double OS_Slow_update_buffer_index(double,double,double,double);
extern double OS_Slow_update_latch1(double,double);
extern double OS_Slow_update_latch2(double,double);
extern double OS_Slow_update_ocell1(double,double);
extern double OS_Slow_update_ocell2(double,double);
double OS_Slow_cell1_v;
double OS_Slow_cell1_mode;
double OS_Slow_cell2_v;
double OS_Slow_cell2_mode;
double OS_Slow_cell1_v_replay = 0.0;
double OS_Slow_cell2_v_replay = 0.0;


static double  OS_Slow_k  =  0.0 ,  OS_Slow_cell1_mode_delayed  =  0.0 ,  OS_Slow_cell2_mode_delayed  =  0.0 ,  OS_Slow_from_cell  =  0.0 ,  OS_Slow_cell1_replay_latch  =  0.0 ,  OS_Slow_cell2_replay_latch  =  0.0 ,  OS_Slow_cell1_v_delayed  =  0.0 ,  OS_Slow_cell2_v_delayed  =  0.0 ,  OS_Slow_wasted  =  0.0 ; //the continuous vars
static double  OS_Slow_k_u , OS_Slow_cell1_mode_delayed_u , OS_Slow_cell2_mode_delayed_u , OS_Slow_from_cell_u , OS_Slow_cell1_replay_latch_u , OS_Slow_cell2_replay_latch_u , OS_Slow_cell1_v_delayed_u , OS_Slow_cell2_v_delayed_u , OS_Slow_wasted_u ; // and their updates
static double  OS_Slow_k_init , OS_Slow_cell1_mode_delayed_init , OS_Slow_cell2_mode_delayed_init , OS_Slow_from_cell_init , OS_Slow_cell1_replay_latch_init , OS_Slow_cell2_replay_latch_init , OS_Slow_cell1_v_delayed_init , OS_Slow_cell2_v_delayed_init , OS_Slow_wasted_init ; // and their inits
static double  slope_OS_Slow_k , slope_OS_Slow_cell1_mode_delayed , slope_OS_Slow_cell2_mode_delayed , slope_OS_Slow_from_cell , slope_OS_Slow_cell1_replay_latch , slope_OS_Slow_cell2_replay_latch , slope_OS_Slow_cell1_v_delayed , slope_OS_Slow_cell2_v_delayed , slope_OS_Slow_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { OS_Slow_idle , OS_Slow_annhilate , OS_Slow_previous_drection1 , OS_Slow_previous_direction2 , OS_Slow_wait_cell1 , OS_Slow_replay_cell1 , OS_Slow_replay_cell2 , OS_Slow_wait_cell2 }; // state declarations

enum states OS_Slow (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( OS_Slow_idle ):
    if (True == False) {;}
    else if  (OS_Slow_cell2_mode == (2.0) && (OS_Slow_cell1_mode != (2.0))) {
      OS_Slow_k_u = 1 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
      cstate =  OS_Slow_previous_direction2 ;
      force_init_update = False;
    }
    else if  (OS_Slow_cell1_mode == (2.0) && (OS_Slow_cell2_mode != (2.0))) {
      OS_Slow_k_u = 1 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
      cstate =  OS_Slow_previous_drection1 ;
      force_init_update = False;
    }
    else if  (OS_Slow_cell1_mode == (2.0) && (OS_Slow_cell2_mode == (2.0))) {
      OS_Slow_k_u = 1 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
      cstate =  OS_Slow_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) OS_Slow_k_init = OS_Slow_k ;
      slope_OS_Slow_k = 1 ;
      OS_Slow_k_u = (slope_OS_Slow_k * d) + OS_Slow_k ;
      /* Possible Saturation */
      
      
      
      cstate =  OS_Slow_idle ;
      force_init_update = False;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell1_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Slow!\n");
      exit(1);
    }
    break;
  case ( OS_Slow_annhilate ):
    if (True == False) {;}
    else if  (OS_Slow_cell1_mode != (2.0) && (OS_Slow_cell2_mode != (2.0))) {
      OS_Slow_k_u = 1 ;
      OS_Slow_from_cell_u = 0 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
      cstate =  OS_Slow_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) OS_Slow_k_init = OS_Slow_k ;
      slope_OS_Slow_k = 1 ;
      OS_Slow_k_u = (slope_OS_Slow_k * d) + OS_Slow_k ;
      /* Possible Saturation */
      
      
      
      cstate =  OS_Slow_annhilate ;
      force_init_update = False;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell1_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Slow!\n");
      exit(1);
    }
    break;
  case ( OS_Slow_previous_drection1 ):
    if (True == False) {;}
    else if  (OS_Slow_from_cell == (1.0)) {
      OS_Slow_k_u = 1 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
      cstate =  OS_Slow_wait_cell1 ;
      force_init_update = False;
    }
    else if  (OS_Slow_from_cell == (0.0)) {
      OS_Slow_k_u = 1 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
      cstate =  OS_Slow_wait_cell1 ;
      force_init_update = False;
    }
    else if  (OS_Slow_from_cell == (2.0) && (OS_Slow_cell2_mode_delayed == (0.0))) {
      OS_Slow_k_u = 1 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
      cstate =  OS_Slow_wait_cell1 ;
      force_init_update = False;
    }
    else if  (OS_Slow_from_cell == (2.0) && (OS_Slow_cell2_mode_delayed != (0.0))) {
      OS_Slow_k_u = 1 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
      cstate =  OS_Slow_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) OS_Slow_k_init = OS_Slow_k ;
      slope_OS_Slow_k = 1 ;
      OS_Slow_k_u = (slope_OS_Slow_k * d) + OS_Slow_k ;
      /* Possible Saturation */
      
      
      
      cstate =  OS_Slow_previous_drection1 ;
      force_init_update = False;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell1_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Slow!\n");
      exit(1);
    }
    break;
  case ( OS_Slow_previous_direction2 ):
    if (True == False) {;}
    else if  (OS_Slow_from_cell == (1.0) && (OS_Slow_cell1_mode_delayed != (0.0))) {
      OS_Slow_k_u = 1 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
      cstate =  OS_Slow_annhilate ;
      force_init_update = False;
    }
    else if  (OS_Slow_from_cell == (2.0)) {
      OS_Slow_k_u = 1 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
      cstate =  OS_Slow_replay_cell1 ;
      force_init_update = False;
    }
    else if  (OS_Slow_from_cell == (0.0)) {
      OS_Slow_k_u = 1 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
      cstate =  OS_Slow_replay_cell1 ;
      force_init_update = False;
    }
    else if  (OS_Slow_from_cell == (1.0) && (OS_Slow_cell1_mode_delayed == (0.0))) {
      OS_Slow_k_u = 1 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
      cstate =  OS_Slow_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) OS_Slow_k_init = OS_Slow_k ;
      slope_OS_Slow_k = 1 ;
      OS_Slow_k_u = (slope_OS_Slow_k * d) + OS_Slow_k ;
      /* Possible Saturation */
      
      
      
      cstate =  OS_Slow_previous_direction2 ;
      force_init_update = False;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell1_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Slow!\n");
      exit(1);
    }
    break;
  case ( OS_Slow_wait_cell1 ):
    if (True == False) {;}
    else if  (OS_Slow_cell2_mode == (2.0)) {
      OS_Slow_k_u = 1 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
      cstate =  OS_Slow_annhilate ;
      force_init_update = False;
    }
    else if  (OS_Slow_k >= (30.0)) {
      OS_Slow_from_cell_u = 1 ;
      OS_Slow_cell1_replay_latch_u = 1 ;
      OS_Slow_k_u = 1 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
      cstate =  OS_Slow_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) OS_Slow_k_init = OS_Slow_k ;
      slope_OS_Slow_k = 1 ;
      OS_Slow_k_u = (slope_OS_Slow_k * d) + OS_Slow_k ;
      /* Possible Saturation */
      
      
      
      cstate =  OS_Slow_wait_cell1 ;
      force_init_update = False;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell1_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Slow!\n");
      exit(1);
    }
    break;
  case ( OS_Slow_replay_cell1 ):
    if (True == False) {;}
    else if  (OS_Slow_cell1_mode == (2.0)) {
      OS_Slow_k_u = 1 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
      cstate =  OS_Slow_annhilate ;
      force_init_update = False;
    }
    else if  (OS_Slow_k >= (30.0)) {
      OS_Slow_from_cell_u = 2 ;
      OS_Slow_cell2_replay_latch_u = 1 ;
      OS_Slow_k_u = 1 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
      cstate =  OS_Slow_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) OS_Slow_k_init = OS_Slow_k ;
      slope_OS_Slow_k = 1 ;
      OS_Slow_k_u = (slope_OS_Slow_k * d) + OS_Slow_k ;
      /* Possible Saturation */
      
      
      
      cstate =  OS_Slow_replay_cell1 ;
      force_init_update = False;
      OS_Slow_cell1_replay_latch_u = 1 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell1_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Slow!\n");
      exit(1);
    }
    break;
  case ( OS_Slow_replay_cell2 ):
    if (True == False) {;}
    else if  (OS_Slow_k >= (10.0)) {
      OS_Slow_k_u = 1 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
      cstate =  OS_Slow_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) OS_Slow_k_init = OS_Slow_k ;
      slope_OS_Slow_k = 1 ;
      OS_Slow_k_u = (slope_OS_Slow_k * d) + OS_Slow_k ;
      /* Possible Saturation */
      
      
      
      cstate =  OS_Slow_replay_cell2 ;
      force_init_update = False;
      OS_Slow_cell2_replay_latch_u = 1 ;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell1_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Slow!\n");
      exit(1);
    }
    break;
  case ( OS_Slow_wait_cell2 ):
    if (True == False) {;}
    else if  (OS_Slow_k >= (10.0)) {
      OS_Slow_k_u = 1 ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
      cstate =  OS_Slow_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) OS_Slow_k_init = OS_Slow_k ;
      slope_OS_Slow_k = 1 ;
      OS_Slow_k_u = (slope_OS_Slow_k * d) + OS_Slow_k ;
      /* Possible Saturation */
      
      
      
      cstate =  OS_Slow_wait_cell2 ;
      force_init_update = False;
      OS_Slow_cell1_v_delayed_u = OS_Slow_update_c1vd () ;
      OS_Slow_cell2_v_delayed_u = OS_Slow_update_c2vd () ;
      OS_Slow_cell1_mode_delayed_u = OS_Slow_update_c1md () ;
      OS_Slow_cell2_mode_delayed_u = OS_Slow_update_c2md () ;
      OS_Slow_wasted_u = OS_Slow_update_buffer_index (OS_Slow_cell1_v,OS_Slow_cell2_v,OS_Slow_cell1_mode,OS_Slow_cell2_mode) ;
      OS_Slow_cell1_replay_latch_u = OS_Slow_update_latch1 (OS_Slow_cell1_mode_delayed,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_replay_latch_u = OS_Slow_update_latch2 (OS_Slow_cell2_mode_delayed,OS_Slow_cell2_replay_latch_u) ;
      OS_Slow_cell1_v_replay = OS_Slow_update_ocell1 (OS_Slow_cell1_v_delayed_u,OS_Slow_cell1_replay_latch_u) ;
      OS_Slow_cell2_v_replay = OS_Slow_update_ocell2 (OS_Slow_cell2_v_delayed_u,OS_Slow_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Slow!\n");
      exit(1);
    }
    break;
  }
  OS_Slow_k = OS_Slow_k_u;
  OS_Slow_cell1_mode_delayed = OS_Slow_cell1_mode_delayed_u;
  OS_Slow_cell2_mode_delayed = OS_Slow_cell2_mode_delayed_u;
  OS_Slow_from_cell = OS_Slow_from_cell_u;
  OS_Slow_cell1_replay_latch = OS_Slow_cell1_replay_latch_u;
  OS_Slow_cell2_replay_latch = OS_Slow_cell2_replay_latch_u;
  OS_Slow_cell1_v_delayed = OS_Slow_cell1_v_delayed_u;
  OS_Slow_cell2_v_delayed = OS_Slow_cell2_v_delayed_u;
  OS_Slow_wasted = OS_Slow_wasted_u;
  return cstate;
}