#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double RVA_RV_update_c1vd();
extern double RVA_RV_update_c2vd();
extern double RVA_RV_update_c1md();
extern double RVA_RV_update_c2md();
extern double RVA_RV_update_buffer_index(double,double,double,double);
extern double RVA_RV_update_latch1(double,double);
extern double RVA_RV_update_latch2(double,double);
extern double RVA_RV_update_ocell1(double,double);
extern double RVA_RV_update_ocell2(double,double);
double RVA_RV_cell1_v;
double RVA_RV_cell1_mode;
double RVA_RV_cell2_v;
double RVA_RV_cell2_mode;
double RVA_RV_cell1_v_replay = 0.0;
double RVA_RV_cell2_v_replay = 0.0;


static double  RVA_RV_k  =  0.0 ,  RVA_RV_cell1_mode_delayed  =  0.0 ,  RVA_RV_cell2_mode_delayed  =  0.0 ,  RVA_RV_from_cell  =  0.0 ,  RVA_RV_cell1_replay_latch  =  0.0 ,  RVA_RV_cell2_replay_latch  =  0.0 ,  RVA_RV_cell1_v_delayed  =  0.0 ,  RVA_RV_cell2_v_delayed  =  0.0 ,  RVA_RV_wasted  =  0.0 ; //the continuous vars
static double  RVA_RV_k_u , RVA_RV_cell1_mode_delayed_u , RVA_RV_cell2_mode_delayed_u , RVA_RV_from_cell_u , RVA_RV_cell1_replay_latch_u , RVA_RV_cell2_replay_latch_u , RVA_RV_cell1_v_delayed_u , RVA_RV_cell2_v_delayed_u , RVA_RV_wasted_u ; // and their updates
static double  RVA_RV_k_init , RVA_RV_cell1_mode_delayed_init , RVA_RV_cell2_mode_delayed_init , RVA_RV_from_cell_init , RVA_RV_cell1_replay_latch_init , RVA_RV_cell2_replay_latch_init , RVA_RV_cell1_v_delayed_init , RVA_RV_cell2_v_delayed_init , RVA_RV_wasted_init ; // and their inits
static double  slope_RVA_RV_k , slope_RVA_RV_cell1_mode_delayed , slope_RVA_RV_cell2_mode_delayed , slope_RVA_RV_from_cell , slope_RVA_RV_cell1_replay_latch , slope_RVA_RV_cell2_replay_latch , slope_RVA_RV_cell1_v_delayed , slope_RVA_RV_cell2_v_delayed , slope_RVA_RV_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { RVA_RV_idle , RVA_RV_annhilate , RVA_RV_previous_drection1 , RVA_RV_previous_direction2 , RVA_RV_wait_cell1 , RVA_RV_replay_cell1 , RVA_RV_replay_cell2 , RVA_RV_wait_cell2 }; // state declarations

enum states RVA_RV (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( RVA_RV_idle ):
    if (True == False) {;}
    else if  (RVA_RV_cell2_mode == (2.0) && (RVA_RV_cell1_mode != (2.0))) {
      RVA_RV_k_u = 1 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
      cstate =  RVA_RV_previous_direction2 ;
      force_init_update = False;
    }
    else if  (RVA_RV_cell1_mode == (2.0) && (RVA_RV_cell2_mode != (2.0))) {
      RVA_RV_k_u = 1 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
      cstate =  RVA_RV_previous_drection1 ;
      force_init_update = False;
    }
    else if  (RVA_RV_cell1_mode == (2.0) && (RVA_RV_cell2_mode == (2.0))) {
      RVA_RV_k_u = 1 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
      cstate =  RVA_RV_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVA_RV_k_init = RVA_RV_k ;
      slope_RVA_RV_k = 1 ;
      RVA_RV_k_u = (slope_RVA_RV_k * d) + RVA_RV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVA_RV_idle ;
      force_init_update = False;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell1_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RV!\n");
      exit(1);
    }
    break;
  case ( RVA_RV_annhilate ):
    if (True == False) {;}
    else if  (RVA_RV_cell1_mode != (2.0) && (RVA_RV_cell2_mode != (2.0))) {
      RVA_RV_k_u = 1 ;
      RVA_RV_from_cell_u = 0 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
      cstate =  RVA_RV_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVA_RV_k_init = RVA_RV_k ;
      slope_RVA_RV_k = 1 ;
      RVA_RV_k_u = (slope_RVA_RV_k * d) + RVA_RV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVA_RV_annhilate ;
      force_init_update = False;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell1_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RV!\n");
      exit(1);
    }
    break;
  case ( RVA_RV_previous_drection1 ):
    if (True == False) {;}
    else if  (RVA_RV_from_cell == (1.0)) {
      RVA_RV_k_u = 1 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
      cstate =  RVA_RV_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RVA_RV_from_cell == (0.0)) {
      RVA_RV_k_u = 1 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
      cstate =  RVA_RV_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RVA_RV_from_cell == (2.0) && (RVA_RV_cell2_mode_delayed == (0.0))) {
      RVA_RV_k_u = 1 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
      cstate =  RVA_RV_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RVA_RV_from_cell == (2.0) && (RVA_RV_cell2_mode_delayed != (0.0))) {
      RVA_RV_k_u = 1 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
      cstate =  RVA_RV_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVA_RV_k_init = RVA_RV_k ;
      slope_RVA_RV_k = 1 ;
      RVA_RV_k_u = (slope_RVA_RV_k * d) + RVA_RV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVA_RV_previous_drection1 ;
      force_init_update = False;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell1_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RV!\n");
      exit(1);
    }
    break;
  case ( RVA_RV_previous_direction2 ):
    if (True == False) {;}
    else if  (RVA_RV_from_cell == (1.0) && (RVA_RV_cell1_mode_delayed != (0.0))) {
      RVA_RV_k_u = 1 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
      cstate =  RVA_RV_annhilate ;
      force_init_update = False;
    }
    else if  (RVA_RV_from_cell == (2.0)) {
      RVA_RV_k_u = 1 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
      cstate =  RVA_RV_replay_cell1 ;
      force_init_update = False;
    }
    else if  (RVA_RV_from_cell == (0.0)) {
      RVA_RV_k_u = 1 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
      cstate =  RVA_RV_replay_cell1 ;
      force_init_update = False;
    }
    else if  (RVA_RV_from_cell == (1.0) && (RVA_RV_cell1_mode_delayed == (0.0))) {
      RVA_RV_k_u = 1 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
      cstate =  RVA_RV_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVA_RV_k_init = RVA_RV_k ;
      slope_RVA_RV_k = 1 ;
      RVA_RV_k_u = (slope_RVA_RV_k * d) + RVA_RV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVA_RV_previous_direction2 ;
      force_init_update = False;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell1_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RV!\n");
      exit(1);
    }
    break;
  case ( RVA_RV_wait_cell1 ):
    if (True == False) {;}
    else if  (RVA_RV_cell2_mode == (2.0)) {
      RVA_RV_k_u = 1 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
      cstate =  RVA_RV_annhilate ;
      force_init_update = False;
    }
    else if  (RVA_RV_k >= (10.0)) {
      RVA_RV_from_cell_u = 1 ;
      RVA_RV_cell1_replay_latch_u = 1 ;
      RVA_RV_k_u = 1 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
      cstate =  RVA_RV_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVA_RV_k_init = RVA_RV_k ;
      slope_RVA_RV_k = 1 ;
      RVA_RV_k_u = (slope_RVA_RV_k * d) + RVA_RV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVA_RV_wait_cell1 ;
      force_init_update = False;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell1_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RV!\n");
      exit(1);
    }
    break;
  case ( RVA_RV_replay_cell1 ):
    if (True == False) {;}
    else if  (RVA_RV_cell1_mode == (2.0)) {
      RVA_RV_k_u = 1 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
      cstate =  RVA_RV_annhilate ;
      force_init_update = False;
    }
    else if  (RVA_RV_k >= (10.0)) {
      RVA_RV_from_cell_u = 2 ;
      RVA_RV_cell2_replay_latch_u = 1 ;
      RVA_RV_k_u = 1 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
      cstate =  RVA_RV_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVA_RV_k_init = RVA_RV_k ;
      slope_RVA_RV_k = 1 ;
      RVA_RV_k_u = (slope_RVA_RV_k * d) + RVA_RV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVA_RV_replay_cell1 ;
      force_init_update = False;
      RVA_RV_cell1_replay_latch_u = 1 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell1_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RV!\n");
      exit(1);
    }
    break;
  case ( RVA_RV_replay_cell2 ):
    if (True == False) {;}
    else if  (RVA_RV_k >= (10.0)) {
      RVA_RV_k_u = 1 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
      cstate =  RVA_RV_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVA_RV_k_init = RVA_RV_k ;
      slope_RVA_RV_k = 1 ;
      RVA_RV_k_u = (slope_RVA_RV_k * d) + RVA_RV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVA_RV_replay_cell2 ;
      force_init_update = False;
      RVA_RV_cell2_replay_latch_u = 1 ;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell1_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RV!\n");
      exit(1);
    }
    break;
  case ( RVA_RV_wait_cell2 ):
    if (True == False) {;}
    else if  (RVA_RV_k >= (10.0)) {
      RVA_RV_k_u = 1 ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
      cstate =  RVA_RV_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVA_RV_k_init = RVA_RV_k ;
      slope_RVA_RV_k = 1 ;
      RVA_RV_k_u = (slope_RVA_RV_k * d) + RVA_RV_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVA_RV_wait_cell2 ;
      force_init_update = False;
      RVA_RV_cell1_v_delayed_u = RVA_RV_update_c1vd () ;
      RVA_RV_cell2_v_delayed_u = RVA_RV_update_c2vd () ;
      RVA_RV_cell1_mode_delayed_u = RVA_RV_update_c1md () ;
      RVA_RV_cell2_mode_delayed_u = RVA_RV_update_c2md () ;
      RVA_RV_wasted_u = RVA_RV_update_buffer_index (RVA_RV_cell1_v,RVA_RV_cell2_v,RVA_RV_cell1_mode,RVA_RV_cell2_mode) ;
      RVA_RV_cell1_replay_latch_u = RVA_RV_update_latch1 (RVA_RV_cell1_mode_delayed,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_replay_latch_u = RVA_RV_update_latch2 (RVA_RV_cell2_mode_delayed,RVA_RV_cell2_replay_latch_u) ;
      RVA_RV_cell1_v_replay = RVA_RV_update_ocell1 (RVA_RV_cell1_v_delayed_u,RVA_RV_cell1_replay_latch_u) ;
      RVA_RV_cell2_v_replay = RVA_RV_update_ocell2 (RVA_RV_cell2_v_delayed_u,RVA_RV_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RV!\n");
      exit(1);
    }
    break;
  }
  RVA_RV_k = RVA_RV_k_u;
  RVA_RV_cell1_mode_delayed = RVA_RV_cell1_mode_delayed_u;
  RVA_RV_cell2_mode_delayed = RVA_RV_cell2_mode_delayed_u;
  RVA_RV_from_cell = RVA_RV_from_cell_u;
  RVA_RV_cell1_replay_latch = RVA_RV_cell1_replay_latch_u;
  RVA_RV_cell2_replay_latch = RVA_RV_cell2_replay_latch_u;
  RVA_RV_cell1_v_delayed = RVA_RV_cell1_v_delayed_u;
  RVA_RV_cell2_v_delayed = RVA_RV_cell2_v_delayed_u;
  RVA_RV_wasted = RVA_RV_wasted_u;
  return cstate;
}