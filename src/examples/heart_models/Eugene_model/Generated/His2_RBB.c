#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double His2_RBB_update_c1vd();
extern double His2_RBB_update_c2vd();
extern double His2_RBB_update_c1md();
extern double His2_RBB_update_c2md();
extern double His2_RBB_update_buffer_index(double,double,double,double);
extern double His2_RBB_update_latch1(double,double);
extern double His2_RBB_update_latch2(double,double);
extern double His2_RBB_update_ocell1(double,double);
extern double His2_RBB_update_ocell2(double,double);
double His2_RBB_cell1_v;
double His2_RBB_cell1_mode;
double His2_RBB_cell2_v;
double His2_RBB_cell2_mode;
double His2_RBB_cell1_v_replay = 0.0;
double His2_RBB_cell2_v_replay = 0.0;


static double  His2_RBB_k  =  0.0 ,  His2_RBB_cell1_mode_delayed  =  0.0 ,  His2_RBB_cell2_mode_delayed  =  0.0 ,  His2_RBB_from_cell  =  0.0 ,  His2_RBB_cell1_replay_latch  =  0.0 ,  His2_RBB_cell2_replay_latch  =  0.0 ,  His2_RBB_cell1_v_delayed  =  0.0 ,  His2_RBB_cell2_v_delayed  =  0.0 ,  His2_RBB_wasted  =  0.0 ; //the continuous vars
static double  His2_RBB_k_u , His2_RBB_cell1_mode_delayed_u , His2_RBB_cell2_mode_delayed_u , His2_RBB_from_cell_u , His2_RBB_cell1_replay_latch_u , His2_RBB_cell2_replay_latch_u , His2_RBB_cell1_v_delayed_u , His2_RBB_cell2_v_delayed_u , His2_RBB_wasted_u ; // and their updates
static double  His2_RBB_k_init , His2_RBB_cell1_mode_delayed_init , His2_RBB_cell2_mode_delayed_init , His2_RBB_from_cell_init , His2_RBB_cell1_replay_latch_init , His2_RBB_cell2_replay_latch_init , His2_RBB_cell1_v_delayed_init , His2_RBB_cell2_v_delayed_init , His2_RBB_wasted_init ; // and their inits
static double  slope_His2_RBB_k , slope_His2_RBB_cell1_mode_delayed , slope_His2_RBB_cell2_mode_delayed , slope_His2_RBB_from_cell , slope_His2_RBB_cell1_replay_latch , slope_His2_RBB_cell2_replay_latch , slope_His2_RBB_cell1_v_delayed , slope_His2_RBB_cell2_v_delayed , slope_His2_RBB_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { His2_RBB_idle , His2_RBB_annhilate , His2_RBB_previous_drection1 , His2_RBB_previous_direction2 , His2_RBB_wait_cell1 , His2_RBB_replay_cell1 , His2_RBB_replay_cell2 , His2_RBB_wait_cell2 }; // state declarations

enum states His2_RBB (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( His2_RBB_idle ):
    if (True == False) {;}
    else if  (His2_RBB_cell2_mode == (2.0) && (His2_RBB_cell1_mode != (2.0))) {
      His2_RBB_k_u = 1 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
      cstate =  His2_RBB_previous_direction2 ;
      force_init_update = False;
    }
    else if  (His2_RBB_cell1_mode == (2.0) && (His2_RBB_cell2_mode != (2.0))) {
      His2_RBB_k_u = 1 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
      cstate =  His2_RBB_previous_drection1 ;
      force_init_update = False;
    }
    else if  (His2_RBB_cell1_mode == (2.0) && (His2_RBB_cell2_mode == (2.0))) {
      His2_RBB_k_u = 1 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
      cstate =  His2_RBB_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His2_RBB_k_init = His2_RBB_k ;
      slope_His2_RBB_k = 1 ;
      His2_RBB_k_u = (slope_His2_RBB_k * d) + His2_RBB_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His2_RBB_idle ;
      force_init_update = False;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell1_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_RBB!\n");
      exit(1);
    }
    break;
  case ( His2_RBB_annhilate ):
    if (True == False) {;}
    else if  (His2_RBB_cell1_mode != (2.0) && (His2_RBB_cell2_mode != (2.0))) {
      His2_RBB_k_u = 1 ;
      His2_RBB_from_cell_u = 0 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
      cstate =  His2_RBB_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His2_RBB_k_init = His2_RBB_k ;
      slope_His2_RBB_k = 1 ;
      His2_RBB_k_u = (slope_His2_RBB_k * d) + His2_RBB_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His2_RBB_annhilate ;
      force_init_update = False;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell1_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_RBB!\n");
      exit(1);
    }
    break;
  case ( His2_RBB_previous_drection1 ):
    if (True == False) {;}
    else if  (His2_RBB_from_cell == (1.0)) {
      His2_RBB_k_u = 1 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
      cstate =  His2_RBB_wait_cell1 ;
      force_init_update = False;
    }
    else if  (His2_RBB_from_cell == (0.0)) {
      His2_RBB_k_u = 1 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
      cstate =  His2_RBB_wait_cell1 ;
      force_init_update = False;
    }
    else if  (His2_RBB_from_cell == (2.0) && (His2_RBB_cell2_mode_delayed == (0.0))) {
      His2_RBB_k_u = 1 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
      cstate =  His2_RBB_wait_cell1 ;
      force_init_update = False;
    }
    else if  (His2_RBB_from_cell == (2.0) && (His2_RBB_cell2_mode_delayed != (0.0))) {
      His2_RBB_k_u = 1 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
      cstate =  His2_RBB_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His2_RBB_k_init = His2_RBB_k ;
      slope_His2_RBB_k = 1 ;
      His2_RBB_k_u = (slope_His2_RBB_k * d) + His2_RBB_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His2_RBB_previous_drection1 ;
      force_init_update = False;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell1_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_RBB!\n");
      exit(1);
    }
    break;
  case ( His2_RBB_previous_direction2 ):
    if (True == False) {;}
    else if  (His2_RBB_from_cell == (1.0) && (His2_RBB_cell1_mode_delayed != (0.0))) {
      His2_RBB_k_u = 1 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
      cstate =  His2_RBB_annhilate ;
      force_init_update = False;
    }
    else if  (His2_RBB_from_cell == (2.0)) {
      His2_RBB_k_u = 1 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
      cstate =  His2_RBB_replay_cell1 ;
      force_init_update = False;
    }
    else if  (His2_RBB_from_cell == (0.0)) {
      His2_RBB_k_u = 1 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
      cstate =  His2_RBB_replay_cell1 ;
      force_init_update = False;
    }
    else if  (His2_RBB_from_cell == (1.0) && (His2_RBB_cell1_mode_delayed == (0.0))) {
      His2_RBB_k_u = 1 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
      cstate =  His2_RBB_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His2_RBB_k_init = His2_RBB_k ;
      slope_His2_RBB_k = 1 ;
      His2_RBB_k_u = (slope_His2_RBB_k * d) + His2_RBB_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His2_RBB_previous_direction2 ;
      force_init_update = False;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell1_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_RBB!\n");
      exit(1);
    }
    break;
  case ( His2_RBB_wait_cell1 ):
    if (True == False) {;}
    else if  (His2_RBB_cell2_mode == (2.0)) {
      His2_RBB_k_u = 1 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
      cstate =  His2_RBB_annhilate ;
      force_init_update = False;
    }
    else if  (His2_RBB_k >= (20.0)) {
      His2_RBB_from_cell_u = 1 ;
      His2_RBB_cell1_replay_latch_u = 1 ;
      His2_RBB_k_u = 1 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
      cstate =  His2_RBB_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His2_RBB_k_init = His2_RBB_k ;
      slope_His2_RBB_k = 1 ;
      His2_RBB_k_u = (slope_His2_RBB_k * d) + His2_RBB_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His2_RBB_wait_cell1 ;
      force_init_update = False;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell1_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_RBB!\n");
      exit(1);
    }
    break;
  case ( His2_RBB_replay_cell1 ):
    if (True == False) {;}
    else if  (His2_RBB_cell1_mode == (2.0)) {
      His2_RBB_k_u = 1 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
      cstate =  His2_RBB_annhilate ;
      force_init_update = False;
    }
    else if  (His2_RBB_k >= (20.0)) {
      His2_RBB_from_cell_u = 2 ;
      His2_RBB_cell2_replay_latch_u = 1 ;
      His2_RBB_k_u = 1 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
      cstate =  His2_RBB_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His2_RBB_k_init = His2_RBB_k ;
      slope_His2_RBB_k = 1 ;
      His2_RBB_k_u = (slope_His2_RBB_k * d) + His2_RBB_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His2_RBB_replay_cell1 ;
      force_init_update = False;
      His2_RBB_cell1_replay_latch_u = 1 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell1_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_RBB!\n");
      exit(1);
    }
    break;
  case ( His2_RBB_replay_cell2 ):
    if (True == False) {;}
    else if  (His2_RBB_k >= (10.0)) {
      His2_RBB_k_u = 1 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
      cstate =  His2_RBB_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His2_RBB_k_init = His2_RBB_k ;
      slope_His2_RBB_k = 1 ;
      His2_RBB_k_u = (slope_His2_RBB_k * d) + His2_RBB_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His2_RBB_replay_cell2 ;
      force_init_update = False;
      His2_RBB_cell2_replay_latch_u = 1 ;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell1_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_RBB!\n");
      exit(1);
    }
    break;
  case ( His2_RBB_wait_cell2 ):
    if (True == False) {;}
    else if  (His2_RBB_k >= (10.0)) {
      His2_RBB_k_u = 1 ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
      cstate =  His2_RBB_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His2_RBB_k_init = His2_RBB_k ;
      slope_His2_RBB_k = 1 ;
      His2_RBB_k_u = (slope_His2_RBB_k * d) + His2_RBB_k ;
      /* Possible Saturation */
      
      
      
      cstate =  His2_RBB_wait_cell2 ;
      force_init_update = False;
      His2_RBB_cell1_v_delayed_u = His2_RBB_update_c1vd () ;
      His2_RBB_cell2_v_delayed_u = His2_RBB_update_c2vd () ;
      His2_RBB_cell1_mode_delayed_u = His2_RBB_update_c1md () ;
      His2_RBB_cell2_mode_delayed_u = His2_RBB_update_c2md () ;
      His2_RBB_wasted_u = His2_RBB_update_buffer_index (His2_RBB_cell1_v,His2_RBB_cell2_v,His2_RBB_cell1_mode,His2_RBB_cell2_mode) ;
      His2_RBB_cell1_replay_latch_u = His2_RBB_update_latch1 (His2_RBB_cell1_mode_delayed,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_replay_latch_u = His2_RBB_update_latch2 (His2_RBB_cell2_mode_delayed,His2_RBB_cell2_replay_latch_u) ;
      His2_RBB_cell1_v_replay = His2_RBB_update_ocell1 (His2_RBB_cell1_v_delayed_u,His2_RBB_cell1_replay_latch_u) ;
      His2_RBB_cell2_v_replay = His2_RBB_update_ocell2 (His2_RBB_cell2_v_delayed_u,His2_RBB_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_RBB!\n");
      exit(1);
    }
    break;
  }
  His2_RBB_k = His2_RBB_k_u;
  His2_RBB_cell1_mode_delayed = His2_RBB_cell1_mode_delayed_u;
  His2_RBB_cell2_mode_delayed = His2_RBB_cell2_mode_delayed_u;
  His2_RBB_from_cell = His2_RBB_from_cell_u;
  His2_RBB_cell1_replay_latch = His2_RBB_cell1_replay_latch_u;
  His2_RBB_cell2_replay_latch = His2_RBB_cell2_replay_latch_u;
  His2_RBB_cell1_v_delayed = His2_RBB_cell1_v_delayed_u;
  His2_RBB_cell2_v_delayed = His2_RBB_cell2_v_delayed_u;
  His2_RBB_wasted = His2_RBB_wasted_u;
  return cstate;
}