#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double LeftAtrium_v_i_0;
double LeftAtrium_v_i_1;
double LeftAtrium_v_i_2;
double LeftAtrium_voo = 0.0;
double LeftAtrium_state = 0.0;


static double  LeftAtrium_vx  =  0 ,  LeftAtrium_vy  =  0 ,  LeftAtrium_vz  =  0 ,  LeftAtrium_g  =  0 ,  LeftAtrium_v  =  0 ,  LeftAtrium_ft  =  0 ,  LeftAtrium_theta  =  0 ,  LeftAtrium_v_O  =  0 ; //the continuous vars
static double  LeftAtrium_vx_u , LeftAtrium_vy_u , LeftAtrium_vz_u , LeftAtrium_g_u , LeftAtrium_v_u , LeftAtrium_ft_u , LeftAtrium_theta_u , LeftAtrium_v_O_u ; // and their updates
static double  LeftAtrium_vx_init , LeftAtrium_vy_init , LeftAtrium_vz_init , LeftAtrium_g_init , LeftAtrium_v_init , LeftAtrium_ft_init , LeftAtrium_theta_init , LeftAtrium_v_O_init ; // and their inits
static double  slope_LeftAtrium_vx , slope_LeftAtrium_vy , slope_LeftAtrium_vz , slope_LeftAtrium_g , slope_LeftAtrium_v , slope_LeftAtrium_ft , slope_LeftAtrium_theta , slope_LeftAtrium_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { LeftAtrium_t1 , LeftAtrium_t2 , LeftAtrium_t3 , LeftAtrium_t4 }; // state declarations

enum states LeftAtrium (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( LeftAtrium_t1 ):
    if (True == False) {;}
    else if  (LeftAtrium_g > (44.5)) {
      LeftAtrium_vx_u = (0.3 * LeftAtrium_v) ;
      LeftAtrium_vy_u = 0 ;
      LeftAtrium_vz_u = (0.7 * LeftAtrium_v) ;
      LeftAtrium_g_u = ((((((((LeftAtrium_v_i_0 + (- ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftAtrium_v_i_1 + (- ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftAtrium_theta_u = (LeftAtrium_v / 30.0) ;
      LeftAtrium_v_O_u = (131.1 + (- (80.1 * pow ( ((LeftAtrium_v / 30.0)) , (0.5) )))) ;
      LeftAtrium_ft_u = f (LeftAtrium_theta,4.0e-2) ;
      cstate =  LeftAtrium_t2 ;
      force_init_update = False;
    }

    else if ( LeftAtrium_v <= (44.5)
               && 
              LeftAtrium_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) LeftAtrium_vx_init = LeftAtrium_vx ;
      slope_LeftAtrium_vx = (LeftAtrium_vx * -8.7) ;
      LeftAtrium_vx_u = (slope_LeftAtrium_vx * d) + LeftAtrium_vx ;
      if ((pstate != cstate) || force_init_update) LeftAtrium_vy_init = LeftAtrium_vy ;
      slope_LeftAtrium_vy = (LeftAtrium_vy * -190.9) ;
      LeftAtrium_vy_u = (slope_LeftAtrium_vy * d) + LeftAtrium_vy ;
      if ((pstate != cstate) || force_init_update) LeftAtrium_vz_init = LeftAtrium_vz ;
      slope_LeftAtrium_vz = (LeftAtrium_vz * -190.4) ;
      LeftAtrium_vz_u = (slope_LeftAtrium_vz * d) + LeftAtrium_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftAtrium_t1 ;
      force_init_update = False;
      LeftAtrium_g_u = ((((((((LeftAtrium_v_i_0 + (- ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftAtrium_v_i_1 + (- ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftAtrium_v_u = ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz) ;
      LeftAtrium_voo = ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz) ;
      LeftAtrium_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftAtrium!\n");
      exit(1);
    }
    break;
  case ( LeftAtrium_t2 ):
    if (True == False) {;}
    else if  (LeftAtrium_v >= (44.5)) {
      LeftAtrium_vx_u = LeftAtrium_vx ;
      LeftAtrium_vy_u = LeftAtrium_vy ;
      LeftAtrium_vz_u = LeftAtrium_vz ;
      LeftAtrium_g_u = ((((((((LeftAtrium_v_i_0 + (- ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftAtrium_v_i_1 + (- ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftAtrium_t3 ;
      force_init_update = False;
    }
    else if  (LeftAtrium_g <= (44.5)
               && LeftAtrium_v < (44.5)) {
      LeftAtrium_vx_u = LeftAtrium_vx ;
      LeftAtrium_vy_u = LeftAtrium_vy ;
      LeftAtrium_vz_u = LeftAtrium_vz ;
      LeftAtrium_g_u = ((((((((LeftAtrium_v_i_0 + (- ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftAtrium_v_i_1 + (- ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftAtrium_t1 ;
      force_init_update = False;
    }

    else if ( LeftAtrium_v < (44.5)
               && LeftAtrium_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftAtrium_vx_init = LeftAtrium_vx ;
      slope_LeftAtrium_vx = ((LeftAtrium_vx * -23.6) + (777200.0 * LeftAtrium_g)) ;
      LeftAtrium_vx_u = (slope_LeftAtrium_vx * d) + LeftAtrium_vx ;
      if ((pstate != cstate) || force_init_update) LeftAtrium_vy_init = LeftAtrium_vy ;
      slope_LeftAtrium_vy = ((LeftAtrium_vy * -45.5) + (58900.0 * LeftAtrium_g)) ;
      LeftAtrium_vy_u = (slope_LeftAtrium_vy * d) + LeftAtrium_vy ;
      if ((pstate != cstate) || force_init_update) LeftAtrium_vz_init = LeftAtrium_vz ;
      slope_LeftAtrium_vz = ((LeftAtrium_vz * -12.9) + (276600.0 * LeftAtrium_g)) ;
      LeftAtrium_vz_u = (slope_LeftAtrium_vz * d) + LeftAtrium_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftAtrium_t2 ;
      force_init_update = False;
      LeftAtrium_g_u = ((((((((LeftAtrium_v_i_0 + (- ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftAtrium_v_i_1 + (- ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftAtrium_v_u = ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz) ;
      LeftAtrium_voo = ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz) ;
      LeftAtrium_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftAtrium!\n");
      exit(1);
    }
    break;
  case ( LeftAtrium_t3 ):
    if (True == False) {;}
    else if  (LeftAtrium_v >= (131.1)) {
      LeftAtrium_vx_u = LeftAtrium_vx ;
      LeftAtrium_vy_u = LeftAtrium_vy ;
      LeftAtrium_vz_u = LeftAtrium_vz ;
      LeftAtrium_g_u = ((((((((LeftAtrium_v_i_0 + (- ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftAtrium_v_i_1 + (- ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftAtrium_t4 ;
      force_init_update = False;
    }

    else if ( LeftAtrium_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) LeftAtrium_vx_init = LeftAtrium_vx ;
      slope_LeftAtrium_vx = (LeftAtrium_vx * -6.9) ;
      LeftAtrium_vx_u = (slope_LeftAtrium_vx * d) + LeftAtrium_vx ;
      if ((pstate != cstate) || force_init_update) LeftAtrium_vy_init = LeftAtrium_vy ;
      slope_LeftAtrium_vy = (LeftAtrium_vy * 75.9) ;
      LeftAtrium_vy_u = (slope_LeftAtrium_vy * d) + LeftAtrium_vy ;
      if ((pstate != cstate) || force_init_update) LeftAtrium_vz_init = LeftAtrium_vz ;
      slope_LeftAtrium_vz = (LeftAtrium_vz * 6826.5) ;
      LeftAtrium_vz_u = (slope_LeftAtrium_vz * d) + LeftAtrium_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftAtrium_t3 ;
      force_init_update = False;
      LeftAtrium_g_u = ((((((((LeftAtrium_v_i_0 + (- ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftAtrium_v_i_1 + (- ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftAtrium_v_u = ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz) ;
      LeftAtrium_voo = ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz) ;
      LeftAtrium_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftAtrium!\n");
      exit(1);
    }
    break;
  case ( LeftAtrium_t4 ):
    if (True == False) {;}
    else if  (LeftAtrium_v <= (30.0)) {
      LeftAtrium_vx_u = LeftAtrium_vx ;
      LeftAtrium_vy_u = LeftAtrium_vy ;
      LeftAtrium_vz_u = LeftAtrium_vz ;
      LeftAtrium_g_u = ((((((((LeftAtrium_v_i_0 + (- ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftAtrium_v_i_1 + (- ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftAtrium_t1 ;
      force_init_update = False;
    }

    else if ( LeftAtrium_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftAtrium_vx_init = LeftAtrium_vx ;
      slope_LeftAtrium_vx = (LeftAtrium_vx * -33.2) ;
      LeftAtrium_vx_u = (slope_LeftAtrium_vx * d) + LeftAtrium_vx ;
      if ((pstate != cstate) || force_init_update) LeftAtrium_vy_init = LeftAtrium_vy ;
      slope_LeftAtrium_vy = ((LeftAtrium_vy * 20.0) * LeftAtrium_ft) ;
      LeftAtrium_vy_u = (slope_LeftAtrium_vy * d) + LeftAtrium_vy ;
      if ((pstate != cstate) || force_init_update) LeftAtrium_vz_init = LeftAtrium_vz ;
      slope_LeftAtrium_vz = ((LeftAtrium_vz * 2.0) * LeftAtrium_ft) ;
      LeftAtrium_vz_u = (slope_LeftAtrium_vz * d) + LeftAtrium_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftAtrium_t4 ;
      force_init_update = False;
      LeftAtrium_g_u = ((((((((LeftAtrium_v_i_0 + (- ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftAtrium_v_i_1 + (- ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftAtrium_v_u = ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz) ;
      LeftAtrium_voo = ((LeftAtrium_vx + (- LeftAtrium_vy)) + LeftAtrium_vz) ;
      LeftAtrium_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftAtrium!\n");
      exit(1);
    }
    break;
  }
  LeftAtrium_vx = LeftAtrium_vx_u;
  LeftAtrium_vy = LeftAtrium_vy_u;
  LeftAtrium_vz = LeftAtrium_vz_u;
  LeftAtrium_g = LeftAtrium_g_u;
  LeftAtrium_v = LeftAtrium_v_u;
  LeftAtrium_ft = LeftAtrium_ft_u;
  LeftAtrium_theta = LeftAtrium_theta_u;
  LeftAtrium_v_O = LeftAtrium_v_O_u;
  return cstate;
}