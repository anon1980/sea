#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double RightVentricularApex_v_i_0;
double RightVentricularApex_v_i_1;
double RightVentricularApex_v_i_2;
double RightVentricularApex_v_i_3;
double RightVentricularApex_v_i_4;
double RightVentricularApex_voo = 0.0;
double RightVentricularApex_state = 0.0;


static double  RightVentricularApex_vx  =  0 ,  RightVentricularApex_vy  =  0 ,  RightVentricularApex_vz  =  0 ,  RightVentricularApex_g  =  0 ,  RightVentricularApex_v  =  0 ,  RightVentricularApex_ft  =  0 ,  RightVentricularApex_theta  =  0 ,  RightVentricularApex_v_O  =  0 ; //the continuous vars
static double  RightVentricularApex_vx_u , RightVentricularApex_vy_u , RightVentricularApex_vz_u , RightVentricularApex_g_u , RightVentricularApex_v_u , RightVentricularApex_ft_u , RightVentricularApex_theta_u , RightVentricularApex_v_O_u ; // and their updates
static double  RightVentricularApex_vx_init , RightVentricularApex_vy_init , RightVentricularApex_vz_init , RightVentricularApex_g_init , RightVentricularApex_v_init , RightVentricularApex_ft_init , RightVentricularApex_theta_init , RightVentricularApex_v_O_init ; // and their inits
static double  slope_RightVentricularApex_vx , slope_RightVentricularApex_vy , slope_RightVentricularApex_vz , slope_RightVentricularApex_g , slope_RightVentricularApex_v , slope_RightVentricularApex_ft , slope_RightVentricularApex_theta , slope_RightVentricularApex_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { RightVentricularApex_t1 , RightVentricularApex_t2 , RightVentricularApex_t3 , RightVentricularApex_t4 }; // state declarations

enum states RightVentricularApex (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( RightVentricularApex_t1 ):
    if (True == False) {;}
    else if  (RightVentricularApex_g > (44.5)) {
      RightVentricularApex_vx_u = (0.3 * RightVentricularApex_v) ;
      RightVentricularApex_vy_u = 0 ;
      RightVentricularApex_vz_u = (0.7 * RightVentricularApex_v) ;
      RightVentricularApex_g_u = ((((((((RightVentricularApex_v_i_0 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricularApex_v_i_1 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricularApex_v_i_2 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricularApex_v_i_3 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) ;
      RightVentricularApex_theta_u = (RightVentricularApex_v / 30.0) ;
      RightVentricularApex_v_O_u = (131.1 + (- (80.1 * pow ( ((RightVentricularApex_v / 30.0)) , (0.5) )))) ;
      RightVentricularApex_ft_u = f (RightVentricularApex_theta,4.0e-2) ;
      cstate =  RightVentricularApex_t2 ;
      force_init_update = False;
    }

    else if ( RightVentricularApex_v <= (44.5)
               && 
              RightVentricularApex_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularApex_vx_init = RightVentricularApex_vx ;
      slope_RightVentricularApex_vx = (RightVentricularApex_vx * -8.7) ;
      RightVentricularApex_vx_u = (slope_RightVentricularApex_vx * d) + RightVentricularApex_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricularApex_vy_init = RightVentricularApex_vy ;
      slope_RightVentricularApex_vy = (RightVentricularApex_vy * -190.9) ;
      RightVentricularApex_vy_u = (slope_RightVentricularApex_vy * d) + RightVentricularApex_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricularApex_vz_init = RightVentricularApex_vz ;
      slope_RightVentricularApex_vz = (RightVentricularApex_vz * -190.4) ;
      RightVentricularApex_vz_u = (slope_RightVentricularApex_vz * d) + RightVentricularApex_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricularApex_t1 ;
      force_init_update = False;
      RightVentricularApex_g_u = ((((((((RightVentricularApex_v_i_0 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricularApex_v_i_1 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricularApex_v_i_2 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricularApex_v_i_3 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) ;
      RightVentricularApex_v_u = ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz) ;
      RightVentricularApex_voo = ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz) ;
      RightVentricularApex_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularApex!\n");
      exit(1);
    }
    break;
  case ( RightVentricularApex_t2 ):
    if (True == False) {;}
    else if  (RightVentricularApex_v >= (44.5)) {
      RightVentricularApex_vx_u = RightVentricularApex_vx ;
      RightVentricularApex_vy_u = RightVentricularApex_vy ;
      RightVentricularApex_vz_u = RightVentricularApex_vz ;
      RightVentricularApex_g_u = ((((((((RightVentricularApex_v_i_0 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricularApex_v_i_1 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricularApex_v_i_2 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricularApex_v_i_3 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) ;
      cstate =  RightVentricularApex_t3 ;
      force_init_update = False;
    }
    else if  (RightVentricularApex_g <= (44.5)
               && 
              RightVentricularApex_v < (44.5)) {
      RightVentricularApex_vx_u = RightVentricularApex_vx ;
      RightVentricularApex_vy_u = RightVentricularApex_vy ;
      RightVentricularApex_vz_u = RightVentricularApex_vz ;
      RightVentricularApex_g_u = ((((((((RightVentricularApex_v_i_0 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricularApex_v_i_1 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricularApex_v_i_2 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricularApex_v_i_3 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) ;
      cstate =  RightVentricularApex_t1 ;
      force_init_update = False;
    }

    else if ( RightVentricularApex_v < (44.5)
               && 
              RightVentricularApex_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularApex_vx_init = RightVentricularApex_vx ;
      slope_RightVentricularApex_vx = ((RightVentricularApex_vx * -23.6) + (777200.0 * RightVentricularApex_g)) ;
      RightVentricularApex_vx_u = (slope_RightVentricularApex_vx * d) + RightVentricularApex_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricularApex_vy_init = RightVentricularApex_vy ;
      slope_RightVentricularApex_vy = ((RightVentricularApex_vy * -45.5) + (58900.0 * RightVentricularApex_g)) ;
      RightVentricularApex_vy_u = (slope_RightVentricularApex_vy * d) + RightVentricularApex_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricularApex_vz_init = RightVentricularApex_vz ;
      slope_RightVentricularApex_vz = ((RightVentricularApex_vz * -12.9) + (276600.0 * RightVentricularApex_g)) ;
      RightVentricularApex_vz_u = (slope_RightVentricularApex_vz * d) + RightVentricularApex_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricularApex_t2 ;
      force_init_update = False;
      RightVentricularApex_g_u = ((((((((RightVentricularApex_v_i_0 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricularApex_v_i_1 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricularApex_v_i_2 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricularApex_v_i_3 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) ;
      RightVentricularApex_v_u = ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz) ;
      RightVentricularApex_voo = ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz) ;
      RightVentricularApex_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularApex!\n");
      exit(1);
    }
    break;
  case ( RightVentricularApex_t3 ):
    if (True == False) {;}
    else if  (RightVentricularApex_v >= (131.1)) {
      RightVentricularApex_vx_u = RightVentricularApex_vx ;
      RightVentricularApex_vy_u = RightVentricularApex_vy ;
      RightVentricularApex_vz_u = RightVentricularApex_vz ;
      RightVentricularApex_g_u = ((((((((RightVentricularApex_v_i_0 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricularApex_v_i_1 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricularApex_v_i_2 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricularApex_v_i_3 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) ;
      cstate =  RightVentricularApex_t4 ;
      force_init_update = False;
    }

    else if ( RightVentricularApex_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularApex_vx_init = RightVentricularApex_vx ;
      slope_RightVentricularApex_vx = (RightVentricularApex_vx * -6.9) ;
      RightVentricularApex_vx_u = (slope_RightVentricularApex_vx * d) + RightVentricularApex_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricularApex_vy_init = RightVentricularApex_vy ;
      slope_RightVentricularApex_vy = (RightVentricularApex_vy * 75.9) ;
      RightVentricularApex_vy_u = (slope_RightVentricularApex_vy * d) + RightVentricularApex_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricularApex_vz_init = RightVentricularApex_vz ;
      slope_RightVentricularApex_vz = (RightVentricularApex_vz * 6826.5) ;
      RightVentricularApex_vz_u = (slope_RightVentricularApex_vz * d) + RightVentricularApex_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricularApex_t3 ;
      force_init_update = False;
      RightVentricularApex_g_u = ((((((((RightVentricularApex_v_i_0 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricularApex_v_i_1 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricularApex_v_i_2 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricularApex_v_i_3 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) ;
      RightVentricularApex_v_u = ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz) ;
      RightVentricularApex_voo = ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz) ;
      RightVentricularApex_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularApex!\n");
      exit(1);
    }
    break;
  case ( RightVentricularApex_t4 ):
    if (True == False) {;}
    else if  (RightVentricularApex_v <= (30.0)) {
      RightVentricularApex_vx_u = RightVentricularApex_vx ;
      RightVentricularApex_vy_u = RightVentricularApex_vy ;
      RightVentricularApex_vz_u = RightVentricularApex_vz ;
      RightVentricularApex_g_u = ((((((((RightVentricularApex_v_i_0 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricularApex_v_i_1 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricularApex_v_i_2 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricularApex_v_i_3 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) ;
      cstate =  RightVentricularApex_t1 ;
      force_init_update = False;
    }

    else if ( RightVentricularApex_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularApex_vx_init = RightVentricularApex_vx ;
      slope_RightVentricularApex_vx = (RightVentricularApex_vx * -33.2) ;
      RightVentricularApex_vx_u = (slope_RightVentricularApex_vx * d) + RightVentricularApex_vx ;
      if ((pstate != cstate) || force_init_update) RightVentricularApex_vy_init = RightVentricularApex_vy ;
      slope_RightVentricularApex_vy = ((RightVentricularApex_vy * 11.0) * RightVentricularApex_ft) ;
      RightVentricularApex_vy_u = (slope_RightVentricularApex_vy * d) + RightVentricularApex_vy ;
      if ((pstate != cstate) || force_init_update) RightVentricularApex_vz_init = RightVentricularApex_vz ;
      slope_RightVentricularApex_vz = ((RightVentricularApex_vz * 2.0) * RightVentricularApex_ft) ;
      RightVentricularApex_vz_u = (slope_RightVentricularApex_vz * d) + RightVentricularApex_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightVentricularApex_t4 ;
      force_init_update = False;
      RightVentricularApex_g_u = ((((((((RightVentricularApex_v_i_0 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightVentricularApex_v_i_1 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricularApex_v_i_2 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((RightVentricularApex_v_i_3 + (- ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) ;
      RightVentricularApex_v_u = ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz) ;
      RightVentricularApex_voo = ((RightVentricularApex_vx + (- RightVentricularApex_vy)) + RightVentricularApex_vz) ;
      RightVentricularApex_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularApex!\n");
      exit(1);
    }
    break;
  }
  RightVentricularApex_vx = RightVentricularApex_vx_u;
  RightVentricularApex_vy = RightVentricularApex_vy_u;
  RightVentricularApex_vz = RightVentricularApex_vz_u;
  RightVentricularApex_g = RightVentricularApex_g_u;
  RightVentricularApex_v = RightVentricularApex_v_u;
  RightVentricularApex_ft = RightVentricularApex_ft_u;
  RightVentricularApex_theta = RightVentricularApex_theta_u;
  RightVentricularApex_v_O = RightVentricularApex_v_O_u;
  return cstate;
}