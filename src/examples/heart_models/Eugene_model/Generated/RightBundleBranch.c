#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double RightBundleBranch_v_i_0;
double RightBundleBranch_v_i_1;
double RightBundleBranch_v_i_2;
double RightBundleBranch_voo = 0.0;
double RightBundleBranch_state = 0.0;


static double  RightBundleBranch_vx  =  0 ,  RightBundleBranch_vy  =  0 ,  RightBundleBranch_vz  =  0 ,  RightBundleBranch_g  =  0 ,  RightBundleBranch_v  =  0 ,  RightBundleBranch_ft  =  0 ,  RightBundleBranch_theta  =  0 ,  RightBundleBranch_v_O  =  0 ; //the continuous vars
static double  RightBundleBranch_vx_u , RightBundleBranch_vy_u , RightBundleBranch_vz_u , RightBundleBranch_g_u , RightBundleBranch_v_u , RightBundleBranch_ft_u , RightBundleBranch_theta_u , RightBundleBranch_v_O_u ; // and their updates
static double  RightBundleBranch_vx_init , RightBundleBranch_vy_init , RightBundleBranch_vz_init , RightBundleBranch_g_init , RightBundleBranch_v_init , RightBundleBranch_ft_init , RightBundleBranch_theta_init , RightBundleBranch_v_O_init ; // and their inits
static double  slope_RightBundleBranch_vx , slope_RightBundleBranch_vy , slope_RightBundleBranch_vz , slope_RightBundleBranch_g , slope_RightBundleBranch_v , slope_RightBundleBranch_ft , slope_RightBundleBranch_theta , slope_RightBundleBranch_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { RightBundleBranch_t1 , RightBundleBranch_t2 , RightBundleBranch_t3 , RightBundleBranch_t4 }; // state declarations

enum states RightBundleBranch (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( RightBundleBranch_t1 ):
    if (True == False) {;}
    else if  (RightBundleBranch_g > (44.5)) {
      RightBundleBranch_vx_u = (0.3 * RightBundleBranch_v) ;
      RightBundleBranch_vy_u = 0 ;
      RightBundleBranch_vz_u = (0.7 * RightBundleBranch_v) ;
      RightBundleBranch_g_u = ((((((((RightBundleBranch_v_i_0 + (- ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightBundleBranch_v_i_1 + (- ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightBundleBranch_theta_u = (RightBundleBranch_v / 30.0) ;
      RightBundleBranch_v_O_u = (131.1 + (- (80.1 * pow ( ((RightBundleBranch_v / 30.0)) , (0.5) )))) ;
      RightBundleBranch_ft_u = f (RightBundleBranch_theta,4.0e-2) ;
      cstate =  RightBundleBranch_t2 ;
      force_init_update = False;
    }

    else if ( RightBundleBranch_v <= (44.5)
               && 
              RightBundleBranch_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) RightBundleBranch_vx_init = RightBundleBranch_vx ;
      slope_RightBundleBranch_vx = (RightBundleBranch_vx * -8.7) ;
      RightBundleBranch_vx_u = (slope_RightBundleBranch_vx * d) + RightBundleBranch_vx ;
      if ((pstate != cstate) || force_init_update) RightBundleBranch_vy_init = RightBundleBranch_vy ;
      slope_RightBundleBranch_vy = (RightBundleBranch_vy * -190.9) ;
      RightBundleBranch_vy_u = (slope_RightBundleBranch_vy * d) + RightBundleBranch_vy ;
      if ((pstate != cstate) || force_init_update) RightBundleBranch_vz_init = RightBundleBranch_vz ;
      slope_RightBundleBranch_vz = (RightBundleBranch_vz * -190.4) ;
      RightBundleBranch_vz_u = (slope_RightBundleBranch_vz * d) + RightBundleBranch_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightBundleBranch_t1 ;
      force_init_update = False;
      RightBundleBranch_g_u = ((((((((RightBundleBranch_v_i_0 + (- ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightBundleBranch_v_i_1 + (- ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightBundleBranch_v_u = ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz) ;
      RightBundleBranch_voo = ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz) ;
      RightBundleBranch_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightBundleBranch!\n");
      exit(1);
    }
    break;
  case ( RightBundleBranch_t2 ):
    if (True == False) {;}
    else if  (RightBundleBranch_v >= (44.5)) {
      RightBundleBranch_vx_u = RightBundleBranch_vx ;
      RightBundleBranch_vy_u = RightBundleBranch_vy ;
      RightBundleBranch_vz_u = RightBundleBranch_vz ;
      RightBundleBranch_g_u = ((((((((RightBundleBranch_v_i_0 + (- ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightBundleBranch_v_i_1 + (- ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  RightBundleBranch_t3 ;
      force_init_update = False;
    }
    else if  (RightBundleBranch_g <= (44.5)
               && 
              RightBundleBranch_v < (44.5)) {
      RightBundleBranch_vx_u = RightBundleBranch_vx ;
      RightBundleBranch_vy_u = RightBundleBranch_vy ;
      RightBundleBranch_vz_u = RightBundleBranch_vz ;
      RightBundleBranch_g_u = ((((((((RightBundleBranch_v_i_0 + (- ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightBundleBranch_v_i_1 + (- ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  RightBundleBranch_t1 ;
      force_init_update = False;
    }

    else if ( RightBundleBranch_v < (44.5)
               && 
              RightBundleBranch_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) RightBundleBranch_vx_init = RightBundleBranch_vx ;
      slope_RightBundleBranch_vx = ((RightBundleBranch_vx * -23.6) + (777200.0 * RightBundleBranch_g)) ;
      RightBundleBranch_vx_u = (slope_RightBundleBranch_vx * d) + RightBundleBranch_vx ;
      if ((pstate != cstate) || force_init_update) RightBundleBranch_vy_init = RightBundleBranch_vy ;
      slope_RightBundleBranch_vy = ((RightBundleBranch_vy * -45.5) + (58900.0 * RightBundleBranch_g)) ;
      RightBundleBranch_vy_u = (slope_RightBundleBranch_vy * d) + RightBundleBranch_vy ;
      if ((pstate != cstate) || force_init_update) RightBundleBranch_vz_init = RightBundleBranch_vz ;
      slope_RightBundleBranch_vz = ((RightBundleBranch_vz * -12.9) + (276600.0 * RightBundleBranch_g)) ;
      RightBundleBranch_vz_u = (slope_RightBundleBranch_vz * d) + RightBundleBranch_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightBundleBranch_t2 ;
      force_init_update = False;
      RightBundleBranch_g_u = ((((((((RightBundleBranch_v_i_0 + (- ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightBundleBranch_v_i_1 + (- ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightBundleBranch_v_u = ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz) ;
      RightBundleBranch_voo = ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz) ;
      RightBundleBranch_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightBundleBranch!\n");
      exit(1);
    }
    break;
  case ( RightBundleBranch_t3 ):
    if (True == False) {;}
    else if  (RightBundleBranch_v >= (131.1)) {
      RightBundleBranch_vx_u = RightBundleBranch_vx ;
      RightBundleBranch_vy_u = RightBundleBranch_vy ;
      RightBundleBranch_vz_u = RightBundleBranch_vz ;
      RightBundleBranch_g_u = ((((((((RightBundleBranch_v_i_0 + (- ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightBundleBranch_v_i_1 + (- ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  RightBundleBranch_t4 ;
      force_init_update = False;
    }

    else if ( RightBundleBranch_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) RightBundleBranch_vx_init = RightBundleBranch_vx ;
      slope_RightBundleBranch_vx = (RightBundleBranch_vx * -6.9) ;
      RightBundleBranch_vx_u = (slope_RightBundleBranch_vx * d) + RightBundleBranch_vx ;
      if ((pstate != cstate) || force_init_update) RightBundleBranch_vy_init = RightBundleBranch_vy ;
      slope_RightBundleBranch_vy = (RightBundleBranch_vy * 75.9) ;
      RightBundleBranch_vy_u = (slope_RightBundleBranch_vy * d) + RightBundleBranch_vy ;
      if ((pstate != cstate) || force_init_update) RightBundleBranch_vz_init = RightBundleBranch_vz ;
      slope_RightBundleBranch_vz = (RightBundleBranch_vz * 6826.5) ;
      RightBundleBranch_vz_u = (slope_RightBundleBranch_vz * d) + RightBundleBranch_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightBundleBranch_t3 ;
      force_init_update = False;
      RightBundleBranch_g_u = ((((((((RightBundleBranch_v_i_0 + (- ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightBundleBranch_v_i_1 + (- ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightBundleBranch_v_u = ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz) ;
      RightBundleBranch_voo = ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz) ;
      RightBundleBranch_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightBundleBranch!\n");
      exit(1);
    }
    break;
  case ( RightBundleBranch_t4 ):
    if (True == False) {;}
    else if  (RightBundleBranch_v <= (30.0)) {
      RightBundleBranch_vx_u = RightBundleBranch_vx ;
      RightBundleBranch_vy_u = RightBundleBranch_vy ;
      RightBundleBranch_vz_u = RightBundleBranch_vz ;
      RightBundleBranch_g_u = ((((((((RightBundleBranch_v_i_0 + (- ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightBundleBranch_v_i_1 + (- ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  RightBundleBranch_t1 ;
      force_init_update = False;
    }

    else if ( RightBundleBranch_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) RightBundleBranch_vx_init = RightBundleBranch_vx ;
      slope_RightBundleBranch_vx = (RightBundleBranch_vx * -33.2) ;
      RightBundleBranch_vx_u = (slope_RightBundleBranch_vx * d) + RightBundleBranch_vx ;
      if ((pstate != cstate) || force_init_update) RightBundleBranch_vy_init = RightBundleBranch_vy ;
      slope_RightBundleBranch_vy = ((RightBundleBranch_vy * 11.0) * RightBundleBranch_ft) ;
      RightBundleBranch_vy_u = (slope_RightBundleBranch_vy * d) + RightBundleBranch_vy ;
      if ((pstate != cstate) || force_init_update) RightBundleBranch_vz_init = RightBundleBranch_vz ;
      slope_RightBundleBranch_vz = ((RightBundleBranch_vz * 2.0) * RightBundleBranch_ft) ;
      RightBundleBranch_vz_u = (slope_RightBundleBranch_vz * d) + RightBundleBranch_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  RightBundleBranch_t4 ;
      force_init_update = False;
      RightBundleBranch_g_u = ((((((((RightBundleBranch_v_i_0 + (- ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((RightBundleBranch_v_i_1 + (- ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      RightBundleBranch_v_u = ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz) ;
      RightBundleBranch_voo = ((RightBundleBranch_vx + (- RightBundleBranch_vy)) + RightBundleBranch_vz) ;
      RightBundleBranch_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RightBundleBranch!\n");
      exit(1);
    }
    break;
  }
  RightBundleBranch_vx = RightBundleBranch_vx_u;
  RightBundleBranch_vy = RightBundleBranch_vy_u;
  RightBundleBranch_vz = RightBundleBranch_vz_u;
  RightBundleBranch_g = RightBundleBranch_g_u;
  RightBundleBranch_v = RightBundleBranch_v_u;
  RightBundleBranch_ft = RightBundleBranch_ft_u;
  RightBundleBranch_theta = RightBundleBranch_theta_u;
  RightBundleBranch_v_O = RightBundleBranch_v_O_u;
  return cstate;
}