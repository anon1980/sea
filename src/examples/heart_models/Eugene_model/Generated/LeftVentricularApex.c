#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double LeftVentricularApex_v_i_0;
double LeftVentricularApex_v_i_1;
double LeftVentricularApex_v_i_2;
double LeftVentricularApex_v_i_3;
double LeftVentricularApex_v_i_4;
double LeftVentricularApex_voo = 0.0;
double LeftVentricularApex_state = 0.0;


static double  LeftVentricularApex_vx  =  0 ,  LeftVentricularApex_vy  =  0 ,  LeftVentricularApex_vz  =  0 ,  LeftVentricularApex_g  =  0 ,  LeftVentricularApex_v  =  0 ,  LeftVentricularApex_ft  =  0 ,  LeftVentricularApex_theta  =  0 ,  LeftVentricularApex_v_O  =  0 ; //the continuous vars
static double  LeftVentricularApex_vx_u , LeftVentricularApex_vy_u , LeftVentricularApex_vz_u , LeftVentricularApex_g_u , LeftVentricularApex_v_u , LeftVentricularApex_ft_u , LeftVentricularApex_theta_u , LeftVentricularApex_v_O_u ; // and their updates
static double  LeftVentricularApex_vx_init , LeftVentricularApex_vy_init , LeftVentricularApex_vz_init , LeftVentricularApex_g_init , LeftVentricularApex_v_init , LeftVentricularApex_ft_init , LeftVentricularApex_theta_init , LeftVentricularApex_v_O_init ; // and their inits
static double  slope_LeftVentricularApex_vx , slope_LeftVentricularApex_vy , slope_LeftVentricularApex_vz , slope_LeftVentricularApex_g , slope_LeftVentricularApex_v , slope_LeftVentricularApex_ft , slope_LeftVentricularApex_theta , slope_LeftVentricularApex_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { LeftVentricularApex_t1 , LeftVentricularApex_t2 , LeftVentricularApex_t3 , LeftVentricularApex_t4 }; // state declarations

enum states LeftVentricularApex (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( LeftVentricularApex_t1 ):
    if (True == False) {;}
    else if  (LeftVentricularApex_g > (44.5)) {
      LeftVentricularApex_vx_u = (0.3 * LeftVentricularApex_v) ;
      LeftVentricularApex_vy_u = 0 ;
      LeftVentricularApex_vz_u = (0.7 * LeftVentricularApex_v) ;
      LeftVentricularApex_g_u = ((((((((LeftVentricularApex_v_i_0 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularApex_v_i_1 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((LeftVentricularApex_v_i_2 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((LeftVentricularApex_v_i_3 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) ;
      LeftVentricularApex_theta_u = (LeftVentricularApex_v / 30.0) ;
      LeftVentricularApex_v_O_u = (131.1 + (- (80.1 * pow ( ((LeftVentricularApex_v / 30.0)) , (0.5) )))) ;
      LeftVentricularApex_ft_u = f (LeftVentricularApex_theta,4.0e-2) ;
      cstate =  LeftVentricularApex_t2 ;
      force_init_update = False;
    }

    else if ( LeftVentricularApex_v <= (44.5)
               && 
              LeftVentricularApex_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularApex_vx_init = LeftVentricularApex_vx ;
      slope_LeftVentricularApex_vx = (LeftVentricularApex_vx * -8.7) ;
      LeftVentricularApex_vx_u = (slope_LeftVentricularApex_vx * d) + LeftVentricularApex_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricularApex_vy_init = LeftVentricularApex_vy ;
      slope_LeftVentricularApex_vy = (LeftVentricularApex_vy * -190.9) ;
      LeftVentricularApex_vy_u = (slope_LeftVentricularApex_vy * d) + LeftVentricularApex_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricularApex_vz_init = LeftVentricularApex_vz ;
      slope_LeftVentricularApex_vz = (LeftVentricularApex_vz * -190.4) ;
      LeftVentricularApex_vz_u = (slope_LeftVentricularApex_vz * d) + LeftVentricularApex_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricularApex_t1 ;
      force_init_update = False;
      LeftVentricularApex_g_u = ((((((((LeftVentricularApex_v_i_0 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularApex_v_i_1 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((LeftVentricularApex_v_i_2 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((LeftVentricularApex_v_i_3 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) ;
      LeftVentricularApex_v_u = ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz) ;
      LeftVentricularApex_voo = ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz) ;
      LeftVentricularApex_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularApex!\n");
      exit(1);
    }
    break;
  case ( LeftVentricularApex_t2 ):
    if (True == False) {;}
    else if  (LeftVentricularApex_v >= (44.5)) {
      LeftVentricularApex_vx_u = LeftVentricularApex_vx ;
      LeftVentricularApex_vy_u = LeftVentricularApex_vy ;
      LeftVentricularApex_vz_u = LeftVentricularApex_vz ;
      LeftVentricularApex_g_u = ((((((((LeftVentricularApex_v_i_0 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularApex_v_i_1 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((LeftVentricularApex_v_i_2 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((LeftVentricularApex_v_i_3 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) ;
      cstate =  LeftVentricularApex_t3 ;
      force_init_update = False;
    }
    else if  (LeftVentricularApex_g <= (44.5)
               && 
              LeftVentricularApex_v < (44.5)) {
      LeftVentricularApex_vx_u = LeftVentricularApex_vx ;
      LeftVentricularApex_vy_u = LeftVentricularApex_vy ;
      LeftVentricularApex_vz_u = LeftVentricularApex_vz ;
      LeftVentricularApex_g_u = ((((((((LeftVentricularApex_v_i_0 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularApex_v_i_1 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((LeftVentricularApex_v_i_2 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((LeftVentricularApex_v_i_3 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) ;
      cstate =  LeftVentricularApex_t1 ;
      force_init_update = False;
    }

    else if ( LeftVentricularApex_v < (44.5)
               && 
              LeftVentricularApex_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularApex_vx_init = LeftVentricularApex_vx ;
      slope_LeftVentricularApex_vx = ((LeftVentricularApex_vx * -23.6) + (777200.0 * LeftVentricularApex_g)) ;
      LeftVentricularApex_vx_u = (slope_LeftVentricularApex_vx * d) + LeftVentricularApex_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricularApex_vy_init = LeftVentricularApex_vy ;
      slope_LeftVentricularApex_vy = ((LeftVentricularApex_vy * -45.5) + (58900.0 * LeftVentricularApex_g)) ;
      LeftVentricularApex_vy_u = (slope_LeftVentricularApex_vy * d) + LeftVentricularApex_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricularApex_vz_init = LeftVentricularApex_vz ;
      slope_LeftVentricularApex_vz = ((LeftVentricularApex_vz * -12.9) + (276600.0 * LeftVentricularApex_g)) ;
      LeftVentricularApex_vz_u = (slope_LeftVentricularApex_vz * d) + LeftVentricularApex_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricularApex_t2 ;
      force_init_update = False;
      LeftVentricularApex_g_u = ((((((((LeftVentricularApex_v_i_0 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularApex_v_i_1 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((LeftVentricularApex_v_i_2 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((LeftVentricularApex_v_i_3 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) ;
      LeftVentricularApex_v_u = ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz) ;
      LeftVentricularApex_voo = ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz) ;
      LeftVentricularApex_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularApex!\n");
      exit(1);
    }
    break;
  case ( LeftVentricularApex_t3 ):
    if (True == False) {;}
    else if  (LeftVentricularApex_v >= (131.1)) {
      LeftVentricularApex_vx_u = LeftVentricularApex_vx ;
      LeftVentricularApex_vy_u = LeftVentricularApex_vy ;
      LeftVentricularApex_vz_u = LeftVentricularApex_vz ;
      LeftVentricularApex_g_u = ((((((((LeftVentricularApex_v_i_0 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularApex_v_i_1 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((LeftVentricularApex_v_i_2 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((LeftVentricularApex_v_i_3 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) ;
      cstate =  LeftVentricularApex_t4 ;
      force_init_update = False;
    }

    else if ( LeftVentricularApex_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularApex_vx_init = LeftVentricularApex_vx ;
      slope_LeftVentricularApex_vx = (LeftVentricularApex_vx * -6.9) ;
      LeftVentricularApex_vx_u = (slope_LeftVentricularApex_vx * d) + LeftVentricularApex_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricularApex_vy_init = LeftVentricularApex_vy ;
      slope_LeftVentricularApex_vy = (LeftVentricularApex_vy * 75.9) ;
      LeftVentricularApex_vy_u = (slope_LeftVentricularApex_vy * d) + LeftVentricularApex_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricularApex_vz_init = LeftVentricularApex_vz ;
      slope_LeftVentricularApex_vz = (LeftVentricularApex_vz * 6826.5) ;
      LeftVentricularApex_vz_u = (slope_LeftVentricularApex_vz * d) + LeftVentricularApex_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricularApex_t3 ;
      force_init_update = False;
      LeftVentricularApex_g_u = ((((((((LeftVentricularApex_v_i_0 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularApex_v_i_1 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((LeftVentricularApex_v_i_2 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((LeftVentricularApex_v_i_3 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) ;
      LeftVentricularApex_v_u = ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz) ;
      LeftVentricularApex_voo = ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz) ;
      LeftVentricularApex_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularApex!\n");
      exit(1);
    }
    break;
  case ( LeftVentricularApex_t4 ):
    if (True == False) {;}
    else if  (LeftVentricularApex_v <= (30.0)) {
      LeftVentricularApex_vx_u = LeftVentricularApex_vx ;
      LeftVentricularApex_vy_u = LeftVentricularApex_vy ;
      LeftVentricularApex_vz_u = LeftVentricularApex_vz ;
      LeftVentricularApex_g_u = ((((((((LeftVentricularApex_v_i_0 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularApex_v_i_1 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((LeftVentricularApex_v_i_2 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((LeftVentricularApex_v_i_3 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) ;
      cstate =  LeftVentricularApex_t1 ;
      force_init_update = False;
    }

    else if ( LeftVentricularApex_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularApex_vx_init = LeftVentricularApex_vx ;
      slope_LeftVentricularApex_vx = (LeftVentricularApex_vx * -33.2) ;
      LeftVentricularApex_vx_u = (slope_LeftVentricularApex_vx * d) + LeftVentricularApex_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricularApex_vy_init = LeftVentricularApex_vy ;
      slope_LeftVentricularApex_vy = ((LeftVentricularApex_vy * 11.0) * LeftVentricularApex_ft) ;
      LeftVentricularApex_vy_u = (slope_LeftVentricularApex_vy * d) + LeftVentricularApex_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricularApex_vz_init = LeftVentricularApex_vz ;
      slope_LeftVentricularApex_vz = ((LeftVentricularApex_vz * 2.0) * LeftVentricularApex_ft) ;
      LeftVentricularApex_vz_u = (slope_LeftVentricularApex_vz * d) + LeftVentricularApex_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricularApex_t4 ;
      force_init_update = False;
      LeftVentricularApex_g_u = ((((((((LeftVentricularApex_v_i_0 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularApex_v_i_1 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((LeftVentricularApex_v_i_2 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((LeftVentricularApex_v_i_3 + (- ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) ;
      LeftVentricularApex_v_u = ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz) ;
      LeftVentricularApex_voo = ((LeftVentricularApex_vx + (- LeftVentricularApex_vy)) + LeftVentricularApex_vz) ;
      LeftVentricularApex_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularApex!\n");
      exit(1);
    }
    break;
  }
  LeftVentricularApex_vx = LeftVentricularApex_vx_u;
  LeftVentricularApex_vy = LeftVentricularApex_vy_u;
  LeftVentricularApex_vz = LeftVentricularApex_vz_u;
  LeftVentricularApex_g = LeftVentricularApex_g_u;
  LeftVentricularApex_v = LeftVentricularApex_v_u;
  LeftVentricularApex_ft = LeftVentricularApex_ft_u;
  LeftVentricularApex_theta = LeftVentricularApex_theta_u;
  LeftVentricularApex_v_O = LeftVentricularApex_v_O_u;
  return cstate;
}