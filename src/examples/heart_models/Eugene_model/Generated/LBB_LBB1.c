#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double LBB_LBB1_update_c1vd();
extern double LBB_LBB1_update_c2vd();
extern double LBB_LBB1_update_c1md();
extern double LBB_LBB1_update_c2md();
extern double LBB_LBB1_update_buffer_index(double,double,double,double);
extern double LBB_LBB1_update_latch1(double,double);
extern double LBB_LBB1_update_latch2(double,double);
extern double LBB_LBB1_update_ocell1(double,double);
extern double LBB_LBB1_update_ocell2(double,double);
double LBB_LBB1_cell1_v;
double LBB_LBB1_cell1_mode;
double LBB_LBB1_cell2_v;
double LBB_LBB1_cell2_mode;
double LBB_LBB1_cell1_v_replay = 0.0;
double LBB_LBB1_cell2_v_replay = 0.0;


static double  LBB_LBB1_k  =  0.0 ,  LBB_LBB1_cell1_mode_delayed  =  0.0 ,  LBB_LBB1_cell2_mode_delayed  =  0.0 ,  LBB_LBB1_from_cell  =  0.0 ,  LBB_LBB1_cell1_replay_latch  =  0.0 ,  LBB_LBB1_cell2_replay_latch  =  0.0 ,  LBB_LBB1_cell1_v_delayed  =  0.0 ,  LBB_LBB1_cell2_v_delayed  =  0.0 ,  LBB_LBB1_wasted  =  0.0 ; //the continuous vars
static double  LBB_LBB1_k_u , LBB_LBB1_cell1_mode_delayed_u , LBB_LBB1_cell2_mode_delayed_u , LBB_LBB1_from_cell_u , LBB_LBB1_cell1_replay_latch_u , LBB_LBB1_cell2_replay_latch_u , LBB_LBB1_cell1_v_delayed_u , LBB_LBB1_cell2_v_delayed_u , LBB_LBB1_wasted_u ; // and their updates
static double  LBB_LBB1_k_init , LBB_LBB1_cell1_mode_delayed_init , LBB_LBB1_cell2_mode_delayed_init , LBB_LBB1_from_cell_init , LBB_LBB1_cell1_replay_latch_init , LBB_LBB1_cell2_replay_latch_init , LBB_LBB1_cell1_v_delayed_init , LBB_LBB1_cell2_v_delayed_init , LBB_LBB1_wasted_init ; // and their inits
static double  slope_LBB_LBB1_k , slope_LBB_LBB1_cell1_mode_delayed , slope_LBB_LBB1_cell2_mode_delayed , slope_LBB_LBB1_from_cell , slope_LBB_LBB1_cell1_replay_latch , slope_LBB_LBB1_cell2_replay_latch , slope_LBB_LBB1_cell1_v_delayed , slope_LBB_LBB1_cell2_v_delayed , slope_LBB_LBB1_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { LBB_LBB1_idle , LBB_LBB1_annhilate , LBB_LBB1_previous_drection1 , LBB_LBB1_previous_direction2 , LBB_LBB1_wait_cell1 , LBB_LBB1_replay_cell1 , LBB_LBB1_replay_cell2 , LBB_LBB1_wait_cell2 }; // state declarations

enum states LBB_LBB1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( LBB_LBB1_idle ):
    if (True == False) {;}
    else if  (LBB_LBB1_cell2_mode == (2.0) && (LBB_LBB1_cell1_mode != (2.0))) {
      LBB_LBB1_k_u = 1 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
      cstate =  LBB_LBB1_previous_direction2 ;
      force_init_update = False;
    }
    else if  (LBB_LBB1_cell1_mode == (2.0) && (LBB_LBB1_cell2_mode != (2.0))) {
      LBB_LBB1_k_u = 1 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
      cstate =  LBB_LBB1_previous_drection1 ;
      force_init_update = False;
    }
    else if  (LBB_LBB1_cell1_mode == (2.0) && (LBB_LBB1_cell2_mode == (2.0))) {
      LBB_LBB1_k_u = 1 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
      cstate =  LBB_LBB1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LBB_LBB1_k_init = LBB_LBB1_k ;
      slope_LBB_LBB1_k = 1 ;
      LBB_LBB1_k_u = (slope_LBB_LBB1_k * d) + LBB_LBB1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LBB_LBB1_idle ;
      force_init_update = False;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell1_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB_LBB1!\n");
      exit(1);
    }
    break;
  case ( LBB_LBB1_annhilate ):
    if (True == False) {;}
    else if  (LBB_LBB1_cell1_mode != (2.0) && (LBB_LBB1_cell2_mode != (2.0))) {
      LBB_LBB1_k_u = 1 ;
      LBB_LBB1_from_cell_u = 0 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
      cstate =  LBB_LBB1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LBB_LBB1_k_init = LBB_LBB1_k ;
      slope_LBB_LBB1_k = 1 ;
      LBB_LBB1_k_u = (slope_LBB_LBB1_k * d) + LBB_LBB1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LBB_LBB1_annhilate ;
      force_init_update = False;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell1_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB_LBB1!\n");
      exit(1);
    }
    break;
  case ( LBB_LBB1_previous_drection1 ):
    if (True == False) {;}
    else if  (LBB_LBB1_from_cell == (1.0)) {
      LBB_LBB1_k_u = 1 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
      cstate =  LBB_LBB1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LBB_LBB1_from_cell == (0.0)) {
      LBB_LBB1_k_u = 1 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
      cstate =  LBB_LBB1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LBB_LBB1_from_cell == (2.0) && (LBB_LBB1_cell2_mode_delayed == (0.0))) {
      LBB_LBB1_k_u = 1 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
      cstate =  LBB_LBB1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LBB_LBB1_from_cell == (2.0) && (LBB_LBB1_cell2_mode_delayed != (0.0))) {
      LBB_LBB1_k_u = 1 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
      cstate =  LBB_LBB1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LBB_LBB1_k_init = LBB_LBB1_k ;
      slope_LBB_LBB1_k = 1 ;
      LBB_LBB1_k_u = (slope_LBB_LBB1_k * d) + LBB_LBB1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LBB_LBB1_previous_drection1 ;
      force_init_update = False;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell1_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB_LBB1!\n");
      exit(1);
    }
    break;
  case ( LBB_LBB1_previous_direction2 ):
    if (True == False) {;}
    else if  (LBB_LBB1_from_cell == (1.0) && (LBB_LBB1_cell1_mode_delayed != (0.0))) {
      LBB_LBB1_k_u = 1 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
      cstate =  LBB_LBB1_annhilate ;
      force_init_update = False;
    }
    else if  (LBB_LBB1_from_cell == (2.0)) {
      LBB_LBB1_k_u = 1 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
      cstate =  LBB_LBB1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (LBB_LBB1_from_cell == (0.0)) {
      LBB_LBB1_k_u = 1 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
      cstate =  LBB_LBB1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (LBB_LBB1_from_cell == (1.0) && (LBB_LBB1_cell1_mode_delayed == (0.0))) {
      LBB_LBB1_k_u = 1 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
      cstate =  LBB_LBB1_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LBB_LBB1_k_init = LBB_LBB1_k ;
      slope_LBB_LBB1_k = 1 ;
      LBB_LBB1_k_u = (slope_LBB_LBB1_k * d) + LBB_LBB1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LBB_LBB1_previous_direction2 ;
      force_init_update = False;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell1_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB_LBB1!\n");
      exit(1);
    }
    break;
  case ( LBB_LBB1_wait_cell1 ):
    if (True == False) {;}
    else if  (LBB_LBB1_cell2_mode == (2.0)) {
      LBB_LBB1_k_u = 1 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
      cstate =  LBB_LBB1_annhilate ;
      force_init_update = False;
    }
    else if  (LBB_LBB1_k >= (5.0)) {
      LBB_LBB1_from_cell_u = 1 ;
      LBB_LBB1_cell1_replay_latch_u = 1 ;
      LBB_LBB1_k_u = 1 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
      cstate =  LBB_LBB1_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LBB_LBB1_k_init = LBB_LBB1_k ;
      slope_LBB_LBB1_k = 1 ;
      LBB_LBB1_k_u = (slope_LBB_LBB1_k * d) + LBB_LBB1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LBB_LBB1_wait_cell1 ;
      force_init_update = False;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell1_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB_LBB1!\n");
      exit(1);
    }
    break;
  case ( LBB_LBB1_replay_cell1 ):
    if (True == False) {;}
    else if  (LBB_LBB1_cell1_mode == (2.0)) {
      LBB_LBB1_k_u = 1 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
      cstate =  LBB_LBB1_annhilate ;
      force_init_update = False;
    }
    else if  (LBB_LBB1_k >= (5.0)) {
      LBB_LBB1_from_cell_u = 2 ;
      LBB_LBB1_cell2_replay_latch_u = 1 ;
      LBB_LBB1_k_u = 1 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
      cstate =  LBB_LBB1_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LBB_LBB1_k_init = LBB_LBB1_k ;
      slope_LBB_LBB1_k = 1 ;
      LBB_LBB1_k_u = (slope_LBB_LBB1_k * d) + LBB_LBB1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LBB_LBB1_replay_cell1 ;
      force_init_update = False;
      LBB_LBB1_cell1_replay_latch_u = 1 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell1_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB_LBB1!\n");
      exit(1);
    }
    break;
  case ( LBB_LBB1_replay_cell2 ):
    if (True == False) {;}
    else if  (LBB_LBB1_k >= (10.0)) {
      LBB_LBB1_k_u = 1 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
      cstate =  LBB_LBB1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LBB_LBB1_k_init = LBB_LBB1_k ;
      slope_LBB_LBB1_k = 1 ;
      LBB_LBB1_k_u = (slope_LBB_LBB1_k * d) + LBB_LBB1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LBB_LBB1_replay_cell2 ;
      force_init_update = False;
      LBB_LBB1_cell2_replay_latch_u = 1 ;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell1_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB_LBB1!\n");
      exit(1);
    }
    break;
  case ( LBB_LBB1_wait_cell2 ):
    if (True == False) {;}
    else if  (LBB_LBB1_k >= (10.0)) {
      LBB_LBB1_k_u = 1 ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
      cstate =  LBB_LBB1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LBB_LBB1_k_init = LBB_LBB1_k ;
      slope_LBB_LBB1_k = 1 ;
      LBB_LBB1_k_u = (slope_LBB_LBB1_k * d) + LBB_LBB1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LBB_LBB1_wait_cell2 ;
      force_init_update = False;
      LBB_LBB1_cell1_v_delayed_u = LBB_LBB1_update_c1vd () ;
      LBB_LBB1_cell2_v_delayed_u = LBB_LBB1_update_c2vd () ;
      LBB_LBB1_cell1_mode_delayed_u = LBB_LBB1_update_c1md () ;
      LBB_LBB1_cell2_mode_delayed_u = LBB_LBB1_update_c2md () ;
      LBB_LBB1_wasted_u = LBB_LBB1_update_buffer_index (LBB_LBB1_cell1_v,LBB_LBB1_cell2_v,LBB_LBB1_cell1_mode,LBB_LBB1_cell2_mode) ;
      LBB_LBB1_cell1_replay_latch_u = LBB_LBB1_update_latch1 (LBB_LBB1_cell1_mode_delayed,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_replay_latch_u = LBB_LBB1_update_latch2 (LBB_LBB1_cell2_mode_delayed,LBB_LBB1_cell2_replay_latch_u) ;
      LBB_LBB1_cell1_v_replay = LBB_LBB1_update_ocell1 (LBB_LBB1_cell1_v_delayed_u,LBB_LBB1_cell1_replay_latch_u) ;
      LBB_LBB1_cell2_v_replay = LBB_LBB1_update_ocell2 (LBB_LBB1_cell2_v_delayed_u,LBB_LBB1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB_LBB1!\n");
      exit(1);
    }
    break;
  }
  LBB_LBB1_k = LBB_LBB1_k_u;
  LBB_LBB1_cell1_mode_delayed = LBB_LBB1_cell1_mode_delayed_u;
  LBB_LBB1_cell2_mode_delayed = LBB_LBB1_cell2_mode_delayed_u;
  LBB_LBB1_from_cell = LBB_LBB1_from_cell_u;
  LBB_LBB1_cell1_replay_latch = LBB_LBB1_cell1_replay_latch_u;
  LBB_LBB1_cell2_replay_latch = LBB_LBB1_cell2_replay_latch_u;
  LBB_LBB1_cell1_v_delayed = LBB_LBB1_cell1_v_delayed_u;
  LBB_LBB1_cell2_v_delayed = LBB_LBB1_cell2_v_delayed_u;
  LBB_LBB1_wasted = LBB_LBB1_wasted_u;
  return cstate;
}