#include<stdlib.h>
#define SA_BB_PC1NI ((int)20.0)
#define SA_BB_PC2NI ((int)20.0)
static double* SA_BB_cell1_v_buffer;
static double* SA_BB_cell2_v_buffer;
static unsigned char* SA_BB_cell1_mode_buffer;
static unsigned char* SA_BB_cell2_mode_buffer;
static unsigned int SA_BB_cell1_buffer_index;
static unsigned int SA_BB_cell2_buffer_index;
void SA_BB_init() {
  SA_BB_cell1_v_buffer = (double*)calloc(SA_BB_PC1NI, sizeof(double));
  SA_BB_cell1_mode_buffer = (unsigned char*)calloc(SA_BB_PC1NI, sizeof(unsigned char));
  SA_BB_cell2_v_buffer = (double*)calloc(SA_BB_PC2NI, sizeof(double));
  SA_BB_cell2_mode_buffer = (unsigned char*)calloc(SA_BB_PC2NI, sizeof(unsigned char));
  SA_BB_cell1_buffer_index = 0;
  SA_BB_cell2_buffer_index = 0;
}
double SA_BB_update_c1vd() {
  return SA_BB_cell1_v_buffer[SA_BB_cell1_buffer_index];
}
double SA_BB_update_c2vd() {
  return SA_BB_cell2_v_buffer[SA_BB_cell2_buffer_index];
}
double SA_BB_update_c1md() {
  return SA_BB_cell1_mode_buffer[SA_BB_cell1_buffer_index];
}
double SA_BB_update_c2md() {
  return SA_BB_cell2_mode_buffer[SA_BB_cell2_buffer_index];
}
double SA_BB_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  SA_BB_cell1_v_buffer[SA_BB_cell1_buffer_index] = c1v;
  SA_BB_cell2_v_buffer[SA_BB_cell2_buffer_index] = c2v;
  SA_BB_cell1_mode_buffer[SA_BB_cell1_buffer_index] = c1m;
  SA_BB_cell2_mode_buffer[SA_BB_cell2_buffer_index] = c2m;
  SA_BB_cell1_buffer_index = ((SA_BB_cell1_buffer_index + 1) >= SA_BB_PC1NI) ? 0 : ++SA_BB_cell1_buffer_index;
  SA_BB_cell2_buffer_index = ((SA_BB_cell2_buffer_index + 1) >= SA_BB_PC2NI) ? 0 : ++SA_BB_cell2_buffer_index;
  return 0.0;
}
double SA_BB_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double SA_BB_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double SA_BB_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double SA_BB_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define SA_OS_PC1NI ((int)20.0)
#define SA_OS_PC2NI ((int)20.0)
static double* SA_OS_cell1_v_buffer;
static double* SA_OS_cell2_v_buffer;
static unsigned char* SA_OS_cell1_mode_buffer;
static unsigned char* SA_OS_cell2_mode_buffer;
static unsigned int SA_OS_cell1_buffer_index;
static unsigned int SA_OS_cell2_buffer_index;
void SA_OS_init() {
  SA_OS_cell1_v_buffer = (double*)calloc(SA_OS_PC1NI, sizeof(double));
  SA_OS_cell1_mode_buffer = (unsigned char*)calloc(SA_OS_PC1NI, sizeof(unsigned char));
  SA_OS_cell2_v_buffer = (double*)calloc(SA_OS_PC2NI, sizeof(double));
  SA_OS_cell2_mode_buffer = (unsigned char*)calloc(SA_OS_PC2NI, sizeof(unsigned char));
  SA_OS_cell1_buffer_index = 0;
  SA_OS_cell2_buffer_index = 0;
}
double SA_OS_update_c1vd() {
  return SA_OS_cell1_v_buffer[SA_OS_cell1_buffer_index];
}
double SA_OS_update_c2vd() {
  return SA_OS_cell2_v_buffer[SA_OS_cell2_buffer_index];
}
double SA_OS_update_c1md() {
  return SA_OS_cell1_mode_buffer[SA_OS_cell1_buffer_index];
}
double SA_OS_update_c2md() {
  return SA_OS_cell2_mode_buffer[SA_OS_cell2_buffer_index];
}
double SA_OS_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  SA_OS_cell1_v_buffer[SA_OS_cell1_buffer_index] = c1v;
  SA_OS_cell2_v_buffer[SA_OS_cell2_buffer_index] = c2v;
  SA_OS_cell1_mode_buffer[SA_OS_cell1_buffer_index] = c1m;
  SA_OS_cell2_mode_buffer[SA_OS_cell2_buffer_index] = c2m;
  SA_OS_cell1_buffer_index = ((SA_OS_cell1_buffer_index + 1) >= SA_OS_PC1NI) ? 0 : ++SA_OS_cell1_buffer_index;
  SA_OS_cell2_buffer_index = ((SA_OS_cell2_buffer_index + 1) >= SA_OS_PC2NI) ? 0 : ++SA_OS_cell2_buffer_index;
  return 0.0;
}
double SA_OS_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double SA_OS_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double SA_OS_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double SA_OS_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define SA_RA_PC1NI ((int)20.0)
#define SA_RA_PC2NI ((int)20.0)
static double* SA_RA_cell1_v_buffer;
static double* SA_RA_cell2_v_buffer;
static unsigned char* SA_RA_cell1_mode_buffer;
static unsigned char* SA_RA_cell2_mode_buffer;
static unsigned int SA_RA_cell1_buffer_index;
static unsigned int SA_RA_cell2_buffer_index;
void SA_RA_init() {
  SA_RA_cell1_v_buffer = (double*)calloc(SA_RA_PC1NI, sizeof(double));
  SA_RA_cell1_mode_buffer = (unsigned char*)calloc(SA_RA_PC1NI, sizeof(unsigned char));
  SA_RA_cell2_v_buffer = (double*)calloc(SA_RA_PC2NI, sizeof(double));
  SA_RA_cell2_mode_buffer = (unsigned char*)calloc(SA_RA_PC2NI, sizeof(unsigned char));
  SA_RA_cell1_buffer_index = 0;
  SA_RA_cell2_buffer_index = 0;
}
double SA_RA_update_c1vd() {
  return SA_RA_cell1_v_buffer[SA_RA_cell1_buffer_index];
}
double SA_RA_update_c2vd() {
  return SA_RA_cell2_v_buffer[SA_RA_cell2_buffer_index];
}
double SA_RA_update_c1md() {
  return SA_RA_cell1_mode_buffer[SA_RA_cell1_buffer_index];
}
double SA_RA_update_c2md() {
  return SA_RA_cell2_mode_buffer[SA_RA_cell2_buffer_index];
}
double SA_RA_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  SA_RA_cell1_v_buffer[SA_RA_cell1_buffer_index] = c1v;
  SA_RA_cell2_v_buffer[SA_RA_cell2_buffer_index] = c2v;
  SA_RA_cell1_mode_buffer[SA_RA_cell1_buffer_index] = c1m;
  SA_RA_cell2_mode_buffer[SA_RA_cell2_buffer_index] = c2m;
  SA_RA_cell1_buffer_index = ((SA_RA_cell1_buffer_index + 1) >= SA_RA_PC1NI) ? 0 : ++SA_RA_cell1_buffer_index;
  SA_RA_cell2_buffer_index = ((SA_RA_cell2_buffer_index + 1) >= SA_RA_PC2NI) ? 0 : ++SA_RA_cell2_buffer_index;
  return 0.0;
}
double SA_RA_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double SA_RA_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double SA_RA_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double SA_RA_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define SA_CT_PC1NI ((int)20.0)
#define SA_CT_PC2NI ((int)20.0)
static double* SA_CT_cell1_v_buffer;
static double* SA_CT_cell2_v_buffer;
static unsigned char* SA_CT_cell1_mode_buffer;
static unsigned char* SA_CT_cell2_mode_buffer;
static unsigned int SA_CT_cell1_buffer_index;
static unsigned int SA_CT_cell2_buffer_index;
void SA_CT_init() {
  SA_CT_cell1_v_buffer = (double*)calloc(SA_CT_PC1NI, sizeof(double));
  SA_CT_cell1_mode_buffer = (unsigned char*)calloc(SA_CT_PC1NI, sizeof(unsigned char));
  SA_CT_cell2_v_buffer = (double*)calloc(SA_CT_PC2NI, sizeof(double));
  SA_CT_cell2_mode_buffer = (unsigned char*)calloc(SA_CT_PC2NI, sizeof(unsigned char));
  SA_CT_cell1_buffer_index = 0;
  SA_CT_cell2_buffer_index = 0;
}
double SA_CT_update_c1vd() {
  return SA_CT_cell1_v_buffer[SA_CT_cell1_buffer_index];
}
double SA_CT_update_c2vd() {
  return SA_CT_cell2_v_buffer[SA_CT_cell2_buffer_index];
}
double SA_CT_update_c1md() {
  return SA_CT_cell1_mode_buffer[SA_CT_cell1_buffer_index];
}
double SA_CT_update_c2md() {
  return SA_CT_cell2_mode_buffer[SA_CT_cell2_buffer_index];
}
double SA_CT_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  SA_CT_cell1_v_buffer[SA_CT_cell1_buffer_index] = c1v;
  SA_CT_cell2_v_buffer[SA_CT_cell2_buffer_index] = c2v;
  SA_CT_cell1_mode_buffer[SA_CT_cell1_buffer_index] = c1m;
  SA_CT_cell2_mode_buffer[SA_CT_cell2_buffer_index] = c2m;
  SA_CT_cell1_buffer_index = ((SA_CT_cell1_buffer_index + 1) >= SA_CT_PC1NI) ? 0 : ++SA_CT_cell1_buffer_index;
  SA_CT_cell2_buffer_index = ((SA_CT_cell2_buffer_index + 1) >= SA_CT_PC2NI) ? 0 : ++SA_CT_cell2_buffer_index;
  return 0.0;
}
double SA_CT_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double SA_CT_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double SA_CT_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double SA_CT_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define BB_LA_PC1NI ((int)30.0)
#define BB_LA_PC2NI ((int)30.0)
static double* BB_LA_cell1_v_buffer;
static double* BB_LA_cell2_v_buffer;
static unsigned char* BB_LA_cell1_mode_buffer;
static unsigned char* BB_LA_cell2_mode_buffer;
static unsigned int BB_LA_cell1_buffer_index;
static unsigned int BB_LA_cell2_buffer_index;
void BB_LA_init() {
  BB_LA_cell1_v_buffer = (double*)calloc(BB_LA_PC1NI, sizeof(double));
  BB_LA_cell1_mode_buffer = (unsigned char*)calloc(BB_LA_PC1NI, sizeof(unsigned char));
  BB_LA_cell2_v_buffer = (double*)calloc(BB_LA_PC2NI, sizeof(double));
  BB_LA_cell2_mode_buffer = (unsigned char*)calloc(BB_LA_PC2NI, sizeof(unsigned char));
  BB_LA_cell1_buffer_index = 0;
  BB_LA_cell2_buffer_index = 0;
}
double BB_LA_update_c1vd() {
  return BB_LA_cell1_v_buffer[BB_LA_cell1_buffer_index];
}
double BB_LA_update_c2vd() {
  return BB_LA_cell2_v_buffer[BB_LA_cell2_buffer_index];
}
double BB_LA_update_c1md() {
  return BB_LA_cell1_mode_buffer[BB_LA_cell1_buffer_index];
}
double BB_LA_update_c2md() {
  return BB_LA_cell2_mode_buffer[BB_LA_cell2_buffer_index];
}
double BB_LA_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  BB_LA_cell1_v_buffer[BB_LA_cell1_buffer_index] = c1v;
  BB_LA_cell2_v_buffer[BB_LA_cell2_buffer_index] = c2v;
  BB_LA_cell1_mode_buffer[BB_LA_cell1_buffer_index] = c1m;
  BB_LA_cell2_mode_buffer[BB_LA_cell2_buffer_index] = c2m;
  BB_LA_cell1_buffer_index = ((BB_LA_cell1_buffer_index + 1) >= BB_LA_PC1NI) ? 0 : ++BB_LA_cell1_buffer_index;
  BB_LA_cell2_buffer_index = ((BB_LA_cell2_buffer_index + 1) >= BB_LA_PC2NI) ? 0 : ++BB_LA_cell2_buffer_index;
  return 0.0;
}
double BB_LA_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double BB_LA_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double BB_LA_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double BB_LA_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define LA_LA1_PC1NI ((int)40.0)
#define LA_LA1_PC2NI ((int)40.0)
static double* LA_LA1_cell1_v_buffer;
static double* LA_LA1_cell2_v_buffer;
static unsigned char* LA_LA1_cell1_mode_buffer;
static unsigned char* LA_LA1_cell2_mode_buffer;
static unsigned int LA_LA1_cell1_buffer_index;
static unsigned int LA_LA1_cell2_buffer_index;
void LA_LA1_init() {
  LA_LA1_cell1_v_buffer = (double*)calloc(LA_LA1_PC1NI, sizeof(double));
  LA_LA1_cell1_mode_buffer = (unsigned char*)calloc(LA_LA1_PC1NI, sizeof(unsigned char));
  LA_LA1_cell2_v_buffer = (double*)calloc(LA_LA1_PC2NI, sizeof(double));
  LA_LA1_cell2_mode_buffer = (unsigned char*)calloc(LA_LA1_PC2NI, sizeof(unsigned char));
  LA_LA1_cell1_buffer_index = 0;
  LA_LA1_cell2_buffer_index = 0;
}
double LA_LA1_update_c1vd() {
  return LA_LA1_cell1_v_buffer[LA_LA1_cell1_buffer_index];
}
double LA_LA1_update_c2vd() {
  return LA_LA1_cell2_v_buffer[LA_LA1_cell2_buffer_index];
}
double LA_LA1_update_c1md() {
  return LA_LA1_cell1_mode_buffer[LA_LA1_cell1_buffer_index];
}
double LA_LA1_update_c2md() {
  return LA_LA1_cell2_mode_buffer[LA_LA1_cell2_buffer_index];
}
double LA_LA1_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  LA_LA1_cell1_v_buffer[LA_LA1_cell1_buffer_index] = c1v;
  LA_LA1_cell2_v_buffer[LA_LA1_cell2_buffer_index] = c2v;
  LA_LA1_cell1_mode_buffer[LA_LA1_cell1_buffer_index] = c1m;
  LA_LA1_cell2_mode_buffer[LA_LA1_cell2_buffer_index] = c2m;
  LA_LA1_cell1_buffer_index = ((LA_LA1_cell1_buffer_index + 1) >= LA_LA1_PC1NI) ? 0 : ++LA_LA1_cell1_buffer_index;
  LA_LA1_cell2_buffer_index = ((LA_LA1_cell2_buffer_index + 1) >= LA_LA1_PC2NI) ? 0 : ++LA_LA1_cell2_buffer_index;
  return 0.0;
}
double LA_LA1_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double LA_LA1_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double LA_LA1_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double LA_LA1_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define RA_RA1_PC1NI ((int)20.0)
#define RA_RA1_PC2NI ((int)20.0)
static double* RA_RA1_cell1_v_buffer;
static double* RA_RA1_cell2_v_buffer;
static unsigned char* RA_RA1_cell1_mode_buffer;
static unsigned char* RA_RA1_cell2_mode_buffer;
static unsigned int RA_RA1_cell1_buffer_index;
static unsigned int RA_RA1_cell2_buffer_index;
void RA_RA1_init() {
  RA_RA1_cell1_v_buffer = (double*)calloc(RA_RA1_PC1NI, sizeof(double));
  RA_RA1_cell1_mode_buffer = (unsigned char*)calloc(RA_RA1_PC1NI, sizeof(unsigned char));
  RA_RA1_cell2_v_buffer = (double*)calloc(RA_RA1_PC2NI, sizeof(double));
  RA_RA1_cell2_mode_buffer = (unsigned char*)calloc(RA_RA1_PC2NI, sizeof(unsigned char));
  RA_RA1_cell1_buffer_index = 0;
  RA_RA1_cell2_buffer_index = 0;
}
double RA_RA1_update_c1vd() {
  return RA_RA1_cell1_v_buffer[RA_RA1_cell1_buffer_index];
}
double RA_RA1_update_c2vd() {
  return RA_RA1_cell2_v_buffer[RA_RA1_cell2_buffer_index];
}
double RA_RA1_update_c1md() {
  return RA_RA1_cell1_mode_buffer[RA_RA1_cell1_buffer_index];
}
double RA_RA1_update_c2md() {
  return RA_RA1_cell2_mode_buffer[RA_RA1_cell2_buffer_index];
}
double RA_RA1_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  RA_RA1_cell1_v_buffer[RA_RA1_cell1_buffer_index] = c1v;
  RA_RA1_cell2_v_buffer[RA_RA1_cell2_buffer_index] = c2v;
  RA_RA1_cell1_mode_buffer[RA_RA1_cell1_buffer_index] = c1m;
  RA_RA1_cell2_mode_buffer[RA_RA1_cell2_buffer_index] = c2m;
  RA_RA1_cell1_buffer_index = ((RA_RA1_cell1_buffer_index + 1) >= RA_RA1_PC1NI) ? 0 : ++RA_RA1_cell1_buffer_index;
  RA_RA1_cell2_buffer_index = ((RA_RA1_cell2_buffer_index + 1) >= RA_RA1_PC2NI) ? 0 : ++RA_RA1_cell2_buffer_index;
  return 0.0;
}
double RA_RA1_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double RA_RA1_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double RA_RA1_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double RA_RA1_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define RA1_CS_PC1NI ((int)20.0)
#define RA1_CS_PC2NI ((int)20.0)
static double* RA1_CS_cell1_v_buffer;
static double* RA1_CS_cell2_v_buffer;
static unsigned char* RA1_CS_cell1_mode_buffer;
static unsigned char* RA1_CS_cell2_mode_buffer;
static unsigned int RA1_CS_cell1_buffer_index;
static unsigned int RA1_CS_cell2_buffer_index;
void RA1_CS_init() {
  RA1_CS_cell1_v_buffer = (double*)calloc(RA1_CS_PC1NI, sizeof(double));
  RA1_CS_cell1_mode_buffer = (unsigned char*)calloc(RA1_CS_PC1NI, sizeof(unsigned char));
  RA1_CS_cell2_v_buffer = (double*)calloc(RA1_CS_PC2NI, sizeof(double));
  RA1_CS_cell2_mode_buffer = (unsigned char*)calloc(RA1_CS_PC2NI, sizeof(unsigned char));
  RA1_CS_cell1_buffer_index = 0;
  RA1_CS_cell2_buffer_index = 0;
}
double RA1_CS_update_c1vd() {
  return RA1_CS_cell1_v_buffer[RA1_CS_cell1_buffer_index];
}
double RA1_CS_update_c2vd() {
  return RA1_CS_cell2_v_buffer[RA1_CS_cell2_buffer_index];
}
double RA1_CS_update_c1md() {
  return RA1_CS_cell1_mode_buffer[RA1_CS_cell1_buffer_index];
}
double RA1_CS_update_c2md() {
  return RA1_CS_cell2_mode_buffer[RA1_CS_cell2_buffer_index];
}
double RA1_CS_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  RA1_CS_cell1_v_buffer[RA1_CS_cell1_buffer_index] = c1v;
  RA1_CS_cell2_v_buffer[RA1_CS_cell2_buffer_index] = c2v;
  RA1_CS_cell1_mode_buffer[RA1_CS_cell1_buffer_index] = c1m;
  RA1_CS_cell2_mode_buffer[RA1_CS_cell2_buffer_index] = c2m;
  RA1_CS_cell1_buffer_index = ((RA1_CS_cell1_buffer_index + 1) >= RA1_CS_PC1NI) ? 0 : ++RA1_CS_cell1_buffer_index;
  RA1_CS_cell2_buffer_index = ((RA1_CS_cell2_buffer_index + 1) >= RA1_CS_PC2NI) ? 0 : ++RA1_CS_cell2_buffer_index;
  return 0.0;
}
double RA1_CS_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double RA1_CS_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double RA1_CS_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double RA1_CS_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define CT_CT1_PC1NI ((int)20.0)
#define CT_CT1_PC2NI ((int)20.0)
static double* CT_CT1_cell1_v_buffer;
static double* CT_CT1_cell2_v_buffer;
static unsigned char* CT_CT1_cell1_mode_buffer;
static unsigned char* CT_CT1_cell2_mode_buffer;
static unsigned int CT_CT1_cell1_buffer_index;
static unsigned int CT_CT1_cell2_buffer_index;
void CT_CT1_init() {
  CT_CT1_cell1_v_buffer = (double*)calloc(CT_CT1_PC1NI, sizeof(double));
  CT_CT1_cell1_mode_buffer = (unsigned char*)calloc(CT_CT1_PC1NI, sizeof(unsigned char));
  CT_CT1_cell2_v_buffer = (double*)calloc(CT_CT1_PC2NI, sizeof(double));
  CT_CT1_cell2_mode_buffer = (unsigned char*)calloc(CT_CT1_PC2NI, sizeof(unsigned char));
  CT_CT1_cell1_buffer_index = 0;
  CT_CT1_cell2_buffer_index = 0;
}
double CT_CT1_update_c1vd() {
  return CT_CT1_cell1_v_buffer[CT_CT1_cell1_buffer_index];
}
double CT_CT1_update_c2vd() {
  return CT_CT1_cell2_v_buffer[CT_CT1_cell2_buffer_index];
}
double CT_CT1_update_c1md() {
  return CT_CT1_cell1_mode_buffer[CT_CT1_cell1_buffer_index];
}
double CT_CT1_update_c2md() {
  return CT_CT1_cell2_mode_buffer[CT_CT1_cell2_buffer_index];
}
double CT_CT1_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  CT_CT1_cell1_v_buffer[CT_CT1_cell1_buffer_index] = c1v;
  CT_CT1_cell2_v_buffer[CT_CT1_cell2_buffer_index] = c2v;
  CT_CT1_cell1_mode_buffer[CT_CT1_cell1_buffer_index] = c1m;
  CT_CT1_cell2_mode_buffer[CT_CT1_cell2_buffer_index] = c2m;
  CT_CT1_cell1_buffer_index = ((CT_CT1_cell1_buffer_index + 1) >= CT_CT1_PC1NI) ? 0 : ++CT_CT1_cell1_buffer_index;
  CT_CT1_cell2_buffer_index = ((CT_CT1_cell2_buffer_index + 1) >= CT_CT1_PC2NI) ? 0 : ++CT_CT1_cell2_buffer_index;
  return 0.0;
}
double CT_CT1_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double CT_CT1_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double CT_CT1_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double CT_CT1_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define OS_Fast_PC1NI ((int)20.0)
#define OS_Fast_PC2NI ((int)20.0)
static double* OS_Fast_cell1_v_buffer;
static double* OS_Fast_cell2_v_buffer;
static unsigned char* OS_Fast_cell1_mode_buffer;
static unsigned char* OS_Fast_cell2_mode_buffer;
static unsigned int OS_Fast_cell1_buffer_index;
static unsigned int OS_Fast_cell2_buffer_index;
void OS_Fast_init() {
  OS_Fast_cell1_v_buffer = (double*)calloc(OS_Fast_PC1NI, sizeof(double));
  OS_Fast_cell1_mode_buffer = (unsigned char*)calloc(OS_Fast_PC1NI, sizeof(unsigned char));
  OS_Fast_cell2_v_buffer = (double*)calloc(OS_Fast_PC2NI, sizeof(double));
  OS_Fast_cell2_mode_buffer = (unsigned char*)calloc(OS_Fast_PC2NI, sizeof(unsigned char));
  OS_Fast_cell1_buffer_index = 0;
  OS_Fast_cell2_buffer_index = 0;
}
double OS_Fast_update_c1vd() {
  return OS_Fast_cell1_v_buffer[OS_Fast_cell1_buffer_index];
}
double OS_Fast_update_c2vd() {
  return OS_Fast_cell2_v_buffer[OS_Fast_cell2_buffer_index];
}
double OS_Fast_update_c1md() {
  return OS_Fast_cell1_mode_buffer[OS_Fast_cell1_buffer_index];
}
double OS_Fast_update_c2md() {
  return OS_Fast_cell2_mode_buffer[OS_Fast_cell2_buffer_index];
}
double OS_Fast_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  OS_Fast_cell1_v_buffer[OS_Fast_cell1_buffer_index] = c1v;
  OS_Fast_cell2_v_buffer[OS_Fast_cell2_buffer_index] = c2v;
  OS_Fast_cell1_mode_buffer[OS_Fast_cell1_buffer_index] = c1m;
  OS_Fast_cell2_mode_buffer[OS_Fast_cell2_buffer_index] = c2m;
  OS_Fast_cell1_buffer_index = ((OS_Fast_cell1_buffer_index + 1) >= OS_Fast_PC1NI) ? 0 : ++OS_Fast_cell1_buffer_index;
  OS_Fast_cell2_buffer_index = ((OS_Fast_cell2_buffer_index + 1) >= OS_Fast_PC2NI) ? 0 : ++OS_Fast_cell2_buffer_index;
  return 0.0;
}
double OS_Fast_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double OS_Fast_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double OS_Fast_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double OS_Fast_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define Fast_Fast1_PC1NI ((int)20.0)
#define Fast_Fast1_PC2NI ((int)20.0)
static double* Fast_Fast1_cell1_v_buffer;
static double* Fast_Fast1_cell2_v_buffer;
static unsigned char* Fast_Fast1_cell1_mode_buffer;
static unsigned char* Fast_Fast1_cell2_mode_buffer;
static unsigned int Fast_Fast1_cell1_buffer_index;
static unsigned int Fast_Fast1_cell2_buffer_index;
void Fast_Fast1_init() {
  Fast_Fast1_cell1_v_buffer = (double*)calloc(Fast_Fast1_PC1NI, sizeof(double));
  Fast_Fast1_cell1_mode_buffer = (unsigned char*)calloc(Fast_Fast1_PC1NI, sizeof(unsigned char));
  Fast_Fast1_cell2_v_buffer = (double*)calloc(Fast_Fast1_PC2NI, sizeof(double));
  Fast_Fast1_cell2_mode_buffer = (unsigned char*)calloc(Fast_Fast1_PC2NI, sizeof(unsigned char));
  Fast_Fast1_cell1_buffer_index = 0;
  Fast_Fast1_cell2_buffer_index = 0;
}
double Fast_Fast1_update_c1vd() {
  return Fast_Fast1_cell1_v_buffer[Fast_Fast1_cell1_buffer_index];
}
double Fast_Fast1_update_c2vd() {
  return Fast_Fast1_cell2_v_buffer[Fast_Fast1_cell2_buffer_index];
}
double Fast_Fast1_update_c1md() {
  return Fast_Fast1_cell1_mode_buffer[Fast_Fast1_cell1_buffer_index];
}
double Fast_Fast1_update_c2md() {
  return Fast_Fast1_cell2_mode_buffer[Fast_Fast1_cell2_buffer_index];
}
double Fast_Fast1_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  Fast_Fast1_cell1_v_buffer[Fast_Fast1_cell1_buffer_index] = c1v;
  Fast_Fast1_cell2_v_buffer[Fast_Fast1_cell2_buffer_index] = c2v;
  Fast_Fast1_cell1_mode_buffer[Fast_Fast1_cell1_buffer_index] = c1m;
  Fast_Fast1_cell2_mode_buffer[Fast_Fast1_cell2_buffer_index] = c2m;
  Fast_Fast1_cell1_buffer_index = ((Fast_Fast1_cell1_buffer_index + 1) >= Fast_Fast1_PC1NI) ? 0 : ++Fast_Fast1_cell1_buffer_index;
  Fast_Fast1_cell2_buffer_index = ((Fast_Fast1_cell2_buffer_index + 1) >= Fast_Fast1_PC2NI) ? 0 : ++Fast_Fast1_cell2_buffer_index;
  return 0.0;
}
double Fast_Fast1_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double Fast_Fast1_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double Fast_Fast1_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double Fast_Fast1_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define OS_Slow_PC1NI ((int)30.0)
#define OS_Slow_PC2NI ((int)30.0)
static double* OS_Slow_cell1_v_buffer;
static double* OS_Slow_cell2_v_buffer;
static unsigned char* OS_Slow_cell1_mode_buffer;
static unsigned char* OS_Slow_cell2_mode_buffer;
static unsigned int OS_Slow_cell1_buffer_index;
static unsigned int OS_Slow_cell2_buffer_index;
void OS_Slow_init() {
  OS_Slow_cell1_v_buffer = (double*)calloc(OS_Slow_PC1NI, sizeof(double));
  OS_Slow_cell1_mode_buffer = (unsigned char*)calloc(OS_Slow_PC1NI, sizeof(unsigned char));
  OS_Slow_cell2_v_buffer = (double*)calloc(OS_Slow_PC2NI, sizeof(double));
  OS_Slow_cell2_mode_buffer = (unsigned char*)calloc(OS_Slow_PC2NI, sizeof(unsigned char));
  OS_Slow_cell1_buffer_index = 0;
  OS_Slow_cell2_buffer_index = 0;
}
double OS_Slow_update_c1vd() {
  return OS_Slow_cell1_v_buffer[OS_Slow_cell1_buffer_index];
}
double OS_Slow_update_c2vd() {
  return OS_Slow_cell2_v_buffer[OS_Slow_cell2_buffer_index];
}
double OS_Slow_update_c1md() {
  return OS_Slow_cell1_mode_buffer[OS_Slow_cell1_buffer_index];
}
double OS_Slow_update_c2md() {
  return OS_Slow_cell2_mode_buffer[OS_Slow_cell2_buffer_index];
}
double OS_Slow_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  OS_Slow_cell1_v_buffer[OS_Slow_cell1_buffer_index] = c1v;
  OS_Slow_cell2_v_buffer[OS_Slow_cell2_buffer_index] = c2v;
  OS_Slow_cell1_mode_buffer[OS_Slow_cell1_buffer_index] = c1m;
  OS_Slow_cell2_mode_buffer[OS_Slow_cell2_buffer_index] = c2m;
  OS_Slow_cell1_buffer_index = ((OS_Slow_cell1_buffer_index + 1) >= OS_Slow_PC1NI) ? 0 : ++OS_Slow_cell1_buffer_index;
  OS_Slow_cell2_buffer_index = ((OS_Slow_cell2_buffer_index + 1) >= OS_Slow_PC2NI) ? 0 : ++OS_Slow_cell2_buffer_index;
  return 0.0;
}
double OS_Slow_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double OS_Slow_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double OS_Slow_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double OS_Slow_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define Slow_Slow1_PC1NI ((int)30.0)
#define Slow_Slow1_PC2NI ((int)30.0)
static double* Slow_Slow1_cell1_v_buffer;
static double* Slow_Slow1_cell2_v_buffer;
static unsigned char* Slow_Slow1_cell1_mode_buffer;
static unsigned char* Slow_Slow1_cell2_mode_buffer;
static unsigned int Slow_Slow1_cell1_buffer_index;
static unsigned int Slow_Slow1_cell2_buffer_index;
void Slow_Slow1_init() {
  Slow_Slow1_cell1_v_buffer = (double*)calloc(Slow_Slow1_PC1NI, sizeof(double));
  Slow_Slow1_cell1_mode_buffer = (unsigned char*)calloc(Slow_Slow1_PC1NI, sizeof(unsigned char));
  Slow_Slow1_cell2_v_buffer = (double*)calloc(Slow_Slow1_PC2NI, sizeof(double));
  Slow_Slow1_cell2_mode_buffer = (unsigned char*)calloc(Slow_Slow1_PC2NI, sizeof(unsigned char));
  Slow_Slow1_cell1_buffer_index = 0;
  Slow_Slow1_cell2_buffer_index = 0;
}
double Slow_Slow1_update_c1vd() {
  return Slow_Slow1_cell1_v_buffer[Slow_Slow1_cell1_buffer_index];
}
double Slow_Slow1_update_c2vd() {
  return Slow_Slow1_cell2_v_buffer[Slow_Slow1_cell2_buffer_index];
}
double Slow_Slow1_update_c1md() {
  return Slow_Slow1_cell1_mode_buffer[Slow_Slow1_cell1_buffer_index];
}
double Slow_Slow1_update_c2md() {
  return Slow_Slow1_cell2_mode_buffer[Slow_Slow1_cell2_buffer_index];
}
double Slow_Slow1_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  Slow_Slow1_cell1_v_buffer[Slow_Slow1_cell1_buffer_index] = c1v;
  Slow_Slow1_cell2_v_buffer[Slow_Slow1_cell2_buffer_index] = c2v;
  Slow_Slow1_cell1_mode_buffer[Slow_Slow1_cell1_buffer_index] = c1m;
  Slow_Slow1_cell2_mode_buffer[Slow_Slow1_cell2_buffer_index] = c2m;
  Slow_Slow1_cell1_buffer_index = ((Slow_Slow1_cell1_buffer_index + 1) >= Slow_Slow1_PC1NI) ? 0 : ++Slow_Slow1_cell1_buffer_index;
  Slow_Slow1_cell2_buffer_index = ((Slow_Slow1_cell2_buffer_index + 1) >= Slow_Slow1_PC2NI) ? 0 : ++Slow_Slow1_cell2_buffer_index;
  return 0.0;
}
double Slow_Slow1_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double Slow_Slow1_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double Slow_Slow1_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double Slow_Slow1_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define Fast1_AV_PC1NI ((int)10.0)
#define Fast1_AV_PC2NI ((int)10.0)
static double* Fast1_AV_cell1_v_buffer;
static double* Fast1_AV_cell2_v_buffer;
static unsigned char* Fast1_AV_cell1_mode_buffer;
static unsigned char* Fast1_AV_cell2_mode_buffer;
static unsigned int Fast1_AV_cell1_buffer_index;
static unsigned int Fast1_AV_cell2_buffer_index;
void Fast1_AV_init() {
  Fast1_AV_cell1_v_buffer = (double*)calloc(Fast1_AV_PC1NI, sizeof(double));
  Fast1_AV_cell1_mode_buffer = (unsigned char*)calloc(Fast1_AV_PC1NI, sizeof(unsigned char));
  Fast1_AV_cell2_v_buffer = (double*)calloc(Fast1_AV_PC2NI, sizeof(double));
  Fast1_AV_cell2_mode_buffer = (unsigned char*)calloc(Fast1_AV_PC2NI, sizeof(unsigned char));
  Fast1_AV_cell1_buffer_index = 0;
  Fast1_AV_cell2_buffer_index = 0;
}
double Fast1_AV_update_c1vd() {
  return Fast1_AV_cell1_v_buffer[Fast1_AV_cell1_buffer_index];
}
double Fast1_AV_update_c2vd() {
  return Fast1_AV_cell2_v_buffer[Fast1_AV_cell2_buffer_index];
}
double Fast1_AV_update_c1md() {
  return Fast1_AV_cell1_mode_buffer[Fast1_AV_cell1_buffer_index];
}
double Fast1_AV_update_c2md() {
  return Fast1_AV_cell2_mode_buffer[Fast1_AV_cell2_buffer_index];
}
double Fast1_AV_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  Fast1_AV_cell1_v_buffer[Fast1_AV_cell1_buffer_index] = c1v;
  Fast1_AV_cell2_v_buffer[Fast1_AV_cell2_buffer_index] = c2v;
  Fast1_AV_cell1_mode_buffer[Fast1_AV_cell1_buffer_index] = c1m;
  Fast1_AV_cell2_mode_buffer[Fast1_AV_cell2_buffer_index] = c2m;
  Fast1_AV_cell1_buffer_index = ((Fast1_AV_cell1_buffer_index + 1) >= Fast1_AV_PC1NI) ? 0 : ++Fast1_AV_cell1_buffer_index;
  Fast1_AV_cell2_buffer_index = ((Fast1_AV_cell2_buffer_index + 1) >= Fast1_AV_PC2NI) ? 0 : ++Fast1_AV_cell2_buffer_index;
  return 0.0;
}
double Fast1_AV_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double Fast1_AV_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double Fast1_AV_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double Fast1_AV_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define Slow1_AV_PC1NI ((int)15.0)
#define Slow1_AV_PC2NI ((int)15.0)
static double* Slow1_AV_cell1_v_buffer;
static double* Slow1_AV_cell2_v_buffer;
static unsigned char* Slow1_AV_cell1_mode_buffer;
static unsigned char* Slow1_AV_cell2_mode_buffer;
static unsigned int Slow1_AV_cell1_buffer_index;
static unsigned int Slow1_AV_cell2_buffer_index;
void Slow1_AV_init() {
  Slow1_AV_cell1_v_buffer = (double*)calloc(Slow1_AV_PC1NI, sizeof(double));
  Slow1_AV_cell1_mode_buffer = (unsigned char*)calloc(Slow1_AV_PC1NI, sizeof(unsigned char));
  Slow1_AV_cell2_v_buffer = (double*)calloc(Slow1_AV_PC2NI, sizeof(double));
  Slow1_AV_cell2_mode_buffer = (unsigned char*)calloc(Slow1_AV_PC2NI, sizeof(unsigned char));
  Slow1_AV_cell1_buffer_index = 0;
  Slow1_AV_cell2_buffer_index = 0;
}
double Slow1_AV_update_c1vd() {
  return Slow1_AV_cell1_v_buffer[Slow1_AV_cell1_buffer_index];
}
double Slow1_AV_update_c2vd() {
  return Slow1_AV_cell2_v_buffer[Slow1_AV_cell2_buffer_index];
}
double Slow1_AV_update_c1md() {
  return Slow1_AV_cell1_mode_buffer[Slow1_AV_cell1_buffer_index];
}
double Slow1_AV_update_c2md() {
  return Slow1_AV_cell2_mode_buffer[Slow1_AV_cell2_buffer_index];
}
double Slow1_AV_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  Slow1_AV_cell1_v_buffer[Slow1_AV_cell1_buffer_index] = c1v;
  Slow1_AV_cell2_v_buffer[Slow1_AV_cell2_buffer_index] = c2v;
  Slow1_AV_cell1_mode_buffer[Slow1_AV_cell1_buffer_index] = c1m;
  Slow1_AV_cell2_mode_buffer[Slow1_AV_cell2_buffer_index] = c2m;
  Slow1_AV_cell1_buffer_index = ((Slow1_AV_cell1_buffer_index + 1) >= Slow1_AV_PC1NI) ? 0 : ++Slow1_AV_cell1_buffer_index;
  Slow1_AV_cell2_buffer_index = ((Slow1_AV_cell2_buffer_index + 1) >= Slow1_AV_PC2NI) ? 0 : ++Slow1_AV_cell2_buffer_index;
  return 0.0;
}
double Slow1_AV_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double Slow1_AV_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double Slow1_AV_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double Slow1_AV_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define AV_His_PC1NI ((int)30.0)
#define AV_His_PC2NI ((int)30.0)
static double* AV_His_cell1_v_buffer;
static double* AV_His_cell2_v_buffer;
static unsigned char* AV_His_cell1_mode_buffer;
static unsigned char* AV_His_cell2_mode_buffer;
static unsigned int AV_His_cell1_buffer_index;
static unsigned int AV_His_cell2_buffer_index;
void AV_His_init() {
  AV_His_cell1_v_buffer = (double*)calloc(AV_His_PC1NI, sizeof(double));
  AV_His_cell1_mode_buffer = (unsigned char*)calloc(AV_His_PC1NI, sizeof(unsigned char));
  AV_His_cell2_v_buffer = (double*)calloc(AV_His_PC2NI, sizeof(double));
  AV_His_cell2_mode_buffer = (unsigned char*)calloc(AV_His_PC2NI, sizeof(unsigned char));
  AV_His_cell1_buffer_index = 0;
  AV_His_cell2_buffer_index = 0;
}
double AV_His_update_c1vd() {
  return AV_His_cell1_v_buffer[AV_His_cell1_buffer_index];
}
double AV_His_update_c2vd() {
  return AV_His_cell2_v_buffer[AV_His_cell2_buffer_index];
}
double AV_His_update_c1md() {
  return AV_His_cell1_mode_buffer[AV_His_cell1_buffer_index];
}
double AV_His_update_c2md() {
  return AV_His_cell2_mode_buffer[AV_His_cell2_buffer_index];
}
double AV_His_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  AV_His_cell1_v_buffer[AV_His_cell1_buffer_index] = c1v;
  AV_His_cell2_v_buffer[AV_His_cell2_buffer_index] = c2v;
  AV_His_cell1_mode_buffer[AV_His_cell1_buffer_index] = c1m;
  AV_His_cell2_mode_buffer[AV_His_cell2_buffer_index] = c2m;
  AV_His_cell1_buffer_index = ((AV_His_cell1_buffer_index + 1) >= AV_His_PC1NI) ? 0 : ++AV_His_cell1_buffer_index;
  AV_His_cell2_buffer_index = ((AV_His_cell2_buffer_index + 1) >= AV_His_PC2NI) ? 0 : ++AV_His_cell2_buffer_index;
  return 0.0;
}
double AV_His_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double AV_His_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double AV_His_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double AV_His_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define His_His1_PC1NI ((int)20.0)
#define His_His1_PC2NI ((int)20.0)
static double* His_His1_cell1_v_buffer;
static double* His_His1_cell2_v_buffer;
static unsigned char* His_His1_cell1_mode_buffer;
static unsigned char* His_His1_cell2_mode_buffer;
static unsigned int His_His1_cell1_buffer_index;
static unsigned int His_His1_cell2_buffer_index;
void His_His1_init() {
  His_His1_cell1_v_buffer = (double*)calloc(His_His1_PC1NI, sizeof(double));
  His_His1_cell1_mode_buffer = (unsigned char*)calloc(His_His1_PC1NI, sizeof(unsigned char));
  His_His1_cell2_v_buffer = (double*)calloc(His_His1_PC2NI, sizeof(double));
  His_His1_cell2_mode_buffer = (unsigned char*)calloc(His_His1_PC2NI, sizeof(unsigned char));
  His_His1_cell1_buffer_index = 0;
  His_His1_cell2_buffer_index = 0;
}
double His_His1_update_c1vd() {
  return His_His1_cell1_v_buffer[His_His1_cell1_buffer_index];
}
double His_His1_update_c2vd() {
  return His_His1_cell2_v_buffer[His_His1_cell2_buffer_index];
}
double His_His1_update_c1md() {
  return His_His1_cell1_mode_buffer[His_His1_cell1_buffer_index];
}
double His_His1_update_c2md() {
  return His_His1_cell2_mode_buffer[His_His1_cell2_buffer_index];
}
double His_His1_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  His_His1_cell1_v_buffer[His_His1_cell1_buffer_index] = c1v;
  His_His1_cell2_v_buffer[His_His1_cell2_buffer_index] = c2v;
  His_His1_cell1_mode_buffer[His_His1_cell1_buffer_index] = c1m;
  His_His1_cell2_mode_buffer[His_His1_cell2_buffer_index] = c2m;
  His_His1_cell1_buffer_index = ((His_His1_cell1_buffer_index + 1) >= His_His1_PC1NI) ? 0 : ++His_His1_cell1_buffer_index;
  His_His1_cell2_buffer_index = ((His_His1_cell2_buffer_index + 1) >= His_His1_PC2NI) ? 0 : ++His_His1_cell2_buffer_index;
  return 0.0;
}
double His_His1_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double His_His1_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double His_His1_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double His_His1_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define His1_His2_PC1NI ((int)20.0)
#define His1_His2_PC2NI ((int)20.0)
static double* His1_His2_cell1_v_buffer;
static double* His1_His2_cell2_v_buffer;
static unsigned char* His1_His2_cell1_mode_buffer;
static unsigned char* His1_His2_cell2_mode_buffer;
static unsigned int His1_His2_cell1_buffer_index;
static unsigned int His1_His2_cell2_buffer_index;
void His1_His2_init() {
  His1_His2_cell1_v_buffer = (double*)calloc(His1_His2_PC1NI, sizeof(double));
  His1_His2_cell1_mode_buffer = (unsigned char*)calloc(His1_His2_PC1NI, sizeof(unsigned char));
  His1_His2_cell2_v_buffer = (double*)calloc(His1_His2_PC2NI, sizeof(double));
  His1_His2_cell2_mode_buffer = (unsigned char*)calloc(His1_His2_PC2NI, sizeof(unsigned char));
  His1_His2_cell1_buffer_index = 0;
  His1_His2_cell2_buffer_index = 0;
}
double His1_His2_update_c1vd() {
  return His1_His2_cell1_v_buffer[His1_His2_cell1_buffer_index];
}
double His1_His2_update_c2vd() {
  return His1_His2_cell2_v_buffer[His1_His2_cell2_buffer_index];
}
double His1_His2_update_c1md() {
  return His1_His2_cell1_mode_buffer[His1_His2_cell1_buffer_index];
}
double His1_His2_update_c2md() {
  return His1_His2_cell2_mode_buffer[His1_His2_cell2_buffer_index];
}
double His1_His2_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  His1_His2_cell1_v_buffer[His1_His2_cell1_buffer_index] = c1v;
  His1_His2_cell2_v_buffer[His1_His2_cell2_buffer_index] = c2v;
  His1_His2_cell1_mode_buffer[His1_His2_cell1_buffer_index] = c1m;
  His1_His2_cell2_mode_buffer[His1_His2_cell2_buffer_index] = c2m;
  His1_His2_cell1_buffer_index = ((His1_His2_cell1_buffer_index + 1) >= His1_His2_PC1NI) ? 0 : ++His1_His2_cell1_buffer_index;
  His1_His2_cell2_buffer_index = ((His1_His2_cell2_buffer_index + 1) >= His1_His2_PC2NI) ? 0 : ++His1_His2_cell2_buffer_index;
  return 0.0;
}
double His1_His2_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double His1_His2_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double His1_His2_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double His1_His2_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define His2_LBB_PC1NI ((int)20.0)
#define His2_LBB_PC2NI ((int)20.0)
static double* His2_LBB_cell1_v_buffer;
static double* His2_LBB_cell2_v_buffer;
static unsigned char* His2_LBB_cell1_mode_buffer;
static unsigned char* His2_LBB_cell2_mode_buffer;
static unsigned int His2_LBB_cell1_buffer_index;
static unsigned int His2_LBB_cell2_buffer_index;
void His2_LBB_init() {
  His2_LBB_cell1_v_buffer = (double*)calloc(His2_LBB_PC1NI, sizeof(double));
  His2_LBB_cell1_mode_buffer = (unsigned char*)calloc(His2_LBB_PC1NI, sizeof(unsigned char));
  His2_LBB_cell2_v_buffer = (double*)calloc(His2_LBB_PC2NI, sizeof(double));
  His2_LBB_cell2_mode_buffer = (unsigned char*)calloc(His2_LBB_PC2NI, sizeof(unsigned char));
  His2_LBB_cell1_buffer_index = 0;
  His2_LBB_cell2_buffer_index = 0;
}
double His2_LBB_update_c1vd() {
  return His2_LBB_cell1_v_buffer[His2_LBB_cell1_buffer_index];
}
double His2_LBB_update_c2vd() {
  return His2_LBB_cell2_v_buffer[His2_LBB_cell2_buffer_index];
}
double His2_LBB_update_c1md() {
  return His2_LBB_cell1_mode_buffer[His2_LBB_cell1_buffer_index];
}
double His2_LBB_update_c2md() {
  return His2_LBB_cell2_mode_buffer[His2_LBB_cell2_buffer_index];
}
double His2_LBB_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  His2_LBB_cell1_v_buffer[His2_LBB_cell1_buffer_index] = c1v;
  His2_LBB_cell2_v_buffer[His2_LBB_cell2_buffer_index] = c2v;
  His2_LBB_cell1_mode_buffer[His2_LBB_cell1_buffer_index] = c1m;
  His2_LBB_cell2_mode_buffer[His2_LBB_cell2_buffer_index] = c2m;
  His2_LBB_cell1_buffer_index = ((His2_LBB_cell1_buffer_index + 1) >= His2_LBB_PC1NI) ? 0 : ++His2_LBB_cell1_buffer_index;
  His2_LBB_cell2_buffer_index = ((His2_LBB_cell2_buffer_index + 1) >= His2_LBB_PC2NI) ? 0 : ++His2_LBB_cell2_buffer_index;
  return 0.0;
}
double His2_LBB_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double His2_LBB_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double His2_LBB_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double His2_LBB_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define LBB_LBB1_PC1NI ((int)5.0)
#define LBB_LBB1_PC2NI ((int)5.0)
static double* LBB_LBB1_cell1_v_buffer;
static double* LBB_LBB1_cell2_v_buffer;
static unsigned char* LBB_LBB1_cell1_mode_buffer;
static unsigned char* LBB_LBB1_cell2_mode_buffer;
static unsigned int LBB_LBB1_cell1_buffer_index;
static unsigned int LBB_LBB1_cell2_buffer_index;
void LBB_LBB1_init() {
  LBB_LBB1_cell1_v_buffer = (double*)calloc(LBB_LBB1_PC1NI, sizeof(double));
  LBB_LBB1_cell1_mode_buffer = (unsigned char*)calloc(LBB_LBB1_PC1NI, sizeof(unsigned char));
  LBB_LBB1_cell2_v_buffer = (double*)calloc(LBB_LBB1_PC2NI, sizeof(double));
  LBB_LBB1_cell2_mode_buffer = (unsigned char*)calloc(LBB_LBB1_PC2NI, sizeof(unsigned char));
  LBB_LBB1_cell1_buffer_index = 0;
  LBB_LBB1_cell2_buffer_index = 0;
}
double LBB_LBB1_update_c1vd() {
  return LBB_LBB1_cell1_v_buffer[LBB_LBB1_cell1_buffer_index];
}
double LBB_LBB1_update_c2vd() {
  return LBB_LBB1_cell2_v_buffer[LBB_LBB1_cell2_buffer_index];
}
double LBB_LBB1_update_c1md() {
  return LBB_LBB1_cell1_mode_buffer[LBB_LBB1_cell1_buffer_index];
}
double LBB_LBB1_update_c2md() {
  return LBB_LBB1_cell2_mode_buffer[LBB_LBB1_cell2_buffer_index];
}
double LBB_LBB1_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  LBB_LBB1_cell1_v_buffer[LBB_LBB1_cell1_buffer_index] = c1v;
  LBB_LBB1_cell2_v_buffer[LBB_LBB1_cell2_buffer_index] = c2v;
  LBB_LBB1_cell1_mode_buffer[LBB_LBB1_cell1_buffer_index] = c1m;
  LBB_LBB1_cell2_mode_buffer[LBB_LBB1_cell2_buffer_index] = c2m;
  LBB_LBB1_cell1_buffer_index = ((LBB_LBB1_cell1_buffer_index + 1) >= LBB_LBB1_PC1NI) ? 0 : ++LBB_LBB1_cell1_buffer_index;
  LBB_LBB1_cell2_buffer_index = ((LBB_LBB1_cell2_buffer_index + 1) >= LBB_LBB1_PC2NI) ? 0 : ++LBB_LBB1_cell2_buffer_index;
  return 0.0;
}
double LBB_LBB1_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double LBB_LBB1_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double LBB_LBB1_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double LBB_LBB1_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define LBB1_LVA_PC1NI ((int)5.0)
#define LBB1_LVA_PC2NI ((int)5.0)
static double* LBB1_LVA_cell1_v_buffer;
static double* LBB1_LVA_cell2_v_buffer;
static unsigned char* LBB1_LVA_cell1_mode_buffer;
static unsigned char* LBB1_LVA_cell2_mode_buffer;
static unsigned int LBB1_LVA_cell1_buffer_index;
static unsigned int LBB1_LVA_cell2_buffer_index;
void LBB1_LVA_init() {
  LBB1_LVA_cell1_v_buffer = (double*)calloc(LBB1_LVA_PC1NI, sizeof(double));
  LBB1_LVA_cell1_mode_buffer = (unsigned char*)calloc(LBB1_LVA_PC1NI, sizeof(unsigned char));
  LBB1_LVA_cell2_v_buffer = (double*)calloc(LBB1_LVA_PC2NI, sizeof(double));
  LBB1_LVA_cell2_mode_buffer = (unsigned char*)calloc(LBB1_LVA_PC2NI, sizeof(unsigned char));
  LBB1_LVA_cell1_buffer_index = 0;
  LBB1_LVA_cell2_buffer_index = 0;
}
double LBB1_LVA_update_c1vd() {
  return LBB1_LVA_cell1_v_buffer[LBB1_LVA_cell1_buffer_index];
}
double LBB1_LVA_update_c2vd() {
  return LBB1_LVA_cell2_v_buffer[LBB1_LVA_cell2_buffer_index];
}
double LBB1_LVA_update_c1md() {
  return LBB1_LVA_cell1_mode_buffer[LBB1_LVA_cell1_buffer_index];
}
double LBB1_LVA_update_c2md() {
  return LBB1_LVA_cell2_mode_buffer[LBB1_LVA_cell2_buffer_index];
}
double LBB1_LVA_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  LBB1_LVA_cell1_v_buffer[LBB1_LVA_cell1_buffer_index] = c1v;
  LBB1_LVA_cell2_v_buffer[LBB1_LVA_cell2_buffer_index] = c2v;
  LBB1_LVA_cell1_mode_buffer[LBB1_LVA_cell1_buffer_index] = c1m;
  LBB1_LVA_cell2_mode_buffer[LBB1_LVA_cell2_buffer_index] = c2m;
  LBB1_LVA_cell1_buffer_index = ((LBB1_LVA_cell1_buffer_index + 1) >= LBB1_LVA_PC1NI) ? 0 : ++LBB1_LVA_cell1_buffer_index;
  LBB1_LVA_cell2_buffer_index = ((LBB1_LVA_cell2_buffer_index + 1) >= LBB1_LVA_PC2NI) ? 0 : ++LBB1_LVA_cell2_buffer_index;
  return 0.0;
}
double LBB1_LVA_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double LBB1_LVA_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double LBB1_LVA_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double LBB1_LVA_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define His2_RBB_PC1NI ((int)20.0)
#define His2_RBB_PC2NI ((int)20.0)
static double* His2_RBB_cell1_v_buffer;
static double* His2_RBB_cell2_v_buffer;
static unsigned char* His2_RBB_cell1_mode_buffer;
static unsigned char* His2_RBB_cell2_mode_buffer;
static unsigned int His2_RBB_cell1_buffer_index;
static unsigned int His2_RBB_cell2_buffer_index;
void His2_RBB_init() {
  His2_RBB_cell1_v_buffer = (double*)calloc(His2_RBB_PC1NI, sizeof(double));
  His2_RBB_cell1_mode_buffer = (unsigned char*)calloc(His2_RBB_PC1NI, sizeof(unsigned char));
  His2_RBB_cell2_v_buffer = (double*)calloc(His2_RBB_PC2NI, sizeof(double));
  His2_RBB_cell2_mode_buffer = (unsigned char*)calloc(His2_RBB_PC2NI, sizeof(unsigned char));
  His2_RBB_cell1_buffer_index = 0;
  His2_RBB_cell2_buffer_index = 0;
}
double His2_RBB_update_c1vd() {
  return His2_RBB_cell1_v_buffer[His2_RBB_cell1_buffer_index];
}
double His2_RBB_update_c2vd() {
  return His2_RBB_cell2_v_buffer[His2_RBB_cell2_buffer_index];
}
double His2_RBB_update_c1md() {
  return His2_RBB_cell1_mode_buffer[His2_RBB_cell1_buffer_index];
}
double His2_RBB_update_c2md() {
  return His2_RBB_cell2_mode_buffer[His2_RBB_cell2_buffer_index];
}
double His2_RBB_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  His2_RBB_cell1_v_buffer[His2_RBB_cell1_buffer_index] = c1v;
  His2_RBB_cell2_v_buffer[His2_RBB_cell2_buffer_index] = c2v;
  His2_RBB_cell1_mode_buffer[His2_RBB_cell1_buffer_index] = c1m;
  His2_RBB_cell2_mode_buffer[His2_RBB_cell2_buffer_index] = c2m;
  His2_RBB_cell1_buffer_index = ((His2_RBB_cell1_buffer_index + 1) >= His2_RBB_PC1NI) ? 0 : ++His2_RBB_cell1_buffer_index;
  His2_RBB_cell2_buffer_index = ((His2_RBB_cell2_buffer_index + 1) >= His2_RBB_PC2NI) ? 0 : ++His2_RBB_cell2_buffer_index;
  return 0.0;
}
double His2_RBB_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double His2_RBB_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double His2_RBB_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double His2_RBB_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define RBB_RBB1_PC1NI ((int)5.0)
#define RBB_RBB1_PC2NI ((int)5.0)
static double* RBB_RBB1_cell1_v_buffer;
static double* RBB_RBB1_cell2_v_buffer;
static unsigned char* RBB_RBB1_cell1_mode_buffer;
static unsigned char* RBB_RBB1_cell2_mode_buffer;
static unsigned int RBB_RBB1_cell1_buffer_index;
static unsigned int RBB_RBB1_cell2_buffer_index;
void RBB_RBB1_init() {
  RBB_RBB1_cell1_v_buffer = (double*)calloc(RBB_RBB1_PC1NI, sizeof(double));
  RBB_RBB1_cell1_mode_buffer = (unsigned char*)calloc(RBB_RBB1_PC1NI, sizeof(unsigned char));
  RBB_RBB1_cell2_v_buffer = (double*)calloc(RBB_RBB1_PC2NI, sizeof(double));
  RBB_RBB1_cell2_mode_buffer = (unsigned char*)calloc(RBB_RBB1_PC2NI, sizeof(unsigned char));
  RBB_RBB1_cell1_buffer_index = 0;
  RBB_RBB1_cell2_buffer_index = 0;
}
double RBB_RBB1_update_c1vd() {
  return RBB_RBB1_cell1_v_buffer[RBB_RBB1_cell1_buffer_index];
}
double RBB_RBB1_update_c2vd() {
  return RBB_RBB1_cell2_v_buffer[RBB_RBB1_cell2_buffer_index];
}
double RBB_RBB1_update_c1md() {
  return RBB_RBB1_cell1_mode_buffer[RBB_RBB1_cell1_buffer_index];
}
double RBB_RBB1_update_c2md() {
  return RBB_RBB1_cell2_mode_buffer[RBB_RBB1_cell2_buffer_index];
}
double RBB_RBB1_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  RBB_RBB1_cell1_v_buffer[RBB_RBB1_cell1_buffer_index] = c1v;
  RBB_RBB1_cell2_v_buffer[RBB_RBB1_cell2_buffer_index] = c2v;
  RBB_RBB1_cell1_mode_buffer[RBB_RBB1_cell1_buffer_index] = c1m;
  RBB_RBB1_cell2_mode_buffer[RBB_RBB1_cell2_buffer_index] = c2m;
  RBB_RBB1_cell1_buffer_index = ((RBB_RBB1_cell1_buffer_index + 1) >= RBB_RBB1_PC1NI) ? 0 : ++RBB_RBB1_cell1_buffer_index;
  RBB_RBB1_cell2_buffer_index = ((RBB_RBB1_cell2_buffer_index + 1) >= RBB_RBB1_PC2NI) ? 0 : ++RBB_RBB1_cell2_buffer_index;
  return 0.0;
}
double RBB_RBB1_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double RBB_RBB1_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double RBB_RBB1_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double RBB_RBB1_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define RBB1_RVA_PC1NI ((int)5.0)
#define RBB1_RVA_PC2NI ((int)5.0)
static double* RBB1_RVA_cell1_v_buffer;
static double* RBB1_RVA_cell2_v_buffer;
static unsigned char* RBB1_RVA_cell1_mode_buffer;
static unsigned char* RBB1_RVA_cell2_mode_buffer;
static unsigned int RBB1_RVA_cell1_buffer_index;
static unsigned int RBB1_RVA_cell2_buffer_index;
void RBB1_RVA_init() {
  RBB1_RVA_cell1_v_buffer = (double*)calloc(RBB1_RVA_PC1NI, sizeof(double));
  RBB1_RVA_cell1_mode_buffer = (unsigned char*)calloc(RBB1_RVA_PC1NI, sizeof(unsigned char));
  RBB1_RVA_cell2_v_buffer = (double*)calloc(RBB1_RVA_PC2NI, sizeof(double));
  RBB1_RVA_cell2_mode_buffer = (unsigned char*)calloc(RBB1_RVA_PC2NI, sizeof(unsigned char));
  RBB1_RVA_cell1_buffer_index = 0;
  RBB1_RVA_cell2_buffer_index = 0;
}
double RBB1_RVA_update_c1vd() {
  return RBB1_RVA_cell1_v_buffer[RBB1_RVA_cell1_buffer_index];
}
double RBB1_RVA_update_c2vd() {
  return RBB1_RVA_cell2_v_buffer[RBB1_RVA_cell2_buffer_index];
}
double RBB1_RVA_update_c1md() {
  return RBB1_RVA_cell1_mode_buffer[RBB1_RVA_cell1_buffer_index];
}
double RBB1_RVA_update_c2md() {
  return RBB1_RVA_cell2_mode_buffer[RBB1_RVA_cell2_buffer_index];
}
double RBB1_RVA_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  RBB1_RVA_cell1_v_buffer[RBB1_RVA_cell1_buffer_index] = c1v;
  RBB1_RVA_cell2_v_buffer[RBB1_RVA_cell2_buffer_index] = c2v;
  RBB1_RVA_cell1_mode_buffer[RBB1_RVA_cell1_buffer_index] = c1m;
  RBB1_RVA_cell2_mode_buffer[RBB1_RVA_cell2_buffer_index] = c2m;
  RBB1_RVA_cell1_buffer_index = ((RBB1_RVA_cell1_buffer_index + 1) >= RBB1_RVA_PC1NI) ? 0 : ++RBB1_RVA_cell1_buffer_index;
  RBB1_RVA_cell2_buffer_index = ((RBB1_RVA_cell2_buffer_index + 1) >= RBB1_RVA_PC2NI) ? 0 : ++RBB1_RVA_cell2_buffer_index;
  return 0.0;
}
double RBB1_RVA_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double RBB1_RVA_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double RBB1_RVA_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double RBB1_RVA_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define LVA_RVA_PC1NI ((int)5.0)
#define LVA_RVA_PC2NI ((int)5.0)
static double* LVA_RVA_cell1_v_buffer;
static double* LVA_RVA_cell2_v_buffer;
static unsigned char* LVA_RVA_cell1_mode_buffer;
static unsigned char* LVA_RVA_cell2_mode_buffer;
static unsigned int LVA_RVA_cell1_buffer_index;
static unsigned int LVA_RVA_cell2_buffer_index;
void LVA_RVA_init() {
  LVA_RVA_cell1_v_buffer = (double*)calloc(LVA_RVA_PC1NI, sizeof(double));
  LVA_RVA_cell1_mode_buffer = (unsigned char*)calloc(LVA_RVA_PC1NI, sizeof(unsigned char));
  LVA_RVA_cell2_v_buffer = (double*)calloc(LVA_RVA_PC2NI, sizeof(double));
  LVA_RVA_cell2_mode_buffer = (unsigned char*)calloc(LVA_RVA_PC2NI, sizeof(unsigned char));
  LVA_RVA_cell1_buffer_index = 0;
  LVA_RVA_cell2_buffer_index = 0;
}
double LVA_RVA_update_c1vd() {
  return LVA_RVA_cell1_v_buffer[LVA_RVA_cell1_buffer_index];
}
double LVA_RVA_update_c2vd() {
  return LVA_RVA_cell2_v_buffer[LVA_RVA_cell2_buffer_index];
}
double LVA_RVA_update_c1md() {
  return LVA_RVA_cell1_mode_buffer[LVA_RVA_cell1_buffer_index];
}
double LVA_RVA_update_c2md() {
  return LVA_RVA_cell2_mode_buffer[LVA_RVA_cell2_buffer_index];
}
double LVA_RVA_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  LVA_RVA_cell1_v_buffer[LVA_RVA_cell1_buffer_index] = c1v;
  LVA_RVA_cell2_v_buffer[LVA_RVA_cell2_buffer_index] = c2v;
  LVA_RVA_cell1_mode_buffer[LVA_RVA_cell1_buffer_index] = c1m;
  LVA_RVA_cell2_mode_buffer[LVA_RVA_cell2_buffer_index] = c2m;
  LVA_RVA_cell1_buffer_index = ((LVA_RVA_cell1_buffer_index + 1) >= LVA_RVA_PC1NI) ? 0 : ++LVA_RVA_cell1_buffer_index;
  LVA_RVA_cell2_buffer_index = ((LVA_RVA_cell2_buffer_index + 1) >= LVA_RVA_PC2NI) ? 0 : ++LVA_RVA_cell2_buffer_index;
  return 0.0;
}
double LVA_RVA_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double LVA_RVA_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double LVA_RVA_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double LVA_RVA_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define LVA_LV_PC1NI ((int)20.0)
#define LVA_LV_PC2NI ((int)20.0)
static double* LVA_LV_cell1_v_buffer;
static double* LVA_LV_cell2_v_buffer;
static unsigned char* LVA_LV_cell1_mode_buffer;
static unsigned char* LVA_LV_cell2_mode_buffer;
static unsigned int LVA_LV_cell1_buffer_index;
static unsigned int LVA_LV_cell2_buffer_index;
void LVA_LV_init() {
  LVA_LV_cell1_v_buffer = (double*)calloc(LVA_LV_PC1NI, sizeof(double));
  LVA_LV_cell1_mode_buffer = (unsigned char*)calloc(LVA_LV_PC1NI, sizeof(unsigned char));
  LVA_LV_cell2_v_buffer = (double*)calloc(LVA_LV_PC2NI, sizeof(double));
  LVA_LV_cell2_mode_buffer = (unsigned char*)calloc(LVA_LV_PC2NI, sizeof(unsigned char));
  LVA_LV_cell1_buffer_index = 0;
  LVA_LV_cell2_buffer_index = 0;
}
double LVA_LV_update_c1vd() {
  return LVA_LV_cell1_v_buffer[LVA_LV_cell1_buffer_index];
}
double LVA_LV_update_c2vd() {
  return LVA_LV_cell2_v_buffer[LVA_LV_cell2_buffer_index];
}
double LVA_LV_update_c1md() {
  return LVA_LV_cell1_mode_buffer[LVA_LV_cell1_buffer_index];
}
double LVA_LV_update_c2md() {
  return LVA_LV_cell2_mode_buffer[LVA_LV_cell2_buffer_index];
}
double LVA_LV_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  LVA_LV_cell1_v_buffer[LVA_LV_cell1_buffer_index] = c1v;
  LVA_LV_cell2_v_buffer[LVA_LV_cell2_buffer_index] = c2v;
  LVA_LV_cell1_mode_buffer[LVA_LV_cell1_buffer_index] = c1m;
  LVA_LV_cell2_mode_buffer[LVA_LV_cell2_buffer_index] = c2m;
  LVA_LV_cell1_buffer_index = ((LVA_LV_cell1_buffer_index + 1) >= LVA_LV_PC1NI) ? 0 : ++LVA_LV_cell1_buffer_index;
  LVA_LV_cell2_buffer_index = ((LVA_LV_cell2_buffer_index + 1) >= LVA_LV_PC2NI) ? 0 : ++LVA_LV_cell2_buffer_index;
  return 0.0;
}
double LVA_LV_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double LVA_LV_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double LVA_LV_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double LVA_LV_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define LV_LV1_PC1NI ((int)30.0)
#define LV_LV1_PC2NI ((int)30.0)
static double* LV_LV1_cell1_v_buffer;
static double* LV_LV1_cell2_v_buffer;
static unsigned char* LV_LV1_cell1_mode_buffer;
static unsigned char* LV_LV1_cell2_mode_buffer;
static unsigned int LV_LV1_cell1_buffer_index;
static unsigned int LV_LV1_cell2_buffer_index;
void LV_LV1_init() {
  LV_LV1_cell1_v_buffer = (double*)calloc(LV_LV1_PC1NI, sizeof(double));
  LV_LV1_cell1_mode_buffer = (unsigned char*)calloc(LV_LV1_PC1NI, sizeof(unsigned char));
  LV_LV1_cell2_v_buffer = (double*)calloc(LV_LV1_PC2NI, sizeof(double));
  LV_LV1_cell2_mode_buffer = (unsigned char*)calloc(LV_LV1_PC2NI, sizeof(unsigned char));
  LV_LV1_cell1_buffer_index = 0;
  LV_LV1_cell2_buffer_index = 0;
}
double LV_LV1_update_c1vd() {
  return LV_LV1_cell1_v_buffer[LV_LV1_cell1_buffer_index];
}
double LV_LV1_update_c2vd() {
  return LV_LV1_cell2_v_buffer[LV_LV1_cell2_buffer_index];
}
double LV_LV1_update_c1md() {
  return LV_LV1_cell1_mode_buffer[LV_LV1_cell1_buffer_index];
}
double LV_LV1_update_c2md() {
  return LV_LV1_cell2_mode_buffer[LV_LV1_cell2_buffer_index];
}
double LV_LV1_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  LV_LV1_cell1_v_buffer[LV_LV1_cell1_buffer_index] = c1v;
  LV_LV1_cell2_v_buffer[LV_LV1_cell2_buffer_index] = c2v;
  LV_LV1_cell1_mode_buffer[LV_LV1_cell1_buffer_index] = c1m;
  LV_LV1_cell2_mode_buffer[LV_LV1_cell2_buffer_index] = c2m;
  LV_LV1_cell1_buffer_index = ((LV_LV1_cell1_buffer_index + 1) >= LV_LV1_PC1NI) ? 0 : ++LV_LV1_cell1_buffer_index;
  LV_LV1_cell2_buffer_index = ((LV_LV1_cell2_buffer_index + 1) >= LV_LV1_PC2NI) ? 0 : ++LV_LV1_cell2_buffer_index;
  return 0.0;
}
double LV_LV1_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double LV_LV1_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double LV_LV1_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double LV_LV1_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define LVA_LVS_PC1NI ((int)15.0)
#define LVA_LVS_PC2NI ((int)15.0)
static double* LVA_LVS_cell1_v_buffer;
static double* LVA_LVS_cell2_v_buffer;
static unsigned char* LVA_LVS_cell1_mode_buffer;
static unsigned char* LVA_LVS_cell2_mode_buffer;
static unsigned int LVA_LVS_cell1_buffer_index;
static unsigned int LVA_LVS_cell2_buffer_index;
void LVA_LVS_init() {
  LVA_LVS_cell1_v_buffer = (double*)calloc(LVA_LVS_PC1NI, sizeof(double));
  LVA_LVS_cell1_mode_buffer = (unsigned char*)calloc(LVA_LVS_PC1NI, sizeof(unsigned char));
  LVA_LVS_cell2_v_buffer = (double*)calloc(LVA_LVS_PC2NI, sizeof(double));
  LVA_LVS_cell2_mode_buffer = (unsigned char*)calloc(LVA_LVS_PC2NI, sizeof(unsigned char));
  LVA_LVS_cell1_buffer_index = 0;
  LVA_LVS_cell2_buffer_index = 0;
}
double LVA_LVS_update_c1vd() {
  return LVA_LVS_cell1_v_buffer[LVA_LVS_cell1_buffer_index];
}
double LVA_LVS_update_c2vd() {
  return LVA_LVS_cell2_v_buffer[LVA_LVS_cell2_buffer_index];
}
double LVA_LVS_update_c1md() {
  return LVA_LVS_cell1_mode_buffer[LVA_LVS_cell1_buffer_index];
}
double LVA_LVS_update_c2md() {
  return LVA_LVS_cell2_mode_buffer[LVA_LVS_cell2_buffer_index];
}
double LVA_LVS_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  LVA_LVS_cell1_v_buffer[LVA_LVS_cell1_buffer_index] = c1v;
  LVA_LVS_cell2_v_buffer[LVA_LVS_cell2_buffer_index] = c2v;
  LVA_LVS_cell1_mode_buffer[LVA_LVS_cell1_buffer_index] = c1m;
  LVA_LVS_cell2_mode_buffer[LVA_LVS_cell2_buffer_index] = c2m;
  LVA_LVS_cell1_buffer_index = ((LVA_LVS_cell1_buffer_index + 1) >= LVA_LVS_PC1NI) ? 0 : ++LVA_LVS_cell1_buffer_index;
  LVA_LVS_cell2_buffer_index = ((LVA_LVS_cell2_buffer_index + 1) >= LVA_LVS_PC2NI) ? 0 : ++LVA_LVS_cell2_buffer_index;
  return 0.0;
}
double LVA_LVS_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double LVA_LVS_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double LVA_LVS_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double LVA_LVS_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define LVS_LVS1_PC1NI ((int)15.0)
#define LVS_LVS1_PC2NI ((int)15.0)
static double* LVS_LVS1_cell1_v_buffer;
static double* LVS_LVS1_cell2_v_buffer;
static unsigned char* LVS_LVS1_cell1_mode_buffer;
static unsigned char* LVS_LVS1_cell2_mode_buffer;
static unsigned int LVS_LVS1_cell1_buffer_index;
static unsigned int LVS_LVS1_cell2_buffer_index;
void LVS_LVS1_init() {
  LVS_LVS1_cell1_v_buffer = (double*)calloc(LVS_LVS1_PC1NI, sizeof(double));
  LVS_LVS1_cell1_mode_buffer = (unsigned char*)calloc(LVS_LVS1_PC1NI, sizeof(unsigned char));
  LVS_LVS1_cell2_v_buffer = (double*)calloc(LVS_LVS1_PC2NI, sizeof(double));
  LVS_LVS1_cell2_mode_buffer = (unsigned char*)calloc(LVS_LVS1_PC2NI, sizeof(unsigned char));
  LVS_LVS1_cell1_buffer_index = 0;
  LVS_LVS1_cell2_buffer_index = 0;
}
double LVS_LVS1_update_c1vd() {
  return LVS_LVS1_cell1_v_buffer[LVS_LVS1_cell1_buffer_index];
}
double LVS_LVS1_update_c2vd() {
  return LVS_LVS1_cell2_v_buffer[LVS_LVS1_cell2_buffer_index];
}
double LVS_LVS1_update_c1md() {
  return LVS_LVS1_cell1_mode_buffer[LVS_LVS1_cell1_buffer_index];
}
double LVS_LVS1_update_c2md() {
  return LVS_LVS1_cell2_mode_buffer[LVS_LVS1_cell2_buffer_index];
}
double LVS_LVS1_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  LVS_LVS1_cell1_v_buffer[LVS_LVS1_cell1_buffer_index] = c1v;
  LVS_LVS1_cell2_v_buffer[LVS_LVS1_cell2_buffer_index] = c2v;
  LVS_LVS1_cell1_mode_buffer[LVS_LVS1_cell1_buffer_index] = c1m;
  LVS_LVS1_cell2_mode_buffer[LVS_LVS1_cell2_buffer_index] = c2m;
  LVS_LVS1_cell1_buffer_index = ((LVS_LVS1_cell1_buffer_index + 1) >= LVS_LVS1_PC1NI) ? 0 : ++LVS_LVS1_cell1_buffer_index;
  LVS_LVS1_cell2_buffer_index = ((LVS_LVS1_cell2_buffer_index + 1) >= LVS_LVS1_PC2NI) ? 0 : ++LVS_LVS1_cell2_buffer_index;
  return 0.0;
}
double LVS_LVS1_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double LVS_LVS1_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double LVS_LVS1_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double LVS_LVS1_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define LVS1_CSLV_PC1NI ((int)20.0)
#define LVS1_CSLV_PC2NI ((int)20.0)
static double* LVS1_CSLV_cell1_v_buffer;
static double* LVS1_CSLV_cell2_v_buffer;
static unsigned char* LVS1_CSLV_cell1_mode_buffer;
static unsigned char* LVS1_CSLV_cell2_mode_buffer;
static unsigned int LVS1_CSLV_cell1_buffer_index;
static unsigned int LVS1_CSLV_cell2_buffer_index;
void LVS1_CSLV_init() {
  LVS1_CSLV_cell1_v_buffer = (double*)calloc(LVS1_CSLV_PC1NI, sizeof(double));
  LVS1_CSLV_cell1_mode_buffer = (unsigned char*)calloc(LVS1_CSLV_PC1NI, sizeof(unsigned char));
  LVS1_CSLV_cell2_v_buffer = (double*)calloc(LVS1_CSLV_PC2NI, sizeof(double));
  LVS1_CSLV_cell2_mode_buffer = (unsigned char*)calloc(LVS1_CSLV_PC2NI, sizeof(unsigned char));
  LVS1_CSLV_cell1_buffer_index = 0;
  LVS1_CSLV_cell2_buffer_index = 0;
}
double LVS1_CSLV_update_c1vd() {
  return LVS1_CSLV_cell1_v_buffer[LVS1_CSLV_cell1_buffer_index];
}
double LVS1_CSLV_update_c2vd() {
  return LVS1_CSLV_cell2_v_buffer[LVS1_CSLV_cell2_buffer_index];
}
double LVS1_CSLV_update_c1md() {
  return LVS1_CSLV_cell1_mode_buffer[LVS1_CSLV_cell1_buffer_index];
}
double LVS1_CSLV_update_c2md() {
  return LVS1_CSLV_cell2_mode_buffer[LVS1_CSLV_cell2_buffer_index];
}
double LVS1_CSLV_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  LVS1_CSLV_cell1_v_buffer[LVS1_CSLV_cell1_buffer_index] = c1v;
  LVS1_CSLV_cell2_v_buffer[LVS1_CSLV_cell2_buffer_index] = c2v;
  LVS1_CSLV_cell1_mode_buffer[LVS1_CSLV_cell1_buffer_index] = c1m;
  LVS1_CSLV_cell2_mode_buffer[LVS1_CSLV_cell2_buffer_index] = c2m;
  LVS1_CSLV_cell1_buffer_index = ((LVS1_CSLV_cell1_buffer_index + 1) >= LVS1_CSLV_PC1NI) ? 0 : ++LVS1_CSLV_cell1_buffer_index;
  LVS1_CSLV_cell2_buffer_index = ((LVS1_CSLV_cell2_buffer_index + 1) >= LVS1_CSLV_PC2NI) ? 0 : ++LVS1_CSLV_cell2_buffer_index;
  return 0.0;
}
double LVS1_CSLV_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double LVS1_CSLV_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double LVS1_CSLV_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double LVS1_CSLV_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define RVA_RV_PC1NI ((int)10.0)
#define RVA_RV_PC2NI ((int)10.0)
static double* RVA_RV_cell1_v_buffer;
static double* RVA_RV_cell2_v_buffer;
static unsigned char* RVA_RV_cell1_mode_buffer;
static unsigned char* RVA_RV_cell2_mode_buffer;
static unsigned int RVA_RV_cell1_buffer_index;
static unsigned int RVA_RV_cell2_buffer_index;
void RVA_RV_init() {
  RVA_RV_cell1_v_buffer = (double*)calloc(RVA_RV_PC1NI, sizeof(double));
  RVA_RV_cell1_mode_buffer = (unsigned char*)calloc(RVA_RV_PC1NI, sizeof(unsigned char));
  RVA_RV_cell2_v_buffer = (double*)calloc(RVA_RV_PC2NI, sizeof(double));
  RVA_RV_cell2_mode_buffer = (unsigned char*)calloc(RVA_RV_PC2NI, sizeof(unsigned char));
  RVA_RV_cell1_buffer_index = 0;
  RVA_RV_cell2_buffer_index = 0;
}
double RVA_RV_update_c1vd() {
  return RVA_RV_cell1_v_buffer[RVA_RV_cell1_buffer_index];
}
double RVA_RV_update_c2vd() {
  return RVA_RV_cell2_v_buffer[RVA_RV_cell2_buffer_index];
}
double RVA_RV_update_c1md() {
  return RVA_RV_cell1_mode_buffer[RVA_RV_cell1_buffer_index];
}
double RVA_RV_update_c2md() {
  return RVA_RV_cell2_mode_buffer[RVA_RV_cell2_buffer_index];
}
double RVA_RV_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  RVA_RV_cell1_v_buffer[RVA_RV_cell1_buffer_index] = c1v;
  RVA_RV_cell2_v_buffer[RVA_RV_cell2_buffer_index] = c2v;
  RVA_RV_cell1_mode_buffer[RVA_RV_cell1_buffer_index] = c1m;
  RVA_RV_cell2_mode_buffer[RVA_RV_cell2_buffer_index] = c2m;
  RVA_RV_cell1_buffer_index = ((RVA_RV_cell1_buffer_index + 1) >= RVA_RV_PC1NI) ? 0 : ++RVA_RV_cell1_buffer_index;
  RVA_RV_cell2_buffer_index = ((RVA_RV_cell2_buffer_index + 1) >= RVA_RV_PC2NI) ? 0 : ++RVA_RV_cell2_buffer_index;
  return 0.0;
}
double RVA_RV_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double RVA_RV_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double RVA_RV_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double RVA_RV_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define RV_RV1_PC1NI ((int)20.0)
#define RV_RV1_PC2NI ((int)20.0)
static double* RV_RV1_cell1_v_buffer;
static double* RV_RV1_cell2_v_buffer;
static unsigned char* RV_RV1_cell1_mode_buffer;
static unsigned char* RV_RV1_cell2_mode_buffer;
static unsigned int RV_RV1_cell1_buffer_index;
static unsigned int RV_RV1_cell2_buffer_index;
void RV_RV1_init() {
  RV_RV1_cell1_v_buffer = (double*)calloc(RV_RV1_PC1NI, sizeof(double));
  RV_RV1_cell1_mode_buffer = (unsigned char*)calloc(RV_RV1_PC1NI, sizeof(unsigned char));
  RV_RV1_cell2_v_buffer = (double*)calloc(RV_RV1_PC2NI, sizeof(double));
  RV_RV1_cell2_mode_buffer = (unsigned char*)calloc(RV_RV1_PC2NI, sizeof(unsigned char));
  RV_RV1_cell1_buffer_index = 0;
  RV_RV1_cell2_buffer_index = 0;
}
double RV_RV1_update_c1vd() {
  return RV_RV1_cell1_v_buffer[RV_RV1_cell1_buffer_index];
}
double RV_RV1_update_c2vd() {
  return RV_RV1_cell2_v_buffer[RV_RV1_cell2_buffer_index];
}
double RV_RV1_update_c1md() {
  return RV_RV1_cell1_mode_buffer[RV_RV1_cell1_buffer_index];
}
double RV_RV1_update_c2md() {
  return RV_RV1_cell2_mode_buffer[RV_RV1_cell2_buffer_index];
}
double RV_RV1_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  RV_RV1_cell1_v_buffer[RV_RV1_cell1_buffer_index] = c1v;
  RV_RV1_cell2_v_buffer[RV_RV1_cell2_buffer_index] = c2v;
  RV_RV1_cell1_mode_buffer[RV_RV1_cell1_buffer_index] = c1m;
  RV_RV1_cell2_mode_buffer[RV_RV1_cell2_buffer_index] = c2m;
  RV_RV1_cell1_buffer_index = ((RV_RV1_cell1_buffer_index + 1) >= RV_RV1_PC1NI) ? 0 : ++RV_RV1_cell1_buffer_index;
  RV_RV1_cell2_buffer_index = ((RV_RV1_cell2_buffer_index + 1) >= RV_RV1_PC2NI) ? 0 : ++RV_RV1_cell2_buffer_index;
  return 0.0;
}
double RV_RV1_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double RV_RV1_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double RV_RV1_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double RV_RV1_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define RVA_RVS_PC1NI ((int)15.0)
#define RVA_RVS_PC2NI ((int)15.0)
static double* RVA_RVS_cell1_v_buffer;
static double* RVA_RVS_cell2_v_buffer;
static unsigned char* RVA_RVS_cell1_mode_buffer;
static unsigned char* RVA_RVS_cell2_mode_buffer;
static unsigned int RVA_RVS_cell1_buffer_index;
static unsigned int RVA_RVS_cell2_buffer_index;
void RVA_RVS_init() {
  RVA_RVS_cell1_v_buffer = (double*)calloc(RVA_RVS_PC1NI, sizeof(double));
  RVA_RVS_cell1_mode_buffer = (unsigned char*)calloc(RVA_RVS_PC1NI, sizeof(unsigned char));
  RVA_RVS_cell2_v_buffer = (double*)calloc(RVA_RVS_PC2NI, sizeof(double));
  RVA_RVS_cell2_mode_buffer = (unsigned char*)calloc(RVA_RVS_PC2NI, sizeof(unsigned char));
  RVA_RVS_cell1_buffer_index = 0;
  RVA_RVS_cell2_buffer_index = 0;
}
double RVA_RVS_update_c1vd() {
  return RVA_RVS_cell1_v_buffer[RVA_RVS_cell1_buffer_index];
}
double RVA_RVS_update_c2vd() {
  return RVA_RVS_cell2_v_buffer[RVA_RVS_cell2_buffer_index];
}
double RVA_RVS_update_c1md() {
  return RVA_RVS_cell1_mode_buffer[RVA_RVS_cell1_buffer_index];
}
double RVA_RVS_update_c2md() {
  return RVA_RVS_cell2_mode_buffer[RVA_RVS_cell2_buffer_index];
}
double RVA_RVS_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  RVA_RVS_cell1_v_buffer[RVA_RVS_cell1_buffer_index] = c1v;
  RVA_RVS_cell2_v_buffer[RVA_RVS_cell2_buffer_index] = c2v;
  RVA_RVS_cell1_mode_buffer[RVA_RVS_cell1_buffer_index] = c1m;
  RVA_RVS_cell2_mode_buffer[RVA_RVS_cell2_buffer_index] = c2m;
  RVA_RVS_cell1_buffer_index = ((RVA_RVS_cell1_buffer_index + 1) >= RVA_RVS_PC1NI) ? 0 : ++RVA_RVS_cell1_buffer_index;
  RVA_RVS_cell2_buffer_index = ((RVA_RVS_cell2_buffer_index + 1) >= RVA_RVS_PC2NI) ? 0 : ++RVA_RVS_cell2_buffer_index;
  return 0.0;
}
double RVA_RVS_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double RVA_RVS_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double RVA_RVS_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double RVA_RVS_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}


#include<stdlib.h>
#define RVS_RVS1_PC1NI ((int)20.0)
#define RVS_RVS1_PC2NI ((int)20.0)
static double* RVS_RVS1_cell1_v_buffer;
static double* RVS_RVS1_cell2_v_buffer;
static unsigned char* RVS_RVS1_cell1_mode_buffer;
static unsigned char* RVS_RVS1_cell2_mode_buffer;
static unsigned int RVS_RVS1_cell1_buffer_index;
static unsigned int RVS_RVS1_cell2_buffer_index;
void RVS_RVS1_init() {
  RVS_RVS1_cell1_v_buffer = (double*)calloc(RVS_RVS1_PC1NI, sizeof(double));
  RVS_RVS1_cell1_mode_buffer = (unsigned char*)calloc(RVS_RVS1_PC1NI, sizeof(unsigned char));
  RVS_RVS1_cell2_v_buffer = (double*)calloc(RVS_RVS1_PC2NI, sizeof(double));
  RVS_RVS1_cell2_mode_buffer = (unsigned char*)calloc(RVS_RVS1_PC2NI, sizeof(unsigned char));
  RVS_RVS1_cell1_buffer_index = 0;
  RVS_RVS1_cell2_buffer_index = 0;
}
double RVS_RVS1_update_c1vd() {
  return RVS_RVS1_cell1_v_buffer[RVS_RVS1_cell1_buffer_index];
}
double RVS_RVS1_update_c2vd() {
  return RVS_RVS1_cell2_v_buffer[RVS_RVS1_cell2_buffer_index];
}
double RVS_RVS1_update_c1md() {
  return RVS_RVS1_cell1_mode_buffer[RVS_RVS1_cell1_buffer_index];
}
double RVS_RVS1_update_c2md() {
  return RVS_RVS1_cell2_mode_buffer[RVS_RVS1_cell2_buffer_index];
}
double RVS_RVS1_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {
  RVS_RVS1_cell1_v_buffer[RVS_RVS1_cell1_buffer_index] = c1v;
  RVS_RVS1_cell2_v_buffer[RVS_RVS1_cell2_buffer_index] = c2v;
  RVS_RVS1_cell1_mode_buffer[RVS_RVS1_cell1_buffer_index] = c1m;
  RVS_RVS1_cell2_mode_buffer[RVS_RVS1_cell2_buffer_index] = c2m;
  RVS_RVS1_cell1_buffer_index = ((RVS_RVS1_cell1_buffer_index + 1) >= RVS_RVS1_PC1NI) ? 0 : ++RVS_RVS1_cell1_buffer_index;
  RVS_RVS1_cell2_buffer_index = ((RVS_RVS1_cell2_buffer_index + 1) >= RVS_RVS1_PC2NI) ? 0 : ++RVS_RVS1_cell2_buffer_index;
  return 0.0;
}
double RVS_RVS1_update_ocell1(double c1vd, double c1lu) {
  return ((c1lu == 1) ? c1vd : 0);
}
double RVS_RVS1_update_ocell2(double c2vd, double c2lu) {
  return ((c2lu == 1) ? c2vd : 0);
}
double RVS_RVS1_update_latch1(double c1md, double c1lu) {
  return ((c1md == 0) ? 0 : c1lu);
}
double RVS_RVS1_update_latch2(double c2md, double c2lu) {
  return ((c2md == 0) ? 0 : c2lu);
}

