#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double AV_His_update_c1vd();
extern double AV_His_update_c2vd();
extern double AV_His_update_c1md();
extern double AV_His_update_c2md();
extern double AV_His_update_buffer_index(double,double,double,double);
extern double AV_His_update_latch1(double,double);
extern double AV_His_update_latch2(double,double);
extern double AV_His_update_ocell1(double,double);
extern double AV_His_update_ocell2(double,double);
double AV_His_cell1_v;
double AV_His_cell1_mode;
double AV_His_cell2_v;
double AV_His_cell2_mode;
double AV_His_cell1_v_replay = 0.0;
double AV_His_cell2_v_replay = 0.0;


static double  AV_His_k  =  0.0 ,  AV_His_cell1_mode_delayed  =  0.0 ,  AV_His_cell2_mode_delayed  =  0.0 ,  AV_His_from_cell  =  0.0 ,  AV_His_cell1_replay_latch  =  0.0 ,  AV_His_cell2_replay_latch  =  0.0 ,  AV_His_cell1_v_delayed  =  0.0 ,  AV_His_cell2_v_delayed  =  0.0 ,  AV_His_wasted  =  0.0 ; //the continuous vars
static double  AV_His_k_u , AV_His_cell1_mode_delayed_u , AV_His_cell2_mode_delayed_u , AV_His_from_cell_u , AV_His_cell1_replay_latch_u , AV_His_cell2_replay_latch_u , AV_His_cell1_v_delayed_u , AV_His_cell2_v_delayed_u , AV_His_wasted_u ; // and their updates
static double  AV_His_k_init , AV_His_cell1_mode_delayed_init , AV_His_cell2_mode_delayed_init , AV_His_from_cell_init , AV_His_cell1_replay_latch_init , AV_His_cell2_replay_latch_init , AV_His_cell1_v_delayed_init , AV_His_cell2_v_delayed_init , AV_His_wasted_init ; // and their inits
static double  slope_AV_His_k , slope_AV_His_cell1_mode_delayed , slope_AV_His_cell2_mode_delayed , slope_AV_His_from_cell , slope_AV_His_cell1_replay_latch , slope_AV_His_cell2_replay_latch , slope_AV_His_cell1_v_delayed , slope_AV_His_cell2_v_delayed , slope_AV_His_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { AV_His_idle , AV_His_annhilate , AV_His_previous_drection1 , AV_His_previous_direction2 , AV_His_wait_cell1 , AV_His_replay_cell1 , AV_His_replay_cell2 , AV_His_wait_cell2 }; // state declarations

enum states AV_His (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( AV_His_idle ):
    if (True == False) {;}
    else if  (AV_His_cell2_mode == (2.0) && (AV_His_cell1_mode != (2.0))) {
      AV_His_k_u = 1 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
      cstate =  AV_His_previous_direction2 ;
      force_init_update = False;
    }
    else if  (AV_His_cell1_mode == (2.0) && (AV_His_cell2_mode != (2.0))) {
      AV_His_k_u = 1 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
      cstate =  AV_His_previous_drection1 ;
      force_init_update = False;
    }
    else if  (AV_His_cell1_mode == (2.0) && (AV_His_cell2_mode == (2.0))) {
      AV_His_k_u = 1 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
      cstate =  AV_His_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) AV_His_k_init = AV_His_k ;
      slope_AV_His_k = 1 ;
      AV_His_k_u = (slope_AV_His_k * d) + AV_His_k ;
      /* Possible Saturation */
      
      
      
      cstate =  AV_His_idle ;
      force_init_update = False;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell1_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: AV_His!\n");
      exit(1);
    }
    break;
  case ( AV_His_annhilate ):
    if (True == False) {;}
    else if  (AV_His_cell1_mode != (2.0) && (AV_His_cell2_mode != (2.0))) {
      AV_His_k_u = 1 ;
      AV_His_from_cell_u = 0 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
      cstate =  AV_His_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) AV_His_k_init = AV_His_k ;
      slope_AV_His_k = 1 ;
      AV_His_k_u = (slope_AV_His_k * d) + AV_His_k ;
      /* Possible Saturation */
      
      
      
      cstate =  AV_His_annhilate ;
      force_init_update = False;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell1_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: AV_His!\n");
      exit(1);
    }
    break;
  case ( AV_His_previous_drection1 ):
    if (True == False) {;}
    else if  (AV_His_from_cell == (1.0)) {
      AV_His_k_u = 1 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
      cstate =  AV_His_wait_cell1 ;
      force_init_update = False;
    }
    else if  (AV_His_from_cell == (0.0)) {
      AV_His_k_u = 1 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
      cstate =  AV_His_wait_cell1 ;
      force_init_update = False;
    }
    else if  (AV_His_from_cell == (2.0) && (AV_His_cell2_mode_delayed == (0.0))) {
      AV_His_k_u = 1 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
      cstate =  AV_His_wait_cell1 ;
      force_init_update = False;
    }
    else if  (AV_His_from_cell == (2.0) && (AV_His_cell2_mode_delayed != (0.0))) {
      AV_His_k_u = 1 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
      cstate =  AV_His_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) AV_His_k_init = AV_His_k ;
      slope_AV_His_k = 1 ;
      AV_His_k_u = (slope_AV_His_k * d) + AV_His_k ;
      /* Possible Saturation */
      
      
      
      cstate =  AV_His_previous_drection1 ;
      force_init_update = False;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell1_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: AV_His!\n");
      exit(1);
    }
    break;
  case ( AV_His_previous_direction2 ):
    if (True == False) {;}
    else if  (AV_His_from_cell == (1.0) && (AV_His_cell1_mode_delayed != (0.0))) {
      AV_His_k_u = 1 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
      cstate =  AV_His_annhilate ;
      force_init_update = False;
    }
    else if  (AV_His_from_cell == (2.0)) {
      AV_His_k_u = 1 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
      cstate =  AV_His_replay_cell1 ;
      force_init_update = False;
    }
    else if  (AV_His_from_cell == (0.0)) {
      AV_His_k_u = 1 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
      cstate =  AV_His_replay_cell1 ;
      force_init_update = False;
    }
    else if  (AV_His_from_cell == (1.0) && (AV_His_cell1_mode_delayed == (0.0))) {
      AV_His_k_u = 1 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
      cstate =  AV_His_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) AV_His_k_init = AV_His_k ;
      slope_AV_His_k = 1 ;
      AV_His_k_u = (slope_AV_His_k * d) + AV_His_k ;
      /* Possible Saturation */
      
      
      
      cstate =  AV_His_previous_direction2 ;
      force_init_update = False;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell1_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: AV_His!\n");
      exit(1);
    }
    break;
  case ( AV_His_wait_cell1 ):
    if (True == False) {;}
    else if  (AV_His_cell2_mode == (2.0)) {
      AV_His_k_u = 1 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
      cstate =  AV_His_annhilate ;
      force_init_update = False;
    }
    else if  (AV_His_k >= (30.0)) {
      AV_His_from_cell_u = 1 ;
      AV_His_cell1_replay_latch_u = 1 ;
      AV_His_k_u = 1 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
      cstate =  AV_His_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) AV_His_k_init = AV_His_k ;
      slope_AV_His_k = 1 ;
      AV_His_k_u = (slope_AV_His_k * d) + AV_His_k ;
      /* Possible Saturation */
      
      
      
      cstate =  AV_His_wait_cell1 ;
      force_init_update = False;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell1_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: AV_His!\n");
      exit(1);
    }
    break;
  case ( AV_His_replay_cell1 ):
    if (True == False) {;}
    else if  (AV_His_cell1_mode == (2.0)) {
      AV_His_k_u = 1 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
      cstate =  AV_His_annhilate ;
      force_init_update = False;
    }
    else if  (AV_His_k >= (30.0)) {
      AV_His_from_cell_u = 2 ;
      AV_His_cell2_replay_latch_u = 1 ;
      AV_His_k_u = 1 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
      cstate =  AV_His_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) AV_His_k_init = AV_His_k ;
      slope_AV_His_k = 1 ;
      AV_His_k_u = (slope_AV_His_k * d) + AV_His_k ;
      /* Possible Saturation */
      
      
      
      cstate =  AV_His_replay_cell1 ;
      force_init_update = False;
      AV_His_cell1_replay_latch_u = 1 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell1_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: AV_His!\n");
      exit(1);
    }
    break;
  case ( AV_His_replay_cell2 ):
    if (True == False) {;}
    else if  (AV_His_k >= (10.0)) {
      AV_His_k_u = 1 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
      cstate =  AV_His_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) AV_His_k_init = AV_His_k ;
      slope_AV_His_k = 1 ;
      AV_His_k_u = (slope_AV_His_k * d) + AV_His_k ;
      /* Possible Saturation */
      
      
      
      cstate =  AV_His_replay_cell2 ;
      force_init_update = False;
      AV_His_cell2_replay_latch_u = 1 ;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell1_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: AV_His!\n");
      exit(1);
    }
    break;
  case ( AV_His_wait_cell2 ):
    if (True == False) {;}
    else if  (AV_His_k >= (10.0)) {
      AV_His_k_u = 1 ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
      cstate =  AV_His_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) AV_His_k_init = AV_His_k ;
      slope_AV_His_k = 1 ;
      AV_His_k_u = (slope_AV_His_k * d) + AV_His_k ;
      /* Possible Saturation */
      
      
      
      cstate =  AV_His_wait_cell2 ;
      force_init_update = False;
      AV_His_cell1_v_delayed_u = AV_His_update_c1vd () ;
      AV_His_cell2_v_delayed_u = AV_His_update_c2vd () ;
      AV_His_cell1_mode_delayed_u = AV_His_update_c1md () ;
      AV_His_cell2_mode_delayed_u = AV_His_update_c2md () ;
      AV_His_wasted_u = AV_His_update_buffer_index (AV_His_cell1_v,AV_His_cell2_v,AV_His_cell1_mode,AV_His_cell2_mode) ;
      AV_His_cell1_replay_latch_u = AV_His_update_latch1 (AV_His_cell1_mode_delayed,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_replay_latch_u = AV_His_update_latch2 (AV_His_cell2_mode_delayed,AV_His_cell2_replay_latch_u) ;
      AV_His_cell1_v_replay = AV_His_update_ocell1 (AV_His_cell1_v_delayed_u,AV_His_cell1_replay_latch_u) ;
      AV_His_cell2_v_replay = AV_His_update_ocell2 (AV_His_cell2_v_delayed_u,AV_His_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: AV_His!\n");
      exit(1);
    }
    break;
  }
  AV_His_k = AV_His_k_u;
  AV_His_cell1_mode_delayed = AV_His_cell1_mode_delayed_u;
  AV_His_cell2_mode_delayed = AV_His_cell2_mode_delayed_u;
  AV_His_from_cell = AV_His_from_cell_u;
  AV_His_cell1_replay_latch = AV_His_cell1_replay_latch_u;
  AV_His_cell2_replay_latch = AV_His_cell2_replay_latch_u;
  AV_His_cell1_v_delayed = AV_His_cell1_v_delayed_u;
  AV_His_cell2_v_delayed = AV_His_cell2_v_delayed_u;
  AV_His_wasted = AV_His_wasted_u;
  return cstate;
}