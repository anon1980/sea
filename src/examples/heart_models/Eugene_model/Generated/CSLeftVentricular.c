#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double CSLeftVentricular_v_i_0;
double CSLeftVentricular_v_i_1;
double CSLeftVentricular_voo = 0.0;
double CSLeftVentricular_state = 0.0;


static double  CSLeftVentricular_vx  =  0 ,  CSLeftVentricular_vy  =  0 ,  CSLeftVentricular_vz  =  0 ,  CSLeftVentricular_g  =  0 ,  CSLeftVentricular_v  =  0 ,  CSLeftVentricular_ft  =  0 ,  CSLeftVentricular_theta  =  0 ,  CSLeftVentricular_v_O  =  0 ; //the continuous vars
static double  CSLeftVentricular_vx_u , CSLeftVentricular_vy_u , CSLeftVentricular_vz_u , CSLeftVentricular_g_u , CSLeftVentricular_v_u , CSLeftVentricular_ft_u , CSLeftVentricular_theta_u , CSLeftVentricular_v_O_u ; // and their updates
static double  CSLeftVentricular_vx_init , CSLeftVentricular_vy_init , CSLeftVentricular_vz_init , CSLeftVentricular_g_init , CSLeftVentricular_v_init , CSLeftVentricular_ft_init , CSLeftVentricular_theta_init , CSLeftVentricular_v_O_init ; // and their inits
static double  slope_CSLeftVentricular_vx , slope_CSLeftVentricular_vy , slope_CSLeftVentricular_vz , slope_CSLeftVentricular_g , slope_CSLeftVentricular_v , slope_CSLeftVentricular_ft , slope_CSLeftVentricular_theta , slope_CSLeftVentricular_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { CSLeftVentricular_t1 , CSLeftVentricular_t2 , CSLeftVentricular_t3 , CSLeftVentricular_t4 }; // state declarations

enum states CSLeftVentricular (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( CSLeftVentricular_t1 ):
    if (True == False) {;}
    else if  (CSLeftVentricular_g > (44.5)) {
      CSLeftVentricular_vx_u = (0.3 * CSLeftVentricular_v) ;
      CSLeftVentricular_vy_u = 0 ;
      CSLeftVentricular_vz_u = (0.7 * CSLeftVentricular_v) ;
      CSLeftVentricular_g_u = ((((((((CSLeftVentricular_v_i_0 + (- ((CSLeftVentricular_vx + (- CSLeftVentricular_vy)) + CSLeftVentricular_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      CSLeftVentricular_theta_u = (CSLeftVentricular_v / 30.0) ;
      CSLeftVentricular_v_O_u = (131.1 + (- (80.1 * pow ( ((CSLeftVentricular_v / 30.0)) , (0.5) )))) ;
      CSLeftVentricular_ft_u = f (CSLeftVentricular_theta,4.0e-2) ;
      cstate =  CSLeftVentricular_t2 ;
      force_init_update = False;
    }

    else if ( CSLeftVentricular_v <= (44.5)
               && 
              CSLeftVentricular_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) CSLeftVentricular_vx_init = CSLeftVentricular_vx ;
      slope_CSLeftVentricular_vx = (CSLeftVentricular_vx * -8.7) ;
      CSLeftVentricular_vx_u = (slope_CSLeftVentricular_vx * d) + CSLeftVentricular_vx ;
      if ((pstate != cstate) || force_init_update) CSLeftVentricular_vy_init = CSLeftVentricular_vy ;
      slope_CSLeftVentricular_vy = (CSLeftVentricular_vy * -190.9) ;
      CSLeftVentricular_vy_u = (slope_CSLeftVentricular_vy * d) + CSLeftVentricular_vy ;
      if ((pstate != cstate) || force_init_update) CSLeftVentricular_vz_init = CSLeftVentricular_vz ;
      slope_CSLeftVentricular_vz = (CSLeftVentricular_vz * -190.4) ;
      CSLeftVentricular_vz_u = (slope_CSLeftVentricular_vz * d) + CSLeftVentricular_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  CSLeftVentricular_t1 ;
      force_init_update = False;
      CSLeftVentricular_g_u = ((((((((CSLeftVentricular_v_i_0 + (- ((CSLeftVentricular_vx + (- CSLeftVentricular_vy)) + CSLeftVentricular_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      CSLeftVentricular_v_u = ((CSLeftVentricular_vx + (- CSLeftVentricular_vy)) + CSLeftVentricular_vz) ;
      CSLeftVentricular_voo = ((CSLeftVentricular_vx + (- CSLeftVentricular_vy)) + CSLeftVentricular_vz) ;
      CSLeftVentricular_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CSLeftVentricular!\n");
      exit(1);
    }
    break;
  case ( CSLeftVentricular_t2 ):
    if (True == False) {;}
    else if  (CSLeftVentricular_v >= (44.5)) {
      CSLeftVentricular_vx_u = CSLeftVentricular_vx ;
      CSLeftVentricular_vy_u = CSLeftVentricular_vy ;
      CSLeftVentricular_vz_u = CSLeftVentricular_vz ;
      CSLeftVentricular_g_u = ((((((((CSLeftVentricular_v_i_0 + (- ((CSLeftVentricular_vx + (- CSLeftVentricular_vy)) + CSLeftVentricular_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  CSLeftVentricular_t3 ;
      force_init_update = False;
    }
    else if  (CSLeftVentricular_g <= (44.5)
               && 
              CSLeftVentricular_v < (44.5)) {
      CSLeftVentricular_vx_u = CSLeftVentricular_vx ;
      CSLeftVentricular_vy_u = CSLeftVentricular_vy ;
      CSLeftVentricular_vz_u = CSLeftVentricular_vz ;
      CSLeftVentricular_g_u = ((((((((CSLeftVentricular_v_i_0 + (- ((CSLeftVentricular_vx + (- CSLeftVentricular_vy)) + CSLeftVentricular_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  CSLeftVentricular_t1 ;
      force_init_update = False;
    }

    else if ( CSLeftVentricular_v < (44.5)
               && 
              CSLeftVentricular_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) CSLeftVentricular_vx_init = CSLeftVentricular_vx ;
      slope_CSLeftVentricular_vx = ((CSLeftVentricular_vx * -23.6) + (777200.0 * CSLeftVentricular_g)) ;
      CSLeftVentricular_vx_u = (slope_CSLeftVentricular_vx * d) + CSLeftVentricular_vx ;
      if ((pstate != cstate) || force_init_update) CSLeftVentricular_vy_init = CSLeftVentricular_vy ;
      slope_CSLeftVentricular_vy = ((CSLeftVentricular_vy * -45.5) + (58900.0 * CSLeftVentricular_g)) ;
      CSLeftVentricular_vy_u = (slope_CSLeftVentricular_vy * d) + CSLeftVentricular_vy ;
      if ((pstate != cstate) || force_init_update) CSLeftVentricular_vz_init = CSLeftVentricular_vz ;
      slope_CSLeftVentricular_vz = ((CSLeftVentricular_vz * -12.9) + (276600.0 * CSLeftVentricular_g)) ;
      CSLeftVentricular_vz_u = (slope_CSLeftVentricular_vz * d) + CSLeftVentricular_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  CSLeftVentricular_t2 ;
      force_init_update = False;
      CSLeftVentricular_g_u = ((((((((CSLeftVentricular_v_i_0 + (- ((CSLeftVentricular_vx + (- CSLeftVentricular_vy)) + CSLeftVentricular_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      CSLeftVentricular_v_u = ((CSLeftVentricular_vx + (- CSLeftVentricular_vy)) + CSLeftVentricular_vz) ;
      CSLeftVentricular_voo = ((CSLeftVentricular_vx + (- CSLeftVentricular_vy)) + CSLeftVentricular_vz) ;
      CSLeftVentricular_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CSLeftVentricular!\n");
      exit(1);
    }
    break;
  case ( CSLeftVentricular_t3 ):
    if (True == False) {;}
    else if  (CSLeftVentricular_v >= (131.1)) {
      CSLeftVentricular_vx_u = CSLeftVentricular_vx ;
      CSLeftVentricular_vy_u = CSLeftVentricular_vy ;
      CSLeftVentricular_vz_u = CSLeftVentricular_vz ;
      CSLeftVentricular_g_u = ((((((((CSLeftVentricular_v_i_0 + (- ((CSLeftVentricular_vx + (- CSLeftVentricular_vy)) + CSLeftVentricular_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  CSLeftVentricular_t4 ;
      force_init_update = False;
    }

    else if ( CSLeftVentricular_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) CSLeftVentricular_vx_init = CSLeftVentricular_vx ;
      slope_CSLeftVentricular_vx = (CSLeftVentricular_vx * -6.9) ;
      CSLeftVentricular_vx_u = (slope_CSLeftVentricular_vx * d) + CSLeftVentricular_vx ;
      if ((pstate != cstate) || force_init_update) CSLeftVentricular_vy_init = CSLeftVentricular_vy ;
      slope_CSLeftVentricular_vy = (CSLeftVentricular_vy * 75.9) ;
      CSLeftVentricular_vy_u = (slope_CSLeftVentricular_vy * d) + CSLeftVentricular_vy ;
      if ((pstate != cstate) || force_init_update) CSLeftVentricular_vz_init = CSLeftVentricular_vz ;
      slope_CSLeftVentricular_vz = (CSLeftVentricular_vz * 6826.5) ;
      CSLeftVentricular_vz_u = (slope_CSLeftVentricular_vz * d) + CSLeftVentricular_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  CSLeftVentricular_t3 ;
      force_init_update = False;
      CSLeftVentricular_g_u = ((((((((CSLeftVentricular_v_i_0 + (- ((CSLeftVentricular_vx + (- CSLeftVentricular_vy)) + CSLeftVentricular_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      CSLeftVentricular_v_u = ((CSLeftVentricular_vx + (- CSLeftVentricular_vy)) + CSLeftVentricular_vz) ;
      CSLeftVentricular_voo = ((CSLeftVentricular_vx + (- CSLeftVentricular_vy)) + CSLeftVentricular_vz) ;
      CSLeftVentricular_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CSLeftVentricular!\n");
      exit(1);
    }
    break;
  case ( CSLeftVentricular_t4 ):
    if (True == False) {;}
    else if  (CSLeftVentricular_v <= (30.0)) {
      CSLeftVentricular_vx_u = CSLeftVentricular_vx ;
      CSLeftVentricular_vy_u = CSLeftVentricular_vy ;
      CSLeftVentricular_vz_u = CSLeftVentricular_vz ;
      CSLeftVentricular_g_u = ((((((((CSLeftVentricular_v_i_0 + (- ((CSLeftVentricular_vx + (- CSLeftVentricular_vy)) + CSLeftVentricular_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      cstate =  CSLeftVentricular_t1 ;
      force_init_update = False;
    }

    else if ( CSLeftVentricular_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) CSLeftVentricular_vx_init = CSLeftVentricular_vx ;
      slope_CSLeftVentricular_vx = (CSLeftVentricular_vx * -33.2) ;
      CSLeftVentricular_vx_u = (slope_CSLeftVentricular_vx * d) + CSLeftVentricular_vx ;
      if ((pstate != cstate) || force_init_update) CSLeftVentricular_vy_init = CSLeftVentricular_vy ;
      slope_CSLeftVentricular_vy = ((CSLeftVentricular_vy * 11.0) * CSLeftVentricular_ft) ;
      CSLeftVentricular_vy_u = (slope_CSLeftVentricular_vy * d) + CSLeftVentricular_vy ;
      if ((pstate != cstate) || force_init_update) CSLeftVentricular_vz_init = CSLeftVentricular_vz ;
      slope_CSLeftVentricular_vz = ((CSLeftVentricular_vz * 2.0) * CSLeftVentricular_ft) ;
      CSLeftVentricular_vz_u = (slope_CSLeftVentricular_vz * d) + CSLeftVentricular_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  CSLeftVentricular_t4 ;
      force_init_update = False;
      CSLeftVentricular_g_u = ((((((((CSLeftVentricular_v_i_0 + (- ((CSLeftVentricular_vx + (- CSLeftVentricular_vy)) + CSLeftVentricular_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + 0) + 0) + 0) + 0) ;
      CSLeftVentricular_v_u = ((CSLeftVentricular_vx + (- CSLeftVentricular_vy)) + CSLeftVentricular_vz) ;
      CSLeftVentricular_voo = ((CSLeftVentricular_vx + (- CSLeftVentricular_vy)) + CSLeftVentricular_vz) ;
      CSLeftVentricular_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: CSLeftVentricular!\n");
      exit(1);
    }
    break;
  }
  CSLeftVentricular_vx = CSLeftVentricular_vx_u;
  CSLeftVentricular_vy = CSLeftVentricular_vy_u;
  CSLeftVentricular_vz = CSLeftVentricular_vz_u;
  CSLeftVentricular_g = CSLeftVentricular_g_u;
  CSLeftVentricular_v = CSLeftVentricular_v_u;
  CSLeftVentricular_ft = CSLeftVentricular_ft_u;
  CSLeftVentricular_theta = CSLeftVentricular_theta_u;
  CSLeftVentricular_v_O = CSLeftVentricular_v_O_u;
  return cstate;
}