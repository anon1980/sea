#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double LeftBundleBranch_v_i_0;
double LeftBundleBranch_v_i_1;
double LeftBundleBranch_v_i_2;
double LeftBundleBranch_voo = 0.0;
double LeftBundleBranch_state = 0.0;


static double  LeftBundleBranch_vx  =  0 ,  LeftBundleBranch_vy  =  0 ,  LeftBundleBranch_vz  =  0 ,  LeftBundleBranch_g  =  0 ,  LeftBundleBranch_v  =  0 ,  LeftBundleBranch_ft  =  0 ,  LeftBundleBranch_theta  =  0 ,  LeftBundleBranch_v_O  =  0 ; //the continuous vars
static double  LeftBundleBranch_vx_u , LeftBundleBranch_vy_u , LeftBundleBranch_vz_u , LeftBundleBranch_g_u , LeftBundleBranch_v_u , LeftBundleBranch_ft_u , LeftBundleBranch_theta_u , LeftBundleBranch_v_O_u ; // and their updates
static double  LeftBundleBranch_vx_init , LeftBundleBranch_vy_init , LeftBundleBranch_vz_init , LeftBundleBranch_g_init , LeftBundleBranch_v_init , LeftBundleBranch_ft_init , LeftBundleBranch_theta_init , LeftBundleBranch_v_O_init ; // and their inits
static double  slope_LeftBundleBranch_vx , slope_LeftBundleBranch_vy , slope_LeftBundleBranch_vz , slope_LeftBundleBranch_g , slope_LeftBundleBranch_v , slope_LeftBundleBranch_ft , slope_LeftBundleBranch_theta , slope_LeftBundleBranch_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { LeftBundleBranch_t1 , LeftBundleBranch_t2 , LeftBundleBranch_t3 , LeftBundleBranch_t4 }; // state declarations

enum states LeftBundleBranch (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( LeftBundleBranch_t1 ):
    if (True == False) {;}
    else if  (LeftBundleBranch_g > (44.5)) {
      LeftBundleBranch_vx_u = (0.3 * LeftBundleBranch_v) ;
      LeftBundleBranch_vy_u = 0 ;
      LeftBundleBranch_vz_u = (0.7 * LeftBundleBranch_v) ;
      LeftBundleBranch_g_u = ((((((((LeftBundleBranch_v_i_0 + (- ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftBundleBranch_v_i_1 + (- ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftBundleBranch_theta_u = (LeftBundleBranch_v / 30.0) ;
      LeftBundleBranch_v_O_u = (131.1 + (- (80.1 * pow ( ((LeftBundleBranch_v / 30.0)) , (0.5) )))) ;
      LeftBundleBranch_ft_u = f (LeftBundleBranch_theta,4.0e-2) ;
      cstate =  LeftBundleBranch_t2 ;
      force_init_update = False;
    }

    else if ( LeftBundleBranch_v <= (44.5)
               && 
              LeftBundleBranch_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) LeftBundleBranch_vx_init = LeftBundleBranch_vx ;
      slope_LeftBundleBranch_vx = (LeftBundleBranch_vx * -8.7) ;
      LeftBundleBranch_vx_u = (slope_LeftBundleBranch_vx * d) + LeftBundleBranch_vx ;
      if ((pstate != cstate) || force_init_update) LeftBundleBranch_vy_init = LeftBundleBranch_vy ;
      slope_LeftBundleBranch_vy = (LeftBundleBranch_vy * -190.9) ;
      LeftBundleBranch_vy_u = (slope_LeftBundleBranch_vy * d) + LeftBundleBranch_vy ;
      if ((pstate != cstate) || force_init_update) LeftBundleBranch_vz_init = LeftBundleBranch_vz ;
      slope_LeftBundleBranch_vz = (LeftBundleBranch_vz * -190.4) ;
      LeftBundleBranch_vz_u = (slope_LeftBundleBranch_vz * d) + LeftBundleBranch_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftBundleBranch_t1 ;
      force_init_update = False;
      LeftBundleBranch_g_u = ((((((((LeftBundleBranch_v_i_0 + (- ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftBundleBranch_v_i_1 + (- ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftBundleBranch_v_u = ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz) ;
      LeftBundleBranch_voo = ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz) ;
      LeftBundleBranch_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftBundleBranch!\n");
      exit(1);
    }
    break;
  case ( LeftBundleBranch_t2 ):
    if (True == False) {;}
    else if  (LeftBundleBranch_v >= (44.5)) {
      LeftBundleBranch_vx_u = LeftBundleBranch_vx ;
      LeftBundleBranch_vy_u = LeftBundleBranch_vy ;
      LeftBundleBranch_vz_u = LeftBundleBranch_vz ;
      LeftBundleBranch_g_u = ((((((((LeftBundleBranch_v_i_0 + (- ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftBundleBranch_v_i_1 + (- ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftBundleBranch_t3 ;
      force_init_update = False;
    }
    else if  (LeftBundleBranch_g <= (44.5)
               && 
              LeftBundleBranch_v < (44.5)) {
      LeftBundleBranch_vx_u = LeftBundleBranch_vx ;
      LeftBundleBranch_vy_u = LeftBundleBranch_vy ;
      LeftBundleBranch_vz_u = LeftBundleBranch_vz ;
      LeftBundleBranch_g_u = ((((((((LeftBundleBranch_v_i_0 + (- ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftBundleBranch_v_i_1 + (- ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftBundleBranch_t1 ;
      force_init_update = False;
    }

    else if ( LeftBundleBranch_v < (44.5)
               && 
              LeftBundleBranch_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftBundleBranch_vx_init = LeftBundleBranch_vx ;
      slope_LeftBundleBranch_vx = ((LeftBundleBranch_vx * -23.6) + (777200.0 * LeftBundleBranch_g)) ;
      LeftBundleBranch_vx_u = (slope_LeftBundleBranch_vx * d) + LeftBundleBranch_vx ;
      if ((pstate != cstate) || force_init_update) LeftBundleBranch_vy_init = LeftBundleBranch_vy ;
      slope_LeftBundleBranch_vy = ((LeftBundleBranch_vy * -45.5) + (58900.0 * LeftBundleBranch_g)) ;
      LeftBundleBranch_vy_u = (slope_LeftBundleBranch_vy * d) + LeftBundleBranch_vy ;
      if ((pstate != cstate) || force_init_update) LeftBundleBranch_vz_init = LeftBundleBranch_vz ;
      slope_LeftBundleBranch_vz = ((LeftBundleBranch_vz * -12.9) + (276600.0 * LeftBundleBranch_g)) ;
      LeftBundleBranch_vz_u = (slope_LeftBundleBranch_vz * d) + LeftBundleBranch_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftBundleBranch_t2 ;
      force_init_update = False;
      LeftBundleBranch_g_u = ((((((((LeftBundleBranch_v_i_0 + (- ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftBundleBranch_v_i_1 + (- ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftBundleBranch_v_u = ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz) ;
      LeftBundleBranch_voo = ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz) ;
      LeftBundleBranch_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftBundleBranch!\n");
      exit(1);
    }
    break;
  case ( LeftBundleBranch_t3 ):
    if (True == False) {;}
    else if  (LeftBundleBranch_v >= (131.1)) {
      LeftBundleBranch_vx_u = LeftBundleBranch_vx ;
      LeftBundleBranch_vy_u = LeftBundleBranch_vy ;
      LeftBundleBranch_vz_u = LeftBundleBranch_vz ;
      LeftBundleBranch_g_u = ((((((((LeftBundleBranch_v_i_0 + (- ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftBundleBranch_v_i_1 + (- ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftBundleBranch_t4 ;
      force_init_update = False;
    }

    else if ( LeftBundleBranch_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) LeftBundleBranch_vx_init = LeftBundleBranch_vx ;
      slope_LeftBundleBranch_vx = (LeftBundleBranch_vx * -6.9) ;
      LeftBundleBranch_vx_u = (slope_LeftBundleBranch_vx * d) + LeftBundleBranch_vx ;
      if ((pstate != cstate) || force_init_update) LeftBundleBranch_vy_init = LeftBundleBranch_vy ;
      slope_LeftBundleBranch_vy = (LeftBundleBranch_vy * 75.9) ;
      LeftBundleBranch_vy_u = (slope_LeftBundleBranch_vy * d) + LeftBundleBranch_vy ;
      if ((pstate != cstate) || force_init_update) LeftBundleBranch_vz_init = LeftBundleBranch_vz ;
      slope_LeftBundleBranch_vz = (LeftBundleBranch_vz * 6826.5) ;
      LeftBundleBranch_vz_u = (slope_LeftBundleBranch_vz * d) + LeftBundleBranch_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftBundleBranch_t3 ;
      force_init_update = False;
      LeftBundleBranch_g_u = ((((((((LeftBundleBranch_v_i_0 + (- ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftBundleBranch_v_i_1 + (- ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftBundleBranch_v_u = ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz) ;
      LeftBundleBranch_voo = ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz) ;
      LeftBundleBranch_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftBundleBranch!\n");
      exit(1);
    }
    break;
  case ( LeftBundleBranch_t4 ):
    if (True == False) {;}
    else if  (LeftBundleBranch_v <= (30.0)) {
      LeftBundleBranch_vx_u = LeftBundleBranch_vx ;
      LeftBundleBranch_vy_u = LeftBundleBranch_vy ;
      LeftBundleBranch_vz_u = LeftBundleBranch_vz ;
      LeftBundleBranch_g_u = ((((((((LeftBundleBranch_v_i_0 + (- ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftBundleBranch_v_i_1 + (- ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftBundleBranch_t1 ;
      force_init_update = False;
    }

    else if ( LeftBundleBranch_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftBundleBranch_vx_init = LeftBundleBranch_vx ;
      slope_LeftBundleBranch_vx = (LeftBundleBranch_vx * -33.2) ;
      LeftBundleBranch_vx_u = (slope_LeftBundleBranch_vx * d) + LeftBundleBranch_vx ;
      if ((pstate != cstate) || force_init_update) LeftBundleBranch_vy_init = LeftBundleBranch_vy ;
      slope_LeftBundleBranch_vy = ((LeftBundleBranch_vy * 11.0) * LeftBundleBranch_ft) ;
      LeftBundleBranch_vy_u = (slope_LeftBundleBranch_vy * d) + LeftBundleBranch_vy ;
      if ((pstate != cstate) || force_init_update) LeftBundleBranch_vz_init = LeftBundleBranch_vz ;
      slope_LeftBundleBranch_vz = ((LeftBundleBranch_vz * 2.0) * LeftBundleBranch_ft) ;
      LeftBundleBranch_vz_u = (slope_LeftBundleBranch_vz * d) + LeftBundleBranch_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftBundleBranch_t4 ;
      force_init_update = False;
      LeftBundleBranch_g_u = ((((((((LeftBundleBranch_v_i_0 + (- ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftBundleBranch_v_i_1 + (- ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftBundleBranch_v_u = ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz) ;
      LeftBundleBranch_voo = ((LeftBundleBranch_vx + (- LeftBundleBranch_vy)) + LeftBundleBranch_vz) ;
      LeftBundleBranch_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftBundleBranch!\n");
      exit(1);
    }
    break;
  }
  LeftBundleBranch_vx = LeftBundleBranch_vx_u;
  LeftBundleBranch_vy = LeftBundleBranch_vy_u;
  LeftBundleBranch_vz = LeftBundleBranch_vz_u;
  LeftBundleBranch_g = LeftBundleBranch_g_u;
  LeftBundleBranch_v = LeftBundleBranch_v_u;
  LeftBundleBranch_ft = LeftBundleBranch_ft_u;
  LeftBundleBranch_theta = LeftBundleBranch_theta_u;
  LeftBundleBranch_v_O = LeftBundleBranch_v_O_u;
  return cstate;
}