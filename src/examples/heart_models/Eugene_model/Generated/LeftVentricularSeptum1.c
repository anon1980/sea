#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double LeftVentricularSeptum1_v_i_0;
double LeftVentricularSeptum1_v_i_1;
double LeftVentricularSeptum1_v_i_2;
double LeftVentricularSeptum1_voo = 0.0;
double LeftVentricularSeptum1_state = 0.0;


static double  LeftVentricularSeptum1_vx  =  0 ,  LeftVentricularSeptum1_vy  =  0 ,  LeftVentricularSeptum1_vz  =  0 ,  LeftVentricularSeptum1_g  =  0 ,  LeftVentricularSeptum1_v  =  0 ,  LeftVentricularSeptum1_ft  =  0 ,  LeftVentricularSeptum1_theta  =  0 ,  LeftVentricularSeptum1_v_O  =  0 ; //the continuous vars
static double  LeftVentricularSeptum1_vx_u , LeftVentricularSeptum1_vy_u , LeftVentricularSeptum1_vz_u , LeftVentricularSeptum1_g_u , LeftVentricularSeptum1_v_u , LeftVentricularSeptum1_ft_u , LeftVentricularSeptum1_theta_u , LeftVentricularSeptum1_v_O_u ; // and their updates
static double  LeftVentricularSeptum1_vx_init , LeftVentricularSeptum1_vy_init , LeftVentricularSeptum1_vz_init , LeftVentricularSeptum1_g_init , LeftVentricularSeptum1_v_init , LeftVentricularSeptum1_ft_init , LeftVentricularSeptum1_theta_init , LeftVentricularSeptum1_v_O_init ; // and their inits
static double  slope_LeftVentricularSeptum1_vx , slope_LeftVentricularSeptum1_vy , slope_LeftVentricularSeptum1_vz , slope_LeftVentricularSeptum1_g , slope_LeftVentricularSeptum1_v , slope_LeftVentricularSeptum1_ft , slope_LeftVentricularSeptum1_theta , slope_LeftVentricularSeptum1_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { LeftVentricularSeptum1_t1 , LeftVentricularSeptum1_t2 , LeftVentricularSeptum1_t3 , LeftVentricularSeptum1_t4 }; // state declarations

enum states LeftVentricularSeptum1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( LeftVentricularSeptum1_t1 ):
    if (True == False) {;}
    else if  (LeftVentricularSeptum1_g > (44.5)) {
      LeftVentricularSeptum1_vx_u = (0.3 * LeftVentricularSeptum1_v) ;
      LeftVentricularSeptum1_vy_u = 0 ;
      LeftVentricularSeptum1_vz_u = (0.7 * LeftVentricularSeptum1_v) ;
      LeftVentricularSeptum1_g_u = ((((((((LeftVentricularSeptum1_v_i_0 + (- ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularSeptum1_v_i_1 + (- ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftVentricularSeptum1_theta_u = (LeftVentricularSeptum1_v / 30.0) ;
      LeftVentricularSeptum1_v_O_u = (131.1 + (- (80.1 * pow ( ((LeftVentricularSeptum1_v / 30.0)) , (0.5) )))) ;
      LeftVentricularSeptum1_ft_u = f (LeftVentricularSeptum1_theta,4.0e-2) ;
      cstate =  LeftVentricularSeptum1_t2 ;
      force_init_update = False;
    }

    else if ( LeftVentricularSeptum1_v <= (44.5)
               && 
              LeftVentricularSeptum1_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum1_vx_init = LeftVentricularSeptum1_vx ;
      slope_LeftVentricularSeptum1_vx = (LeftVentricularSeptum1_vx * -8.7) ;
      LeftVentricularSeptum1_vx_u = (slope_LeftVentricularSeptum1_vx * d) + LeftVentricularSeptum1_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum1_vy_init = LeftVentricularSeptum1_vy ;
      slope_LeftVentricularSeptum1_vy = (LeftVentricularSeptum1_vy * -190.9) ;
      LeftVentricularSeptum1_vy_u = (slope_LeftVentricularSeptum1_vy * d) + LeftVentricularSeptum1_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum1_vz_init = LeftVentricularSeptum1_vz ;
      slope_LeftVentricularSeptum1_vz = (LeftVentricularSeptum1_vz * -190.4) ;
      LeftVentricularSeptum1_vz_u = (slope_LeftVentricularSeptum1_vz * d) + LeftVentricularSeptum1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricularSeptum1_t1 ;
      force_init_update = False;
      LeftVentricularSeptum1_g_u = ((((((((LeftVentricularSeptum1_v_i_0 + (- ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularSeptum1_v_i_1 + (- ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftVentricularSeptum1_v_u = ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz) ;
      LeftVentricularSeptum1_voo = ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz) ;
      LeftVentricularSeptum1_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularSeptum1!\n");
      exit(1);
    }
    break;
  case ( LeftVentricularSeptum1_t2 ):
    if (True == False) {;}
    else if  (LeftVentricularSeptum1_v >= (44.5)) {
      LeftVentricularSeptum1_vx_u = LeftVentricularSeptum1_vx ;
      LeftVentricularSeptum1_vy_u = LeftVentricularSeptum1_vy ;
      LeftVentricularSeptum1_vz_u = LeftVentricularSeptum1_vz ;
      LeftVentricularSeptum1_g_u = ((((((((LeftVentricularSeptum1_v_i_0 + (- ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularSeptum1_v_i_1 + (- ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftVentricularSeptum1_t3 ;
      force_init_update = False;
    }
    else if  (LeftVentricularSeptum1_g <= (44.5)
               && 
              LeftVentricularSeptum1_v < (44.5)) {
      LeftVentricularSeptum1_vx_u = LeftVentricularSeptum1_vx ;
      LeftVentricularSeptum1_vy_u = LeftVentricularSeptum1_vy ;
      LeftVentricularSeptum1_vz_u = LeftVentricularSeptum1_vz ;
      LeftVentricularSeptum1_g_u = ((((((((LeftVentricularSeptum1_v_i_0 + (- ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularSeptum1_v_i_1 + (- ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftVentricularSeptum1_t1 ;
      force_init_update = False;
    }

    else if ( LeftVentricularSeptum1_v < (44.5)
               && 
              LeftVentricularSeptum1_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum1_vx_init = LeftVentricularSeptum1_vx ;
      slope_LeftVentricularSeptum1_vx = ((LeftVentricularSeptum1_vx * -23.6) + (777200.0 * LeftVentricularSeptum1_g)) ;
      LeftVentricularSeptum1_vx_u = (slope_LeftVentricularSeptum1_vx * d) + LeftVentricularSeptum1_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum1_vy_init = LeftVentricularSeptum1_vy ;
      slope_LeftVentricularSeptum1_vy = ((LeftVentricularSeptum1_vy * -45.5) + (58900.0 * LeftVentricularSeptum1_g)) ;
      LeftVentricularSeptum1_vy_u = (slope_LeftVentricularSeptum1_vy * d) + LeftVentricularSeptum1_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum1_vz_init = LeftVentricularSeptum1_vz ;
      slope_LeftVentricularSeptum1_vz = ((LeftVentricularSeptum1_vz * -12.9) + (276600.0 * LeftVentricularSeptum1_g)) ;
      LeftVentricularSeptum1_vz_u = (slope_LeftVentricularSeptum1_vz * d) + LeftVentricularSeptum1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricularSeptum1_t2 ;
      force_init_update = False;
      LeftVentricularSeptum1_g_u = ((((((((LeftVentricularSeptum1_v_i_0 + (- ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularSeptum1_v_i_1 + (- ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftVentricularSeptum1_v_u = ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz) ;
      LeftVentricularSeptum1_voo = ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz) ;
      LeftVentricularSeptum1_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularSeptum1!\n");
      exit(1);
    }
    break;
  case ( LeftVentricularSeptum1_t3 ):
    if (True == False) {;}
    else if  (LeftVentricularSeptum1_v >= (131.1)) {
      LeftVentricularSeptum1_vx_u = LeftVentricularSeptum1_vx ;
      LeftVentricularSeptum1_vy_u = LeftVentricularSeptum1_vy ;
      LeftVentricularSeptum1_vz_u = LeftVentricularSeptum1_vz ;
      LeftVentricularSeptum1_g_u = ((((((((LeftVentricularSeptum1_v_i_0 + (- ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularSeptum1_v_i_1 + (- ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftVentricularSeptum1_t4 ;
      force_init_update = False;
    }

    else if ( LeftVentricularSeptum1_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum1_vx_init = LeftVentricularSeptum1_vx ;
      slope_LeftVentricularSeptum1_vx = (LeftVentricularSeptum1_vx * -6.9) ;
      LeftVentricularSeptum1_vx_u = (slope_LeftVentricularSeptum1_vx * d) + LeftVentricularSeptum1_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum1_vy_init = LeftVentricularSeptum1_vy ;
      slope_LeftVentricularSeptum1_vy = (LeftVentricularSeptum1_vy * 75.9) ;
      LeftVentricularSeptum1_vy_u = (slope_LeftVentricularSeptum1_vy * d) + LeftVentricularSeptum1_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum1_vz_init = LeftVentricularSeptum1_vz ;
      slope_LeftVentricularSeptum1_vz = (LeftVentricularSeptum1_vz * 6826.5) ;
      LeftVentricularSeptum1_vz_u = (slope_LeftVentricularSeptum1_vz * d) + LeftVentricularSeptum1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricularSeptum1_t3 ;
      force_init_update = False;
      LeftVentricularSeptum1_g_u = ((((((((LeftVentricularSeptum1_v_i_0 + (- ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularSeptum1_v_i_1 + (- ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftVentricularSeptum1_v_u = ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz) ;
      LeftVentricularSeptum1_voo = ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz) ;
      LeftVentricularSeptum1_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularSeptum1!\n");
      exit(1);
    }
    break;
  case ( LeftVentricularSeptum1_t4 ):
    if (True == False) {;}
    else if  (LeftVentricularSeptum1_v <= (30.0)) {
      LeftVentricularSeptum1_vx_u = LeftVentricularSeptum1_vx ;
      LeftVentricularSeptum1_vy_u = LeftVentricularSeptum1_vy ;
      LeftVentricularSeptum1_vz_u = LeftVentricularSeptum1_vz ;
      LeftVentricularSeptum1_g_u = ((((((((LeftVentricularSeptum1_v_i_0 + (- ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularSeptum1_v_i_1 + (- ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftVentricularSeptum1_t1 ;
      force_init_update = False;
    }

    else if ( LeftVentricularSeptum1_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum1_vx_init = LeftVentricularSeptum1_vx ;
      slope_LeftVentricularSeptum1_vx = (LeftVentricularSeptum1_vx * -33.2) ;
      LeftVentricularSeptum1_vx_u = (slope_LeftVentricularSeptum1_vx * d) + LeftVentricularSeptum1_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum1_vy_init = LeftVentricularSeptum1_vy ;
      slope_LeftVentricularSeptum1_vy = ((LeftVentricularSeptum1_vy * 11.0) * LeftVentricularSeptum1_ft) ;
      LeftVentricularSeptum1_vy_u = (slope_LeftVentricularSeptum1_vy * d) + LeftVentricularSeptum1_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum1_vz_init = LeftVentricularSeptum1_vz ;
      slope_LeftVentricularSeptum1_vz = ((LeftVentricularSeptum1_vz * 2.0) * LeftVentricularSeptum1_ft) ;
      LeftVentricularSeptum1_vz_u = (slope_LeftVentricularSeptum1_vz * d) + LeftVentricularSeptum1_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricularSeptum1_t4 ;
      force_init_update = False;
      LeftVentricularSeptum1_g_u = ((((((((LeftVentricularSeptum1_v_i_0 + (- ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricularSeptum1_v_i_1 + (- ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftVentricularSeptum1_v_u = ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz) ;
      LeftVentricularSeptum1_voo = ((LeftVentricularSeptum1_vx + (- LeftVentricularSeptum1_vy)) + LeftVentricularSeptum1_vz) ;
      LeftVentricularSeptum1_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularSeptum1!\n");
      exit(1);
    }
    break;
  }
  LeftVentricularSeptum1_vx = LeftVentricularSeptum1_vx_u;
  LeftVentricularSeptum1_vy = LeftVentricularSeptum1_vy_u;
  LeftVentricularSeptum1_vz = LeftVentricularSeptum1_vz_u;
  LeftVentricularSeptum1_g = LeftVentricularSeptum1_g_u;
  LeftVentricularSeptum1_v = LeftVentricularSeptum1_v_u;
  LeftVentricularSeptum1_ft = LeftVentricularSeptum1_ft_u;
  LeftVentricularSeptum1_theta = LeftVentricularSeptum1_theta_u;
  LeftVentricularSeptum1_v_O = LeftVentricularSeptum1_v_O_u;
  return cstate;
}