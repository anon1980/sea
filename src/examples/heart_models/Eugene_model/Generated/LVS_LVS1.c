#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double LVS_LVS1_update_c1vd();
extern double LVS_LVS1_update_c2vd();
extern double LVS_LVS1_update_c1md();
extern double LVS_LVS1_update_c2md();
extern double LVS_LVS1_update_buffer_index(double,double,double,double);
extern double LVS_LVS1_update_latch1(double,double);
extern double LVS_LVS1_update_latch2(double,double);
extern double LVS_LVS1_update_ocell1(double,double);
extern double LVS_LVS1_update_ocell2(double,double);
double LVS_LVS1_cell1_v;
double LVS_LVS1_cell1_mode;
double LVS_LVS1_cell2_v;
double LVS_LVS1_cell2_mode;
double LVS_LVS1_cell1_v_replay = 0.0;
double LVS_LVS1_cell2_v_replay = 0.0;


static double  LVS_LVS1_k  =  0.0 ,  LVS_LVS1_cell1_mode_delayed  =  0.0 ,  LVS_LVS1_cell2_mode_delayed  =  0.0 ,  LVS_LVS1_from_cell  =  0.0 ,  LVS_LVS1_cell1_replay_latch  =  0.0 ,  LVS_LVS1_cell2_replay_latch  =  0.0 ,  LVS_LVS1_cell1_v_delayed  =  0.0 ,  LVS_LVS1_cell2_v_delayed  =  0.0 ,  LVS_LVS1_wasted  =  0.0 ; //the continuous vars
static double  LVS_LVS1_k_u , LVS_LVS1_cell1_mode_delayed_u , LVS_LVS1_cell2_mode_delayed_u , LVS_LVS1_from_cell_u , LVS_LVS1_cell1_replay_latch_u , LVS_LVS1_cell2_replay_latch_u , LVS_LVS1_cell1_v_delayed_u , LVS_LVS1_cell2_v_delayed_u , LVS_LVS1_wasted_u ; // and their updates
static double  LVS_LVS1_k_init , LVS_LVS1_cell1_mode_delayed_init , LVS_LVS1_cell2_mode_delayed_init , LVS_LVS1_from_cell_init , LVS_LVS1_cell1_replay_latch_init , LVS_LVS1_cell2_replay_latch_init , LVS_LVS1_cell1_v_delayed_init , LVS_LVS1_cell2_v_delayed_init , LVS_LVS1_wasted_init ; // and their inits
static double  slope_LVS_LVS1_k , slope_LVS_LVS1_cell1_mode_delayed , slope_LVS_LVS1_cell2_mode_delayed , slope_LVS_LVS1_from_cell , slope_LVS_LVS1_cell1_replay_latch , slope_LVS_LVS1_cell2_replay_latch , slope_LVS_LVS1_cell1_v_delayed , slope_LVS_LVS1_cell2_v_delayed , slope_LVS_LVS1_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { LVS_LVS1_idle , LVS_LVS1_annhilate , LVS_LVS1_previous_drection1 , LVS_LVS1_previous_direction2 , LVS_LVS1_wait_cell1 , LVS_LVS1_replay_cell1 , LVS_LVS1_replay_cell2 , LVS_LVS1_wait_cell2 }; // state declarations

enum states LVS_LVS1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( LVS_LVS1_idle ):
    if (True == False) {;}
    else if  (LVS_LVS1_cell2_mode == (2.0) && (LVS_LVS1_cell1_mode != (2.0))) {
      LVS_LVS1_k_u = 1 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
      cstate =  LVS_LVS1_previous_direction2 ;
      force_init_update = False;
    }
    else if  (LVS_LVS1_cell1_mode == (2.0) && (LVS_LVS1_cell2_mode != (2.0))) {
      LVS_LVS1_k_u = 1 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
      cstate =  LVS_LVS1_previous_drection1 ;
      force_init_update = False;
    }
    else if  (LVS_LVS1_cell1_mode == (2.0) && (LVS_LVS1_cell2_mode == (2.0))) {
      LVS_LVS1_k_u = 1 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
      cstate =  LVS_LVS1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVS_LVS1_k_init = LVS_LVS1_k ;
      slope_LVS_LVS1_k = 1 ;
      LVS_LVS1_k_u = (slope_LVS_LVS1_k * d) + LVS_LVS1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVS_LVS1_idle ;
      force_init_update = False;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell1_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVS_LVS1!\n");
      exit(1);
    }
    break;
  case ( LVS_LVS1_annhilate ):
    if (True == False) {;}
    else if  (LVS_LVS1_cell1_mode != (2.0) && (LVS_LVS1_cell2_mode != (2.0))) {
      LVS_LVS1_k_u = 1 ;
      LVS_LVS1_from_cell_u = 0 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
      cstate =  LVS_LVS1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVS_LVS1_k_init = LVS_LVS1_k ;
      slope_LVS_LVS1_k = 1 ;
      LVS_LVS1_k_u = (slope_LVS_LVS1_k * d) + LVS_LVS1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVS_LVS1_annhilate ;
      force_init_update = False;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell1_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVS_LVS1!\n");
      exit(1);
    }
    break;
  case ( LVS_LVS1_previous_drection1 ):
    if (True == False) {;}
    else if  (LVS_LVS1_from_cell == (1.0)) {
      LVS_LVS1_k_u = 1 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
      cstate =  LVS_LVS1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LVS_LVS1_from_cell == (0.0)) {
      LVS_LVS1_k_u = 1 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
      cstate =  LVS_LVS1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LVS_LVS1_from_cell == (2.0) && (LVS_LVS1_cell2_mode_delayed == (0.0))) {
      LVS_LVS1_k_u = 1 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
      cstate =  LVS_LVS1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (LVS_LVS1_from_cell == (2.0) && (LVS_LVS1_cell2_mode_delayed != (0.0))) {
      LVS_LVS1_k_u = 1 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
      cstate =  LVS_LVS1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVS_LVS1_k_init = LVS_LVS1_k ;
      slope_LVS_LVS1_k = 1 ;
      LVS_LVS1_k_u = (slope_LVS_LVS1_k * d) + LVS_LVS1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVS_LVS1_previous_drection1 ;
      force_init_update = False;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell1_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVS_LVS1!\n");
      exit(1);
    }
    break;
  case ( LVS_LVS1_previous_direction2 ):
    if (True == False) {;}
    else if  (LVS_LVS1_from_cell == (1.0) && (LVS_LVS1_cell1_mode_delayed != (0.0))) {
      LVS_LVS1_k_u = 1 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
      cstate =  LVS_LVS1_annhilate ;
      force_init_update = False;
    }
    else if  (LVS_LVS1_from_cell == (2.0)) {
      LVS_LVS1_k_u = 1 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
      cstate =  LVS_LVS1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (LVS_LVS1_from_cell == (0.0)) {
      LVS_LVS1_k_u = 1 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
      cstate =  LVS_LVS1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (LVS_LVS1_from_cell == (1.0) && (LVS_LVS1_cell1_mode_delayed == (0.0))) {
      LVS_LVS1_k_u = 1 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
      cstate =  LVS_LVS1_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVS_LVS1_k_init = LVS_LVS1_k ;
      slope_LVS_LVS1_k = 1 ;
      LVS_LVS1_k_u = (slope_LVS_LVS1_k * d) + LVS_LVS1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVS_LVS1_previous_direction2 ;
      force_init_update = False;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell1_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVS_LVS1!\n");
      exit(1);
    }
    break;
  case ( LVS_LVS1_wait_cell1 ):
    if (True == False) {;}
    else if  (LVS_LVS1_cell2_mode == (2.0)) {
      LVS_LVS1_k_u = 1 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
      cstate =  LVS_LVS1_annhilate ;
      force_init_update = False;
    }
    else if  (LVS_LVS1_k >= (15.0)) {
      LVS_LVS1_from_cell_u = 1 ;
      LVS_LVS1_cell1_replay_latch_u = 1 ;
      LVS_LVS1_k_u = 1 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
      cstate =  LVS_LVS1_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVS_LVS1_k_init = LVS_LVS1_k ;
      slope_LVS_LVS1_k = 1 ;
      LVS_LVS1_k_u = (slope_LVS_LVS1_k * d) + LVS_LVS1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVS_LVS1_wait_cell1 ;
      force_init_update = False;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell1_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVS_LVS1!\n");
      exit(1);
    }
    break;
  case ( LVS_LVS1_replay_cell1 ):
    if (True == False) {;}
    else if  (LVS_LVS1_cell1_mode == (2.0)) {
      LVS_LVS1_k_u = 1 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
      cstate =  LVS_LVS1_annhilate ;
      force_init_update = False;
    }
    else if  (LVS_LVS1_k >= (15.0)) {
      LVS_LVS1_from_cell_u = 2 ;
      LVS_LVS1_cell2_replay_latch_u = 1 ;
      LVS_LVS1_k_u = 1 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
      cstate =  LVS_LVS1_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVS_LVS1_k_init = LVS_LVS1_k ;
      slope_LVS_LVS1_k = 1 ;
      LVS_LVS1_k_u = (slope_LVS_LVS1_k * d) + LVS_LVS1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVS_LVS1_replay_cell1 ;
      force_init_update = False;
      LVS_LVS1_cell1_replay_latch_u = 1 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell1_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVS_LVS1!\n");
      exit(1);
    }
    break;
  case ( LVS_LVS1_replay_cell2 ):
    if (True == False) {;}
    else if  (LVS_LVS1_k >= (10.0)) {
      LVS_LVS1_k_u = 1 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
      cstate =  LVS_LVS1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVS_LVS1_k_init = LVS_LVS1_k ;
      slope_LVS_LVS1_k = 1 ;
      LVS_LVS1_k_u = (slope_LVS_LVS1_k * d) + LVS_LVS1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVS_LVS1_replay_cell2 ;
      force_init_update = False;
      LVS_LVS1_cell2_replay_latch_u = 1 ;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell1_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVS_LVS1!\n");
      exit(1);
    }
    break;
  case ( LVS_LVS1_wait_cell2 ):
    if (True == False) {;}
    else if  (LVS_LVS1_k >= (10.0)) {
      LVS_LVS1_k_u = 1 ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
      cstate =  LVS_LVS1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVS_LVS1_k_init = LVS_LVS1_k ;
      slope_LVS_LVS1_k = 1 ;
      LVS_LVS1_k_u = (slope_LVS_LVS1_k * d) + LVS_LVS1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  LVS_LVS1_wait_cell2 ;
      force_init_update = False;
      LVS_LVS1_cell1_v_delayed_u = LVS_LVS1_update_c1vd () ;
      LVS_LVS1_cell2_v_delayed_u = LVS_LVS1_update_c2vd () ;
      LVS_LVS1_cell1_mode_delayed_u = LVS_LVS1_update_c1md () ;
      LVS_LVS1_cell2_mode_delayed_u = LVS_LVS1_update_c2md () ;
      LVS_LVS1_wasted_u = LVS_LVS1_update_buffer_index (LVS_LVS1_cell1_v,LVS_LVS1_cell2_v,LVS_LVS1_cell1_mode,LVS_LVS1_cell2_mode) ;
      LVS_LVS1_cell1_replay_latch_u = LVS_LVS1_update_latch1 (LVS_LVS1_cell1_mode_delayed,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_replay_latch_u = LVS_LVS1_update_latch2 (LVS_LVS1_cell2_mode_delayed,LVS_LVS1_cell2_replay_latch_u) ;
      LVS_LVS1_cell1_v_replay = LVS_LVS1_update_ocell1 (LVS_LVS1_cell1_v_delayed_u,LVS_LVS1_cell1_replay_latch_u) ;
      LVS_LVS1_cell2_v_replay = LVS_LVS1_update_ocell2 (LVS_LVS1_cell2_v_delayed_u,LVS_LVS1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LVS_LVS1!\n");
      exit(1);
    }
    break;
  }
  LVS_LVS1_k = LVS_LVS1_k_u;
  LVS_LVS1_cell1_mode_delayed = LVS_LVS1_cell1_mode_delayed_u;
  LVS_LVS1_cell2_mode_delayed = LVS_LVS1_cell2_mode_delayed_u;
  LVS_LVS1_from_cell = LVS_LVS1_from_cell_u;
  LVS_LVS1_cell1_replay_latch = LVS_LVS1_cell1_replay_latch_u;
  LVS_LVS1_cell2_replay_latch = LVS_LVS1_cell2_replay_latch_u;
  LVS_LVS1_cell1_v_delayed = LVS_LVS1_cell1_v_delayed_u;
  LVS_LVS1_cell2_v_delayed = LVS_LVS1_cell2_v_delayed_u;
  LVS_LVS1_wasted = LVS_LVS1_wasted_u;
  return cstate;
}