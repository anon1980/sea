#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double LeftVentricle_v_i_0;
double LeftVentricle_v_i_1;
double LeftVentricle_v_i_2;
double LeftVentricle_voo = 0.0;
double LeftVentricle_state = 0.0;


static double  LeftVentricle_vx  =  0 ,  LeftVentricle_vy  =  0 ,  LeftVentricle_vz  =  0 ,  LeftVentricle_g  =  0 ,  LeftVentricle_v  =  0 ,  LeftVentricle_ft  =  0 ,  LeftVentricle_theta  =  0 ,  LeftVentricle_v_O  =  0 ; //the continuous vars
static double  LeftVentricle_vx_u , LeftVentricle_vy_u , LeftVentricle_vz_u , LeftVentricle_g_u , LeftVentricle_v_u , LeftVentricle_ft_u , LeftVentricle_theta_u , LeftVentricle_v_O_u ; // and their updates
static double  LeftVentricle_vx_init , LeftVentricle_vy_init , LeftVentricle_vz_init , LeftVentricle_g_init , LeftVentricle_v_init , LeftVentricle_ft_init , LeftVentricle_theta_init , LeftVentricle_v_O_init ; // and their inits
static double  slope_LeftVentricle_vx , slope_LeftVentricle_vy , slope_LeftVentricle_vz , slope_LeftVentricle_g , slope_LeftVentricle_v , slope_LeftVentricle_ft , slope_LeftVentricle_theta , slope_LeftVentricle_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { LeftVentricle_t1 , LeftVentricle_t2 , LeftVentricle_t3 , LeftVentricle_t4 }; // state declarations

enum states LeftVentricle (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( LeftVentricle_t1 ):
    if (True == False) {;}
    else if  (LeftVentricle_g > (44.5)) {
      LeftVentricle_vx_u = (0.3 * LeftVentricle_v) ;
      LeftVentricle_vy_u = 0 ;
      LeftVentricle_vz_u = (0.7 * LeftVentricle_v) ;
      LeftVentricle_g_u = ((((((((LeftVentricle_v_i_0 + (- ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricle_v_i_1 + (- ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftVentricle_theta_u = (LeftVentricle_v / 30.0) ;
      LeftVentricle_v_O_u = (131.1 + (- (80.1 * pow ( ((LeftVentricle_v / 30.0)) , (0.5) )))) ;
      LeftVentricle_ft_u = f (LeftVentricle_theta,4.0e-2) ;
      cstate =  LeftVentricle_t2 ;
      force_init_update = False;
    }

    else if ( LeftVentricle_v <= (44.5)
               && 
              LeftVentricle_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricle_vx_init = LeftVentricle_vx ;
      slope_LeftVentricle_vx = (LeftVentricle_vx * -8.7) ;
      LeftVentricle_vx_u = (slope_LeftVentricle_vx * d) + LeftVentricle_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricle_vy_init = LeftVentricle_vy ;
      slope_LeftVentricle_vy = (LeftVentricle_vy * -190.9) ;
      LeftVentricle_vy_u = (slope_LeftVentricle_vy * d) + LeftVentricle_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricle_vz_init = LeftVentricle_vz ;
      slope_LeftVentricle_vz = (LeftVentricle_vz * -190.4) ;
      LeftVentricle_vz_u = (slope_LeftVentricle_vz * d) + LeftVentricle_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricle_t1 ;
      force_init_update = False;
      LeftVentricle_g_u = ((((((((LeftVentricle_v_i_0 + (- ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricle_v_i_1 + (- ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftVentricle_v_u = ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz) ;
      LeftVentricle_voo = ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz) ;
      LeftVentricle_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricle!\n");
      exit(1);
    }
    break;
  case ( LeftVentricle_t2 ):
    if (True == False) {;}
    else if  (LeftVentricle_v >= (44.5)) {
      LeftVentricle_vx_u = LeftVentricle_vx ;
      LeftVentricle_vy_u = LeftVentricle_vy ;
      LeftVentricle_vz_u = LeftVentricle_vz ;
      LeftVentricle_g_u = ((((((((LeftVentricle_v_i_0 + (- ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricle_v_i_1 + (- ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftVentricle_t3 ;
      force_init_update = False;
    }
    else if  (LeftVentricle_g <= (44.5)
               && LeftVentricle_v < (44.5)) {
      LeftVentricle_vx_u = LeftVentricle_vx ;
      LeftVentricle_vy_u = LeftVentricle_vy ;
      LeftVentricle_vz_u = LeftVentricle_vz ;
      LeftVentricle_g_u = ((((((((LeftVentricle_v_i_0 + (- ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricle_v_i_1 + (- ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftVentricle_t1 ;
      force_init_update = False;
    }

    else if ( LeftVentricle_v < (44.5)
               && 
              LeftVentricle_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricle_vx_init = LeftVentricle_vx ;
      slope_LeftVentricle_vx = ((LeftVentricle_vx * -23.6) + (777200.0 * LeftVentricle_g)) ;
      LeftVentricle_vx_u = (slope_LeftVentricle_vx * d) + LeftVentricle_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricle_vy_init = LeftVentricle_vy ;
      slope_LeftVentricle_vy = ((LeftVentricle_vy * -45.5) + (58900.0 * LeftVentricle_g)) ;
      LeftVentricle_vy_u = (slope_LeftVentricle_vy * d) + LeftVentricle_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricle_vz_init = LeftVentricle_vz ;
      slope_LeftVentricle_vz = ((LeftVentricle_vz * -12.9) + (276600.0 * LeftVentricle_g)) ;
      LeftVentricle_vz_u = (slope_LeftVentricle_vz * d) + LeftVentricle_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricle_t2 ;
      force_init_update = False;
      LeftVentricle_g_u = ((((((((LeftVentricle_v_i_0 + (- ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricle_v_i_1 + (- ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftVentricle_v_u = ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz) ;
      LeftVentricle_voo = ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz) ;
      LeftVentricle_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricle!\n");
      exit(1);
    }
    break;
  case ( LeftVentricle_t3 ):
    if (True == False) {;}
    else if  (LeftVentricle_v >= (131.1)) {
      LeftVentricle_vx_u = LeftVentricle_vx ;
      LeftVentricle_vy_u = LeftVentricle_vy ;
      LeftVentricle_vz_u = LeftVentricle_vz ;
      LeftVentricle_g_u = ((((((((LeftVentricle_v_i_0 + (- ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricle_v_i_1 + (- ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftVentricle_t4 ;
      force_init_update = False;
    }

    else if ( LeftVentricle_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricle_vx_init = LeftVentricle_vx ;
      slope_LeftVentricle_vx = (LeftVentricle_vx * -6.9) ;
      LeftVentricle_vx_u = (slope_LeftVentricle_vx * d) + LeftVentricle_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricle_vy_init = LeftVentricle_vy ;
      slope_LeftVentricle_vy = (LeftVentricle_vy * 75.9) ;
      LeftVentricle_vy_u = (slope_LeftVentricle_vy * d) + LeftVentricle_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricle_vz_init = LeftVentricle_vz ;
      slope_LeftVentricle_vz = (LeftVentricle_vz * 6826.5) ;
      LeftVentricle_vz_u = (slope_LeftVentricle_vz * d) + LeftVentricle_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricle_t3 ;
      force_init_update = False;
      LeftVentricle_g_u = ((((((((LeftVentricle_v_i_0 + (- ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricle_v_i_1 + (- ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftVentricle_v_u = ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz) ;
      LeftVentricle_voo = ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz) ;
      LeftVentricle_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricle!\n");
      exit(1);
    }
    break;
  case ( LeftVentricle_t4 ):
    if (True == False) {;}
    else if  (LeftVentricle_v <= (30.0)) {
      LeftVentricle_vx_u = LeftVentricle_vx ;
      LeftVentricle_vy_u = LeftVentricle_vy ;
      LeftVentricle_vz_u = LeftVentricle_vz ;
      LeftVentricle_g_u = ((((((((LeftVentricle_v_i_0 + (- ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricle_v_i_1 + (- ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      cstate =  LeftVentricle_t1 ;
      force_init_update = False;
    }

    else if ( LeftVentricle_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricle_vx_init = LeftVentricle_vx ;
      slope_LeftVentricle_vx = (LeftVentricle_vx * -33.2) ;
      LeftVentricle_vx_u = (slope_LeftVentricle_vx * d) + LeftVentricle_vx ;
      if ((pstate != cstate) || force_init_update) LeftVentricle_vy_init = LeftVentricle_vy ;
      slope_LeftVentricle_vy = ((LeftVentricle_vy * 11.0) * LeftVentricle_ft) ;
      LeftVentricle_vy_u = (slope_LeftVentricle_vy * d) + LeftVentricle_vy ;
      if ((pstate != cstate) || force_init_update) LeftVentricle_vz_init = LeftVentricle_vz ;
      slope_LeftVentricle_vz = ((LeftVentricle_vz * 2.0) * LeftVentricle_ft) ;
      LeftVentricle_vz_u = (slope_LeftVentricle_vz * d) + LeftVentricle_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  LeftVentricle_t4 ;
      force_init_update = False;
      LeftVentricle_g_u = ((((((((LeftVentricle_v_i_0 + (- ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0)) + ((((LeftVentricle_v_i_1 + (- ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + 0) + 0) + 0) ;
      LeftVentricle_v_u = ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz) ;
      LeftVentricle_voo = ((LeftVentricle_vx + (- LeftVentricle_vy)) + LeftVentricle_vz) ;
      LeftVentricle_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricle!\n");
      exit(1);
    }
    break;
  }
  LeftVentricle_vx = LeftVentricle_vx_u;
  LeftVentricle_vy = LeftVentricle_vy_u;
  LeftVentricle_vz = LeftVentricle_vz_u;
  LeftVentricle_g = LeftVentricle_g_u;
  LeftVentricle_v = LeftVentricle_v_u;
  LeftVentricle_ft = LeftVentricle_ft_u;
  LeftVentricle_theta = LeftVentricle_theta_u;
  LeftVentricle_v_O = LeftVentricle_v_O_u;
  return cstate;
}