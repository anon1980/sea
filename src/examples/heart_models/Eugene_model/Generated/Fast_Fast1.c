#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double Fast_Fast1_update_c1vd();
extern double Fast_Fast1_update_c2vd();
extern double Fast_Fast1_update_c1md();
extern double Fast_Fast1_update_c2md();
extern double Fast_Fast1_update_buffer_index(double,double,double,double);
extern double Fast_Fast1_update_latch1(double,double);
extern double Fast_Fast1_update_latch2(double,double);
extern double Fast_Fast1_update_ocell1(double,double);
extern double Fast_Fast1_update_ocell2(double,double);
double Fast_Fast1_cell1_v;
double Fast_Fast1_cell1_mode;
double Fast_Fast1_cell2_v;
double Fast_Fast1_cell2_mode;
double Fast_Fast1_cell1_v_replay = 0.0;
double Fast_Fast1_cell2_v_replay = 0.0;


static double  Fast_Fast1_k  =  0.0 ,  Fast_Fast1_cell1_mode_delayed  =  0.0 ,  Fast_Fast1_cell2_mode_delayed  =  0.0 ,  Fast_Fast1_from_cell  =  0.0 ,  Fast_Fast1_cell1_replay_latch  =  0.0 ,  Fast_Fast1_cell2_replay_latch  =  0.0 ,  Fast_Fast1_cell1_v_delayed  =  0.0 ,  Fast_Fast1_cell2_v_delayed  =  0.0 ,  Fast_Fast1_wasted  =  0.0 ; //the continuous vars
static double  Fast_Fast1_k_u , Fast_Fast1_cell1_mode_delayed_u , Fast_Fast1_cell2_mode_delayed_u , Fast_Fast1_from_cell_u , Fast_Fast1_cell1_replay_latch_u , Fast_Fast1_cell2_replay_latch_u , Fast_Fast1_cell1_v_delayed_u , Fast_Fast1_cell2_v_delayed_u , Fast_Fast1_wasted_u ; // and their updates
static double  Fast_Fast1_k_init , Fast_Fast1_cell1_mode_delayed_init , Fast_Fast1_cell2_mode_delayed_init , Fast_Fast1_from_cell_init , Fast_Fast1_cell1_replay_latch_init , Fast_Fast1_cell2_replay_latch_init , Fast_Fast1_cell1_v_delayed_init , Fast_Fast1_cell2_v_delayed_init , Fast_Fast1_wasted_init ; // and their inits
static double  slope_Fast_Fast1_k , slope_Fast_Fast1_cell1_mode_delayed , slope_Fast_Fast1_cell2_mode_delayed , slope_Fast_Fast1_from_cell , slope_Fast_Fast1_cell1_replay_latch , slope_Fast_Fast1_cell2_replay_latch , slope_Fast_Fast1_cell1_v_delayed , slope_Fast_Fast1_cell2_v_delayed , slope_Fast_Fast1_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { Fast_Fast1_idle , Fast_Fast1_annhilate , Fast_Fast1_previous_drection1 , Fast_Fast1_previous_direction2 , Fast_Fast1_wait_cell1 , Fast_Fast1_replay_cell1 , Fast_Fast1_replay_cell2 , Fast_Fast1_wait_cell2 }; // state declarations

enum states Fast_Fast1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( Fast_Fast1_idle ):
    if (True == False) {;}
    else if  (Fast_Fast1_cell2_mode == (2.0) && (Fast_Fast1_cell1_mode != (2.0))) {
      Fast_Fast1_k_u = 1 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
      cstate =  Fast_Fast1_previous_direction2 ;
      force_init_update = False;
    }
    else if  (Fast_Fast1_cell1_mode == (2.0) && (Fast_Fast1_cell2_mode != (2.0))) {
      Fast_Fast1_k_u = 1 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
      cstate =  Fast_Fast1_previous_drection1 ;
      force_init_update = False;
    }
    else if  (Fast_Fast1_cell1_mode == (2.0) && (Fast_Fast1_cell2_mode == (2.0))) {
      Fast_Fast1_k_u = 1 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
      cstate =  Fast_Fast1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Fast_Fast1_k_init = Fast_Fast1_k ;
      slope_Fast_Fast1_k = 1 ;
      Fast_Fast1_k_u = (slope_Fast_Fast1_k * d) + Fast_Fast1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Fast_Fast1_idle ;
      force_init_update = False;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell1_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast_Fast1!\n");
      exit(1);
    }
    break;
  case ( Fast_Fast1_annhilate ):
    if (True == False) {;}
    else if  (Fast_Fast1_cell1_mode != (2.0) && (Fast_Fast1_cell2_mode != (2.0))) {
      Fast_Fast1_k_u = 1 ;
      Fast_Fast1_from_cell_u = 0 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
      cstate =  Fast_Fast1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Fast_Fast1_k_init = Fast_Fast1_k ;
      slope_Fast_Fast1_k = 1 ;
      Fast_Fast1_k_u = (slope_Fast_Fast1_k * d) + Fast_Fast1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Fast_Fast1_annhilate ;
      force_init_update = False;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell1_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast_Fast1!\n");
      exit(1);
    }
    break;
  case ( Fast_Fast1_previous_drection1 ):
    if (True == False) {;}
    else if  (Fast_Fast1_from_cell == (1.0)) {
      Fast_Fast1_k_u = 1 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
      cstate =  Fast_Fast1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (Fast_Fast1_from_cell == (0.0)) {
      Fast_Fast1_k_u = 1 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
      cstate =  Fast_Fast1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (Fast_Fast1_from_cell == (2.0) && (Fast_Fast1_cell2_mode_delayed == (0.0))) {
      Fast_Fast1_k_u = 1 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
      cstate =  Fast_Fast1_wait_cell1 ;
      force_init_update = False;
    }
    else if  (Fast_Fast1_from_cell == (2.0) && (Fast_Fast1_cell2_mode_delayed != (0.0))) {
      Fast_Fast1_k_u = 1 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
      cstate =  Fast_Fast1_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Fast_Fast1_k_init = Fast_Fast1_k ;
      slope_Fast_Fast1_k = 1 ;
      Fast_Fast1_k_u = (slope_Fast_Fast1_k * d) + Fast_Fast1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Fast_Fast1_previous_drection1 ;
      force_init_update = False;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell1_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast_Fast1!\n");
      exit(1);
    }
    break;
  case ( Fast_Fast1_previous_direction2 ):
    if (True == False) {;}
    else if  (Fast_Fast1_from_cell == (1.0) && (Fast_Fast1_cell1_mode_delayed != (0.0))) {
      Fast_Fast1_k_u = 1 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
      cstate =  Fast_Fast1_annhilate ;
      force_init_update = False;
    }
    else if  (Fast_Fast1_from_cell == (2.0)) {
      Fast_Fast1_k_u = 1 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
      cstate =  Fast_Fast1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (Fast_Fast1_from_cell == (0.0)) {
      Fast_Fast1_k_u = 1 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
      cstate =  Fast_Fast1_replay_cell1 ;
      force_init_update = False;
    }
    else if  (Fast_Fast1_from_cell == (1.0) && (Fast_Fast1_cell1_mode_delayed == (0.0))) {
      Fast_Fast1_k_u = 1 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
      cstate =  Fast_Fast1_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Fast_Fast1_k_init = Fast_Fast1_k ;
      slope_Fast_Fast1_k = 1 ;
      Fast_Fast1_k_u = (slope_Fast_Fast1_k * d) + Fast_Fast1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Fast_Fast1_previous_direction2 ;
      force_init_update = False;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell1_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast_Fast1!\n");
      exit(1);
    }
    break;
  case ( Fast_Fast1_wait_cell1 ):
    if (True == False) {;}
    else if  (Fast_Fast1_cell2_mode == (2.0)) {
      Fast_Fast1_k_u = 1 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
      cstate =  Fast_Fast1_annhilate ;
      force_init_update = False;
    }
    else if  (Fast_Fast1_k >= (20.0)) {
      Fast_Fast1_from_cell_u = 1 ;
      Fast_Fast1_cell1_replay_latch_u = 1 ;
      Fast_Fast1_k_u = 1 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
      cstate =  Fast_Fast1_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Fast_Fast1_k_init = Fast_Fast1_k ;
      slope_Fast_Fast1_k = 1 ;
      Fast_Fast1_k_u = (slope_Fast_Fast1_k * d) + Fast_Fast1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Fast_Fast1_wait_cell1 ;
      force_init_update = False;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell1_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast_Fast1!\n");
      exit(1);
    }
    break;
  case ( Fast_Fast1_replay_cell1 ):
    if (True == False) {;}
    else if  (Fast_Fast1_cell1_mode == (2.0)) {
      Fast_Fast1_k_u = 1 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
      cstate =  Fast_Fast1_annhilate ;
      force_init_update = False;
    }
    else if  (Fast_Fast1_k >= (20.0)) {
      Fast_Fast1_from_cell_u = 2 ;
      Fast_Fast1_cell2_replay_latch_u = 1 ;
      Fast_Fast1_k_u = 1 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
      cstate =  Fast_Fast1_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Fast_Fast1_k_init = Fast_Fast1_k ;
      slope_Fast_Fast1_k = 1 ;
      Fast_Fast1_k_u = (slope_Fast_Fast1_k * d) + Fast_Fast1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Fast_Fast1_replay_cell1 ;
      force_init_update = False;
      Fast_Fast1_cell1_replay_latch_u = 1 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell1_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast_Fast1!\n");
      exit(1);
    }
    break;
  case ( Fast_Fast1_replay_cell2 ):
    if (True == False) {;}
    else if  (Fast_Fast1_k >= (10.0)) {
      Fast_Fast1_k_u = 1 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
      cstate =  Fast_Fast1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Fast_Fast1_k_init = Fast_Fast1_k ;
      slope_Fast_Fast1_k = 1 ;
      Fast_Fast1_k_u = (slope_Fast_Fast1_k * d) + Fast_Fast1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Fast_Fast1_replay_cell2 ;
      force_init_update = False;
      Fast_Fast1_cell2_replay_latch_u = 1 ;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell1_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast_Fast1!\n");
      exit(1);
    }
    break;
  case ( Fast_Fast1_wait_cell2 ):
    if (True == False) {;}
    else if  (Fast_Fast1_k >= (10.0)) {
      Fast_Fast1_k_u = 1 ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
      cstate =  Fast_Fast1_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Fast_Fast1_k_init = Fast_Fast1_k ;
      slope_Fast_Fast1_k = 1 ;
      Fast_Fast1_k_u = (slope_Fast_Fast1_k * d) + Fast_Fast1_k ;
      /* Possible Saturation */
      
      
      
      cstate =  Fast_Fast1_wait_cell2 ;
      force_init_update = False;
      Fast_Fast1_cell1_v_delayed_u = Fast_Fast1_update_c1vd () ;
      Fast_Fast1_cell2_v_delayed_u = Fast_Fast1_update_c2vd () ;
      Fast_Fast1_cell1_mode_delayed_u = Fast_Fast1_update_c1md () ;
      Fast_Fast1_cell2_mode_delayed_u = Fast_Fast1_update_c2md () ;
      Fast_Fast1_wasted_u = Fast_Fast1_update_buffer_index (Fast_Fast1_cell1_v,Fast_Fast1_cell2_v,Fast_Fast1_cell1_mode,Fast_Fast1_cell2_mode) ;
      Fast_Fast1_cell1_replay_latch_u = Fast_Fast1_update_latch1 (Fast_Fast1_cell1_mode_delayed,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_replay_latch_u = Fast_Fast1_update_latch2 (Fast_Fast1_cell2_mode_delayed,Fast_Fast1_cell2_replay_latch_u) ;
      Fast_Fast1_cell1_v_replay = Fast_Fast1_update_ocell1 (Fast_Fast1_cell1_v_delayed_u,Fast_Fast1_cell1_replay_latch_u) ;
      Fast_Fast1_cell2_v_replay = Fast_Fast1_update_ocell2 (Fast_Fast1_cell2_v_delayed_u,Fast_Fast1_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast_Fast1!\n");
      exit(1);
    }
    break;
  }
  Fast_Fast1_k = Fast_Fast1_k_u;
  Fast_Fast1_cell1_mode_delayed = Fast_Fast1_cell1_mode_delayed_u;
  Fast_Fast1_cell2_mode_delayed = Fast_Fast1_cell2_mode_delayed_u;
  Fast_Fast1_from_cell = Fast_Fast1_from_cell_u;
  Fast_Fast1_cell1_replay_latch = Fast_Fast1_cell1_replay_latch_u;
  Fast_Fast1_cell2_replay_latch = Fast_Fast1_cell2_replay_latch_u;
  Fast_Fast1_cell1_v_delayed = Fast_Fast1_cell1_v_delayed_u;
  Fast_Fast1_cell2_v_delayed = Fast_Fast1_cell2_v_delayed_u;
  Fast_Fast1_wasted = Fast_Fast1_wasted_u;
  return cstate;
}