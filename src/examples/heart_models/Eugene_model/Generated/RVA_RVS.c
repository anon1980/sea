#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double RVA_RVS_update_c1vd();
extern double RVA_RVS_update_c2vd();
extern double RVA_RVS_update_c1md();
extern double RVA_RVS_update_c2md();
extern double RVA_RVS_update_buffer_index(double,double,double,double);
extern double RVA_RVS_update_latch1(double,double);
extern double RVA_RVS_update_latch2(double,double);
extern double RVA_RVS_update_ocell1(double,double);
extern double RVA_RVS_update_ocell2(double,double);
double RVA_RVS_cell1_v;
double RVA_RVS_cell1_mode;
double RVA_RVS_cell2_v;
double RVA_RVS_cell2_mode;
double RVA_RVS_cell1_v_replay = 0.0;
double RVA_RVS_cell2_v_replay = 0.0;


static double  RVA_RVS_k  =  0.0 ,  RVA_RVS_cell1_mode_delayed  =  0.0 ,  RVA_RVS_cell2_mode_delayed  =  0.0 ,  RVA_RVS_from_cell  =  0.0 ,  RVA_RVS_cell1_replay_latch  =  0.0 ,  RVA_RVS_cell2_replay_latch  =  0.0 ,  RVA_RVS_cell1_v_delayed  =  0.0 ,  RVA_RVS_cell2_v_delayed  =  0.0 ,  RVA_RVS_wasted  =  0.0 ; //the continuous vars
static double  RVA_RVS_k_u , RVA_RVS_cell1_mode_delayed_u , RVA_RVS_cell2_mode_delayed_u , RVA_RVS_from_cell_u , RVA_RVS_cell1_replay_latch_u , RVA_RVS_cell2_replay_latch_u , RVA_RVS_cell1_v_delayed_u , RVA_RVS_cell2_v_delayed_u , RVA_RVS_wasted_u ; // and their updates
static double  RVA_RVS_k_init , RVA_RVS_cell1_mode_delayed_init , RVA_RVS_cell2_mode_delayed_init , RVA_RVS_from_cell_init , RVA_RVS_cell1_replay_latch_init , RVA_RVS_cell2_replay_latch_init , RVA_RVS_cell1_v_delayed_init , RVA_RVS_cell2_v_delayed_init , RVA_RVS_wasted_init ; // and their inits
static double  slope_RVA_RVS_k , slope_RVA_RVS_cell1_mode_delayed , slope_RVA_RVS_cell2_mode_delayed , slope_RVA_RVS_from_cell , slope_RVA_RVS_cell1_replay_latch , slope_RVA_RVS_cell2_replay_latch , slope_RVA_RVS_cell1_v_delayed , slope_RVA_RVS_cell2_v_delayed , slope_RVA_RVS_wasted ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { RVA_RVS_idle , RVA_RVS_annhilate , RVA_RVS_previous_drection1 , RVA_RVS_previous_direction2 , RVA_RVS_wait_cell1 , RVA_RVS_replay_cell1 , RVA_RVS_replay_cell2 , RVA_RVS_wait_cell2 }; // state declarations

enum states RVA_RVS (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( RVA_RVS_idle ):
    if (True == False) {;}
    else if  (RVA_RVS_cell2_mode == (2.0) && (RVA_RVS_cell1_mode != (2.0))) {
      RVA_RVS_k_u = 1 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
      cstate =  RVA_RVS_previous_direction2 ;
      force_init_update = False;
    }
    else if  (RVA_RVS_cell1_mode == (2.0) && (RVA_RVS_cell2_mode != (2.0))) {
      RVA_RVS_k_u = 1 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
      cstate =  RVA_RVS_previous_drection1 ;
      force_init_update = False;
    }
    else if  (RVA_RVS_cell1_mode == (2.0) && (RVA_RVS_cell2_mode == (2.0))) {
      RVA_RVS_k_u = 1 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
      cstate =  RVA_RVS_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVA_RVS_k_init = RVA_RVS_k ;
      slope_RVA_RVS_k = 1 ;
      RVA_RVS_k_u = (slope_RVA_RVS_k * d) + RVA_RVS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVA_RVS_idle ;
      force_init_update = False;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell1_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RVS!\n");
      exit(1);
    }
    break;
  case ( RVA_RVS_annhilate ):
    if (True == False) {;}
    else if  (RVA_RVS_cell1_mode != (2.0) && (RVA_RVS_cell2_mode != (2.0))) {
      RVA_RVS_k_u = 1 ;
      RVA_RVS_from_cell_u = 0 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
      cstate =  RVA_RVS_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVA_RVS_k_init = RVA_RVS_k ;
      slope_RVA_RVS_k = 1 ;
      RVA_RVS_k_u = (slope_RVA_RVS_k * d) + RVA_RVS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVA_RVS_annhilate ;
      force_init_update = False;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell1_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RVS!\n");
      exit(1);
    }
    break;
  case ( RVA_RVS_previous_drection1 ):
    if (True == False) {;}
    else if  (RVA_RVS_from_cell == (1.0)) {
      RVA_RVS_k_u = 1 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
      cstate =  RVA_RVS_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RVA_RVS_from_cell == (0.0)) {
      RVA_RVS_k_u = 1 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
      cstate =  RVA_RVS_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RVA_RVS_from_cell == (2.0) && (RVA_RVS_cell2_mode_delayed == (0.0))) {
      RVA_RVS_k_u = 1 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
      cstate =  RVA_RVS_wait_cell1 ;
      force_init_update = False;
    }
    else if  (RVA_RVS_from_cell == (2.0) && (RVA_RVS_cell2_mode_delayed != (0.0))) {
      RVA_RVS_k_u = 1 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
      cstate =  RVA_RVS_annhilate ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVA_RVS_k_init = RVA_RVS_k ;
      slope_RVA_RVS_k = 1 ;
      RVA_RVS_k_u = (slope_RVA_RVS_k * d) + RVA_RVS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVA_RVS_previous_drection1 ;
      force_init_update = False;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell1_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RVS!\n");
      exit(1);
    }
    break;
  case ( RVA_RVS_previous_direction2 ):
    if (True == False) {;}
    else if  (RVA_RVS_from_cell == (1.0) && (RVA_RVS_cell1_mode_delayed != (0.0))) {
      RVA_RVS_k_u = 1 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
      cstate =  RVA_RVS_annhilate ;
      force_init_update = False;
    }
    else if  (RVA_RVS_from_cell == (2.0)) {
      RVA_RVS_k_u = 1 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
      cstate =  RVA_RVS_replay_cell1 ;
      force_init_update = False;
    }
    else if  (RVA_RVS_from_cell == (0.0)) {
      RVA_RVS_k_u = 1 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
      cstate =  RVA_RVS_replay_cell1 ;
      force_init_update = False;
    }
    else if  (RVA_RVS_from_cell == (1.0) && (RVA_RVS_cell1_mode_delayed == (0.0))) {
      RVA_RVS_k_u = 1 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
      cstate =  RVA_RVS_replay_cell1 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVA_RVS_k_init = RVA_RVS_k ;
      slope_RVA_RVS_k = 1 ;
      RVA_RVS_k_u = (slope_RVA_RVS_k * d) + RVA_RVS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVA_RVS_previous_direction2 ;
      force_init_update = False;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell1_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RVS!\n");
      exit(1);
    }
    break;
  case ( RVA_RVS_wait_cell1 ):
    if (True == False) {;}
    else if  (RVA_RVS_cell2_mode == (2.0)) {
      RVA_RVS_k_u = 1 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
      cstate =  RVA_RVS_annhilate ;
      force_init_update = False;
    }
    else if  (RVA_RVS_k >= (15.0)) {
      RVA_RVS_from_cell_u = 1 ;
      RVA_RVS_cell1_replay_latch_u = 1 ;
      RVA_RVS_k_u = 1 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
      cstate =  RVA_RVS_replay_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVA_RVS_k_init = RVA_RVS_k ;
      slope_RVA_RVS_k = 1 ;
      RVA_RVS_k_u = (slope_RVA_RVS_k * d) + RVA_RVS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVA_RVS_wait_cell1 ;
      force_init_update = False;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell1_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RVS!\n");
      exit(1);
    }
    break;
  case ( RVA_RVS_replay_cell1 ):
    if (True == False) {;}
    else if  (RVA_RVS_cell1_mode == (2.0)) {
      RVA_RVS_k_u = 1 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
      cstate =  RVA_RVS_annhilate ;
      force_init_update = False;
    }
    else if  (RVA_RVS_k >= (15.0)) {
      RVA_RVS_from_cell_u = 2 ;
      RVA_RVS_cell2_replay_latch_u = 1 ;
      RVA_RVS_k_u = 1 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
      cstate =  RVA_RVS_wait_cell2 ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVA_RVS_k_init = RVA_RVS_k ;
      slope_RVA_RVS_k = 1 ;
      RVA_RVS_k_u = (slope_RVA_RVS_k * d) + RVA_RVS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVA_RVS_replay_cell1 ;
      force_init_update = False;
      RVA_RVS_cell1_replay_latch_u = 1 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell1_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RVS!\n");
      exit(1);
    }
    break;
  case ( RVA_RVS_replay_cell2 ):
    if (True == False) {;}
    else if  (RVA_RVS_k >= (10.0)) {
      RVA_RVS_k_u = 1 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
      cstate =  RVA_RVS_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVA_RVS_k_init = RVA_RVS_k ;
      slope_RVA_RVS_k = 1 ;
      RVA_RVS_k_u = (slope_RVA_RVS_k * d) + RVA_RVS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVA_RVS_replay_cell2 ;
      force_init_update = False;
      RVA_RVS_cell2_replay_latch_u = 1 ;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell1_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RVS!\n");
      exit(1);
    }
    break;
  case ( RVA_RVS_wait_cell2 ):
    if (True == False) {;}
    else if  (RVA_RVS_k >= (10.0)) {
      RVA_RVS_k_u = 1 ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
      cstate =  RVA_RVS_idle ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVA_RVS_k_init = RVA_RVS_k ;
      slope_RVA_RVS_k = 1 ;
      RVA_RVS_k_u = (slope_RVA_RVS_k * d) + RVA_RVS_k ;
      /* Possible Saturation */
      
      
      
      cstate =  RVA_RVS_wait_cell2 ;
      force_init_update = False;
      RVA_RVS_cell1_v_delayed_u = RVA_RVS_update_c1vd () ;
      RVA_RVS_cell2_v_delayed_u = RVA_RVS_update_c2vd () ;
      RVA_RVS_cell1_mode_delayed_u = RVA_RVS_update_c1md () ;
      RVA_RVS_cell2_mode_delayed_u = RVA_RVS_update_c2md () ;
      RVA_RVS_wasted_u = RVA_RVS_update_buffer_index (RVA_RVS_cell1_v,RVA_RVS_cell2_v,RVA_RVS_cell1_mode,RVA_RVS_cell2_mode) ;
      RVA_RVS_cell1_replay_latch_u = RVA_RVS_update_latch1 (RVA_RVS_cell1_mode_delayed,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_replay_latch_u = RVA_RVS_update_latch2 (RVA_RVS_cell2_mode_delayed,RVA_RVS_cell2_replay_latch_u) ;
      RVA_RVS_cell1_v_replay = RVA_RVS_update_ocell1 (RVA_RVS_cell1_v_delayed_u,RVA_RVS_cell1_replay_latch_u) ;
      RVA_RVS_cell2_v_replay = RVA_RVS_update_ocell2 (RVA_RVS_cell2_v_delayed_u,RVA_RVS_cell2_replay_latch_u) ;
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RVS!\n");
      exit(1);
    }
    break;
  }
  RVA_RVS_k = RVA_RVS_k_u;
  RVA_RVS_cell1_mode_delayed = RVA_RVS_cell1_mode_delayed_u;
  RVA_RVS_cell2_mode_delayed = RVA_RVS_cell2_mode_delayed_u;
  RVA_RVS_from_cell = RVA_RVS_from_cell_u;
  RVA_RVS_cell1_replay_latch = RVA_RVS_cell1_replay_latch_u;
  RVA_RVS_cell2_replay_latch = RVA_RVS_cell2_replay_latch_u;
  RVA_RVS_cell1_v_delayed = RVA_RVS_cell1_v_delayed_u;
  RVA_RVS_cell2_v_delayed = RVA_RVS_cell2_v_delayed_u;
  RVA_RVS_wasted = RVA_RVS_wasted_u;
  return cstate;
}