#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

extern double f(double,double);
double SinoatrialNode_v_i_0;
double SinoatrialNode_v_i_1;
double SinoatrialNode_v_i_2;
double SinoatrialNode_v_i_3;
double SinoatrialNode_v_i_4;
double SinoatrialNode_voo = 0.0;
double SinoatrialNode_state = 0.0;


static double  SinoatrialNode_vx  =  0 ,  SinoatrialNode_vy  =  0 ,  SinoatrialNode_vz  =  0 ,  SinoatrialNode_g  =  0 ,  SinoatrialNode_v  =  0 ,  SinoatrialNode_ft  =  0 ,  SinoatrialNode_theta  =  0 ,  SinoatrialNode_v_O  =  0 ; //the continuous vars
static double  SinoatrialNode_vx_u , SinoatrialNode_vy_u , SinoatrialNode_vz_u , SinoatrialNode_g_u , SinoatrialNode_v_u , SinoatrialNode_ft_u , SinoatrialNode_theta_u , SinoatrialNode_v_O_u ; // and their updates
static double  SinoatrialNode_vx_init , SinoatrialNode_vy_init , SinoatrialNode_vz_init , SinoatrialNode_g_init , SinoatrialNode_v_init , SinoatrialNode_ft_init , SinoatrialNode_theta_init , SinoatrialNode_v_O_init ; // and their inits
static double  slope_SinoatrialNode_vx , slope_SinoatrialNode_vy , slope_SinoatrialNode_vz , slope_SinoatrialNode_g , slope_SinoatrialNode_v , slope_SinoatrialNode_ft , slope_SinoatrialNode_theta , slope_SinoatrialNode_v_O ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { SinoatrialNode_t1 , SinoatrialNode_t2 , SinoatrialNode_t3 , SinoatrialNode_t4 }; // state declarations

enum states SinoatrialNode (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( SinoatrialNode_t1 ):
    if (True == False) {;}
    else if  (SinoatrialNode_g > (44.5)) {
      SinoatrialNode_vx_u = (0.3 * SinoatrialNode_v) ;
      SinoatrialNode_vy_u = 0 ;
      SinoatrialNode_vz_u = (0.7 * SinoatrialNode_v) ;
      SinoatrialNode_g_u = ((((((((SinoatrialNode_v_i_0 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0)) + ((((SinoatrialNode_v_i_1 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_2 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_3 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_4 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) ;
      SinoatrialNode_theta_u = (SinoatrialNode_v / 30.0) ;
      SinoatrialNode_v_O_u = (131.1 + (- (80.1 * pow ( ((SinoatrialNode_v / 30.0)) , (0.5) )))) ;
      SinoatrialNode_ft_u = f (SinoatrialNode_theta,4.0e-2) ;
      cstate =  SinoatrialNode_t2 ;
      force_init_update = False;
    }

    else if ( SinoatrialNode_v <= (44.5)
               && 
              SinoatrialNode_g <= (44.5)     ) {
      if ((pstate != cstate) || force_init_update) SinoatrialNode_vx_init = SinoatrialNode_vx ;
      slope_SinoatrialNode_vx = (SinoatrialNode_vx * -8.7) ;
      SinoatrialNode_vx_u = (slope_SinoatrialNode_vx * d) + SinoatrialNode_vx ;
      if ((pstate != cstate) || force_init_update) SinoatrialNode_vy_init = SinoatrialNode_vy ;
      slope_SinoatrialNode_vy = (SinoatrialNode_vy * -190.9) ;
      SinoatrialNode_vy_u = (slope_SinoatrialNode_vy * d) + SinoatrialNode_vy ;
      if ((pstate != cstate) || force_init_update) SinoatrialNode_vz_init = SinoatrialNode_vz ;
      slope_SinoatrialNode_vz = (SinoatrialNode_vz * -190.4) ;
      SinoatrialNode_vz_u = (slope_SinoatrialNode_vz * d) + SinoatrialNode_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  SinoatrialNode_t1 ;
      force_init_update = False;
      SinoatrialNode_g_u = ((((((((SinoatrialNode_v_i_0 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0)) + ((((SinoatrialNode_v_i_1 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_2 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_3 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_4 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) ;
      SinoatrialNode_v_u = ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz) ;
      SinoatrialNode_voo = ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz) ;
      SinoatrialNode_state = 0 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SinoatrialNode!\n");
      exit(1);
    }
    break;
  case ( SinoatrialNode_t2 ):
    if (True == False) {;}
    else if  (SinoatrialNode_v >= (44.5)) {
      SinoatrialNode_vx_u = SinoatrialNode_vx ;
      SinoatrialNode_vy_u = SinoatrialNode_vy ;
      SinoatrialNode_vz_u = SinoatrialNode_vz ;
      SinoatrialNode_g_u = ((((((((SinoatrialNode_v_i_0 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0)) + ((((SinoatrialNode_v_i_1 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_2 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_3 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_4 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) ;
      cstate =  SinoatrialNode_t3 ;
      force_init_update = False;
    }
    else if  (SinoatrialNode_g <= (44.5)
               && SinoatrialNode_v < (44.5)) {
      SinoatrialNode_vx_u = SinoatrialNode_vx ;
      SinoatrialNode_vy_u = SinoatrialNode_vy ;
      SinoatrialNode_vz_u = SinoatrialNode_vz ;
      SinoatrialNode_g_u = ((((((((SinoatrialNode_v_i_0 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0)) + ((((SinoatrialNode_v_i_1 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_2 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_3 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_4 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) ;
      cstate =  SinoatrialNode_t1 ;
      force_init_update = False;
    }

    else if ( SinoatrialNode_v < (44.5)
               && 
              SinoatrialNode_g > (0.0)     ) {
      if ((pstate != cstate) || force_init_update) SinoatrialNode_vx_init = SinoatrialNode_vx ;
      slope_SinoatrialNode_vx = ((SinoatrialNode_vx * -23.6) + (777200.0 * SinoatrialNode_g)) ;
      SinoatrialNode_vx_u = (slope_SinoatrialNode_vx * d) + SinoatrialNode_vx ;
      if ((pstate != cstate) || force_init_update) SinoatrialNode_vy_init = SinoatrialNode_vy ;
      slope_SinoatrialNode_vy = ((SinoatrialNode_vy * -45.5) + (58900.0 * SinoatrialNode_g)) ;
      SinoatrialNode_vy_u = (slope_SinoatrialNode_vy * d) + SinoatrialNode_vy ;
      if ((pstate != cstate) || force_init_update) SinoatrialNode_vz_init = SinoatrialNode_vz ;
      slope_SinoatrialNode_vz = ((SinoatrialNode_vz * -12.9) + (276600.0 * SinoatrialNode_g)) ;
      SinoatrialNode_vz_u = (slope_SinoatrialNode_vz * d) + SinoatrialNode_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  SinoatrialNode_t2 ;
      force_init_update = False;
      SinoatrialNode_g_u = ((((((((SinoatrialNode_v_i_0 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0)) + ((((SinoatrialNode_v_i_1 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_2 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_3 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_4 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) ;
      SinoatrialNode_v_u = ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz) ;
      SinoatrialNode_voo = ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz) ;
      SinoatrialNode_state = 1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SinoatrialNode!\n");
      exit(1);
    }
    break;
  case ( SinoatrialNode_t3 ):
    if (True == False) {;}
    else if  (SinoatrialNode_v >= (131.1)) {
      SinoatrialNode_vx_u = SinoatrialNode_vx ;
      SinoatrialNode_vy_u = SinoatrialNode_vy ;
      SinoatrialNode_vz_u = SinoatrialNode_vz ;
      SinoatrialNode_g_u = ((((((((SinoatrialNode_v_i_0 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0)) + ((((SinoatrialNode_v_i_1 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_2 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_3 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_4 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) ;
      cstate =  SinoatrialNode_t4 ;
      force_init_update = False;
    }

    else if ( SinoatrialNode_v < (131.1)     ) {
      if ((pstate != cstate) || force_init_update) SinoatrialNode_vx_init = SinoatrialNode_vx ;
      slope_SinoatrialNode_vx = (SinoatrialNode_vx * -6.9) ;
      SinoatrialNode_vx_u = (slope_SinoatrialNode_vx * d) + SinoatrialNode_vx ;
      if ((pstate != cstate) || force_init_update) SinoatrialNode_vy_init = SinoatrialNode_vy ;
      slope_SinoatrialNode_vy = (SinoatrialNode_vy * 75.9) ;
      SinoatrialNode_vy_u = (slope_SinoatrialNode_vy * d) + SinoatrialNode_vy ;
      if ((pstate != cstate) || force_init_update) SinoatrialNode_vz_init = SinoatrialNode_vz ;
      slope_SinoatrialNode_vz = (SinoatrialNode_vz * 6826.5) ;
      SinoatrialNode_vz_u = (slope_SinoatrialNode_vz * d) + SinoatrialNode_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  SinoatrialNode_t3 ;
      force_init_update = False;
      SinoatrialNode_g_u = ((((((((SinoatrialNode_v_i_0 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0)) + ((((SinoatrialNode_v_i_1 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_2 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_3 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_4 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) ;
      SinoatrialNode_v_u = ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz) ;
      SinoatrialNode_voo = ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz) ;
      SinoatrialNode_state = 2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SinoatrialNode!\n");
      exit(1);
    }
    break;
  case ( SinoatrialNode_t4 ):
    if (True == False) {;}
    else if  (SinoatrialNode_v <= (30.0)) {
      SinoatrialNode_vx_u = SinoatrialNode_vx ;
      SinoatrialNode_vy_u = SinoatrialNode_vy ;
      SinoatrialNode_vz_u = SinoatrialNode_vz ;
      SinoatrialNode_g_u = ((((((((SinoatrialNode_v_i_0 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0)) + ((((SinoatrialNode_v_i_1 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_2 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_3 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_4 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) ;
      cstate =  SinoatrialNode_t1 ;
      force_init_update = False;
    }

    else if ( SinoatrialNode_v > (30.0)     ) {
      if ((pstate != cstate) || force_init_update) SinoatrialNode_vx_init = SinoatrialNode_vx ;
      slope_SinoatrialNode_vx = (SinoatrialNode_vx * -33.2) ;
      SinoatrialNode_vx_u = (slope_SinoatrialNode_vx * d) + SinoatrialNode_vx ;
      if ((pstate != cstate) || force_init_update) SinoatrialNode_vy_init = SinoatrialNode_vy ;
      slope_SinoatrialNode_vy = ((SinoatrialNode_vy * 20.0) * SinoatrialNode_ft) ;
      SinoatrialNode_vy_u = (slope_SinoatrialNode_vy * d) + SinoatrialNode_vy ;
      if ((pstate != cstate) || force_init_update) SinoatrialNode_vz_init = SinoatrialNode_vz ;
      slope_SinoatrialNode_vz = ((SinoatrialNode_vz * 2.0) * SinoatrialNode_ft) ;
      SinoatrialNode_vz_u = (slope_SinoatrialNode_vz * d) + SinoatrialNode_vz ;
      /* Possible Saturation */
      
      
      
      
      
      cstate =  SinoatrialNode_t4 ;
      force_init_update = False;
      SinoatrialNode_g_u = ((((((((SinoatrialNode_v_i_0 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 1.0)) + ((((SinoatrialNode_v_i_1 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_2 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_3 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) + ((((SinoatrialNode_v_i_4 + (- ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz))) * 100.0) * 0.1) / ((100.0 * 1.0e-2) * 10.0))) ;
      SinoatrialNode_v_u = ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz) ;
      SinoatrialNode_voo = ((SinoatrialNode_vx + (- SinoatrialNode_vy)) + SinoatrialNode_vz) ;
      SinoatrialNode_state = 3 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: SinoatrialNode!\n");
      exit(1);
    }
    break;
  }
  SinoatrialNode_vx = SinoatrialNode_vx_u;
  SinoatrialNode_vy = SinoatrialNode_vy_u;
  SinoatrialNode_vz = SinoatrialNode_vz_u;
  SinoatrialNode_g = SinoatrialNode_g_u;
  SinoatrialNode_v = SinoatrialNode_v_u;
  SinoatrialNode_ft = SinoatrialNode_ft_u;
  SinoatrialNode_theta = SinoatrialNode_theta_u;
  SinoatrialNode_v_O = SinoatrialNode_v_O_u;
  return cstate;
}