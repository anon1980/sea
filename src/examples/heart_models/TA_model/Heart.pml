
c_decl {\#include<math.h>};
c_decl{extern double d;};
bool RVS_RVS1_tick = false;
bool RVA_RVS_tick = false;
bool RV_RV1_tick = false;
bool RVA_RV_tick = false;
bool LVS1_CSLV_tick = false;
bool LVS_LVS1_tick = false;
bool LVA_LVS_tick = false;
bool LV_LV1_tick = false;
bool LVA_LV_tick = false;
bool LVA_RVA_tick = false;
bool RBB1_RVA_tick = false;
bool RBB_RBB1_tick = false;
bool His2_RBB_tick = false;
bool LBB1_LVA_tick = false;
bool LBB_LBB1_tick = false;
bool His2_LBB_tick = false;
bool His1_His2_tick = false;
bool His_His1_tick = false;
bool AV_His_tick = false;
bool Slow1_AV_tick = false;
bool Fast1_AV_tick = false;
bool Slow_Slow1_tick = false;
bool OS_Slow_tick = false;
bool Fast_Fast1_tick = false;
bool OS_Fast_tick = false;
bool CT_CT1_tick = false;
bool RA1_CS_tick = false;
bool RA_RA1_tick = false;
bool LA_LA1_tick = false;
bool BB_LA_tick = false;
bool SA_CT_tick = false;
bool SA_RA_tick = false;
bool SA_OS_tick = false;
bool SA_BB_tick = false;
bool RightVentricularSeptum1_tick = false;
bool RightVentricularSeptum_tick = false;
bool RightVentricle1_tick = false;
bool RightVentricle_tick = false;
bool RightVentricularApex_tick = false;
bool RightBundleBranch1_tick = false;
bool RightBundleBranch_tick = false;
bool CSLeftVentricular_tick = false;
bool LeftVentricularSeptum1_tick = false;
bool LeftVentricularSeptum_tick = false;
bool LeftVentricle1_tick = false;
bool LeftVentricle_tick = false;
bool LeftVentricularApex_tick = false;
bool LeftBundleBranch1_tick = false;
bool LeftBundleBranch_tick = false;
bool BundleOfHis2_tick = false;
bool BundleOfHis1_tick = false;
bool BundleOfHis_tick = false;
bool AtrioventricularNode_tick = false;
bool Slow1_tick = false;
bool Slow_tick = false;
bool Fast1_tick = false;
bool Fast_tick = false;
bool Ostium_tick = false;
bool CristaTerminalis1_tick = false;
bool CristaTerminalis_tick = false;
bool CoronarySinus_tick = false;
bool RightAtrium1_tick = false;
bool RightAtrium_tick = false;
bool LeftAtrium1_tick = false;
bool LeftAtrium_tick = false;
bool BachmannBundle_tick = false;
bool SinoatrialNode_tick = false;

mtype RVS_RVS1_cstate , RVS_RVS1_pstate ;
bool RVS_RVS1_force_init_update ;

mtype RVA_RVS_cstate , RVA_RVS_pstate ;
bool RVA_RVS_force_init_update ;

mtype RV_RV1_cstate , RV_RV1_pstate ;
bool RV_RV1_force_init_update ;

mtype RVA_RV_cstate , RVA_RV_pstate ;
bool RVA_RV_force_init_update ;

mtype LVS1_CSLV_cstate , LVS1_CSLV_pstate ;
bool LVS1_CSLV_force_init_update ;

mtype LVS_LVS1_cstate , LVS_LVS1_pstate ;
bool LVS_LVS1_force_init_update ;

mtype LVA_LVS_cstate , LVA_LVS_pstate ;
bool LVA_LVS_force_init_update ;

mtype LV_LV1_cstate , LV_LV1_pstate ;
bool LV_LV1_force_init_update ;

mtype LVA_LV_cstate , LVA_LV_pstate ;
bool LVA_LV_force_init_update ;

mtype LVA_RVA_cstate , LVA_RVA_pstate ;
bool LVA_RVA_force_init_update ;

mtype RBB1_RVA_cstate , RBB1_RVA_pstate ;
bool RBB1_RVA_force_init_update ;

mtype RBB_RBB1_cstate , RBB_RBB1_pstate ;
bool RBB_RBB1_force_init_update ;

mtype His2_RBB_cstate , His2_RBB_pstate ;
bool His2_RBB_force_init_update ;

mtype LBB1_LVA_cstate , LBB1_LVA_pstate ;
bool LBB1_LVA_force_init_update ;

mtype LBB_LBB1_cstate , LBB_LBB1_pstate ;
bool LBB_LBB1_force_init_update ;

mtype His2_LBB_cstate , His2_LBB_pstate ;
bool His2_LBB_force_init_update ;

mtype His1_His2_cstate , His1_His2_pstate ;
bool His1_His2_force_init_update ;

mtype His_His1_cstate , His_His1_pstate ;
bool His_His1_force_init_update ;

mtype AV_His_cstate , AV_His_pstate ;
bool AV_His_force_init_update ;

mtype Slow1_AV_cstate , Slow1_AV_pstate ;
bool Slow1_AV_force_init_update ;

mtype Fast1_AV_cstate , Fast1_AV_pstate ;
bool Fast1_AV_force_init_update ;

mtype Slow_Slow1_cstate , Slow_Slow1_pstate ;
bool Slow_Slow1_force_init_update ;

mtype OS_Slow_cstate , OS_Slow_pstate ;
bool OS_Slow_force_init_update ;

mtype Fast_Fast1_cstate , Fast_Fast1_pstate ;
bool Fast_Fast1_force_init_update ;

mtype OS_Fast_cstate , OS_Fast_pstate ;
bool OS_Fast_force_init_update ;

mtype CT_CT1_cstate , CT_CT1_pstate ;
bool CT_CT1_force_init_update ;

mtype RA1_CS_cstate , RA1_CS_pstate ;
bool RA1_CS_force_init_update ;

mtype RA_RA1_cstate , RA_RA1_pstate ;
bool RA_RA1_force_init_update ;

mtype LA_LA1_cstate , LA_LA1_pstate ;
bool LA_LA1_force_init_update ;

mtype BB_LA_cstate , BB_LA_pstate ;
bool BB_LA_force_init_update ;

mtype SA_CT_cstate , SA_CT_pstate ;
bool SA_CT_force_init_update ;

mtype SA_RA_cstate , SA_RA_pstate ;
bool SA_RA_force_init_update ;

mtype SA_OS_cstate , SA_OS_pstate ;
bool SA_OS_force_init_update ;

mtype SA_BB_cstate , SA_BB_pstate ;
bool SA_BB_force_init_update ;

mtype RightVentricularSeptum1_cstate , RightVentricularSeptum1_pstate ;
bool RightVentricularSeptum1_force_init_update ;

mtype RightVentricularSeptum_cstate , RightVentricularSeptum_pstate ;
bool RightVentricularSeptum_force_init_update ;

mtype RightVentricle1_cstate , RightVentricle1_pstate ;
bool RightVentricle1_force_init_update ;

mtype RightVentricle_cstate , RightVentricle_pstate ;
bool RightVentricle_force_init_update ;

mtype RightVentricularApex_cstate , RightVentricularApex_pstate ;
bool RightVentricularApex_force_init_update ;

mtype RightBundleBranch1_cstate , RightBundleBranch1_pstate ;
bool RightBundleBranch1_force_init_update ;

mtype RightBundleBranch_cstate , RightBundleBranch_pstate ;
bool RightBundleBranch_force_init_update ;

mtype CSLeftVentricular_cstate , CSLeftVentricular_pstate ;
bool CSLeftVentricular_force_init_update ;

mtype LeftVentricularSeptum1_cstate , LeftVentricularSeptum1_pstate ;
bool LeftVentricularSeptum1_force_init_update ;

mtype LeftVentricularSeptum_cstate , LeftVentricularSeptum_pstate ;
bool LeftVentricularSeptum_force_init_update ;

mtype LeftVentricle1_cstate , LeftVentricle1_pstate ;
bool LeftVentricle1_force_init_update ;

mtype LeftVentricle_cstate , LeftVentricle_pstate ;
bool LeftVentricle_force_init_update ;

mtype LeftVentricularApex_cstate , LeftVentricularApex_pstate ;
bool LeftVentricularApex_force_init_update ;

mtype LeftBundleBranch1_cstate , LeftBundleBranch1_pstate ;
bool LeftBundleBranch1_force_init_update ;

mtype LeftBundleBranch_cstate , LeftBundleBranch_pstate ;
bool LeftBundleBranch_force_init_update ;

mtype BundleOfHis2_cstate , BundleOfHis2_pstate ;
bool BundleOfHis2_force_init_update ;

mtype BundleOfHis1_cstate , BundleOfHis1_pstate ;
bool BundleOfHis1_force_init_update ;

mtype BundleOfHis_cstate , BundleOfHis_pstate ;
bool BundleOfHis_force_init_update ;

mtype AtrioventricularNode_cstate , AtrioventricularNode_pstate ;
bool AtrioventricularNode_force_init_update ;

mtype Slow1_cstate , Slow1_pstate ;
bool Slow1_force_init_update ;

mtype Slow_cstate , Slow_pstate ;
bool Slow_force_init_update ;

mtype Fast1_cstate , Fast1_pstate ;
bool Fast1_force_init_update ;

mtype Fast_cstate , Fast_pstate ;
bool Fast_force_init_update ;

mtype Ostium_cstate , Ostium_pstate ;
bool Ostium_force_init_update ;

mtype CristaTerminalis1_cstate , CristaTerminalis1_pstate ;
bool CristaTerminalis1_force_init_update ;

mtype CristaTerminalis_cstate , CristaTerminalis_pstate ;
bool CristaTerminalis_force_init_update ;

mtype CoronarySinus_cstate , CoronarySinus_pstate ;
bool CoronarySinus_force_init_update ;

mtype RightAtrium1_cstate , RightAtrium1_pstate ;
bool RightAtrium1_force_init_update ;

mtype RightAtrium_cstate , RightAtrium_pstate ;
bool RightAtrium_force_init_update ;

mtype LeftAtrium1_cstate , LeftAtrium1_pstate ;
bool LeftAtrium1_force_init_update ;

mtype LeftAtrium_cstate , LeftAtrium_pstate ;
bool LeftAtrium_force_init_update ;

mtype BachmannBundle_cstate , BachmannBundle_pstate ;
bool BachmannBundle_force_init_update ;

mtype SinoatrialNode_cstate , SinoatrialNode_pstate ;
bool SinoatrialNode_force_init_update ;


c_decl {


};
c_decl { double SinoatrialNode_t_u, SinoatrialNode_t_slope, SinoatrialNode_t_init, SinoatrialNode_t =  0 ;};
c_track "&SinoatrialNode_t" "sizeof(double)";
c_track "&SinoatrialNode_t_slope" "sizeof(double)";
c_track "&SinoatrialNode_t_u" "sizeof(double)";
c_track "&SinoatrialNode_t_init" "sizeof(double)";


bool SinoatrialNode_ACTpath = 0 ;
bool SinoatrialNode_ACTcell1 ;
mtype { SinoatrialNode_REST, SinoatrialNode_ST, SinoatrialNode_ERP, SinoatrialNode_RRP };
proctype SinoatrialNode () {
REST:
   if
    :: (c_expr { (SinoatrialNode_t <= 400.0) }  &&  (! SinoatrialNode_ACTcell1)  &&  SinoatrialNode_cstate == SinoatrialNode_REST) ->
     if
      :: SinoatrialNode_pstate != SinoatrialNode_cstate || SinoatrialNode_force_init_update ->
       c_code { SinoatrialNode_t_init = SinoatrialNode_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       SinoatrialNode_t_slope =  1 ;
       SinoatrialNode_t_u = (SinoatrialNode_t_slope * d) + SinoatrialNode_t ;
       
       
       
      };
      SinoatrialNode_pstate = SinoatrialNode_cstate;
      SinoatrialNode_cstate = SinoatrialNode_REST;
      SinoatrialNode_force_init_update = false;
      SinoatrialNode_ACTpath  = false;
      SinoatrialNode_ACTcell1  = false;
      SinoatrialNode_tick = true;
     };
     do
      :: SinoatrialNode_tick -> true
      :: else -> goto  REST
     od;
     ::  (SinoatrialNode_ACTcell1
           && 
          SinoatrialNode_cstate == SinoatrialNode_REST)  -> 
      d_step {
            c_code {  SinoatrialNode_t_u = 0 ; };
            SinoatrialNode_pstate =  SinoatrialNode_cstate ;
            SinoatrialNode_cstate =  SinoatrialNode_ST ;
            SinoatrialNode_force_init_update = false;
            SinoatrialNode_tick = true;
      };
      do
       :: SinoatrialNode_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (SinoatrialNode_t > 400.0) }
           && 
          SinoatrialNode_cstate == SinoatrialNode_REST)  -> 
      d_step {
            c_code {  SinoatrialNode_t_u = 0 ; };
            SinoatrialNode_pstate =  SinoatrialNode_cstate ;
            SinoatrialNode_cstate =  SinoatrialNode_ST ;
            SinoatrialNode_force_init_update = false;
            SinoatrialNode_tick = true;
      };
      do
       :: SinoatrialNode_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (SinoatrialNode_t <= 10.0) }  &&  (! SinoatrialNode_ACTcell1)  &&  SinoatrialNode_cstate == SinoatrialNode_ST) ->
     if
      :: SinoatrialNode_pstate != SinoatrialNode_cstate || SinoatrialNode_force_init_update ->
       c_code { SinoatrialNode_t_init = SinoatrialNode_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       SinoatrialNode_t_slope =  1 ;
       SinoatrialNode_t_u = (SinoatrialNode_t_slope * d) + SinoatrialNode_t ;
       
       
       
      };
      SinoatrialNode_pstate = SinoatrialNode_cstate;
      SinoatrialNode_cstate = SinoatrialNode_ST;
      SinoatrialNode_force_init_update = false;
      SinoatrialNode_ACTpath  = false;
      SinoatrialNode_ACTcell1  = false;
      SinoatrialNode_tick = true;
     };
     do
      :: SinoatrialNode_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (SinoatrialNode_t > 10.0) }
           && 
          SinoatrialNode_cstate == SinoatrialNode_ST)  -> 
      d_step {
            c_code {  SinoatrialNode_t_u = 0 ; };
            SinoatrialNode_ACTpath = 1;
            SinoatrialNode_pstate =  SinoatrialNode_cstate ;
            SinoatrialNode_cstate =  SinoatrialNode_ERP ;
            SinoatrialNode_force_init_update = false;
            SinoatrialNode_tick = true;
      };
      do
       :: SinoatrialNode_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (SinoatrialNode_t <= 200.0) }  &&  (! SinoatrialNode_ACTcell1)  &&  SinoatrialNode_cstate == SinoatrialNode_ERP) ->
     if
      :: SinoatrialNode_pstate != SinoatrialNode_cstate || SinoatrialNode_force_init_update ->
       c_code { SinoatrialNode_t_init = SinoatrialNode_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       SinoatrialNode_t_slope =  1 ;
       SinoatrialNode_t_u = (SinoatrialNode_t_slope * d) + SinoatrialNode_t ;
       
       
       
      };
      SinoatrialNode_pstate = SinoatrialNode_cstate;
      SinoatrialNode_cstate = SinoatrialNode_ERP;
      SinoatrialNode_force_init_update = false;
      SinoatrialNode_ACTpath  = false;
      SinoatrialNode_ACTcell1  = false;
      SinoatrialNode_tick = true;
     };
     do
      :: SinoatrialNode_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (SinoatrialNode_t > 200.0) }
           && 
          SinoatrialNode_cstate == SinoatrialNode_ERP)  -> 
      d_step {
            c_code {  SinoatrialNode_t_u = 0 ; };
            SinoatrialNode_pstate =  SinoatrialNode_cstate ;
            SinoatrialNode_cstate =  SinoatrialNode_RRP ;
            SinoatrialNode_force_init_update = false;
            SinoatrialNode_tick = true;
      };
      do
       :: SinoatrialNode_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (SinoatrialNode_t <= 100.0) }  &&  (! SinoatrialNode_ACTcell1)  &&  SinoatrialNode_cstate == SinoatrialNode_RRP) ->
     if
      :: SinoatrialNode_pstate != SinoatrialNode_cstate || SinoatrialNode_force_init_update ->
       c_code { SinoatrialNode_t_init = SinoatrialNode_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       SinoatrialNode_t_slope =  1 ;
       SinoatrialNode_t_u = (SinoatrialNode_t_slope * d) + SinoatrialNode_t ;
       
       
       
      };
      SinoatrialNode_pstate = SinoatrialNode_cstate;
      SinoatrialNode_cstate = SinoatrialNode_RRP;
      SinoatrialNode_force_init_update = false;
      SinoatrialNode_ACTpath  = false;
      SinoatrialNode_ACTcell1  = false;
      SinoatrialNode_tick = true;
     };
     do
      :: SinoatrialNode_tick -> true
      :: else -> goto  RRP
     od;
     ::  (SinoatrialNode_ACTcell1
           && 
          SinoatrialNode_cstate == SinoatrialNode_RRP)  -> 
      d_step {
            c_code {  SinoatrialNode_t_u = 0 ; };
            SinoatrialNode_pstate =  SinoatrialNode_cstate ;
            SinoatrialNode_cstate =  SinoatrialNode_ST ;
            SinoatrialNode_force_init_update = false;
            SinoatrialNode_tick = true;
      };
      do
       :: SinoatrialNode_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (SinoatrialNode_t > 100.0) }
           && 
          SinoatrialNode_cstate == SinoatrialNode_RRP)  -> 
      d_step {
            c_code {  SinoatrialNode_t_u = 0 ; };
            SinoatrialNode_pstate =  SinoatrialNode_cstate ;
            SinoatrialNode_cstate =  SinoatrialNode_REST ;
            SinoatrialNode_force_init_update = false;
            SinoatrialNode_tick = true;
      };
      do
       :: SinoatrialNode_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double BachmannBundle_t_u, BachmannBundle_t_slope, BachmannBundle_t_init, BachmannBundle_t =  0 ;};
c_track "&BachmannBundle_t" "sizeof(double)";
c_track "&BachmannBundle_t_slope" "sizeof(double)";
c_track "&BachmannBundle_t_u" "sizeof(double)";
c_track "&BachmannBundle_t_init" "sizeof(double)";


bool BachmannBundle_ACTpath = 0 ;
bool BachmannBundle_ACTcell1 ;
mtype { BachmannBundle_REST, BachmannBundle_ST, BachmannBundle_ERP, BachmannBundle_RRP };
proctype BachmannBundle () {
REST:
   if
    :: (c_expr { (BachmannBundle_t <= 400.0) }  &&  (! BachmannBundle_ACTcell1)  &&  BachmannBundle_cstate == BachmannBundle_REST) ->
     if
      :: BachmannBundle_pstate != BachmannBundle_cstate || BachmannBundle_force_init_update ->
       c_code { BachmannBundle_t_init = BachmannBundle_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       BachmannBundle_t_slope =  1 ;
       BachmannBundle_t_u = (BachmannBundle_t_slope * d) + BachmannBundle_t ;
       
       
       
      };
      BachmannBundle_pstate = BachmannBundle_cstate;
      BachmannBundle_cstate = BachmannBundle_REST;
      BachmannBundle_force_init_update = false;
      BachmannBundle_ACTpath  = false;
      BachmannBundle_ACTcell1  = false;
      BachmannBundle_tick = true;
     };
     do
      :: BachmannBundle_tick -> true
      :: else -> goto  REST
     od;
     ::  (BachmannBundle_ACTcell1
           && 
          BachmannBundle_cstate == BachmannBundle_REST)  -> 
      d_step {
            c_code {  BachmannBundle_t_u = 0 ; };
            BachmannBundle_pstate =  BachmannBundle_cstate ;
            BachmannBundle_cstate =  BachmannBundle_ST ;
            BachmannBundle_force_init_update = false;
            BachmannBundle_tick = true;
      };
      do
       :: BachmannBundle_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (BachmannBundle_t > 400.0) }
           && 
          BachmannBundle_cstate == BachmannBundle_REST)  -> 
      d_step {
            c_code {  BachmannBundle_t_u = 0 ; };
            BachmannBundle_pstate =  BachmannBundle_cstate ;
            BachmannBundle_cstate =  BachmannBundle_ST ;
            BachmannBundle_force_init_update = false;
            BachmannBundle_tick = true;
      };
      do
       :: BachmannBundle_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (BachmannBundle_t <= 10.0) }  &&  (! BachmannBundle_ACTcell1)  &&  BachmannBundle_cstate == BachmannBundle_ST) ->
     if
      :: BachmannBundle_pstate != BachmannBundle_cstate || BachmannBundle_force_init_update ->
       c_code { BachmannBundle_t_init = BachmannBundle_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       BachmannBundle_t_slope =  1 ;
       BachmannBundle_t_u = (BachmannBundle_t_slope * d) + BachmannBundle_t ;
       
       
       
      };
      BachmannBundle_pstate = BachmannBundle_cstate;
      BachmannBundle_cstate = BachmannBundle_ST;
      BachmannBundle_force_init_update = false;
      BachmannBundle_ACTpath  = false;
      BachmannBundle_ACTcell1  = false;
      BachmannBundle_tick = true;
     };
     do
      :: BachmannBundle_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (BachmannBundle_t > 10.0) }
           && 
          BachmannBundle_cstate == BachmannBundle_ST)  -> 
      d_step {
            c_code {  BachmannBundle_t_u = 0 ; };
            BachmannBundle_ACTpath = 1;
            BachmannBundle_pstate =  BachmannBundle_cstate ;
            BachmannBundle_cstate =  BachmannBundle_ERP ;
            BachmannBundle_force_init_update = false;
            BachmannBundle_tick = true;
      };
      do
       :: BachmannBundle_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (BachmannBundle_t <= 200.0) }  &&  (! BachmannBundle_ACTcell1)  &&  BachmannBundle_cstate == BachmannBundle_ERP) ->
     if
      :: BachmannBundle_pstate != BachmannBundle_cstate || BachmannBundle_force_init_update ->
       c_code { BachmannBundle_t_init = BachmannBundle_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       BachmannBundle_t_slope =  1 ;
       BachmannBundle_t_u = (BachmannBundle_t_slope * d) + BachmannBundle_t ;
       
       
       
      };
      BachmannBundle_pstate = BachmannBundle_cstate;
      BachmannBundle_cstate = BachmannBundle_ERP;
      BachmannBundle_force_init_update = false;
      BachmannBundle_ACTpath  = false;
      BachmannBundle_ACTcell1  = false;
      BachmannBundle_tick = true;
     };
     do
      :: BachmannBundle_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (BachmannBundle_t > 200.0) }
           && 
          BachmannBundle_cstate == BachmannBundle_ERP)  -> 
      d_step {
            c_code {  BachmannBundle_t_u = 0 ; };
            BachmannBundle_pstate =  BachmannBundle_cstate ;
            BachmannBundle_cstate =  BachmannBundle_RRP ;
            BachmannBundle_force_init_update = false;
            BachmannBundle_tick = true;
      };
      do
       :: BachmannBundle_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (BachmannBundle_t <= 100.0) }  &&  (! BachmannBundle_ACTcell1)  &&  BachmannBundle_cstate == BachmannBundle_RRP) ->
     if
      :: BachmannBundle_pstate != BachmannBundle_cstate || BachmannBundle_force_init_update ->
       c_code { BachmannBundle_t_init = BachmannBundle_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       BachmannBundle_t_slope =  1 ;
       BachmannBundle_t_u = (BachmannBundle_t_slope * d) + BachmannBundle_t ;
       
       
       
      };
      BachmannBundle_pstate = BachmannBundle_cstate;
      BachmannBundle_cstate = BachmannBundle_RRP;
      BachmannBundle_force_init_update = false;
      BachmannBundle_ACTpath  = false;
      BachmannBundle_ACTcell1  = false;
      BachmannBundle_tick = true;
     };
     do
      :: BachmannBundle_tick -> true
      :: else -> goto  RRP
     od;
     ::  (BachmannBundle_ACTcell1
           && 
          BachmannBundle_cstate == BachmannBundle_RRP)  -> 
      d_step {
            c_code {  BachmannBundle_t_u = 0 ; };
            BachmannBundle_pstate =  BachmannBundle_cstate ;
            BachmannBundle_cstate =  BachmannBundle_ST ;
            BachmannBundle_force_init_update = false;
            BachmannBundle_tick = true;
      };
      do
       :: BachmannBundle_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (BachmannBundle_t > 100.0) }
           && 
          BachmannBundle_cstate == BachmannBundle_RRP)  -> 
      d_step {
            c_code {  BachmannBundle_t_u = 0 ; };
            BachmannBundle_pstate =  BachmannBundle_cstate ;
            BachmannBundle_cstate =  BachmannBundle_REST ;
            BachmannBundle_force_init_update = false;
            BachmannBundle_tick = true;
      };
      do
       :: BachmannBundle_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double LeftAtrium_t_u, LeftAtrium_t_slope, LeftAtrium_t_init, LeftAtrium_t =  0 ;};
c_track "&LeftAtrium_t" "sizeof(double)";
c_track "&LeftAtrium_t_slope" "sizeof(double)";
c_track "&LeftAtrium_t_u" "sizeof(double)";
c_track "&LeftAtrium_t_init" "sizeof(double)";


bool LeftAtrium_ACTpath = 0 ;
bool LeftAtrium_ACTcell1 ;
mtype { LeftAtrium_REST, LeftAtrium_ST, LeftAtrium_ERP, LeftAtrium_RRP };
proctype LeftAtrium () {
REST:
   if
    :: (c_expr { (LeftAtrium_t <= 400.0) }  &&  (! LeftAtrium_ACTcell1)  &&  LeftAtrium_cstate == LeftAtrium_REST) ->
     if
      :: LeftAtrium_pstate != LeftAtrium_cstate || LeftAtrium_force_init_update ->
       c_code { LeftAtrium_t_init = LeftAtrium_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftAtrium_t_slope =  1 ;
       LeftAtrium_t_u = (LeftAtrium_t_slope * d) + LeftAtrium_t ;
       
       
       
      };
      LeftAtrium_pstate = LeftAtrium_cstate;
      LeftAtrium_cstate = LeftAtrium_REST;
      LeftAtrium_force_init_update = false;
      LeftAtrium_ACTpath  = false;
      LeftAtrium_ACTcell1  = false;
      LeftAtrium_tick = true;
     };
     do
      :: LeftAtrium_tick -> true
      :: else -> goto  REST
     od;
     ::  (LeftAtrium_ACTcell1
           && 
          LeftAtrium_cstate == LeftAtrium_REST)  -> 
      d_step {
            c_code {  LeftAtrium_t_u = 0 ; };
            LeftAtrium_pstate =  LeftAtrium_cstate ;
            LeftAtrium_cstate =  LeftAtrium_ST ;
            LeftAtrium_force_init_update = false;
            LeftAtrium_tick = true;
      };
      do
       :: LeftAtrium_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (LeftAtrium_t > 400.0) }
           && 
          LeftAtrium_cstate == LeftAtrium_REST)  -> 
      d_step {
            c_code {  LeftAtrium_t_u = 0 ; };
            LeftAtrium_pstate =  LeftAtrium_cstate ;
            LeftAtrium_cstate =  LeftAtrium_ST ;
            LeftAtrium_force_init_update = false;
            LeftAtrium_tick = true;
      };
      do
       :: LeftAtrium_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (LeftAtrium_t <= 10.0) }  &&  (! LeftAtrium_ACTcell1)  &&  LeftAtrium_cstate == LeftAtrium_ST) ->
     if
      :: LeftAtrium_pstate != LeftAtrium_cstate || LeftAtrium_force_init_update ->
       c_code { LeftAtrium_t_init = LeftAtrium_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftAtrium_t_slope =  1 ;
       LeftAtrium_t_u = (LeftAtrium_t_slope * d) + LeftAtrium_t ;
       
       
       
      };
      LeftAtrium_pstate = LeftAtrium_cstate;
      LeftAtrium_cstate = LeftAtrium_ST;
      LeftAtrium_force_init_update = false;
      LeftAtrium_ACTpath  = false;
      LeftAtrium_ACTcell1  = false;
      LeftAtrium_tick = true;
     };
     do
      :: LeftAtrium_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (LeftAtrium_t > 10.0) }
           && 
          LeftAtrium_cstate == LeftAtrium_ST)  -> 
      d_step {
            c_code {  LeftAtrium_t_u = 0 ; };
            LeftAtrium_ACTpath = 1;
            LeftAtrium_pstate =  LeftAtrium_cstate ;
            LeftAtrium_cstate =  LeftAtrium_ERP ;
            LeftAtrium_force_init_update = false;
            LeftAtrium_tick = true;
      };
      do
       :: LeftAtrium_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (LeftAtrium_t <= 200.0) }  &&  (! LeftAtrium_ACTcell1)  &&  LeftAtrium_cstate == LeftAtrium_ERP) ->
     if
      :: LeftAtrium_pstate != LeftAtrium_cstate || LeftAtrium_force_init_update ->
       c_code { LeftAtrium_t_init = LeftAtrium_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftAtrium_t_slope =  1 ;
       LeftAtrium_t_u = (LeftAtrium_t_slope * d) + LeftAtrium_t ;
       
       
       
      };
      LeftAtrium_pstate = LeftAtrium_cstate;
      LeftAtrium_cstate = LeftAtrium_ERP;
      LeftAtrium_force_init_update = false;
      LeftAtrium_ACTpath  = false;
      LeftAtrium_ACTcell1  = false;
      LeftAtrium_tick = true;
     };
     do
      :: LeftAtrium_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (LeftAtrium_t > 200.0) }
           && 
          LeftAtrium_cstate == LeftAtrium_ERP)  -> 
      d_step {
            c_code {  LeftAtrium_t_u = 0 ; };
            LeftAtrium_pstate =  LeftAtrium_cstate ;
            LeftAtrium_cstate =  LeftAtrium_RRP ;
            LeftAtrium_force_init_update = false;
            LeftAtrium_tick = true;
      };
      do
       :: LeftAtrium_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (LeftAtrium_t <= 100.0) }  &&  (! LeftAtrium_ACTcell1)  &&  LeftAtrium_cstate == LeftAtrium_RRP) ->
     if
      :: LeftAtrium_pstate != LeftAtrium_cstate || LeftAtrium_force_init_update ->
       c_code { LeftAtrium_t_init = LeftAtrium_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftAtrium_t_slope =  1 ;
       LeftAtrium_t_u = (LeftAtrium_t_slope * d) + LeftAtrium_t ;
       
       
       
      };
      LeftAtrium_pstate = LeftAtrium_cstate;
      LeftAtrium_cstate = LeftAtrium_RRP;
      LeftAtrium_force_init_update = false;
      LeftAtrium_ACTpath  = false;
      LeftAtrium_ACTcell1  = false;
      LeftAtrium_tick = true;
     };
     do
      :: LeftAtrium_tick -> true
      :: else -> goto  RRP
     od;
     ::  (LeftAtrium_ACTcell1
           && 
          LeftAtrium_cstate == LeftAtrium_RRP)  -> 
      d_step {
            c_code {  LeftAtrium_t_u = 0 ; };
            LeftAtrium_pstate =  LeftAtrium_cstate ;
            LeftAtrium_cstate =  LeftAtrium_ST ;
            LeftAtrium_force_init_update = false;
            LeftAtrium_tick = true;
      };
      do
       :: LeftAtrium_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (LeftAtrium_t > 100.0) }
           && 
          LeftAtrium_cstate == LeftAtrium_RRP)  -> 
      d_step {
            c_code {  LeftAtrium_t_u = 0 ; };
            LeftAtrium_pstate =  LeftAtrium_cstate ;
            LeftAtrium_cstate =  LeftAtrium_REST ;
            LeftAtrium_force_init_update = false;
            LeftAtrium_tick = true;
      };
      do
       :: LeftAtrium_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double LeftAtrium1_t_u, LeftAtrium1_t_slope, LeftAtrium1_t_init, LeftAtrium1_t =  0 ;};
c_track "&LeftAtrium1_t" "sizeof(double)";
c_track "&LeftAtrium1_t_slope" "sizeof(double)";
c_track "&LeftAtrium1_t_u" "sizeof(double)";
c_track "&LeftAtrium1_t_init" "sizeof(double)";


bool LeftAtrium1_ACTpath = 0 ;
bool LeftAtrium1_ACTcell1 ;
mtype { LeftAtrium1_REST, LeftAtrium1_ST, LeftAtrium1_ERP, LeftAtrium1_RRP };
proctype LeftAtrium1 () {
REST:
   if
    :: (c_expr { (LeftAtrium1_t <= 400.0) }  &&  (! LeftAtrium1_ACTcell1)  &&  LeftAtrium1_cstate == LeftAtrium1_REST) ->
     if
      :: LeftAtrium1_pstate != LeftAtrium1_cstate || LeftAtrium1_force_init_update ->
       c_code { LeftAtrium1_t_init = LeftAtrium1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftAtrium1_t_slope =  1 ;
       LeftAtrium1_t_u = (LeftAtrium1_t_slope * d) + LeftAtrium1_t ;
       
       
       
      };
      LeftAtrium1_pstate = LeftAtrium1_cstate;
      LeftAtrium1_cstate = LeftAtrium1_REST;
      LeftAtrium1_force_init_update = false;
      LeftAtrium1_ACTpath  = false;
      LeftAtrium1_ACTcell1  = false;
      LeftAtrium1_tick = true;
     };
     do
      :: LeftAtrium1_tick -> true
      :: else -> goto  REST
     od;
     ::  (LeftAtrium1_ACTcell1
           && 
          LeftAtrium1_cstate == LeftAtrium1_REST)  -> 
      d_step {
            c_code {  LeftAtrium1_t_u = 0 ; };
            LeftAtrium1_pstate =  LeftAtrium1_cstate ;
            LeftAtrium1_cstate =  LeftAtrium1_ST ;
            LeftAtrium1_force_init_update = false;
            LeftAtrium1_tick = true;
      };
      do
       :: LeftAtrium1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (LeftAtrium1_t > 400.0) }
           && 
          LeftAtrium1_cstate == LeftAtrium1_REST)  -> 
      d_step {
            c_code {  LeftAtrium1_t_u = 0 ; };
            LeftAtrium1_pstate =  LeftAtrium1_cstate ;
            LeftAtrium1_cstate =  LeftAtrium1_ST ;
            LeftAtrium1_force_init_update = false;
            LeftAtrium1_tick = true;
      };
      do
       :: LeftAtrium1_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (LeftAtrium1_t <= 10.0) }  &&  (! LeftAtrium1_ACTcell1)  &&  LeftAtrium1_cstate == LeftAtrium1_ST) ->
     if
      :: LeftAtrium1_pstate != LeftAtrium1_cstate || LeftAtrium1_force_init_update ->
       c_code { LeftAtrium1_t_init = LeftAtrium1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftAtrium1_t_slope =  1 ;
       LeftAtrium1_t_u = (LeftAtrium1_t_slope * d) + LeftAtrium1_t ;
       
       
       
      };
      LeftAtrium1_pstate = LeftAtrium1_cstate;
      LeftAtrium1_cstate = LeftAtrium1_ST;
      LeftAtrium1_force_init_update = false;
      LeftAtrium1_ACTpath  = false;
      LeftAtrium1_ACTcell1  = false;
      LeftAtrium1_tick = true;
     };
     do
      :: LeftAtrium1_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (LeftAtrium1_t > 10.0) }
           && 
          LeftAtrium1_cstate == LeftAtrium1_ST)  -> 
      d_step {
            c_code {  LeftAtrium1_t_u = 0 ; };
            LeftAtrium1_ACTpath = 1;
            LeftAtrium1_pstate =  LeftAtrium1_cstate ;
            LeftAtrium1_cstate =  LeftAtrium1_ERP ;
            LeftAtrium1_force_init_update = false;
            LeftAtrium1_tick = true;
      };
      do
       :: LeftAtrium1_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (LeftAtrium1_t <= 200.0) }  &&  (! LeftAtrium1_ACTcell1)  &&  LeftAtrium1_cstate == LeftAtrium1_ERP) ->
     if
      :: LeftAtrium1_pstate != LeftAtrium1_cstate || LeftAtrium1_force_init_update ->
       c_code { LeftAtrium1_t_init = LeftAtrium1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftAtrium1_t_slope =  1 ;
       LeftAtrium1_t_u = (LeftAtrium1_t_slope * d) + LeftAtrium1_t ;
       
       
       
      };
      LeftAtrium1_pstate = LeftAtrium1_cstate;
      LeftAtrium1_cstate = LeftAtrium1_ERP;
      LeftAtrium1_force_init_update = false;
      LeftAtrium1_ACTpath  = false;
      LeftAtrium1_ACTcell1  = false;
      LeftAtrium1_tick = true;
     };
     do
      :: LeftAtrium1_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (LeftAtrium1_t > 200.0) }
           && 
          LeftAtrium1_cstate == LeftAtrium1_ERP)  -> 
      d_step {
            c_code {  LeftAtrium1_t_u = 0 ; };
            LeftAtrium1_pstate =  LeftAtrium1_cstate ;
            LeftAtrium1_cstate =  LeftAtrium1_RRP ;
            LeftAtrium1_force_init_update = false;
            LeftAtrium1_tick = true;
      };
      do
       :: LeftAtrium1_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (LeftAtrium1_t <= 100.0) }  &&  (! LeftAtrium1_ACTcell1)  &&  LeftAtrium1_cstate == LeftAtrium1_RRP) ->
     if
      :: LeftAtrium1_pstate != LeftAtrium1_cstate || LeftAtrium1_force_init_update ->
       c_code { LeftAtrium1_t_init = LeftAtrium1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftAtrium1_t_slope =  1 ;
       LeftAtrium1_t_u = (LeftAtrium1_t_slope * d) + LeftAtrium1_t ;
       
       
       
      };
      LeftAtrium1_pstate = LeftAtrium1_cstate;
      LeftAtrium1_cstate = LeftAtrium1_RRP;
      LeftAtrium1_force_init_update = false;
      LeftAtrium1_ACTpath  = false;
      LeftAtrium1_ACTcell1  = false;
      LeftAtrium1_tick = true;
     };
     do
      :: LeftAtrium1_tick -> true
      :: else -> goto  RRP
     od;
     ::  (LeftAtrium1_ACTcell1
           && 
          LeftAtrium1_cstate == LeftAtrium1_RRP)  -> 
      d_step {
            c_code {  LeftAtrium1_t_u = 0 ; };
            LeftAtrium1_pstate =  LeftAtrium1_cstate ;
            LeftAtrium1_cstate =  LeftAtrium1_ST ;
            LeftAtrium1_force_init_update = false;
            LeftAtrium1_tick = true;
      };
      do
       :: LeftAtrium1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (LeftAtrium1_t > 100.0) }
           && 
          LeftAtrium1_cstate == LeftAtrium1_RRP)  -> 
      d_step {
            c_code {  LeftAtrium1_t_u = 0 ; };
            LeftAtrium1_pstate =  LeftAtrium1_cstate ;
            LeftAtrium1_cstate =  LeftAtrium1_REST ;
            LeftAtrium1_force_init_update = false;
            LeftAtrium1_tick = true;
      };
      do
       :: LeftAtrium1_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double RightAtrium_t_u, RightAtrium_t_slope, RightAtrium_t_init, RightAtrium_t =  0 ;};
c_track "&RightAtrium_t" "sizeof(double)";
c_track "&RightAtrium_t_slope" "sizeof(double)";
c_track "&RightAtrium_t_u" "sizeof(double)";
c_track "&RightAtrium_t_init" "sizeof(double)";


bool RightAtrium_ACTpath = 0 ;
bool RightAtrium_ACTcell1 ;
mtype { RightAtrium_REST, RightAtrium_ST, RightAtrium_ERP, RightAtrium_RRP };
proctype RightAtrium () {
REST:
   if
    :: (c_expr { (RightAtrium_t <= 400.0) }  &&  (! RightAtrium_ACTcell1)  &&  RightAtrium_cstate == RightAtrium_REST) ->
     if
      :: RightAtrium_pstate != RightAtrium_cstate || RightAtrium_force_init_update ->
       c_code { RightAtrium_t_init = RightAtrium_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightAtrium_t_slope =  1 ;
       RightAtrium_t_u = (RightAtrium_t_slope * d) + RightAtrium_t ;
       
       
       
      };
      RightAtrium_pstate = RightAtrium_cstate;
      RightAtrium_cstate = RightAtrium_REST;
      RightAtrium_force_init_update = false;
      RightAtrium_ACTpath  = false;
      RightAtrium_ACTcell1  = false;
      RightAtrium_tick = true;
     };
     do
      :: RightAtrium_tick -> true
      :: else -> goto  REST
     od;
     ::  (RightAtrium_ACTcell1
           && 
          RightAtrium_cstate == RightAtrium_REST)  -> 
      d_step {
            c_code {  RightAtrium_t_u = 0 ; };
            RightAtrium_pstate =  RightAtrium_cstate ;
            RightAtrium_cstate =  RightAtrium_ST ;
            RightAtrium_force_init_update = false;
            RightAtrium_tick = true;
      };
      do
       :: RightAtrium_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (RightAtrium_t > 400.0) }
           && 
          RightAtrium_cstate == RightAtrium_REST)  -> 
      d_step {
            c_code {  RightAtrium_t_u = 0 ; };
            RightAtrium_pstate =  RightAtrium_cstate ;
            RightAtrium_cstate =  RightAtrium_ST ;
            RightAtrium_force_init_update = false;
            RightAtrium_tick = true;
      };
      do
       :: RightAtrium_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (RightAtrium_t <= 10.0) }  &&  (! RightAtrium_ACTcell1)  &&  RightAtrium_cstate == RightAtrium_ST) ->
     if
      :: RightAtrium_pstate != RightAtrium_cstate || RightAtrium_force_init_update ->
       c_code { RightAtrium_t_init = RightAtrium_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightAtrium_t_slope =  1 ;
       RightAtrium_t_u = (RightAtrium_t_slope * d) + RightAtrium_t ;
       
       
       
      };
      RightAtrium_pstate = RightAtrium_cstate;
      RightAtrium_cstate = RightAtrium_ST;
      RightAtrium_force_init_update = false;
      RightAtrium_ACTpath  = false;
      RightAtrium_ACTcell1  = false;
      RightAtrium_tick = true;
     };
     do
      :: RightAtrium_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (RightAtrium_t > 10.0) }
           && 
          RightAtrium_cstate == RightAtrium_ST)  -> 
      d_step {
            c_code {  RightAtrium_t_u = 0 ; };
            RightAtrium_ACTpath = 1;
            RightAtrium_pstate =  RightAtrium_cstate ;
            RightAtrium_cstate =  RightAtrium_ERP ;
            RightAtrium_force_init_update = false;
            RightAtrium_tick = true;
      };
      do
       :: RightAtrium_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (RightAtrium_t <= 200.0) }  &&  (! RightAtrium_ACTcell1)  &&  RightAtrium_cstate == RightAtrium_ERP) ->
     if
      :: RightAtrium_pstate != RightAtrium_cstate || RightAtrium_force_init_update ->
       c_code { RightAtrium_t_init = RightAtrium_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightAtrium_t_slope =  1 ;
       RightAtrium_t_u = (RightAtrium_t_slope * d) + RightAtrium_t ;
       
       
       
      };
      RightAtrium_pstate = RightAtrium_cstate;
      RightAtrium_cstate = RightAtrium_ERP;
      RightAtrium_force_init_update = false;
      RightAtrium_ACTpath  = false;
      RightAtrium_ACTcell1  = false;
      RightAtrium_tick = true;
     };
     do
      :: RightAtrium_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (RightAtrium_t > 200.0) }
           && 
          RightAtrium_cstate == RightAtrium_ERP)  -> 
      d_step {
            c_code {  RightAtrium_t_u = 0 ; };
            RightAtrium_pstate =  RightAtrium_cstate ;
            RightAtrium_cstate =  RightAtrium_RRP ;
            RightAtrium_force_init_update = false;
            RightAtrium_tick = true;
      };
      do
       :: RightAtrium_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (RightAtrium_t <= 100.0) }  &&  (! RightAtrium_ACTcell1)  &&  RightAtrium_cstate == RightAtrium_RRP) ->
     if
      :: RightAtrium_pstate != RightAtrium_cstate || RightAtrium_force_init_update ->
       c_code { RightAtrium_t_init = RightAtrium_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightAtrium_t_slope =  1 ;
       RightAtrium_t_u = (RightAtrium_t_slope * d) + RightAtrium_t ;
       
       
       
      };
      RightAtrium_pstate = RightAtrium_cstate;
      RightAtrium_cstate = RightAtrium_RRP;
      RightAtrium_force_init_update = false;
      RightAtrium_ACTpath  = false;
      RightAtrium_ACTcell1  = false;
      RightAtrium_tick = true;
     };
     do
      :: RightAtrium_tick -> true
      :: else -> goto  RRP
     od;
     ::  (RightAtrium_ACTcell1
           && 
          RightAtrium_cstate == RightAtrium_RRP)  -> 
      d_step {
            c_code {  RightAtrium_t_u = 0 ; };
            RightAtrium_pstate =  RightAtrium_cstate ;
            RightAtrium_cstate =  RightAtrium_ST ;
            RightAtrium_force_init_update = false;
            RightAtrium_tick = true;
      };
      do
       :: RightAtrium_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (RightAtrium_t > 100.0) }
           && 
          RightAtrium_cstate == RightAtrium_RRP)  -> 
      d_step {
            c_code {  RightAtrium_t_u = 0 ; };
            RightAtrium_pstate =  RightAtrium_cstate ;
            RightAtrium_cstate =  RightAtrium_REST ;
            RightAtrium_force_init_update = false;
            RightAtrium_tick = true;
      };
      do
       :: RightAtrium_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double RightAtrium1_t_u, RightAtrium1_t_slope, RightAtrium1_t_init, RightAtrium1_t =  0 ;};
c_track "&RightAtrium1_t" "sizeof(double)";
c_track "&RightAtrium1_t_slope" "sizeof(double)";
c_track "&RightAtrium1_t_u" "sizeof(double)";
c_track "&RightAtrium1_t_init" "sizeof(double)";


bool RightAtrium1_ACTpath = 0 ;
bool RightAtrium1_ACTcell1 ;
mtype { RightAtrium1_REST, RightAtrium1_ST, RightAtrium1_ERP, RightAtrium1_RRP };
proctype RightAtrium1 () {
REST:
   if
    :: (c_expr { (RightAtrium1_t <= 400.0) }  &&  (! RightAtrium1_ACTcell1)  &&  RightAtrium1_cstate == RightAtrium1_REST) ->
     if
      :: RightAtrium1_pstate != RightAtrium1_cstate || RightAtrium1_force_init_update ->
       c_code { RightAtrium1_t_init = RightAtrium1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightAtrium1_t_slope =  1 ;
       RightAtrium1_t_u = (RightAtrium1_t_slope * d) + RightAtrium1_t ;
       
       
       
      };
      RightAtrium1_pstate = RightAtrium1_cstate;
      RightAtrium1_cstate = RightAtrium1_REST;
      RightAtrium1_force_init_update = false;
      RightAtrium1_ACTpath  = false;
      RightAtrium1_ACTcell1  = false;
      RightAtrium1_tick = true;
     };
     do
      :: RightAtrium1_tick -> true
      :: else -> goto  REST
     od;
     ::  (RightAtrium1_ACTcell1
           && 
          RightAtrium1_cstate == RightAtrium1_REST)  -> 
      d_step {
            c_code {  RightAtrium1_t_u = 0 ; };
            RightAtrium1_pstate =  RightAtrium1_cstate ;
            RightAtrium1_cstate =  RightAtrium1_ST ;
            RightAtrium1_force_init_update = false;
            RightAtrium1_tick = true;
      };
      do
       :: RightAtrium1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (RightAtrium1_t > 400.0) }
           && 
          RightAtrium1_cstate == RightAtrium1_REST)  -> 
      d_step {
            c_code {  RightAtrium1_t_u = 0 ; };
            RightAtrium1_pstate =  RightAtrium1_cstate ;
            RightAtrium1_cstate =  RightAtrium1_ST ;
            RightAtrium1_force_init_update = false;
            RightAtrium1_tick = true;
      };
      do
       :: RightAtrium1_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (RightAtrium1_t <= 10.0) }  &&  (! RightAtrium1_ACTcell1)  &&  RightAtrium1_cstate == RightAtrium1_ST) ->
     if
      :: RightAtrium1_pstate != RightAtrium1_cstate || RightAtrium1_force_init_update ->
       c_code { RightAtrium1_t_init = RightAtrium1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightAtrium1_t_slope =  1 ;
       RightAtrium1_t_u = (RightAtrium1_t_slope * d) + RightAtrium1_t ;
       
       
       
      };
      RightAtrium1_pstate = RightAtrium1_cstate;
      RightAtrium1_cstate = RightAtrium1_ST;
      RightAtrium1_force_init_update = false;
      RightAtrium1_ACTpath  = false;
      RightAtrium1_ACTcell1  = false;
      RightAtrium1_tick = true;
     };
     do
      :: RightAtrium1_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (RightAtrium1_t > 10.0) }
           && 
          RightAtrium1_cstate == RightAtrium1_ST)  -> 
      d_step {
            c_code {  RightAtrium1_t_u = 0 ; };
            RightAtrium1_ACTpath = 1;
            RightAtrium1_pstate =  RightAtrium1_cstate ;
            RightAtrium1_cstate =  RightAtrium1_ERP ;
            RightAtrium1_force_init_update = false;
            RightAtrium1_tick = true;
      };
      do
       :: RightAtrium1_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (RightAtrium1_t <= 200.0) }  &&  (! RightAtrium1_ACTcell1)  &&  RightAtrium1_cstate == RightAtrium1_ERP) ->
     if
      :: RightAtrium1_pstate != RightAtrium1_cstate || RightAtrium1_force_init_update ->
       c_code { RightAtrium1_t_init = RightAtrium1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightAtrium1_t_slope =  1 ;
       RightAtrium1_t_u = (RightAtrium1_t_slope * d) + RightAtrium1_t ;
       
       
       
      };
      RightAtrium1_pstate = RightAtrium1_cstate;
      RightAtrium1_cstate = RightAtrium1_ERP;
      RightAtrium1_force_init_update = false;
      RightAtrium1_ACTpath  = false;
      RightAtrium1_ACTcell1  = false;
      RightAtrium1_tick = true;
     };
     do
      :: RightAtrium1_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (RightAtrium1_t > 200.0) }
           && 
          RightAtrium1_cstate == RightAtrium1_ERP)  -> 
      d_step {
            c_code {  RightAtrium1_t_u = 0 ; };
            RightAtrium1_pstate =  RightAtrium1_cstate ;
            RightAtrium1_cstate =  RightAtrium1_RRP ;
            RightAtrium1_force_init_update = false;
            RightAtrium1_tick = true;
      };
      do
       :: RightAtrium1_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (RightAtrium1_t <= 100.0) }  &&  (! RightAtrium1_ACTcell1)  &&  RightAtrium1_cstate == RightAtrium1_RRP) ->
     if
      :: RightAtrium1_pstate != RightAtrium1_cstate || RightAtrium1_force_init_update ->
       c_code { RightAtrium1_t_init = RightAtrium1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightAtrium1_t_slope =  1 ;
       RightAtrium1_t_u = (RightAtrium1_t_slope * d) + RightAtrium1_t ;
       
       
       
      };
      RightAtrium1_pstate = RightAtrium1_cstate;
      RightAtrium1_cstate = RightAtrium1_RRP;
      RightAtrium1_force_init_update = false;
      RightAtrium1_ACTpath  = false;
      RightAtrium1_ACTcell1  = false;
      RightAtrium1_tick = true;
     };
     do
      :: RightAtrium1_tick -> true
      :: else -> goto  RRP
     od;
     ::  (RightAtrium1_ACTcell1
           && 
          RightAtrium1_cstate == RightAtrium1_RRP)  -> 
      d_step {
            c_code {  RightAtrium1_t_u = 0 ; };
            RightAtrium1_pstate =  RightAtrium1_cstate ;
            RightAtrium1_cstate =  RightAtrium1_ST ;
            RightAtrium1_force_init_update = false;
            RightAtrium1_tick = true;
      };
      do
       :: RightAtrium1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (RightAtrium1_t > 100.0) }
           && 
          RightAtrium1_cstate == RightAtrium1_RRP)  -> 
      d_step {
            c_code {  RightAtrium1_t_u = 0 ; };
            RightAtrium1_pstate =  RightAtrium1_cstate ;
            RightAtrium1_cstate =  RightAtrium1_REST ;
            RightAtrium1_force_init_update = false;
            RightAtrium1_tick = true;
      };
      do
       :: RightAtrium1_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double CoronarySinus_t_u, CoronarySinus_t_slope, CoronarySinus_t_init, CoronarySinus_t =  0 ;};
c_track "&CoronarySinus_t" "sizeof(double)";
c_track "&CoronarySinus_t_slope" "sizeof(double)";
c_track "&CoronarySinus_t_u" "sizeof(double)";
c_track "&CoronarySinus_t_init" "sizeof(double)";


bool CoronarySinus_ACTpath = 0 ;
bool CoronarySinus_ACTcell1 ;
mtype { CoronarySinus_REST, CoronarySinus_ST, CoronarySinus_ERP, CoronarySinus_RRP };
proctype CoronarySinus () {
REST:
   if
    :: (c_expr { (CoronarySinus_t <= 400.0) }  &&  (! CoronarySinus_ACTcell1)  &&  CoronarySinus_cstate == CoronarySinus_REST) ->
     if
      :: CoronarySinus_pstate != CoronarySinus_cstate || CoronarySinus_force_init_update ->
       c_code { CoronarySinus_t_init = CoronarySinus_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       CoronarySinus_t_slope =  1 ;
       CoronarySinus_t_u = (CoronarySinus_t_slope * d) + CoronarySinus_t ;
       
       
       
      };
      CoronarySinus_pstate = CoronarySinus_cstate;
      CoronarySinus_cstate = CoronarySinus_REST;
      CoronarySinus_force_init_update = false;
      CoronarySinus_ACTpath  = false;
      CoronarySinus_ACTcell1  = false;
      CoronarySinus_tick = true;
     };
     do
      :: CoronarySinus_tick -> true
      :: else -> goto  REST
     od;
     ::  (CoronarySinus_ACTcell1
           && 
          CoronarySinus_cstate == CoronarySinus_REST)  -> 
      d_step {
            c_code {  CoronarySinus_t_u = 0 ; };
            CoronarySinus_pstate =  CoronarySinus_cstate ;
            CoronarySinus_cstate =  CoronarySinus_ST ;
            CoronarySinus_force_init_update = false;
            CoronarySinus_tick = true;
      };
      do
       :: CoronarySinus_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (CoronarySinus_t > 400.0) }
           && 
          CoronarySinus_cstate == CoronarySinus_REST)  -> 
      d_step {
            c_code {  CoronarySinus_t_u = 0 ; };
            CoronarySinus_pstate =  CoronarySinus_cstate ;
            CoronarySinus_cstate =  CoronarySinus_ST ;
            CoronarySinus_force_init_update = false;
            CoronarySinus_tick = true;
      };
      do
       :: CoronarySinus_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (CoronarySinus_t <= 10.0) }  &&  (! CoronarySinus_ACTcell1)  &&  CoronarySinus_cstate == CoronarySinus_ST) ->
     if
      :: CoronarySinus_pstate != CoronarySinus_cstate || CoronarySinus_force_init_update ->
       c_code { CoronarySinus_t_init = CoronarySinus_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       CoronarySinus_t_slope =  1 ;
       CoronarySinus_t_u = (CoronarySinus_t_slope * d) + CoronarySinus_t ;
       
       
       
      };
      CoronarySinus_pstate = CoronarySinus_cstate;
      CoronarySinus_cstate = CoronarySinus_ST;
      CoronarySinus_force_init_update = false;
      CoronarySinus_ACTpath  = false;
      CoronarySinus_ACTcell1  = false;
      CoronarySinus_tick = true;
     };
     do
      :: CoronarySinus_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (CoronarySinus_t > 10.0) }
           && 
          CoronarySinus_cstate == CoronarySinus_ST)  -> 
      d_step {
            c_code {  CoronarySinus_t_u = 0 ; };
            CoronarySinus_ACTpath = 1;
            CoronarySinus_pstate =  CoronarySinus_cstate ;
            CoronarySinus_cstate =  CoronarySinus_ERP ;
            CoronarySinus_force_init_update = false;
            CoronarySinus_tick = true;
      };
      do
       :: CoronarySinus_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (CoronarySinus_t <= 200.0) }  &&  (! CoronarySinus_ACTcell1)  &&  CoronarySinus_cstate == CoronarySinus_ERP) ->
     if
      :: CoronarySinus_pstate != CoronarySinus_cstate || CoronarySinus_force_init_update ->
       c_code { CoronarySinus_t_init = CoronarySinus_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       CoronarySinus_t_slope =  1 ;
       CoronarySinus_t_u = (CoronarySinus_t_slope * d) + CoronarySinus_t ;
       
       
       
      };
      CoronarySinus_pstate = CoronarySinus_cstate;
      CoronarySinus_cstate = CoronarySinus_ERP;
      CoronarySinus_force_init_update = false;
      CoronarySinus_ACTpath  = false;
      CoronarySinus_ACTcell1  = false;
      CoronarySinus_tick = true;
     };
     do
      :: CoronarySinus_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (CoronarySinus_t > 200.0) }
           && 
          CoronarySinus_cstate == CoronarySinus_ERP)  -> 
      d_step {
            c_code {  CoronarySinus_t_u = 0 ; };
            CoronarySinus_pstate =  CoronarySinus_cstate ;
            CoronarySinus_cstate =  CoronarySinus_RRP ;
            CoronarySinus_force_init_update = false;
            CoronarySinus_tick = true;
      };
      do
       :: CoronarySinus_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (CoronarySinus_t <= 100.0) }  &&  (! CoronarySinus_ACTcell1)  &&  CoronarySinus_cstate == CoronarySinus_RRP) ->
     if
      :: CoronarySinus_pstate != CoronarySinus_cstate || CoronarySinus_force_init_update ->
       c_code { CoronarySinus_t_init = CoronarySinus_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       CoronarySinus_t_slope =  1 ;
       CoronarySinus_t_u = (CoronarySinus_t_slope * d) + CoronarySinus_t ;
       
       
       
      };
      CoronarySinus_pstate = CoronarySinus_cstate;
      CoronarySinus_cstate = CoronarySinus_RRP;
      CoronarySinus_force_init_update = false;
      CoronarySinus_ACTpath  = false;
      CoronarySinus_ACTcell1  = false;
      CoronarySinus_tick = true;
     };
     do
      :: CoronarySinus_tick -> true
      :: else -> goto  RRP
     od;
     ::  (CoronarySinus_ACTcell1
           && 
          CoronarySinus_cstate == CoronarySinus_RRP)  -> 
      d_step {
            c_code {  CoronarySinus_t_u = 0 ; };
            CoronarySinus_pstate =  CoronarySinus_cstate ;
            CoronarySinus_cstate =  CoronarySinus_ST ;
            CoronarySinus_force_init_update = false;
            CoronarySinus_tick = true;
      };
      do
       :: CoronarySinus_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (CoronarySinus_t > 100.0) }
           && 
          CoronarySinus_cstate == CoronarySinus_RRP)  -> 
      d_step {
            c_code {  CoronarySinus_t_u = 0 ; };
            CoronarySinus_pstate =  CoronarySinus_cstate ;
            CoronarySinus_cstate =  CoronarySinus_REST ;
            CoronarySinus_force_init_update = false;
            CoronarySinus_tick = true;
      };
      do
       :: CoronarySinus_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double CristaTerminalis_t_u, CristaTerminalis_t_slope, CristaTerminalis_t_init, CristaTerminalis_t =  0 ;};
c_track "&CristaTerminalis_t" "sizeof(double)";
c_track "&CristaTerminalis_t_slope" "sizeof(double)";
c_track "&CristaTerminalis_t_u" "sizeof(double)";
c_track "&CristaTerminalis_t_init" "sizeof(double)";


bool CristaTerminalis_ACTpath = 0 ;
bool CristaTerminalis_ACTcell1 ;
mtype { CristaTerminalis_REST, CristaTerminalis_ST, CristaTerminalis_ERP, CristaTerminalis_RRP };
proctype CristaTerminalis () {
REST:
   if
    :: (c_expr { (CristaTerminalis_t <= 400.0) }  &&  (! CristaTerminalis_ACTcell1)  &&  CristaTerminalis_cstate == CristaTerminalis_REST) ->
     if
      :: CristaTerminalis_pstate != CristaTerminalis_cstate || CristaTerminalis_force_init_update ->
       c_code { CristaTerminalis_t_init = CristaTerminalis_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       CristaTerminalis_t_slope =  1 ;
       CristaTerminalis_t_u = (CristaTerminalis_t_slope * d) + CristaTerminalis_t ;
       
       
       
      };
      CristaTerminalis_pstate = CristaTerminalis_cstate;
      CristaTerminalis_cstate = CristaTerminalis_REST;
      CristaTerminalis_force_init_update = false;
      CristaTerminalis_ACTpath  = false;
      CristaTerminalis_ACTcell1  = false;
      CristaTerminalis_tick = true;
     };
     do
      :: CristaTerminalis_tick -> true
      :: else -> goto  REST
     od;
     ::  (CristaTerminalis_ACTcell1
           && 
          CristaTerminalis_cstate == CristaTerminalis_REST)  -> 
      d_step {
            c_code {  CristaTerminalis_t_u = 0 ; };
            CristaTerminalis_pstate =  CristaTerminalis_cstate ;
            CristaTerminalis_cstate =  CristaTerminalis_ST ;
            CristaTerminalis_force_init_update = false;
            CristaTerminalis_tick = true;
      };
      do
       :: CristaTerminalis_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (CristaTerminalis_t > 400.0) }
           && 
          CristaTerminalis_cstate == CristaTerminalis_REST)  -> 
      d_step {
            c_code {  CristaTerminalis_t_u = 0 ; };
            CristaTerminalis_pstate =  CristaTerminalis_cstate ;
            CristaTerminalis_cstate =  CristaTerminalis_ST ;
            CristaTerminalis_force_init_update = false;
            CristaTerminalis_tick = true;
      };
      do
       :: CristaTerminalis_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (CristaTerminalis_t <= 10.0) }  &&  (! CristaTerminalis_ACTcell1)  &&  CristaTerminalis_cstate == CristaTerminalis_ST) ->
     if
      :: CristaTerminalis_pstate != CristaTerminalis_cstate || CristaTerminalis_force_init_update ->
       c_code { CristaTerminalis_t_init = CristaTerminalis_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       CristaTerminalis_t_slope =  1 ;
       CristaTerminalis_t_u = (CristaTerminalis_t_slope * d) + CristaTerminalis_t ;
       
       
       
      };
      CristaTerminalis_pstate = CristaTerminalis_cstate;
      CristaTerminalis_cstate = CristaTerminalis_ST;
      CristaTerminalis_force_init_update = false;
      CristaTerminalis_ACTpath  = false;
      CristaTerminalis_ACTcell1  = false;
      CristaTerminalis_tick = true;
     };
     do
      :: CristaTerminalis_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (CristaTerminalis_t > 10.0) }
           && 
          CristaTerminalis_cstate == CristaTerminalis_ST)  -> 
      d_step {
            c_code {  CristaTerminalis_t_u = 0 ; };
            CristaTerminalis_ACTpath = 1;
            CristaTerminalis_pstate =  CristaTerminalis_cstate ;
            CristaTerminalis_cstate =  CristaTerminalis_ERP ;
            CristaTerminalis_force_init_update = false;
            CristaTerminalis_tick = true;
      };
      do
       :: CristaTerminalis_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (CristaTerminalis_t <= 200.0) }  &&  (! CristaTerminalis_ACTcell1)  &&  CristaTerminalis_cstate == CristaTerminalis_ERP) ->
     if
      :: CristaTerminalis_pstate != CristaTerminalis_cstate || CristaTerminalis_force_init_update ->
       c_code { CristaTerminalis_t_init = CristaTerminalis_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       CristaTerminalis_t_slope =  1 ;
       CristaTerminalis_t_u = (CristaTerminalis_t_slope * d) + CristaTerminalis_t ;
       
       
       
      };
      CristaTerminalis_pstate = CristaTerminalis_cstate;
      CristaTerminalis_cstate = CristaTerminalis_ERP;
      CristaTerminalis_force_init_update = false;
      CristaTerminalis_ACTpath  = false;
      CristaTerminalis_ACTcell1  = false;
      CristaTerminalis_tick = true;
     };
     do
      :: CristaTerminalis_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (CristaTerminalis_t > 200.0) }
           && 
          CristaTerminalis_cstate == CristaTerminalis_ERP)  -> 
      d_step {
            c_code {  CristaTerminalis_t_u = 0 ; };
            CristaTerminalis_pstate =  CristaTerminalis_cstate ;
            CristaTerminalis_cstate =  CristaTerminalis_RRP ;
            CristaTerminalis_force_init_update = false;
            CristaTerminalis_tick = true;
      };
      do
       :: CristaTerminalis_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (CristaTerminalis_t <= 100.0) }  &&  (! CristaTerminalis_ACTcell1)  &&  CristaTerminalis_cstate == CristaTerminalis_RRP) ->
     if
      :: CristaTerminalis_pstate != CristaTerminalis_cstate || CristaTerminalis_force_init_update ->
       c_code { CristaTerminalis_t_init = CristaTerminalis_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       CristaTerminalis_t_slope =  1 ;
       CristaTerminalis_t_u = (CristaTerminalis_t_slope * d) + CristaTerminalis_t ;
       
       
       
      };
      CristaTerminalis_pstate = CristaTerminalis_cstate;
      CristaTerminalis_cstate = CristaTerminalis_RRP;
      CristaTerminalis_force_init_update = false;
      CristaTerminalis_ACTpath  = false;
      CristaTerminalis_ACTcell1  = false;
      CristaTerminalis_tick = true;
     };
     do
      :: CristaTerminalis_tick -> true
      :: else -> goto  RRP
     od;
     ::  (CristaTerminalis_ACTcell1
           && 
          CristaTerminalis_cstate == CristaTerminalis_RRP)  -> 
      d_step {
            c_code {  CristaTerminalis_t_u = 0 ; };
            CristaTerminalis_pstate =  CristaTerminalis_cstate ;
            CristaTerminalis_cstate =  CristaTerminalis_ST ;
            CristaTerminalis_force_init_update = false;
            CristaTerminalis_tick = true;
      };
      do
       :: CristaTerminalis_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (CristaTerminalis_t > 100.0) }
           && 
          CristaTerminalis_cstate == CristaTerminalis_RRP)  -> 
      d_step {
            c_code {  CristaTerminalis_t_u = 0 ; };
            CristaTerminalis_pstate =  CristaTerminalis_cstate ;
            CristaTerminalis_cstate =  CristaTerminalis_REST ;
            CristaTerminalis_force_init_update = false;
            CristaTerminalis_tick = true;
      };
      do
       :: CristaTerminalis_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double CristaTerminalis1_t_u, CristaTerminalis1_t_slope, CristaTerminalis1_t_init, CristaTerminalis1_t =  0 ;};
c_track "&CristaTerminalis1_t" "sizeof(double)";
c_track "&CristaTerminalis1_t_slope" "sizeof(double)";
c_track "&CristaTerminalis1_t_u" "sizeof(double)";
c_track "&CristaTerminalis1_t_init" "sizeof(double)";


bool CristaTerminalis1_ACTpath = 0 ;
bool CristaTerminalis1_ACTcell1 ;
mtype { CristaTerminalis1_REST, CristaTerminalis1_ST, CristaTerminalis1_ERP, CristaTerminalis1_RRP };
proctype CristaTerminalis1 () {
REST:
   if
    :: (c_expr { (CristaTerminalis1_t <= 400.0) }  &&  (! CristaTerminalis1_ACTcell1)  &&  CristaTerminalis1_cstate == CristaTerminalis1_REST) ->
     if
      :: CristaTerminalis1_pstate != CristaTerminalis1_cstate || CristaTerminalis1_force_init_update ->
       c_code { CristaTerminalis1_t_init = CristaTerminalis1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       CristaTerminalis1_t_slope =  1 ;
       CristaTerminalis1_t_u = (CristaTerminalis1_t_slope * d) + CristaTerminalis1_t ;
       
       
       
      };
      CristaTerminalis1_pstate = CristaTerminalis1_cstate;
      CristaTerminalis1_cstate = CristaTerminalis1_REST;
      CristaTerminalis1_force_init_update = false;
      CristaTerminalis1_ACTpath  = false;
      CristaTerminalis1_ACTcell1  = false;
      CristaTerminalis1_tick = true;
     };
     do
      :: CristaTerminalis1_tick -> true
      :: else -> goto  REST
     od;
     ::  (CristaTerminalis1_ACTcell1
           && 
          CristaTerminalis1_cstate == CristaTerminalis1_REST)  -> 
      d_step {
            c_code {  CristaTerminalis1_t_u = 0 ; };
            CristaTerminalis1_pstate =  CristaTerminalis1_cstate ;
            CristaTerminalis1_cstate =  CristaTerminalis1_ST ;
            CristaTerminalis1_force_init_update = false;
            CristaTerminalis1_tick = true;
      };
      do
       :: CristaTerminalis1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (CristaTerminalis1_t > 400.0) }
           && 
          CristaTerminalis1_cstate == CristaTerminalis1_REST)  -> 
      d_step {
            c_code {  CristaTerminalis1_t_u = 0 ; };
            CristaTerminalis1_pstate =  CristaTerminalis1_cstate ;
            CristaTerminalis1_cstate =  CristaTerminalis1_ST ;
            CristaTerminalis1_force_init_update = false;
            CristaTerminalis1_tick = true;
      };
      do
       :: CristaTerminalis1_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (CristaTerminalis1_t <= 10.0) }  &&  (! CristaTerminalis1_ACTcell1)  &&  CristaTerminalis1_cstate == CristaTerminalis1_ST) ->
     if
      :: CristaTerminalis1_pstate != CristaTerminalis1_cstate || CristaTerminalis1_force_init_update ->
       c_code { CristaTerminalis1_t_init = CristaTerminalis1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       CristaTerminalis1_t_slope =  1 ;
       CristaTerminalis1_t_u = (CristaTerminalis1_t_slope * d) + CristaTerminalis1_t ;
       
       
       
      };
      CristaTerminalis1_pstate = CristaTerminalis1_cstate;
      CristaTerminalis1_cstate = CristaTerminalis1_ST;
      CristaTerminalis1_force_init_update = false;
      CristaTerminalis1_ACTpath  = false;
      CristaTerminalis1_ACTcell1  = false;
      CristaTerminalis1_tick = true;
     };
     do
      :: CristaTerminalis1_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (CristaTerminalis1_t > 10.0) }
           && 
          CristaTerminalis1_cstate == CristaTerminalis1_ST)  -> 
      d_step {
            c_code {  CristaTerminalis1_t_u = 0 ; };
            CristaTerminalis1_ACTpath = 1;
            CristaTerminalis1_pstate =  CristaTerminalis1_cstate ;
            CristaTerminalis1_cstate =  CristaTerminalis1_ERP ;
            CristaTerminalis1_force_init_update = false;
            CristaTerminalis1_tick = true;
      };
      do
       :: CristaTerminalis1_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (CristaTerminalis1_t <= 200.0) }  &&  (! CristaTerminalis1_ACTcell1)  &&  CristaTerminalis1_cstate == CristaTerminalis1_ERP) ->
     if
      :: CristaTerminalis1_pstate != CristaTerminalis1_cstate || CristaTerminalis1_force_init_update ->
       c_code { CristaTerminalis1_t_init = CristaTerminalis1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       CristaTerminalis1_t_slope =  1 ;
       CristaTerminalis1_t_u = (CristaTerminalis1_t_slope * d) + CristaTerminalis1_t ;
       
       
       
      };
      CristaTerminalis1_pstate = CristaTerminalis1_cstate;
      CristaTerminalis1_cstate = CristaTerminalis1_ERP;
      CristaTerminalis1_force_init_update = false;
      CristaTerminalis1_ACTpath  = false;
      CristaTerminalis1_ACTcell1  = false;
      CristaTerminalis1_tick = true;
     };
     do
      :: CristaTerminalis1_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (CristaTerminalis1_t > 200.0) }
           && 
          CristaTerminalis1_cstate == CristaTerminalis1_ERP)  -> 
      d_step {
            c_code {  CristaTerminalis1_t_u = 0 ; };
            CristaTerminalis1_pstate =  CristaTerminalis1_cstate ;
            CristaTerminalis1_cstate =  CristaTerminalis1_RRP ;
            CristaTerminalis1_force_init_update = false;
            CristaTerminalis1_tick = true;
      };
      do
       :: CristaTerminalis1_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (CristaTerminalis1_t <= 100.0) }  &&  (! CristaTerminalis1_ACTcell1)  &&  CristaTerminalis1_cstate == CristaTerminalis1_RRP) ->
     if
      :: CristaTerminalis1_pstate != CristaTerminalis1_cstate || CristaTerminalis1_force_init_update ->
       c_code { CristaTerminalis1_t_init = CristaTerminalis1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       CristaTerminalis1_t_slope =  1 ;
       CristaTerminalis1_t_u = (CristaTerminalis1_t_slope * d) + CristaTerminalis1_t ;
       
       
       
      };
      CristaTerminalis1_pstate = CristaTerminalis1_cstate;
      CristaTerminalis1_cstate = CristaTerminalis1_RRP;
      CristaTerminalis1_force_init_update = false;
      CristaTerminalis1_ACTpath  = false;
      CristaTerminalis1_ACTcell1  = false;
      CristaTerminalis1_tick = true;
     };
     do
      :: CristaTerminalis1_tick -> true
      :: else -> goto  RRP
     od;
     ::  (CristaTerminalis1_ACTcell1
           && 
          CristaTerminalis1_cstate == CristaTerminalis1_RRP)  -> 
      d_step {
            c_code {  CristaTerminalis1_t_u = 0 ; };
            CristaTerminalis1_pstate =  CristaTerminalis1_cstate ;
            CristaTerminalis1_cstate =  CristaTerminalis1_ST ;
            CristaTerminalis1_force_init_update = false;
            CristaTerminalis1_tick = true;
      };
      do
       :: CristaTerminalis1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (CristaTerminalis1_t > 100.0) }
           && 
          CristaTerminalis1_cstate == CristaTerminalis1_RRP)  -> 
      d_step {
            c_code {  CristaTerminalis1_t_u = 0 ; };
            CristaTerminalis1_pstate =  CristaTerminalis1_cstate ;
            CristaTerminalis1_cstate =  CristaTerminalis1_REST ;
            CristaTerminalis1_force_init_update = false;
            CristaTerminalis1_tick = true;
      };
      do
       :: CristaTerminalis1_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double Ostium_t_u, Ostium_t_slope, Ostium_t_init, Ostium_t =  0 ;};
c_track "&Ostium_t" "sizeof(double)";
c_track "&Ostium_t_slope" "sizeof(double)";
c_track "&Ostium_t_u" "sizeof(double)";
c_track "&Ostium_t_init" "sizeof(double)";


bool Ostium_ACTpath = 0 ;
bool Ostium_ACTcell1 ;
mtype { Ostium_REST, Ostium_ST, Ostium_ERP, Ostium_RRP };
proctype Ostium () {
REST:
   if
    :: (c_expr { (Ostium_t <= 400.0) }  &&  (! Ostium_ACTcell1)  &&  Ostium_cstate == Ostium_REST) ->
     if
      :: Ostium_pstate != Ostium_cstate || Ostium_force_init_update ->
       c_code { Ostium_t_init = Ostium_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Ostium_t_slope =  1 ;
       Ostium_t_u = (Ostium_t_slope * d) + Ostium_t ;
       
       
       
      };
      Ostium_pstate = Ostium_cstate;
      Ostium_cstate = Ostium_REST;
      Ostium_force_init_update = false;
      Ostium_ACTpath  = false;
      Ostium_ACTcell1  = false;
      Ostium_tick = true;
     };
     do
      :: Ostium_tick -> true
      :: else -> goto  REST
     od;
     ::  (Ostium_ACTcell1 && 
          Ostium_cstate == Ostium_REST)  -> 
      d_step {
            c_code {  Ostium_t_u = 0 ; };
            Ostium_pstate =  Ostium_cstate ;
            Ostium_cstate =  Ostium_ST ;
            Ostium_force_init_update = false;
            Ostium_tick = true;
      };
      do
       :: Ostium_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (Ostium_t > 400.0) }
           && 
          Ostium_cstate == Ostium_REST)  -> 
      d_step {
            c_code {  Ostium_t_u = 0 ; };
            Ostium_pstate =  Ostium_cstate ;
            Ostium_cstate =  Ostium_ST ;
            Ostium_force_init_update = false;
            Ostium_tick = true;
      };
      do
       :: Ostium_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (Ostium_t <= 10.0) }  &&  (! Ostium_ACTcell1)  &&  Ostium_cstate == Ostium_ST) ->
     if
      :: Ostium_pstate != Ostium_cstate || Ostium_force_init_update ->
       c_code { Ostium_t_init = Ostium_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Ostium_t_slope =  1 ;
       Ostium_t_u = (Ostium_t_slope * d) + Ostium_t ;
       
       
       
      };
      Ostium_pstate = Ostium_cstate;
      Ostium_cstate = Ostium_ST;
      Ostium_force_init_update = false;
      Ostium_ACTpath  = false;
      Ostium_ACTcell1  = false;
      Ostium_tick = true;
     };
     do
      :: Ostium_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (Ostium_t > 10.0) }
           && 
          Ostium_cstate == Ostium_ST)  -> 
      d_step {
            c_code {  Ostium_t_u = 0 ; };
            Ostium_ACTpath = 1;
            Ostium_pstate =  Ostium_cstate ;
            Ostium_cstate =  Ostium_ERP ;
            Ostium_force_init_update = false;
            Ostium_tick = true;
      };
      do
       :: Ostium_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (Ostium_t <= 200.0) }  &&  (! Ostium_ACTcell1)  &&  Ostium_cstate == Ostium_ERP) ->
     if
      :: Ostium_pstate != Ostium_cstate || Ostium_force_init_update ->
       c_code { Ostium_t_init = Ostium_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Ostium_t_slope =  1 ;
       Ostium_t_u = (Ostium_t_slope * d) + Ostium_t ;
       
       
       
      };
      Ostium_pstate = Ostium_cstate;
      Ostium_cstate = Ostium_ERP;
      Ostium_force_init_update = false;
      Ostium_ACTpath  = false;
      Ostium_ACTcell1  = false;
      Ostium_tick = true;
     };
     do
      :: Ostium_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (Ostium_t > 200.0) }
           && 
          Ostium_cstate == Ostium_ERP)  -> 
      d_step {
            c_code {  Ostium_t_u = 0 ; };
            Ostium_pstate =  Ostium_cstate ;
            Ostium_cstate =  Ostium_RRP ;
            Ostium_force_init_update = false;
            Ostium_tick = true;
      };
      do
       :: Ostium_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (Ostium_t <= 100.0) }  &&  (! Ostium_ACTcell1)  &&  Ostium_cstate == Ostium_RRP) ->
     if
      :: Ostium_pstate != Ostium_cstate || Ostium_force_init_update ->
       c_code { Ostium_t_init = Ostium_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Ostium_t_slope =  1 ;
       Ostium_t_u = (Ostium_t_slope * d) + Ostium_t ;
       
       
       
      };
      Ostium_pstate = Ostium_cstate;
      Ostium_cstate = Ostium_RRP;
      Ostium_force_init_update = false;
      Ostium_ACTpath  = false;
      Ostium_ACTcell1  = false;
      Ostium_tick = true;
     };
     do
      :: Ostium_tick -> true
      :: else -> goto  RRP
     od;
     ::  (Ostium_ACTcell1 && 
          Ostium_cstate == Ostium_RRP)  -> 
      d_step {
            c_code {  Ostium_t_u = 0 ; };
            Ostium_pstate =  Ostium_cstate ;
            Ostium_cstate =  Ostium_ST ;
            Ostium_force_init_update = false;
            Ostium_tick = true;
      };
      do
       :: Ostium_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (Ostium_t > 100.0) }
           && 
          Ostium_cstate == Ostium_RRP)  -> 
      d_step {
            c_code {  Ostium_t_u = 0 ; };
            Ostium_pstate =  Ostium_cstate ;
            Ostium_cstate =  Ostium_REST ;
            Ostium_force_init_update = false;
            Ostium_tick = true;
      };
      do
       :: Ostium_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double Fast_t_u, Fast_t_slope, Fast_t_init, Fast_t =  0 ;};
c_track "&Fast_t" "sizeof(double)";
c_track "&Fast_t_slope" "sizeof(double)";
c_track "&Fast_t_u" "sizeof(double)";
c_track "&Fast_t_init" "sizeof(double)";


bool Fast_ACTpath = 0 ;
bool Fast_ACTcell1 ;
mtype { Fast_REST, Fast_ST, Fast_ERP, Fast_RRP };
proctype Fast () {
REST:
   if
    :: (c_expr { (Fast_t <= 400.0) }  &&  (! Fast_ACTcell1)  &&  Fast_cstate == Fast_REST) ->
     if
      :: Fast_pstate != Fast_cstate || Fast_force_init_update ->
       c_code { Fast_t_init = Fast_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Fast_t_slope =  1 ;
       Fast_t_u = (Fast_t_slope * d) + Fast_t ;
       
       
       
      };
      Fast_pstate = Fast_cstate;
      Fast_cstate = Fast_REST;
      Fast_force_init_update = false;
      Fast_ACTpath  = false;
      Fast_ACTcell1  = false;
      Fast_tick = true;
     };
     do
      :: Fast_tick -> true
      :: else -> goto  REST
     od;
     ::  (Fast_ACTcell1 && 
          Fast_cstate == Fast_REST)  -> 
      d_step {
            c_code {  Fast_t_u = 0 ; };
            Fast_pstate =  Fast_cstate ;
            Fast_cstate =  Fast_ST ;
            Fast_force_init_update = false;
            Fast_tick = true;
      };
      do
       :: Fast_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (Fast_t > 400.0) }
           && 
          Fast_cstate == Fast_REST)  -> 
      d_step {
            c_code {  Fast_t_u = 0 ; };
            Fast_pstate =  Fast_cstate ;
            Fast_cstate =  Fast_ST ;
            Fast_force_init_update = false;
            Fast_tick = true;
      };
      do
       :: Fast_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (Fast_t <= 10.0) }  &&  (! Fast_ACTcell1)  &&  Fast_cstate == Fast_ST) ->
     if
      :: Fast_pstate != Fast_cstate || Fast_force_init_update ->
       c_code { Fast_t_init = Fast_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Fast_t_slope =  1 ;
       Fast_t_u = (Fast_t_slope * d) + Fast_t ;
       
       
       
      };
      Fast_pstate = Fast_cstate;
      Fast_cstate = Fast_ST;
      Fast_force_init_update = false;
      Fast_ACTpath  = false;
      Fast_ACTcell1  = false;
      Fast_tick = true;
     };
     do
      :: Fast_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (Fast_t > 10.0) }
           && Fast_cstate == Fast_ST)  -> 
      d_step {
            c_code {  Fast_t_u = 0 ; };
            Fast_ACTpath = 1;
            Fast_pstate =  Fast_cstate ;
            Fast_cstate =  Fast_ERP ;
            Fast_force_init_update = false;
            Fast_tick = true;
      };
      do
       :: Fast_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (Fast_t <= 200.0) }  &&  (! Fast_ACTcell1)  &&  Fast_cstate == Fast_ERP) ->
     if
      :: Fast_pstate != Fast_cstate || Fast_force_init_update ->
       c_code { Fast_t_init = Fast_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Fast_t_slope =  1 ;
       Fast_t_u = (Fast_t_slope * d) + Fast_t ;
       
       
       
      };
      Fast_pstate = Fast_cstate;
      Fast_cstate = Fast_ERP;
      Fast_force_init_update = false;
      Fast_ACTpath  = false;
      Fast_ACTcell1  = false;
      Fast_tick = true;
     };
     do
      :: Fast_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (Fast_t > 200.0) }
           && 
          Fast_cstate == Fast_ERP)  -> 
      d_step {
            c_code {  Fast_t_u = 0 ; };
            Fast_pstate =  Fast_cstate ;
            Fast_cstate =  Fast_RRP ;
            Fast_force_init_update = false;
            Fast_tick = true;
      };
      do
       :: Fast_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (Fast_t <= 100.0) }  &&  (! Fast_ACTcell1)  &&  Fast_cstate == Fast_RRP) ->
     if
      :: Fast_pstate != Fast_cstate || Fast_force_init_update ->
       c_code { Fast_t_init = Fast_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Fast_t_slope =  1 ;
       Fast_t_u = (Fast_t_slope * d) + Fast_t ;
       
       
       
      };
      Fast_pstate = Fast_cstate;
      Fast_cstate = Fast_RRP;
      Fast_force_init_update = false;
      Fast_ACTpath  = false;
      Fast_ACTcell1  = false;
      Fast_tick = true;
     };
     do
      :: Fast_tick -> true
      :: else -> goto  RRP
     od;
     ::  (Fast_ACTcell1 && 
          Fast_cstate == Fast_RRP)  -> 
      d_step {
            c_code {  Fast_t_u = 0 ; };
            Fast_pstate =  Fast_cstate ;
            Fast_cstate =  Fast_ST ;
            Fast_force_init_update = false;
            Fast_tick = true;
      };
      do
       :: Fast_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (Fast_t > 100.0) }
           && 
          Fast_cstate == Fast_RRP)  -> 
      d_step {
            c_code {  Fast_t_u = 0 ; };
            Fast_pstate =  Fast_cstate ;
            Fast_cstate =  Fast_REST ;
            Fast_force_init_update = false;
            Fast_tick = true;
      };
      do
       :: Fast_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double Fast1_t_u, Fast1_t_slope, Fast1_t_init, Fast1_t =  0 ;};
c_track "&Fast1_t" "sizeof(double)";
c_track "&Fast1_t_slope" "sizeof(double)";
c_track "&Fast1_t_u" "sizeof(double)";
c_track "&Fast1_t_init" "sizeof(double)";


bool Fast1_ACTpath = 0 ;
bool Fast1_ACTcell1 ;
mtype { Fast1_REST, Fast1_ST, Fast1_ERP, Fast1_RRP };
proctype Fast1 () {
REST:
   if
    :: (c_expr { (Fast1_t <= 400.0) }  &&  (! Fast1_ACTcell1)  &&  Fast1_cstate == Fast1_REST) ->
     if
      :: Fast1_pstate != Fast1_cstate || Fast1_force_init_update ->
       c_code { Fast1_t_init = Fast1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Fast1_t_slope =  1 ;
       Fast1_t_u = (Fast1_t_slope * d) + Fast1_t ;
       
       
       
      };
      Fast1_pstate = Fast1_cstate;
      Fast1_cstate = Fast1_REST;
      Fast1_force_init_update = false;
      Fast1_ACTpath  = false;
      Fast1_ACTcell1  = false;
      Fast1_tick = true;
     };
     do
      :: Fast1_tick -> true
      :: else -> goto  REST
     od;
     ::  (Fast1_ACTcell1 && 
          Fast1_cstate == Fast1_REST)  -> 
      d_step {
            c_code {  Fast1_t_u = 0 ; };
            Fast1_pstate =  Fast1_cstate ;
            Fast1_cstate =  Fast1_ST ;
            Fast1_force_init_update = false;
            Fast1_tick = true;
      };
      do
       :: Fast1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (Fast1_t > 400.0) }
           && 
          Fast1_cstate == Fast1_REST)  -> 
      d_step {
            c_code {  Fast1_t_u = 0 ; };
            Fast1_pstate =  Fast1_cstate ;
            Fast1_cstate =  Fast1_ST ;
            Fast1_force_init_update = false;
            Fast1_tick = true;
      };
      do
       :: Fast1_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (Fast1_t <= 10.0) }  &&  (! Fast1_ACTcell1)  &&  Fast1_cstate == Fast1_ST) ->
     if
      :: Fast1_pstate != Fast1_cstate || Fast1_force_init_update ->
       c_code { Fast1_t_init = Fast1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Fast1_t_slope =  1 ;
       Fast1_t_u = (Fast1_t_slope * d) + Fast1_t ;
       
       
       
      };
      Fast1_pstate = Fast1_cstate;
      Fast1_cstate = Fast1_ST;
      Fast1_force_init_update = false;
      Fast1_ACTpath  = false;
      Fast1_ACTcell1  = false;
      Fast1_tick = true;
     };
     do
      :: Fast1_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (Fast1_t > 10.0) }
           && 
          Fast1_cstate == Fast1_ST)  -> 
      d_step {
            c_code {  Fast1_t_u = 0 ; };
            Fast1_ACTpath = 1;
            Fast1_pstate =  Fast1_cstate ;
            Fast1_cstate =  Fast1_ERP ;
            Fast1_force_init_update = false;
            Fast1_tick = true;
      };
      do
       :: Fast1_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (Fast1_t <= 200.0) }  &&  (! Fast1_ACTcell1)  &&  Fast1_cstate == Fast1_ERP) ->
     if
      :: Fast1_pstate != Fast1_cstate || Fast1_force_init_update ->
       c_code { Fast1_t_init = Fast1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Fast1_t_slope =  1 ;
       Fast1_t_u = (Fast1_t_slope * d) + Fast1_t ;
       
       
       
      };
      Fast1_pstate = Fast1_cstate;
      Fast1_cstate = Fast1_ERP;
      Fast1_force_init_update = false;
      Fast1_ACTpath  = false;
      Fast1_ACTcell1  = false;
      Fast1_tick = true;
     };
     do
      :: Fast1_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (Fast1_t > 200.0) }
           && 
          Fast1_cstate == Fast1_ERP)  -> 
      d_step {
            c_code {  Fast1_t_u = 0 ; };
            Fast1_pstate =  Fast1_cstate ;
            Fast1_cstate =  Fast1_RRP ;
            Fast1_force_init_update = false;
            Fast1_tick = true;
      };
      do
       :: Fast1_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (Fast1_t <= 100.0) }  &&  (! Fast1_ACTcell1)  &&  Fast1_cstate == Fast1_RRP) ->
     if
      :: Fast1_pstate != Fast1_cstate || Fast1_force_init_update ->
       c_code { Fast1_t_init = Fast1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Fast1_t_slope =  1 ;
       Fast1_t_u = (Fast1_t_slope * d) + Fast1_t ;
       
       
       
      };
      Fast1_pstate = Fast1_cstate;
      Fast1_cstate = Fast1_RRP;
      Fast1_force_init_update = false;
      Fast1_ACTpath  = false;
      Fast1_ACTcell1  = false;
      Fast1_tick = true;
     };
     do
      :: Fast1_tick -> true
      :: else -> goto  RRP
     od;
     ::  (Fast1_ACTcell1 && 
          Fast1_cstate == Fast1_RRP)  -> 
      d_step {
            c_code {  Fast1_t_u = 0 ; };
            Fast1_pstate =  Fast1_cstate ;
            Fast1_cstate =  Fast1_ST ;
            Fast1_force_init_update = false;
            Fast1_tick = true;
      };
      do
       :: Fast1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (Fast1_t > 100.0) }
           && 
          Fast1_cstate == Fast1_RRP)  -> 
      d_step {
            c_code {  Fast1_t_u = 0 ; };
            Fast1_pstate =  Fast1_cstate ;
            Fast1_cstate =  Fast1_REST ;
            Fast1_force_init_update = false;
            Fast1_tick = true;
      };
      do
       :: Fast1_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double Slow_t_u, Slow_t_slope, Slow_t_init, Slow_t =  0 ;};
c_track "&Slow_t" "sizeof(double)";
c_track "&Slow_t_slope" "sizeof(double)";
c_track "&Slow_t_u" "sizeof(double)";
c_track "&Slow_t_init" "sizeof(double)";


bool Slow_ACTpath = 0 ;
bool Slow_ACTcell1 ;
mtype { Slow_REST, Slow_ST, Slow_ERP, Slow_RRP };
proctype Slow () {
REST:
   if
    :: (c_expr { (Slow_t <= 400.0) }  &&  (! Slow_ACTcell1)  &&  Slow_cstate == Slow_REST) ->
     if
      :: Slow_pstate != Slow_cstate || Slow_force_init_update ->
       c_code { Slow_t_init = Slow_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Slow_t_slope =  1 ;
       Slow_t_u = (Slow_t_slope * d) + Slow_t ;
       
       
       
      };
      Slow_pstate = Slow_cstate;
      Slow_cstate = Slow_REST;
      Slow_force_init_update = false;
      Slow_ACTpath  = false;
      Slow_ACTcell1  = false;
      Slow_tick = true;
     };
     do
      :: Slow_tick -> true
      :: else -> goto  REST
     od;
     ::  (Slow_ACTcell1 && 
          Slow_cstate == Slow_REST)  -> 
      d_step {
            c_code {  Slow_t_u = 0 ; };
            Slow_pstate =  Slow_cstate ;
            Slow_cstate =  Slow_ST ;
            Slow_force_init_update = false;
            Slow_tick = true;
      };
      do
       :: Slow_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (Slow_t > 400.0) }
           && 
          Slow_cstate == Slow_REST)  -> 
      d_step {
            c_code {  Slow_t_u = 0 ; };
            Slow_pstate =  Slow_cstate ;
            Slow_cstate =  Slow_ST ;
            Slow_force_init_update = false;
            Slow_tick = true;
      };
      do
       :: Slow_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (Slow_t <= 10.0) }  &&  (! Slow_ACTcell1)  &&  Slow_cstate == Slow_ST) ->
     if
      :: Slow_pstate != Slow_cstate || Slow_force_init_update ->
       c_code { Slow_t_init = Slow_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Slow_t_slope =  1 ;
       Slow_t_u = (Slow_t_slope * d) + Slow_t ;
       
       
       
      };
      Slow_pstate = Slow_cstate;
      Slow_cstate = Slow_ST;
      Slow_force_init_update = false;
      Slow_ACTpath  = false;
      Slow_ACTcell1  = false;
      Slow_tick = true;
     };
     do
      :: Slow_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (Slow_t > 10.0) }
           && Slow_cstate == Slow_ST)  -> 
      d_step {
            c_code {  Slow_t_u = 0 ; };
            Slow_ACTpath = 1;
            Slow_pstate =  Slow_cstate ;
            Slow_cstate =  Slow_ERP ;
            Slow_force_init_update = false;
            Slow_tick = true;
      };
      do
       :: Slow_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (Slow_t <= 200.0) }  &&  (! Slow_ACTcell1)  &&  Slow_cstate == Slow_ERP) ->
     if
      :: Slow_pstate != Slow_cstate || Slow_force_init_update ->
       c_code { Slow_t_init = Slow_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Slow_t_slope =  1 ;
       Slow_t_u = (Slow_t_slope * d) + Slow_t ;
       
       
       
      };
      Slow_pstate = Slow_cstate;
      Slow_cstate = Slow_ERP;
      Slow_force_init_update = false;
      Slow_ACTpath  = false;
      Slow_ACTcell1  = false;
      Slow_tick = true;
     };
     do
      :: Slow_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (Slow_t > 200.0) }
           && 
          Slow_cstate == Slow_ERP)  -> 
      d_step {
            c_code {  Slow_t_u = 0 ; };
            Slow_pstate =  Slow_cstate ;
            Slow_cstate =  Slow_RRP ;
            Slow_force_init_update = false;
            Slow_tick = true;
      };
      do
       :: Slow_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (Slow_t <= 100.0) }  &&  (! Slow_ACTcell1)  &&  Slow_cstate == Slow_RRP) ->
     if
      :: Slow_pstate != Slow_cstate || Slow_force_init_update ->
       c_code { Slow_t_init = Slow_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Slow_t_slope =  1 ;
       Slow_t_u = (Slow_t_slope * d) + Slow_t ;
       
       
       
      };
      Slow_pstate = Slow_cstate;
      Slow_cstate = Slow_RRP;
      Slow_force_init_update = false;
      Slow_ACTpath  = false;
      Slow_ACTcell1  = false;
      Slow_tick = true;
     };
     do
      :: Slow_tick -> true
      :: else -> goto  RRP
     od;
     ::  (Slow_ACTcell1 && 
          Slow_cstate == Slow_RRP)  -> 
      d_step {
            c_code {  Slow_t_u = 0 ; };
            Slow_pstate =  Slow_cstate ;
            Slow_cstate =  Slow_ST ;
            Slow_force_init_update = false;
            Slow_tick = true;
      };
      do
       :: Slow_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (Slow_t > 100.0) }
           && 
          Slow_cstate == Slow_RRP)  -> 
      d_step {
            c_code {  Slow_t_u = 0 ; };
            Slow_pstate =  Slow_cstate ;
            Slow_cstate =  Slow_REST ;
            Slow_force_init_update = false;
            Slow_tick = true;
      };
      do
       :: Slow_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double Slow1_t_u, Slow1_t_slope, Slow1_t_init, Slow1_t =  0 ;};
c_track "&Slow1_t" "sizeof(double)";
c_track "&Slow1_t_slope" "sizeof(double)";
c_track "&Slow1_t_u" "sizeof(double)";
c_track "&Slow1_t_init" "sizeof(double)";


bool Slow1_ACTpath = 0 ;
bool Slow1_ACTcell1 ;
mtype { Slow1_REST, Slow1_ST, Slow1_ERP, Slow1_RRP };
proctype Slow1 () {
REST:
   if
    :: (c_expr { (Slow1_t <= 400.0) }  &&  (! Slow1_ACTcell1)  &&  Slow1_cstate == Slow1_REST) ->
     if
      :: Slow1_pstate != Slow1_cstate || Slow1_force_init_update ->
       c_code { Slow1_t_init = Slow1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Slow1_t_slope =  1 ;
       Slow1_t_u = (Slow1_t_slope * d) + Slow1_t ;
       
       
       
      };
      Slow1_pstate = Slow1_cstate;
      Slow1_cstate = Slow1_REST;
      Slow1_force_init_update = false;
      Slow1_ACTpath  = false;
      Slow1_ACTcell1  = false;
      Slow1_tick = true;
     };
     do
      :: Slow1_tick -> true
      :: else -> goto  REST
     od;
     ::  (Slow1_ACTcell1 && 
          Slow1_cstate == Slow1_REST)  -> 
      d_step {
            c_code {  Slow1_t_u = 0 ; };
            Slow1_pstate =  Slow1_cstate ;
            Slow1_cstate =  Slow1_ST ;
            Slow1_force_init_update = false;
            Slow1_tick = true;
      };
      do
       :: Slow1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (Slow1_t > 400.0) }
           && 
          Slow1_cstate == Slow1_REST)  -> 
      d_step {
            c_code {  Slow1_t_u = 0 ; };
            Slow1_pstate =  Slow1_cstate ;
            Slow1_cstate =  Slow1_ST ;
            Slow1_force_init_update = false;
            Slow1_tick = true;
      };
      do
       :: Slow1_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (Slow1_t <= 10.0) }  &&  (! Slow1_ACTcell1)  &&  Slow1_cstate == Slow1_ST) ->
     if
      :: Slow1_pstate != Slow1_cstate || Slow1_force_init_update ->
       c_code { Slow1_t_init = Slow1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Slow1_t_slope =  1 ;
       Slow1_t_u = (Slow1_t_slope * d) + Slow1_t ;
       
       
       
      };
      Slow1_pstate = Slow1_cstate;
      Slow1_cstate = Slow1_ST;
      Slow1_force_init_update = false;
      Slow1_ACTpath  = false;
      Slow1_ACTcell1  = false;
      Slow1_tick = true;
     };
     do
      :: Slow1_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (Slow1_t > 10.0) }
           && 
          Slow1_cstate == Slow1_ST)  -> 
      d_step {
            c_code {  Slow1_t_u = 0 ; };
            Slow1_ACTpath = 1;
            Slow1_pstate =  Slow1_cstate ;
            Slow1_cstate =  Slow1_ERP ;
            Slow1_force_init_update = false;
            Slow1_tick = true;
      };
      do
       :: Slow1_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (Slow1_t <= 200.0) }  &&  (! Slow1_ACTcell1)  &&  Slow1_cstate == Slow1_ERP) ->
     if
      :: Slow1_pstate != Slow1_cstate || Slow1_force_init_update ->
       c_code { Slow1_t_init = Slow1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Slow1_t_slope =  1 ;
       Slow1_t_u = (Slow1_t_slope * d) + Slow1_t ;
       
       
       
      };
      Slow1_pstate = Slow1_cstate;
      Slow1_cstate = Slow1_ERP;
      Slow1_force_init_update = false;
      Slow1_ACTpath  = false;
      Slow1_ACTcell1  = false;
      Slow1_tick = true;
     };
     do
      :: Slow1_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (Slow1_t > 200.0) }
           && 
          Slow1_cstate == Slow1_ERP)  -> 
      d_step {
            c_code {  Slow1_t_u = 0 ; };
            Slow1_pstate =  Slow1_cstate ;
            Slow1_cstate =  Slow1_RRP ;
            Slow1_force_init_update = false;
            Slow1_tick = true;
      };
      do
       :: Slow1_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (Slow1_t <= 100.0) }  &&  (! Slow1_ACTcell1)  &&  Slow1_cstate == Slow1_RRP) ->
     if
      :: Slow1_pstate != Slow1_cstate || Slow1_force_init_update ->
       c_code { Slow1_t_init = Slow1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Slow1_t_slope =  1 ;
       Slow1_t_u = (Slow1_t_slope * d) + Slow1_t ;
       
       
       
      };
      Slow1_pstate = Slow1_cstate;
      Slow1_cstate = Slow1_RRP;
      Slow1_force_init_update = false;
      Slow1_ACTpath  = false;
      Slow1_ACTcell1  = false;
      Slow1_tick = true;
     };
     do
      :: Slow1_tick -> true
      :: else -> goto  RRP
     od;
     ::  (Slow1_ACTcell1 && 
          Slow1_cstate == Slow1_RRP)  -> 
      d_step {
            c_code {  Slow1_t_u = 0 ; };
            Slow1_pstate =  Slow1_cstate ;
            Slow1_cstate =  Slow1_ST ;
            Slow1_force_init_update = false;
            Slow1_tick = true;
      };
      do
       :: Slow1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (Slow1_t > 100.0) }
           && 
          Slow1_cstate == Slow1_RRP)  -> 
      d_step {
            c_code {  Slow1_t_u = 0 ; };
            Slow1_pstate =  Slow1_cstate ;
            Slow1_cstate =  Slow1_REST ;
            Slow1_force_init_update = false;
            Slow1_tick = true;
      };
      do
       :: Slow1_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double AtrioventricularNode_t_u, AtrioventricularNode_t_slope, AtrioventricularNode_t_init, AtrioventricularNode_t =  0 ;};
c_track "&AtrioventricularNode_t" "sizeof(double)";
c_track "&AtrioventricularNode_t_slope" "sizeof(double)";
c_track "&AtrioventricularNode_t_u" "sizeof(double)";
c_track "&AtrioventricularNode_t_init" "sizeof(double)";


bool AtrioventricularNode_ACTpath = 0 ;
bool AtrioventricularNode_ACTcell1 ;
bool AtrioventricularNode_ACTcell2 ;
mtype { AtrioventricularNode_REST, AtrioventricularNode_ST, AtrioventricularNode_ERP, AtrioventricularNode_RRP };
proctype AtrioventricularNode () {
REST:
   if
    :: (c_expr { (AtrioventricularNode_t <= 400.0) }  &&  (! AtrioventricularNode_ACTcell1)
     && 
    (! AtrioventricularNode_ACTcell2)  &&  AtrioventricularNode_cstate == AtrioventricularNode_REST) ->
     if
      :: AtrioventricularNode_pstate != AtrioventricularNode_cstate || AtrioventricularNode_force_init_update ->
       c_code { AtrioventricularNode_t_init = AtrioventricularNode_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       AtrioventricularNode_t_slope =  1 ;
       AtrioventricularNode_t_u = (AtrioventricularNode_t_slope * d) + AtrioventricularNode_t ;
       
       
       
      };
      AtrioventricularNode_pstate = AtrioventricularNode_cstate;
      AtrioventricularNode_cstate = AtrioventricularNode_REST;
      AtrioventricularNode_force_init_update = false;
      AtrioventricularNode_ACTpath  = false;
      AtrioventricularNode_ACTcell1  = false;
      AtrioventricularNode_ACTcell2  = false;
      AtrioventricularNode_tick = true;
     };
     do
      :: AtrioventricularNode_tick -> true
      :: else -> goto  REST
     od;
     ::  (AtrioventricularNode_ACTcell1
           && 
          AtrioventricularNode_cstate == AtrioventricularNode_REST)  -> 
      d_step {
            c_code {  AtrioventricularNode_t_u = 0 ; };
            AtrioventricularNode_pstate =  AtrioventricularNode_cstate ;
            AtrioventricularNode_cstate =  AtrioventricularNode_ST ;
            AtrioventricularNode_force_init_update = false;
            AtrioventricularNode_tick = true;
      };
      do
       :: AtrioventricularNode_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (AtrioventricularNode_t > 400.0) }
           && 
          AtrioventricularNode_cstate == AtrioventricularNode_REST)  -> 
      d_step {
            c_code {  AtrioventricularNode_t_u = 0 ; };
            AtrioventricularNode_pstate =  AtrioventricularNode_cstate ;
            AtrioventricularNode_cstate =  AtrioventricularNode_ST ;
            AtrioventricularNode_force_init_update = false;
            AtrioventricularNode_tick = true;
      };
      do
       :: AtrioventricularNode_tick -> true
       :: else -> goto  ST
      od;
     ::  (AtrioventricularNode_ACTcell2
           && 
          AtrioventricularNode_cstate == AtrioventricularNode_REST)  -> 
      d_step {
            c_code {  AtrioventricularNode_t_u = 0 ; };
            AtrioventricularNode_pstate =  AtrioventricularNode_cstate ;
            AtrioventricularNode_cstate =  AtrioventricularNode_ST ;
            AtrioventricularNode_force_init_update = false;
            AtrioventricularNode_tick = true;
      };
      do
       :: AtrioventricularNode_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (AtrioventricularNode_t <= 10.0) }  &&  (! AtrioventricularNode_ACTcell1)
     && 
    (! AtrioventricularNode_ACTcell2)  &&  AtrioventricularNode_cstate == AtrioventricularNode_ST) ->
     if
      :: AtrioventricularNode_pstate != AtrioventricularNode_cstate || AtrioventricularNode_force_init_update ->
       c_code { AtrioventricularNode_t_init = AtrioventricularNode_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       AtrioventricularNode_t_slope =  1 ;
       AtrioventricularNode_t_u = (AtrioventricularNode_t_slope * d) + AtrioventricularNode_t ;
       
       
       
      };
      AtrioventricularNode_pstate = AtrioventricularNode_cstate;
      AtrioventricularNode_cstate = AtrioventricularNode_ST;
      AtrioventricularNode_force_init_update = false;
      AtrioventricularNode_ACTpath  = false;
      AtrioventricularNode_ACTcell1  = false;
      AtrioventricularNode_ACTcell2  = false;
      AtrioventricularNode_tick = true;
     };
     do
      :: AtrioventricularNode_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (AtrioventricularNode_t > 10.0) }
           && 
          AtrioventricularNode_cstate == AtrioventricularNode_ST)  -> 
      d_step {
            c_code {  AtrioventricularNode_t_u = 0 ; };
            AtrioventricularNode_ACTpath = 1;
            AtrioventricularNode_pstate =  AtrioventricularNode_cstate ;
            AtrioventricularNode_cstate =  AtrioventricularNode_ERP ;
            AtrioventricularNode_force_init_update = false;
            AtrioventricularNode_tick = true;
      };
      do
       :: AtrioventricularNode_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (AtrioventricularNode_t <= 200.0) }  &&  (! AtrioventricularNode_ACTcell1)
     && 
    (! AtrioventricularNode_ACTcell2)  &&  AtrioventricularNode_cstate == AtrioventricularNode_ERP) ->
     if
      :: AtrioventricularNode_pstate != AtrioventricularNode_cstate || AtrioventricularNode_force_init_update ->
       c_code { AtrioventricularNode_t_init = AtrioventricularNode_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       AtrioventricularNode_t_slope =  1 ;
       AtrioventricularNode_t_u = (AtrioventricularNode_t_slope * d) + AtrioventricularNode_t ;
       
       
       
      };
      AtrioventricularNode_pstate = AtrioventricularNode_cstate;
      AtrioventricularNode_cstate = AtrioventricularNode_ERP;
      AtrioventricularNode_force_init_update = false;
      AtrioventricularNode_ACTpath  = false;
      AtrioventricularNode_ACTcell1  = false;
      AtrioventricularNode_ACTcell2  = false;
      AtrioventricularNode_tick = true;
     };
     do
      :: AtrioventricularNode_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (AtrioventricularNode_t > 200.0) }
           && 
          AtrioventricularNode_cstate == AtrioventricularNode_ERP)  -> 
      d_step {
            c_code {  AtrioventricularNode_t_u = 0 ; };
            AtrioventricularNode_pstate =  AtrioventricularNode_cstate ;
            AtrioventricularNode_cstate =  AtrioventricularNode_RRP ;
            AtrioventricularNode_force_init_update = false;
            AtrioventricularNode_tick = true;
      };
      do
       :: AtrioventricularNode_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (AtrioventricularNode_t <= 100.0) }  &&  (! AtrioventricularNode_ACTcell1)
     && 
    (! AtrioventricularNode_ACTcell2)  &&  AtrioventricularNode_cstate == AtrioventricularNode_RRP) ->
     if
      :: AtrioventricularNode_pstate != AtrioventricularNode_cstate || AtrioventricularNode_force_init_update ->
       c_code { AtrioventricularNode_t_init = AtrioventricularNode_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       AtrioventricularNode_t_slope =  1 ;
       AtrioventricularNode_t_u = (AtrioventricularNode_t_slope * d) + AtrioventricularNode_t ;
       
       
       
      };
      AtrioventricularNode_pstate = AtrioventricularNode_cstate;
      AtrioventricularNode_cstate = AtrioventricularNode_RRP;
      AtrioventricularNode_force_init_update = false;
      AtrioventricularNode_ACTpath  = false;
      AtrioventricularNode_ACTcell1  = false;
      AtrioventricularNode_ACTcell2  = false;
      AtrioventricularNode_tick = true;
     };
     do
      :: AtrioventricularNode_tick -> true
      :: else -> goto  RRP
     od;
     ::  (AtrioventricularNode_ACTcell1
           && 
          AtrioventricularNode_cstate == AtrioventricularNode_RRP)  -> 
      d_step {
            c_code {  AtrioventricularNode_t_u = 0 ; };
            AtrioventricularNode_pstate =  AtrioventricularNode_cstate ;
            AtrioventricularNode_cstate =  AtrioventricularNode_ST ;
            AtrioventricularNode_force_init_update = false;
            AtrioventricularNode_tick = true;
      };
      do
       :: AtrioventricularNode_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (AtrioventricularNode_t > 100.0) }
           && 
          AtrioventricularNode_cstate == AtrioventricularNode_RRP)  -> 
      d_step {
            c_code {  AtrioventricularNode_t_u = 0 ; };
            AtrioventricularNode_pstate =  AtrioventricularNode_cstate ;
            AtrioventricularNode_cstate =  AtrioventricularNode_REST ;
            AtrioventricularNode_force_init_update = false;
            AtrioventricularNode_tick = true;
      };
      do
       :: AtrioventricularNode_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double BundleOfHis_t_u, BundleOfHis_t_slope, BundleOfHis_t_init, BundleOfHis_t =  0 ;};
c_track "&BundleOfHis_t" "sizeof(double)";
c_track "&BundleOfHis_t_slope" "sizeof(double)";
c_track "&BundleOfHis_t_u" "sizeof(double)";
c_track "&BundleOfHis_t_init" "sizeof(double)";


bool BundleOfHis_ACTpath = 0 ;
bool BundleOfHis_ACTcell1 ;
mtype { BundleOfHis_REST, BundleOfHis_ST, BundleOfHis_ERP, BundleOfHis_RRP };
proctype BundleOfHis () {
REST:
   if
    :: (c_expr { (BundleOfHis_t <= 400.0) }  &&  (! BundleOfHis_ACTcell1)  &&  BundleOfHis_cstate == BundleOfHis_REST) ->
     if
      :: BundleOfHis_pstate != BundleOfHis_cstate || BundleOfHis_force_init_update ->
       c_code { BundleOfHis_t_init = BundleOfHis_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       BundleOfHis_t_slope =  1 ;
       BundleOfHis_t_u = (BundleOfHis_t_slope * d) + BundleOfHis_t ;
       
       
       
      };
      BundleOfHis_pstate = BundleOfHis_cstate;
      BundleOfHis_cstate = BundleOfHis_REST;
      BundleOfHis_force_init_update = false;
      BundleOfHis_ACTpath  = false;
      BundleOfHis_ACTcell1  = false;
      BundleOfHis_tick = true;
     };
     do
      :: BundleOfHis_tick -> true
      :: else -> goto  REST
     od;
     ::  (BundleOfHis_ACTcell1
           && 
          BundleOfHis_cstate == BundleOfHis_REST)  -> 
      d_step {
            c_code {  BundleOfHis_t_u = 0 ; };
            BundleOfHis_pstate =  BundleOfHis_cstate ;
            BundleOfHis_cstate =  BundleOfHis_ST ;
            BundleOfHis_force_init_update = false;
            BundleOfHis_tick = true;
      };
      do
       :: BundleOfHis_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (BundleOfHis_t > 400.0) }
           && 
          BundleOfHis_cstate == BundleOfHis_REST)  -> 
      d_step {
            c_code {  BundleOfHis_t_u = 0 ; };
            BundleOfHis_pstate =  BundleOfHis_cstate ;
            BundleOfHis_cstate =  BundleOfHis_ST ;
            BundleOfHis_force_init_update = false;
            BundleOfHis_tick = true;
      };
      do
       :: BundleOfHis_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (BundleOfHis_t <= 10.0) }  &&  (! BundleOfHis_ACTcell1)  &&  BundleOfHis_cstate == BundleOfHis_ST) ->
     if
      :: BundleOfHis_pstate != BundleOfHis_cstate || BundleOfHis_force_init_update ->
       c_code { BundleOfHis_t_init = BundleOfHis_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       BundleOfHis_t_slope =  1 ;
       BundleOfHis_t_u = (BundleOfHis_t_slope * d) + BundleOfHis_t ;
       
       
       
      };
      BundleOfHis_pstate = BundleOfHis_cstate;
      BundleOfHis_cstate = BundleOfHis_ST;
      BundleOfHis_force_init_update = false;
      BundleOfHis_ACTpath  = false;
      BundleOfHis_ACTcell1  = false;
      BundleOfHis_tick = true;
     };
     do
      :: BundleOfHis_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (BundleOfHis_t > 10.0) }
           && 
          BundleOfHis_cstate == BundleOfHis_ST)  -> 
      d_step {
            c_code {  BundleOfHis_t_u = 0 ; };
            BundleOfHis_ACTpath = 1;
            BundleOfHis_pstate =  BundleOfHis_cstate ;
            BundleOfHis_cstate =  BundleOfHis_ERP ;
            BundleOfHis_force_init_update = false;
            BundleOfHis_tick = true;
      };
      do
       :: BundleOfHis_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (BundleOfHis_t <= 200.0) }  &&  (! BundleOfHis_ACTcell1)  &&  BundleOfHis_cstate == BundleOfHis_ERP) ->
     if
      :: BundleOfHis_pstate != BundleOfHis_cstate || BundleOfHis_force_init_update ->
       c_code { BundleOfHis_t_init = BundleOfHis_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       BundleOfHis_t_slope =  1 ;
       BundleOfHis_t_u = (BundleOfHis_t_slope * d) + BundleOfHis_t ;
       
       
       
      };
      BundleOfHis_pstate = BundleOfHis_cstate;
      BundleOfHis_cstate = BundleOfHis_ERP;
      BundleOfHis_force_init_update = false;
      BundleOfHis_ACTpath  = false;
      BundleOfHis_ACTcell1  = false;
      BundleOfHis_tick = true;
     };
     do
      :: BundleOfHis_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (BundleOfHis_t > 200.0) }
           && 
          BundleOfHis_cstate == BundleOfHis_ERP)  -> 
      d_step {
            c_code {  BundleOfHis_t_u = 0 ; };
            BundleOfHis_pstate =  BundleOfHis_cstate ;
            BundleOfHis_cstate =  BundleOfHis_RRP ;
            BundleOfHis_force_init_update = false;
            BundleOfHis_tick = true;
      };
      do
       :: BundleOfHis_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (BundleOfHis_t <= 100.0) }  &&  (! BundleOfHis_ACTcell1)  &&  BundleOfHis_cstate == BundleOfHis_RRP) ->
     if
      :: BundleOfHis_pstate != BundleOfHis_cstate || BundleOfHis_force_init_update ->
       c_code { BundleOfHis_t_init = BundleOfHis_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       BundleOfHis_t_slope =  1 ;
       BundleOfHis_t_u = (BundleOfHis_t_slope * d) + BundleOfHis_t ;
       
       
       
      };
      BundleOfHis_pstate = BundleOfHis_cstate;
      BundleOfHis_cstate = BundleOfHis_RRP;
      BundleOfHis_force_init_update = false;
      BundleOfHis_ACTpath  = false;
      BundleOfHis_ACTcell1  = false;
      BundleOfHis_tick = true;
     };
     do
      :: BundleOfHis_tick -> true
      :: else -> goto  RRP
     od;
     ::  (BundleOfHis_ACTcell1
           && 
          BundleOfHis_cstate == BundleOfHis_RRP)  -> 
      d_step {
            c_code {  BundleOfHis_t_u = 0 ; };
            BundleOfHis_pstate =  BundleOfHis_cstate ;
            BundleOfHis_cstate =  BundleOfHis_ST ;
            BundleOfHis_force_init_update = false;
            BundleOfHis_tick = true;
      };
      do
       :: BundleOfHis_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (BundleOfHis_t > 100.0) }
           && 
          BundleOfHis_cstate == BundleOfHis_RRP)  -> 
      d_step {
            c_code {  BundleOfHis_t_u = 0 ; };
            BundleOfHis_pstate =  BundleOfHis_cstate ;
            BundleOfHis_cstate =  BundleOfHis_REST ;
            BundleOfHis_force_init_update = false;
            BundleOfHis_tick = true;
      };
      do
       :: BundleOfHis_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double BundleOfHis1_t_u, BundleOfHis1_t_slope, BundleOfHis1_t_init, BundleOfHis1_t =  0 ;};
c_track "&BundleOfHis1_t" "sizeof(double)";
c_track "&BundleOfHis1_t_slope" "sizeof(double)";
c_track "&BundleOfHis1_t_u" "sizeof(double)";
c_track "&BundleOfHis1_t_init" "sizeof(double)";


bool BundleOfHis1_ACTpath = 0 ;
bool BundleOfHis1_ACTcell1 ;
mtype { BundleOfHis1_REST, BundleOfHis1_ST, BundleOfHis1_ERP, BundleOfHis1_RRP };
proctype BundleOfHis1 () {
REST:
   if
    :: (c_expr { (BundleOfHis1_t <= 400.0) }  &&  (! BundleOfHis1_ACTcell1)  &&  BundleOfHis1_cstate == BundleOfHis1_REST) ->
     if
      :: BundleOfHis1_pstate != BundleOfHis1_cstate || BundleOfHis1_force_init_update ->
       c_code { BundleOfHis1_t_init = BundleOfHis1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       BundleOfHis1_t_slope =  1 ;
       BundleOfHis1_t_u = (BundleOfHis1_t_slope * d) + BundleOfHis1_t ;
       
       
       
      };
      BundleOfHis1_pstate = BundleOfHis1_cstate;
      BundleOfHis1_cstate = BundleOfHis1_REST;
      BundleOfHis1_force_init_update = false;
      BundleOfHis1_ACTpath  = false;
      BundleOfHis1_ACTcell1  = false;
      BundleOfHis1_tick = true;
     };
     do
      :: BundleOfHis1_tick -> true
      :: else -> goto  REST
     od;
     ::  (BundleOfHis1_ACTcell1
           && 
          BundleOfHis1_cstate == BundleOfHis1_REST)  -> 
      d_step {
            c_code {  BundleOfHis1_t_u = 0 ; };
            BundleOfHis1_pstate =  BundleOfHis1_cstate ;
            BundleOfHis1_cstate =  BundleOfHis1_ST ;
            BundleOfHis1_force_init_update = false;
            BundleOfHis1_tick = true;
      };
      do
       :: BundleOfHis1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (BundleOfHis1_t > 400.0) }
           && 
          BundleOfHis1_cstate == BundleOfHis1_REST)  -> 
      d_step {
            c_code {  BundleOfHis1_t_u = 0 ; };
            BundleOfHis1_pstate =  BundleOfHis1_cstate ;
            BundleOfHis1_cstate =  BundleOfHis1_ST ;
            BundleOfHis1_force_init_update = false;
            BundleOfHis1_tick = true;
      };
      do
       :: BundleOfHis1_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (BundleOfHis1_t <= 10.0) }  &&  (! BundleOfHis1_ACTcell1)  &&  BundleOfHis1_cstate == BundleOfHis1_ST) ->
     if
      :: BundleOfHis1_pstate != BundleOfHis1_cstate || BundleOfHis1_force_init_update ->
       c_code { BundleOfHis1_t_init = BundleOfHis1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       BundleOfHis1_t_slope =  1 ;
       BundleOfHis1_t_u = (BundleOfHis1_t_slope * d) + BundleOfHis1_t ;
       
       
       
      };
      BundleOfHis1_pstate = BundleOfHis1_cstate;
      BundleOfHis1_cstate = BundleOfHis1_ST;
      BundleOfHis1_force_init_update = false;
      BundleOfHis1_ACTpath  = false;
      BundleOfHis1_ACTcell1  = false;
      BundleOfHis1_tick = true;
     };
     do
      :: BundleOfHis1_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (BundleOfHis1_t > 10.0) }
           && 
          BundleOfHis1_cstate == BundleOfHis1_ST)  -> 
      d_step {
            c_code {  BundleOfHis1_t_u = 0 ; };
            BundleOfHis1_ACTpath = 1;
            BundleOfHis1_pstate =  BundleOfHis1_cstate ;
            BundleOfHis1_cstate =  BundleOfHis1_ERP ;
            BundleOfHis1_force_init_update = false;
            BundleOfHis1_tick = true;
      };
      do
       :: BundleOfHis1_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (BundleOfHis1_t <= 200.0) }  &&  (! BundleOfHis1_ACTcell1)  &&  BundleOfHis1_cstate == BundleOfHis1_ERP) ->
     if
      :: BundleOfHis1_pstate != BundleOfHis1_cstate || BundleOfHis1_force_init_update ->
       c_code { BundleOfHis1_t_init = BundleOfHis1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       BundleOfHis1_t_slope =  1 ;
       BundleOfHis1_t_u = (BundleOfHis1_t_slope * d) + BundleOfHis1_t ;
       
       
       
      };
      BundleOfHis1_pstate = BundleOfHis1_cstate;
      BundleOfHis1_cstate = BundleOfHis1_ERP;
      BundleOfHis1_force_init_update = false;
      BundleOfHis1_ACTpath  = false;
      BundleOfHis1_ACTcell1  = false;
      BundleOfHis1_tick = true;
     };
     do
      :: BundleOfHis1_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (BundleOfHis1_t > 200.0) }
           && 
          BundleOfHis1_cstate == BundleOfHis1_ERP)  -> 
      d_step {
            c_code {  BundleOfHis1_t_u = 0 ; };
            BundleOfHis1_pstate =  BundleOfHis1_cstate ;
            BundleOfHis1_cstate =  BundleOfHis1_RRP ;
            BundleOfHis1_force_init_update = false;
            BundleOfHis1_tick = true;
      };
      do
       :: BundleOfHis1_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (BundleOfHis1_t <= 100.0) }  &&  (! BundleOfHis1_ACTcell1)  &&  BundleOfHis1_cstate == BundleOfHis1_RRP) ->
     if
      :: BundleOfHis1_pstate != BundleOfHis1_cstate || BundleOfHis1_force_init_update ->
       c_code { BundleOfHis1_t_init = BundleOfHis1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       BundleOfHis1_t_slope =  1 ;
       BundleOfHis1_t_u = (BundleOfHis1_t_slope * d) + BundleOfHis1_t ;
       
       
       
      };
      BundleOfHis1_pstate = BundleOfHis1_cstate;
      BundleOfHis1_cstate = BundleOfHis1_RRP;
      BundleOfHis1_force_init_update = false;
      BundleOfHis1_ACTpath  = false;
      BundleOfHis1_ACTcell1  = false;
      BundleOfHis1_tick = true;
     };
     do
      :: BundleOfHis1_tick -> true
      :: else -> goto  RRP
     od;
     ::  (BundleOfHis1_ACTcell1
           && 
          BundleOfHis1_cstate == BundleOfHis1_RRP)  -> 
      d_step {
            c_code {  BundleOfHis1_t_u = 0 ; };
            BundleOfHis1_pstate =  BundleOfHis1_cstate ;
            BundleOfHis1_cstate =  BundleOfHis1_ST ;
            BundleOfHis1_force_init_update = false;
            BundleOfHis1_tick = true;
      };
      do
       :: BundleOfHis1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (BundleOfHis1_t > 100.0) }
           && 
          BundleOfHis1_cstate == BundleOfHis1_RRP)  -> 
      d_step {
            c_code {  BundleOfHis1_t_u = 0 ; };
            BundleOfHis1_pstate =  BundleOfHis1_cstate ;
            BundleOfHis1_cstate =  BundleOfHis1_REST ;
            BundleOfHis1_force_init_update = false;
            BundleOfHis1_tick = true;
      };
      do
       :: BundleOfHis1_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double BundleOfHis2_t_u, BundleOfHis2_t_slope, BundleOfHis2_t_init, BundleOfHis2_t =  0 ;};
c_track "&BundleOfHis2_t" "sizeof(double)";
c_track "&BundleOfHis2_t_slope" "sizeof(double)";
c_track "&BundleOfHis2_t_u" "sizeof(double)";
c_track "&BundleOfHis2_t_init" "sizeof(double)";


bool BundleOfHis2_ACTpath = 0 ;
bool BundleOfHis2_ACTcell1 ;
mtype { BundleOfHis2_REST, BundleOfHis2_ST, BundleOfHis2_ERP, BundleOfHis2_RRP };
proctype BundleOfHis2 () {
REST:
   if
    :: (c_expr { (BundleOfHis2_t <= 400.0) }  &&  (! BundleOfHis2_ACTcell1)  &&  BundleOfHis2_cstate == BundleOfHis2_REST) ->
     if
      :: BundleOfHis2_pstate != BundleOfHis2_cstate || BundleOfHis2_force_init_update ->
       c_code { BundleOfHis2_t_init = BundleOfHis2_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       BundleOfHis2_t_slope =  1 ;
       BundleOfHis2_t_u = (BundleOfHis2_t_slope * d) + BundleOfHis2_t ;
       
       
       
      };
      BundleOfHis2_pstate = BundleOfHis2_cstate;
      BundleOfHis2_cstate = BundleOfHis2_REST;
      BundleOfHis2_force_init_update = false;
      BundleOfHis2_ACTpath  = false;
      BundleOfHis2_ACTcell1  = false;
      BundleOfHis2_tick = true;
     };
     do
      :: BundleOfHis2_tick -> true
      :: else -> goto  REST
     od;
     ::  (BundleOfHis2_ACTcell1
           && 
          BundleOfHis2_cstate == BundleOfHis2_REST)  -> 
      d_step {
            c_code {  BundleOfHis2_t_u = 0 ; };
            BundleOfHis2_pstate =  BundleOfHis2_cstate ;
            BundleOfHis2_cstate =  BundleOfHis2_ST ;
            BundleOfHis2_force_init_update = false;
            BundleOfHis2_tick = true;
      };
      do
       :: BundleOfHis2_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (BundleOfHis2_t > 400.0) }
           && 
          BundleOfHis2_cstate == BundleOfHis2_REST)  -> 
      d_step {
            c_code {  BundleOfHis2_t_u = 0 ; };
            BundleOfHis2_pstate =  BundleOfHis2_cstate ;
            BundleOfHis2_cstate =  BundleOfHis2_ST ;
            BundleOfHis2_force_init_update = false;
            BundleOfHis2_tick = true;
      };
      do
       :: BundleOfHis2_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (BundleOfHis2_t <= 10.0) }  &&  (! BundleOfHis2_ACTcell1)  &&  BundleOfHis2_cstate == BundleOfHis2_ST) ->
     if
      :: BundleOfHis2_pstate != BundleOfHis2_cstate || BundleOfHis2_force_init_update ->
       c_code { BundleOfHis2_t_init = BundleOfHis2_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       BundleOfHis2_t_slope =  1 ;
       BundleOfHis2_t_u = (BundleOfHis2_t_slope * d) + BundleOfHis2_t ;
       
       
       
      };
      BundleOfHis2_pstate = BundleOfHis2_cstate;
      BundleOfHis2_cstate = BundleOfHis2_ST;
      BundleOfHis2_force_init_update = false;
      BundleOfHis2_ACTpath  = false;
      BundleOfHis2_ACTcell1  = false;
      BundleOfHis2_tick = true;
     };
     do
      :: BundleOfHis2_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (BundleOfHis2_t > 10.0) }
           && 
          BundleOfHis2_cstate == BundleOfHis2_ST)  -> 
      d_step {
            c_code {  BundleOfHis2_t_u = 0 ; };
            BundleOfHis2_ACTpath = 1;
            BundleOfHis2_pstate =  BundleOfHis2_cstate ;
            BundleOfHis2_cstate =  BundleOfHis2_ERP ;
            BundleOfHis2_force_init_update = false;
            BundleOfHis2_tick = true;
      };
      do
       :: BundleOfHis2_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (BundleOfHis2_t <= 200.0) }  &&  (! BundleOfHis2_ACTcell1)  &&  BundleOfHis2_cstate == BundleOfHis2_ERP) ->
     if
      :: BundleOfHis2_pstate != BundleOfHis2_cstate || BundleOfHis2_force_init_update ->
       c_code { BundleOfHis2_t_init = BundleOfHis2_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       BundleOfHis2_t_slope =  1 ;
       BundleOfHis2_t_u = (BundleOfHis2_t_slope * d) + BundleOfHis2_t ;
       
       
       
      };
      BundleOfHis2_pstate = BundleOfHis2_cstate;
      BundleOfHis2_cstate = BundleOfHis2_ERP;
      BundleOfHis2_force_init_update = false;
      BundleOfHis2_ACTpath  = false;
      BundleOfHis2_ACTcell1  = false;
      BundleOfHis2_tick = true;
     };
     do
      :: BundleOfHis2_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (BundleOfHis2_t > 200.0) }
           && 
          BundleOfHis2_cstate == BundleOfHis2_ERP)  -> 
      d_step {
            c_code {  BundleOfHis2_t_u = 0 ; };
            BundleOfHis2_pstate =  BundleOfHis2_cstate ;
            BundleOfHis2_cstate =  BundleOfHis2_RRP ;
            BundleOfHis2_force_init_update = false;
            BundleOfHis2_tick = true;
      };
      do
       :: BundleOfHis2_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (BundleOfHis2_t <= 100.0) }  &&  (! BundleOfHis2_ACTcell1)  &&  BundleOfHis2_cstate == BundleOfHis2_RRP) ->
     if
      :: BundleOfHis2_pstate != BundleOfHis2_cstate || BundleOfHis2_force_init_update ->
       c_code { BundleOfHis2_t_init = BundleOfHis2_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       BundleOfHis2_t_slope =  1 ;
       BundleOfHis2_t_u = (BundleOfHis2_t_slope * d) + BundleOfHis2_t ;
       
       
       
      };
      BundleOfHis2_pstate = BundleOfHis2_cstate;
      BundleOfHis2_cstate = BundleOfHis2_RRP;
      BundleOfHis2_force_init_update = false;
      BundleOfHis2_ACTpath  = false;
      BundleOfHis2_ACTcell1  = false;
      BundleOfHis2_tick = true;
     };
     do
      :: BundleOfHis2_tick -> true
      :: else -> goto  RRP
     od;
     ::  (BundleOfHis2_ACTcell1
           && 
          BundleOfHis2_cstate == BundleOfHis2_RRP)  -> 
      d_step {
            c_code {  BundleOfHis2_t_u = 0 ; };
            BundleOfHis2_pstate =  BundleOfHis2_cstate ;
            BundleOfHis2_cstate =  BundleOfHis2_ST ;
            BundleOfHis2_force_init_update = false;
            BundleOfHis2_tick = true;
      };
      do
       :: BundleOfHis2_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (BundleOfHis2_t > 100.0) }
           && 
          BundleOfHis2_cstate == BundleOfHis2_RRP)  -> 
      d_step {
            c_code {  BundleOfHis2_t_u = 0 ; };
            BundleOfHis2_pstate =  BundleOfHis2_cstate ;
            BundleOfHis2_cstate =  BundleOfHis2_REST ;
            BundleOfHis2_force_init_update = false;
            BundleOfHis2_tick = true;
      };
      do
       :: BundleOfHis2_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double LeftBundleBranch_t_u, LeftBundleBranch_t_slope, LeftBundleBranch_t_init, LeftBundleBranch_t =  0 ;};
c_track "&LeftBundleBranch_t" "sizeof(double)";
c_track "&LeftBundleBranch_t_slope" "sizeof(double)";
c_track "&LeftBundleBranch_t_u" "sizeof(double)";
c_track "&LeftBundleBranch_t_init" "sizeof(double)";


bool LeftBundleBranch_ACTpath = 0 ;
bool LeftBundleBranch_ACTcell1 ;
mtype { LeftBundleBranch_REST, LeftBundleBranch_ST, LeftBundleBranch_ERP, LeftBundleBranch_RRP };
proctype LeftBundleBranch () {
REST:
   if
    :: (c_expr { (LeftBundleBranch_t <= 400.0) }  &&  (! LeftBundleBranch_ACTcell1)  &&  LeftBundleBranch_cstate == LeftBundleBranch_REST) ->
     if
      :: LeftBundleBranch_pstate != LeftBundleBranch_cstate || LeftBundleBranch_force_init_update ->
       c_code { LeftBundleBranch_t_init = LeftBundleBranch_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftBundleBranch_t_slope =  1 ;
       LeftBundleBranch_t_u = (LeftBundleBranch_t_slope * d) + LeftBundleBranch_t ;
       
       
       
      };
      LeftBundleBranch_pstate = LeftBundleBranch_cstate;
      LeftBundleBranch_cstate = LeftBundleBranch_REST;
      LeftBundleBranch_force_init_update = false;
      LeftBundleBranch_ACTpath  = false;
      LeftBundleBranch_ACTcell1  = false;
      LeftBundleBranch_tick = true;
     };
     do
      :: LeftBundleBranch_tick -> true
      :: else -> goto  REST
     od;
     ::  (LeftBundleBranch_ACTcell1
           && 
          LeftBundleBranch_cstate == LeftBundleBranch_REST)  -> 
      d_step {
            c_code {  LeftBundleBranch_t_u = 0 ; };
            LeftBundleBranch_pstate =  LeftBundleBranch_cstate ;
            LeftBundleBranch_cstate =  LeftBundleBranch_ST ;
            LeftBundleBranch_force_init_update = false;
            LeftBundleBranch_tick = true;
      };
      do
       :: LeftBundleBranch_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (LeftBundleBranch_t > 400.0) }
           && 
          LeftBundleBranch_cstate == LeftBundleBranch_REST)  -> 
      d_step {
            c_code {  LeftBundleBranch_t_u = 0 ; };
            LeftBundleBranch_pstate =  LeftBundleBranch_cstate ;
            LeftBundleBranch_cstate =  LeftBundleBranch_ST ;
            LeftBundleBranch_force_init_update = false;
            LeftBundleBranch_tick = true;
      };
      do
       :: LeftBundleBranch_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (LeftBundleBranch_t <= 10.0) }  &&  (! LeftBundleBranch_ACTcell1)  &&  LeftBundleBranch_cstate == LeftBundleBranch_ST) ->
     if
      :: LeftBundleBranch_pstate != LeftBundleBranch_cstate || LeftBundleBranch_force_init_update ->
       c_code { LeftBundleBranch_t_init = LeftBundleBranch_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftBundleBranch_t_slope =  1 ;
       LeftBundleBranch_t_u = (LeftBundleBranch_t_slope * d) + LeftBundleBranch_t ;
       
       
       
      };
      LeftBundleBranch_pstate = LeftBundleBranch_cstate;
      LeftBundleBranch_cstate = LeftBundleBranch_ST;
      LeftBundleBranch_force_init_update = false;
      LeftBundleBranch_ACTpath  = false;
      LeftBundleBranch_ACTcell1  = false;
      LeftBundleBranch_tick = true;
     };
     do
      :: LeftBundleBranch_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (LeftBundleBranch_t > 10.0) }
           && 
          LeftBundleBranch_cstate == LeftBundleBranch_ST)  -> 
      d_step {
            c_code {  LeftBundleBranch_t_u = 0 ; };
            LeftBundleBranch_ACTpath = 1;
            LeftBundleBranch_pstate =  LeftBundleBranch_cstate ;
            LeftBundleBranch_cstate =  LeftBundleBranch_ERP ;
            LeftBundleBranch_force_init_update = false;
            LeftBundleBranch_tick = true;
      };
      do
       :: LeftBundleBranch_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (LeftBundleBranch_t <= 200.0) }  &&  (! LeftBundleBranch_ACTcell1)  &&  LeftBundleBranch_cstate == LeftBundleBranch_ERP) ->
     if
      :: LeftBundleBranch_pstate != LeftBundleBranch_cstate || LeftBundleBranch_force_init_update ->
       c_code { LeftBundleBranch_t_init = LeftBundleBranch_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftBundleBranch_t_slope =  1 ;
       LeftBundleBranch_t_u = (LeftBundleBranch_t_slope * d) + LeftBundleBranch_t ;
       
       
       
      };
      LeftBundleBranch_pstate = LeftBundleBranch_cstate;
      LeftBundleBranch_cstate = LeftBundleBranch_ERP;
      LeftBundleBranch_force_init_update = false;
      LeftBundleBranch_ACTpath  = false;
      LeftBundleBranch_ACTcell1  = false;
      LeftBundleBranch_tick = true;
     };
     do
      :: LeftBundleBranch_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (LeftBundleBranch_t > 200.0) }
           && 
          LeftBundleBranch_cstate == LeftBundleBranch_ERP)  -> 
      d_step {
            c_code {  LeftBundleBranch_t_u = 0 ; };
            LeftBundleBranch_pstate =  LeftBundleBranch_cstate ;
            LeftBundleBranch_cstate =  LeftBundleBranch_RRP ;
            LeftBundleBranch_force_init_update = false;
            LeftBundleBranch_tick = true;
      };
      do
       :: LeftBundleBranch_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (LeftBundleBranch_t <= 100.0) }  &&  (! LeftBundleBranch_ACTcell1)  &&  LeftBundleBranch_cstate == LeftBundleBranch_RRP) ->
     if
      :: LeftBundleBranch_pstate != LeftBundleBranch_cstate || LeftBundleBranch_force_init_update ->
       c_code { LeftBundleBranch_t_init = LeftBundleBranch_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftBundleBranch_t_slope =  1 ;
       LeftBundleBranch_t_u = (LeftBundleBranch_t_slope * d) + LeftBundleBranch_t ;
       
       
       
      };
      LeftBundleBranch_pstate = LeftBundleBranch_cstate;
      LeftBundleBranch_cstate = LeftBundleBranch_RRP;
      LeftBundleBranch_force_init_update = false;
      LeftBundleBranch_ACTpath  = false;
      LeftBundleBranch_ACTcell1  = false;
      LeftBundleBranch_tick = true;
     };
     do
      :: LeftBundleBranch_tick -> true
      :: else -> goto  RRP
     od;
     ::  (LeftBundleBranch_ACTcell1
           && 
          LeftBundleBranch_cstate == LeftBundleBranch_RRP)  -> 
      d_step {
            c_code {  LeftBundleBranch_t_u = 0 ; };
            LeftBundleBranch_pstate =  LeftBundleBranch_cstate ;
            LeftBundleBranch_cstate =  LeftBundleBranch_ST ;
            LeftBundleBranch_force_init_update = false;
            LeftBundleBranch_tick = true;
      };
      do
       :: LeftBundleBranch_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (LeftBundleBranch_t > 100.0) }
           && 
          LeftBundleBranch_cstate == LeftBundleBranch_RRP)  -> 
      d_step {
            c_code {  LeftBundleBranch_t_u = 0 ; };
            LeftBundleBranch_pstate =  LeftBundleBranch_cstate ;
            LeftBundleBranch_cstate =  LeftBundleBranch_REST ;
            LeftBundleBranch_force_init_update = false;
            LeftBundleBranch_tick = true;
      };
      do
       :: LeftBundleBranch_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double LeftBundleBranch1_t_u, LeftBundleBranch1_t_slope, LeftBundleBranch1_t_init, LeftBundleBranch1_t =  0 ;};
c_track "&LeftBundleBranch1_t" "sizeof(double)";
c_track "&LeftBundleBranch1_t_slope" "sizeof(double)";
c_track "&LeftBundleBranch1_t_u" "sizeof(double)";
c_track "&LeftBundleBranch1_t_init" "sizeof(double)";


bool LeftBundleBranch1_ACTpath = 0 ;
bool LeftBundleBranch1_ACTcell1 ;
mtype { LeftBundleBranch1_REST, LeftBundleBranch1_ST, LeftBundleBranch1_ERP, LeftBundleBranch1_RRP };
proctype LeftBundleBranch1 () {
REST:
   if
    :: (c_expr { (LeftBundleBranch1_t <= 400.0) }  &&  (! LeftBundleBranch1_ACTcell1)  &&  LeftBundleBranch1_cstate == LeftBundleBranch1_REST) ->
     if
      :: LeftBundleBranch1_pstate != LeftBundleBranch1_cstate || LeftBundleBranch1_force_init_update ->
       c_code { LeftBundleBranch1_t_init = LeftBundleBranch1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftBundleBranch1_t_slope =  1 ;
       LeftBundleBranch1_t_u = (LeftBundleBranch1_t_slope * d) + LeftBundleBranch1_t ;
       
       
       
      };
      LeftBundleBranch1_pstate = LeftBundleBranch1_cstate;
      LeftBundleBranch1_cstate = LeftBundleBranch1_REST;
      LeftBundleBranch1_force_init_update = false;
      LeftBundleBranch1_ACTpath  = false;
      LeftBundleBranch1_ACTcell1  = false;
      LeftBundleBranch1_tick = true;
     };
     do
      :: LeftBundleBranch1_tick -> true
      :: else -> goto  REST
     od;
     ::  (LeftBundleBranch1_ACTcell1
           && 
          LeftBundleBranch1_cstate == LeftBundleBranch1_REST)  -> 
      d_step {
            c_code {  LeftBundleBranch1_t_u = 0 ; };
            LeftBundleBranch1_pstate =  LeftBundleBranch1_cstate ;
            LeftBundleBranch1_cstate =  LeftBundleBranch1_ST ;
            LeftBundleBranch1_force_init_update = false;
            LeftBundleBranch1_tick = true;
      };
      do
       :: LeftBundleBranch1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (LeftBundleBranch1_t > 400.0) }
           && 
          LeftBundleBranch1_cstate == LeftBundleBranch1_REST)  -> 
      d_step {
            c_code {  LeftBundleBranch1_t_u = 0 ; };
            LeftBundleBranch1_pstate =  LeftBundleBranch1_cstate ;
            LeftBundleBranch1_cstate =  LeftBundleBranch1_ST ;
            LeftBundleBranch1_force_init_update = false;
            LeftBundleBranch1_tick = true;
      };
      do
       :: LeftBundleBranch1_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (LeftBundleBranch1_t <= 10.0) }  &&  (! LeftBundleBranch1_ACTcell1)  &&  LeftBundleBranch1_cstate == LeftBundleBranch1_ST) ->
     if
      :: LeftBundleBranch1_pstate != LeftBundleBranch1_cstate || LeftBundleBranch1_force_init_update ->
       c_code { LeftBundleBranch1_t_init = LeftBundleBranch1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftBundleBranch1_t_slope =  1 ;
       LeftBundleBranch1_t_u = (LeftBundleBranch1_t_slope * d) + LeftBundleBranch1_t ;
       
       
       
      };
      LeftBundleBranch1_pstate = LeftBundleBranch1_cstate;
      LeftBundleBranch1_cstate = LeftBundleBranch1_ST;
      LeftBundleBranch1_force_init_update = false;
      LeftBundleBranch1_ACTpath  = false;
      LeftBundleBranch1_ACTcell1  = false;
      LeftBundleBranch1_tick = true;
     };
     do
      :: LeftBundleBranch1_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (LeftBundleBranch1_t > 10.0) }
           && 
          LeftBundleBranch1_cstate == LeftBundleBranch1_ST)  -> 
      d_step {
            c_code {  LeftBundleBranch1_t_u = 0 ; };
            LeftBundleBranch1_ACTpath = 1;
            LeftBundleBranch1_pstate =  LeftBundleBranch1_cstate ;
            LeftBundleBranch1_cstate =  LeftBundleBranch1_ERP ;
            LeftBundleBranch1_force_init_update = false;
            LeftBundleBranch1_tick = true;
      };
      do
       :: LeftBundleBranch1_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (LeftBundleBranch1_t <= 200.0) }  &&  (! LeftBundleBranch1_ACTcell1)  &&  LeftBundleBranch1_cstate == LeftBundleBranch1_ERP) ->
     if
      :: LeftBundleBranch1_pstate != LeftBundleBranch1_cstate || LeftBundleBranch1_force_init_update ->
       c_code { LeftBundleBranch1_t_init = LeftBundleBranch1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftBundleBranch1_t_slope =  1 ;
       LeftBundleBranch1_t_u = (LeftBundleBranch1_t_slope * d) + LeftBundleBranch1_t ;
       
       
       
      };
      LeftBundleBranch1_pstate = LeftBundleBranch1_cstate;
      LeftBundleBranch1_cstate = LeftBundleBranch1_ERP;
      LeftBundleBranch1_force_init_update = false;
      LeftBundleBranch1_ACTpath  = false;
      LeftBundleBranch1_ACTcell1  = false;
      LeftBundleBranch1_tick = true;
     };
     do
      :: LeftBundleBranch1_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (LeftBundleBranch1_t > 200.0) }
           && 
          LeftBundleBranch1_cstate == LeftBundleBranch1_ERP)  -> 
      d_step {
            c_code {  LeftBundleBranch1_t_u = 0 ; };
            LeftBundleBranch1_pstate =  LeftBundleBranch1_cstate ;
            LeftBundleBranch1_cstate =  LeftBundleBranch1_RRP ;
            LeftBundleBranch1_force_init_update = false;
            LeftBundleBranch1_tick = true;
      };
      do
       :: LeftBundleBranch1_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (LeftBundleBranch1_t <= 100.0) }  &&  (! LeftBundleBranch1_ACTcell1)  &&  LeftBundleBranch1_cstate == LeftBundleBranch1_RRP) ->
     if
      :: LeftBundleBranch1_pstate != LeftBundleBranch1_cstate || LeftBundleBranch1_force_init_update ->
       c_code { LeftBundleBranch1_t_init = LeftBundleBranch1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftBundleBranch1_t_slope =  1 ;
       LeftBundleBranch1_t_u = (LeftBundleBranch1_t_slope * d) + LeftBundleBranch1_t ;
       
       
       
      };
      LeftBundleBranch1_pstate = LeftBundleBranch1_cstate;
      LeftBundleBranch1_cstate = LeftBundleBranch1_RRP;
      LeftBundleBranch1_force_init_update = false;
      LeftBundleBranch1_ACTpath  = false;
      LeftBundleBranch1_ACTcell1  = false;
      LeftBundleBranch1_tick = true;
     };
     do
      :: LeftBundleBranch1_tick -> true
      :: else -> goto  RRP
     od;
     ::  (LeftBundleBranch1_ACTcell1
           && 
          LeftBundleBranch1_cstate == LeftBundleBranch1_RRP)  -> 
      d_step {
            c_code {  LeftBundleBranch1_t_u = 0 ; };
            LeftBundleBranch1_pstate =  LeftBundleBranch1_cstate ;
            LeftBundleBranch1_cstate =  LeftBundleBranch1_ST ;
            LeftBundleBranch1_force_init_update = false;
            LeftBundleBranch1_tick = true;
      };
      do
       :: LeftBundleBranch1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (LeftBundleBranch1_t > 100.0) }
           && 
          LeftBundleBranch1_cstate == LeftBundleBranch1_RRP)  -> 
      d_step {
            c_code {  LeftBundleBranch1_t_u = 0 ; };
            LeftBundleBranch1_pstate =  LeftBundleBranch1_cstate ;
            LeftBundleBranch1_cstate =  LeftBundleBranch1_REST ;
            LeftBundleBranch1_force_init_update = false;
            LeftBundleBranch1_tick = true;
      };
      do
       :: LeftBundleBranch1_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double LeftVentricularApex_t_u, LeftVentricularApex_t_slope, LeftVentricularApex_t_init, LeftVentricularApex_t =  0 ;};
c_track "&LeftVentricularApex_t" "sizeof(double)";
c_track "&LeftVentricularApex_t_slope" "sizeof(double)";
c_track "&LeftVentricularApex_t_u" "sizeof(double)";
c_track "&LeftVentricularApex_t_init" "sizeof(double)";


bool LeftVentricularApex_ACTpath = 0 ;
bool LeftVentricularApex_ACTcell1 ;
mtype { LeftVentricularApex_REST, LeftVentricularApex_ST, LeftVentricularApex_ERP, LeftVentricularApex_RRP };
proctype LeftVentricularApex () {
REST:
   if
    :: (c_expr { (LeftVentricularApex_t <= 400.0) }  &&  (! LeftVentricularApex_ACTcell1)  &&  LeftVentricularApex_cstate == LeftVentricularApex_REST) ->
     if
      :: LeftVentricularApex_pstate != LeftVentricularApex_cstate || LeftVentricularApex_force_init_update ->
       c_code { LeftVentricularApex_t_init = LeftVentricularApex_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricularApex_t_slope =  1 ;
       LeftVentricularApex_t_u = (LeftVentricularApex_t_slope * d) + LeftVentricularApex_t ;
       
       
       
      };
      LeftVentricularApex_pstate = LeftVentricularApex_cstate;
      LeftVentricularApex_cstate = LeftVentricularApex_REST;
      LeftVentricularApex_force_init_update = false;
      LeftVentricularApex_ACTpath  = false;
      LeftVentricularApex_ACTcell1  = false;
      LeftVentricularApex_tick = true;
     };
     do
      :: LeftVentricularApex_tick -> true
      :: else -> goto  REST
     od;
     ::  (LeftVentricularApex_ACTcell1
           && 
          LeftVentricularApex_cstate == LeftVentricularApex_REST)  -> 
      d_step {
            c_code {  LeftVentricularApex_t_u = 0 ; };
            LeftVentricularApex_pstate =  LeftVentricularApex_cstate ;
            LeftVentricularApex_cstate =  LeftVentricularApex_ST ;
            LeftVentricularApex_force_init_update = false;
            LeftVentricularApex_tick = true;
      };
      do
       :: LeftVentricularApex_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (LeftVentricularApex_t > 400.0) }
           && 
          LeftVentricularApex_cstate == LeftVentricularApex_REST)  -> 
      d_step {
            c_code {  LeftVentricularApex_t_u = 0 ; };
            LeftVentricularApex_pstate =  LeftVentricularApex_cstate ;
            LeftVentricularApex_cstate =  LeftVentricularApex_ST ;
            LeftVentricularApex_force_init_update = false;
            LeftVentricularApex_tick = true;
      };
      do
       :: LeftVentricularApex_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (LeftVentricularApex_t <= 10.0) }  &&  (! LeftVentricularApex_ACTcell1)  &&  LeftVentricularApex_cstate == LeftVentricularApex_ST) ->
     if
      :: LeftVentricularApex_pstate != LeftVentricularApex_cstate || LeftVentricularApex_force_init_update ->
       c_code { LeftVentricularApex_t_init = LeftVentricularApex_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricularApex_t_slope =  1 ;
       LeftVentricularApex_t_u = (LeftVentricularApex_t_slope * d) + LeftVentricularApex_t ;
       
       
       
      };
      LeftVentricularApex_pstate = LeftVentricularApex_cstate;
      LeftVentricularApex_cstate = LeftVentricularApex_ST;
      LeftVentricularApex_force_init_update = false;
      LeftVentricularApex_ACTpath  = false;
      LeftVentricularApex_ACTcell1  = false;
      LeftVentricularApex_tick = true;
     };
     do
      :: LeftVentricularApex_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (LeftVentricularApex_t > 10.0) }
           && 
          LeftVentricularApex_cstate == LeftVentricularApex_ST)  -> 
      d_step {
            c_code {  LeftVentricularApex_t_u = 0 ; };
            LeftVentricularApex_ACTpath = 1;
            LeftVentricularApex_pstate =  LeftVentricularApex_cstate ;
            LeftVentricularApex_cstate =  LeftVentricularApex_ERP ;
            LeftVentricularApex_force_init_update = false;
            LeftVentricularApex_tick = true;
      };
      do
       :: LeftVentricularApex_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (LeftVentricularApex_t <= 200.0) }  &&  (! LeftVentricularApex_ACTcell1)  &&  LeftVentricularApex_cstate == LeftVentricularApex_ERP) ->
     if
      :: LeftVentricularApex_pstate != LeftVentricularApex_cstate || LeftVentricularApex_force_init_update ->
       c_code { LeftVentricularApex_t_init = LeftVentricularApex_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricularApex_t_slope =  1 ;
       LeftVentricularApex_t_u = (LeftVentricularApex_t_slope * d) + LeftVentricularApex_t ;
       
       
       
      };
      LeftVentricularApex_pstate = LeftVentricularApex_cstate;
      LeftVentricularApex_cstate = LeftVentricularApex_ERP;
      LeftVentricularApex_force_init_update = false;
      LeftVentricularApex_ACTpath  = false;
      LeftVentricularApex_ACTcell1  = false;
      LeftVentricularApex_tick = true;
     };
     do
      :: LeftVentricularApex_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (LeftVentricularApex_t > 200.0) }
           && 
          LeftVentricularApex_cstate == LeftVentricularApex_ERP)  -> 
      d_step {
            c_code {  LeftVentricularApex_t_u = 0 ; };
            LeftVentricularApex_pstate =  LeftVentricularApex_cstate ;
            LeftVentricularApex_cstate =  LeftVentricularApex_RRP ;
            LeftVentricularApex_force_init_update = false;
            LeftVentricularApex_tick = true;
      };
      do
       :: LeftVentricularApex_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (LeftVentricularApex_t <= 100.0) }  &&  (! LeftVentricularApex_ACTcell1)  &&  LeftVentricularApex_cstate == LeftVentricularApex_RRP) ->
     if
      :: LeftVentricularApex_pstate != LeftVentricularApex_cstate || LeftVentricularApex_force_init_update ->
       c_code { LeftVentricularApex_t_init = LeftVentricularApex_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricularApex_t_slope =  1 ;
       LeftVentricularApex_t_u = (LeftVentricularApex_t_slope * d) + LeftVentricularApex_t ;
       
       
       
      };
      LeftVentricularApex_pstate = LeftVentricularApex_cstate;
      LeftVentricularApex_cstate = LeftVentricularApex_RRP;
      LeftVentricularApex_force_init_update = false;
      LeftVentricularApex_ACTpath  = false;
      LeftVentricularApex_ACTcell1  = false;
      LeftVentricularApex_tick = true;
     };
     do
      :: LeftVentricularApex_tick -> true
      :: else -> goto  RRP
     od;
     ::  (LeftVentricularApex_ACTcell1
           && 
          LeftVentricularApex_cstate == LeftVentricularApex_RRP)  -> 
      d_step {
            c_code {  LeftVentricularApex_t_u = 0 ; };
            LeftVentricularApex_pstate =  LeftVentricularApex_cstate ;
            LeftVentricularApex_cstate =  LeftVentricularApex_ST ;
            LeftVentricularApex_force_init_update = false;
            LeftVentricularApex_tick = true;
      };
      do
       :: LeftVentricularApex_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (LeftVentricularApex_t > 100.0) }
           && 
          LeftVentricularApex_cstate == LeftVentricularApex_RRP)  -> 
      d_step {
            c_code {  LeftVentricularApex_t_u = 0 ; };
            LeftVentricularApex_pstate =  LeftVentricularApex_cstate ;
            LeftVentricularApex_cstate =  LeftVentricularApex_REST ;
            LeftVentricularApex_force_init_update = false;
            LeftVentricularApex_tick = true;
      };
      do
       :: LeftVentricularApex_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double LeftVentricle_t_u, LeftVentricle_t_slope, LeftVentricle_t_init, LeftVentricle_t =  0 ;};
c_track "&LeftVentricle_t" "sizeof(double)";
c_track "&LeftVentricle_t_slope" "sizeof(double)";
c_track "&LeftVentricle_t_u" "sizeof(double)";
c_track "&LeftVentricle_t_init" "sizeof(double)";


bool LeftVentricle_ACTpath = 0 ;
bool LeftVentricle_ACTcell1 ;
mtype { LeftVentricle_REST, LeftVentricle_ST, LeftVentricle_ERP, LeftVentricle_RRP };
proctype LeftVentricle () {
REST:
   if
    :: (c_expr { (LeftVentricle_t <= 400.0) }  &&  (! LeftVentricle_ACTcell1)  &&  LeftVentricle_cstate == LeftVentricle_REST) ->
     if
      :: LeftVentricle_pstate != LeftVentricle_cstate || LeftVentricle_force_init_update ->
       c_code { LeftVentricle_t_init = LeftVentricle_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricle_t_slope =  1 ;
       LeftVentricle_t_u = (LeftVentricle_t_slope * d) + LeftVentricle_t ;
       
       
       
      };
      LeftVentricle_pstate = LeftVentricle_cstate;
      LeftVentricle_cstate = LeftVentricle_REST;
      LeftVentricle_force_init_update = false;
      LeftVentricle_ACTpath  = false;
      LeftVentricle_ACTcell1  = false;
      LeftVentricle_tick = true;
     };
     do
      :: LeftVentricle_tick -> true
      :: else -> goto  REST
     od;
     ::  (LeftVentricle_ACTcell1
           && 
          LeftVentricle_cstate == LeftVentricle_REST)  -> 
      d_step {
            c_code {  LeftVentricle_t_u = 0 ; };
            LeftVentricle_pstate =  LeftVentricle_cstate ;
            LeftVentricle_cstate =  LeftVentricle_ST ;
            LeftVentricle_force_init_update = false;
            LeftVentricle_tick = true;
      };
      do
       :: LeftVentricle_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (LeftVentricle_t > 400.0) }
           && 
          LeftVentricle_cstate == LeftVentricle_REST)  -> 
      d_step {
            c_code {  LeftVentricle_t_u = 0 ; };
            LeftVentricle_pstate =  LeftVentricle_cstate ;
            LeftVentricle_cstate =  LeftVentricle_ST ;
            LeftVentricle_force_init_update = false;
            LeftVentricle_tick = true;
      };
      do
       :: LeftVentricle_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (LeftVentricle_t <= 10.0) }  &&  (! LeftVentricle_ACTcell1)  &&  LeftVentricle_cstate == LeftVentricle_ST) ->
     if
      :: LeftVentricle_pstate != LeftVentricle_cstate || LeftVentricle_force_init_update ->
       c_code { LeftVentricle_t_init = LeftVentricle_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricle_t_slope =  1 ;
       LeftVentricle_t_u = (LeftVentricle_t_slope * d) + LeftVentricle_t ;
       
       
       
      };
      LeftVentricle_pstate = LeftVentricle_cstate;
      LeftVentricle_cstate = LeftVentricle_ST;
      LeftVentricle_force_init_update = false;
      LeftVentricle_ACTpath  = false;
      LeftVentricle_ACTcell1  = false;
      LeftVentricle_tick = true;
     };
     do
      :: LeftVentricle_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (LeftVentricle_t > 10.0) }
           && 
          LeftVentricle_cstate == LeftVentricle_ST)  -> 
      d_step {
            c_code {  LeftVentricle_t_u = 0 ; };
            LeftVentricle_ACTpath = 1;
            LeftVentricle_pstate =  LeftVentricle_cstate ;
            LeftVentricle_cstate =  LeftVentricle_ERP ;
            LeftVentricle_force_init_update = false;
            LeftVentricle_tick = true;
      };
      do
       :: LeftVentricle_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (LeftVentricle_t <= 200.0) }  &&  (! LeftVentricle_ACTcell1)  &&  LeftVentricle_cstate == LeftVentricle_ERP) ->
     if
      :: LeftVentricle_pstate != LeftVentricle_cstate || LeftVentricle_force_init_update ->
       c_code { LeftVentricle_t_init = LeftVentricle_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricle_t_slope =  1 ;
       LeftVentricle_t_u = (LeftVentricle_t_slope * d) + LeftVentricle_t ;
       
       
       
      };
      LeftVentricle_pstate = LeftVentricle_cstate;
      LeftVentricle_cstate = LeftVentricle_ERP;
      LeftVentricle_force_init_update = false;
      LeftVentricle_ACTpath  = false;
      LeftVentricle_ACTcell1  = false;
      LeftVentricle_tick = true;
     };
     do
      :: LeftVentricle_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (LeftVentricle_t > 200.0) }
           && 
          LeftVentricle_cstate == LeftVentricle_ERP)  -> 
      d_step {
            c_code {  LeftVentricle_t_u = 0 ; };
            LeftVentricle_pstate =  LeftVentricle_cstate ;
            LeftVentricle_cstate =  LeftVentricle_RRP ;
            LeftVentricle_force_init_update = false;
            LeftVentricle_tick = true;
      };
      do
       :: LeftVentricle_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (LeftVentricle_t <= 100.0) }  &&  (! LeftVentricle_ACTcell1)  &&  LeftVentricle_cstate == LeftVentricle_RRP) ->
     if
      :: LeftVentricle_pstate != LeftVentricle_cstate || LeftVentricle_force_init_update ->
       c_code { LeftVentricle_t_init = LeftVentricle_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricle_t_slope =  1 ;
       LeftVentricle_t_u = (LeftVentricle_t_slope * d) + LeftVentricle_t ;
       
       
       
      };
      LeftVentricle_pstate = LeftVentricle_cstate;
      LeftVentricle_cstate = LeftVentricle_RRP;
      LeftVentricle_force_init_update = false;
      LeftVentricle_ACTpath  = false;
      LeftVentricle_ACTcell1  = false;
      LeftVentricle_tick = true;
     };
     do
      :: LeftVentricle_tick -> true
      :: else -> goto  RRP
     od;
     ::  (LeftVentricle_ACTcell1
           && 
          LeftVentricle_cstate == LeftVentricle_RRP)  -> 
      d_step {
            c_code {  LeftVentricle_t_u = 0 ; };
            LeftVentricle_pstate =  LeftVentricle_cstate ;
            LeftVentricle_cstate =  LeftVentricle_ST ;
            LeftVentricle_force_init_update = false;
            LeftVentricle_tick = true;
      };
      do
       :: LeftVentricle_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (LeftVentricle_t > 100.0) }
           && 
          LeftVentricle_cstate == LeftVentricle_RRP)  -> 
      d_step {
            c_code {  LeftVentricle_t_u = 0 ; };
            LeftVentricle_pstate =  LeftVentricle_cstate ;
            LeftVentricle_cstate =  LeftVentricle_REST ;
            LeftVentricle_force_init_update = false;
            LeftVentricle_tick = true;
      };
      do
       :: LeftVentricle_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double LeftVentricle1_t_u, LeftVentricle1_t_slope, LeftVentricle1_t_init, LeftVentricle1_t =  0 ;};
c_track "&LeftVentricle1_t" "sizeof(double)";
c_track "&LeftVentricle1_t_slope" "sizeof(double)";
c_track "&LeftVentricle1_t_u" "sizeof(double)";
c_track "&LeftVentricle1_t_init" "sizeof(double)";


bool LeftVentricle1_ACTpath = 0 ;
bool LeftVentricle1_ACTcell1 ;
mtype { LeftVentricle1_REST, LeftVentricle1_ST, LeftVentricle1_ERP, LeftVentricle1_RRP };
proctype LeftVentricle1 () {
REST:
   if
    :: (c_expr { (LeftVentricle1_t <= 400.0) }  &&  (! LeftVentricle1_ACTcell1)  &&  LeftVentricle1_cstate == LeftVentricle1_REST) ->
     if
      :: LeftVentricle1_pstate != LeftVentricle1_cstate || LeftVentricle1_force_init_update ->
       c_code { LeftVentricle1_t_init = LeftVentricle1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricle1_t_slope =  1 ;
       LeftVentricle1_t_u = (LeftVentricle1_t_slope * d) + LeftVentricle1_t ;
       
       
       
      };
      LeftVentricle1_pstate = LeftVentricle1_cstate;
      LeftVentricle1_cstate = LeftVentricle1_REST;
      LeftVentricle1_force_init_update = false;
      LeftVentricle1_ACTpath  = false;
      LeftVentricle1_ACTcell1  = false;
      LeftVentricle1_tick = true;
     };
     do
      :: LeftVentricle1_tick -> true
      :: else -> goto  REST
     od;
     ::  (LeftVentricle1_ACTcell1
           && 
          LeftVentricle1_cstate == LeftVentricle1_REST)  -> 
      d_step {
            c_code {  LeftVentricle1_t_u = 0 ; };
            LeftVentricle1_pstate =  LeftVentricle1_cstate ;
            LeftVentricle1_cstate =  LeftVentricle1_ST ;
            LeftVentricle1_force_init_update = false;
            LeftVentricle1_tick = true;
      };
      do
       :: LeftVentricle1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (LeftVentricle1_t > 400.0) }
           && 
          LeftVentricle1_cstate == LeftVentricle1_REST)  -> 
      d_step {
            c_code {  LeftVentricle1_t_u = 0 ; };
            LeftVentricle1_pstate =  LeftVentricle1_cstate ;
            LeftVentricle1_cstate =  LeftVentricle1_ST ;
            LeftVentricle1_force_init_update = false;
            LeftVentricle1_tick = true;
      };
      do
       :: LeftVentricle1_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (LeftVentricle1_t <= 10.0) }  &&  (! LeftVentricle1_ACTcell1)  &&  LeftVentricle1_cstate == LeftVentricle1_ST) ->
     if
      :: LeftVentricle1_pstate != LeftVentricle1_cstate || LeftVentricle1_force_init_update ->
       c_code { LeftVentricle1_t_init = LeftVentricle1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricle1_t_slope =  1 ;
       LeftVentricle1_t_u = (LeftVentricle1_t_slope * d) + LeftVentricle1_t ;
       
       
       
      };
      LeftVentricle1_pstate = LeftVentricle1_cstate;
      LeftVentricle1_cstate = LeftVentricle1_ST;
      LeftVentricle1_force_init_update = false;
      LeftVentricle1_ACTpath  = false;
      LeftVentricle1_ACTcell1  = false;
      LeftVentricle1_tick = true;
     };
     do
      :: LeftVentricle1_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (LeftVentricle1_t > 10.0) }
           && 
          LeftVentricle1_cstate == LeftVentricle1_ST)  -> 
      d_step {
            c_code {  LeftVentricle1_t_u = 0 ; };
            LeftVentricle1_ACTpath = 1;
            LeftVentricle1_pstate =  LeftVentricle1_cstate ;
            LeftVentricle1_cstate =  LeftVentricle1_ERP ;
            LeftVentricle1_force_init_update = false;
            LeftVentricle1_tick = true;
      };
      do
       :: LeftVentricle1_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (LeftVentricle1_t <= 200.0) }  &&  (! LeftVentricle1_ACTcell1)  &&  LeftVentricle1_cstate == LeftVentricle1_ERP) ->
     if
      :: LeftVentricle1_pstate != LeftVentricle1_cstate || LeftVentricle1_force_init_update ->
       c_code { LeftVentricle1_t_init = LeftVentricle1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricle1_t_slope =  1 ;
       LeftVentricle1_t_u = (LeftVentricle1_t_slope * d) + LeftVentricle1_t ;
       
       
       
      };
      LeftVentricle1_pstate = LeftVentricle1_cstate;
      LeftVentricle1_cstate = LeftVentricle1_ERP;
      LeftVentricle1_force_init_update = false;
      LeftVentricle1_ACTpath  = false;
      LeftVentricle1_ACTcell1  = false;
      LeftVentricle1_tick = true;
     };
     do
      :: LeftVentricle1_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (LeftVentricle1_t > 200.0) }
           && 
          LeftVentricle1_cstate == LeftVentricle1_ERP)  -> 
      d_step {
            c_code {  LeftVentricle1_t_u = 0 ; };
            LeftVentricle1_pstate =  LeftVentricle1_cstate ;
            LeftVentricle1_cstate =  LeftVentricle1_RRP ;
            LeftVentricle1_force_init_update = false;
            LeftVentricle1_tick = true;
      };
      do
       :: LeftVentricle1_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (LeftVentricle1_t <= 100.0) }  &&  (! LeftVentricle1_ACTcell1)  &&  LeftVentricle1_cstate == LeftVentricle1_RRP) ->
     if
      :: LeftVentricle1_pstate != LeftVentricle1_cstate || LeftVentricle1_force_init_update ->
       c_code { LeftVentricle1_t_init = LeftVentricle1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricle1_t_slope =  1 ;
       LeftVentricle1_t_u = (LeftVentricle1_t_slope * d) + LeftVentricle1_t ;
       
       
       
      };
      LeftVentricle1_pstate = LeftVentricle1_cstate;
      LeftVentricle1_cstate = LeftVentricle1_RRP;
      LeftVentricle1_force_init_update = false;
      LeftVentricle1_ACTpath  = false;
      LeftVentricle1_ACTcell1  = false;
      LeftVentricle1_tick = true;
     };
     do
      :: LeftVentricle1_tick -> true
      :: else -> goto  RRP
     od;
     ::  (LeftVentricle1_ACTcell1
           && 
          LeftVentricle1_cstate == LeftVentricle1_RRP)  -> 
      d_step {
            c_code {  LeftVentricle1_t_u = 0 ; };
            LeftVentricle1_pstate =  LeftVentricle1_cstate ;
            LeftVentricle1_cstate =  LeftVentricle1_ST ;
            LeftVentricle1_force_init_update = false;
            LeftVentricle1_tick = true;
      };
      do
       :: LeftVentricle1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (LeftVentricle1_t > 100.0) }
           && 
          LeftVentricle1_cstate == LeftVentricle1_RRP)  -> 
      d_step {
            c_code {  LeftVentricle1_t_u = 0 ; };
            LeftVentricle1_pstate =  LeftVentricle1_cstate ;
            LeftVentricle1_cstate =  LeftVentricle1_REST ;
            LeftVentricle1_force_init_update = false;
            LeftVentricle1_tick = true;
      };
      do
       :: LeftVentricle1_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double LeftVentricularSeptum_t_u, LeftVentricularSeptum_t_slope, LeftVentricularSeptum_t_init, LeftVentricularSeptum_t =  0 ;};
c_track "&LeftVentricularSeptum_t" "sizeof(double)";
c_track "&LeftVentricularSeptum_t_slope" "sizeof(double)";
c_track "&LeftVentricularSeptum_t_u" "sizeof(double)";
c_track "&LeftVentricularSeptum_t_init" "sizeof(double)";


bool LeftVentricularSeptum_ACTpath = 0 ;
bool LeftVentricularSeptum_ACTcell1 ;
mtype { LeftVentricularSeptum_REST, LeftVentricularSeptum_ST, LeftVentricularSeptum_ERP, LeftVentricularSeptum_RRP };
proctype LeftVentricularSeptum () {
REST:
   if
    :: (c_expr { (LeftVentricularSeptum_t <= 400.0) }  &&  (! LeftVentricularSeptum_ACTcell1)  &&  LeftVentricularSeptum_cstate == LeftVentricularSeptum_REST) ->
     if
      :: LeftVentricularSeptum_pstate != LeftVentricularSeptum_cstate || LeftVentricularSeptum_force_init_update ->
       c_code { LeftVentricularSeptum_t_init = LeftVentricularSeptum_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricularSeptum_t_slope =  1 ;
       LeftVentricularSeptum_t_u = (LeftVentricularSeptum_t_slope * d) + LeftVentricularSeptum_t ;
       
       
       
      };
      LeftVentricularSeptum_pstate = LeftVentricularSeptum_cstate;
      LeftVentricularSeptum_cstate = LeftVentricularSeptum_REST;
      LeftVentricularSeptum_force_init_update = false;
      LeftVentricularSeptum_ACTpath  = false;
      LeftVentricularSeptum_ACTcell1  = false;
      LeftVentricularSeptum_tick = true;
     };
     do
      :: LeftVentricularSeptum_tick -> true
      :: else -> goto  REST
     od;
     ::  (LeftVentricularSeptum_ACTcell1
           && 
          LeftVentricularSeptum_cstate == LeftVentricularSeptum_REST)  -> 
      d_step {
            c_code {  LeftVentricularSeptum_t_u = 0 ; };
            LeftVentricularSeptum_pstate =  LeftVentricularSeptum_cstate ;
            LeftVentricularSeptum_cstate =  LeftVentricularSeptum_ST ;
            LeftVentricularSeptum_force_init_update = false;
            LeftVentricularSeptum_tick = true;
      };
      do
       :: LeftVentricularSeptum_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (LeftVentricularSeptum_t > 400.0) }
           && 
          LeftVentricularSeptum_cstate == LeftVentricularSeptum_REST)  -> 
      d_step {
            c_code {  LeftVentricularSeptum_t_u = 0 ; };
            LeftVentricularSeptum_pstate =  LeftVentricularSeptum_cstate ;
            LeftVentricularSeptum_cstate =  LeftVentricularSeptum_ST ;
            LeftVentricularSeptum_force_init_update = false;
            LeftVentricularSeptum_tick = true;
      };
      do
       :: LeftVentricularSeptum_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (LeftVentricularSeptum_t <= 10.0) }  &&  (! LeftVentricularSeptum_ACTcell1)  &&  LeftVentricularSeptum_cstate == LeftVentricularSeptum_ST) ->
     if
      :: LeftVentricularSeptum_pstate != LeftVentricularSeptum_cstate || LeftVentricularSeptum_force_init_update ->
       c_code { LeftVentricularSeptum_t_init = LeftVentricularSeptum_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricularSeptum_t_slope =  1 ;
       LeftVentricularSeptum_t_u = (LeftVentricularSeptum_t_slope * d) + LeftVentricularSeptum_t ;
       
       
       
      };
      LeftVentricularSeptum_pstate = LeftVentricularSeptum_cstate;
      LeftVentricularSeptum_cstate = LeftVentricularSeptum_ST;
      LeftVentricularSeptum_force_init_update = false;
      LeftVentricularSeptum_ACTpath  = false;
      LeftVentricularSeptum_ACTcell1  = false;
      LeftVentricularSeptum_tick = true;
     };
     do
      :: LeftVentricularSeptum_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (LeftVentricularSeptum_t > 10.0) }
           && 
          LeftVentricularSeptum_cstate == LeftVentricularSeptum_ST)  -> 
      d_step {
            c_code {  LeftVentricularSeptum_t_u = 0 ; };
            LeftVentricularSeptum_ACTpath = 1;
            LeftVentricularSeptum_pstate =  LeftVentricularSeptum_cstate ;
            LeftVentricularSeptum_cstate =  LeftVentricularSeptum_ERP ;
            LeftVentricularSeptum_force_init_update = false;
            LeftVentricularSeptum_tick = true;
      };
      do
       :: LeftVentricularSeptum_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (LeftVentricularSeptum_t <= 200.0) }  &&  (! LeftVentricularSeptum_ACTcell1)  &&  LeftVentricularSeptum_cstate == LeftVentricularSeptum_ERP) ->
     if
      :: LeftVentricularSeptum_pstate != LeftVentricularSeptum_cstate || LeftVentricularSeptum_force_init_update ->
       c_code { LeftVentricularSeptum_t_init = LeftVentricularSeptum_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricularSeptum_t_slope =  1 ;
       LeftVentricularSeptum_t_u = (LeftVentricularSeptum_t_slope * d) + LeftVentricularSeptum_t ;
       
       
       
      };
      LeftVentricularSeptum_pstate = LeftVentricularSeptum_cstate;
      LeftVentricularSeptum_cstate = LeftVentricularSeptum_ERP;
      LeftVentricularSeptum_force_init_update = false;
      LeftVentricularSeptum_ACTpath  = false;
      LeftVentricularSeptum_ACTcell1  = false;
      LeftVentricularSeptum_tick = true;
     };
     do
      :: LeftVentricularSeptum_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (LeftVentricularSeptum_t > 200.0) }
           && 
          LeftVentricularSeptum_cstate == LeftVentricularSeptum_ERP)  -> 
      d_step {
            c_code {  LeftVentricularSeptum_t_u = 0 ; };
            LeftVentricularSeptum_pstate =  LeftVentricularSeptum_cstate ;
            LeftVentricularSeptum_cstate =  LeftVentricularSeptum_RRP ;
            LeftVentricularSeptum_force_init_update = false;
            LeftVentricularSeptum_tick = true;
      };
      do
       :: LeftVentricularSeptum_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (LeftVentricularSeptum_t <= 100.0) }  &&  (! LeftVentricularSeptum_ACTcell1)  &&  LeftVentricularSeptum_cstate == LeftVentricularSeptum_RRP) ->
     if
      :: LeftVentricularSeptum_pstate != LeftVentricularSeptum_cstate || LeftVentricularSeptum_force_init_update ->
       c_code { LeftVentricularSeptum_t_init = LeftVentricularSeptum_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricularSeptum_t_slope =  1 ;
       LeftVentricularSeptum_t_u = (LeftVentricularSeptum_t_slope * d) + LeftVentricularSeptum_t ;
       
       
       
      };
      LeftVentricularSeptum_pstate = LeftVentricularSeptum_cstate;
      LeftVentricularSeptum_cstate = LeftVentricularSeptum_RRP;
      LeftVentricularSeptum_force_init_update = false;
      LeftVentricularSeptum_ACTpath  = false;
      LeftVentricularSeptum_ACTcell1  = false;
      LeftVentricularSeptum_tick = true;
     };
     do
      :: LeftVentricularSeptum_tick -> true
      :: else -> goto  RRP
     od;
     ::  (LeftVentricularSeptum_ACTcell1
           && 
          LeftVentricularSeptum_cstate == LeftVentricularSeptum_RRP)  -> 
      d_step {
            c_code {  LeftVentricularSeptum_t_u = 0 ; };
            LeftVentricularSeptum_pstate =  LeftVentricularSeptum_cstate ;
            LeftVentricularSeptum_cstate =  LeftVentricularSeptum_ST ;
            LeftVentricularSeptum_force_init_update = false;
            LeftVentricularSeptum_tick = true;
      };
      do
       :: LeftVentricularSeptum_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (LeftVentricularSeptum_t > 100.0) }
           && 
          LeftVentricularSeptum_cstate == LeftVentricularSeptum_RRP)  -> 
      d_step {
            c_code {  LeftVentricularSeptum_t_u = 0 ; };
            LeftVentricularSeptum_pstate =  LeftVentricularSeptum_cstate ;
            LeftVentricularSeptum_cstate =  LeftVentricularSeptum_REST ;
            LeftVentricularSeptum_force_init_update = false;
            LeftVentricularSeptum_tick = true;
      };
      do
       :: LeftVentricularSeptum_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double LeftVentricularSeptum1_t_u, LeftVentricularSeptum1_t_slope, LeftVentricularSeptum1_t_init, LeftVentricularSeptum1_t =  0 ;};
c_track "&LeftVentricularSeptum1_t" "sizeof(double)";
c_track "&LeftVentricularSeptum1_t_slope" "sizeof(double)";
c_track "&LeftVentricularSeptum1_t_u" "sizeof(double)";
c_track "&LeftVentricularSeptum1_t_init" "sizeof(double)";


bool LeftVentricularSeptum1_ACTpath = 0 ;
bool LeftVentricularSeptum1_ACTcell1 ;
mtype { LeftVentricularSeptum1_REST, LeftVentricularSeptum1_ST, LeftVentricularSeptum1_ERP, LeftVentricularSeptum1_RRP };
proctype LeftVentricularSeptum1 () {
REST:
   if
    :: (c_expr { (LeftVentricularSeptum1_t <= 400.0) }  &&  (! LeftVentricularSeptum1_ACTcell1)  &&  LeftVentricularSeptum1_cstate == LeftVentricularSeptum1_REST) ->
     if
      :: LeftVentricularSeptum1_pstate != LeftVentricularSeptum1_cstate || LeftVentricularSeptum1_force_init_update ->
       c_code { LeftVentricularSeptum1_t_init = LeftVentricularSeptum1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricularSeptum1_t_slope =  1 ;
       LeftVentricularSeptum1_t_u = (LeftVentricularSeptum1_t_slope * d) + LeftVentricularSeptum1_t ;
       
       
       
      };
      LeftVentricularSeptum1_pstate = LeftVentricularSeptum1_cstate;
      LeftVentricularSeptum1_cstate = LeftVentricularSeptum1_REST;
      LeftVentricularSeptum1_force_init_update = false;
      LeftVentricularSeptum1_ACTpath  = false;
      LeftVentricularSeptum1_ACTcell1  = false;
      LeftVentricularSeptum1_tick = true;
     };
     do
      :: LeftVentricularSeptum1_tick -> true
      :: else -> goto  REST
     od;
     ::  (LeftVentricularSeptum1_ACTcell1
           && 
          LeftVentricularSeptum1_cstate == LeftVentricularSeptum1_REST)  -> 
      d_step {
            c_code {  LeftVentricularSeptum1_t_u = 0 ; };
            LeftVentricularSeptum1_pstate =  LeftVentricularSeptum1_cstate ;
            LeftVentricularSeptum1_cstate =  LeftVentricularSeptum1_ST ;
            LeftVentricularSeptum1_force_init_update = false;
            LeftVentricularSeptum1_tick = true;
      };
      do
       :: LeftVentricularSeptum1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (LeftVentricularSeptum1_t > 400.0) }
           && 
          LeftVentricularSeptum1_cstate == LeftVentricularSeptum1_REST)  -> 
      d_step {
            c_code {  LeftVentricularSeptum1_t_u = 0 ; };
            LeftVentricularSeptum1_pstate =  LeftVentricularSeptum1_cstate ;
            LeftVentricularSeptum1_cstate =  LeftVentricularSeptum1_ST ;
            LeftVentricularSeptum1_force_init_update = false;
            LeftVentricularSeptum1_tick = true;
      };
      do
       :: LeftVentricularSeptum1_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (LeftVentricularSeptum1_t <= 10.0) }  &&  (! LeftVentricularSeptum1_ACTcell1)  &&  LeftVentricularSeptum1_cstate == LeftVentricularSeptum1_ST) ->
     if
      :: LeftVentricularSeptum1_pstate != LeftVentricularSeptum1_cstate || LeftVentricularSeptum1_force_init_update ->
       c_code { LeftVentricularSeptum1_t_init = LeftVentricularSeptum1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricularSeptum1_t_slope =  1 ;
       LeftVentricularSeptum1_t_u = (LeftVentricularSeptum1_t_slope * d) + LeftVentricularSeptum1_t ;
       
       
       
      };
      LeftVentricularSeptum1_pstate = LeftVentricularSeptum1_cstate;
      LeftVentricularSeptum1_cstate = LeftVentricularSeptum1_ST;
      LeftVentricularSeptum1_force_init_update = false;
      LeftVentricularSeptum1_ACTpath  = false;
      LeftVentricularSeptum1_ACTcell1  = false;
      LeftVentricularSeptum1_tick = true;
     };
     do
      :: LeftVentricularSeptum1_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (LeftVentricularSeptum1_t > 10.0) }
           && 
          LeftVentricularSeptum1_cstate == LeftVentricularSeptum1_ST)  -> 
      d_step {
            c_code {  LeftVentricularSeptum1_t_u = 0 ; };
            LeftVentricularSeptum1_ACTpath = 1;
            LeftVentricularSeptum1_pstate =  LeftVentricularSeptum1_cstate ;
            LeftVentricularSeptum1_cstate =  LeftVentricularSeptum1_ERP ;
            LeftVentricularSeptum1_force_init_update = false;
            LeftVentricularSeptum1_tick = true;
      };
      do
       :: LeftVentricularSeptum1_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (LeftVentricularSeptum1_t <= 200.0) }  &&  (! LeftVentricularSeptum1_ACTcell1)  &&  LeftVentricularSeptum1_cstate == LeftVentricularSeptum1_ERP) ->
     if
      :: LeftVentricularSeptum1_pstate != LeftVentricularSeptum1_cstate || LeftVentricularSeptum1_force_init_update ->
       c_code { LeftVentricularSeptum1_t_init = LeftVentricularSeptum1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricularSeptum1_t_slope =  1 ;
       LeftVentricularSeptum1_t_u = (LeftVentricularSeptum1_t_slope * d) + LeftVentricularSeptum1_t ;
       
       
       
      };
      LeftVentricularSeptum1_pstate = LeftVentricularSeptum1_cstate;
      LeftVentricularSeptum1_cstate = LeftVentricularSeptum1_ERP;
      LeftVentricularSeptum1_force_init_update = false;
      LeftVentricularSeptum1_ACTpath  = false;
      LeftVentricularSeptum1_ACTcell1  = false;
      LeftVentricularSeptum1_tick = true;
     };
     do
      :: LeftVentricularSeptum1_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (LeftVentricularSeptum1_t > 200.0) }
           && 
          LeftVentricularSeptum1_cstate == LeftVentricularSeptum1_ERP)  -> 
      d_step {
            c_code {  LeftVentricularSeptum1_t_u = 0 ; };
            LeftVentricularSeptum1_pstate =  LeftVentricularSeptum1_cstate ;
            LeftVentricularSeptum1_cstate =  LeftVentricularSeptum1_RRP ;
            LeftVentricularSeptum1_force_init_update = false;
            LeftVentricularSeptum1_tick = true;
      };
      do
       :: LeftVentricularSeptum1_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (LeftVentricularSeptum1_t <= 100.0) }  &&  (! LeftVentricularSeptum1_ACTcell1)  &&  LeftVentricularSeptum1_cstate == LeftVentricularSeptum1_RRP) ->
     if
      :: LeftVentricularSeptum1_pstate != LeftVentricularSeptum1_cstate || LeftVentricularSeptum1_force_init_update ->
       c_code { LeftVentricularSeptum1_t_init = LeftVentricularSeptum1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LeftVentricularSeptum1_t_slope =  1 ;
       LeftVentricularSeptum1_t_u = (LeftVentricularSeptum1_t_slope * d) + LeftVentricularSeptum1_t ;
       
       
       
      };
      LeftVentricularSeptum1_pstate = LeftVentricularSeptum1_cstate;
      LeftVentricularSeptum1_cstate = LeftVentricularSeptum1_RRP;
      LeftVentricularSeptum1_force_init_update = false;
      LeftVentricularSeptum1_ACTpath  = false;
      LeftVentricularSeptum1_ACTcell1  = false;
      LeftVentricularSeptum1_tick = true;
     };
     do
      :: LeftVentricularSeptum1_tick -> true
      :: else -> goto  RRP
     od;
     ::  (LeftVentricularSeptum1_ACTcell1
           && 
          LeftVentricularSeptum1_cstate == LeftVentricularSeptum1_RRP)  -> 
      d_step {
            c_code {  LeftVentricularSeptum1_t_u = 0 ; };
            LeftVentricularSeptum1_pstate =  LeftVentricularSeptum1_cstate ;
            LeftVentricularSeptum1_cstate =  LeftVentricularSeptum1_ST ;
            LeftVentricularSeptum1_force_init_update = false;
            LeftVentricularSeptum1_tick = true;
      };
      do
       :: LeftVentricularSeptum1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (LeftVentricularSeptum1_t > 100.0) }
           && 
          LeftVentricularSeptum1_cstate == LeftVentricularSeptum1_RRP)  -> 
      d_step {
            c_code {  LeftVentricularSeptum1_t_u = 0 ; };
            LeftVentricularSeptum1_pstate =  LeftVentricularSeptum1_cstate ;
            LeftVentricularSeptum1_cstate =  LeftVentricularSeptum1_REST ;
            LeftVentricularSeptum1_force_init_update = false;
            LeftVentricularSeptum1_tick = true;
      };
      do
       :: LeftVentricularSeptum1_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double CSLeftVentricular_t_u, CSLeftVentricular_t_slope, CSLeftVentricular_t_init, CSLeftVentricular_t =  0 ;};
c_track "&CSLeftVentricular_t" "sizeof(double)";
c_track "&CSLeftVentricular_t_slope" "sizeof(double)";
c_track "&CSLeftVentricular_t_u" "sizeof(double)";
c_track "&CSLeftVentricular_t_init" "sizeof(double)";


bool CSLeftVentricular_ACTpath = 0 ;
bool CSLeftVentricular_ACTcell1 ;
mtype { CSLeftVentricular_REST, CSLeftVentricular_ST, CSLeftVentricular_ERP, CSLeftVentricular_RRP };
proctype CSLeftVentricular () {
REST:
   if
    :: (c_expr { (CSLeftVentricular_t <= 400.0) }  &&  (! CSLeftVentricular_ACTcell1)  &&  CSLeftVentricular_cstate == CSLeftVentricular_REST) ->
     if
      :: CSLeftVentricular_pstate != CSLeftVentricular_cstate || CSLeftVentricular_force_init_update ->
       c_code { CSLeftVentricular_t_init = CSLeftVentricular_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       CSLeftVentricular_t_slope =  1 ;
       CSLeftVentricular_t_u = (CSLeftVentricular_t_slope * d) + CSLeftVentricular_t ;
       
       
       
      };
      CSLeftVentricular_pstate = CSLeftVentricular_cstate;
      CSLeftVentricular_cstate = CSLeftVentricular_REST;
      CSLeftVentricular_force_init_update = false;
      CSLeftVentricular_ACTpath  = false;
      CSLeftVentricular_ACTcell1  = false;
      CSLeftVentricular_tick = true;
     };
     do
      :: CSLeftVentricular_tick -> true
      :: else -> goto  REST
     od;
     ::  (CSLeftVentricular_ACTcell1
           && 
          CSLeftVentricular_cstate == CSLeftVentricular_REST)  -> 
      d_step {
            c_code {  CSLeftVentricular_t_u = 0 ; };
            CSLeftVentricular_pstate =  CSLeftVentricular_cstate ;
            CSLeftVentricular_cstate =  CSLeftVentricular_ST ;
            CSLeftVentricular_force_init_update = false;
            CSLeftVentricular_tick = true;
      };
      do
       :: CSLeftVentricular_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (CSLeftVentricular_t > 400.0) }
           && 
          CSLeftVentricular_cstate == CSLeftVentricular_REST)  -> 
      d_step {
            c_code {  CSLeftVentricular_t_u = 0 ; };
            CSLeftVentricular_pstate =  CSLeftVentricular_cstate ;
            CSLeftVentricular_cstate =  CSLeftVentricular_ST ;
            CSLeftVentricular_force_init_update = false;
            CSLeftVentricular_tick = true;
      };
      do
       :: CSLeftVentricular_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (CSLeftVentricular_t <= 10.0) }  &&  (! CSLeftVentricular_ACTcell1)  &&  CSLeftVentricular_cstate == CSLeftVentricular_ST) ->
     if
      :: CSLeftVentricular_pstate != CSLeftVentricular_cstate || CSLeftVentricular_force_init_update ->
       c_code { CSLeftVentricular_t_init = CSLeftVentricular_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       CSLeftVentricular_t_slope =  1 ;
       CSLeftVentricular_t_u = (CSLeftVentricular_t_slope * d) + CSLeftVentricular_t ;
       
       
       
      };
      CSLeftVentricular_pstate = CSLeftVentricular_cstate;
      CSLeftVentricular_cstate = CSLeftVentricular_ST;
      CSLeftVentricular_force_init_update = false;
      CSLeftVentricular_ACTpath  = false;
      CSLeftVentricular_ACTcell1  = false;
      CSLeftVentricular_tick = true;
     };
     do
      :: CSLeftVentricular_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (CSLeftVentricular_t > 10.0) }
           && 
          CSLeftVentricular_cstate == CSLeftVentricular_ST)  -> 
      d_step {
            c_code {  CSLeftVentricular_t_u = 0 ; };
            CSLeftVentricular_ACTpath = 1;
            CSLeftVentricular_pstate =  CSLeftVentricular_cstate ;
            CSLeftVentricular_cstate =  CSLeftVentricular_ERP ;
            CSLeftVentricular_force_init_update = false;
            CSLeftVentricular_tick = true;
      };
      do
       :: CSLeftVentricular_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (CSLeftVentricular_t <= 200.0) }  &&  (! CSLeftVentricular_ACTcell1)  &&  CSLeftVentricular_cstate == CSLeftVentricular_ERP) ->
     if
      :: CSLeftVentricular_pstate != CSLeftVentricular_cstate || CSLeftVentricular_force_init_update ->
       c_code { CSLeftVentricular_t_init = CSLeftVentricular_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       CSLeftVentricular_t_slope =  1 ;
       CSLeftVentricular_t_u = (CSLeftVentricular_t_slope * d) + CSLeftVentricular_t ;
       
       
       
      };
      CSLeftVentricular_pstate = CSLeftVentricular_cstate;
      CSLeftVentricular_cstate = CSLeftVentricular_ERP;
      CSLeftVentricular_force_init_update = false;
      CSLeftVentricular_ACTpath  = false;
      CSLeftVentricular_ACTcell1  = false;
      CSLeftVentricular_tick = true;
     };
     do
      :: CSLeftVentricular_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (CSLeftVentricular_t > 200.0) }
           && 
          CSLeftVentricular_cstate == CSLeftVentricular_ERP)  -> 
      d_step {
            c_code {  CSLeftVentricular_t_u = 0 ; };
            CSLeftVentricular_pstate =  CSLeftVentricular_cstate ;
            CSLeftVentricular_cstate =  CSLeftVentricular_RRP ;
            CSLeftVentricular_force_init_update = false;
            CSLeftVentricular_tick = true;
      };
      do
       :: CSLeftVentricular_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (CSLeftVentricular_t <= 100.0) }  &&  (! CSLeftVentricular_ACTcell1)  &&  CSLeftVentricular_cstate == CSLeftVentricular_RRP) ->
     if
      :: CSLeftVentricular_pstate != CSLeftVentricular_cstate || CSLeftVentricular_force_init_update ->
       c_code { CSLeftVentricular_t_init = CSLeftVentricular_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       CSLeftVentricular_t_slope =  1 ;
       CSLeftVentricular_t_u = (CSLeftVentricular_t_slope * d) + CSLeftVentricular_t ;
       
       
       
      };
      CSLeftVentricular_pstate = CSLeftVentricular_cstate;
      CSLeftVentricular_cstate = CSLeftVentricular_RRP;
      CSLeftVentricular_force_init_update = false;
      CSLeftVentricular_ACTpath  = false;
      CSLeftVentricular_ACTcell1  = false;
      CSLeftVentricular_tick = true;
     };
     do
      :: CSLeftVentricular_tick -> true
      :: else -> goto  RRP
     od;
     ::  (CSLeftVentricular_ACTcell1
           && 
          CSLeftVentricular_cstate == CSLeftVentricular_RRP)  -> 
      d_step {
            c_code {  CSLeftVentricular_t_u = 0 ; };
            CSLeftVentricular_pstate =  CSLeftVentricular_cstate ;
            CSLeftVentricular_cstate =  CSLeftVentricular_ST ;
            CSLeftVentricular_force_init_update = false;
            CSLeftVentricular_tick = true;
      };
      do
       :: CSLeftVentricular_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (CSLeftVentricular_t > 100.0) }
           && 
          CSLeftVentricular_cstate == CSLeftVentricular_RRP)  -> 
      d_step {
            c_code {  CSLeftVentricular_t_u = 0 ; };
            CSLeftVentricular_pstate =  CSLeftVentricular_cstate ;
            CSLeftVentricular_cstate =  CSLeftVentricular_REST ;
            CSLeftVentricular_force_init_update = false;
            CSLeftVentricular_tick = true;
      };
      do
       :: CSLeftVentricular_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double RightBundleBranch_t_u, RightBundleBranch_t_slope, RightBundleBranch_t_init, RightBundleBranch_t =  0 ;};
c_track "&RightBundleBranch_t" "sizeof(double)";
c_track "&RightBundleBranch_t_slope" "sizeof(double)";
c_track "&RightBundleBranch_t_u" "sizeof(double)";
c_track "&RightBundleBranch_t_init" "sizeof(double)";


bool RightBundleBranch_ACTpath = 0 ;
bool RightBundleBranch_ACTcell1 ;
mtype { RightBundleBranch_REST, RightBundleBranch_ST, RightBundleBranch_ERP, RightBundleBranch_RRP };
proctype RightBundleBranch () {
REST:
   if
    :: (c_expr { (RightBundleBranch_t <= 400.0) }  &&  (! RightBundleBranch_ACTcell1)  &&  RightBundleBranch_cstate == RightBundleBranch_REST) ->
     if
      :: RightBundleBranch_pstate != RightBundleBranch_cstate || RightBundleBranch_force_init_update ->
       c_code { RightBundleBranch_t_init = RightBundleBranch_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightBundleBranch_t_slope =  1 ;
       RightBundleBranch_t_u = (RightBundleBranch_t_slope * d) + RightBundleBranch_t ;
       
       
       
      };
      RightBundleBranch_pstate = RightBundleBranch_cstate;
      RightBundleBranch_cstate = RightBundleBranch_REST;
      RightBundleBranch_force_init_update = false;
      RightBundleBranch_ACTpath  = false;
      RightBundleBranch_ACTcell1  = false;
      RightBundleBranch_tick = true;
     };
     do
      :: RightBundleBranch_tick -> true
      :: else -> goto  REST
     od;
     ::  (RightBundleBranch_ACTcell1
           && 
          RightBundleBranch_cstate == RightBundleBranch_REST)  -> 
      d_step {
            c_code {  RightBundleBranch_t_u = 0 ; };
            RightBundleBranch_pstate =  RightBundleBranch_cstate ;
            RightBundleBranch_cstate =  RightBundleBranch_ST ;
            RightBundleBranch_force_init_update = false;
            RightBundleBranch_tick = true;
      };
      do
       :: RightBundleBranch_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (RightBundleBranch_t > 400.0) }
           && 
          RightBundleBranch_cstate == RightBundleBranch_REST)  -> 
      d_step {
            c_code {  RightBundleBranch_t_u = 0 ; };
            RightBundleBranch_pstate =  RightBundleBranch_cstate ;
            RightBundleBranch_cstate =  RightBundleBranch_ST ;
            RightBundleBranch_force_init_update = false;
            RightBundleBranch_tick = true;
      };
      do
       :: RightBundleBranch_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (RightBundleBranch_t <= 10.0) }  &&  (! RightBundleBranch_ACTcell1)  &&  RightBundleBranch_cstate == RightBundleBranch_ST) ->
     if
      :: RightBundleBranch_pstate != RightBundleBranch_cstate || RightBundleBranch_force_init_update ->
       c_code { RightBundleBranch_t_init = RightBundleBranch_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightBundleBranch_t_slope =  1 ;
       RightBundleBranch_t_u = (RightBundleBranch_t_slope * d) + RightBundleBranch_t ;
       
       
       
      };
      RightBundleBranch_pstate = RightBundleBranch_cstate;
      RightBundleBranch_cstate = RightBundleBranch_ST;
      RightBundleBranch_force_init_update = false;
      RightBundleBranch_ACTpath  = false;
      RightBundleBranch_ACTcell1  = false;
      RightBundleBranch_tick = true;
     };
     do
      :: RightBundleBranch_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (RightBundleBranch_t > 10.0) }
           && 
          RightBundleBranch_cstate == RightBundleBranch_ST)  -> 
      d_step {
            c_code {  RightBundleBranch_t_u = 0 ; };
            RightBundleBranch_ACTpath = 1;
            RightBundleBranch_pstate =  RightBundleBranch_cstate ;
            RightBundleBranch_cstate =  RightBundleBranch_ERP ;
            RightBundleBranch_force_init_update = false;
            RightBundleBranch_tick = true;
      };
      do
       :: RightBundleBranch_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (RightBundleBranch_t <= 200.0) }  &&  (! RightBundleBranch_ACTcell1)  &&  RightBundleBranch_cstate == RightBundleBranch_ERP) ->
     if
      :: RightBundleBranch_pstate != RightBundleBranch_cstate || RightBundleBranch_force_init_update ->
       c_code { RightBundleBranch_t_init = RightBundleBranch_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightBundleBranch_t_slope =  1 ;
       RightBundleBranch_t_u = (RightBundleBranch_t_slope * d) + RightBundleBranch_t ;
       
       
       
      };
      RightBundleBranch_pstate = RightBundleBranch_cstate;
      RightBundleBranch_cstate = RightBundleBranch_ERP;
      RightBundleBranch_force_init_update = false;
      RightBundleBranch_ACTpath  = false;
      RightBundleBranch_ACTcell1  = false;
      RightBundleBranch_tick = true;
     };
     do
      :: RightBundleBranch_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (RightBundleBranch_t > 200.0) }
           && 
          RightBundleBranch_cstate == RightBundleBranch_ERP)  -> 
      d_step {
            c_code {  RightBundleBranch_t_u = 0 ; };
            RightBundleBranch_pstate =  RightBundleBranch_cstate ;
            RightBundleBranch_cstate =  RightBundleBranch_RRP ;
            RightBundleBranch_force_init_update = false;
            RightBundleBranch_tick = true;
      };
      do
       :: RightBundleBranch_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (RightBundleBranch_t <= 100.0) }  &&  (! RightBundleBranch_ACTcell1)  &&  RightBundleBranch_cstate == RightBundleBranch_RRP) ->
     if
      :: RightBundleBranch_pstate != RightBundleBranch_cstate || RightBundleBranch_force_init_update ->
       c_code { RightBundleBranch_t_init = RightBundleBranch_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightBundleBranch_t_slope =  1 ;
       RightBundleBranch_t_u = (RightBundleBranch_t_slope * d) + RightBundleBranch_t ;
       
       
       
      };
      RightBundleBranch_pstate = RightBundleBranch_cstate;
      RightBundleBranch_cstate = RightBundleBranch_RRP;
      RightBundleBranch_force_init_update = false;
      RightBundleBranch_ACTpath  = false;
      RightBundleBranch_ACTcell1  = false;
      RightBundleBranch_tick = true;
     };
     do
      :: RightBundleBranch_tick -> true
      :: else -> goto  RRP
     od;
     ::  (RightBundleBranch_ACTcell1
           && 
          RightBundleBranch_cstate == RightBundleBranch_RRP)  -> 
      d_step {
            c_code {  RightBundleBranch_t_u = 0 ; };
            RightBundleBranch_pstate =  RightBundleBranch_cstate ;
            RightBundleBranch_cstate =  RightBundleBranch_ST ;
            RightBundleBranch_force_init_update = false;
            RightBundleBranch_tick = true;
      };
      do
       :: RightBundleBranch_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (RightBundleBranch_t > 100.0) }
           && 
          RightBundleBranch_cstate == RightBundleBranch_RRP)  -> 
      d_step {
            c_code {  RightBundleBranch_t_u = 0 ; };
            RightBundleBranch_pstate =  RightBundleBranch_cstate ;
            RightBundleBranch_cstate =  RightBundleBranch_REST ;
            RightBundleBranch_force_init_update = false;
            RightBundleBranch_tick = true;
      };
      do
       :: RightBundleBranch_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double RightBundleBranch1_t_u, RightBundleBranch1_t_slope, RightBundleBranch1_t_init, RightBundleBranch1_t =  0 ;};
c_track "&RightBundleBranch1_t" "sizeof(double)";
c_track "&RightBundleBranch1_t_slope" "sizeof(double)";
c_track "&RightBundleBranch1_t_u" "sizeof(double)";
c_track "&RightBundleBranch1_t_init" "sizeof(double)";


bool RightBundleBranch1_ACTpath = 0 ;
bool RightBundleBranch1_ACTcell1 ;
mtype { RightBundleBranch1_REST, RightBundleBranch1_ST, RightBundleBranch1_ERP, RightBundleBranch1_RRP };
proctype RightBundleBranch1 () {
REST:
   if
    :: (c_expr { (RightBundleBranch1_t <= 400.0) }  &&  (! RightBundleBranch1_ACTcell1)  &&  RightBundleBranch1_cstate == RightBundleBranch1_REST) ->
     if
      :: RightBundleBranch1_pstate != RightBundleBranch1_cstate || RightBundleBranch1_force_init_update ->
       c_code { RightBundleBranch1_t_init = RightBundleBranch1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightBundleBranch1_t_slope =  1 ;
       RightBundleBranch1_t_u = (RightBundleBranch1_t_slope * d) + RightBundleBranch1_t ;
       
       
       
      };
      RightBundleBranch1_pstate = RightBundleBranch1_cstate;
      RightBundleBranch1_cstate = RightBundleBranch1_REST;
      RightBundleBranch1_force_init_update = false;
      RightBundleBranch1_ACTpath  = false;
      RightBundleBranch1_ACTcell1  = false;
      RightBundleBranch1_tick = true;
     };
     do
      :: RightBundleBranch1_tick -> true
      :: else -> goto  REST
     od;
     ::  (RightBundleBranch1_ACTcell1
           && 
          RightBundleBranch1_cstate == RightBundleBranch1_REST)  -> 
      d_step {
            c_code {  RightBundleBranch1_t_u = 0 ; };
            RightBundleBranch1_pstate =  RightBundleBranch1_cstate ;
            RightBundleBranch1_cstate =  RightBundleBranch1_ST ;
            RightBundleBranch1_force_init_update = false;
            RightBundleBranch1_tick = true;
      };
      do
       :: RightBundleBranch1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (RightBundleBranch1_t > 400.0) }
           && 
          RightBundleBranch1_cstate == RightBundleBranch1_REST)  -> 
      d_step {
            c_code {  RightBundleBranch1_t_u = 0 ; };
            RightBundleBranch1_pstate =  RightBundleBranch1_cstate ;
            RightBundleBranch1_cstate =  RightBundleBranch1_ST ;
            RightBundleBranch1_force_init_update = false;
            RightBundleBranch1_tick = true;
      };
      do
       :: RightBundleBranch1_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (RightBundleBranch1_t <= 10.0) }  &&  (! RightBundleBranch1_ACTcell1)  &&  RightBundleBranch1_cstate == RightBundleBranch1_ST) ->
     if
      :: RightBundleBranch1_pstate != RightBundleBranch1_cstate || RightBundleBranch1_force_init_update ->
       c_code { RightBundleBranch1_t_init = RightBundleBranch1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightBundleBranch1_t_slope =  1 ;
       RightBundleBranch1_t_u = (RightBundleBranch1_t_slope * d) + RightBundleBranch1_t ;
       
       
       
      };
      RightBundleBranch1_pstate = RightBundleBranch1_cstate;
      RightBundleBranch1_cstate = RightBundleBranch1_ST;
      RightBundleBranch1_force_init_update = false;
      RightBundleBranch1_ACTpath  = false;
      RightBundleBranch1_ACTcell1  = false;
      RightBundleBranch1_tick = true;
     };
     do
      :: RightBundleBranch1_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (RightBundleBranch1_t > 10.0) }
           && 
          RightBundleBranch1_cstate == RightBundleBranch1_ST)  -> 
      d_step {
            c_code {  RightBundleBranch1_t_u = 0 ; };
            RightBundleBranch1_ACTpath = 1;
            RightBundleBranch1_pstate =  RightBundleBranch1_cstate ;
            RightBundleBranch1_cstate =  RightBundleBranch1_ERP ;
            RightBundleBranch1_force_init_update = false;
            RightBundleBranch1_tick = true;
      };
      do
       :: RightBundleBranch1_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (RightBundleBranch1_t <= 200.0) }  &&  (! RightBundleBranch1_ACTcell1)  &&  RightBundleBranch1_cstate == RightBundleBranch1_ERP) ->
     if
      :: RightBundleBranch1_pstate != RightBundleBranch1_cstate || RightBundleBranch1_force_init_update ->
       c_code { RightBundleBranch1_t_init = RightBundleBranch1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightBundleBranch1_t_slope =  1 ;
       RightBundleBranch1_t_u = (RightBundleBranch1_t_slope * d) + RightBundleBranch1_t ;
       
       
       
      };
      RightBundleBranch1_pstate = RightBundleBranch1_cstate;
      RightBundleBranch1_cstate = RightBundleBranch1_ERP;
      RightBundleBranch1_force_init_update = false;
      RightBundleBranch1_ACTpath  = false;
      RightBundleBranch1_ACTcell1  = false;
      RightBundleBranch1_tick = true;
     };
     do
      :: RightBundleBranch1_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (RightBundleBranch1_t > 200.0) }
           && 
          RightBundleBranch1_cstate == RightBundleBranch1_ERP)  -> 
      d_step {
            c_code {  RightBundleBranch1_t_u = 0 ; };
            RightBundleBranch1_pstate =  RightBundleBranch1_cstate ;
            RightBundleBranch1_cstate =  RightBundleBranch1_RRP ;
            RightBundleBranch1_force_init_update = false;
            RightBundleBranch1_tick = true;
      };
      do
       :: RightBundleBranch1_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (RightBundleBranch1_t <= 100.0) }  &&  (! RightBundleBranch1_ACTcell1)  &&  RightBundleBranch1_cstate == RightBundleBranch1_RRP) ->
     if
      :: RightBundleBranch1_pstate != RightBundleBranch1_cstate || RightBundleBranch1_force_init_update ->
       c_code { RightBundleBranch1_t_init = RightBundleBranch1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightBundleBranch1_t_slope =  1 ;
       RightBundleBranch1_t_u = (RightBundleBranch1_t_slope * d) + RightBundleBranch1_t ;
       
       
       
      };
      RightBundleBranch1_pstate = RightBundleBranch1_cstate;
      RightBundleBranch1_cstate = RightBundleBranch1_RRP;
      RightBundleBranch1_force_init_update = false;
      RightBundleBranch1_ACTpath  = false;
      RightBundleBranch1_ACTcell1  = false;
      RightBundleBranch1_tick = true;
     };
     do
      :: RightBundleBranch1_tick -> true
      :: else -> goto  RRP
     od;
     ::  (RightBundleBranch1_ACTcell1
           && 
          RightBundleBranch1_cstate == RightBundleBranch1_RRP)  -> 
      d_step {
            c_code {  RightBundleBranch1_t_u = 0 ; };
            RightBundleBranch1_pstate =  RightBundleBranch1_cstate ;
            RightBundleBranch1_cstate =  RightBundleBranch1_ST ;
            RightBundleBranch1_force_init_update = false;
            RightBundleBranch1_tick = true;
      };
      do
       :: RightBundleBranch1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (RightBundleBranch1_t > 100.0) }
           && 
          RightBundleBranch1_cstate == RightBundleBranch1_RRP)  -> 
      d_step {
            c_code {  RightBundleBranch1_t_u = 0 ; };
            RightBundleBranch1_pstate =  RightBundleBranch1_cstate ;
            RightBundleBranch1_cstate =  RightBundleBranch1_REST ;
            RightBundleBranch1_force_init_update = false;
            RightBundleBranch1_tick = true;
      };
      do
       :: RightBundleBranch1_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double RightVentricularApex_t_u, RightVentricularApex_t_slope, RightVentricularApex_t_init, RightVentricularApex_t =  0 ;};
c_track "&RightVentricularApex_t" "sizeof(double)";
c_track "&RightVentricularApex_t_slope" "sizeof(double)";
c_track "&RightVentricularApex_t_u" "sizeof(double)";
c_track "&RightVentricularApex_t_init" "sizeof(double)";


bool RightVentricularApex_ACTpath = 0 ;
bool RightVentricularApex_ACTcell1 ;
mtype { RightVentricularApex_REST, RightVentricularApex_ST, RightVentricularApex_ERP, RightVentricularApex_RRP };
proctype RightVentricularApex () {
REST:
   if
    :: (c_expr { (RightVentricularApex_t <= 400.0) }  &&  (! RightVentricularApex_ACTcell1)  &&  RightVentricularApex_cstate == RightVentricularApex_REST) ->
     if
      :: RightVentricularApex_pstate != RightVentricularApex_cstate || RightVentricularApex_force_init_update ->
       c_code { RightVentricularApex_t_init = RightVentricularApex_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricularApex_t_slope =  1 ;
       RightVentricularApex_t_u = (RightVentricularApex_t_slope * d) + RightVentricularApex_t ;
       
       
       
      };
      RightVentricularApex_pstate = RightVentricularApex_cstate;
      RightVentricularApex_cstate = RightVentricularApex_REST;
      RightVentricularApex_force_init_update = false;
      RightVentricularApex_ACTpath  = false;
      RightVentricularApex_ACTcell1  = false;
      RightVentricularApex_tick = true;
     };
     do
      :: RightVentricularApex_tick -> true
      :: else -> goto  REST
     od;
     ::  (RightVentricularApex_ACTcell1
           && 
          RightVentricularApex_cstate == RightVentricularApex_REST)  -> 
      d_step {
            c_code {  RightVentricularApex_t_u = 0 ; };
            RightVentricularApex_pstate =  RightVentricularApex_cstate ;
            RightVentricularApex_cstate =  RightVentricularApex_ST ;
            RightVentricularApex_force_init_update = false;
            RightVentricularApex_tick = true;
      };
      do
       :: RightVentricularApex_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (RightVentricularApex_t > 400.0) }
           && 
          RightVentricularApex_cstate == RightVentricularApex_REST)  -> 
      d_step {
            c_code {  RightVentricularApex_t_u = 0 ; };
            RightVentricularApex_pstate =  RightVentricularApex_cstate ;
            RightVentricularApex_cstate =  RightVentricularApex_ST ;
            RightVentricularApex_force_init_update = false;
            RightVentricularApex_tick = true;
      };
      do
       :: RightVentricularApex_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (RightVentricularApex_t <= 10.0) }  &&  (! RightVentricularApex_ACTcell1)  &&  RightVentricularApex_cstate == RightVentricularApex_ST) ->
     if
      :: RightVentricularApex_pstate != RightVentricularApex_cstate || RightVentricularApex_force_init_update ->
       c_code { RightVentricularApex_t_init = RightVentricularApex_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricularApex_t_slope =  1 ;
       RightVentricularApex_t_u = (RightVentricularApex_t_slope * d) + RightVentricularApex_t ;
       
       
       
      };
      RightVentricularApex_pstate = RightVentricularApex_cstate;
      RightVentricularApex_cstate = RightVentricularApex_ST;
      RightVentricularApex_force_init_update = false;
      RightVentricularApex_ACTpath  = false;
      RightVentricularApex_ACTcell1  = false;
      RightVentricularApex_tick = true;
     };
     do
      :: RightVentricularApex_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (RightVentricularApex_t > 10.0) }
           && 
          RightVentricularApex_cstate == RightVentricularApex_ST)  -> 
      d_step {
            c_code {  RightVentricularApex_t_u = 0 ; };
            RightVentricularApex_ACTpath = 1;
            RightVentricularApex_pstate =  RightVentricularApex_cstate ;
            RightVentricularApex_cstate =  RightVentricularApex_ERP ;
            RightVentricularApex_force_init_update = false;
            RightVentricularApex_tick = true;
      };
      do
       :: RightVentricularApex_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (RightVentricularApex_t <= 200.0) }  &&  (! RightVentricularApex_ACTcell1)  &&  RightVentricularApex_cstate == RightVentricularApex_ERP) ->
     if
      :: RightVentricularApex_pstate != RightVentricularApex_cstate || RightVentricularApex_force_init_update ->
       c_code { RightVentricularApex_t_init = RightVentricularApex_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricularApex_t_slope =  1 ;
       RightVentricularApex_t_u = (RightVentricularApex_t_slope * d) + RightVentricularApex_t ;
       
       
       
      };
      RightVentricularApex_pstate = RightVentricularApex_cstate;
      RightVentricularApex_cstate = RightVentricularApex_ERP;
      RightVentricularApex_force_init_update = false;
      RightVentricularApex_ACTpath  = false;
      RightVentricularApex_ACTcell1  = false;
      RightVentricularApex_tick = true;
     };
     do
      :: RightVentricularApex_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (RightVentricularApex_t > 200.0) }
           && 
          RightVentricularApex_cstate == RightVentricularApex_ERP)  -> 
      d_step {
            c_code {  RightVentricularApex_t_u = 0 ; };
            RightVentricularApex_pstate =  RightVentricularApex_cstate ;
            RightVentricularApex_cstate =  RightVentricularApex_RRP ;
            RightVentricularApex_force_init_update = false;
            RightVentricularApex_tick = true;
      };
      do
       :: RightVentricularApex_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (RightVentricularApex_t <= 100.0) }  &&  (! RightVentricularApex_ACTcell1)  &&  RightVentricularApex_cstate == RightVentricularApex_RRP) ->
     if
      :: RightVentricularApex_pstate != RightVentricularApex_cstate || RightVentricularApex_force_init_update ->
       c_code { RightVentricularApex_t_init = RightVentricularApex_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricularApex_t_slope =  1 ;
       RightVentricularApex_t_u = (RightVentricularApex_t_slope * d) + RightVentricularApex_t ;
       
       
       
      };
      RightVentricularApex_pstate = RightVentricularApex_cstate;
      RightVentricularApex_cstate = RightVentricularApex_RRP;
      RightVentricularApex_force_init_update = false;
      RightVentricularApex_ACTpath  = false;
      RightVentricularApex_ACTcell1  = false;
      RightVentricularApex_tick = true;
     };
     do
      :: RightVentricularApex_tick -> true
      :: else -> goto  RRP
     od;
     ::  (RightVentricularApex_ACTcell1
           && 
          RightVentricularApex_cstate == RightVentricularApex_RRP)  -> 
      d_step {
            c_code {  RightVentricularApex_t_u = 0 ; };
            RightVentricularApex_pstate =  RightVentricularApex_cstate ;
            RightVentricularApex_cstate =  RightVentricularApex_ST ;
            RightVentricularApex_force_init_update = false;
            RightVentricularApex_tick = true;
      };
      do
       :: RightVentricularApex_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (RightVentricularApex_t > 100.0) }
           && 
          RightVentricularApex_cstate == RightVentricularApex_RRP)  -> 
      d_step {
            c_code {  RightVentricularApex_t_u = 0 ; };
            RightVentricularApex_pstate =  RightVentricularApex_cstate ;
            RightVentricularApex_cstate =  RightVentricularApex_REST ;
            RightVentricularApex_force_init_update = false;
            RightVentricularApex_tick = true;
      };
      do
       :: RightVentricularApex_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double RightVentricle_t_u, RightVentricle_t_slope, RightVentricle_t_init, RightVentricle_t =  0 ;};
c_track "&RightVentricle_t" "sizeof(double)";
c_track "&RightVentricle_t_slope" "sizeof(double)";
c_track "&RightVentricle_t_u" "sizeof(double)";
c_track "&RightVentricle_t_init" "sizeof(double)";


bool RightVentricle_ACTpath = 0 ;
bool RightVentricle_ACTcell1 ;
mtype { RightVentricle_REST, RightVentricle_ST, RightVentricle_ERP, RightVentricle_RRP };
proctype RightVentricle () {
REST:
   if
    :: (c_expr { (RightVentricle_t <= 400.0) }  &&  (! RightVentricle_ACTcell1)  &&  RightVentricle_cstate == RightVentricle_REST) ->
     if
      :: RightVentricle_pstate != RightVentricle_cstate || RightVentricle_force_init_update ->
       c_code { RightVentricle_t_init = RightVentricle_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricle_t_slope =  1 ;
       RightVentricle_t_u = (RightVentricle_t_slope * d) + RightVentricle_t ;
       
       
       
      };
      RightVentricle_pstate = RightVentricle_cstate;
      RightVentricle_cstate = RightVentricle_REST;
      RightVentricle_force_init_update = false;
      RightVentricle_ACTpath  = false;
      RightVentricle_ACTcell1  = false;
      RightVentricle_tick = true;
     };
     do
      :: RightVentricle_tick -> true
      :: else -> goto  REST
     od;
     ::  (RightVentricle_ACTcell1
           && 
          RightVentricle_cstate == RightVentricle_REST)  -> 
      d_step {
            c_code {  RightVentricle_t_u = 0 ; };
            RightVentricle_pstate =  RightVentricle_cstate ;
            RightVentricle_cstate =  RightVentricle_ST ;
            RightVentricle_force_init_update = false;
            RightVentricle_tick = true;
      };
      do
       :: RightVentricle_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (RightVentricle_t > 400.0) }
           && 
          RightVentricle_cstate == RightVentricle_REST)  -> 
      d_step {
            c_code {  RightVentricle_t_u = 0 ; };
            RightVentricle_pstate =  RightVentricle_cstate ;
            RightVentricle_cstate =  RightVentricle_ST ;
            RightVentricle_force_init_update = false;
            RightVentricle_tick = true;
      };
      do
       :: RightVentricle_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (RightVentricle_t <= 10.0) }  &&  (! RightVentricle_ACTcell1)  &&  RightVentricle_cstate == RightVentricle_ST) ->
     if
      :: RightVentricle_pstate != RightVentricle_cstate || RightVentricle_force_init_update ->
       c_code { RightVentricle_t_init = RightVentricle_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricle_t_slope =  1 ;
       RightVentricle_t_u = (RightVentricle_t_slope * d) + RightVentricle_t ;
       
       
       
      };
      RightVentricle_pstate = RightVentricle_cstate;
      RightVentricle_cstate = RightVentricle_ST;
      RightVentricle_force_init_update = false;
      RightVentricle_ACTpath  = false;
      RightVentricle_ACTcell1  = false;
      RightVentricle_tick = true;
     };
     do
      :: RightVentricle_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (RightVentricle_t > 10.0) }
           && 
          RightVentricle_cstate == RightVentricle_ST)  -> 
      d_step {
            c_code {  RightVentricle_t_u = 0 ; };
            RightVentricle_ACTpath = 1;
            RightVentricle_pstate =  RightVentricle_cstate ;
            RightVentricle_cstate =  RightVentricle_ERP ;
            RightVentricle_force_init_update = false;
            RightVentricle_tick = true;
      };
      do
       :: RightVentricle_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (RightVentricle_t <= 200.0) }  &&  (! RightVentricle_ACTcell1)  &&  RightVentricle_cstate == RightVentricle_ERP) ->
     if
      :: RightVentricle_pstate != RightVentricle_cstate || RightVentricle_force_init_update ->
       c_code { RightVentricle_t_init = RightVentricle_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricle_t_slope =  1 ;
       RightVentricle_t_u = (RightVentricle_t_slope * d) + RightVentricle_t ;
       
       
       
      };
      RightVentricle_pstate = RightVentricle_cstate;
      RightVentricle_cstate = RightVentricle_ERP;
      RightVentricle_force_init_update = false;
      RightVentricle_ACTpath  = false;
      RightVentricle_ACTcell1  = false;
      RightVentricle_tick = true;
     };
     do
      :: RightVentricle_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (RightVentricle_t > 200.0) }
           && 
          RightVentricle_cstate == RightVentricle_ERP)  -> 
      d_step {
            c_code {  RightVentricle_t_u = 0 ; };
            RightVentricle_pstate =  RightVentricle_cstate ;
            RightVentricle_cstate =  RightVentricle_RRP ;
            RightVentricle_force_init_update = false;
            RightVentricle_tick = true;
      };
      do
       :: RightVentricle_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (RightVentricle_t <= 100.0) }  &&  (! RightVentricle_ACTcell1)  &&  RightVentricle_cstate == RightVentricle_RRP) ->
     if
      :: RightVentricle_pstate != RightVentricle_cstate || RightVentricle_force_init_update ->
       c_code { RightVentricle_t_init = RightVentricle_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricle_t_slope =  1 ;
       RightVentricle_t_u = (RightVentricle_t_slope * d) + RightVentricle_t ;
       
       
       
      };
      RightVentricle_pstate = RightVentricle_cstate;
      RightVentricle_cstate = RightVentricle_RRP;
      RightVentricle_force_init_update = false;
      RightVentricle_ACTpath  = false;
      RightVentricle_ACTcell1  = false;
      RightVentricle_tick = true;
     };
     do
      :: RightVentricle_tick -> true
      :: else -> goto  RRP
     od;
     ::  (RightVentricle_ACTcell1
           && 
          RightVentricle_cstate == RightVentricle_RRP)  -> 
      d_step {
            c_code {  RightVentricle_t_u = 0 ; };
            RightVentricle_pstate =  RightVentricle_cstate ;
            RightVentricle_cstate =  RightVentricle_ST ;
            RightVentricle_force_init_update = false;
            RightVentricle_tick = true;
      };
      do
       :: RightVentricle_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (RightVentricle_t > 100.0) }
           && 
          RightVentricle_cstate == RightVentricle_RRP)  -> 
      d_step {
            c_code {  RightVentricle_t_u = 0 ; };
            RightVentricle_pstate =  RightVentricle_cstate ;
            RightVentricle_cstate =  RightVentricle_REST ;
            RightVentricle_force_init_update = false;
            RightVentricle_tick = true;
      };
      do
       :: RightVentricle_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double RightVentricle1_t_u, RightVentricle1_t_slope, RightVentricle1_t_init, RightVentricle1_t =  0 ;};
c_track "&RightVentricle1_t" "sizeof(double)";
c_track "&RightVentricle1_t_slope" "sizeof(double)";
c_track "&RightVentricle1_t_u" "sizeof(double)";
c_track "&RightVentricle1_t_init" "sizeof(double)";


bool RightVentricle1_ACTpath = 0 ;
bool RightVentricle1_ACTcell1 ;
mtype { RightVentricle1_REST, RightVentricle1_ST, RightVentricle1_ERP, RightVentricle1_RRP };
proctype RightVentricle1 () {
REST:
   if
    :: (c_expr { (RightVentricle1_t <= 400.0) }  &&  (! RightVentricle1_ACTcell1)  &&  RightVentricle1_cstate == RightVentricle1_REST) ->
     if
      :: RightVentricle1_pstate != RightVentricle1_cstate || RightVentricle1_force_init_update ->
       c_code { RightVentricle1_t_init = RightVentricle1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricle1_t_slope =  1 ;
       RightVentricle1_t_u = (RightVentricle1_t_slope * d) + RightVentricle1_t ;
       
       
       
      };
      RightVentricle1_pstate = RightVentricle1_cstate;
      RightVentricle1_cstate = RightVentricle1_REST;
      RightVentricle1_force_init_update = false;
      RightVentricle1_ACTpath  = false;
      RightVentricle1_ACTcell1  = false;
      RightVentricle1_tick = true;
     };
     do
      :: RightVentricle1_tick -> true
      :: else -> goto  REST
     od;
     ::  (RightVentricle1_ACTcell1
           && 
          RightVentricle1_cstate == RightVentricle1_REST)  -> 
      d_step {
            c_code {  RightVentricle1_t_u = 0 ; };
            RightVentricle1_pstate =  RightVentricle1_cstate ;
            RightVentricle1_cstate =  RightVentricle1_ST ;
            RightVentricle1_force_init_update = false;
            RightVentricle1_tick = true;
      };
      do
       :: RightVentricle1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (RightVentricle1_t > 400.0) }
           && 
          RightVentricle1_cstate == RightVentricle1_REST)  -> 
      d_step {
            c_code {  RightVentricle1_t_u = 0 ; };
            RightVentricle1_pstate =  RightVentricle1_cstate ;
            RightVentricle1_cstate =  RightVentricle1_ST ;
            RightVentricle1_force_init_update = false;
            RightVentricle1_tick = true;
      };
      do
       :: RightVentricle1_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (RightVentricle1_t <= 10.0) }  &&  (! RightVentricle1_ACTcell1)  &&  RightVentricle1_cstate == RightVentricle1_ST) ->
     if
      :: RightVentricle1_pstate != RightVentricle1_cstate || RightVentricle1_force_init_update ->
       c_code { RightVentricle1_t_init = RightVentricle1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricle1_t_slope =  1 ;
       RightVentricle1_t_u = (RightVentricle1_t_slope * d) + RightVentricle1_t ;
       
       
       
      };
      RightVentricle1_pstate = RightVentricle1_cstate;
      RightVentricle1_cstate = RightVentricle1_ST;
      RightVentricle1_force_init_update = false;
      RightVentricle1_ACTpath  = false;
      RightVentricle1_ACTcell1  = false;
      RightVentricle1_tick = true;
     };
     do
      :: RightVentricle1_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (RightVentricle1_t > 10.0) }
           && 
          RightVentricle1_cstate == RightVentricle1_ST)  -> 
      d_step {
            c_code {  RightVentricle1_t_u = 0 ; };
            RightVentricle1_ACTpath = 1;
            RightVentricle1_pstate =  RightVentricle1_cstate ;
            RightVentricle1_cstate =  RightVentricle1_ERP ;
            RightVentricle1_force_init_update = false;
            RightVentricle1_tick = true;
      };
      do
       :: RightVentricle1_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (RightVentricle1_t <= 200.0) }  &&  (! RightVentricle1_ACTcell1)  &&  RightVentricle1_cstate == RightVentricle1_ERP) ->
     if
      :: RightVentricle1_pstate != RightVentricle1_cstate || RightVentricle1_force_init_update ->
       c_code { RightVentricle1_t_init = RightVentricle1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricle1_t_slope =  1 ;
       RightVentricle1_t_u = (RightVentricle1_t_slope * d) + RightVentricle1_t ;
       
       
       
      };
      RightVentricle1_pstate = RightVentricle1_cstate;
      RightVentricle1_cstate = RightVentricle1_ERP;
      RightVentricle1_force_init_update = false;
      RightVentricle1_ACTpath  = false;
      RightVentricle1_ACTcell1  = false;
      RightVentricle1_tick = true;
     };
     do
      :: RightVentricle1_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (RightVentricle1_t > 200.0) }
           && 
          RightVentricle1_cstate == RightVentricle1_ERP)  -> 
      d_step {
            c_code {  RightVentricle1_t_u = 0 ; };
            RightVentricle1_pstate =  RightVentricle1_cstate ;
            RightVentricle1_cstate =  RightVentricle1_RRP ;
            RightVentricle1_force_init_update = false;
            RightVentricle1_tick = true;
      };
      do
       :: RightVentricle1_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (RightVentricle1_t <= 100.0) }  &&  (! RightVentricle1_ACTcell1)  &&  RightVentricle1_cstate == RightVentricle1_RRP) ->
     if
      :: RightVentricle1_pstate != RightVentricle1_cstate || RightVentricle1_force_init_update ->
       c_code { RightVentricle1_t_init = RightVentricle1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricle1_t_slope =  1 ;
       RightVentricle1_t_u = (RightVentricle1_t_slope * d) + RightVentricle1_t ;
       
       
       
      };
      RightVentricle1_pstate = RightVentricle1_cstate;
      RightVentricle1_cstate = RightVentricle1_RRP;
      RightVentricle1_force_init_update = false;
      RightVentricle1_ACTpath  = false;
      RightVentricle1_ACTcell1  = false;
      RightVentricle1_tick = true;
     };
     do
      :: RightVentricle1_tick -> true
      :: else -> goto  RRP
     od;
     ::  (RightVentricle1_ACTcell1
           && 
          RightVentricle1_cstate == RightVentricle1_RRP)  -> 
      d_step {
            c_code {  RightVentricle1_t_u = 0 ; };
            RightVentricle1_pstate =  RightVentricle1_cstate ;
            RightVentricle1_cstate =  RightVentricle1_ST ;
            RightVentricle1_force_init_update = false;
            RightVentricle1_tick = true;
      };
      do
       :: RightVentricle1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (RightVentricle1_t > 100.0) }
           && 
          RightVentricle1_cstate == RightVentricle1_RRP)  -> 
      d_step {
            c_code {  RightVentricle1_t_u = 0 ; };
            RightVentricle1_pstate =  RightVentricle1_cstate ;
            RightVentricle1_cstate =  RightVentricle1_REST ;
            RightVentricle1_force_init_update = false;
            RightVentricle1_tick = true;
      };
      do
       :: RightVentricle1_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double RightVentricularSeptum_t_u, RightVentricularSeptum_t_slope, RightVentricularSeptum_t_init, RightVentricularSeptum_t =  0 ;};
c_track "&RightVentricularSeptum_t" "sizeof(double)";
c_track "&RightVentricularSeptum_t_slope" "sizeof(double)";
c_track "&RightVentricularSeptum_t_u" "sizeof(double)";
c_track "&RightVentricularSeptum_t_init" "sizeof(double)";


bool RightVentricularSeptum_ACTpath = 0 ;
bool RightVentricularSeptum_ACTcell1 ;
mtype { RightVentricularSeptum_REST, RightVentricularSeptum_ST, RightVentricularSeptum_ERP, RightVentricularSeptum_RRP };
proctype RightVentricularSeptum () {
REST:
   if
    :: (c_expr { (RightVentricularSeptum_t <= 400.0) }  &&  (! RightVentricularSeptum_ACTcell1)  &&  RightVentricularSeptum_cstate == RightVentricularSeptum_REST) ->
     if
      :: RightVentricularSeptum_pstate != RightVentricularSeptum_cstate || RightVentricularSeptum_force_init_update ->
       c_code { RightVentricularSeptum_t_init = RightVentricularSeptum_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricularSeptum_t_slope =  1 ;
       RightVentricularSeptum_t_u = (RightVentricularSeptum_t_slope * d) + RightVentricularSeptum_t ;
       
       
       
      };
      RightVentricularSeptum_pstate = RightVentricularSeptum_cstate;
      RightVentricularSeptum_cstate = RightVentricularSeptum_REST;
      RightVentricularSeptum_force_init_update = false;
      RightVentricularSeptum_ACTpath  = false;
      RightVentricularSeptum_ACTcell1  = false;
      RightVentricularSeptum_tick = true;
     };
     do
      :: RightVentricularSeptum_tick -> true
      :: else -> goto  REST
     od;
     ::  (RightVentricularSeptum_ACTcell1
           && 
          RightVentricularSeptum_cstate == RightVentricularSeptum_REST)  -> 
      d_step {
            c_code {  RightVentricularSeptum_t_u = 0 ; };
            RightVentricularSeptum_pstate =  RightVentricularSeptum_cstate ;
            RightVentricularSeptum_cstate =  RightVentricularSeptum_ST ;
            RightVentricularSeptum_force_init_update = false;
            RightVentricularSeptum_tick = true;
      };
      do
       :: RightVentricularSeptum_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (RightVentricularSeptum_t > 400.0) }
           && 
          RightVentricularSeptum_cstate == RightVentricularSeptum_REST)  -> 
      d_step {
            c_code {  RightVentricularSeptum_t_u = 0 ; };
            RightVentricularSeptum_pstate =  RightVentricularSeptum_cstate ;
            RightVentricularSeptum_cstate =  RightVentricularSeptum_ST ;
            RightVentricularSeptum_force_init_update = false;
            RightVentricularSeptum_tick = true;
      };
      do
       :: RightVentricularSeptum_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (RightVentricularSeptum_t <= 10.0) }  &&  (! RightVentricularSeptum_ACTcell1)  &&  RightVentricularSeptum_cstate == RightVentricularSeptum_ST) ->
     if
      :: RightVentricularSeptum_pstate != RightVentricularSeptum_cstate || RightVentricularSeptum_force_init_update ->
       c_code { RightVentricularSeptum_t_init = RightVentricularSeptum_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricularSeptum_t_slope =  1 ;
       RightVentricularSeptum_t_u = (RightVentricularSeptum_t_slope * d) + RightVentricularSeptum_t ;
       
       
       
      };
      RightVentricularSeptum_pstate = RightVentricularSeptum_cstate;
      RightVentricularSeptum_cstate = RightVentricularSeptum_ST;
      RightVentricularSeptum_force_init_update = false;
      RightVentricularSeptum_ACTpath  = false;
      RightVentricularSeptum_ACTcell1  = false;
      RightVentricularSeptum_tick = true;
     };
     do
      :: RightVentricularSeptum_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (RightVentricularSeptum_t > 10.0) }
           && 
          RightVentricularSeptum_cstate == RightVentricularSeptum_ST)  -> 
      d_step {
            c_code {  RightVentricularSeptum_t_u = 0 ; };
            RightVentricularSeptum_ACTpath = 1;
            RightVentricularSeptum_pstate =  RightVentricularSeptum_cstate ;
            RightVentricularSeptum_cstate =  RightVentricularSeptum_ERP ;
            RightVentricularSeptum_force_init_update = false;
            RightVentricularSeptum_tick = true;
      };
      do
       :: RightVentricularSeptum_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (RightVentricularSeptum_t <= 200.0) }  &&  (! RightVentricularSeptum_ACTcell1)  &&  RightVentricularSeptum_cstate == RightVentricularSeptum_ERP) ->
     if
      :: RightVentricularSeptum_pstate != RightVentricularSeptum_cstate || RightVentricularSeptum_force_init_update ->
       c_code { RightVentricularSeptum_t_init = RightVentricularSeptum_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricularSeptum_t_slope =  1 ;
       RightVentricularSeptum_t_u = (RightVentricularSeptum_t_slope * d) + RightVentricularSeptum_t ;
       
       
       
      };
      RightVentricularSeptum_pstate = RightVentricularSeptum_cstate;
      RightVentricularSeptum_cstate = RightVentricularSeptum_ERP;
      RightVentricularSeptum_force_init_update = false;
      RightVentricularSeptum_ACTpath  = false;
      RightVentricularSeptum_ACTcell1  = false;
      RightVentricularSeptum_tick = true;
     };
     do
      :: RightVentricularSeptum_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (RightVentricularSeptum_t > 200.0) }
           && 
          RightVentricularSeptum_cstate == RightVentricularSeptum_ERP)  -> 
      d_step {
            c_code {  RightVentricularSeptum_t_u = 0 ; };
            RightVentricularSeptum_pstate =  RightVentricularSeptum_cstate ;
            RightVentricularSeptum_cstate =  RightVentricularSeptum_RRP ;
            RightVentricularSeptum_force_init_update = false;
            RightVentricularSeptum_tick = true;
      };
      do
       :: RightVentricularSeptum_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (RightVentricularSeptum_t <= 100.0) }  &&  (! RightVentricularSeptum_ACTcell1)  &&  RightVentricularSeptum_cstate == RightVentricularSeptum_RRP) ->
     if
      :: RightVentricularSeptum_pstate != RightVentricularSeptum_cstate || RightVentricularSeptum_force_init_update ->
       c_code { RightVentricularSeptum_t_init = RightVentricularSeptum_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricularSeptum_t_slope =  1 ;
       RightVentricularSeptum_t_u = (RightVentricularSeptum_t_slope * d) + RightVentricularSeptum_t ;
       
       
       
      };
      RightVentricularSeptum_pstate = RightVentricularSeptum_cstate;
      RightVentricularSeptum_cstate = RightVentricularSeptum_RRP;
      RightVentricularSeptum_force_init_update = false;
      RightVentricularSeptum_ACTpath  = false;
      RightVentricularSeptum_ACTcell1  = false;
      RightVentricularSeptum_tick = true;
     };
     do
      :: RightVentricularSeptum_tick -> true
      :: else -> goto  RRP
     od;
     ::  (RightVentricularSeptum_ACTcell1
           && 
          RightVentricularSeptum_cstate == RightVentricularSeptum_RRP)  -> 
      d_step {
            c_code {  RightVentricularSeptum_t_u = 0 ; };
            RightVentricularSeptum_pstate =  RightVentricularSeptum_cstate ;
            RightVentricularSeptum_cstate =  RightVentricularSeptum_ST ;
            RightVentricularSeptum_force_init_update = false;
            RightVentricularSeptum_tick = true;
      };
      do
       :: RightVentricularSeptum_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (RightVentricularSeptum_t > 100.0) }
           && 
          RightVentricularSeptum_cstate == RightVentricularSeptum_RRP)  -> 
      d_step {
            c_code {  RightVentricularSeptum_t_u = 0 ; };
            RightVentricularSeptum_pstate =  RightVentricularSeptum_cstate ;
            RightVentricularSeptum_cstate =  RightVentricularSeptum_REST ;
            RightVentricularSeptum_force_init_update = false;
            RightVentricularSeptum_tick = true;
      };
      do
       :: RightVentricularSeptum_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double RightVentricularSeptum1_t_u, RightVentricularSeptum1_t_slope, RightVentricularSeptum1_t_init, RightVentricularSeptum1_t =  0 ;};
c_track "&RightVentricularSeptum1_t" "sizeof(double)";
c_track "&RightVentricularSeptum1_t_slope" "sizeof(double)";
c_track "&RightVentricularSeptum1_t_u" "sizeof(double)";
c_track "&RightVentricularSeptum1_t_init" "sizeof(double)";


bool RightVentricularSeptum1_ACTpath = 0 ;
bool RightVentricularSeptum1_ACTcell1 ;
mtype { RightVentricularSeptum1_REST, RightVentricularSeptum1_ST, RightVentricularSeptum1_ERP, RightVentricularSeptum1_RRP };
proctype RightVentricularSeptum1 () {
REST:
   if
    :: (c_expr { (RightVentricularSeptum1_t <= 400.0) }  &&  (! RightVentricularSeptum1_ACTcell1)  &&  RightVentricularSeptum1_cstate == RightVentricularSeptum1_REST) ->
     if
      :: RightVentricularSeptum1_pstate != RightVentricularSeptum1_cstate || RightVentricularSeptum1_force_init_update ->
       c_code { RightVentricularSeptum1_t_init = RightVentricularSeptum1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricularSeptum1_t_slope =  1 ;
       RightVentricularSeptum1_t_u = (RightVentricularSeptum1_t_slope * d) + RightVentricularSeptum1_t ;
       
       
       
      };
      RightVentricularSeptum1_pstate = RightVentricularSeptum1_cstate;
      RightVentricularSeptum1_cstate = RightVentricularSeptum1_REST;
      RightVentricularSeptum1_force_init_update = false;
      RightVentricularSeptum1_ACTpath  = false;
      RightVentricularSeptum1_ACTcell1  = false;
      RightVentricularSeptum1_tick = true;
     };
     do
      :: RightVentricularSeptum1_tick -> true
      :: else -> goto  REST
     od;
     ::  (RightVentricularSeptum1_ACTcell1
           && 
          RightVentricularSeptum1_cstate == RightVentricularSeptum1_REST)  -> 
      d_step {
            c_code {  RightVentricularSeptum1_t_u = 0 ; };
            RightVentricularSeptum1_pstate =  RightVentricularSeptum1_cstate ;
            RightVentricularSeptum1_cstate =  RightVentricularSeptum1_ST ;
            RightVentricularSeptum1_force_init_update = false;
            RightVentricularSeptum1_tick = true;
      };
      do
       :: RightVentricularSeptum1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (RightVentricularSeptum1_t > 400.0) }
           && 
          RightVentricularSeptum1_cstate == RightVentricularSeptum1_REST)  -> 
      d_step {
            c_code {  RightVentricularSeptum1_t_u = 0 ; };
            RightVentricularSeptum1_pstate =  RightVentricularSeptum1_cstate ;
            RightVentricularSeptum1_cstate =  RightVentricularSeptum1_ST ;
            RightVentricularSeptum1_force_init_update = false;
            RightVentricularSeptum1_tick = true;
      };
      do
       :: RightVentricularSeptum1_tick -> true
       :: else -> goto  ST
      od;

     :: else -> true
   fi;
ST:
   if
    :: (c_expr { (RightVentricularSeptum1_t <= 10.0) }  &&  (! RightVentricularSeptum1_ACTcell1)  &&  RightVentricularSeptum1_cstate == RightVentricularSeptum1_ST) ->
     if
      :: RightVentricularSeptum1_pstate != RightVentricularSeptum1_cstate || RightVentricularSeptum1_force_init_update ->
       c_code { RightVentricularSeptum1_t_init = RightVentricularSeptum1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricularSeptum1_t_slope =  1 ;
       RightVentricularSeptum1_t_u = (RightVentricularSeptum1_t_slope * d) + RightVentricularSeptum1_t ;
       
       
       
      };
      RightVentricularSeptum1_pstate = RightVentricularSeptum1_cstate;
      RightVentricularSeptum1_cstate = RightVentricularSeptum1_ST;
      RightVentricularSeptum1_force_init_update = false;
      RightVentricularSeptum1_ACTpath  = false;
      RightVentricularSeptum1_ACTcell1  = false;
      RightVentricularSeptum1_tick = true;
     };
     do
      :: RightVentricularSeptum1_tick -> true
      :: else -> goto  ST
     od;
     ::  (c_expr { (RightVentricularSeptum1_t > 10.0) }
           && 
          RightVentricularSeptum1_cstate == RightVentricularSeptum1_ST)  -> 
      d_step {
            c_code {  RightVentricularSeptum1_t_u = 0 ; };
            RightVentricularSeptum1_ACTpath = 1;
            RightVentricularSeptum1_pstate =  RightVentricularSeptum1_cstate ;
            RightVentricularSeptum1_cstate =  RightVentricularSeptum1_ERP ;
            RightVentricularSeptum1_force_init_update = false;
            RightVentricularSeptum1_tick = true;
      };
      do
       :: RightVentricularSeptum1_tick -> true
       :: else -> goto  ERP
      od;

     :: else -> true
   fi;
ERP:
   if
    :: (c_expr { (RightVentricularSeptum1_t <= 200.0) }  &&  (! RightVentricularSeptum1_ACTcell1)  &&  RightVentricularSeptum1_cstate == RightVentricularSeptum1_ERP) ->
     if
      :: RightVentricularSeptum1_pstate != RightVentricularSeptum1_cstate || RightVentricularSeptum1_force_init_update ->
       c_code { RightVentricularSeptum1_t_init = RightVentricularSeptum1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricularSeptum1_t_slope =  1 ;
       RightVentricularSeptum1_t_u = (RightVentricularSeptum1_t_slope * d) + RightVentricularSeptum1_t ;
       
       
       
      };
      RightVentricularSeptum1_pstate = RightVentricularSeptum1_cstate;
      RightVentricularSeptum1_cstate = RightVentricularSeptum1_ERP;
      RightVentricularSeptum1_force_init_update = false;
      RightVentricularSeptum1_ACTpath  = false;
      RightVentricularSeptum1_ACTcell1  = false;
      RightVentricularSeptum1_tick = true;
     };
     do
      :: RightVentricularSeptum1_tick -> true
      :: else -> goto  ERP
     od;
     ::  (c_expr { (RightVentricularSeptum1_t > 200.0) }
           && 
          RightVentricularSeptum1_cstate == RightVentricularSeptum1_ERP)  -> 
      d_step {
            c_code {  RightVentricularSeptum1_t_u = 0 ; };
            RightVentricularSeptum1_pstate =  RightVentricularSeptum1_cstate ;
            RightVentricularSeptum1_cstate =  RightVentricularSeptum1_RRP ;
            RightVentricularSeptum1_force_init_update = false;
            RightVentricularSeptum1_tick = true;
      };
      do
       :: RightVentricularSeptum1_tick -> true
       :: else -> goto  RRP
      od;

     :: else -> true
   fi;
RRP:
   if
    :: (c_expr { (RightVentricularSeptum1_t <= 100.0) }  &&  (! RightVentricularSeptum1_ACTcell1)  &&  RightVentricularSeptum1_cstate == RightVentricularSeptum1_RRP) ->
     if
      :: RightVentricularSeptum1_pstate != RightVentricularSeptum1_cstate || RightVentricularSeptum1_force_init_update ->
       c_code { RightVentricularSeptum1_t_init = RightVentricularSeptum1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RightVentricularSeptum1_t_slope =  1 ;
       RightVentricularSeptum1_t_u = (RightVentricularSeptum1_t_slope * d) + RightVentricularSeptum1_t ;
       
       
       
      };
      RightVentricularSeptum1_pstate = RightVentricularSeptum1_cstate;
      RightVentricularSeptum1_cstate = RightVentricularSeptum1_RRP;
      RightVentricularSeptum1_force_init_update = false;
      RightVentricularSeptum1_ACTpath  = false;
      RightVentricularSeptum1_ACTcell1  = false;
      RightVentricularSeptum1_tick = true;
     };
     do
      :: RightVentricularSeptum1_tick -> true
      :: else -> goto  RRP
     od;
     ::  (RightVentricularSeptum1_ACTcell1
           && 
          RightVentricularSeptum1_cstate == RightVentricularSeptum1_RRP)  -> 
      d_step {
            c_code {  RightVentricularSeptum1_t_u = 0 ; };
            RightVentricularSeptum1_pstate =  RightVentricularSeptum1_cstate ;
            RightVentricularSeptum1_cstate =  RightVentricularSeptum1_ST ;
            RightVentricularSeptum1_force_init_update = false;
            RightVentricularSeptum1_tick = true;
      };
      do
       :: RightVentricularSeptum1_tick -> true
       :: else -> goto  ST
      od;
     ::  (c_expr { (RightVentricularSeptum1_t > 100.0) }
           && 
          RightVentricularSeptum1_cstate == RightVentricularSeptum1_RRP)  -> 
      d_step {
            c_code {  RightVentricularSeptum1_t_u = 0 ; };
            RightVentricularSeptum1_pstate =  RightVentricularSeptum1_cstate ;
            RightVentricularSeptum1_cstate =  RightVentricularSeptum1_REST ;
            RightVentricularSeptum1_force_init_update = false;
            RightVentricularSeptum1_tick = true;
      };
      do
       :: RightVentricularSeptum1_tick -> true
       :: else -> goto  REST
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double SA_BB_t_u, SA_BB_t_slope, SA_BB_t_init, SA_BB_t =  0 ;};
c_track "&SA_BB_t" "sizeof(double)";
c_track "&SA_BB_t_slope" "sizeof(double)";
c_track "&SA_BB_t_u" "sizeof(double)";
c_track "&SA_BB_t_init" "sizeof(double)";


bool SA_BB_ACTcell = 0 ;
bool SA_BB_ACTpath ;
mtype { SA_BB_IDLE, SA_BB_ACT };
proctype SA_BB () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! SA_BB_ACTpath)  &&  SA_BB_cstate == SA_BB_IDLE) ->
     if
      :: SA_BB_pstate != SA_BB_cstate || SA_BB_force_init_update ->
       c_code { SA_BB_t_init = SA_BB_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       SA_BB_t_slope =  0 ;
       SA_BB_t_u = (SA_BB_t_slope * d) + SA_BB_t ;
       
       
       
      };
      SA_BB_pstate = SA_BB_cstate;
      SA_BB_cstate = SA_BB_IDLE;
      SA_BB_force_init_update = false;
      SA_BB_ACTcell  = false;
      SA_BB_ACTpath  = false;
      SA_BB_tick = true;
     };
     do
      :: SA_BB_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (SA_BB_ACTpath && 
          SA_BB_cstate == SA_BB_IDLE)  -> 
      d_step {
            c_code {  SA_BB_t_u = 0 ; };
            SA_BB_pstate =  SA_BB_cstate ;
            SA_BB_cstate =  SA_BB_ACT ;
            SA_BB_force_init_update = false;
            SA_BB_tick = true;
      };
      do
       :: SA_BB_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (SA_BB_t <= 20.0) }  &&  (! SA_BB_ACTpath)  &&  SA_BB_cstate == SA_BB_ACT) ->
     if
      :: SA_BB_pstate != SA_BB_cstate || SA_BB_force_init_update ->
       c_code { SA_BB_t_init = SA_BB_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       SA_BB_t_slope =  1 ;
       SA_BB_t_u = (SA_BB_t_slope * d) + SA_BB_t ;
       
       
       
      };
      SA_BB_pstate = SA_BB_cstate;
      SA_BB_cstate = SA_BB_ACT;
      SA_BB_force_init_update = false;
      SA_BB_ACTcell  = false;
      SA_BB_ACTpath  = false;
      SA_BB_tick = true;
     };
     do
      :: SA_BB_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (SA_BB_t > 20.0) }
           && 
          SA_BB_cstate == SA_BB_ACT)  -> 
      d_step {
            c_code {  SA_BB_t_u = 0 ; };
            SA_BB_ACTcell = 1;
            SA_BB_pstate =  SA_BB_cstate ;
            SA_BB_cstate =  SA_BB_IDLE ;
            SA_BB_force_init_update = false;
            SA_BB_tick = true;
      };
      do
       :: SA_BB_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double SA_OS_t_u, SA_OS_t_slope, SA_OS_t_init, SA_OS_t =  0 ;};
c_track "&SA_OS_t" "sizeof(double)";
c_track "&SA_OS_t_slope" "sizeof(double)";
c_track "&SA_OS_t_u" "sizeof(double)";
c_track "&SA_OS_t_init" "sizeof(double)";


bool SA_OS_ACTcell = 0 ;
bool SA_OS_ACTpath ;
mtype { SA_OS_IDLE, SA_OS_ACT };
proctype SA_OS () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! SA_OS_ACTpath)  &&  SA_OS_cstate == SA_OS_IDLE) ->
     if
      :: SA_OS_pstate != SA_OS_cstate || SA_OS_force_init_update ->
       c_code { SA_OS_t_init = SA_OS_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       SA_OS_t_slope =  0 ;
       SA_OS_t_u = (SA_OS_t_slope * d) + SA_OS_t ;
       
       
       
      };
      SA_OS_pstate = SA_OS_cstate;
      SA_OS_cstate = SA_OS_IDLE;
      SA_OS_force_init_update = false;
      SA_OS_ACTcell  = false;
      SA_OS_ACTpath  = false;
      SA_OS_tick = true;
     };
     do
      :: SA_OS_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (SA_OS_ACTpath && 
          SA_OS_cstate == SA_OS_IDLE)  -> 
      d_step {
            c_code {  SA_OS_t_u = 0 ; };
            SA_OS_pstate =  SA_OS_cstate ;
            SA_OS_cstate =  SA_OS_ACT ;
            SA_OS_force_init_update = false;
            SA_OS_tick = true;
      };
      do
       :: SA_OS_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (SA_OS_t <= 20.0) }  &&  (! SA_OS_ACTpath)  &&  SA_OS_cstate == SA_OS_ACT) ->
     if
      :: SA_OS_pstate != SA_OS_cstate || SA_OS_force_init_update ->
       c_code { SA_OS_t_init = SA_OS_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       SA_OS_t_slope =  1 ;
       SA_OS_t_u = (SA_OS_t_slope * d) + SA_OS_t ;
       
       
       
      };
      SA_OS_pstate = SA_OS_cstate;
      SA_OS_cstate = SA_OS_ACT;
      SA_OS_force_init_update = false;
      SA_OS_ACTcell  = false;
      SA_OS_ACTpath  = false;
      SA_OS_tick = true;
     };
     do
      :: SA_OS_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (SA_OS_t > 20.0) }
           && 
          SA_OS_cstate == SA_OS_ACT)  -> 
      d_step {
            c_code {  SA_OS_t_u = 0 ; };
            SA_OS_ACTcell = 1;
            SA_OS_pstate =  SA_OS_cstate ;
            SA_OS_cstate =  SA_OS_IDLE ;
            SA_OS_force_init_update = false;
            SA_OS_tick = true;
      };
      do
       :: SA_OS_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double SA_RA_t_u, SA_RA_t_slope, SA_RA_t_init, SA_RA_t =  0 ;};
c_track "&SA_RA_t" "sizeof(double)";
c_track "&SA_RA_t_slope" "sizeof(double)";
c_track "&SA_RA_t_u" "sizeof(double)";
c_track "&SA_RA_t_init" "sizeof(double)";


bool SA_RA_ACTcell = 0 ;
bool SA_RA_ACTpath ;
mtype { SA_RA_IDLE, SA_RA_ACT };
proctype SA_RA () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! SA_RA_ACTpath)  &&  SA_RA_cstate == SA_RA_IDLE) ->
     if
      :: SA_RA_pstate != SA_RA_cstate || SA_RA_force_init_update ->
       c_code { SA_RA_t_init = SA_RA_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       SA_RA_t_slope =  0 ;
       SA_RA_t_u = (SA_RA_t_slope * d) + SA_RA_t ;
       
       
       
      };
      SA_RA_pstate = SA_RA_cstate;
      SA_RA_cstate = SA_RA_IDLE;
      SA_RA_force_init_update = false;
      SA_RA_ACTcell  = false;
      SA_RA_ACTpath  = false;
      SA_RA_tick = true;
     };
     do
      :: SA_RA_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (SA_RA_ACTpath && 
          SA_RA_cstate == SA_RA_IDLE)  -> 
      d_step {
            c_code {  SA_RA_t_u = 0 ; };
            SA_RA_pstate =  SA_RA_cstate ;
            SA_RA_cstate =  SA_RA_ACT ;
            SA_RA_force_init_update = false;
            SA_RA_tick = true;
      };
      do
       :: SA_RA_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (SA_RA_t <= 20.0) }  &&  (! SA_RA_ACTpath)  &&  SA_RA_cstate == SA_RA_ACT) ->
     if
      :: SA_RA_pstate != SA_RA_cstate || SA_RA_force_init_update ->
       c_code { SA_RA_t_init = SA_RA_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       SA_RA_t_slope =  1 ;
       SA_RA_t_u = (SA_RA_t_slope * d) + SA_RA_t ;
       
       
       
      };
      SA_RA_pstate = SA_RA_cstate;
      SA_RA_cstate = SA_RA_ACT;
      SA_RA_force_init_update = false;
      SA_RA_ACTcell  = false;
      SA_RA_ACTpath  = false;
      SA_RA_tick = true;
     };
     do
      :: SA_RA_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (SA_RA_t > 20.0) }
           && 
          SA_RA_cstate == SA_RA_ACT)  -> 
      d_step {
            c_code {  SA_RA_t_u = 0 ; };
            SA_RA_ACTcell = 1;
            SA_RA_pstate =  SA_RA_cstate ;
            SA_RA_cstate =  SA_RA_IDLE ;
            SA_RA_force_init_update = false;
            SA_RA_tick = true;
      };
      do
       :: SA_RA_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double SA_CT_t_u, SA_CT_t_slope, SA_CT_t_init, SA_CT_t =  0 ;};
c_track "&SA_CT_t" "sizeof(double)";
c_track "&SA_CT_t_slope" "sizeof(double)";
c_track "&SA_CT_t_u" "sizeof(double)";
c_track "&SA_CT_t_init" "sizeof(double)";


bool SA_CT_ACTcell = 0 ;
bool SA_CT_ACTpath ;
mtype { SA_CT_IDLE, SA_CT_ACT };
proctype SA_CT () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! SA_CT_ACTpath)  &&  SA_CT_cstate == SA_CT_IDLE) ->
     if
      :: SA_CT_pstate != SA_CT_cstate || SA_CT_force_init_update ->
       c_code { SA_CT_t_init = SA_CT_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       SA_CT_t_slope =  0 ;
       SA_CT_t_u = (SA_CT_t_slope * d) + SA_CT_t ;
       
       
       
      };
      SA_CT_pstate = SA_CT_cstate;
      SA_CT_cstate = SA_CT_IDLE;
      SA_CT_force_init_update = false;
      SA_CT_ACTcell  = false;
      SA_CT_ACTpath  = false;
      SA_CT_tick = true;
     };
     do
      :: SA_CT_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (SA_CT_ACTpath && 
          SA_CT_cstate == SA_CT_IDLE)  -> 
      d_step {
            c_code {  SA_CT_t_u = 0 ; };
            SA_CT_pstate =  SA_CT_cstate ;
            SA_CT_cstate =  SA_CT_ACT ;
            SA_CT_force_init_update = false;
            SA_CT_tick = true;
      };
      do
       :: SA_CT_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (SA_CT_t <= 20.0) }  &&  (! SA_CT_ACTpath)  &&  SA_CT_cstate == SA_CT_ACT) ->
     if
      :: SA_CT_pstate != SA_CT_cstate || SA_CT_force_init_update ->
       c_code { SA_CT_t_init = SA_CT_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       SA_CT_t_slope =  1 ;
       SA_CT_t_u = (SA_CT_t_slope * d) + SA_CT_t ;
       
       
       
      };
      SA_CT_pstate = SA_CT_cstate;
      SA_CT_cstate = SA_CT_ACT;
      SA_CT_force_init_update = false;
      SA_CT_ACTcell  = false;
      SA_CT_ACTpath  = false;
      SA_CT_tick = true;
     };
     do
      :: SA_CT_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (SA_CT_t > 20.0) }
           && 
          SA_CT_cstate == SA_CT_ACT)  -> 
      d_step {
            c_code {  SA_CT_t_u = 0 ; };
            SA_CT_ACTcell = 1;
            SA_CT_pstate =  SA_CT_cstate ;
            SA_CT_cstate =  SA_CT_IDLE ;
            SA_CT_force_init_update = false;
            SA_CT_tick = true;
      };
      do
       :: SA_CT_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double BB_LA_t_u, BB_LA_t_slope, BB_LA_t_init, BB_LA_t =  0 ;};
c_track "&BB_LA_t" "sizeof(double)";
c_track "&BB_LA_t_slope" "sizeof(double)";
c_track "&BB_LA_t_u" "sizeof(double)";
c_track "&BB_LA_t_init" "sizeof(double)";


bool BB_LA_ACTcell = 0 ;
bool BB_LA_ACTpath ;
mtype { BB_LA_IDLE, BB_LA_ACT };
proctype BB_LA () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! BB_LA_ACTpath)  &&  BB_LA_cstate == BB_LA_IDLE) ->
     if
      :: BB_LA_pstate != BB_LA_cstate || BB_LA_force_init_update ->
       c_code { BB_LA_t_init = BB_LA_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       BB_LA_t_slope =  0 ;
       BB_LA_t_u = (BB_LA_t_slope * d) + BB_LA_t ;
       
       
       
      };
      BB_LA_pstate = BB_LA_cstate;
      BB_LA_cstate = BB_LA_IDLE;
      BB_LA_force_init_update = false;
      BB_LA_ACTcell  = false;
      BB_LA_ACTpath  = false;
      BB_LA_tick = true;
     };
     do
      :: BB_LA_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (BB_LA_ACTpath && 
          BB_LA_cstate == BB_LA_IDLE)  -> 
      d_step {
            c_code {  BB_LA_t_u = 0 ; };
            BB_LA_pstate =  BB_LA_cstate ;
            BB_LA_cstate =  BB_LA_ACT ;
            BB_LA_force_init_update = false;
            BB_LA_tick = true;
      };
      do
       :: BB_LA_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (BB_LA_t <= 30.0) }  &&  (! BB_LA_ACTpath)  &&  BB_LA_cstate == BB_LA_ACT) ->
     if
      :: BB_LA_pstate != BB_LA_cstate || BB_LA_force_init_update ->
       c_code { BB_LA_t_init = BB_LA_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       BB_LA_t_slope =  1 ;
       BB_LA_t_u = (BB_LA_t_slope * d) + BB_LA_t ;
       
       
       
      };
      BB_LA_pstate = BB_LA_cstate;
      BB_LA_cstate = BB_LA_ACT;
      BB_LA_force_init_update = false;
      BB_LA_ACTcell  = false;
      BB_LA_ACTpath  = false;
      BB_LA_tick = true;
     };
     do
      :: BB_LA_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (BB_LA_t > 30.0) }
           && 
          BB_LA_cstate == BB_LA_ACT)  -> 
      d_step {
            c_code {  BB_LA_t_u = 0 ; };
            BB_LA_ACTcell = 1;
            BB_LA_pstate =  BB_LA_cstate ;
            BB_LA_cstate =  BB_LA_IDLE ;
            BB_LA_force_init_update = false;
            BB_LA_tick = true;
      };
      do
       :: BB_LA_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double LA_LA1_t_u, LA_LA1_t_slope, LA_LA1_t_init, LA_LA1_t =  0 ;};
c_track "&LA_LA1_t" "sizeof(double)";
c_track "&LA_LA1_t_slope" "sizeof(double)";
c_track "&LA_LA1_t_u" "sizeof(double)";
c_track "&LA_LA1_t_init" "sizeof(double)";


bool LA_LA1_ACTcell = 0 ;
bool LA_LA1_ACTpath ;
mtype { LA_LA1_IDLE, LA_LA1_ACT };
proctype LA_LA1 () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! LA_LA1_ACTpath)  &&  LA_LA1_cstate == LA_LA1_IDLE) ->
     if
      :: LA_LA1_pstate != LA_LA1_cstate || LA_LA1_force_init_update ->
       c_code { LA_LA1_t_init = LA_LA1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LA_LA1_t_slope =  0 ;
       LA_LA1_t_u = (LA_LA1_t_slope * d) + LA_LA1_t ;
       
       
       
      };
      LA_LA1_pstate = LA_LA1_cstate;
      LA_LA1_cstate = LA_LA1_IDLE;
      LA_LA1_force_init_update = false;
      LA_LA1_ACTcell  = false;
      LA_LA1_ACTpath  = false;
      LA_LA1_tick = true;
     };
     do
      :: LA_LA1_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (LA_LA1_ACTpath && 
          LA_LA1_cstate == LA_LA1_IDLE)  -> 
      d_step {
            c_code {  LA_LA1_t_u = 0 ; };
            LA_LA1_pstate =  LA_LA1_cstate ;
            LA_LA1_cstate =  LA_LA1_ACT ;
            LA_LA1_force_init_update = false;
            LA_LA1_tick = true;
      };
      do
       :: LA_LA1_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (LA_LA1_t <= 40.0) }  &&  (! LA_LA1_ACTpath)  &&  LA_LA1_cstate == LA_LA1_ACT) ->
     if
      :: LA_LA1_pstate != LA_LA1_cstate || LA_LA1_force_init_update ->
       c_code { LA_LA1_t_init = LA_LA1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LA_LA1_t_slope =  1 ;
       LA_LA1_t_u = (LA_LA1_t_slope * d) + LA_LA1_t ;
       
       
       
      };
      LA_LA1_pstate = LA_LA1_cstate;
      LA_LA1_cstate = LA_LA1_ACT;
      LA_LA1_force_init_update = false;
      LA_LA1_ACTcell  = false;
      LA_LA1_ACTpath  = false;
      LA_LA1_tick = true;
     };
     do
      :: LA_LA1_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (LA_LA1_t > 40.0) }
           && 
          LA_LA1_cstate == LA_LA1_ACT)  -> 
      d_step {
            c_code {  LA_LA1_t_u = 0 ; };
            LA_LA1_ACTcell = 1;
            LA_LA1_pstate =  LA_LA1_cstate ;
            LA_LA1_cstate =  LA_LA1_IDLE ;
            LA_LA1_force_init_update = false;
            LA_LA1_tick = true;
      };
      do
       :: LA_LA1_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double RA_RA1_t_u, RA_RA1_t_slope, RA_RA1_t_init, RA_RA1_t =  0 ;};
c_track "&RA_RA1_t" "sizeof(double)";
c_track "&RA_RA1_t_slope" "sizeof(double)";
c_track "&RA_RA1_t_u" "sizeof(double)";
c_track "&RA_RA1_t_init" "sizeof(double)";


bool RA_RA1_ACTcell = 0 ;
bool RA_RA1_ACTpath ;
mtype { RA_RA1_IDLE, RA_RA1_ACT };
proctype RA_RA1 () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! RA_RA1_ACTpath)  &&  RA_RA1_cstate == RA_RA1_IDLE) ->
     if
      :: RA_RA1_pstate != RA_RA1_cstate || RA_RA1_force_init_update ->
       c_code { RA_RA1_t_init = RA_RA1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RA_RA1_t_slope =  0 ;
       RA_RA1_t_u = (RA_RA1_t_slope * d) + RA_RA1_t ;
       
       
       
      };
      RA_RA1_pstate = RA_RA1_cstate;
      RA_RA1_cstate = RA_RA1_IDLE;
      RA_RA1_force_init_update = false;
      RA_RA1_ACTcell  = false;
      RA_RA1_ACTpath  = false;
      RA_RA1_tick = true;
     };
     do
      :: RA_RA1_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (RA_RA1_ACTpath && 
          RA_RA1_cstate == RA_RA1_IDLE)  -> 
      d_step {
            c_code {  RA_RA1_t_u = 0 ; };
            RA_RA1_pstate =  RA_RA1_cstate ;
            RA_RA1_cstate =  RA_RA1_ACT ;
            RA_RA1_force_init_update = false;
            RA_RA1_tick = true;
      };
      do
       :: RA_RA1_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (RA_RA1_t <= 20.0) }  &&  (! RA_RA1_ACTpath)  &&  RA_RA1_cstate == RA_RA1_ACT) ->
     if
      :: RA_RA1_pstate != RA_RA1_cstate || RA_RA1_force_init_update ->
       c_code { RA_RA1_t_init = RA_RA1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RA_RA1_t_slope =  1 ;
       RA_RA1_t_u = (RA_RA1_t_slope * d) + RA_RA1_t ;
       
       
       
      };
      RA_RA1_pstate = RA_RA1_cstate;
      RA_RA1_cstate = RA_RA1_ACT;
      RA_RA1_force_init_update = false;
      RA_RA1_ACTcell  = false;
      RA_RA1_ACTpath  = false;
      RA_RA1_tick = true;
     };
     do
      :: RA_RA1_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (RA_RA1_t > 20.0) }
           && 
          RA_RA1_cstate == RA_RA1_ACT)  -> 
      d_step {
            c_code {  RA_RA1_t_u = 0 ; };
            RA_RA1_ACTcell = 1;
            RA_RA1_pstate =  RA_RA1_cstate ;
            RA_RA1_cstate =  RA_RA1_IDLE ;
            RA_RA1_force_init_update = false;
            RA_RA1_tick = true;
      };
      do
       :: RA_RA1_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double RA1_CS_t_u, RA1_CS_t_slope, RA1_CS_t_init, RA1_CS_t =  0 ;};
c_track "&RA1_CS_t" "sizeof(double)";
c_track "&RA1_CS_t_slope" "sizeof(double)";
c_track "&RA1_CS_t_u" "sizeof(double)";
c_track "&RA1_CS_t_init" "sizeof(double)";


bool RA1_CS_ACTcell = 0 ;
bool RA1_CS_ACTpath ;
mtype { RA1_CS_IDLE, RA1_CS_ACT };
proctype RA1_CS () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! RA1_CS_ACTpath)  &&  RA1_CS_cstate == RA1_CS_IDLE) ->
     if
      :: RA1_CS_pstate != RA1_CS_cstate || RA1_CS_force_init_update ->
       c_code { RA1_CS_t_init = RA1_CS_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RA1_CS_t_slope =  0 ;
       RA1_CS_t_u = (RA1_CS_t_slope * d) + RA1_CS_t ;
       
       
       
      };
      RA1_CS_pstate = RA1_CS_cstate;
      RA1_CS_cstate = RA1_CS_IDLE;
      RA1_CS_force_init_update = false;
      RA1_CS_ACTcell  = false;
      RA1_CS_ACTpath  = false;
      RA1_CS_tick = true;
     };
     do
      :: RA1_CS_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (RA1_CS_ACTpath && 
          RA1_CS_cstate == RA1_CS_IDLE)  -> 
      d_step {
            c_code {  RA1_CS_t_u = 0 ; };
            RA1_CS_pstate =  RA1_CS_cstate ;
            RA1_CS_cstate =  RA1_CS_ACT ;
            RA1_CS_force_init_update = false;
            RA1_CS_tick = true;
      };
      do
       :: RA1_CS_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (RA1_CS_t <= 20.0) }  &&  (! RA1_CS_ACTpath)  &&  RA1_CS_cstate == RA1_CS_ACT) ->
     if
      :: RA1_CS_pstate != RA1_CS_cstate || RA1_CS_force_init_update ->
       c_code { RA1_CS_t_init = RA1_CS_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RA1_CS_t_slope =  1 ;
       RA1_CS_t_u = (RA1_CS_t_slope * d) + RA1_CS_t ;
       
       
       
      };
      RA1_CS_pstate = RA1_CS_cstate;
      RA1_CS_cstate = RA1_CS_ACT;
      RA1_CS_force_init_update = false;
      RA1_CS_ACTcell  = false;
      RA1_CS_ACTpath  = false;
      RA1_CS_tick = true;
     };
     do
      :: RA1_CS_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (RA1_CS_t > 20.0) }
           && 
          RA1_CS_cstate == RA1_CS_ACT)  -> 
      d_step {
            c_code {  RA1_CS_t_u = 0 ; };
            RA1_CS_ACTcell = 1;
            RA1_CS_pstate =  RA1_CS_cstate ;
            RA1_CS_cstate =  RA1_CS_IDLE ;
            RA1_CS_force_init_update = false;
            RA1_CS_tick = true;
      };
      do
       :: RA1_CS_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double CT_CT1_t_u, CT_CT1_t_slope, CT_CT1_t_init, CT_CT1_t =  0 ;};
c_track "&CT_CT1_t" "sizeof(double)";
c_track "&CT_CT1_t_slope" "sizeof(double)";
c_track "&CT_CT1_t_u" "sizeof(double)";
c_track "&CT_CT1_t_init" "sizeof(double)";


bool CT_CT1_ACTcell = 0 ;
bool CT_CT1_ACTpath ;
mtype { CT_CT1_IDLE, CT_CT1_ACT };
proctype CT_CT1 () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! CT_CT1_ACTpath)  &&  CT_CT1_cstate == CT_CT1_IDLE) ->
     if
      :: CT_CT1_pstate != CT_CT1_cstate || CT_CT1_force_init_update ->
       c_code { CT_CT1_t_init = CT_CT1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       CT_CT1_t_slope =  0 ;
       CT_CT1_t_u = (CT_CT1_t_slope * d) + CT_CT1_t ;
       
       
       
      };
      CT_CT1_pstate = CT_CT1_cstate;
      CT_CT1_cstate = CT_CT1_IDLE;
      CT_CT1_force_init_update = false;
      CT_CT1_ACTcell  = false;
      CT_CT1_ACTpath  = false;
      CT_CT1_tick = true;
     };
     do
      :: CT_CT1_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (CT_CT1_ACTpath && 
          CT_CT1_cstate == CT_CT1_IDLE)  -> 
      d_step {
            c_code {  CT_CT1_t_u = 0 ; };
            CT_CT1_pstate =  CT_CT1_cstate ;
            CT_CT1_cstate =  CT_CT1_ACT ;
            CT_CT1_force_init_update = false;
            CT_CT1_tick = true;
      };
      do
       :: CT_CT1_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (CT_CT1_t <= 20.0) }  &&  (! CT_CT1_ACTpath)  &&  CT_CT1_cstate == CT_CT1_ACT) ->
     if
      :: CT_CT1_pstate != CT_CT1_cstate || CT_CT1_force_init_update ->
       c_code { CT_CT1_t_init = CT_CT1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       CT_CT1_t_slope =  1 ;
       CT_CT1_t_u = (CT_CT1_t_slope * d) + CT_CT1_t ;
       
       
       
      };
      CT_CT1_pstate = CT_CT1_cstate;
      CT_CT1_cstate = CT_CT1_ACT;
      CT_CT1_force_init_update = false;
      CT_CT1_ACTcell  = false;
      CT_CT1_ACTpath  = false;
      CT_CT1_tick = true;
     };
     do
      :: CT_CT1_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (CT_CT1_t > 20.0) }
           && 
          CT_CT1_cstate == CT_CT1_ACT)  -> 
      d_step {
            c_code {  CT_CT1_t_u = 0 ; };
            CT_CT1_ACTcell = 1;
            CT_CT1_pstate =  CT_CT1_cstate ;
            CT_CT1_cstate =  CT_CT1_IDLE ;
            CT_CT1_force_init_update = false;
            CT_CT1_tick = true;
      };
      do
       :: CT_CT1_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double OS_Fast_t_u, OS_Fast_t_slope, OS_Fast_t_init, OS_Fast_t =  0 ;};
c_track "&OS_Fast_t" "sizeof(double)";
c_track "&OS_Fast_t_slope" "sizeof(double)";
c_track "&OS_Fast_t_u" "sizeof(double)";
c_track "&OS_Fast_t_init" "sizeof(double)";


bool OS_Fast_ACTcell = 0 ;
bool OS_Fast_ACTpath ;
mtype { OS_Fast_IDLE, OS_Fast_ACT };
proctype OS_Fast () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! OS_Fast_ACTpath)  &&  OS_Fast_cstate == OS_Fast_IDLE) ->
     if
      :: OS_Fast_pstate != OS_Fast_cstate || OS_Fast_force_init_update ->
       c_code { OS_Fast_t_init = OS_Fast_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       OS_Fast_t_slope =  0 ;
       OS_Fast_t_u = (OS_Fast_t_slope * d) + OS_Fast_t ;
       
       
       
      };
      OS_Fast_pstate = OS_Fast_cstate;
      OS_Fast_cstate = OS_Fast_IDLE;
      OS_Fast_force_init_update = false;
      OS_Fast_ACTcell  = false;
      OS_Fast_ACTpath  = false;
      OS_Fast_tick = true;
     };
     do
      :: OS_Fast_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (OS_Fast_ACTpath && 
          OS_Fast_cstate == OS_Fast_IDLE)  -> 
      d_step {
            c_code {  OS_Fast_t_u = 0 ; };
            OS_Fast_pstate =  OS_Fast_cstate ;
            OS_Fast_cstate =  OS_Fast_ACT ;
            OS_Fast_force_init_update = false;
            OS_Fast_tick = true;
      };
      do
       :: OS_Fast_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (OS_Fast_t <= 20.0) }  &&  (! OS_Fast_ACTpath)  &&  OS_Fast_cstate == OS_Fast_ACT) ->
     if
      :: OS_Fast_pstate != OS_Fast_cstate || OS_Fast_force_init_update ->
       c_code { OS_Fast_t_init = OS_Fast_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       OS_Fast_t_slope =  1 ;
       OS_Fast_t_u = (OS_Fast_t_slope * d) + OS_Fast_t ;
       
       
       
      };
      OS_Fast_pstate = OS_Fast_cstate;
      OS_Fast_cstate = OS_Fast_ACT;
      OS_Fast_force_init_update = false;
      OS_Fast_ACTcell  = false;
      OS_Fast_ACTpath  = false;
      OS_Fast_tick = true;
     };
     do
      :: OS_Fast_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (OS_Fast_t > 20.0) }
           && 
          OS_Fast_cstate == OS_Fast_ACT)  -> 
      d_step {
            c_code {  OS_Fast_t_u = 0 ; };
            OS_Fast_ACTcell = 1;
            OS_Fast_pstate =  OS_Fast_cstate ;
            OS_Fast_cstate =  OS_Fast_IDLE ;
            OS_Fast_force_init_update = false;
            OS_Fast_tick = true;
      };
      do
       :: OS_Fast_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double Fast_Fast1_t_u, Fast_Fast1_t_slope, Fast_Fast1_t_init, Fast_Fast1_t =  0 ;};
c_track "&Fast_Fast1_t" "sizeof(double)";
c_track "&Fast_Fast1_t_slope" "sizeof(double)";
c_track "&Fast_Fast1_t_u" "sizeof(double)";
c_track "&Fast_Fast1_t_init" "sizeof(double)";


bool Fast_Fast1_ACTcell = 0 ;
bool Fast_Fast1_ACTpath ;
mtype { Fast_Fast1_IDLE, Fast_Fast1_ACT };
proctype Fast_Fast1 () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! Fast_Fast1_ACTpath)  &&  Fast_Fast1_cstate == Fast_Fast1_IDLE) ->
     if
      :: Fast_Fast1_pstate != Fast_Fast1_cstate || Fast_Fast1_force_init_update ->
       c_code { Fast_Fast1_t_init = Fast_Fast1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Fast_Fast1_t_slope =  0 ;
       Fast_Fast1_t_u = (Fast_Fast1_t_slope * d) + Fast_Fast1_t ;
       
       
       
      };
      Fast_Fast1_pstate = Fast_Fast1_cstate;
      Fast_Fast1_cstate = Fast_Fast1_IDLE;
      Fast_Fast1_force_init_update = false;
      Fast_Fast1_ACTcell  = false;
      Fast_Fast1_ACTpath  = false;
      Fast_Fast1_tick = true;
     };
     do
      :: Fast_Fast1_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (Fast_Fast1_ACTpath && 
          Fast_Fast1_cstate == Fast_Fast1_IDLE)  -> 
      d_step {
            c_code {  Fast_Fast1_t_u = 0 ; };
            Fast_Fast1_pstate =  Fast_Fast1_cstate ;
            Fast_Fast1_cstate =  Fast_Fast1_ACT ;
            Fast_Fast1_force_init_update = false;
            Fast_Fast1_tick = true;
      };
      do
       :: Fast_Fast1_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (Fast_Fast1_t <= 20.0) }  &&  (! Fast_Fast1_ACTpath)  &&  Fast_Fast1_cstate == Fast_Fast1_ACT) ->
     if
      :: Fast_Fast1_pstate != Fast_Fast1_cstate || Fast_Fast1_force_init_update ->
       c_code { Fast_Fast1_t_init = Fast_Fast1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Fast_Fast1_t_slope =  1 ;
       Fast_Fast1_t_u = (Fast_Fast1_t_slope * d) + Fast_Fast1_t ;
       
       
       
      };
      Fast_Fast1_pstate = Fast_Fast1_cstate;
      Fast_Fast1_cstate = Fast_Fast1_ACT;
      Fast_Fast1_force_init_update = false;
      Fast_Fast1_ACTcell  = false;
      Fast_Fast1_ACTpath  = false;
      Fast_Fast1_tick = true;
     };
     do
      :: Fast_Fast1_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (Fast_Fast1_t > 20.0) }
           && 
          Fast_Fast1_cstate == Fast_Fast1_ACT)  -> 
      d_step {
            c_code {  Fast_Fast1_t_u = 0 ; };
            Fast_Fast1_ACTcell = 1;
            Fast_Fast1_pstate =  Fast_Fast1_cstate ;
            Fast_Fast1_cstate =  Fast_Fast1_IDLE ;
            Fast_Fast1_force_init_update = false;
            Fast_Fast1_tick = true;
      };
      do
       :: Fast_Fast1_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double OS_Slow_t_u, OS_Slow_t_slope, OS_Slow_t_init, OS_Slow_t =  0 ;};
c_track "&OS_Slow_t" "sizeof(double)";
c_track "&OS_Slow_t_slope" "sizeof(double)";
c_track "&OS_Slow_t_u" "sizeof(double)";
c_track "&OS_Slow_t_init" "sizeof(double)";


bool OS_Slow_ACTcell = 0 ;
bool OS_Slow_ACTpath ;
mtype { OS_Slow_IDLE, OS_Slow_ACT };
proctype OS_Slow () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! OS_Slow_ACTpath)  &&  OS_Slow_cstate == OS_Slow_IDLE) ->
     if
      :: OS_Slow_pstate != OS_Slow_cstate || OS_Slow_force_init_update ->
       c_code { OS_Slow_t_init = OS_Slow_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       OS_Slow_t_slope =  0 ;
       OS_Slow_t_u = (OS_Slow_t_slope * d) + OS_Slow_t ;
       
       
       
      };
      OS_Slow_pstate = OS_Slow_cstate;
      OS_Slow_cstate = OS_Slow_IDLE;
      OS_Slow_force_init_update = false;
      OS_Slow_ACTcell  = false;
      OS_Slow_ACTpath  = false;
      OS_Slow_tick = true;
     };
     do
      :: OS_Slow_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (OS_Slow_ACTpath && 
          OS_Slow_cstate == OS_Slow_IDLE)  -> 
      d_step {
            c_code {  OS_Slow_t_u = 0 ; };
            OS_Slow_pstate =  OS_Slow_cstate ;
            OS_Slow_cstate =  OS_Slow_ACT ;
            OS_Slow_force_init_update = false;
            OS_Slow_tick = true;
      };
      do
       :: OS_Slow_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (OS_Slow_t <= 30.0) }  &&  (! OS_Slow_ACTpath)  &&  OS_Slow_cstate == OS_Slow_ACT) ->
     if
      :: OS_Slow_pstate != OS_Slow_cstate || OS_Slow_force_init_update ->
       c_code { OS_Slow_t_init = OS_Slow_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       OS_Slow_t_slope =  1 ;
       OS_Slow_t_u = (OS_Slow_t_slope * d) + OS_Slow_t ;
       
       
       
      };
      OS_Slow_pstate = OS_Slow_cstate;
      OS_Slow_cstate = OS_Slow_ACT;
      OS_Slow_force_init_update = false;
      OS_Slow_ACTcell  = false;
      OS_Slow_ACTpath  = false;
      OS_Slow_tick = true;
     };
     do
      :: OS_Slow_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (OS_Slow_t > 30.0) }
           && 
          OS_Slow_cstate == OS_Slow_ACT)  -> 
      d_step {
            c_code {  OS_Slow_t_u = 0 ; };
            OS_Slow_ACTcell = 1;
            OS_Slow_pstate =  OS_Slow_cstate ;
            OS_Slow_cstate =  OS_Slow_IDLE ;
            OS_Slow_force_init_update = false;
            OS_Slow_tick = true;
      };
      do
       :: OS_Slow_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double Slow_Slow1_t_u, Slow_Slow1_t_slope, Slow_Slow1_t_init, Slow_Slow1_t =  0 ;};
c_track "&Slow_Slow1_t" "sizeof(double)";
c_track "&Slow_Slow1_t_slope" "sizeof(double)";
c_track "&Slow_Slow1_t_u" "sizeof(double)";
c_track "&Slow_Slow1_t_init" "sizeof(double)";


bool Slow_Slow1_ACTcell = 0 ;
bool Slow_Slow1_ACTpath ;
mtype { Slow_Slow1_IDLE, Slow_Slow1_ACT };
proctype Slow_Slow1 () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! Slow_Slow1_ACTpath)  &&  Slow_Slow1_cstate == Slow_Slow1_IDLE) ->
     if
      :: Slow_Slow1_pstate != Slow_Slow1_cstate || Slow_Slow1_force_init_update ->
       c_code { Slow_Slow1_t_init = Slow_Slow1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Slow_Slow1_t_slope =  0 ;
       Slow_Slow1_t_u = (Slow_Slow1_t_slope * d) + Slow_Slow1_t ;
       
       
       
      };
      Slow_Slow1_pstate = Slow_Slow1_cstate;
      Slow_Slow1_cstate = Slow_Slow1_IDLE;
      Slow_Slow1_force_init_update = false;
      Slow_Slow1_ACTcell  = false;
      Slow_Slow1_ACTpath  = false;
      Slow_Slow1_tick = true;
     };
     do
      :: Slow_Slow1_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (Slow_Slow1_ACTpath && 
          Slow_Slow1_cstate == Slow_Slow1_IDLE)  -> 
      d_step {
            c_code {  Slow_Slow1_t_u = 0 ; };
            Slow_Slow1_pstate =  Slow_Slow1_cstate ;
            Slow_Slow1_cstate =  Slow_Slow1_ACT ;
            Slow_Slow1_force_init_update = false;
            Slow_Slow1_tick = true;
      };
      do
       :: Slow_Slow1_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (Slow_Slow1_t <= 30.0) }  &&  (! Slow_Slow1_ACTpath)  &&  Slow_Slow1_cstate == Slow_Slow1_ACT) ->
     if
      :: Slow_Slow1_pstate != Slow_Slow1_cstate || Slow_Slow1_force_init_update ->
       c_code { Slow_Slow1_t_init = Slow_Slow1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Slow_Slow1_t_slope =  1 ;
       Slow_Slow1_t_u = (Slow_Slow1_t_slope * d) + Slow_Slow1_t ;
       
       
       
      };
      Slow_Slow1_pstate = Slow_Slow1_cstate;
      Slow_Slow1_cstate = Slow_Slow1_ACT;
      Slow_Slow1_force_init_update = false;
      Slow_Slow1_ACTcell  = false;
      Slow_Slow1_ACTpath  = false;
      Slow_Slow1_tick = true;
     };
     do
      :: Slow_Slow1_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (Slow_Slow1_t > 30.0) }
           && 
          Slow_Slow1_cstate == Slow_Slow1_ACT)  -> 
      d_step {
            c_code {  Slow_Slow1_t_u = 0 ; };
            Slow_Slow1_ACTcell = 1;
            Slow_Slow1_pstate =  Slow_Slow1_cstate ;
            Slow_Slow1_cstate =  Slow_Slow1_IDLE ;
            Slow_Slow1_force_init_update = false;
            Slow_Slow1_tick = true;
      };
      do
       :: Slow_Slow1_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double Fast1_AV_t_u, Fast1_AV_t_slope, Fast1_AV_t_init, Fast1_AV_t =  0 ;};
c_track "&Fast1_AV_t" "sizeof(double)";
c_track "&Fast1_AV_t_slope" "sizeof(double)";
c_track "&Fast1_AV_t_u" "sizeof(double)";
c_track "&Fast1_AV_t_init" "sizeof(double)";


bool Fast1_AV_ACTcell = 0 ;
bool Fast1_AV_ACTpath ;
mtype { Fast1_AV_IDLE, Fast1_AV_ACT };
proctype Fast1_AV () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! Fast1_AV_ACTpath)  &&  Fast1_AV_cstate == Fast1_AV_IDLE) ->
     if
      :: Fast1_AV_pstate != Fast1_AV_cstate || Fast1_AV_force_init_update ->
       c_code { Fast1_AV_t_init = Fast1_AV_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Fast1_AV_t_slope =  0 ;
       Fast1_AV_t_u = (Fast1_AV_t_slope * d) + Fast1_AV_t ;
       
       
       
      };
      Fast1_AV_pstate = Fast1_AV_cstate;
      Fast1_AV_cstate = Fast1_AV_IDLE;
      Fast1_AV_force_init_update = false;
      Fast1_AV_ACTcell  = false;
      Fast1_AV_ACTpath  = false;
      Fast1_AV_tick = true;
     };
     do
      :: Fast1_AV_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (Fast1_AV_ACTpath && 
          Fast1_AV_cstate == Fast1_AV_IDLE)  -> 
      d_step {
            c_code {  Fast1_AV_t_u = 0 ; };
            Fast1_AV_pstate =  Fast1_AV_cstate ;
            Fast1_AV_cstate =  Fast1_AV_ACT ;
            Fast1_AV_force_init_update = false;
            Fast1_AV_tick = true;
      };
      do
       :: Fast1_AV_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (Fast1_AV_t <= 10.0) }  &&  (! Fast1_AV_ACTpath)  &&  Fast1_AV_cstate == Fast1_AV_ACT) ->
     if
      :: Fast1_AV_pstate != Fast1_AV_cstate || Fast1_AV_force_init_update ->
       c_code { Fast1_AV_t_init = Fast1_AV_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Fast1_AV_t_slope =  1 ;
       Fast1_AV_t_u = (Fast1_AV_t_slope * d) + Fast1_AV_t ;
       
       
       
      };
      Fast1_AV_pstate = Fast1_AV_cstate;
      Fast1_AV_cstate = Fast1_AV_ACT;
      Fast1_AV_force_init_update = false;
      Fast1_AV_ACTcell  = false;
      Fast1_AV_ACTpath  = false;
      Fast1_AV_tick = true;
     };
     do
      :: Fast1_AV_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (Fast1_AV_t > 10.0) }
           && 
          Fast1_AV_cstate == Fast1_AV_ACT)  -> 
      d_step {
            c_code {  Fast1_AV_t_u = 0 ; };
            Fast1_AV_ACTcell = 1;
            Fast1_AV_pstate =  Fast1_AV_cstate ;
            Fast1_AV_cstate =  Fast1_AV_IDLE ;
            Fast1_AV_force_init_update = false;
            Fast1_AV_tick = true;
      };
      do
       :: Fast1_AV_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double Slow1_AV_t_u, Slow1_AV_t_slope, Slow1_AV_t_init, Slow1_AV_t =  0 ;};
c_track "&Slow1_AV_t" "sizeof(double)";
c_track "&Slow1_AV_t_slope" "sizeof(double)";
c_track "&Slow1_AV_t_u" "sizeof(double)";
c_track "&Slow1_AV_t_init" "sizeof(double)";


bool Slow1_AV_ACTcell = 0 ;
bool Slow1_AV_ACTpath ;
mtype { Slow1_AV_IDLE, Slow1_AV_ACT };
proctype Slow1_AV () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! Slow1_AV_ACTpath)  &&  Slow1_AV_cstate == Slow1_AV_IDLE) ->
     if
      :: Slow1_AV_pstate != Slow1_AV_cstate || Slow1_AV_force_init_update ->
       c_code { Slow1_AV_t_init = Slow1_AV_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Slow1_AV_t_slope =  0 ;
       Slow1_AV_t_u = (Slow1_AV_t_slope * d) + Slow1_AV_t ;
       
       
       
      };
      Slow1_AV_pstate = Slow1_AV_cstate;
      Slow1_AV_cstate = Slow1_AV_IDLE;
      Slow1_AV_force_init_update = false;
      Slow1_AV_ACTcell  = false;
      Slow1_AV_ACTpath  = false;
      Slow1_AV_tick = true;
     };
     do
      :: Slow1_AV_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (Slow1_AV_ACTpath && 
          Slow1_AV_cstate == Slow1_AV_IDLE)  -> 
      d_step {
            c_code {  Slow1_AV_t_u = 0 ; };
            Slow1_AV_pstate =  Slow1_AV_cstate ;
            Slow1_AV_cstate =  Slow1_AV_ACT ;
            Slow1_AV_force_init_update = false;
            Slow1_AV_tick = true;
      };
      do
       :: Slow1_AV_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (Slow1_AV_t <= 15.0) }  &&  (! Slow1_AV_ACTpath)  &&  Slow1_AV_cstate == Slow1_AV_ACT) ->
     if
      :: Slow1_AV_pstate != Slow1_AV_cstate || Slow1_AV_force_init_update ->
       c_code { Slow1_AV_t_init = Slow1_AV_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       Slow1_AV_t_slope =  1 ;
       Slow1_AV_t_u = (Slow1_AV_t_slope * d) + Slow1_AV_t ;
       
       
       
      };
      Slow1_AV_pstate = Slow1_AV_cstate;
      Slow1_AV_cstate = Slow1_AV_ACT;
      Slow1_AV_force_init_update = false;
      Slow1_AV_ACTcell  = false;
      Slow1_AV_ACTpath  = false;
      Slow1_AV_tick = true;
     };
     do
      :: Slow1_AV_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (Slow1_AV_t > 15.0) }
           && 
          Slow1_AV_cstate == Slow1_AV_ACT)  -> 
      d_step {
            c_code {  Slow1_AV_t_u = 0 ; };
            Slow1_AV_ACTcell = 1;
            Slow1_AV_pstate =  Slow1_AV_cstate ;
            Slow1_AV_cstate =  Slow1_AV_IDLE ;
            Slow1_AV_force_init_update = false;
            Slow1_AV_tick = true;
      };
      do
       :: Slow1_AV_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double AV_His_t_u, AV_His_t_slope, AV_His_t_init, AV_His_t =  0 ;};
c_track "&AV_His_t" "sizeof(double)";
c_track "&AV_His_t_slope" "sizeof(double)";
c_track "&AV_His_t_u" "sizeof(double)";
c_track "&AV_His_t_init" "sizeof(double)";


bool AV_His_ACTcell = 0 ;
bool AV_His_ACTpath ;
mtype { AV_His_IDLE, AV_His_ACT };
proctype AV_His () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! AV_His_ACTpath)  &&  AV_His_cstate == AV_His_IDLE) ->
     if
      :: AV_His_pstate != AV_His_cstate || AV_His_force_init_update ->
       c_code { AV_His_t_init = AV_His_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       AV_His_t_slope =  0 ;
       AV_His_t_u = (AV_His_t_slope * d) + AV_His_t ;
       
       
       
      };
      AV_His_pstate = AV_His_cstate;
      AV_His_cstate = AV_His_IDLE;
      AV_His_force_init_update = false;
      AV_His_ACTcell  = false;
      AV_His_ACTpath  = false;
      AV_His_tick = true;
     };
     do
      :: AV_His_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (AV_His_ACTpath && 
          AV_His_cstate == AV_His_IDLE)  -> 
      d_step {
            c_code {  AV_His_t_u = 0 ; };
            AV_His_pstate =  AV_His_cstate ;
            AV_His_cstate =  AV_His_ACT ;
            AV_His_force_init_update = false;
            AV_His_tick = true;
      };
      do
       :: AV_His_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (AV_His_t <= 30.0) }  &&  (! AV_His_ACTpath)  &&  AV_His_cstate == AV_His_ACT) ->
     if
      :: AV_His_pstate != AV_His_cstate || AV_His_force_init_update ->
       c_code { AV_His_t_init = AV_His_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       AV_His_t_slope =  1 ;
       AV_His_t_u = (AV_His_t_slope * d) + AV_His_t ;
       
       
       
      };
      AV_His_pstate = AV_His_cstate;
      AV_His_cstate = AV_His_ACT;
      AV_His_force_init_update = false;
      AV_His_ACTcell  = false;
      AV_His_ACTpath  = false;
      AV_His_tick = true;
     };
     do
      :: AV_His_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (AV_His_t > 30.0) }
           && 
          AV_His_cstate == AV_His_ACT)  -> 
      d_step {
            c_code {  AV_His_t_u = 0 ; };
            AV_His_ACTcell = 1;
            AV_His_pstate =  AV_His_cstate ;
            AV_His_cstate =  AV_His_IDLE ;
            AV_His_force_init_update = false;
            AV_His_tick = true;
      };
      do
       :: AV_His_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double His_His1_t_u, His_His1_t_slope, His_His1_t_init, His_His1_t =  0 ;};
c_track "&His_His1_t" "sizeof(double)";
c_track "&His_His1_t_slope" "sizeof(double)";
c_track "&His_His1_t_u" "sizeof(double)";
c_track "&His_His1_t_init" "sizeof(double)";


bool His_His1_ACTcell = 0 ;
bool His_His1_ACTpath ;
mtype { His_His1_IDLE, His_His1_ACT };
proctype His_His1 () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! His_His1_ACTpath)  &&  His_His1_cstate == His_His1_IDLE) ->
     if
      :: His_His1_pstate != His_His1_cstate || His_His1_force_init_update ->
       c_code { His_His1_t_init = His_His1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       His_His1_t_slope =  0 ;
       His_His1_t_u = (His_His1_t_slope * d) + His_His1_t ;
       
       
       
      };
      His_His1_pstate = His_His1_cstate;
      His_His1_cstate = His_His1_IDLE;
      His_His1_force_init_update = false;
      His_His1_ACTcell  = false;
      His_His1_ACTpath  = false;
      His_His1_tick = true;
     };
     do
      :: His_His1_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (His_His1_ACTpath && 
          His_His1_cstate == His_His1_IDLE)  -> 
      d_step {
            c_code {  His_His1_t_u = 0 ; };
            His_His1_pstate =  His_His1_cstate ;
            His_His1_cstate =  His_His1_ACT ;
            His_His1_force_init_update = false;
            His_His1_tick = true;
      };
      do
       :: His_His1_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (His_His1_t <= 20.0) }  &&  (! His_His1_ACTpath)  &&  His_His1_cstate == His_His1_ACT) ->
     if
      :: His_His1_pstate != His_His1_cstate || His_His1_force_init_update ->
       c_code { His_His1_t_init = His_His1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       His_His1_t_slope =  1 ;
       His_His1_t_u = (His_His1_t_slope * d) + His_His1_t ;
       
       
       
      };
      His_His1_pstate = His_His1_cstate;
      His_His1_cstate = His_His1_ACT;
      His_His1_force_init_update = false;
      His_His1_ACTcell  = false;
      His_His1_ACTpath  = false;
      His_His1_tick = true;
     };
     do
      :: His_His1_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (His_His1_t > 20.0) }
           && 
          His_His1_cstate == His_His1_ACT)  -> 
      d_step {
            c_code {  His_His1_t_u = 0 ; };
            His_His1_ACTcell = 1;
            His_His1_pstate =  His_His1_cstate ;
            His_His1_cstate =  His_His1_IDLE ;
            His_His1_force_init_update = false;
            His_His1_tick = true;
      };
      do
       :: His_His1_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double His1_His2_t_u, His1_His2_t_slope, His1_His2_t_init, His1_His2_t =  0 ;};
c_track "&His1_His2_t" "sizeof(double)";
c_track "&His1_His2_t_slope" "sizeof(double)";
c_track "&His1_His2_t_u" "sizeof(double)";
c_track "&His1_His2_t_init" "sizeof(double)";


bool His1_His2_ACTcell = 0 ;
bool His1_His2_ACTpath ;
mtype { His1_His2_IDLE, His1_His2_ACT };
proctype His1_His2 () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! His1_His2_ACTpath)  &&  His1_His2_cstate == His1_His2_IDLE) ->
     if
      :: His1_His2_pstate != His1_His2_cstate || His1_His2_force_init_update ->
       c_code { His1_His2_t_init = His1_His2_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       His1_His2_t_slope =  0 ;
       His1_His2_t_u = (His1_His2_t_slope * d) + His1_His2_t ;
       
       
       
      };
      His1_His2_pstate = His1_His2_cstate;
      His1_His2_cstate = His1_His2_IDLE;
      His1_His2_force_init_update = false;
      His1_His2_ACTcell  = false;
      His1_His2_ACTpath  = false;
      His1_His2_tick = true;
     };
     do
      :: His1_His2_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (His1_His2_ACTpath && 
          His1_His2_cstate == His1_His2_IDLE)  -> 
      d_step {
            c_code {  His1_His2_t_u = 0 ; };
            His1_His2_pstate =  His1_His2_cstate ;
            His1_His2_cstate =  His1_His2_ACT ;
            His1_His2_force_init_update = false;
            His1_His2_tick = true;
      };
      do
       :: His1_His2_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (His1_His2_t <= 20.0) }  &&  (! His1_His2_ACTpath)  &&  His1_His2_cstate == His1_His2_ACT) ->
     if
      :: His1_His2_pstate != His1_His2_cstate || His1_His2_force_init_update ->
       c_code { His1_His2_t_init = His1_His2_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       His1_His2_t_slope =  1 ;
       His1_His2_t_u = (His1_His2_t_slope * d) + His1_His2_t ;
       
       
       
      };
      His1_His2_pstate = His1_His2_cstate;
      His1_His2_cstate = His1_His2_ACT;
      His1_His2_force_init_update = false;
      His1_His2_ACTcell  = false;
      His1_His2_ACTpath  = false;
      His1_His2_tick = true;
     };
     do
      :: His1_His2_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (His1_His2_t > 20.0) }
           && 
          His1_His2_cstate == His1_His2_ACT)  -> 
      d_step {
            c_code {  His1_His2_t_u = 0 ; };
            His1_His2_ACTcell = 1;
            His1_His2_pstate =  His1_His2_cstate ;
            His1_His2_cstate =  His1_His2_IDLE ;
            His1_His2_force_init_update = false;
            His1_His2_tick = true;
      };
      do
       :: His1_His2_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double His2_LBB_t_u, His2_LBB_t_slope, His2_LBB_t_init, His2_LBB_t =  0 ;};
c_track "&His2_LBB_t" "sizeof(double)";
c_track "&His2_LBB_t_slope" "sizeof(double)";
c_track "&His2_LBB_t_u" "sizeof(double)";
c_track "&His2_LBB_t_init" "sizeof(double)";


bool His2_LBB_ACTcell = 0 ;
bool His2_LBB_ACTpath ;
mtype { His2_LBB_IDLE, His2_LBB_ACT };
proctype His2_LBB () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! His2_LBB_ACTpath)  &&  His2_LBB_cstate == His2_LBB_IDLE) ->
     if
      :: His2_LBB_pstate != His2_LBB_cstate || His2_LBB_force_init_update ->
       c_code { His2_LBB_t_init = His2_LBB_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       His2_LBB_t_slope =  0 ;
       His2_LBB_t_u = (His2_LBB_t_slope * d) + His2_LBB_t ;
       
       
       
      };
      His2_LBB_pstate = His2_LBB_cstate;
      His2_LBB_cstate = His2_LBB_IDLE;
      His2_LBB_force_init_update = false;
      His2_LBB_ACTcell  = false;
      His2_LBB_ACTpath  = false;
      His2_LBB_tick = true;
     };
     do
      :: His2_LBB_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (His2_LBB_ACTpath && 
          His2_LBB_cstate == His2_LBB_IDLE)  -> 
      d_step {
            c_code {  His2_LBB_t_u = 0 ; };
            His2_LBB_pstate =  His2_LBB_cstate ;
            His2_LBB_cstate =  His2_LBB_ACT ;
            His2_LBB_force_init_update = false;
            His2_LBB_tick = true;
      };
      do
       :: His2_LBB_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (His2_LBB_t <= 20.0) }  &&  (! His2_LBB_ACTpath)  &&  His2_LBB_cstate == His2_LBB_ACT) ->
     if
      :: His2_LBB_pstate != His2_LBB_cstate || His2_LBB_force_init_update ->
       c_code { His2_LBB_t_init = His2_LBB_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       His2_LBB_t_slope =  1 ;
       His2_LBB_t_u = (His2_LBB_t_slope * d) + His2_LBB_t ;
       
       
       
      };
      His2_LBB_pstate = His2_LBB_cstate;
      His2_LBB_cstate = His2_LBB_ACT;
      His2_LBB_force_init_update = false;
      His2_LBB_ACTcell  = false;
      His2_LBB_ACTpath  = false;
      His2_LBB_tick = true;
     };
     do
      :: His2_LBB_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (His2_LBB_t > 20.0) }
           && 
          His2_LBB_cstate == His2_LBB_ACT)  -> 
      d_step {
            c_code {  His2_LBB_t_u = 0 ; };
            His2_LBB_ACTcell = 1;
            His2_LBB_pstate =  His2_LBB_cstate ;
            His2_LBB_cstate =  His2_LBB_IDLE ;
            His2_LBB_force_init_update = false;
            His2_LBB_tick = true;
      };
      do
       :: His2_LBB_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double LBB_LBB1_t_u, LBB_LBB1_t_slope, LBB_LBB1_t_init, LBB_LBB1_t =  0 ;};
c_track "&LBB_LBB1_t" "sizeof(double)";
c_track "&LBB_LBB1_t_slope" "sizeof(double)";
c_track "&LBB_LBB1_t_u" "sizeof(double)";
c_track "&LBB_LBB1_t_init" "sizeof(double)";


bool LBB_LBB1_ACTcell = 0 ;
bool LBB_LBB1_ACTpath ;
mtype { LBB_LBB1_IDLE, LBB_LBB1_ACT };
proctype LBB_LBB1 () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! LBB_LBB1_ACTpath)  &&  LBB_LBB1_cstate == LBB_LBB1_IDLE) ->
     if
      :: LBB_LBB1_pstate != LBB_LBB1_cstate || LBB_LBB1_force_init_update ->
       c_code { LBB_LBB1_t_init = LBB_LBB1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LBB_LBB1_t_slope =  0 ;
       LBB_LBB1_t_u = (LBB_LBB1_t_slope * d) + LBB_LBB1_t ;
       
       
       
      };
      LBB_LBB1_pstate = LBB_LBB1_cstate;
      LBB_LBB1_cstate = LBB_LBB1_IDLE;
      LBB_LBB1_force_init_update = false;
      LBB_LBB1_ACTcell  = false;
      LBB_LBB1_ACTpath  = false;
      LBB_LBB1_tick = true;
     };
     do
      :: LBB_LBB1_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (LBB_LBB1_ACTpath && 
          LBB_LBB1_cstate == LBB_LBB1_IDLE)  -> 
      d_step {
            c_code {  LBB_LBB1_t_u = 0 ; };
            LBB_LBB1_pstate =  LBB_LBB1_cstate ;
            LBB_LBB1_cstate =  LBB_LBB1_ACT ;
            LBB_LBB1_force_init_update = false;
            LBB_LBB1_tick = true;
      };
      do
       :: LBB_LBB1_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (LBB_LBB1_t <= 5.0) }  &&  (! LBB_LBB1_ACTpath)  &&  LBB_LBB1_cstate == LBB_LBB1_ACT) ->
     if
      :: LBB_LBB1_pstate != LBB_LBB1_cstate || LBB_LBB1_force_init_update ->
       c_code { LBB_LBB1_t_init = LBB_LBB1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LBB_LBB1_t_slope =  1 ;
       LBB_LBB1_t_u = (LBB_LBB1_t_slope * d) + LBB_LBB1_t ;
       
       
       
      };
      LBB_LBB1_pstate = LBB_LBB1_cstate;
      LBB_LBB1_cstate = LBB_LBB1_ACT;
      LBB_LBB1_force_init_update = false;
      LBB_LBB1_ACTcell  = false;
      LBB_LBB1_ACTpath  = false;
      LBB_LBB1_tick = true;
     };
     do
      :: LBB_LBB1_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (LBB_LBB1_t > 5.0) }
           && 
          LBB_LBB1_cstate == LBB_LBB1_ACT)  -> 
      d_step {
            c_code {  LBB_LBB1_t_u = 0 ; };
            LBB_LBB1_ACTcell = 1;
            LBB_LBB1_pstate =  LBB_LBB1_cstate ;
            LBB_LBB1_cstate =  LBB_LBB1_IDLE ;
            LBB_LBB1_force_init_update = false;
            LBB_LBB1_tick = true;
      };
      do
       :: LBB_LBB1_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double LBB1_LVA_t_u, LBB1_LVA_t_slope, LBB1_LVA_t_init, LBB1_LVA_t =  0 ;};
c_track "&LBB1_LVA_t" "sizeof(double)";
c_track "&LBB1_LVA_t_slope" "sizeof(double)";
c_track "&LBB1_LVA_t_u" "sizeof(double)";
c_track "&LBB1_LVA_t_init" "sizeof(double)";


bool LBB1_LVA_ACTcell = 0 ;
bool LBB1_LVA_ACTpath ;
mtype { LBB1_LVA_IDLE, LBB1_LVA_ACT };
proctype LBB1_LVA () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! LBB1_LVA_ACTpath)  &&  LBB1_LVA_cstate == LBB1_LVA_IDLE) ->
     if
      :: LBB1_LVA_pstate != LBB1_LVA_cstate || LBB1_LVA_force_init_update ->
       c_code { LBB1_LVA_t_init = LBB1_LVA_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LBB1_LVA_t_slope =  0 ;
       LBB1_LVA_t_u = (LBB1_LVA_t_slope * d) + LBB1_LVA_t ;
       
       
       
      };
      LBB1_LVA_pstate = LBB1_LVA_cstate;
      LBB1_LVA_cstate = LBB1_LVA_IDLE;
      LBB1_LVA_force_init_update = false;
      LBB1_LVA_ACTcell  = false;
      LBB1_LVA_ACTpath  = false;
      LBB1_LVA_tick = true;
     };
     do
      :: LBB1_LVA_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (LBB1_LVA_ACTpath && 
          LBB1_LVA_cstate == LBB1_LVA_IDLE)  -> 
      d_step {
            c_code {  LBB1_LVA_t_u = 0 ; };
            LBB1_LVA_pstate =  LBB1_LVA_cstate ;
            LBB1_LVA_cstate =  LBB1_LVA_ACT ;
            LBB1_LVA_force_init_update = false;
            LBB1_LVA_tick = true;
      };
      do
       :: LBB1_LVA_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (LBB1_LVA_t <= 5.0) }  &&  (! LBB1_LVA_ACTpath)  &&  LBB1_LVA_cstate == LBB1_LVA_ACT) ->
     if
      :: LBB1_LVA_pstate != LBB1_LVA_cstate || LBB1_LVA_force_init_update ->
       c_code { LBB1_LVA_t_init = LBB1_LVA_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LBB1_LVA_t_slope =  1 ;
       LBB1_LVA_t_u = (LBB1_LVA_t_slope * d) + LBB1_LVA_t ;
       
       
       
      };
      LBB1_LVA_pstate = LBB1_LVA_cstate;
      LBB1_LVA_cstate = LBB1_LVA_ACT;
      LBB1_LVA_force_init_update = false;
      LBB1_LVA_ACTcell  = false;
      LBB1_LVA_ACTpath  = false;
      LBB1_LVA_tick = true;
     };
     do
      :: LBB1_LVA_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (LBB1_LVA_t > 5.0) }
           && 
          LBB1_LVA_cstate == LBB1_LVA_ACT)  -> 
      d_step {
            c_code {  LBB1_LVA_t_u = 0 ; };
            LBB1_LVA_ACTcell = 1;
            LBB1_LVA_pstate =  LBB1_LVA_cstate ;
            LBB1_LVA_cstate =  LBB1_LVA_IDLE ;
            LBB1_LVA_force_init_update = false;
            LBB1_LVA_tick = true;
      };
      do
       :: LBB1_LVA_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double His2_RBB_t_u, His2_RBB_t_slope, His2_RBB_t_init, His2_RBB_t =  0 ;};
c_track "&His2_RBB_t" "sizeof(double)";
c_track "&His2_RBB_t_slope" "sizeof(double)";
c_track "&His2_RBB_t_u" "sizeof(double)";
c_track "&His2_RBB_t_init" "sizeof(double)";


bool His2_RBB_ACTcell = 0 ;
bool His2_RBB_ACTpath ;
mtype { His2_RBB_IDLE, His2_RBB_ACT };
proctype His2_RBB () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! His2_RBB_ACTpath)  &&  His2_RBB_cstate == His2_RBB_IDLE) ->
     if
      :: His2_RBB_pstate != His2_RBB_cstate || His2_RBB_force_init_update ->
       c_code { His2_RBB_t_init = His2_RBB_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       His2_RBB_t_slope =  0 ;
       His2_RBB_t_u = (His2_RBB_t_slope * d) + His2_RBB_t ;
       
       
       
      };
      His2_RBB_pstate = His2_RBB_cstate;
      His2_RBB_cstate = His2_RBB_IDLE;
      His2_RBB_force_init_update = false;
      His2_RBB_ACTcell  = false;
      His2_RBB_ACTpath  = false;
      His2_RBB_tick = true;
     };
     do
      :: His2_RBB_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (His2_RBB_ACTpath && 
          His2_RBB_cstate == His2_RBB_IDLE)  -> 
      d_step {
            c_code {  His2_RBB_t_u = 0 ; };
            His2_RBB_pstate =  His2_RBB_cstate ;
            His2_RBB_cstate =  His2_RBB_ACT ;
            His2_RBB_force_init_update = false;
            His2_RBB_tick = true;
      };
      do
       :: His2_RBB_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (His2_RBB_t <= 20.0) }  &&  (! His2_RBB_ACTpath)  &&  His2_RBB_cstate == His2_RBB_ACT) ->
     if
      :: His2_RBB_pstate != His2_RBB_cstate || His2_RBB_force_init_update ->
       c_code { His2_RBB_t_init = His2_RBB_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       His2_RBB_t_slope =  1 ;
       His2_RBB_t_u = (His2_RBB_t_slope * d) + His2_RBB_t ;
       
       
       
      };
      His2_RBB_pstate = His2_RBB_cstate;
      His2_RBB_cstate = His2_RBB_ACT;
      His2_RBB_force_init_update = false;
      His2_RBB_ACTcell  = false;
      His2_RBB_ACTpath  = false;
      His2_RBB_tick = true;
     };
     do
      :: His2_RBB_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (His2_RBB_t > 20.0) }
           && 
          His2_RBB_cstate == His2_RBB_ACT)  -> 
      d_step {
            c_code {  His2_RBB_t_u = 0 ; };
            His2_RBB_ACTcell = 1;
            His2_RBB_pstate =  His2_RBB_cstate ;
            His2_RBB_cstate =  His2_RBB_IDLE ;
            His2_RBB_force_init_update = false;
            His2_RBB_tick = true;
      };
      do
       :: His2_RBB_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double RBB_RBB1_t_u, RBB_RBB1_t_slope, RBB_RBB1_t_init, RBB_RBB1_t =  0 ;};
c_track "&RBB_RBB1_t" "sizeof(double)";
c_track "&RBB_RBB1_t_slope" "sizeof(double)";
c_track "&RBB_RBB1_t_u" "sizeof(double)";
c_track "&RBB_RBB1_t_init" "sizeof(double)";


bool RBB_RBB1_ACTcell = 0 ;
bool RBB_RBB1_ACTpath ;
mtype { RBB_RBB1_IDLE, RBB_RBB1_ACT };
proctype RBB_RBB1 () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! RBB_RBB1_ACTpath)  &&  RBB_RBB1_cstate == RBB_RBB1_IDLE) ->
     if
      :: RBB_RBB1_pstate != RBB_RBB1_cstate || RBB_RBB1_force_init_update ->
       c_code { RBB_RBB1_t_init = RBB_RBB1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RBB_RBB1_t_slope =  0 ;
       RBB_RBB1_t_u = (RBB_RBB1_t_slope * d) + RBB_RBB1_t ;
       
       
       
      };
      RBB_RBB1_pstate = RBB_RBB1_cstate;
      RBB_RBB1_cstate = RBB_RBB1_IDLE;
      RBB_RBB1_force_init_update = false;
      RBB_RBB1_ACTcell  = false;
      RBB_RBB1_ACTpath  = false;
      RBB_RBB1_tick = true;
     };
     do
      :: RBB_RBB1_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (RBB_RBB1_ACTpath && 
          RBB_RBB1_cstate == RBB_RBB1_IDLE)  -> 
      d_step {
            c_code {  RBB_RBB1_t_u = 0 ; };
            RBB_RBB1_pstate =  RBB_RBB1_cstate ;
            RBB_RBB1_cstate =  RBB_RBB1_ACT ;
            RBB_RBB1_force_init_update = false;
            RBB_RBB1_tick = true;
      };
      do
       :: RBB_RBB1_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (RBB_RBB1_t <= 5.0) }  &&  (! RBB_RBB1_ACTpath)  &&  RBB_RBB1_cstate == RBB_RBB1_ACT) ->
     if
      :: RBB_RBB1_pstate != RBB_RBB1_cstate || RBB_RBB1_force_init_update ->
       c_code { RBB_RBB1_t_init = RBB_RBB1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RBB_RBB1_t_slope =  1 ;
       RBB_RBB1_t_u = (RBB_RBB1_t_slope * d) + RBB_RBB1_t ;
       
       
       
      };
      RBB_RBB1_pstate = RBB_RBB1_cstate;
      RBB_RBB1_cstate = RBB_RBB1_ACT;
      RBB_RBB1_force_init_update = false;
      RBB_RBB1_ACTcell  = false;
      RBB_RBB1_ACTpath  = false;
      RBB_RBB1_tick = true;
     };
     do
      :: RBB_RBB1_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (RBB_RBB1_t > 5.0) }
           && 
          RBB_RBB1_cstate == RBB_RBB1_ACT)  -> 
      d_step {
            c_code {  RBB_RBB1_t_u = 0 ; };
            RBB_RBB1_ACTcell = 1;
            RBB_RBB1_pstate =  RBB_RBB1_cstate ;
            RBB_RBB1_cstate =  RBB_RBB1_IDLE ;
            RBB_RBB1_force_init_update = false;
            RBB_RBB1_tick = true;
      };
      do
       :: RBB_RBB1_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double RBB1_RVA_t_u, RBB1_RVA_t_slope, RBB1_RVA_t_init, RBB1_RVA_t =  0 ;};
c_track "&RBB1_RVA_t" "sizeof(double)";
c_track "&RBB1_RVA_t_slope" "sizeof(double)";
c_track "&RBB1_RVA_t_u" "sizeof(double)";
c_track "&RBB1_RVA_t_init" "sizeof(double)";


bool RBB1_RVA_ACTcell = 0 ;
bool RBB1_RVA_ACTpath ;
mtype { RBB1_RVA_IDLE, RBB1_RVA_ACT };
proctype RBB1_RVA () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! RBB1_RVA_ACTpath)  &&  RBB1_RVA_cstate == RBB1_RVA_IDLE) ->
     if
      :: RBB1_RVA_pstate != RBB1_RVA_cstate || RBB1_RVA_force_init_update ->
       c_code { RBB1_RVA_t_init = RBB1_RVA_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RBB1_RVA_t_slope =  0 ;
       RBB1_RVA_t_u = (RBB1_RVA_t_slope * d) + RBB1_RVA_t ;
       
       
       
      };
      RBB1_RVA_pstate = RBB1_RVA_cstate;
      RBB1_RVA_cstate = RBB1_RVA_IDLE;
      RBB1_RVA_force_init_update = false;
      RBB1_RVA_ACTcell  = false;
      RBB1_RVA_ACTpath  = false;
      RBB1_RVA_tick = true;
     };
     do
      :: RBB1_RVA_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (RBB1_RVA_ACTpath && 
          RBB1_RVA_cstate == RBB1_RVA_IDLE)  -> 
      d_step {
            c_code {  RBB1_RVA_t_u = 0 ; };
            RBB1_RVA_pstate =  RBB1_RVA_cstate ;
            RBB1_RVA_cstate =  RBB1_RVA_ACT ;
            RBB1_RVA_force_init_update = false;
            RBB1_RVA_tick = true;
      };
      do
       :: RBB1_RVA_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (RBB1_RVA_t <= 5.0) }  &&  (! RBB1_RVA_ACTpath)  &&  RBB1_RVA_cstate == RBB1_RVA_ACT) ->
     if
      :: RBB1_RVA_pstate != RBB1_RVA_cstate || RBB1_RVA_force_init_update ->
       c_code { RBB1_RVA_t_init = RBB1_RVA_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RBB1_RVA_t_slope =  1 ;
       RBB1_RVA_t_u = (RBB1_RVA_t_slope * d) + RBB1_RVA_t ;
       
       
       
      };
      RBB1_RVA_pstate = RBB1_RVA_cstate;
      RBB1_RVA_cstate = RBB1_RVA_ACT;
      RBB1_RVA_force_init_update = false;
      RBB1_RVA_ACTcell  = false;
      RBB1_RVA_ACTpath  = false;
      RBB1_RVA_tick = true;
     };
     do
      :: RBB1_RVA_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (RBB1_RVA_t > 5.0) }
           && 
          RBB1_RVA_cstate == RBB1_RVA_ACT)  -> 
      d_step {
            c_code {  RBB1_RVA_t_u = 0 ; };
            RBB1_RVA_ACTcell = 1;
            RBB1_RVA_pstate =  RBB1_RVA_cstate ;
            RBB1_RVA_cstate =  RBB1_RVA_IDLE ;
            RBB1_RVA_force_init_update = false;
            RBB1_RVA_tick = true;
      };
      do
       :: RBB1_RVA_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double LVA_RVA_t_u, LVA_RVA_t_slope, LVA_RVA_t_init, LVA_RVA_t =  0 ;};
c_track "&LVA_RVA_t" "sizeof(double)";
c_track "&LVA_RVA_t_slope" "sizeof(double)";
c_track "&LVA_RVA_t_u" "sizeof(double)";
c_track "&LVA_RVA_t_init" "sizeof(double)";


bool LVA_RVA_ACTcell = 0 ;
bool LVA_RVA_ACTpath ;
mtype { LVA_RVA_IDLE, LVA_RVA_ACT };
proctype LVA_RVA () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! LVA_RVA_ACTpath)  &&  LVA_RVA_cstate == LVA_RVA_IDLE) ->
     if
      :: LVA_RVA_pstate != LVA_RVA_cstate || LVA_RVA_force_init_update ->
       c_code { LVA_RVA_t_init = LVA_RVA_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LVA_RVA_t_slope =  0 ;
       LVA_RVA_t_u = (LVA_RVA_t_slope * d) + LVA_RVA_t ;
       
       
       
      };
      LVA_RVA_pstate = LVA_RVA_cstate;
      LVA_RVA_cstate = LVA_RVA_IDLE;
      LVA_RVA_force_init_update = false;
      LVA_RVA_ACTcell  = false;
      LVA_RVA_ACTpath  = false;
      LVA_RVA_tick = true;
     };
     do
      :: LVA_RVA_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (LVA_RVA_ACTpath && 
          LVA_RVA_cstate == LVA_RVA_IDLE)  -> 
      d_step {
            c_code {  LVA_RVA_t_u = 0 ; };
            LVA_RVA_pstate =  LVA_RVA_cstate ;
            LVA_RVA_cstate =  LVA_RVA_ACT ;
            LVA_RVA_force_init_update = false;
            LVA_RVA_tick = true;
      };
      do
       :: LVA_RVA_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (LVA_RVA_t <= 5.0) }  &&  (! LVA_RVA_ACTpath)  &&  LVA_RVA_cstate == LVA_RVA_ACT) ->
     if
      :: LVA_RVA_pstate != LVA_RVA_cstate || LVA_RVA_force_init_update ->
       c_code { LVA_RVA_t_init = LVA_RVA_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LVA_RVA_t_slope =  1 ;
       LVA_RVA_t_u = (LVA_RVA_t_slope * d) + LVA_RVA_t ;
       
       
       
      };
      LVA_RVA_pstate = LVA_RVA_cstate;
      LVA_RVA_cstate = LVA_RVA_ACT;
      LVA_RVA_force_init_update = false;
      LVA_RVA_ACTcell  = false;
      LVA_RVA_ACTpath  = false;
      LVA_RVA_tick = true;
     };
     do
      :: LVA_RVA_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (LVA_RVA_t > 5.0) }
           && 
          LVA_RVA_cstate == LVA_RVA_ACT)  -> 
      d_step {
            c_code {  LVA_RVA_t_u = 0 ; };
            LVA_RVA_ACTcell = 1;
            LVA_RVA_pstate =  LVA_RVA_cstate ;
            LVA_RVA_cstate =  LVA_RVA_IDLE ;
            LVA_RVA_force_init_update = false;
            LVA_RVA_tick = true;
      };
      do
       :: LVA_RVA_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double LVA_LV_t_u, LVA_LV_t_slope, LVA_LV_t_init, LVA_LV_t =  0 ;};
c_track "&LVA_LV_t" "sizeof(double)";
c_track "&LVA_LV_t_slope" "sizeof(double)";
c_track "&LVA_LV_t_u" "sizeof(double)";
c_track "&LVA_LV_t_init" "sizeof(double)";


bool LVA_LV_ACTcell = 0 ;
bool LVA_LV_ACTpath ;
mtype { LVA_LV_IDLE, LVA_LV_ACT };
proctype LVA_LV () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! LVA_LV_ACTpath)  &&  LVA_LV_cstate == LVA_LV_IDLE) ->
     if
      :: LVA_LV_pstate != LVA_LV_cstate || LVA_LV_force_init_update ->
       c_code { LVA_LV_t_init = LVA_LV_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LVA_LV_t_slope =  0 ;
       LVA_LV_t_u = (LVA_LV_t_slope * d) + LVA_LV_t ;
       
       
       
      };
      LVA_LV_pstate = LVA_LV_cstate;
      LVA_LV_cstate = LVA_LV_IDLE;
      LVA_LV_force_init_update = false;
      LVA_LV_ACTcell  = false;
      LVA_LV_ACTpath  = false;
      LVA_LV_tick = true;
     };
     do
      :: LVA_LV_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (LVA_LV_ACTpath && 
          LVA_LV_cstate == LVA_LV_IDLE)  -> 
      d_step {
            c_code {  LVA_LV_t_u = 0 ; };
            LVA_LV_pstate =  LVA_LV_cstate ;
            LVA_LV_cstate =  LVA_LV_ACT ;
            LVA_LV_force_init_update = false;
            LVA_LV_tick = true;
      };
      do
       :: LVA_LV_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (LVA_LV_t <= 20.0) }  &&  (! LVA_LV_ACTpath)  &&  LVA_LV_cstate == LVA_LV_ACT) ->
     if
      :: LVA_LV_pstate != LVA_LV_cstate || LVA_LV_force_init_update ->
       c_code { LVA_LV_t_init = LVA_LV_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LVA_LV_t_slope =  1 ;
       LVA_LV_t_u = (LVA_LV_t_slope * d) + LVA_LV_t ;
       
       
       
      };
      LVA_LV_pstate = LVA_LV_cstate;
      LVA_LV_cstate = LVA_LV_ACT;
      LVA_LV_force_init_update = false;
      LVA_LV_ACTcell  = false;
      LVA_LV_ACTpath  = false;
      LVA_LV_tick = true;
     };
     do
      :: LVA_LV_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (LVA_LV_t > 20.0) }
           && 
          LVA_LV_cstate == LVA_LV_ACT)  -> 
      d_step {
            c_code {  LVA_LV_t_u = 0 ; };
            LVA_LV_ACTcell = 1;
            LVA_LV_pstate =  LVA_LV_cstate ;
            LVA_LV_cstate =  LVA_LV_IDLE ;
            LVA_LV_force_init_update = false;
            LVA_LV_tick = true;
      };
      do
       :: LVA_LV_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double LV_LV1_t_u, LV_LV1_t_slope, LV_LV1_t_init, LV_LV1_t =  0 ;};
c_track "&LV_LV1_t" "sizeof(double)";
c_track "&LV_LV1_t_slope" "sizeof(double)";
c_track "&LV_LV1_t_u" "sizeof(double)";
c_track "&LV_LV1_t_init" "sizeof(double)";


bool LV_LV1_ACTcell = 0 ;
bool LV_LV1_ACTpath ;
mtype { LV_LV1_IDLE, LV_LV1_ACT };
proctype LV_LV1 () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! LV_LV1_ACTpath)  &&  LV_LV1_cstate == LV_LV1_IDLE) ->
     if
      :: LV_LV1_pstate != LV_LV1_cstate || LV_LV1_force_init_update ->
       c_code { LV_LV1_t_init = LV_LV1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LV_LV1_t_slope =  0 ;
       LV_LV1_t_u = (LV_LV1_t_slope * d) + LV_LV1_t ;
       
       
       
      };
      LV_LV1_pstate = LV_LV1_cstate;
      LV_LV1_cstate = LV_LV1_IDLE;
      LV_LV1_force_init_update = false;
      LV_LV1_ACTcell  = false;
      LV_LV1_ACTpath  = false;
      LV_LV1_tick = true;
     };
     do
      :: LV_LV1_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (LV_LV1_ACTpath && 
          LV_LV1_cstate == LV_LV1_IDLE)  -> 
      d_step {
            c_code {  LV_LV1_t_u = 0 ; };
            LV_LV1_pstate =  LV_LV1_cstate ;
            LV_LV1_cstate =  LV_LV1_ACT ;
            LV_LV1_force_init_update = false;
            LV_LV1_tick = true;
      };
      do
       :: LV_LV1_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (LV_LV1_t <= 30.0) }  &&  (! LV_LV1_ACTpath)  &&  LV_LV1_cstate == LV_LV1_ACT) ->
     if
      :: LV_LV1_pstate != LV_LV1_cstate || LV_LV1_force_init_update ->
       c_code { LV_LV1_t_init = LV_LV1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LV_LV1_t_slope =  1 ;
       LV_LV1_t_u = (LV_LV1_t_slope * d) + LV_LV1_t ;
       
       
       
      };
      LV_LV1_pstate = LV_LV1_cstate;
      LV_LV1_cstate = LV_LV1_ACT;
      LV_LV1_force_init_update = false;
      LV_LV1_ACTcell  = false;
      LV_LV1_ACTpath  = false;
      LV_LV1_tick = true;
     };
     do
      :: LV_LV1_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (LV_LV1_t > 30.0) }
           && 
          LV_LV1_cstate == LV_LV1_ACT)  -> 
      d_step {
            c_code {  LV_LV1_t_u = 0 ; };
            LV_LV1_ACTcell = 1;
            LV_LV1_pstate =  LV_LV1_cstate ;
            LV_LV1_cstate =  LV_LV1_IDLE ;
            LV_LV1_force_init_update = false;
            LV_LV1_tick = true;
      };
      do
       :: LV_LV1_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double LVA_LVS_t_u, LVA_LVS_t_slope, LVA_LVS_t_init, LVA_LVS_t =  0 ;};
c_track "&LVA_LVS_t" "sizeof(double)";
c_track "&LVA_LVS_t_slope" "sizeof(double)";
c_track "&LVA_LVS_t_u" "sizeof(double)";
c_track "&LVA_LVS_t_init" "sizeof(double)";


bool LVA_LVS_ACTcell = 0 ;
bool LVA_LVS_ACTpath ;
mtype { LVA_LVS_IDLE, LVA_LVS_ACT };
proctype LVA_LVS () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! LVA_LVS_ACTpath)  &&  LVA_LVS_cstate == LVA_LVS_IDLE) ->
     if
      :: LVA_LVS_pstate != LVA_LVS_cstate || LVA_LVS_force_init_update ->
       c_code { LVA_LVS_t_init = LVA_LVS_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LVA_LVS_t_slope =  0 ;
       LVA_LVS_t_u = (LVA_LVS_t_slope * d) + LVA_LVS_t ;
       
       
       
      };
      LVA_LVS_pstate = LVA_LVS_cstate;
      LVA_LVS_cstate = LVA_LVS_IDLE;
      LVA_LVS_force_init_update = false;
      LVA_LVS_ACTcell  = false;
      LVA_LVS_ACTpath  = false;
      LVA_LVS_tick = true;
     };
     do
      :: LVA_LVS_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (LVA_LVS_ACTpath && 
          LVA_LVS_cstate == LVA_LVS_IDLE)  -> 
      d_step {
            c_code {  LVA_LVS_t_u = 0 ; };
            LVA_LVS_pstate =  LVA_LVS_cstate ;
            LVA_LVS_cstate =  LVA_LVS_ACT ;
            LVA_LVS_force_init_update = false;
            LVA_LVS_tick = true;
      };
      do
       :: LVA_LVS_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (LVA_LVS_t <= 15.0) }  &&  (! LVA_LVS_ACTpath)  &&  LVA_LVS_cstate == LVA_LVS_ACT) ->
     if
      :: LVA_LVS_pstate != LVA_LVS_cstate || LVA_LVS_force_init_update ->
       c_code { LVA_LVS_t_init = LVA_LVS_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LVA_LVS_t_slope =  1 ;
       LVA_LVS_t_u = (LVA_LVS_t_slope * d) + LVA_LVS_t ;
       
       
       
      };
      LVA_LVS_pstate = LVA_LVS_cstate;
      LVA_LVS_cstate = LVA_LVS_ACT;
      LVA_LVS_force_init_update = false;
      LVA_LVS_ACTcell  = false;
      LVA_LVS_ACTpath  = false;
      LVA_LVS_tick = true;
     };
     do
      :: LVA_LVS_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (LVA_LVS_t > 15.0) }
           && 
          LVA_LVS_cstate == LVA_LVS_ACT)  -> 
      d_step {
            c_code {  LVA_LVS_t_u = 0 ; };
            LVA_LVS_ACTcell = 1;
            LVA_LVS_pstate =  LVA_LVS_cstate ;
            LVA_LVS_cstate =  LVA_LVS_IDLE ;
            LVA_LVS_force_init_update = false;
            LVA_LVS_tick = true;
      };
      do
       :: LVA_LVS_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double LVS_LVS1_t_u, LVS_LVS1_t_slope, LVS_LVS1_t_init, LVS_LVS1_t =  0 ;};
c_track "&LVS_LVS1_t" "sizeof(double)";
c_track "&LVS_LVS1_t_slope" "sizeof(double)";
c_track "&LVS_LVS1_t_u" "sizeof(double)";
c_track "&LVS_LVS1_t_init" "sizeof(double)";


bool LVS_LVS1_ACTcell = 0 ;
bool LVS_LVS1_ACTpath ;
mtype { LVS_LVS1_IDLE, LVS_LVS1_ACT };
proctype LVS_LVS1 () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! LVS_LVS1_ACTpath)  &&  LVS_LVS1_cstate == LVS_LVS1_IDLE) ->
     if
      :: LVS_LVS1_pstate != LVS_LVS1_cstate || LVS_LVS1_force_init_update ->
       c_code { LVS_LVS1_t_init = LVS_LVS1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LVS_LVS1_t_slope =  0 ;
       LVS_LVS1_t_u = (LVS_LVS1_t_slope * d) + LVS_LVS1_t ;
       
       
       
      };
      LVS_LVS1_pstate = LVS_LVS1_cstate;
      LVS_LVS1_cstate = LVS_LVS1_IDLE;
      LVS_LVS1_force_init_update = false;
      LVS_LVS1_ACTcell  = false;
      LVS_LVS1_ACTpath  = false;
      LVS_LVS1_tick = true;
     };
     do
      :: LVS_LVS1_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (LVS_LVS1_ACTpath && 
          LVS_LVS1_cstate == LVS_LVS1_IDLE)  -> 
      d_step {
            c_code {  LVS_LVS1_t_u = 0 ; };
            LVS_LVS1_pstate =  LVS_LVS1_cstate ;
            LVS_LVS1_cstate =  LVS_LVS1_ACT ;
            LVS_LVS1_force_init_update = false;
            LVS_LVS1_tick = true;
      };
      do
       :: LVS_LVS1_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (LVS_LVS1_t <= 15.0) }  &&  (! LVS_LVS1_ACTpath)  &&  LVS_LVS1_cstate == LVS_LVS1_ACT) ->
     if
      :: LVS_LVS1_pstate != LVS_LVS1_cstate || LVS_LVS1_force_init_update ->
       c_code { LVS_LVS1_t_init = LVS_LVS1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LVS_LVS1_t_slope =  1 ;
       LVS_LVS1_t_u = (LVS_LVS1_t_slope * d) + LVS_LVS1_t ;
       
       
       
      };
      LVS_LVS1_pstate = LVS_LVS1_cstate;
      LVS_LVS1_cstate = LVS_LVS1_ACT;
      LVS_LVS1_force_init_update = false;
      LVS_LVS1_ACTcell  = false;
      LVS_LVS1_ACTpath  = false;
      LVS_LVS1_tick = true;
     };
     do
      :: LVS_LVS1_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (LVS_LVS1_t > 15.0) }
           && 
          LVS_LVS1_cstate == LVS_LVS1_ACT)  -> 
      d_step {
            c_code {  LVS_LVS1_t_u = 0 ; };
            LVS_LVS1_ACTcell = 1;
            LVS_LVS1_pstate =  LVS_LVS1_cstate ;
            LVS_LVS1_cstate =  LVS_LVS1_IDLE ;
            LVS_LVS1_force_init_update = false;
            LVS_LVS1_tick = true;
      };
      do
       :: LVS_LVS1_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double LVS1_CSLV_t_u, LVS1_CSLV_t_slope, LVS1_CSLV_t_init, LVS1_CSLV_t =  0 ;};
c_track "&LVS1_CSLV_t" "sizeof(double)";
c_track "&LVS1_CSLV_t_slope" "sizeof(double)";
c_track "&LVS1_CSLV_t_u" "sizeof(double)";
c_track "&LVS1_CSLV_t_init" "sizeof(double)";


bool LVS1_CSLV_ACTcell = 0 ;
bool LVS1_CSLV_ACTpath ;
mtype { LVS1_CSLV_IDLE, LVS1_CSLV_ACT };
proctype LVS1_CSLV () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! LVS1_CSLV_ACTpath)  &&  LVS1_CSLV_cstate == LVS1_CSLV_IDLE) ->
     if
      :: LVS1_CSLV_pstate != LVS1_CSLV_cstate || LVS1_CSLV_force_init_update ->
       c_code { LVS1_CSLV_t_init = LVS1_CSLV_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LVS1_CSLV_t_slope =  0 ;
       LVS1_CSLV_t_u = (LVS1_CSLV_t_slope * d) + LVS1_CSLV_t ;
       
       
       
      };
      LVS1_CSLV_pstate = LVS1_CSLV_cstate;
      LVS1_CSLV_cstate = LVS1_CSLV_IDLE;
      LVS1_CSLV_force_init_update = false;
      LVS1_CSLV_ACTcell  = false;
      LVS1_CSLV_ACTpath  = false;
      LVS1_CSLV_tick = true;
     };
     do
      :: LVS1_CSLV_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (LVS1_CSLV_ACTpath && 
          LVS1_CSLV_cstate == LVS1_CSLV_IDLE)  -> 
      d_step {
            c_code {  LVS1_CSLV_t_u = 0 ; };
            LVS1_CSLV_pstate =  LVS1_CSLV_cstate ;
            LVS1_CSLV_cstate =  LVS1_CSLV_ACT ;
            LVS1_CSLV_force_init_update = false;
            LVS1_CSLV_tick = true;
      };
      do
       :: LVS1_CSLV_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (LVS1_CSLV_t <= 20.0) }  &&  (! LVS1_CSLV_ACTpath)  &&  LVS1_CSLV_cstate == LVS1_CSLV_ACT) ->
     if
      :: LVS1_CSLV_pstate != LVS1_CSLV_cstate || LVS1_CSLV_force_init_update ->
       c_code { LVS1_CSLV_t_init = LVS1_CSLV_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       LVS1_CSLV_t_slope =  1 ;
       LVS1_CSLV_t_u = (LVS1_CSLV_t_slope * d) + LVS1_CSLV_t ;
       
       
       
      };
      LVS1_CSLV_pstate = LVS1_CSLV_cstate;
      LVS1_CSLV_cstate = LVS1_CSLV_ACT;
      LVS1_CSLV_force_init_update = false;
      LVS1_CSLV_ACTcell  = false;
      LVS1_CSLV_ACTpath  = false;
      LVS1_CSLV_tick = true;
     };
     do
      :: LVS1_CSLV_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (LVS1_CSLV_t > 20.0) }
           && 
          LVS1_CSLV_cstate == LVS1_CSLV_ACT)  -> 
      d_step {
            c_code {  LVS1_CSLV_t_u = 0 ; };
            LVS1_CSLV_ACTcell = 1;
            LVS1_CSLV_pstate =  LVS1_CSLV_cstate ;
            LVS1_CSLV_cstate =  LVS1_CSLV_IDLE ;
            LVS1_CSLV_force_init_update = false;
            LVS1_CSLV_tick = true;
      };
      do
       :: LVS1_CSLV_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double RVA_RV_t_u, RVA_RV_t_slope, RVA_RV_t_init, RVA_RV_t =  0 ;};
c_track "&RVA_RV_t" "sizeof(double)";
c_track "&RVA_RV_t_slope" "sizeof(double)";
c_track "&RVA_RV_t_u" "sizeof(double)";
c_track "&RVA_RV_t_init" "sizeof(double)";


bool RVA_RV_ACTcell = 0 ;
bool RVA_RV_ACTpath ;
mtype { RVA_RV_IDLE, RVA_RV_ACT };
proctype RVA_RV () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! RVA_RV_ACTpath)  &&  RVA_RV_cstate == RVA_RV_IDLE) ->
     if
      :: RVA_RV_pstate != RVA_RV_cstate || RVA_RV_force_init_update ->
       c_code { RVA_RV_t_init = RVA_RV_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RVA_RV_t_slope =  0 ;
       RVA_RV_t_u = (RVA_RV_t_slope * d) + RVA_RV_t ;
       
       
       
      };
      RVA_RV_pstate = RVA_RV_cstate;
      RVA_RV_cstate = RVA_RV_IDLE;
      RVA_RV_force_init_update = false;
      RVA_RV_ACTcell  = false;
      RVA_RV_ACTpath  = false;
      RVA_RV_tick = true;
     };
     do
      :: RVA_RV_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (RVA_RV_ACTpath && 
          RVA_RV_cstate == RVA_RV_IDLE)  -> 
      d_step {
            c_code {  RVA_RV_t_u = 0 ; };
            RVA_RV_pstate =  RVA_RV_cstate ;
            RVA_RV_cstate =  RVA_RV_ACT ;
            RVA_RV_force_init_update = false;
            RVA_RV_tick = true;
      };
      do
       :: RVA_RV_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (RVA_RV_t <= 10.0) }  &&  (! RVA_RV_ACTpath)  &&  RVA_RV_cstate == RVA_RV_ACT) ->
     if
      :: RVA_RV_pstate != RVA_RV_cstate || RVA_RV_force_init_update ->
       c_code { RVA_RV_t_init = RVA_RV_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RVA_RV_t_slope =  1 ;
       RVA_RV_t_u = (RVA_RV_t_slope * d) + RVA_RV_t ;
       
       
       
      };
      RVA_RV_pstate = RVA_RV_cstate;
      RVA_RV_cstate = RVA_RV_ACT;
      RVA_RV_force_init_update = false;
      RVA_RV_ACTcell  = false;
      RVA_RV_ACTpath  = false;
      RVA_RV_tick = true;
     };
     do
      :: RVA_RV_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (RVA_RV_t > 10.0) }
           && 
          RVA_RV_cstate == RVA_RV_ACT)  -> 
      d_step {
            c_code {  RVA_RV_t_u = 0 ; };
            RVA_RV_ACTcell = 1;
            RVA_RV_pstate =  RVA_RV_cstate ;
            RVA_RV_cstate =  RVA_RV_IDLE ;
            RVA_RV_force_init_update = false;
            RVA_RV_tick = true;
      };
      do
       :: RVA_RV_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double RV_RV1_t_u, RV_RV1_t_slope, RV_RV1_t_init, RV_RV1_t =  0 ;};
c_track "&RV_RV1_t" "sizeof(double)";
c_track "&RV_RV1_t_slope" "sizeof(double)";
c_track "&RV_RV1_t_u" "sizeof(double)";
c_track "&RV_RV1_t_init" "sizeof(double)";


bool RV_RV1_ACTcell = 0 ;
bool RV_RV1_ACTpath ;
mtype { RV_RV1_IDLE, RV_RV1_ACT };
proctype RV_RV1 () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! RV_RV1_ACTpath)  &&  RV_RV1_cstate == RV_RV1_IDLE) ->
     if
      :: RV_RV1_pstate != RV_RV1_cstate || RV_RV1_force_init_update ->
       c_code { RV_RV1_t_init = RV_RV1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RV_RV1_t_slope =  0 ;
       RV_RV1_t_u = (RV_RV1_t_slope * d) + RV_RV1_t ;
       
       
       
      };
      RV_RV1_pstate = RV_RV1_cstate;
      RV_RV1_cstate = RV_RV1_IDLE;
      RV_RV1_force_init_update = false;
      RV_RV1_ACTcell  = false;
      RV_RV1_ACTpath  = false;
      RV_RV1_tick = true;
     };
     do
      :: RV_RV1_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (RV_RV1_ACTpath && 
          RV_RV1_cstate == RV_RV1_IDLE)  -> 
      d_step {
            c_code {  RV_RV1_t_u = 0 ; };
            RV_RV1_pstate =  RV_RV1_cstate ;
            RV_RV1_cstate =  RV_RV1_ACT ;
            RV_RV1_force_init_update = false;
            RV_RV1_tick = true;
      };
      do
       :: RV_RV1_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (RV_RV1_t <= 20.0) }  &&  (! RV_RV1_ACTpath)  &&  RV_RV1_cstate == RV_RV1_ACT) ->
     if
      :: RV_RV1_pstate != RV_RV1_cstate || RV_RV1_force_init_update ->
       c_code { RV_RV1_t_init = RV_RV1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RV_RV1_t_slope =  1 ;
       RV_RV1_t_u = (RV_RV1_t_slope * d) + RV_RV1_t ;
       
       
       
      };
      RV_RV1_pstate = RV_RV1_cstate;
      RV_RV1_cstate = RV_RV1_ACT;
      RV_RV1_force_init_update = false;
      RV_RV1_ACTcell  = false;
      RV_RV1_ACTpath  = false;
      RV_RV1_tick = true;
     };
     do
      :: RV_RV1_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (RV_RV1_t > 20.0) }
           && 
          RV_RV1_cstate == RV_RV1_ACT)  -> 
      d_step {
            c_code {  RV_RV1_t_u = 0 ; };
            RV_RV1_ACTcell = 1;
            RV_RV1_pstate =  RV_RV1_cstate ;
            RV_RV1_cstate =  RV_RV1_IDLE ;
            RV_RV1_force_init_update = false;
            RV_RV1_tick = true;
      };
      do
       :: RV_RV1_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double RVA_RVS_t_u, RVA_RVS_t_slope, RVA_RVS_t_init, RVA_RVS_t =  0 ;};
c_track "&RVA_RVS_t" "sizeof(double)";
c_track "&RVA_RVS_t_slope" "sizeof(double)";
c_track "&RVA_RVS_t_u" "sizeof(double)";
c_track "&RVA_RVS_t_init" "sizeof(double)";


bool RVA_RVS_ACTcell = 0 ;
bool RVA_RVS_ACTpath ;
mtype { RVA_RVS_IDLE, RVA_RVS_ACT };
proctype RVA_RVS () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! RVA_RVS_ACTpath)  &&  RVA_RVS_cstate == RVA_RVS_IDLE) ->
     if
      :: RVA_RVS_pstate != RVA_RVS_cstate || RVA_RVS_force_init_update ->
       c_code { RVA_RVS_t_init = RVA_RVS_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RVA_RVS_t_slope =  0 ;
       RVA_RVS_t_u = (RVA_RVS_t_slope * d) + RVA_RVS_t ;
       
       
       
      };
      RVA_RVS_pstate = RVA_RVS_cstate;
      RVA_RVS_cstate = RVA_RVS_IDLE;
      RVA_RVS_force_init_update = false;
      RVA_RVS_ACTcell  = false;
      RVA_RVS_ACTpath  = false;
      RVA_RVS_tick = true;
     };
     do
      :: RVA_RVS_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (RVA_RVS_ACTpath && 
          RVA_RVS_cstate == RVA_RVS_IDLE)  -> 
      d_step {
            c_code {  RVA_RVS_t_u = 0 ; };
            RVA_RVS_pstate =  RVA_RVS_cstate ;
            RVA_RVS_cstate =  RVA_RVS_ACT ;
            RVA_RVS_force_init_update = false;
            RVA_RVS_tick = true;
      };
      do
       :: RVA_RVS_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (RVA_RVS_t <= 15.0) }  &&  (! RVA_RVS_ACTpath)  &&  RVA_RVS_cstate == RVA_RVS_ACT) ->
     if
      :: RVA_RVS_pstate != RVA_RVS_cstate || RVA_RVS_force_init_update ->
       c_code { RVA_RVS_t_init = RVA_RVS_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RVA_RVS_t_slope =  1 ;
       RVA_RVS_t_u = (RVA_RVS_t_slope * d) + RVA_RVS_t ;
       
       
       
      };
      RVA_RVS_pstate = RVA_RVS_cstate;
      RVA_RVS_cstate = RVA_RVS_ACT;
      RVA_RVS_force_init_update = false;
      RVA_RVS_ACTcell  = false;
      RVA_RVS_ACTpath  = false;
      RVA_RVS_tick = true;
     };
     do
      :: RVA_RVS_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (RVA_RVS_t > 15.0) }
           && 
          RVA_RVS_cstate == RVA_RVS_ACT)  -> 
      d_step {
            c_code {  RVA_RVS_t_u = 0 ; };
            RVA_RVS_ACTcell = 1;
            RVA_RVS_pstate =  RVA_RVS_cstate ;
            RVA_RVS_cstate =  RVA_RVS_IDLE ;
            RVA_RVS_force_init_update = false;
            RVA_RVS_tick = true;
      };
      do
       :: RVA_RVS_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double RVS_RVS1_t_u, RVS_RVS1_t_slope, RVS_RVS1_t_init, RVS_RVS1_t =  0 ;};
c_track "&RVS_RVS1_t" "sizeof(double)";
c_track "&RVS_RVS1_t_slope" "sizeof(double)";
c_track "&RVS_RVS1_t_u" "sizeof(double)";
c_track "&RVS_RVS1_t_init" "sizeof(double)";


bool RVS_RVS1_ACTcell = 0 ;
bool RVS_RVS1_ACTpath ;
mtype { RVS_RVS1_IDLE, RVS_RVS1_ACT };
proctype RVS_RVS1 () {
IDLE:
   if
    :: (c_expr { 1 }  &&  (! RVS_RVS1_ACTpath)  &&  RVS_RVS1_cstate == RVS_RVS1_IDLE) ->
     if
      :: RVS_RVS1_pstate != RVS_RVS1_cstate || RVS_RVS1_force_init_update ->
       c_code { RVS_RVS1_t_init = RVS_RVS1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RVS_RVS1_t_slope =  0 ;
       RVS_RVS1_t_u = (RVS_RVS1_t_slope * d) + RVS_RVS1_t ;
       
       
       
      };
      RVS_RVS1_pstate = RVS_RVS1_cstate;
      RVS_RVS1_cstate = RVS_RVS1_IDLE;
      RVS_RVS1_force_init_update = false;
      RVS_RVS1_ACTcell  = false;
      RVS_RVS1_ACTpath  = false;
      RVS_RVS1_tick = true;
     };
     do
      :: RVS_RVS1_tick -> true
      :: else -> goto  IDLE
     od;
     ::  (RVS_RVS1_ACTpath && 
          RVS_RVS1_cstate == RVS_RVS1_IDLE)  -> 
      d_step {
            c_code {  RVS_RVS1_t_u = 0 ; };
            RVS_RVS1_pstate =  RVS_RVS1_cstate ;
            RVS_RVS1_cstate =  RVS_RVS1_ACT ;
            RVS_RVS1_force_init_update = false;
            RVS_RVS1_tick = true;
      };
      do
       :: RVS_RVS1_tick -> true
       :: else -> goto  ACT
      od;

     :: else -> true
   fi;
ACT:
   if
    :: (c_expr { (RVS_RVS1_t <= 20.0) }  &&  (! RVS_RVS1_ACTpath)  &&  RVS_RVS1_cstate == RVS_RVS1_ACT) ->
     if
      :: RVS_RVS1_pstate != RVS_RVS1_cstate || RVS_RVS1_force_init_update ->
       c_code { RVS_RVS1_t_init = RVS_RVS1_t ; }
      :: else -> true
     fi;
     d_step {
      c_code {
       RVS_RVS1_t_slope =  1 ;
       RVS_RVS1_t_u = (RVS_RVS1_t_slope * d) + RVS_RVS1_t ;
       
       
       
      };
      RVS_RVS1_pstate = RVS_RVS1_cstate;
      RVS_RVS1_cstate = RVS_RVS1_ACT;
      RVS_RVS1_force_init_update = false;
      RVS_RVS1_ACTcell  = false;
      RVS_RVS1_ACTpath  = false;
      RVS_RVS1_tick = true;
     };
     do
      :: RVS_RVS1_tick -> true
      :: else -> goto  ACT
     od;
     ::  (c_expr { (RVS_RVS1_t > 20.0) }
           && 
          RVS_RVS1_cstate == RVS_RVS1_ACT)  -> 
      d_step {
            c_code {  RVS_RVS1_t_u = 0 ; };
            RVS_RVS1_ACTcell = 1;
            RVS_RVS1_pstate =  RVS_RVS1_cstate ;
            RVS_RVS1_cstate =  RVS_RVS1_IDLE ;
            RVS_RVS1_force_init_update = false;
            RVS_RVS1_tick = true;
      };
      do
       :: RVS_RVS1_tick -> true
       :: else -> goto  IDLE
      od;

     :: else -> true
   fi;
}
proctype main () {
  do
     ::  (SinoatrialNode_tick
           && BachmannBundle_tick && 
          LeftAtrium_tick && 
          LeftAtrium1_tick && 
          RightAtrium_tick && 
          RightAtrium1_tick && 
          CoronarySinus_tick && 
          CristaTerminalis_tick && 
          CristaTerminalis1_tick && 
          Ostium_tick && Fast_tick && 
          Fast1_tick && Slow_tick && 
          Slow1_tick && 
          AtrioventricularNode_tick && 
          BundleOfHis_tick && 
          BundleOfHis1_tick && 
          BundleOfHis2_tick && 
          LeftBundleBranch_tick && 
          LeftBundleBranch1_tick && 
          LeftVentricularApex_tick && 
          LeftVentricle_tick && 
          LeftVentricle1_tick && 
          LeftVentricularSeptum_tick && 
          LeftVentricularSeptum1_tick && 
          CSLeftVentricular_tick && 
          RightBundleBranch_tick && 
          RightBundleBranch1_tick && 
          RightVentricularApex_tick && 
          RightVentricle_tick && 
          RightVentricle1_tick && 
          RightVentricularSeptum_tick && 
          RightVentricularSeptum1_tick && 
          SA_BB_tick && SA_OS_tick && 
          SA_RA_tick && SA_CT_tick && 
          BB_LA_tick && LA_LA1_tick && 
          RA_RA1_tick && RA1_CS_tick && 
          CT_CT1_tick && OS_Fast_tick && 
          Fast_Fast1_tick && OS_Slow_tick
           && Slow_Slow1_tick && 
          Fast1_AV_tick && Slow1_AV_tick
           && AV_His_tick && His_His1_tick
           && His1_His2_tick && 
          His2_LBB_tick && LBB_LBB1_tick
           && LBB1_LVA_tick && 
          His2_RBB_tick && RBB_RBB1_tick
           && RBB1_RVA_tick && 
          LVA_RVA_tick && LVA_LV_tick && 
          LV_LV1_tick && LVA_LVS_tick && 
          LVS_LVS1_tick && LVS1_CSLV_tick
           && RVA_RV_tick && RV_RV1_tick
           && RVA_RVS_tick && 
          RVS_RVS1_tick)  -> 
      d_step {
        c_code { SinoatrialNode_t = SinoatrialNode_t_u;
                 BachmannBundle_t = BachmannBundle_t_u;
                 LeftAtrium_t = LeftAtrium_t_u;
                 LeftAtrium1_t = LeftAtrium1_t_u;
                 RightAtrium_t = RightAtrium_t_u;
                 RightAtrium1_t = RightAtrium1_t_u;
                 CoronarySinus_t = CoronarySinus_t_u;
                 CristaTerminalis_t = CristaTerminalis_t_u;
                 CristaTerminalis1_t = CristaTerminalis1_t_u;
                 Ostium_t = Ostium_t_u;
                 Fast_t = Fast_t_u;
                 Fast1_t = Fast1_t_u;
                 Slow_t = Slow_t_u;
                 Slow1_t = Slow1_t_u;
                 AtrioventricularNode_t = AtrioventricularNode_t_u;
                 BundleOfHis_t = BundleOfHis_t_u;
                 BundleOfHis1_t = BundleOfHis1_t_u;
                 BundleOfHis2_t = BundleOfHis2_t_u;
                 LeftBundleBranch_t = LeftBundleBranch_t_u;
                 LeftBundleBranch1_t = LeftBundleBranch1_t_u;
                 LeftVentricularApex_t = LeftVentricularApex_t_u;
                 LeftVentricle_t = LeftVentricle_t_u;
                 LeftVentricle1_t = LeftVentricle1_t_u;
                 LeftVentricularSeptum_t = LeftVentricularSeptum_t_u;
                 LeftVentricularSeptum1_t = LeftVentricularSeptum1_t_u;
                 CSLeftVentricular_t = CSLeftVentricular_t_u;
                 RightBundleBranch_t = RightBundleBranch_t_u;
                 RightBundleBranch1_t = RightBundleBranch1_t_u;
                 RightVentricularApex_t = RightVentricularApex_t_u;
                 RightVentricle_t = RightVentricle_t_u;
                 RightVentricle1_t = RightVentricle1_t_u;
                 RightVentricularSeptum_t = RightVentricularSeptum_t_u;
                 RightVentricularSeptum1_t = RightVentricularSeptum1_t_u;
                 SA_BB_t = SA_BB_t_u;
                 SA_OS_t = SA_OS_t_u;
                 SA_RA_t = SA_RA_t_u;
                 SA_CT_t = SA_CT_t_u;
                 BB_LA_t = BB_LA_t_u;
                 LA_LA1_t = LA_LA1_t_u;
                 RA_RA1_t = RA_RA1_t_u;
                 RA1_CS_t = RA1_CS_t_u;
                 CT_CT1_t = CT_CT1_t_u;
                 OS_Fast_t = OS_Fast_t_u;
                 Fast_Fast1_t = Fast_Fast1_t_u;
                 OS_Slow_t = OS_Slow_t_u;
                 Slow_Slow1_t = Slow_Slow1_t_u;
                 Fast1_AV_t = Fast1_AV_t_u;
                 Slow1_AV_t = Slow1_AV_t_u;
                 AV_His_t = AV_His_t_u;
                 His_His1_t = His_His1_t_u;
                 His1_His2_t = His1_His2_t_u;
                 His2_LBB_t = His2_LBB_t_u;
                 LBB_LBB1_t = LBB_LBB1_t_u;
                 LBB1_LVA_t = LBB1_LVA_t_u;
                 His2_RBB_t = His2_RBB_t_u;
                 RBB_RBB1_t = RBB_RBB1_t_u;
                 RBB1_RVA_t = RBB1_RVA_t_u;
                 LVA_RVA_t = LVA_RVA_t_u;
                 LVA_LV_t = LVA_LV_t_u;
                 LV_LV1_t = LV_LV1_t_u;
                 LVA_LVS_t = LVA_LVS_t_u;
                 LVS_LVS1_t = LVS_LVS1_t_u;
                 LVS1_CSLV_t = LVS1_CSLV_t_u;
                 RVA_RV_t = RVA_RV_t_u;
                 RV_RV1_t = RV_RV1_t_u;
                 RVA_RVS_t = RVA_RVS_t_u;
                 RVS_RVS1_t = RVS_RVS1_t_u; };
        c_code {  };
        BachmannBundle_ACTcell1 = SA_BB_ACTcell;
        LeftAtrium_ACTcell1 = BB_LA_ACTcell;
        LeftAtrium1_ACTcell1 = LA_LA1_ACTcell;
        RightAtrium_ACTcell1 = SA_RA_ACTcell;
        RightAtrium1_ACTcell1 = RA_RA1_ACTcell;
        CoronarySinus_ACTcell1 = RA1_CS_ACTcell;
        CristaTerminalis_ACTcell1 = SA_CT_ACTcell;
        CristaTerminalis1_ACTcell1 = CT_CT1_ACTcell;
        Ostium_ACTcell1 = SA_OS_ACTcell;
        Fast_ACTcell1 = OS_Fast_ACTcell;
        Fast1_ACTcell1 = Fast_Fast1_ACTcell;
        Slow_ACTcell1 = OS_Slow_ACTcell;
        Slow1_ACTcell1 = Slow_Slow1_ACTcell;
        AtrioventricularNode_ACTcell1 = Slow1_AV_ACTcell;
        BundleOfHis_ACTcell1 = AV_His_ACTcell;
        BundleOfHis1_ACTcell1 = His_His1_ACTcell;
        BundleOfHis2_ACTcell1 = His1_His2_ACTcell;
        LeftBundleBranch_ACTcell1 = His2_LBB_ACTcell;
        LeftBundleBranch1_ACTcell1 = LBB_LBB1_ACTcell;
        LeftVentricularApex_ACTcell1 = LBB1_LVA_ACTcell;
        RightBundleBranch_ACTcell1 = His2_RBB_ACTcell;
        RightBundleBranch1_ACTcell1 = RBB_RBB1_ACTcell;
        RightVentricularApex_ACTcell1 = RBB1_RVA_ACTcell;
        LeftVentricle_ACTcell1 = LVA_LV_ACTcell;
        LeftVentricle1_ACTcell1 = LV_LV1_ACTcell;
        LeftVentricularSeptum_ACTcell1 = LVA_LVS_ACTcell;
        LeftVentricularSeptum1_ACTcell1 = LVS_LVS1_ACTcell;
        CSLeftVentricular_ACTcell1 = LVS1_CSLV_ACTcell;
        RightVentricle_ACTcell1 = RVA_RV_ACTcell;
        RightVentricle1_ACTcell1 = RV_RV1_ACTcell;
        RightVentricularSeptum_ACTcell1 = RVA_RVS_ACTcell;
        RightVentricularSeptum1_ACTcell1 = RVS_RVS1_ACTcell;
        AtrioventricularNode_ACTcell2 = Fast1_AV_ACTcell;
        SA_BB_ACTpath = SinoatrialNode_ACTpath;
        SA_OS_ACTpath = SinoatrialNode_ACTpath;
        SA_RA_ACTpath = SinoatrialNode_ACTpath;
        SA_CT_ACTpath = SinoatrialNode_ACTpath;
        BB_LA_ACTpath = BachmannBundle_ACTpath;
        LA_LA1_ACTpath = LeftAtrium_ACTpath;
        RA_RA1_ACTpath = RightAtrium_ACTpath;
        RA1_CS_ACTpath = RightAtrium1_ACTpath;
        CT_CT1_ACTpath = CristaTerminalis_ACTpath;
        OS_Fast_ACTpath = Ostium_ACTpath;
        OS_Slow_ACTpath = Ostium_ACTpath;
        Fast_Fast1_ACTpath = Fast_ACTpath;
        Fast1_AV_ACTpath = Fast1_ACTpath;
        Slow_Slow1_ACTpath = Slow_ACTpath;
        Slow1_AV_ACTpath = Slow1_ACTpath;
        AV_His_ACTpath = AtrioventricularNode_ACTpath;
        His_His1_ACTpath = BundleOfHis_ACTpath;
        His1_His2_ACTpath = BundleOfHis1_ACTpath;
        His2_LBB_ACTpath = BundleOfHis2_ACTpath;
        LBB_LBB1_ACTpath = LeftBundleBranch_ACTpath;
        LBB1_LVA_ACTpath = LeftBundleBranch1_ACTpath;
        His2_RBB_ACTpath = BundleOfHis2_ACTpath;
        RBB_RBB1_ACTpath = RightBundleBranch_ACTpath;
        RBB1_RVA_ACTpath = RightBundleBranch1_ACTpath;
        LVA_LV_ACTpath = LeftVentricularApex_ACTpath;
        LV_LV1_ACTpath = LeftVentricle_ACTpath;
        LVA_LVS_ACTpath = LeftVentricularApex_ACTpath;
        LVS_LVS1_ACTpath = LeftVentricularSeptum_ACTpath;
        LVS1_CSLV_ACTpath = LeftVentricularSeptum1_ACTpath;
        RVA_RV_ACTpath = RightVentricularApex_ACTpath;
        RV_RV1_ACTpath = RightVentricle_ACTpath;
        RVA_RVS_ACTpath = RightVentricularApex_ACTpath;
        RVS_RVS1_ACTpath = RightVentricularSeptum_ACTpath;
        RVS_RVS1_tick = false;
        RVA_RVS_tick = false;
        RV_RV1_tick = false;
        RVA_RV_tick = false;
        LVS1_CSLV_tick = false;
        LVS_LVS1_tick = false;
        LVA_LVS_tick = false;
        LV_LV1_tick = false;
        LVA_LV_tick = false;
        LVA_RVA_tick = false;
        RBB1_RVA_tick = false;
        RBB_RBB1_tick = false;
        His2_RBB_tick = false;
        LBB1_LVA_tick = false;
        LBB_LBB1_tick = false;
        His2_LBB_tick = false;
        His1_His2_tick = false;
        His_His1_tick = false;
        AV_His_tick = false;
        Slow1_AV_tick = false;
        Fast1_AV_tick = false;
        Slow_Slow1_tick = false;
        OS_Slow_tick = false;
        Fast_Fast1_tick = false;
        OS_Fast_tick = false;
        CT_CT1_tick = false;
        RA1_CS_tick = false;
        RA_RA1_tick = false;
        LA_LA1_tick = false;
        BB_LA_tick = false;
        SA_CT_tick = false;
        SA_RA_tick = false;
        SA_OS_tick = false;
        SA_BB_tick = false;
        RightVentricularSeptum1_tick = false;
        RightVentricularSeptum_tick = false;
        RightVentricle1_tick = false;
        RightVentricle_tick = false;
        RightVentricularApex_tick = false;
        RightBundleBranch1_tick = false;
        RightBundleBranch_tick = false;
        CSLeftVentricular_tick = false;
        LeftVentricularSeptum1_tick = false;
        LeftVentricularSeptum_tick = false;
        LeftVentricle1_tick = false;
        LeftVentricle_tick = false;
        LeftVentricularApex_tick = false;
        LeftBundleBranch1_tick = false;
        LeftBundleBranch_tick = false;
        BundleOfHis2_tick = false;
        BundleOfHis1_tick = false;
        BundleOfHis_tick = false;
        AtrioventricularNode_tick = false;
        Slow1_tick = false;
        Slow_tick = false;
        Fast1_tick = false;
        Fast_tick = false;
        Ostium_tick = false;
        CristaTerminalis1_tick = false;
        CristaTerminalis_tick = false;
        CoronarySinus_tick = false;
        RightAtrium1_tick = false;
        RightAtrium_tick = false;
        LeftAtrium1_tick = false;
        LeftAtrium_tick = false;
        BachmannBundle_tick = false;
        SinoatrialNode_tick = false;
        
      }
     :: else -> true
  od
}
init {
  d_step {
    c_code {
    
    };
    c_code {  };
    BachmannBundle_ACTcell1 = SA_BB_ACTcell;
    LeftAtrium_ACTcell1 = BB_LA_ACTcell;
    LeftAtrium1_ACTcell1 = LA_LA1_ACTcell;
    RightAtrium_ACTcell1 = SA_RA_ACTcell;
    RightAtrium1_ACTcell1 = RA_RA1_ACTcell;
    CoronarySinus_ACTcell1 = RA1_CS_ACTcell;
    CristaTerminalis_ACTcell1 = SA_CT_ACTcell;
    CristaTerminalis1_ACTcell1 = CT_CT1_ACTcell;
    Ostium_ACTcell1 = SA_OS_ACTcell;
    Fast_ACTcell1 = OS_Fast_ACTcell;
    Fast1_ACTcell1 = Fast_Fast1_ACTcell;
    Slow_ACTcell1 = OS_Slow_ACTcell;
    Slow1_ACTcell1 = Slow_Slow1_ACTcell;
    AtrioventricularNode_ACTcell1 = Slow1_AV_ACTcell;
    BundleOfHis_ACTcell1 = AV_His_ACTcell;
    BundleOfHis1_ACTcell1 = His_His1_ACTcell;
    BundleOfHis2_ACTcell1 = His1_His2_ACTcell;
    LeftBundleBranch_ACTcell1 = His2_LBB_ACTcell;
    LeftBundleBranch1_ACTcell1 = LBB_LBB1_ACTcell;
    LeftVentricularApex_ACTcell1 = LBB1_LVA_ACTcell;
    RightBundleBranch_ACTcell1 = His2_RBB_ACTcell;
    RightBundleBranch1_ACTcell1 = RBB_RBB1_ACTcell;
    RightVentricularApex_ACTcell1 = RBB1_RVA_ACTcell;
    LeftVentricle_ACTcell1 = LVA_LV_ACTcell;
    LeftVentricle1_ACTcell1 = LV_LV1_ACTcell;
    LeftVentricularSeptum_ACTcell1 = LVA_LVS_ACTcell;
    LeftVentricularSeptum1_ACTcell1 = LVS_LVS1_ACTcell;
    CSLeftVentricular_ACTcell1 = LVS1_CSLV_ACTcell;
    RightVentricle_ACTcell1 = RVA_RV_ACTcell;
    RightVentricle1_ACTcell1 = RV_RV1_ACTcell;
    RightVentricularSeptum_ACTcell1 = RVA_RVS_ACTcell;
    RightVentricularSeptum1_ACTcell1 = RVS_RVS1_ACTcell;
    AtrioventricularNode_ACTcell2 = Fast1_AV_ACTcell;
    SA_BB_ACTpath = SinoatrialNode_ACTpath;
    SA_OS_ACTpath = SinoatrialNode_ACTpath;
    SA_RA_ACTpath = SinoatrialNode_ACTpath;
    SA_CT_ACTpath = SinoatrialNode_ACTpath;
    BB_LA_ACTpath = BachmannBundle_ACTpath;
    LA_LA1_ACTpath = LeftAtrium_ACTpath;
    RA_RA1_ACTpath = RightAtrium_ACTpath;
    RA1_CS_ACTpath = RightAtrium1_ACTpath;
    CT_CT1_ACTpath = CristaTerminalis_ACTpath;
    OS_Fast_ACTpath = Ostium_ACTpath;
    OS_Slow_ACTpath = Ostium_ACTpath;
    Fast_Fast1_ACTpath = Fast_ACTpath;
    Fast1_AV_ACTpath = Fast1_ACTpath;
    Slow_Slow1_ACTpath = Slow_ACTpath;
    Slow1_AV_ACTpath = Slow1_ACTpath;
    AV_His_ACTpath = AtrioventricularNode_ACTpath;
    His_His1_ACTpath = BundleOfHis_ACTpath;
    His1_His2_ACTpath = BundleOfHis1_ACTpath;
    His2_LBB_ACTpath = BundleOfHis2_ACTpath;
    LBB_LBB1_ACTpath = LeftBundleBranch_ACTpath;
    LBB1_LVA_ACTpath = LeftBundleBranch1_ACTpath;
    His2_RBB_ACTpath = BundleOfHis2_ACTpath;
    RBB_RBB1_ACTpath = RightBundleBranch_ACTpath;
    RBB1_RVA_ACTpath = RightBundleBranch1_ACTpath;
    LVA_LV_ACTpath = LeftVentricularApex_ACTpath;
    LV_LV1_ACTpath = LeftVentricle_ACTpath;
    LVA_LVS_ACTpath = LeftVentricularApex_ACTpath;
    LVS_LVS1_ACTpath = LeftVentricularSeptum_ACTpath;
    LVS1_CSLV_ACTpath = LeftVentricularSeptum1_ACTpath;
    RVA_RV_ACTpath = RightVentricularApex_ACTpath;
    RV_RV1_ACTpath = RightVentricle_ACTpath;
    RVA_RVS_ACTpath = RightVentricularApex_ACTpath;
    RVS_RVS1_ACTpath = RightVentricularSeptum_ACTpath;
    SinoatrialNode_cstate = SinoatrialNode_REST;
    BachmannBundle_cstate = BachmannBundle_REST;
    LeftAtrium_cstate = LeftAtrium_REST;
    LeftAtrium1_cstate = LeftAtrium1_REST;
    RightAtrium_cstate = RightAtrium_REST;
    RightAtrium1_cstate = RightAtrium1_REST;
    CoronarySinus_cstate = CoronarySinus_REST;
    CristaTerminalis_cstate = CristaTerminalis_REST;
    CristaTerminalis1_cstate = CristaTerminalis1_REST;
    Ostium_cstate = Ostium_REST;
    Fast_cstate = Fast_REST;
    Fast1_cstate = Fast1_REST;
    Slow_cstate = Slow_REST;
    Slow1_cstate = Slow1_REST;
    AtrioventricularNode_cstate = AtrioventricularNode_REST;
    BundleOfHis_cstate = BundleOfHis_REST;
    BundleOfHis1_cstate = BundleOfHis1_REST;
    BundleOfHis2_cstate = BundleOfHis2_REST;
    LeftBundleBranch_cstate = LeftBundleBranch_REST;
    LeftBundleBranch1_cstate = LeftBundleBranch1_REST;
    LeftVentricularApex_cstate = LeftVentricularApex_REST;
    LeftVentricle_cstate = LeftVentricle_REST;
    LeftVentricle1_cstate = LeftVentricle1_REST;
    LeftVentricularSeptum_cstate = LeftVentricularSeptum_REST;
    LeftVentricularSeptum1_cstate = LeftVentricularSeptum1_REST;
    CSLeftVentricular_cstate = CSLeftVentricular_REST;
    RightBundleBranch_cstate = RightBundleBranch_REST;
    RightBundleBranch1_cstate = RightBundleBranch1_REST;
    RightVentricularApex_cstate = RightVentricularApex_REST;
    RightVentricle_cstate = RightVentricle_REST;
    RightVentricle1_cstate = RightVentricle1_REST;
    RightVentricularSeptum_cstate = RightVentricularSeptum_REST;
    RightVentricularSeptum1_cstate = RightVentricularSeptum1_REST;
    SA_BB_cstate = SA_BB_IDLE;
    SA_OS_cstate = SA_OS_IDLE;
    SA_RA_cstate = SA_RA_IDLE;
    SA_CT_cstate = SA_CT_IDLE;
    BB_LA_cstate = BB_LA_IDLE;
    LA_LA1_cstate = LA_LA1_IDLE;
    RA_RA1_cstate = RA_RA1_IDLE;
    RA1_CS_cstate = RA1_CS_IDLE;
    CT_CT1_cstate = CT_CT1_IDLE;
    OS_Fast_cstate = OS_Fast_IDLE;
    Fast_Fast1_cstate = Fast_Fast1_IDLE;
    OS_Slow_cstate = OS_Slow_IDLE;
    Slow_Slow1_cstate = Slow_Slow1_IDLE;
    Fast1_AV_cstate = Fast1_AV_IDLE;
    Slow1_AV_cstate = Slow1_AV_IDLE;
    AV_His_cstate = AV_His_IDLE;
    His_His1_cstate = His_His1_IDLE;
    His1_His2_cstate = His1_His2_IDLE;
    His2_LBB_cstate = His2_LBB_IDLE;
    LBB_LBB1_cstate = LBB_LBB1_IDLE;
    LBB1_LVA_cstate = LBB1_LVA_IDLE;
    His2_RBB_cstate = His2_RBB_IDLE;
    RBB_RBB1_cstate = RBB_RBB1_IDLE;
    RBB1_RVA_cstate = RBB1_RVA_IDLE;
    LVA_RVA_cstate = LVA_RVA_IDLE;
    LVA_LV_cstate = LVA_LV_IDLE;
    LV_LV1_cstate = LV_LV1_IDLE;
    LVA_LVS_cstate = LVA_LVS_IDLE;
    LVS_LVS1_cstate = LVS_LVS1_IDLE;
    LVS1_CSLV_cstate = LVS1_CSLV_IDLE;
    RVA_RV_cstate = RVA_RV_IDLE;
    RV_RV1_cstate = RV_RV1_IDLE;
    RVA_RVS_cstate = RVA_RVS_IDLE;
    RVS_RVS1_cstate = RVS_RVS1_IDLE;
  }
  atomic {
    run main();
    run RVS_RVS1();
    run RVA_RVS();
    run RV_RV1();
    run RVA_RV();
    run LVS1_CSLV();
    run LVS_LVS1();
    run LVA_LVS();
    run LV_LV1();
    run LVA_LV();
    run LVA_RVA();
    run RBB1_RVA();
    run RBB_RBB1();
    run His2_RBB();
    run LBB1_LVA();
    run LBB_LBB1();
    run His2_LBB();
    run His1_His2();
    run His_His1();
    run AV_His();
    run Slow1_AV();
    run Fast1_AV();
    run Slow_Slow1();
    run OS_Slow();
    run Fast_Fast1();
    run OS_Fast();
    run CT_CT1();
    run RA1_CS();
    run RA_RA1();
    run LA_LA1();
    run BB_LA();
    run SA_CT();
    run SA_RA();
    run SA_OS();
    run SA_BB();
    run RightVentricularSeptum1();
    run RightVentricularSeptum();
    run RightVentricle1();
    run RightVentricle();
    run RightVentricularApex();
    run RightBundleBranch1();
    run RightBundleBranch();
    run CSLeftVentricular();
    run LeftVentricularSeptum1();
    run LeftVentricularSeptum();
    run LeftVentricle1();
    run LeftVentricle();
    run LeftVentricularApex();
    run LeftBundleBranch1();
    run LeftBundleBranch();
    run BundleOfHis2();
    run BundleOfHis1();
    run BundleOfHis();
    run AtrioventricularNode();
    run Slow1();
    run Slow();
    run Fast1();
    run Fast();
    run Ostium();
    run CristaTerminalis1();
    run CristaTerminalis();
    run CoronarySinus();
    run RightAtrium1();
    run RightAtrium();
    run LeftAtrium1();
    run LeftAtrium();
    run BachmannBundle();
    run SinoatrialNode();
    
  }
}