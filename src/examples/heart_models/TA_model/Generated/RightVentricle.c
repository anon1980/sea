#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char RightVentricle_ACTpath =  False ;
extern unsigned char  RightVentricle_ACTcell1 ;//Events
static double  RightVentricle_t  =  0 ; //the continuous vars
static double  RightVentricle_t_u ; // and their updates
static double  RightVentricle_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states RightVentricle (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (RightVentricle_ACTcell1) {
      RightVentricle_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (RightVentricle_t > (400.0)) {
      RightVentricle_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( RightVentricle_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricle_t_init = RightVentricle_t ;
      slope =  1 ;
      RightVentricle_t_u = (slope * d) + RightVentricle_t ;
      /* Possible Saturation */
      
      RightVentricle_ACTpath  = False;
      RightVentricle_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricle!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (RightVentricle_t > (10.0)) {
      RightVentricle_t_u = 0 ;
      RightVentricle_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( RightVentricle_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricle_t_init = RightVentricle_t ;
      slope =  1 ;
      RightVentricle_t_u = (slope * d) + RightVentricle_t ;
      /* Possible Saturation */
      
      RightVentricle_ACTpath  = False;
      RightVentricle_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricle!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (RightVentricle_t > (200.0)) {
      RightVentricle_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( RightVentricle_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricle_t_init = RightVentricle_t ;
      slope =  1 ;
      RightVentricle_t_u = (slope * d) + RightVentricle_t ;
      /* Possible Saturation */
      
      RightVentricle_ACTpath  = False;
      RightVentricle_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricle!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (RightVentricle_ACTcell1) {
      RightVentricle_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (RightVentricle_t > (100.0)) {
      RightVentricle_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( RightVentricle_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricle_t_init = RightVentricle_t ;
      slope =  1 ;
      RightVentricle_t_u = (slope * d) + RightVentricle_t ;
      /* Possible Saturation */
      
      RightVentricle_ACTpath  = False;
      RightVentricle_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricle!\n");
      exit(1);
    }
    break;
  }
  RightVentricle_t = RightVentricle_t_u;
  return cstate;
}