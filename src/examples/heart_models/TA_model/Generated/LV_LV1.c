#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char LV_LV1_ACTcell =  False ;
extern unsigned char  LV_LV1_ACTpath ;//Events
static double  LV_LV1_t  =  0 ; //the continuous vars
static double  LV_LV1_t_u ; // and their updates
static double  LV_LV1_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states LV_LV1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (LV_LV1_ACTpath) {
      LV_LV1_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LV_LV1_t_init = LV_LV1_t ;
      slope =  0 ;
      LV_LV1_t_u = (slope * d) + LV_LV1_t ;
      /* Possible Saturation */
      
      LV_LV1_ACTcell  = False;
      LV_LV1_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LV_LV1!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (LV_LV1_t > (30.0)) {
      LV_LV1_t_u = 0 ;
      LV_LV1_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( LV_LV1_t <= (30.0)     ) {
      if ((pstate != cstate) || force_init_update) LV_LV1_t_init = LV_LV1_t ;
      slope =  1 ;
      LV_LV1_t_u = (slope * d) + LV_LV1_t ;
      /* Possible Saturation */
      
      LV_LV1_ACTcell  = False;
      LV_LV1_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LV_LV1!\n");
      exit(1);
    }
    break;
  }
  LV_LV1_t = LV_LV1_t_u;
  return cstate;
}