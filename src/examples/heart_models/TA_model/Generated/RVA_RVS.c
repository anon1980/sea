#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char RVA_RVS_ACTcell =  False ;
extern unsigned char  RVA_RVS_ACTpath ;//Events
static double  RVA_RVS_t  =  0 ; //the continuous vars
static double  RVA_RVS_t_u ; // and their updates
static double  RVA_RVS_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states RVA_RVS (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (RVA_RVS_ACTpath) {
      RVA_RVS_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVA_RVS_t_init = RVA_RVS_t ;
      slope =  0 ;
      RVA_RVS_t_u = (slope * d) + RVA_RVS_t ;
      /* Possible Saturation */
      
      RVA_RVS_ACTcell  = False;
      RVA_RVS_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RVS!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (RVA_RVS_t > (15.0)) {
      RVA_RVS_t_u = 0 ;
      RVA_RVS_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( RVA_RVS_t <= (15.0)     ) {
      if ((pstate != cstate) || force_init_update) RVA_RVS_t_init = RVA_RVS_t ;
      slope =  1 ;
      RVA_RVS_t_u = (slope * d) + RVA_RVS_t ;
      /* Possible Saturation */
      
      RVA_RVS_ACTcell  = False;
      RVA_RVS_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RVS!\n");
      exit(1);
    }
    break;
  }
  RVA_RVS_t = RVA_RVS_t_u;
  return cstate;
}