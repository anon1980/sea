#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char OS_Slow_ACTcell =  False ;
extern unsigned char  OS_Slow_ACTpath ;//Events
static double  OS_Slow_t  =  0 ; //the continuous vars
static double  OS_Slow_t_u ; // and their updates
static double  OS_Slow_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states OS_Slow (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (OS_Slow_ACTpath) {
      OS_Slow_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) OS_Slow_t_init = OS_Slow_t ;
      slope =  0 ;
      OS_Slow_t_u = (slope * d) + OS_Slow_t ;
      /* Possible Saturation */
      
      OS_Slow_ACTcell  = False;
      OS_Slow_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Slow!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (OS_Slow_t > (30.0)) {
      OS_Slow_t_u = 0 ;
      OS_Slow_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( OS_Slow_t <= (30.0)     ) {
      if ((pstate != cstate) || force_init_update) OS_Slow_t_init = OS_Slow_t ;
      slope =  1 ;
      OS_Slow_t_u = (slope * d) + OS_Slow_t ;
      /* Possible Saturation */
      
      OS_Slow_ACTcell  = False;
      OS_Slow_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Slow!\n");
      exit(1);
    }
    break;
  }
  OS_Slow_t = OS_Slow_t_u;
  return cstate;
}