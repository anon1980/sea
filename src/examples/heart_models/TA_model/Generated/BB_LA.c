#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char BB_LA_ACTcell =  False ;
extern unsigned char  BB_LA_ACTpath ;//Events
static double  BB_LA_t  =  0 ; //the continuous vars
static double  BB_LA_t_u ; // and their updates
static double  BB_LA_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states BB_LA (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (BB_LA_ACTpath) {
      BB_LA_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) BB_LA_t_init = BB_LA_t ;
      slope =  0 ;
      BB_LA_t_u = (slope * d) + BB_LA_t ;
      /* Possible Saturation */
      
      BB_LA_ACTcell  = False;
      BB_LA_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: BB_LA!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (BB_LA_t > (30.0)) {
      BB_LA_t_u = 0 ;
      BB_LA_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( BB_LA_t <= (30.0)     ) {
      if ((pstate != cstate) || force_init_update) BB_LA_t_init = BB_LA_t ;
      slope =  1 ;
      BB_LA_t_u = (slope * d) + BB_LA_t ;
      /* Possible Saturation */
      
      BB_LA_ACTcell  = False;
      BB_LA_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: BB_LA!\n");
      exit(1);
    }
    break;
  }
  BB_LA_t = BB_LA_t_u;
  return cstate;
}