#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char Slow_ACTpath =  False ;
extern unsigned char  Slow_ACTcell1 ;//Events
static double  Slow_t  =  0 ; //the continuous vars
static double  Slow_t_u ; // and their updates
static double  Slow_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states Slow (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (Slow_ACTcell1) {
      Slow_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (Slow_t > (400.0)) {
      Slow_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( Slow_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) Slow_t_init = Slow_t ;
      slope =  1 ;
      Slow_t_u = (slope * d) + Slow_t ;
      /* Possible Saturation */
      
      Slow_ACTpath  = False;
      Slow_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (Slow_t > (10.0)) {
      Slow_t_u = 0 ;
      Slow_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( Slow_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) Slow_t_init = Slow_t ;
      slope =  1 ;
      Slow_t_u = (slope * d) + Slow_t ;
      /* Possible Saturation */
      
      Slow_ACTpath  = False;
      Slow_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (Slow_t > (200.0)) {
      Slow_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( Slow_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) Slow_t_init = Slow_t ;
      slope =  1 ;
      Slow_t_u = (slope * d) + Slow_t ;
      /* Possible Saturation */
      
      Slow_ACTpath  = False;
      Slow_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (Slow_ACTcell1) {
      Slow_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (Slow_t > (100.0)) {
      Slow_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( Slow_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) Slow_t_init = Slow_t ;
      slope =  1 ;
      Slow_t_u = (slope * d) + Slow_t ;
      /* Possible Saturation */
      
      Slow_ACTpath  = False;
      Slow_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow!\n");
      exit(1);
    }
    break;
  }
  Slow_t = Slow_t_u;
  return cstate;
}