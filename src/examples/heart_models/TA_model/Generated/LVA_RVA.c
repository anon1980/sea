#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char LVA_RVA_ACTcell =  False ;
extern unsigned char  LVA_RVA_ACTpath ;//Events
static double  LVA_RVA_t  =  0 ; //the continuous vars
static double  LVA_RVA_t_u ; // and their updates
static double  LVA_RVA_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states LVA_RVA (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (LVA_RVA_ACTpath) {
      LVA_RVA_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_RVA_t_init = LVA_RVA_t ;
      slope =  0 ;
      LVA_RVA_t_u = (slope * d) + LVA_RVA_t ;
      /* Possible Saturation */
      
      LVA_RVA_ACTcell  = False;
      LVA_RVA_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_RVA!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (LVA_RVA_t > (5.0)) {
      LVA_RVA_t_u = 0 ;
      LVA_RVA_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( LVA_RVA_t <= (5.0)     ) {
      if ((pstate != cstate) || force_init_update) LVA_RVA_t_init = LVA_RVA_t ;
      slope =  1 ;
      LVA_RVA_t_u = (slope * d) + LVA_RVA_t ;
      /* Possible Saturation */
      
      LVA_RVA_ACTcell  = False;
      LVA_RVA_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_RVA!\n");
      exit(1);
    }
    break;
  }
  LVA_RVA_t = LVA_RVA_t_u;
  return cstate;
}