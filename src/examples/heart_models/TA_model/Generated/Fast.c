#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char Fast_ACTpath =  False ;
extern unsigned char  Fast_ACTcell1 ;//Events
static double  Fast_t  =  0 ; //the continuous vars
static double  Fast_t_u ; // and their updates
static double  Fast_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states Fast (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (Fast_ACTcell1) {
      Fast_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (Fast_t > (400.0)) {
      Fast_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( Fast_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) Fast_t_init = Fast_t ;
      slope =  1 ;
      Fast_t_u = (slope * d) + Fast_t ;
      /* Possible Saturation */
      
      Fast_ACTpath  = False;
      Fast_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (Fast_t > (10.0)) {
      Fast_t_u = 0 ;
      Fast_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( Fast_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) Fast_t_init = Fast_t ;
      slope =  1 ;
      Fast_t_u = (slope * d) + Fast_t ;
      /* Possible Saturation */
      
      Fast_ACTpath  = False;
      Fast_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (Fast_t > (200.0)) {
      Fast_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( Fast_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) Fast_t_init = Fast_t ;
      slope =  1 ;
      Fast_t_u = (slope * d) + Fast_t ;
      /* Possible Saturation */
      
      Fast_ACTpath  = False;
      Fast_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (Fast_ACTcell1) {
      Fast_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (Fast_t > (100.0)) {
      Fast_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( Fast_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) Fast_t_init = Fast_t ;
      slope =  1 ;
      Fast_t_u = (slope * d) + Fast_t ;
      /* Possible Saturation */
      
      Fast_ACTpath  = False;
      Fast_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast!\n");
      exit(1);
    }
    break;
  }
  Fast_t = Fast_t_u;
  return cstate;
}