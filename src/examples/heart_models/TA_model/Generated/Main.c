#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>



































































#define True 1
#define False 0
extern void readInput();
extern void writeOutput();
extern int SinoatrialNode(int, int);
extern int BachmannBundle(int, int);
extern int LeftAtrium(int, int);
extern int LeftAtrium1(int, int);
extern int RightAtrium(int, int);
extern int RightAtrium1(int, int);
extern int CoronarySinus(int, int);
extern int CristaTerminalis(int, int);
extern int CristaTerminalis1(int, int);
extern int Ostium(int, int);
extern int Fast(int, int);
extern int Fast1(int, int);
extern int Slow(int, int);
extern int Slow1(int, int);
extern int AtrioventricularNode(int, int);
extern int BundleOfHis(int, int);
extern int BundleOfHis1(int, int);
extern int BundleOfHis2(int, int);
extern int LeftBundleBranch(int, int);
extern int LeftBundleBranch1(int, int);
extern int LeftVentricularApex(int, int);
extern int LeftVentricle(int, int);
extern int LeftVentricle1(int, int);
extern int LeftVentricularSeptum(int, int);
extern int LeftVentricularSeptum1(int, int);
extern int CSLeftVentricular(int, int);
extern int RightBundleBranch(int, int);
extern int RightBundleBranch1(int, int);
extern int RightVentricularApex(int, int);
extern int RightVentricle(int, int);
extern int RightVentricle1(int, int);
extern int RightVentricularSeptum(int, int);
extern int RightVentricularSeptum1(int, int);
extern int SA_BB(int, int);
extern int SA_OS(int, int);
extern int SA_RA(int, int);
extern int SA_CT(int, int);
extern int BB_LA(int, int);
extern int LA_LA1(int, int);
extern int RA_RA1(int, int);
extern int RA1_CS(int, int);
extern int CT_CT1(int, int);
extern int OS_Fast(int, int);
extern int Fast_Fast1(int, int);
extern int OS_Slow(int, int);
extern int Slow_Slow1(int, int);
extern int Fast1_AV(int, int);
extern int Slow1_AV(int, int);
extern int AV_His(int, int);
extern int His_His1(int, int);
extern int His1_His2(int, int);
extern int His2_LBB(int, int);
extern int LBB_LBB1(int, int);
extern int LBB1_LVA(int, int);
extern int His2_RBB(int, int);
extern int RBB_RBB1(int, int);
extern int RBB1_RVA(int, int);
extern int LVA_RVA(int, int);
extern int LVA_LV(int, int);
extern int LV_LV1(int, int);
extern int LVA_LVS(int, int);
extern int LVS_LVS1(int, int);
extern int LVS1_CSLV(int, int);
extern int RVA_RV(int, int);
extern int RV_RV1(int, int);
extern int RVA_RVS(int, int);
extern int RVS_RVS1(int, int);
extern unsigned char BachmannBundle_ACTcell1;
extern unsigned char SA_BB_ACTcell;
extern unsigned char LeftAtrium_ACTcell1;
extern unsigned char BB_LA_ACTcell;
extern unsigned char LeftAtrium1_ACTcell1;
extern unsigned char LA_LA1_ACTcell;
extern unsigned char RightAtrium_ACTcell1;
extern unsigned char SA_RA_ACTcell;
extern unsigned char RightAtrium1_ACTcell1;
extern unsigned char RA_RA1_ACTcell;
extern unsigned char CoronarySinus_ACTcell1;
extern unsigned char RA1_CS_ACTcell;
extern unsigned char CristaTerminalis_ACTcell1;
extern unsigned char SA_CT_ACTcell;
extern unsigned char CristaTerminalis1_ACTcell1;
extern unsigned char CT_CT1_ACTcell;
extern unsigned char Ostium_ACTcell1;
extern unsigned char SA_OS_ACTcell;
extern unsigned char Fast_ACTcell1;
extern unsigned char OS_Fast_ACTcell;
extern unsigned char Fast1_ACTcell1;
extern unsigned char Fast_Fast1_ACTcell;
extern unsigned char Slow_ACTcell1;
extern unsigned char OS_Slow_ACTcell;
extern unsigned char Slow1_ACTcell1;
extern unsigned char Slow_Slow1_ACTcell;
extern unsigned char AtrioventricularNode_ACTcell1;
extern unsigned char Slow1_AV_ACTcell;
extern unsigned char BundleOfHis_ACTcell1;
extern unsigned char AV_His_ACTcell;
extern unsigned char BundleOfHis1_ACTcell1;
extern unsigned char His_His1_ACTcell;
extern unsigned char BundleOfHis2_ACTcell1;
extern unsigned char His1_His2_ACTcell;
extern unsigned char LeftBundleBranch_ACTcell1;
extern unsigned char His2_LBB_ACTcell;
extern unsigned char LeftBundleBranch1_ACTcell1;
extern unsigned char LBB_LBB1_ACTcell;
extern unsigned char LeftVentricularApex_ACTcell1;
extern unsigned char LBB1_LVA_ACTcell;
extern unsigned char RightBundleBranch_ACTcell1;
extern unsigned char His2_RBB_ACTcell;
extern unsigned char RightBundleBranch1_ACTcell1;
extern unsigned char RBB_RBB1_ACTcell;
extern unsigned char RightVentricularApex_ACTcell1;
extern unsigned char RBB1_RVA_ACTcell;
extern unsigned char LeftVentricle_ACTcell1;
extern unsigned char LVA_LV_ACTcell;
extern unsigned char LeftVentricle1_ACTcell1;
extern unsigned char LV_LV1_ACTcell;
extern unsigned char LeftVentricularSeptum_ACTcell1;
extern unsigned char LVA_LVS_ACTcell;
extern unsigned char LeftVentricularSeptum1_ACTcell1;
extern unsigned char LVS_LVS1_ACTcell;
extern unsigned char CSLeftVentricular_ACTcell1;
extern unsigned char LVS1_CSLV_ACTcell;
extern unsigned char RightVentricle_ACTcell1;
extern unsigned char RVA_RV_ACTcell;
extern unsigned char RightVentricle1_ACTcell1;
extern unsigned char RV_RV1_ACTcell;
extern unsigned char RightVentricularSeptum_ACTcell1;
extern unsigned char RVA_RVS_ACTcell;
extern unsigned char RightVentricularSeptum1_ACTcell1;
extern unsigned char RVS_RVS1_ACTcell;
extern unsigned char AtrioventricularNode_ACTcell2;
extern unsigned char Fast1_AV_ACTcell;
extern unsigned char SA_BB_ACTpath;
extern unsigned char SinoatrialNode_ACTpath;
extern unsigned char SA_OS_ACTpath;
extern unsigned char SinoatrialNode_ACTpath;
extern unsigned char SA_RA_ACTpath;
extern unsigned char SinoatrialNode_ACTpath;
extern unsigned char SA_CT_ACTpath;
extern unsigned char SinoatrialNode_ACTpath;
extern unsigned char BB_LA_ACTpath;
extern unsigned char BachmannBundle_ACTpath;
extern unsigned char LA_LA1_ACTpath;
extern unsigned char LeftAtrium_ACTpath;
extern unsigned char RA_RA1_ACTpath;
extern unsigned char RightAtrium_ACTpath;
extern unsigned char RA1_CS_ACTpath;
extern unsigned char RightAtrium1_ACTpath;
extern unsigned char CT_CT1_ACTpath;
extern unsigned char CristaTerminalis_ACTpath;
extern unsigned char OS_Fast_ACTpath;
extern unsigned char Ostium_ACTpath;
extern unsigned char OS_Slow_ACTpath;
extern unsigned char Ostium_ACTpath;
extern unsigned char Fast_Fast1_ACTpath;
extern unsigned char Fast_ACTpath;
extern unsigned char Fast1_AV_ACTpath;
extern unsigned char Fast1_ACTpath;
extern unsigned char Slow_Slow1_ACTpath;
extern unsigned char Slow_ACTpath;
extern unsigned char Slow1_AV_ACTpath;
extern unsigned char Slow1_ACTpath;
extern unsigned char AV_His_ACTpath;
extern unsigned char AtrioventricularNode_ACTpath;
extern unsigned char His_His1_ACTpath;
extern unsigned char BundleOfHis_ACTpath;
extern unsigned char His1_His2_ACTpath;
extern unsigned char BundleOfHis1_ACTpath;
extern unsigned char His2_LBB_ACTpath;
extern unsigned char BundleOfHis2_ACTpath;
extern unsigned char LBB_LBB1_ACTpath;
extern unsigned char LeftBundleBranch_ACTpath;
extern unsigned char LBB1_LVA_ACTpath;
extern unsigned char LeftBundleBranch1_ACTpath;
extern unsigned char His2_RBB_ACTpath;
extern unsigned char BundleOfHis2_ACTpath;
extern unsigned char RBB_RBB1_ACTpath;
extern unsigned char RightBundleBranch_ACTpath;
extern unsigned char RBB1_RVA_ACTpath;
extern unsigned char RightBundleBranch1_ACTpath;
extern unsigned char LVA_LV_ACTpath;
extern unsigned char LeftVentricularApex_ACTpath;
extern unsigned char LV_LV1_ACTpath;
extern unsigned char LeftVentricle_ACTpath;
extern unsigned char LVA_LVS_ACTpath;
extern unsigned char LeftVentricularApex_ACTpath;
extern unsigned char LVS_LVS1_ACTpath;
extern unsigned char LeftVentricularSeptum_ACTpath;
extern unsigned char LVS1_CSLV_ACTpath;
extern unsigned char LeftVentricularSeptum1_ACTpath;
extern unsigned char RVA_RV_ACTpath;
extern unsigned char RightVentricularApex_ACTpath;
extern unsigned char RV_RV1_ACTpath;
extern unsigned char RightVentricle_ACTpath;
extern unsigned char RVA_RVS_ACTpath;
extern unsigned char RightVentricularApex_ACTpath;
extern unsigned char RVS_RVS1_ACTpath;
extern unsigned char RightVentricularSeptum_ACTpath;
int main (void){
  int SinoatrialNode_cstate = 0 ;
  int SinoatrialNode_pstate = -1;
  int BachmannBundle_cstate = 0 ;
  int BachmannBundle_pstate = -1;
  int LeftAtrium_cstate = 0 ;
  int LeftAtrium_pstate = -1;
  int LeftAtrium1_cstate = 0 ;
  int LeftAtrium1_pstate = -1;
  int RightAtrium_cstate = 0 ;
  int RightAtrium_pstate = -1;
  int RightAtrium1_cstate = 0 ;
  int RightAtrium1_pstate = -1;
  int CoronarySinus_cstate = 0 ;
  int CoronarySinus_pstate = -1;
  int CristaTerminalis_cstate = 0 ;
  int CristaTerminalis_pstate = -1;
  int CristaTerminalis1_cstate = 0 ;
  int CristaTerminalis1_pstate = -1;
  int Ostium_cstate = 0 ;
  int Ostium_pstate = -1;
  int Fast_cstate = 0 ;
  int Fast_pstate = -1;
  int Fast1_cstate = 0 ;
  int Fast1_pstate = -1;
  int Slow_cstate = 0 ;
  int Slow_pstate = -1;
  int Slow1_cstate = 0 ;
  int Slow1_pstate = -1;
  int AtrioventricularNode_cstate = 0 ;
  int AtrioventricularNode_pstate = -1;
  int BundleOfHis_cstate = 0 ;
  int BundleOfHis_pstate = -1;
  int BundleOfHis1_cstate = 0 ;
  int BundleOfHis1_pstate = -1;
  int BundleOfHis2_cstate = 0 ;
  int BundleOfHis2_pstate = -1;
  int LeftBundleBranch_cstate = 0 ;
  int LeftBundleBranch_pstate = -1;
  int LeftBundleBranch1_cstate = 0 ;
  int LeftBundleBranch1_pstate = -1;
  int LeftVentricularApex_cstate = 0 ;
  int LeftVentricularApex_pstate = -1;
  int LeftVentricle_cstate = 0 ;
  int LeftVentricle_pstate = -1;
  int LeftVentricle1_cstate = 0 ;
  int LeftVentricle1_pstate = -1;
  int LeftVentricularSeptum_cstate = 0 ;
  int LeftVentricularSeptum_pstate = -1;
  int LeftVentricularSeptum1_cstate = 0 ;
  int LeftVentricularSeptum1_pstate = -1;
  int CSLeftVentricular_cstate = 0 ;
  int CSLeftVentricular_pstate = -1;
  int RightBundleBranch_cstate = 0 ;
  int RightBundleBranch_pstate = -1;
  int RightBundleBranch1_cstate = 0 ;
  int RightBundleBranch1_pstate = -1;
  int RightVentricularApex_cstate = 0 ;
  int RightVentricularApex_pstate = -1;
  int RightVentricle_cstate = 0 ;
  int RightVentricle_pstate = -1;
  int RightVentricle1_cstate = 0 ;
  int RightVentricle1_pstate = -1;
  int RightVentricularSeptum_cstate = 0 ;
  int RightVentricularSeptum_pstate = -1;
  int RightVentricularSeptum1_cstate = 0 ;
  int RightVentricularSeptum1_pstate = -1;
  int SA_BB_cstate = 0 ;
  int SA_BB_pstate = -1;
  int SA_OS_cstate = 0 ;
  int SA_OS_pstate = -1;
  int SA_RA_cstate = 0 ;
  int SA_RA_pstate = -1;
  int SA_CT_cstate = 0 ;
  int SA_CT_pstate = -1;
  int BB_LA_cstate = 0 ;
  int BB_LA_pstate = -1;
  int LA_LA1_cstate = 0 ;
  int LA_LA1_pstate = -1;
  int RA_RA1_cstate = 0 ;
  int RA_RA1_pstate = -1;
  int RA1_CS_cstate = 0 ;
  int RA1_CS_pstate = -1;
  int CT_CT1_cstate = 0 ;
  int CT_CT1_pstate = -1;
  int OS_Fast_cstate = 0 ;
  int OS_Fast_pstate = -1;
  int Fast_Fast1_cstate = 0 ;
  int Fast_Fast1_pstate = -1;
  int OS_Slow_cstate = 0 ;
  int OS_Slow_pstate = -1;
  int Slow_Slow1_cstate = 0 ;
  int Slow_Slow1_pstate = -1;
  int Fast1_AV_cstate = 0 ;
  int Fast1_AV_pstate = -1;
  int Slow1_AV_cstate = 0 ;
  int Slow1_AV_pstate = -1;
  int AV_His_cstate = 0 ;
  int AV_His_pstate = -1;
  int His_His1_cstate = 0 ;
  int His_His1_pstate = -1;
  int His1_His2_cstate = 0 ;
  int His1_His2_pstate = -1;
  int His2_LBB_cstate = 0 ;
  int His2_LBB_pstate = -1;
  int LBB_LBB1_cstate = 0 ;
  int LBB_LBB1_pstate = -1;
  int LBB1_LVA_cstate = 0 ;
  int LBB1_LVA_pstate = -1;
  int His2_RBB_cstate = 0 ;
  int His2_RBB_pstate = -1;
  int RBB_RBB1_cstate = 0 ;
  int RBB_RBB1_pstate = -1;
  int RBB1_RVA_cstate = 0 ;
  int RBB1_RVA_pstate = -1;
  int LVA_RVA_cstate = 0 ;
  int LVA_RVA_pstate = -1;
  int LVA_LV_cstate = 0 ;
  int LVA_LV_pstate = -1;
  int LV_LV1_cstate = 0 ;
  int LV_LV1_pstate = -1;
  int LVA_LVS_cstate = 0 ;
  int LVA_LVS_pstate = -1;
  int LVS_LVS1_cstate = 0 ;
  int LVS_LVS1_pstate = -1;
  int LVS1_CSLV_cstate = 0 ;
  int LVS1_CSLV_pstate = -1;
  int RVA_RV_cstate = 0 ;
  int RVA_RV_pstate = -1;
  int RV_RV1_cstate = 0 ;
  int RV_RV1_pstate = -1;
  int RVA_RVS_cstate = 0 ;
  int RVA_RVS_pstate = -1;
  int RVS_RVS1_cstate = 0 ;
  int RVS_RVS1_pstate = -1;
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  while(True) {
    readInput();
    BachmannBundle_ACTcell1 = SA_BB_ACTcell;
    LeftAtrium_ACTcell1 = BB_LA_ACTcell;
    LeftAtrium1_ACTcell1 = LA_LA1_ACTcell;
    RightAtrium_ACTcell1 = SA_RA_ACTcell;
    RightAtrium1_ACTcell1 = RA_RA1_ACTcell;
    CoronarySinus_ACTcell1 = RA1_CS_ACTcell;
    CristaTerminalis_ACTcell1 = SA_CT_ACTcell;
    CristaTerminalis1_ACTcell1 = CT_CT1_ACTcell;
    Ostium_ACTcell1 = SA_OS_ACTcell;
    Fast_ACTcell1 = OS_Fast_ACTcell;
    Fast1_ACTcell1 = Fast_Fast1_ACTcell;
    Slow_ACTcell1 = OS_Slow_ACTcell;
    Slow1_ACTcell1 = Slow_Slow1_ACTcell;
    AtrioventricularNode_ACTcell1 = Slow1_AV_ACTcell;
    BundleOfHis_ACTcell1 = AV_His_ACTcell;
    BundleOfHis1_ACTcell1 = His_His1_ACTcell;
    BundleOfHis2_ACTcell1 = His1_His2_ACTcell;
    LeftBundleBranch_ACTcell1 = His2_LBB_ACTcell;
    LeftBundleBranch1_ACTcell1 = LBB_LBB1_ACTcell;
    LeftVentricularApex_ACTcell1 = LBB1_LVA_ACTcell;
    RightBundleBranch_ACTcell1 = His2_RBB_ACTcell;
    RightBundleBranch1_ACTcell1 = RBB_RBB1_ACTcell;
    RightVentricularApex_ACTcell1 = RBB1_RVA_ACTcell;
    LeftVentricle_ACTcell1 = LVA_LV_ACTcell;
    LeftVentricle1_ACTcell1 = LV_LV1_ACTcell;
    LeftVentricularSeptum_ACTcell1 = LVA_LVS_ACTcell;
    LeftVentricularSeptum1_ACTcell1 = LVS_LVS1_ACTcell;
    CSLeftVentricular_ACTcell1 = LVS1_CSLV_ACTcell;
    RightVentricle_ACTcell1 = RVA_RV_ACTcell;
    RightVentricle1_ACTcell1 = RV_RV1_ACTcell;
    RightVentricularSeptum_ACTcell1 = RVA_RVS_ACTcell;
    RightVentricularSeptum1_ACTcell1 = RVS_RVS1_ACTcell;
    AtrioventricularNode_ACTcell2 = Fast1_AV_ACTcell;
    SA_BB_ACTpath = SinoatrialNode_ACTpath;
    SA_OS_ACTpath = SinoatrialNode_ACTpath;
    SA_RA_ACTpath = SinoatrialNode_ACTpath;
    SA_CT_ACTpath = SinoatrialNode_ACTpath;
    BB_LA_ACTpath = BachmannBundle_ACTpath;
    LA_LA1_ACTpath = LeftAtrium_ACTpath;
    RA_RA1_ACTpath = RightAtrium_ACTpath;
    RA1_CS_ACTpath = RightAtrium1_ACTpath;
    CT_CT1_ACTpath = CristaTerminalis_ACTpath;
    OS_Fast_ACTpath = Ostium_ACTpath;
    OS_Slow_ACTpath = Ostium_ACTpath;
    Fast_Fast1_ACTpath = Fast_ACTpath;
    Fast1_AV_ACTpath = Fast1_ACTpath;
    Slow_Slow1_ACTpath = Slow_ACTpath;
    Slow1_AV_ACTpath = Slow1_ACTpath;
    AV_His_ACTpath = AtrioventricularNode_ACTpath;
    His_His1_ACTpath = BundleOfHis_ACTpath;
    His1_His2_ACTpath = BundleOfHis1_ACTpath;
    His2_LBB_ACTpath = BundleOfHis2_ACTpath;
    LBB_LBB1_ACTpath = LeftBundleBranch_ACTpath;
    LBB1_LVA_ACTpath = LeftBundleBranch1_ACTpath;
    His2_RBB_ACTpath = BundleOfHis2_ACTpath;
    RBB_RBB1_ACTpath = RightBundleBranch_ACTpath;
    RBB1_RVA_ACTpath = RightBundleBranch1_ACTpath;
    LVA_LV_ACTpath = LeftVentricularApex_ACTpath;
    LV_LV1_ACTpath = LeftVentricle_ACTpath;
    LVA_LVS_ACTpath = LeftVentricularApex_ACTpath;
    LVS_LVS1_ACTpath = LeftVentricularSeptum_ACTpath;
    LVS1_CSLV_ACTpath = LeftVentricularSeptum1_ACTpath;
    RVA_RV_ACTpath = RightVentricularApex_ACTpath;
    RV_RV1_ACTpath = RightVentricle_ACTpath;
    RVA_RVS_ACTpath = RightVentricularApex_ACTpath;
    RVS_RVS1_ACTpath = RightVentricularSeptum_ACTpath;
    int SinoatrialNode_rstate = SinoatrialNode (SinoatrialNode_cstate, SinoatrialNode_pstate);
    SinoatrialNode_pstate = SinoatrialNode_cstate;
    SinoatrialNode_cstate = SinoatrialNode_rstate;
    int BachmannBundle_rstate = BachmannBundle (BachmannBundle_cstate, BachmannBundle_pstate);
    BachmannBundle_pstate = BachmannBundle_cstate;
    BachmannBundle_cstate = BachmannBundle_rstate;
    int LeftAtrium_rstate = LeftAtrium (LeftAtrium_cstate, LeftAtrium_pstate);
    LeftAtrium_pstate = LeftAtrium_cstate;
    LeftAtrium_cstate = LeftAtrium_rstate;
    int LeftAtrium1_rstate = LeftAtrium1 (LeftAtrium1_cstate, LeftAtrium1_pstate);
    LeftAtrium1_pstate = LeftAtrium1_cstate;
    LeftAtrium1_cstate = LeftAtrium1_rstate;
    int RightAtrium_rstate = RightAtrium (RightAtrium_cstate, RightAtrium_pstate);
    RightAtrium_pstate = RightAtrium_cstate;
    RightAtrium_cstate = RightAtrium_rstate;
    int RightAtrium1_rstate = RightAtrium1 (RightAtrium1_cstate, RightAtrium1_pstate);
    RightAtrium1_pstate = RightAtrium1_cstate;
    RightAtrium1_cstate = RightAtrium1_rstate;
    int CoronarySinus_rstate = CoronarySinus (CoronarySinus_cstate, CoronarySinus_pstate);
    CoronarySinus_pstate = CoronarySinus_cstate;
    CoronarySinus_cstate = CoronarySinus_rstate;
    int CristaTerminalis_rstate = CristaTerminalis (CristaTerminalis_cstate, CristaTerminalis_pstate);
    CristaTerminalis_pstate = CristaTerminalis_cstate;
    CristaTerminalis_cstate = CristaTerminalis_rstate;
    int CristaTerminalis1_rstate = CristaTerminalis1 (CristaTerminalis1_cstate, CristaTerminalis1_pstate);
    CristaTerminalis1_pstate = CristaTerminalis1_cstate;
    CristaTerminalis1_cstate = CristaTerminalis1_rstate;
    int Ostium_rstate = Ostium (Ostium_cstate, Ostium_pstate);
    Ostium_pstate = Ostium_cstate;
    Ostium_cstate = Ostium_rstate;
    int Fast_rstate = Fast (Fast_cstate, Fast_pstate);
    Fast_pstate = Fast_cstate;
    Fast_cstate = Fast_rstate;
    int Fast1_rstate = Fast1 (Fast1_cstate, Fast1_pstate);
    Fast1_pstate = Fast1_cstate;
    Fast1_cstate = Fast1_rstate;
    int Slow_rstate = Slow (Slow_cstate, Slow_pstate);
    Slow_pstate = Slow_cstate;
    Slow_cstate = Slow_rstate;
    int Slow1_rstate = Slow1 (Slow1_cstate, Slow1_pstate);
    Slow1_pstate = Slow1_cstate;
    Slow1_cstate = Slow1_rstate;
    int AtrioventricularNode_rstate = AtrioventricularNode (AtrioventricularNode_cstate, AtrioventricularNode_pstate);
    AtrioventricularNode_pstate = AtrioventricularNode_cstate;
    AtrioventricularNode_cstate = AtrioventricularNode_rstate;
    int BundleOfHis_rstate = BundleOfHis (BundleOfHis_cstate, BundleOfHis_pstate);
    BundleOfHis_pstate = BundleOfHis_cstate;
    BundleOfHis_cstate = BundleOfHis_rstate;
    int BundleOfHis1_rstate = BundleOfHis1 (BundleOfHis1_cstate, BundleOfHis1_pstate);
    BundleOfHis1_pstate = BundleOfHis1_cstate;
    BundleOfHis1_cstate = BundleOfHis1_rstate;
    int BundleOfHis2_rstate = BundleOfHis2 (BundleOfHis2_cstate, BundleOfHis2_pstate);
    BundleOfHis2_pstate = BundleOfHis2_cstate;
    BundleOfHis2_cstate = BundleOfHis2_rstate;
    int LeftBundleBranch_rstate = LeftBundleBranch (LeftBundleBranch_cstate, LeftBundleBranch_pstate);
    LeftBundleBranch_pstate = LeftBundleBranch_cstate;
    LeftBundleBranch_cstate = LeftBundleBranch_rstate;
    int LeftBundleBranch1_rstate = LeftBundleBranch1 (LeftBundleBranch1_cstate, LeftBundleBranch1_pstate);
    LeftBundleBranch1_pstate = LeftBundleBranch1_cstate;
    LeftBundleBranch1_cstate = LeftBundleBranch1_rstate;
    int LeftVentricularApex_rstate = LeftVentricularApex (LeftVentricularApex_cstate, LeftVentricularApex_pstate);
    LeftVentricularApex_pstate = LeftVentricularApex_cstate;
    LeftVentricularApex_cstate = LeftVentricularApex_rstate;
    int LeftVentricle_rstate = LeftVentricle (LeftVentricle_cstate, LeftVentricle_pstate);
    LeftVentricle_pstate = LeftVentricle_cstate;
    LeftVentricle_cstate = LeftVentricle_rstate;
    int LeftVentricle1_rstate = LeftVentricle1 (LeftVentricle1_cstate, LeftVentricle1_pstate);
    LeftVentricle1_pstate = LeftVentricle1_cstate;
    LeftVentricle1_cstate = LeftVentricle1_rstate;
    int LeftVentricularSeptum_rstate = LeftVentricularSeptum (LeftVentricularSeptum_cstate, LeftVentricularSeptum_pstate);
    LeftVentricularSeptum_pstate = LeftVentricularSeptum_cstate;
    LeftVentricularSeptum_cstate = LeftVentricularSeptum_rstate;
    int LeftVentricularSeptum1_rstate = LeftVentricularSeptum1 (LeftVentricularSeptum1_cstate, LeftVentricularSeptum1_pstate);
    LeftVentricularSeptum1_pstate = LeftVentricularSeptum1_cstate;
    LeftVentricularSeptum1_cstate = LeftVentricularSeptum1_rstate;
    int CSLeftVentricular_rstate = CSLeftVentricular (CSLeftVentricular_cstate, CSLeftVentricular_pstate);
    CSLeftVentricular_pstate = CSLeftVentricular_cstate;
    CSLeftVentricular_cstate = CSLeftVentricular_rstate;
    int RightBundleBranch_rstate = RightBundleBranch (RightBundleBranch_cstate, RightBundleBranch_pstate);
    RightBundleBranch_pstate = RightBundleBranch_cstate;
    RightBundleBranch_cstate = RightBundleBranch_rstate;
    int RightBundleBranch1_rstate = RightBundleBranch1 (RightBundleBranch1_cstate, RightBundleBranch1_pstate);
    RightBundleBranch1_pstate = RightBundleBranch1_cstate;
    RightBundleBranch1_cstate = RightBundleBranch1_rstate;
    int RightVentricularApex_rstate = RightVentricularApex (RightVentricularApex_cstate, RightVentricularApex_pstate);
    RightVentricularApex_pstate = RightVentricularApex_cstate;
    RightVentricularApex_cstate = RightVentricularApex_rstate;
    int RightVentricle_rstate = RightVentricle (RightVentricle_cstate, RightVentricle_pstate);
    RightVentricle_pstate = RightVentricle_cstate;
    RightVentricle_cstate = RightVentricle_rstate;
    int RightVentricle1_rstate = RightVentricle1 (RightVentricle1_cstate, RightVentricle1_pstate);
    RightVentricle1_pstate = RightVentricle1_cstate;
    RightVentricle1_cstate = RightVentricle1_rstate;
    int RightVentricularSeptum_rstate = RightVentricularSeptum (RightVentricularSeptum_cstate, RightVentricularSeptum_pstate);
    RightVentricularSeptum_pstate = RightVentricularSeptum_cstate;
    RightVentricularSeptum_cstate = RightVentricularSeptum_rstate;
    int RightVentricularSeptum1_rstate = RightVentricularSeptum1 (RightVentricularSeptum1_cstate, RightVentricularSeptum1_pstate);
    RightVentricularSeptum1_pstate = RightVentricularSeptum1_cstate;
    RightVentricularSeptum1_cstate = RightVentricularSeptum1_rstate;
    int SA_BB_rstate = SA_BB (SA_BB_cstate, SA_BB_pstate);
    SA_BB_pstate = SA_BB_cstate;
    SA_BB_cstate = SA_BB_rstate;
    int SA_OS_rstate = SA_OS (SA_OS_cstate, SA_OS_pstate);
    SA_OS_pstate = SA_OS_cstate;
    SA_OS_cstate = SA_OS_rstate;
    int SA_RA_rstate = SA_RA (SA_RA_cstate, SA_RA_pstate);
    SA_RA_pstate = SA_RA_cstate;
    SA_RA_cstate = SA_RA_rstate;
    int SA_CT_rstate = SA_CT (SA_CT_cstate, SA_CT_pstate);
    SA_CT_pstate = SA_CT_cstate;
    SA_CT_cstate = SA_CT_rstate;
    int BB_LA_rstate = BB_LA (BB_LA_cstate, BB_LA_pstate);
    BB_LA_pstate = BB_LA_cstate;
    BB_LA_cstate = BB_LA_rstate;
    int LA_LA1_rstate = LA_LA1 (LA_LA1_cstate, LA_LA1_pstate);
    LA_LA1_pstate = LA_LA1_cstate;
    LA_LA1_cstate = LA_LA1_rstate;
    int RA_RA1_rstate = RA_RA1 (RA_RA1_cstate, RA_RA1_pstate);
    RA_RA1_pstate = RA_RA1_cstate;
    RA_RA1_cstate = RA_RA1_rstate;
    int RA1_CS_rstate = RA1_CS (RA1_CS_cstate, RA1_CS_pstate);
    RA1_CS_pstate = RA1_CS_cstate;
    RA1_CS_cstate = RA1_CS_rstate;
    int CT_CT1_rstate = CT_CT1 (CT_CT1_cstate, CT_CT1_pstate);
    CT_CT1_pstate = CT_CT1_cstate;
    CT_CT1_cstate = CT_CT1_rstate;
    int OS_Fast_rstate = OS_Fast (OS_Fast_cstate, OS_Fast_pstate);
    OS_Fast_pstate = OS_Fast_cstate;
    OS_Fast_cstate = OS_Fast_rstate;
    int Fast_Fast1_rstate = Fast_Fast1 (Fast_Fast1_cstate, Fast_Fast1_pstate);
    Fast_Fast1_pstate = Fast_Fast1_cstate;
    Fast_Fast1_cstate = Fast_Fast1_rstate;
    int OS_Slow_rstate = OS_Slow (OS_Slow_cstate, OS_Slow_pstate);
    OS_Slow_pstate = OS_Slow_cstate;
    OS_Slow_cstate = OS_Slow_rstate;
    int Slow_Slow1_rstate = Slow_Slow1 (Slow_Slow1_cstate, Slow_Slow1_pstate);
    Slow_Slow1_pstate = Slow_Slow1_cstate;
    Slow_Slow1_cstate = Slow_Slow1_rstate;
    int Fast1_AV_rstate = Fast1_AV (Fast1_AV_cstate, Fast1_AV_pstate);
    Fast1_AV_pstate = Fast1_AV_cstate;
    Fast1_AV_cstate = Fast1_AV_rstate;
    int Slow1_AV_rstate = Slow1_AV (Slow1_AV_cstate, Slow1_AV_pstate);
    Slow1_AV_pstate = Slow1_AV_cstate;
    Slow1_AV_cstate = Slow1_AV_rstate;
    int AV_His_rstate = AV_His (AV_His_cstate, AV_His_pstate);
    AV_His_pstate = AV_His_cstate;
    AV_His_cstate = AV_His_rstate;
    int His_His1_rstate = His_His1 (His_His1_cstate, His_His1_pstate);
    His_His1_pstate = His_His1_cstate;
    His_His1_cstate = His_His1_rstate;
    int His1_His2_rstate = His1_His2 (His1_His2_cstate, His1_His2_pstate);
    His1_His2_pstate = His1_His2_cstate;
    His1_His2_cstate = His1_His2_rstate;
    int His2_LBB_rstate = His2_LBB (His2_LBB_cstate, His2_LBB_pstate);
    His2_LBB_pstate = His2_LBB_cstate;
    His2_LBB_cstate = His2_LBB_rstate;
    int LBB_LBB1_rstate = LBB_LBB1 (LBB_LBB1_cstate, LBB_LBB1_pstate);
    LBB_LBB1_pstate = LBB_LBB1_cstate;
    LBB_LBB1_cstate = LBB_LBB1_rstate;
    int LBB1_LVA_rstate = LBB1_LVA (LBB1_LVA_cstate, LBB1_LVA_pstate);
    LBB1_LVA_pstate = LBB1_LVA_cstate;
    LBB1_LVA_cstate = LBB1_LVA_rstate;
    int His2_RBB_rstate = His2_RBB (His2_RBB_cstate, His2_RBB_pstate);
    His2_RBB_pstate = His2_RBB_cstate;
    His2_RBB_cstate = His2_RBB_rstate;
    int RBB_RBB1_rstate = RBB_RBB1 (RBB_RBB1_cstate, RBB_RBB1_pstate);
    RBB_RBB1_pstate = RBB_RBB1_cstate;
    RBB_RBB1_cstate = RBB_RBB1_rstate;
    int RBB1_RVA_rstate = RBB1_RVA (RBB1_RVA_cstate, RBB1_RVA_pstate);
    RBB1_RVA_pstate = RBB1_RVA_cstate;
    RBB1_RVA_cstate = RBB1_RVA_rstate;
    int LVA_RVA_rstate = LVA_RVA (LVA_RVA_cstate, LVA_RVA_pstate);
    LVA_RVA_pstate = LVA_RVA_cstate;
    LVA_RVA_cstate = LVA_RVA_rstate;
    int LVA_LV_rstate = LVA_LV (LVA_LV_cstate, LVA_LV_pstate);
    LVA_LV_pstate = LVA_LV_cstate;
    LVA_LV_cstate = LVA_LV_rstate;
    int LV_LV1_rstate = LV_LV1 (LV_LV1_cstate, LV_LV1_pstate);
    LV_LV1_pstate = LV_LV1_cstate;
    LV_LV1_cstate = LV_LV1_rstate;
    int LVA_LVS_rstate = LVA_LVS (LVA_LVS_cstate, LVA_LVS_pstate);
    LVA_LVS_pstate = LVA_LVS_cstate;
    LVA_LVS_cstate = LVA_LVS_rstate;
    int LVS_LVS1_rstate = LVS_LVS1 (LVS_LVS1_cstate, LVS_LVS1_pstate);
    LVS_LVS1_pstate = LVS_LVS1_cstate;
    LVS_LVS1_cstate = LVS_LVS1_rstate;
    int LVS1_CSLV_rstate = LVS1_CSLV (LVS1_CSLV_cstate, LVS1_CSLV_pstate);
    LVS1_CSLV_pstate = LVS1_CSLV_cstate;
    LVS1_CSLV_cstate = LVS1_CSLV_rstate;
    int RVA_RV_rstate = RVA_RV (RVA_RV_cstate, RVA_RV_pstate);
    RVA_RV_pstate = RVA_RV_cstate;
    RVA_RV_cstate = RVA_RV_rstate;
    int RV_RV1_rstate = RV_RV1 (RV_RV1_cstate, RV_RV1_pstate);
    RV_RV1_pstate = RV_RV1_cstate;
    RV_RV1_cstate = RV_RV1_rstate;
    int RVA_RVS_rstate = RVA_RVS (RVA_RVS_cstate, RVA_RVS_pstate);
    RVA_RVS_pstate = RVA_RVS_cstate;
    RVA_RVS_cstate = RVA_RVS_rstate;
    int RVS_RVS1_rstate = RVS_RVS1 (RVS_RVS1_cstate, RVS_RVS1_pstate);
    RVS_RVS1_pstate = RVS_RVS1_cstate;
    RVS_RVS1_cstate = RVS_RVS1_rstate;
    writeOutput();
  }
  return 0;
}