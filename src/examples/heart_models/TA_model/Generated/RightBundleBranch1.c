#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char RightBundleBranch1_ACTpath =  False ;
extern unsigned char  RightBundleBranch1_ACTcell1 ;//Events
static double  RightBundleBranch1_t  =  0 ; //the continuous vars
static double  RightBundleBranch1_t_u ; // and their updates
static double  RightBundleBranch1_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states RightBundleBranch1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (RightBundleBranch1_ACTcell1) {
      RightBundleBranch1_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (RightBundleBranch1_t > (400.0)) {
      RightBundleBranch1_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( RightBundleBranch1_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) RightBundleBranch1_t_init = RightBundleBranch1_t ;
      slope =  1 ;
      RightBundleBranch1_t_u = (slope * d) + RightBundleBranch1_t ;
      /* Possible Saturation */
      
      RightBundleBranch1_ACTpath  = False;
      RightBundleBranch1_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightBundleBranch1!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (RightBundleBranch1_t > (10.0)) {
      RightBundleBranch1_t_u = 0 ;
      RightBundleBranch1_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( RightBundleBranch1_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) RightBundleBranch1_t_init = RightBundleBranch1_t ;
      slope =  1 ;
      RightBundleBranch1_t_u = (slope * d) + RightBundleBranch1_t ;
      /* Possible Saturation */
      
      RightBundleBranch1_ACTpath  = False;
      RightBundleBranch1_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightBundleBranch1!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (RightBundleBranch1_t > (200.0)) {
      RightBundleBranch1_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( RightBundleBranch1_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) RightBundleBranch1_t_init = RightBundleBranch1_t ;
      slope =  1 ;
      RightBundleBranch1_t_u = (slope * d) + RightBundleBranch1_t ;
      /* Possible Saturation */
      
      RightBundleBranch1_ACTpath  = False;
      RightBundleBranch1_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightBundleBranch1!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (RightBundleBranch1_ACTcell1) {
      RightBundleBranch1_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (RightBundleBranch1_t > (100.0)) {
      RightBundleBranch1_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( RightBundleBranch1_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) RightBundleBranch1_t_init = RightBundleBranch1_t ;
      slope =  1 ;
      RightBundleBranch1_t_u = (slope * d) + RightBundleBranch1_t ;
      /* Possible Saturation */
      
      RightBundleBranch1_ACTpath  = False;
      RightBundleBranch1_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightBundleBranch1!\n");
      exit(1);
    }
    break;
  }
  RightBundleBranch1_t = RightBundleBranch1_t_u;
  return cstate;
}