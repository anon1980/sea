#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char LeftBundleBranch_ACTpath =  False ;
extern unsigned char  LeftBundleBranch_ACTcell1 ;//Events
static double  LeftBundleBranch_t  =  0 ; //the continuous vars
static double  LeftBundleBranch_t_u ; // and their updates
static double  LeftBundleBranch_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states LeftBundleBranch (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (LeftBundleBranch_ACTcell1) {
      LeftBundleBranch_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (LeftBundleBranch_t > (400.0)) {
      LeftBundleBranch_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( LeftBundleBranch_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftBundleBranch_t_init = LeftBundleBranch_t ;
      slope =  1 ;
      LeftBundleBranch_t_u = (slope * d) + LeftBundleBranch_t ;
      /* Possible Saturation */
      
      LeftBundleBranch_ACTpath  = False;
      LeftBundleBranch_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftBundleBranch!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (LeftBundleBranch_t > (10.0)) {
      LeftBundleBranch_t_u = 0 ;
      LeftBundleBranch_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( LeftBundleBranch_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftBundleBranch_t_init = LeftBundleBranch_t ;
      slope =  1 ;
      LeftBundleBranch_t_u = (slope * d) + LeftBundleBranch_t ;
      /* Possible Saturation */
      
      LeftBundleBranch_ACTpath  = False;
      LeftBundleBranch_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftBundleBranch!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (LeftBundleBranch_t > (200.0)) {
      LeftBundleBranch_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( LeftBundleBranch_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftBundleBranch_t_init = LeftBundleBranch_t ;
      slope =  1 ;
      LeftBundleBranch_t_u = (slope * d) + LeftBundleBranch_t ;
      /* Possible Saturation */
      
      LeftBundleBranch_ACTpath  = False;
      LeftBundleBranch_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftBundleBranch!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (LeftBundleBranch_ACTcell1) {
      LeftBundleBranch_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (LeftBundleBranch_t > (100.0)) {
      LeftBundleBranch_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( LeftBundleBranch_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftBundleBranch_t_init = LeftBundleBranch_t ;
      slope =  1 ;
      LeftBundleBranch_t_u = (slope * d) + LeftBundleBranch_t ;
      /* Possible Saturation */
      
      LeftBundleBranch_ACTpath  = False;
      LeftBundleBranch_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftBundleBranch!\n");
      exit(1);
    }
    break;
  }
  LeftBundleBranch_t = LeftBundleBranch_t_u;
  return cstate;
}