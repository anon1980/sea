#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char LeftAtrium_ACTpath =  False ;
extern unsigned char  LeftAtrium_ACTcell1 ;//Events
static double  LeftAtrium_t  =  0 ; //the continuous vars
static double  LeftAtrium_t_u ; // and their updates
static double  LeftAtrium_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states LeftAtrium (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (LeftAtrium_ACTcell1) {
      LeftAtrium_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (LeftAtrium_t > (400.0)) {
      LeftAtrium_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( LeftAtrium_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftAtrium_t_init = LeftAtrium_t ;
      slope =  1 ;
      LeftAtrium_t_u = (slope * d) + LeftAtrium_t ;
      /* Possible Saturation */
      
      LeftAtrium_ACTpath  = False;
      LeftAtrium_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftAtrium!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (LeftAtrium_t > (10.0)) {
      LeftAtrium_t_u = 0 ;
      LeftAtrium_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( LeftAtrium_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftAtrium_t_init = LeftAtrium_t ;
      slope =  1 ;
      LeftAtrium_t_u = (slope * d) + LeftAtrium_t ;
      /* Possible Saturation */
      
      LeftAtrium_ACTpath  = False;
      LeftAtrium_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftAtrium!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (LeftAtrium_t > (200.0)) {
      LeftAtrium_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( LeftAtrium_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftAtrium_t_init = LeftAtrium_t ;
      slope =  1 ;
      LeftAtrium_t_u = (slope * d) + LeftAtrium_t ;
      /* Possible Saturation */
      
      LeftAtrium_ACTpath  = False;
      LeftAtrium_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftAtrium!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (LeftAtrium_ACTcell1) {
      LeftAtrium_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (LeftAtrium_t > (100.0)) {
      LeftAtrium_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( LeftAtrium_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftAtrium_t_init = LeftAtrium_t ;
      slope =  1 ;
      LeftAtrium_t_u = (slope * d) + LeftAtrium_t ;
      /* Possible Saturation */
      
      LeftAtrium_ACTpath  = False;
      LeftAtrium_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftAtrium!\n");
      exit(1);
    }
    break;
  }
  LeftAtrium_t = LeftAtrium_t_u;
  return cstate;
}