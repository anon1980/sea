#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char His2_LBB_ACTcell =  False ;
extern unsigned char  His2_LBB_ACTpath ;//Events
static double  His2_LBB_t  =  0 ; //the continuous vars
static double  His2_LBB_t_u ; // and their updates
static double  His2_LBB_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states His2_LBB (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (His2_LBB_ACTpath) {
      His2_LBB_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His2_LBB_t_init = His2_LBB_t ;
      slope =  0 ;
      His2_LBB_t_u = (slope * d) + His2_LBB_t ;
      /* Possible Saturation */
      
      His2_LBB_ACTcell  = False;
      His2_LBB_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_LBB!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (His2_LBB_t > (20.0)) {
      His2_LBB_t_u = 0 ;
      His2_LBB_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( His2_LBB_t <= (20.0)     ) {
      if ((pstate != cstate) || force_init_update) His2_LBB_t_init = His2_LBB_t ;
      slope =  1 ;
      His2_LBB_t_u = (slope * d) + His2_LBB_t ;
      /* Possible Saturation */
      
      His2_LBB_ACTcell  = False;
      His2_LBB_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_LBB!\n");
      exit(1);
    }
    break;
  }
  His2_LBB_t = His2_LBB_t_u;
  return cstate;
}