#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char RightVentricularSeptum_ACTpath =  False ;
extern unsigned char  RightVentricularSeptum_ACTcell1 ;//Events
static double  RightVentricularSeptum_t  =  0 ; //the continuous vars
static double  RightVentricularSeptum_t_u ; // and their updates
static double  RightVentricularSeptum_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states RightVentricularSeptum (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (RightVentricularSeptum_ACTcell1) {
      RightVentricularSeptum_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (RightVentricularSeptum_t > (400.0)) {
      RightVentricularSeptum_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( RightVentricularSeptum_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum_t_init = RightVentricularSeptum_t ;
      slope =  1 ;
      RightVentricularSeptum_t_u = (slope * d) + RightVentricularSeptum_t ;
      /* Possible Saturation */
      
      RightVentricularSeptum_ACTpath  = False;
      RightVentricularSeptum_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularSeptum!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (RightVentricularSeptum_t > (10.0)) {
      RightVentricularSeptum_t_u = 0 ;
      RightVentricularSeptum_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( RightVentricularSeptum_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum_t_init = RightVentricularSeptum_t ;
      slope =  1 ;
      RightVentricularSeptum_t_u = (slope * d) + RightVentricularSeptum_t ;
      /* Possible Saturation */
      
      RightVentricularSeptum_ACTpath  = False;
      RightVentricularSeptum_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularSeptum!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (RightVentricularSeptum_t > (200.0)) {
      RightVentricularSeptum_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( RightVentricularSeptum_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum_t_init = RightVentricularSeptum_t ;
      slope =  1 ;
      RightVentricularSeptum_t_u = (slope * d) + RightVentricularSeptum_t ;
      /* Possible Saturation */
      
      RightVentricularSeptum_ACTpath  = False;
      RightVentricularSeptum_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularSeptum!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (RightVentricularSeptum_ACTcell1) {
      RightVentricularSeptum_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (RightVentricularSeptum_t > (100.0)) {
      RightVentricularSeptum_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( RightVentricularSeptum_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularSeptum_t_init = RightVentricularSeptum_t ;
      slope =  1 ;
      RightVentricularSeptum_t_u = (slope * d) + RightVentricularSeptum_t ;
      /* Possible Saturation */
      
      RightVentricularSeptum_ACTpath  = False;
      RightVentricularSeptum_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularSeptum!\n");
      exit(1);
    }
    break;
  }
  RightVentricularSeptum_t = RightVentricularSeptum_t_u;
  return cstate;
}