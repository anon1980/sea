#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char Fast1_AV_ACTcell =  False ;
extern unsigned char  Fast1_AV_ACTpath ;//Events
static double  Fast1_AV_t  =  0 ; //the continuous vars
static double  Fast1_AV_t_u ; // and their updates
static double  Fast1_AV_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states Fast1_AV (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (Fast1_AV_ACTpath) {
      Fast1_AV_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Fast1_AV_t_init = Fast1_AV_t ;
      slope =  0 ;
      Fast1_AV_t_u = (slope * d) + Fast1_AV_t ;
      /* Possible Saturation */
      
      Fast1_AV_ACTcell  = False;
      Fast1_AV_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast1_AV!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (Fast1_AV_t > (10.0)) {
      Fast1_AV_t_u = 0 ;
      Fast1_AV_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( Fast1_AV_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) Fast1_AV_t_init = Fast1_AV_t ;
      slope =  1 ;
      Fast1_AV_t_u = (slope * d) + Fast1_AV_t ;
      /* Possible Saturation */
      
      Fast1_AV_ACTcell  = False;
      Fast1_AV_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast1_AV!\n");
      exit(1);
    }
    break;
  }
  Fast1_AV_t = Fast1_AV_t_u;
  return cstate;
}