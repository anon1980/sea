#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char OS_Fast_ACTcell =  False ;
extern unsigned char  OS_Fast_ACTpath ;//Events
static double  OS_Fast_t  =  0 ; //the continuous vars
static double  OS_Fast_t_u ; // and their updates
static double  OS_Fast_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states OS_Fast (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (OS_Fast_ACTpath) {
      OS_Fast_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) OS_Fast_t_init = OS_Fast_t ;
      slope =  0 ;
      OS_Fast_t_u = (slope * d) + OS_Fast_t ;
      /* Possible Saturation */
      
      OS_Fast_ACTcell  = False;
      OS_Fast_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Fast!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (OS_Fast_t > (20.0)) {
      OS_Fast_t_u = 0 ;
      OS_Fast_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( OS_Fast_t <= (20.0)     ) {
      if ((pstate != cstate) || force_init_update) OS_Fast_t_init = OS_Fast_t ;
      slope =  1 ;
      OS_Fast_t_u = (slope * d) + OS_Fast_t ;
      /* Possible Saturation */
      
      OS_Fast_ACTcell  = False;
      OS_Fast_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: OS_Fast!\n");
      exit(1);
    }
    break;
  }
  OS_Fast_t = OS_Fast_t_u;
  return cstate;
}