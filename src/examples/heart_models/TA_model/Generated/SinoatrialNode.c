#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char SinoatrialNode_ACTpath =  False ;
extern unsigned char  SinoatrialNode_ACTcell1 ;//Events
static double  SinoatrialNode_t  =  0 ; //the continuous vars
static double  SinoatrialNode_t_u ; // and their updates
static double  SinoatrialNode_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states SinoatrialNode (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (SinoatrialNode_ACTcell1) {
      SinoatrialNode_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (SinoatrialNode_t > (400.0)) {
      SinoatrialNode_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( SinoatrialNode_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) SinoatrialNode_t_init = SinoatrialNode_t ;
      slope =  1 ;
      SinoatrialNode_t_u = (slope * d) + SinoatrialNode_t ;
      /* Possible Saturation */
      
      SinoatrialNode_ACTpath  = False;
      SinoatrialNode_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: SinoatrialNode!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (SinoatrialNode_t > (10.0)) {
      SinoatrialNode_t_u = 0 ;
      SinoatrialNode_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( SinoatrialNode_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) SinoatrialNode_t_init = SinoatrialNode_t ;
      slope =  1 ;
      SinoatrialNode_t_u = (slope * d) + SinoatrialNode_t ;
      /* Possible Saturation */
      
      SinoatrialNode_ACTpath  = False;
      SinoatrialNode_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: SinoatrialNode!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (SinoatrialNode_t > (200.0)) {
      SinoatrialNode_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( SinoatrialNode_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) SinoatrialNode_t_init = SinoatrialNode_t ;
      slope =  1 ;
      SinoatrialNode_t_u = (slope * d) + SinoatrialNode_t ;
      /* Possible Saturation */
      
      SinoatrialNode_ACTpath  = False;
      SinoatrialNode_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: SinoatrialNode!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (SinoatrialNode_ACTcell1) {
      SinoatrialNode_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (SinoatrialNode_t > (100.0)) {
      SinoatrialNode_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( SinoatrialNode_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) SinoatrialNode_t_init = SinoatrialNode_t ;
      slope =  1 ;
      SinoatrialNode_t_u = (slope * d) + SinoatrialNode_t ;
      /* Possible Saturation */
      
      SinoatrialNode_ACTpath  = False;
      SinoatrialNode_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: SinoatrialNode!\n");
      exit(1);
    }
    break;
  }
  SinoatrialNode_t = SinoatrialNode_t_u;
  return cstate;
}