#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char LA_LA1_ACTcell =  False ;
extern unsigned char  LA_LA1_ACTpath ;//Events
static double  LA_LA1_t  =  0 ; //the continuous vars
static double  LA_LA1_t_u ; // and their updates
static double  LA_LA1_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states LA_LA1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (LA_LA1_ACTpath) {
      LA_LA1_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LA_LA1_t_init = LA_LA1_t ;
      slope =  0 ;
      LA_LA1_t_u = (slope * d) + LA_LA1_t ;
      /* Possible Saturation */
      
      LA_LA1_ACTcell  = False;
      LA_LA1_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LA_LA1!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (LA_LA1_t > (40.0)) {
      LA_LA1_t_u = 0 ;
      LA_LA1_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( LA_LA1_t <= (40.0)     ) {
      if ((pstate != cstate) || force_init_update) LA_LA1_t_init = LA_LA1_t ;
      slope =  1 ;
      LA_LA1_t_u = (slope * d) + LA_LA1_t ;
      /* Possible Saturation */
      
      LA_LA1_ACTcell  = False;
      LA_LA1_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LA_LA1!\n");
      exit(1);
    }
    break;
  }
  LA_LA1_t = LA_LA1_t_u;
  return cstate;
}