#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char RVA_RV_ACTcell =  False ;
extern unsigned char  RVA_RV_ACTpath ;//Events
static double  RVA_RV_t  =  0 ; //the continuous vars
static double  RVA_RV_t_u ; // and their updates
static double  RVA_RV_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states RVA_RV (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (RVA_RV_ACTpath) {
      RVA_RV_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVA_RV_t_init = RVA_RV_t ;
      slope =  0 ;
      RVA_RV_t_u = (slope * d) + RVA_RV_t ;
      /* Possible Saturation */
      
      RVA_RV_ACTcell  = False;
      RVA_RV_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RV!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (RVA_RV_t > (10.0)) {
      RVA_RV_t_u = 0 ;
      RVA_RV_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( RVA_RV_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) RVA_RV_t_init = RVA_RV_t ;
      slope =  1 ;
      RVA_RV_t_u = (slope * d) + RVA_RV_t ;
      /* Possible Saturation */
      
      RVA_RV_ACTcell  = False;
      RVA_RV_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RVA_RV!\n");
      exit(1);
    }
    break;
  }
  RVA_RV_t = RVA_RV_t_u;
  return cstate;
}