#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char His_His1_ACTcell =  False ;
extern unsigned char  His_His1_ACTpath ;//Events
static double  His_His1_t  =  0 ; //the continuous vars
static double  His_His1_t_u ; // and their updates
static double  His_His1_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states His_His1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (His_His1_ACTpath) {
      His_His1_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His_His1_t_init = His_His1_t ;
      slope =  0 ;
      His_His1_t_u = (slope * d) + His_His1_t ;
      /* Possible Saturation */
      
      His_His1_ACTcell  = False;
      His_His1_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: His_His1!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (His_His1_t > (20.0)) {
      His_His1_t_u = 0 ;
      His_His1_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( His_His1_t <= (20.0)     ) {
      if ((pstate != cstate) || force_init_update) His_His1_t_init = His_His1_t ;
      slope =  1 ;
      His_His1_t_u = (slope * d) + His_His1_t ;
      /* Possible Saturation */
      
      His_His1_ACTcell  = False;
      His_His1_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: His_His1!\n");
      exit(1);
    }
    break;
  }
  His_His1_t = His_His1_t_u;
  return cstate;
}