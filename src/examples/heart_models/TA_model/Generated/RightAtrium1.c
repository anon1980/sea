#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char RightAtrium1_ACTpath =  False ;
extern unsigned char  RightAtrium1_ACTcell1 ;//Events
static double  RightAtrium1_t  =  0 ; //the continuous vars
static double  RightAtrium1_t_u ; // and their updates
static double  RightAtrium1_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states RightAtrium1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (RightAtrium1_ACTcell1) {
      RightAtrium1_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (RightAtrium1_t > (400.0)) {
      RightAtrium1_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( RightAtrium1_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) RightAtrium1_t_init = RightAtrium1_t ;
      slope =  1 ;
      RightAtrium1_t_u = (slope * d) + RightAtrium1_t ;
      /* Possible Saturation */
      
      RightAtrium1_ACTpath  = False;
      RightAtrium1_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightAtrium1!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (RightAtrium1_t > (10.0)) {
      RightAtrium1_t_u = 0 ;
      RightAtrium1_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( RightAtrium1_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) RightAtrium1_t_init = RightAtrium1_t ;
      slope =  1 ;
      RightAtrium1_t_u = (slope * d) + RightAtrium1_t ;
      /* Possible Saturation */
      
      RightAtrium1_ACTpath  = False;
      RightAtrium1_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightAtrium1!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (RightAtrium1_t > (200.0)) {
      RightAtrium1_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( RightAtrium1_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) RightAtrium1_t_init = RightAtrium1_t ;
      slope =  1 ;
      RightAtrium1_t_u = (slope * d) + RightAtrium1_t ;
      /* Possible Saturation */
      
      RightAtrium1_ACTpath  = False;
      RightAtrium1_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightAtrium1!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (RightAtrium1_ACTcell1) {
      RightAtrium1_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (RightAtrium1_t > (100.0)) {
      RightAtrium1_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( RightAtrium1_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) RightAtrium1_t_init = RightAtrium1_t ;
      slope =  1 ;
      RightAtrium1_t_u = (slope * d) + RightAtrium1_t ;
      /* Possible Saturation */
      
      RightAtrium1_ACTpath  = False;
      RightAtrium1_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightAtrium1!\n");
      exit(1);
    }
    break;
  }
  RightAtrium1_t = RightAtrium1_t_u;
  return cstate;
}