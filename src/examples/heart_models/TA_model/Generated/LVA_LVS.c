#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char LVA_LVS_ACTcell =  False ;
extern unsigned char  LVA_LVS_ACTpath ;//Events
static double  LVA_LVS_t  =  0 ; //the continuous vars
static double  LVA_LVS_t_u ; // and their updates
static double  LVA_LVS_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states LVA_LVS (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (LVA_LVS_ACTpath) {
      LVA_LVS_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVA_LVS_t_init = LVA_LVS_t ;
      slope =  0 ;
      LVA_LVS_t_u = (slope * d) + LVA_LVS_t ;
      /* Possible Saturation */
      
      LVA_LVS_ACTcell  = False;
      LVA_LVS_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_LVS!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (LVA_LVS_t > (15.0)) {
      LVA_LVS_t_u = 0 ;
      LVA_LVS_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( LVA_LVS_t <= (15.0)     ) {
      if ((pstate != cstate) || force_init_update) LVA_LVS_t_init = LVA_LVS_t ;
      slope =  1 ;
      LVA_LVS_t_u = (slope * d) + LVA_LVS_t ;
      /* Possible Saturation */
      
      LVA_LVS_ACTcell  = False;
      LVA_LVS_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LVA_LVS!\n");
      exit(1);
    }
    break;
  }
  LVA_LVS_t = LVA_LVS_t_u;
  return cstate;
}