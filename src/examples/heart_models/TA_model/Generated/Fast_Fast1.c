#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char Fast_Fast1_ACTcell =  False ;
extern unsigned char  Fast_Fast1_ACTpath ;//Events
static double  Fast_Fast1_t  =  0 ; //the continuous vars
static double  Fast_Fast1_t_u ; // and their updates
static double  Fast_Fast1_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states Fast_Fast1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (Fast_Fast1_ACTpath) {
      Fast_Fast1_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Fast_Fast1_t_init = Fast_Fast1_t ;
      slope =  0 ;
      Fast_Fast1_t_u = (slope * d) + Fast_Fast1_t ;
      /* Possible Saturation */
      
      Fast_Fast1_ACTcell  = False;
      Fast_Fast1_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast_Fast1!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (Fast_Fast1_t > (20.0)) {
      Fast_Fast1_t_u = 0 ;
      Fast_Fast1_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( Fast_Fast1_t <= (20.0)     ) {
      if ((pstate != cstate) || force_init_update) Fast_Fast1_t_init = Fast_Fast1_t ;
      slope =  1 ;
      Fast_Fast1_t_u = (slope * d) + Fast_Fast1_t ;
      /* Possible Saturation */
      
      Fast_Fast1_ACTcell  = False;
      Fast_Fast1_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Fast_Fast1!\n");
      exit(1);
    }
    break;
  }
  Fast_Fast1_t = Fast_Fast1_t_u;
  return cstate;
}