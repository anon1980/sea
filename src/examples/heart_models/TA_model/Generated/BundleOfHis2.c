#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char BundleOfHis2_ACTpath =  False ;
extern unsigned char  BundleOfHis2_ACTcell1 ;//Events
static double  BundleOfHis2_t  =  0 ; //the continuous vars
static double  BundleOfHis2_t_u ; // and their updates
static double  BundleOfHis2_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states BundleOfHis2 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (BundleOfHis2_ACTcell1) {
      BundleOfHis2_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (BundleOfHis2_t > (400.0)) {
      BundleOfHis2_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( BundleOfHis2_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis2_t_init = BundleOfHis2_t ;
      slope =  1 ;
      BundleOfHis2_t_u = (slope * d) + BundleOfHis2_t ;
      /* Possible Saturation */
      
      BundleOfHis2_ACTpath  = False;
      BundleOfHis2_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis2!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (BundleOfHis2_t > (10.0)) {
      BundleOfHis2_t_u = 0 ;
      BundleOfHis2_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( BundleOfHis2_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis2_t_init = BundleOfHis2_t ;
      slope =  1 ;
      BundleOfHis2_t_u = (slope * d) + BundleOfHis2_t ;
      /* Possible Saturation */
      
      BundleOfHis2_ACTpath  = False;
      BundleOfHis2_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis2!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (BundleOfHis2_t > (200.0)) {
      BundleOfHis2_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( BundleOfHis2_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis2_t_init = BundleOfHis2_t ;
      slope =  1 ;
      BundleOfHis2_t_u = (slope * d) + BundleOfHis2_t ;
      /* Possible Saturation */
      
      BundleOfHis2_ACTpath  = False;
      BundleOfHis2_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis2!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (BundleOfHis2_ACTcell1) {
      BundleOfHis2_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (BundleOfHis2_t > (100.0)) {
      BundleOfHis2_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( BundleOfHis2_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis2_t_init = BundleOfHis2_t ;
      slope =  1 ;
      BundleOfHis2_t_u = (slope * d) + BundleOfHis2_t ;
      /* Possible Saturation */
      
      BundleOfHis2_ACTpath  = False;
      BundleOfHis2_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis2!\n");
      exit(1);
    }
    break;
  }
  BundleOfHis2_t = BundleOfHis2_t_u;
  return cstate;
}