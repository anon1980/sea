#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char LBB1_LVA_ACTcell =  False ;
extern unsigned char  LBB1_LVA_ACTpath ;//Events
static double  LBB1_LVA_t  =  0 ; //the continuous vars
static double  LBB1_LVA_t_u ; // and their updates
static double  LBB1_LVA_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states LBB1_LVA (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (LBB1_LVA_ACTpath) {
      LBB1_LVA_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LBB1_LVA_t_init = LBB1_LVA_t ;
      slope =  0 ;
      LBB1_LVA_t_u = (slope * d) + LBB1_LVA_t ;
      /* Possible Saturation */
      
      LBB1_LVA_ACTcell  = False;
      LBB1_LVA_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB1_LVA!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (LBB1_LVA_t > (5.0)) {
      LBB1_LVA_t_u = 0 ;
      LBB1_LVA_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( LBB1_LVA_t <= (5.0)     ) {
      if ((pstate != cstate) || force_init_update) LBB1_LVA_t_init = LBB1_LVA_t ;
      slope =  1 ;
      LBB1_LVA_t_u = (slope * d) + LBB1_LVA_t ;
      /* Possible Saturation */
      
      LBB1_LVA_ACTcell  = False;
      LBB1_LVA_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB1_LVA!\n");
      exit(1);
    }
    break;
  }
  LBB1_LVA_t = LBB1_LVA_t_u;
  return cstate;
}