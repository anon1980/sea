#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char LVS_LVS1_ACTcell =  False ;
extern unsigned char  LVS_LVS1_ACTpath ;//Events
static double  LVS_LVS1_t  =  0 ; //the continuous vars
static double  LVS_LVS1_t_u ; // and their updates
static double  LVS_LVS1_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states LVS_LVS1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (LVS_LVS1_ACTpath) {
      LVS_LVS1_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LVS_LVS1_t_init = LVS_LVS1_t ;
      slope =  0 ;
      LVS_LVS1_t_u = (slope * d) + LVS_LVS1_t ;
      /* Possible Saturation */
      
      LVS_LVS1_ACTcell  = False;
      LVS_LVS1_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LVS_LVS1!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (LVS_LVS1_t > (15.0)) {
      LVS_LVS1_t_u = 0 ;
      LVS_LVS1_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( LVS_LVS1_t <= (15.0)     ) {
      if ((pstate != cstate) || force_init_update) LVS_LVS1_t_init = LVS_LVS1_t ;
      slope =  1 ;
      LVS_LVS1_t_u = (slope * d) + LVS_LVS1_t ;
      /* Possible Saturation */
      
      LVS_LVS1_ACTcell  = False;
      LVS_LVS1_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LVS_LVS1!\n");
      exit(1);
    }
    break;
  }
  LVS_LVS1_t = LVS_LVS1_t_u;
  return cstate;
}