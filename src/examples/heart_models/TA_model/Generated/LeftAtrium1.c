#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char LeftAtrium1_ACTpath =  False ;
extern unsigned char  LeftAtrium1_ACTcell1 ;//Events
static double  LeftAtrium1_t  =  0 ; //the continuous vars
static double  LeftAtrium1_t_u ; // and their updates
static double  LeftAtrium1_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states LeftAtrium1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (LeftAtrium1_ACTcell1) {
      LeftAtrium1_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (LeftAtrium1_t > (400.0)) {
      LeftAtrium1_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( LeftAtrium1_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftAtrium1_t_init = LeftAtrium1_t ;
      slope =  1 ;
      LeftAtrium1_t_u = (slope * d) + LeftAtrium1_t ;
      /* Possible Saturation */
      
      LeftAtrium1_ACTpath  = False;
      LeftAtrium1_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftAtrium1!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (LeftAtrium1_t > (10.0)) {
      LeftAtrium1_t_u = 0 ;
      LeftAtrium1_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( LeftAtrium1_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftAtrium1_t_init = LeftAtrium1_t ;
      slope =  1 ;
      LeftAtrium1_t_u = (slope * d) + LeftAtrium1_t ;
      /* Possible Saturation */
      
      LeftAtrium1_ACTpath  = False;
      LeftAtrium1_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftAtrium1!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (LeftAtrium1_t > (200.0)) {
      LeftAtrium1_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( LeftAtrium1_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftAtrium1_t_init = LeftAtrium1_t ;
      slope =  1 ;
      LeftAtrium1_t_u = (slope * d) + LeftAtrium1_t ;
      /* Possible Saturation */
      
      LeftAtrium1_ACTpath  = False;
      LeftAtrium1_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftAtrium1!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (LeftAtrium1_ACTcell1) {
      LeftAtrium1_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (LeftAtrium1_t > (100.0)) {
      LeftAtrium1_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( LeftAtrium1_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftAtrium1_t_init = LeftAtrium1_t ;
      slope =  1 ;
      LeftAtrium1_t_u = (slope * d) + LeftAtrium1_t ;
      /* Possible Saturation */
      
      LeftAtrium1_ACTpath  = False;
      LeftAtrium1_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftAtrium1!\n");
      exit(1);
    }
    break;
  }
  LeftAtrium1_t = LeftAtrium1_t_u;
  return cstate;
}