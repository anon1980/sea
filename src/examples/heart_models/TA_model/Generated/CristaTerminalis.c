#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char CristaTerminalis_ACTpath =  False ;
extern unsigned char  CristaTerminalis_ACTcell1 ;//Events
static double  CristaTerminalis_t  =  0 ; //the continuous vars
static double  CristaTerminalis_t_u ; // and their updates
static double  CristaTerminalis_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states CristaTerminalis (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (CristaTerminalis_ACTcell1) {
      CristaTerminalis_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (CristaTerminalis_t > (400.0)) {
      CristaTerminalis_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( CristaTerminalis_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) CristaTerminalis_t_init = CristaTerminalis_t ;
      slope =  1 ;
      CristaTerminalis_t_u = (slope * d) + CristaTerminalis_t ;
      /* Possible Saturation */
      
      CristaTerminalis_ACTpath  = False;
      CristaTerminalis_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: CristaTerminalis!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (CristaTerminalis_t > (10.0)) {
      CristaTerminalis_t_u = 0 ;
      CristaTerminalis_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( CristaTerminalis_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) CristaTerminalis_t_init = CristaTerminalis_t ;
      slope =  1 ;
      CristaTerminalis_t_u = (slope * d) + CristaTerminalis_t ;
      /* Possible Saturation */
      
      CristaTerminalis_ACTpath  = False;
      CristaTerminalis_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: CristaTerminalis!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (CristaTerminalis_t > (200.0)) {
      CristaTerminalis_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( CristaTerminalis_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) CristaTerminalis_t_init = CristaTerminalis_t ;
      slope =  1 ;
      CristaTerminalis_t_u = (slope * d) + CristaTerminalis_t ;
      /* Possible Saturation */
      
      CristaTerminalis_ACTpath  = False;
      CristaTerminalis_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: CristaTerminalis!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (CristaTerminalis_ACTcell1) {
      CristaTerminalis_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (CristaTerminalis_t > (100.0)) {
      CristaTerminalis_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( CristaTerminalis_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) CristaTerminalis_t_init = CristaTerminalis_t ;
      slope =  1 ;
      CristaTerminalis_t_u = (slope * d) + CristaTerminalis_t ;
      /* Possible Saturation */
      
      CristaTerminalis_ACTpath  = False;
      CristaTerminalis_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: CristaTerminalis!\n");
      exit(1);
    }
    break;
  }
  CristaTerminalis_t = CristaTerminalis_t_u;
  return cstate;
}