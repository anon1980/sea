#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char RBB1_RVA_ACTcell =  False ;
extern unsigned char  RBB1_RVA_ACTpath ;//Events
static double  RBB1_RVA_t  =  0 ; //the continuous vars
static double  RBB1_RVA_t_u ; // and their updates
static double  RBB1_RVA_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states RBB1_RVA (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (RBB1_RVA_ACTpath) {
      RBB1_RVA_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RBB1_RVA_t_init = RBB1_RVA_t ;
      slope =  0 ;
      RBB1_RVA_t_u = (slope * d) + RBB1_RVA_t ;
      /* Possible Saturation */
      
      RBB1_RVA_ACTcell  = False;
      RBB1_RVA_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB1_RVA!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (RBB1_RVA_t > (5.0)) {
      RBB1_RVA_t_u = 0 ;
      RBB1_RVA_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( RBB1_RVA_t <= (5.0)     ) {
      if ((pstate != cstate) || force_init_update) RBB1_RVA_t_init = RBB1_RVA_t ;
      slope =  1 ;
      RBB1_RVA_t_u = (slope * d) + RBB1_RVA_t ;
      /* Possible Saturation */
      
      RBB1_RVA_ACTcell  = False;
      RBB1_RVA_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB1_RVA!\n");
      exit(1);
    }
    break;
  }
  RBB1_RVA_t = RBB1_RVA_t_u;
  return cstate;
}