#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char LeftVentricularApex_ACTpath =  False ;
extern unsigned char  LeftVentricularApex_ACTcell1 ;//Events
static double  LeftVentricularApex_t  =  0 ; //the continuous vars
static double  LeftVentricularApex_t_u ; // and their updates
static double  LeftVentricularApex_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states LeftVentricularApex (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (LeftVentricularApex_ACTcell1) {
      LeftVentricularApex_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (LeftVentricularApex_t > (400.0)) {
      LeftVentricularApex_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( LeftVentricularApex_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularApex_t_init = LeftVentricularApex_t ;
      slope =  1 ;
      LeftVentricularApex_t_u = (slope * d) + LeftVentricularApex_t ;
      /* Possible Saturation */
      
      LeftVentricularApex_ACTpath  = False;
      LeftVentricularApex_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularApex!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (LeftVentricularApex_t > (10.0)) {
      LeftVentricularApex_t_u = 0 ;
      LeftVentricularApex_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( LeftVentricularApex_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularApex_t_init = LeftVentricularApex_t ;
      slope =  1 ;
      LeftVentricularApex_t_u = (slope * d) + LeftVentricularApex_t ;
      /* Possible Saturation */
      
      LeftVentricularApex_ACTpath  = False;
      LeftVentricularApex_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularApex!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (LeftVentricularApex_t > (200.0)) {
      LeftVentricularApex_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( LeftVentricularApex_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularApex_t_init = LeftVentricularApex_t ;
      slope =  1 ;
      LeftVentricularApex_t_u = (slope * d) + LeftVentricularApex_t ;
      /* Possible Saturation */
      
      LeftVentricularApex_ACTpath  = False;
      LeftVentricularApex_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularApex!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (LeftVentricularApex_ACTcell1) {
      LeftVentricularApex_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (LeftVentricularApex_t > (100.0)) {
      LeftVentricularApex_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( LeftVentricularApex_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularApex_t_init = LeftVentricularApex_t ;
      slope =  1 ;
      LeftVentricularApex_t_u = (slope * d) + LeftVentricularApex_t ;
      /* Possible Saturation */
      
      LeftVentricularApex_ACTpath  = False;
      LeftVentricularApex_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularApex!\n");
      exit(1);
    }
    break;
  }
  LeftVentricularApex_t = LeftVentricularApex_t_u;
  return cstate;
}