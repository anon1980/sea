#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char Slow1_AV_ACTcell =  False ;
extern unsigned char  Slow1_AV_ACTpath ;//Events
static double  Slow1_AV_t  =  0 ; //the continuous vars
static double  Slow1_AV_t_u ; // and their updates
static double  Slow1_AV_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states Slow1_AV (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (Slow1_AV_ACTpath) {
      Slow1_AV_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Slow1_AV_t_init = Slow1_AV_t ;
      slope =  0 ;
      Slow1_AV_t_u = (slope * d) + Slow1_AV_t ;
      /* Possible Saturation */
      
      Slow1_AV_ACTcell  = False;
      Slow1_AV_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow1_AV!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (Slow1_AV_t > (15.0)) {
      Slow1_AV_t_u = 0 ;
      Slow1_AV_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( Slow1_AV_t <= (15.0)     ) {
      if ((pstate != cstate) || force_init_update) Slow1_AV_t_init = Slow1_AV_t ;
      slope =  1 ;
      Slow1_AV_t_u = (slope * d) + Slow1_AV_t ;
      /* Possible Saturation */
      
      Slow1_AV_ACTcell  = False;
      Slow1_AV_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow1_AV!\n");
      exit(1);
    }
    break;
  }
  Slow1_AV_t = Slow1_AV_t_u;
  return cstate;
}