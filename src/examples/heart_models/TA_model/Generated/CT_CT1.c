#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char CT_CT1_ACTcell =  False ;
extern unsigned char  CT_CT1_ACTpath ;//Events
static double  CT_CT1_t  =  0 ; //the continuous vars
static double  CT_CT1_t_u ; // and their updates
static double  CT_CT1_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states CT_CT1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (CT_CT1_ACTpath) {
      CT_CT1_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) CT_CT1_t_init = CT_CT1_t ;
      slope =  0 ;
      CT_CT1_t_u = (slope * d) + CT_CT1_t ;
      /* Possible Saturation */
      
      CT_CT1_ACTcell  = False;
      CT_CT1_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: CT_CT1!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (CT_CT1_t > (20.0)) {
      CT_CT1_t_u = 0 ;
      CT_CT1_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( CT_CT1_t <= (20.0)     ) {
      if ((pstate != cstate) || force_init_update) CT_CT1_t_init = CT_CT1_t ;
      slope =  1 ;
      CT_CT1_t_u = (slope * d) + CT_CT1_t ;
      /* Possible Saturation */
      
      CT_CT1_ACTcell  = False;
      CT_CT1_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: CT_CT1!\n");
      exit(1);
    }
    break;
  }
  CT_CT1_t = CT_CT1_t_u;
  return cstate;
}