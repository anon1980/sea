#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char His1_His2_ACTcell =  False ;
extern unsigned char  His1_His2_ACTpath ;//Events
static double  His1_His2_t  =  0 ; //the continuous vars
static double  His1_His2_t_u ; // and their updates
static double  His1_His2_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states His1_His2 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (His1_His2_ACTpath) {
      His1_His2_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His1_His2_t_init = His1_His2_t ;
      slope =  0 ;
      His1_His2_t_u = (slope * d) + His1_His2_t ;
      /* Possible Saturation */
      
      His1_His2_ACTcell  = False;
      His1_His2_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: His1_His2!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (His1_His2_t > (20.0)) {
      His1_His2_t_u = 0 ;
      His1_His2_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( His1_His2_t <= (20.0)     ) {
      if ((pstate != cstate) || force_init_update) His1_His2_t_init = His1_His2_t ;
      slope =  1 ;
      His1_His2_t_u = (slope * d) + His1_His2_t ;
      /* Possible Saturation */
      
      His1_His2_ACTcell  = False;
      His1_His2_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: His1_His2!\n");
      exit(1);
    }
    break;
  }
  His1_His2_t = His1_His2_t_u;
  return cstate;
}