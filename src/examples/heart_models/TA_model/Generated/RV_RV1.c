#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char RV_RV1_ACTcell =  False ;
extern unsigned char  RV_RV1_ACTpath ;//Events
static double  RV_RV1_t  =  0 ; //the continuous vars
static double  RV_RV1_t_u ; // and their updates
static double  RV_RV1_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states RV_RV1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (RV_RV1_ACTpath) {
      RV_RV1_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RV_RV1_t_init = RV_RV1_t ;
      slope =  0 ;
      RV_RV1_t_u = (slope * d) + RV_RV1_t ;
      /* Possible Saturation */
      
      RV_RV1_ACTcell  = False;
      RV_RV1_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RV_RV1!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (RV_RV1_t > (20.0)) {
      RV_RV1_t_u = 0 ;
      RV_RV1_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( RV_RV1_t <= (20.0)     ) {
      if ((pstate != cstate) || force_init_update) RV_RV1_t_init = RV_RV1_t ;
      slope =  1 ;
      RV_RV1_t_u = (slope * d) + RV_RV1_t ;
      /* Possible Saturation */
      
      RV_RV1_ACTcell  = False;
      RV_RV1_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RV_RV1!\n");
      exit(1);
    }
    break;
  }
  RV_RV1_t = RV_RV1_t_u;
  return cstate;
}