#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char SA_CT_ACTcell =  False ;
extern unsigned char  SA_CT_ACTpath ;//Events
static double  SA_CT_t  =  0 ; //the continuous vars
static double  SA_CT_t_u ; // and their updates
static double  SA_CT_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states SA_CT (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (SA_CT_ACTpath) {
      SA_CT_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) SA_CT_t_init = SA_CT_t ;
      slope =  0 ;
      SA_CT_t_u = (slope * d) + SA_CT_t ;
      /* Possible Saturation */
      
      SA_CT_ACTcell  = False;
      SA_CT_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_CT!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (SA_CT_t > (20.0)) {
      SA_CT_t_u = 0 ;
      SA_CT_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( SA_CT_t <= (20.0)     ) {
      if ((pstate != cstate) || force_init_update) SA_CT_t_init = SA_CT_t ;
      slope =  1 ;
      SA_CT_t_u = (slope * d) + SA_CT_t ;
      /* Possible Saturation */
      
      SA_CT_ACTcell  = False;
      SA_CT_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_CT!\n");
      exit(1);
    }
    break;
  }
  SA_CT_t = SA_CT_t_u;
  return cstate;
}