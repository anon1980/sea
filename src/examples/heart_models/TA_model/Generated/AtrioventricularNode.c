#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char AtrioventricularNode_ACTpath =  False ;
extern unsigned char  AtrioventricularNode_ACTcell2 , AtrioventricularNode_ACTcell1 ;//Events
static double  AtrioventricularNode_t  =  0 ; //the continuous vars
static double  AtrioventricularNode_t_u ; // and their updates
static double  AtrioventricularNode_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states AtrioventricularNode (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (AtrioventricularNode_ACTcell1) {
      AtrioventricularNode_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (AtrioventricularNode_t > (400.0)) {
      AtrioventricularNode_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (AtrioventricularNode_ACTcell2) {
      AtrioventricularNode_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( AtrioventricularNode_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) AtrioventricularNode_t_init = AtrioventricularNode_t ;
      slope =  1 ;
      AtrioventricularNode_t_u = (slope * d) + AtrioventricularNode_t ;
      /* Possible Saturation */
      
      AtrioventricularNode_ACTpath  = False;
      AtrioventricularNode_ACTcell1  = False;
      AtrioventricularNode_ACTcell2  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: AtrioventricularNode!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (AtrioventricularNode_t > (10.0)) {
      AtrioventricularNode_t_u = 0 ;
      AtrioventricularNode_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( AtrioventricularNode_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) AtrioventricularNode_t_init = AtrioventricularNode_t ;
      slope =  1 ;
      AtrioventricularNode_t_u = (slope * d) + AtrioventricularNode_t ;
      /* Possible Saturation */
      
      AtrioventricularNode_ACTpath  = False;
      AtrioventricularNode_ACTcell1  = False;
      AtrioventricularNode_ACTcell2  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: AtrioventricularNode!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (AtrioventricularNode_t > (200.0)) {
      AtrioventricularNode_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( AtrioventricularNode_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) AtrioventricularNode_t_init = AtrioventricularNode_t ;
      slope =  1 ;
      AtrioventricularNode_t_u = (slope * d) + AtrioventricularNode_t ;
      /* Possible Saturation */
      
      AtrioventricularNode_ACTpath  = False;
      AtrioventricularNode_ACTcell1  = False;
      AtrioventricularNode_ACTcell2  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: AtrioventricularNode!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (AtrioventricularNode_ACTcell1) {
      AtrioventricularNode_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (AtrioventricularNode_t > (100.0)) {
      AtrioventricularNode_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( AtrioventricularNode_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) AtrioventricularNode_t_init = AtrioventricularNode_t ;
      slope =  1 ;
      AtrioventricularNode_t_u = (slope * d) + AtrioventricularNode_t ;
      /* Possible Saturation */
      
      AtrioventricularNode_ACTpath  = False;
      AtrioventricularNode_ACTcell1  = False;
      AtrioventricularNode_ACTcell2  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: AtrioventricularNode!\n");
      exit(1);
    }
    break;
  }
  AtrioventricularNode_t = AtrioventricularNode_t_u;
  return cstate;
}