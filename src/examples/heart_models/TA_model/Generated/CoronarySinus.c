#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char CoronarySinus_ACTpath =  False ;
extern unsigned char  CoronarySinus_ACTcell1 ;//Events
static double  CoronarySinus_t  =  0 ; //the continuous vars
static double  CoronarySinus_t_u ; // and their updates
static double  CoronarySinus_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states CoronarySinus (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (CoronarySinus_ACTcell1) {
      CoronarySinus_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (CoronarySinus_t > (400.0)) {
      CoronarySinus_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( CoronarySinus_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) CoronarySinus_t_init = CoronarySinus_t ;
      slope =  1 ;
      CoronarySinus_t_u = (slope * d) + CoronarySinus_t ;
      /* Possible Saturation */
      
      CoronarySinus_ACTpath  = False;
      CoronarySinus_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: CoronarySinus!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (CoronarySinus_t > (10.0)) {
      CoronarySinus_t_u = 0 ;
      CoronarySinus_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( CoronarySinus_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) CoronarySinus_t_init = CoronarySinus_t ;
      slope =  1 ;
      CoronarySinus_t_u = (slope * d) + CoronarySinus_t ;
      /* Possible Saturation */
      
      CoronarySinus_ACTpath  = False;
      CoronarySinus_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: CoronarySinus!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (CoronarySinus_t > (200.0)) {
      CoronarySinus_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( CoronarySinus_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) CoronarySinus_t_init = CoronarySinus_t ;
      slope =  1 ;
      CoronarySinus_t_u = (slope * d) + CoronarySinus_t ;
      /* Possible Saturation */
      
      CoronarySinus_ACTpath  = False;
      CoronarySinus_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: CoronarySinus!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (CoronarySinus_ACTcell1) {
      CoronarySinus_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (CoronarySinus_t > (100.0)) {
      CoronarySinus_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( CoronarySinus_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) CoronarySinus_t_init = CoronarySinus_t ;
      slope =  1 ;
      CoronarySinus_t_u = (slope * d) + CoronarySinus_t ;
      /* Possible Saturation */
      
      CoronarySinus_ACTpath  = False;
      CoronarySinus_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: CoronarySinus!\n");
      exit(1);
    }
    break;
  }
  CoronarySinus_t = CoronarySinus_t_u;
  return cstate;
}