#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char His2_RBB_ACTcell =  False ;
extern unsigned char  His2_RBB_ACTpath ;//Events
static double  His2_RBB_t  =  0 ; //the continuous vars
static double  His2_RBB_t_u ; // and their updates
static double  His2_RBB_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states His2_RBB (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (His2_RBB_ACTpath) {
      His2_RBB_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) His2_RBB_t_init = His2_RBB_t ;
      slope =  0 ;
      His2_RBB_t_u = (slope * d) + His2_RBB_t ;
      /* Possible Saturation */
      
      His2_RBB_ACTcell  = False;
      His2_RBB_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_RBB!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (His2_RBB_t > (20.0)) {
      His2_RBB_t_u = 0 ;
      His2_RBB_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( His2_RBB_t <= (20.0)     ) {
      if ((pstate != cstate) || force_init_update) His2_RBB_t_init = His2_RBB_t ;
      slope =  1 ;
      His2_RBB_t_u = (slope * d) + His2_RBB_t ;
      /* Possible Saturation */
      
      His2_RBB_ACTcell  = False;
      His2_RBB_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: His2_RBB!\n");
      exit(1);
    }
    break;
  }
  His2_RBB_t = His2_RBB_t_u;
  return cstate;
}