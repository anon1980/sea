#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char Ostium_ACTpath =  False ;
extern unsigned char  Ostium_ACTcell1 ;//Events
static double  Ostium_t  =  0 ; //the continuous vars
static double  Ostium_t_u ; // and their updates
static double  Ostium_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states Ostium (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (Ostium_ACTcell1) {
      Ostium_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (Ostium_t > (400.0)) {
      Ostium_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( Ostium_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) Ostium_t_init = Ostium_t ;
      slope =  1 ;
      Ostium_t_u = (slope * d) + Ostium_t ;
      /* Possible Saturation */
      
      Ostium_ACTpath  = False;
      Ostium_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Ostium!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (Ostium_t > (10.0)) {
      Ostium_t_u = 0 ;
      Ostium_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( Ostium_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) Ostium_t_init = Ostium_t ;
      slope =  1 ;
      Ostium_t_u = (slope * d) + Ostium_t ;
      /* Possible Saturation */
      
      Ostium_ACTpath  = False;
      Ostium_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Ostium!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (Ostium_t > (200.0)) {
      Ostium_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( Ostium_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) Ostium_t_init = Ostium_t ;
      slope =  1 ;
      Ostium_t_u = (slope * d) + Ostium_t ;
      /* Possible Saturation */
      
      Ostium_ACTpath  = False;
      Ostium_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Ostium!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (Ostium_ACTcell1) {
      Ostium_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (Ostium_t > (100.0)) {
      Ostium_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( Ostium_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) Ostium_t_init = Ostium_t ;
      slope =  1 ;
      Ostium_t_u = (slope * d) + Ostium_t ;
      /* Possible Saturation */
      
      Ostium_ACTpath  = False;
      Ostium_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Ostium!\n");
      exit(1);
    }
    break;
  }
  Ostium_t = Ostium_t_u;
  return cstate;
}