#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char RVS_RVS1_ACTcell =  False ;
extern unsigned char  RVS_RVS1_ACTpath ;//Events
static double  RVS_RVS1_t  =  0 ; //the continuous vars
static double  RVS_RVS1_t_u ; // and their updates
static double  RVS_RVS1_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states RVS_RVS1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (RVS_RVS1_ACTpath) {
      RVS_RVS1_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RVS_RVS1_t_init = RVS_RVS1_t ;
      slope =  0 ;
      RVS_RVS1_t_u = (slope * d) + RVS_RVS1_t ;
      /* Possible Saturation */
      
      RVS_RVS1_ACTcell  = False;
      RVS_RVS1_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RVS_RVS1!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (RVS_RVS1_t > (20.0)) {
      RVS_RVS1_t_u = 0 ;
      RVS_RVS1_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( RVS_RVS1_t <= (20.0)     ) {
      if ((pstate != cstate) || force_init_update) RVS_RVS1_t_init = RVS_RVS1_t ;
      slope =  1 ;
      RVS_RVS1_t_u = (slope * d) + RVS_RVS1_t ;
      /* Possible Saturation */
      
      RVS_RVS1_ACTcell  = False;
      RVS_RVS1_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RVS_RVS1!\n");
      exit(1);
    }
    break;
  }
  RVS_RVS1_t = RVS_RVS1_t_u;
  return cstate;
}