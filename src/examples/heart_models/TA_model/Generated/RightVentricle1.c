#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char RightVentricle1_ACTpath =  False ;
extern unsigned char  RightVentricle1_ACTcell1 ;//Events
static double  RightVentricle1_t  =  0 ; //the continuous vars
static double  RightVentricle1_t_u ; // and their updates
static double  RightVentricle1_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states RightVentricle1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (RightVentricle1_ACTcell1) {
      RightVentricle1_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (RightVentricle1_t > (400.0)) {
      RightVentricle1_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( RightVentricle1_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricle1_t_init = RightVentricle1_t ;
      slope =  1 ;
      RightVentricle1_t_u = (slope * d) + RightVentricle1_t ;
      /* Possible Saturation */
      
      RightVentricle1_ACTpath  = False;
      RightVentricle1_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricle1!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (RightVentricle1_t > (10.0)) {
      RightVentricle1_t_u = 0 ;
      RightVentricle1_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( RightVentricle1_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricle1_t_init = RightVentricle1_t ;
      slope =  1 ;
      RightVentricle1_t_u = (slope * d) + RightVentricle1_t ;
      /* Possible Saturation */
      
      RightVentricle1_ACTpath  = False;
      RightVentricle1_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricle1!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (RightVentricle1_t > (200.0)) {
      RightVentricle1_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( RightVentricle1_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricle1_t_init = RightVentricle1_t ;
      slope =  1 ;
      RightVentricle1_t_u = (slope * d) + RightVentricle1_t ;
      /* Possible Saturation */
      
      RightVentricle1_ACTpath  = False;
      RightVentricle1_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricle1!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (RightVentricle1_ACTcell1) {
      RightVentricle1_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (RightVentricle1_t > (100.0)) {
      RightVentricle1_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( RightVentricle1_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricle1_t_init = RightVentricle1_t ;
      slope =  1 ;
      RightVentricle1_t_u = (slope * d) + RightVentricle1_t ;
      /* Possible Saturation */
      
      RightVentricle1_ACTpath  = False;
      RightVentricle1_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricle1!\n");
      exit(1);
    }
    break;
  }
  RightVentricle1_t = RightVentricle1_t_u;
  return cstate;
}