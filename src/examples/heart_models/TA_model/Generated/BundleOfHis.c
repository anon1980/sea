#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char BundleOfHis_ACTpath =  False ;
extern unsigned char  BundleOfHis_ACTcell1 ;//Events
static double  BundleOfHis_t  =  0 ; //the continuous vars
static double  BundleOfHis_t_u ; // and their updates
static double  BundleOfHis_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states BundleOfHis (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (BundleOfHis_ACTcell1) {
      BundleOfHis_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (BundleOfHis_t > (400.0)) {
      BundleOfHis_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( BundleOfHis_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis_t_init = BundleOfHis_t ;
      slope =  1 ;
      BundleOfHis_t_u = (slope * d) + BundleOfHis_t ;
      /* Possible Saturation */
      
      BundleOfHis_ACTpath  = False;
      BundleOfHis_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (BundleOfHis_t > (10.0)) {
      BundleOfHis_t_u = 0 ;
      BundleOfHis_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( BundleOfHis_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis_t_init = BundleOfHis_t ;
      slope =  1 ;
      BundleOfHis_t_u = (slope * d) + BundleOfHis_t ;
      /* Possible Saturation */
      
      BundleOfHis_ACTpath  = False;
      BundleOfHis_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (BundleOfHis_t > (200.0)) {
      BundleOfHis_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( BundleOfHis_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis_t_init = BundleOfHis_t ;
      slope =  1 ;
      BundleOfHis_t_u = (slope * d) + BundleOfHis_t ;
      /* Possible Saturation */
      
      BundleOfHis_ACTpath  = False;
      BundleOfHis_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (BundleOfHis_ACTcell1) {
      BundleOfHis_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (BundleOfHis_t > (100.0)) {
      BundleOfHis_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( BundleOfHis_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) BundleOfHis_t_init = BundleOfHis_t ;
      slope =  1 ;
      BundleOfHis_t_u = (slope * d) + BundleOfHis_t ;
      /* Possible Saturation */
      
      BundleOfHis_ACTpath  = False;
      BundleOfHis_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: BundleOfHis!\n");
      exit(1);
    }
    break;
  }
  BundleOfHis_t = BundleOfHis_t_u;
  return cstate;
}