#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char SA_OS_ACTcell =  False ;
extern unsigned char  SA_OS_ACTpath ;//Events
static double  SA_OS_t  =  0 ; //the continuous vars
static double  SA_OS_t_u ; // and their updates
static double  SA_OS_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states SA_OS (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (SA_OS_ACTpath) {
      SA_OS_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) SA_OS_t_init = SA_OS_t ;
      slope =  0 ;
      SA_OS_t_u = (slope * d) + SA_OS_t ;
      /* Possible Saturation */
      
      SA_OS_ACTcell  = False;
      SA_OS_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_OS!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (SA_OS_t > (20.0)) {
      SA_OS_t_u = 0 ;
      SA_OS_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( SA_OS_t <= (20.0)     ) {
      if ((pstate != cstate) || force_init_update) SA_OS_t_init = SA_OS_t ;
      slope =  1 ;
      SA_OS_t_u = (slope * d) + SA_OS_t ;
      /* Possible Saturation */
      
      SA_OS_ACTcell  = False;
      SA_OS_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: SA_OS!\n");
      exit(1);
    }
    break;
  }
  SA_OS_t = SA_OS_t_u;
  return cstate;
}