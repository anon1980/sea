#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char Slow1_ACTpath =  False ;
extern unsigned char  Slow1_ACTcell1 ;//Events
static double  Slow1_t  =  0 ; //the continuous vars
static double  Slow1_t_u ; // and their updates
static double  Slow1_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states Slow1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (Slow1_ACTcell1) {
      Slow1_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (Slow1_t > (400.0)) {
      Slow1_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( Slow1_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) Slow1_t_init = Slow1_t ;
      slope =  1 ;
      Slow1_t_u = (slope * d) + Slow1_t ;
      /* Possible Saturation */
      
      Slow1_ACTpath  = False;
      Slow1_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow1!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (Slow1_t > (10.0)) {
      Slow1_t_u = 0 ;
      Slow1_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( Slow1_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) Slow1_t_init = Slow1_t ;
      slope =  1 ;
      Slow1_t_u = (slope * d) + Slow1_t ;
      /* Possible Saturation */
      
      Slow1_ACTpath  = False;
      Slow1_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow1!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (Slow1_t > (200.0)) {
      Slow1_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( Slow1_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) Slow1_t_init = Slow1_t ;
      slope =  1 ;
      Slow1_t_u = (slope * d) + Slow1_t ;
      /* Possible Saturation */
      
      Slow1_ACTpath  = False;
      Slow1_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow1!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (Slow1_ACTcell1) {
      Slow1_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (Slow1_t > (100.0)) {
      Slow1_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( Slow1_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) Slow1_t_init = Slow1_t ;
      slope =  1 ;
      Slow1_t_u = (slope * d) + Slow1_t ;
      /* Possible Saturation */
      
      Slow1_ACTpath  = False;
      Slow1_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow1!\n");
      exit(1);
    }
    break;
  }
  Slow1_t = Slow1_t_u;
  return cstate;
}