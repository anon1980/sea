#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char CristaTerminalis1_ACTpath =  False ;
extern unsigned char  CristaTerminalis1_ACTcell1 ;//Events
static double  CristaTerminalis1_t  =  0 ; //the continuous vars
static double  CristaTerminalis1_t_u ; // and their updates
static double  CristaTerminalis1_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states CristaTerminalis1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (CristaTerminalis1_ACTcell1) {
      CristaTerminalis1_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (CristaTerminalis1_t > (400.0)) {
      CristaTerminalis1_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( CristaTerminalis1_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) CristaTerminalis1_t_init = CristaTerminalis1_t ;
      slope =  1 ;
      CristaTerminalis1_t_u = (slope * d) + CristaTerminalis1_t ;
      /* Possible Saturation */
      
      CristaTerminalis1_ACTpath  = False;
      CristaTerminalis1_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: CristaTerminalis1!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (CristaTerminalis1_t > (10.0)) {
      CristaTerminalis1_t_u = 0 ;
      CristaTerminalis1_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( CristaTerminalis1_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) CristaTerminalis1_t_init = CristaTerminalis1_t ;
      slope =  1 ;
      CristaTerminalis1_t_u = (slope * d) + CristaTerminalis1_t ;
      /* Possible Saturation */
      
      CristaTerminalis1_ACTpath  = False;
      CristaTerminalis1_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: CristaTerminalis1!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (CristaTerminalis1_t > (200.0)) {
      CristaTerminalis1_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( CristaTerminalis1_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) CristaTerminalis1_t_init = CristaTerminalis1_t ;
      slope =  1 ;
      CristaTerminalis1_t_u = (slope * d) + CristaTerminalis1_t ;
      /* Possible Saturation */
      
      CristaTerminalis1_ACTpath  = False;
      CristaTerminalis1_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: CristaTerminalis1!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (CristaTerminalis1_ACTcell1) {
      CristaTerminalis1_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (CristaTerminalis1_t > (100.0)) {
      CristaTerminalis1_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( CristaTerminalis1_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) CristaTerminalis1_t_init = CristaTerminalis1_t ;
      slope =  1 ;
      CristaTerminalis1_t_u = (slope * d) + CristaTerminalis1_t ;
      /* Possible Saturation */
      
      CristaTerminalis1_ACTpath  = False;
      CristaTerminalis1_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: CristaTerminalis1!\n");
      exit(1);
    }
    break;
  }
  CristaTerminalis1_t = CristaTerminalis1_t_u;
  return cstate;
}