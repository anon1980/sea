#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char CSLeftVentricular_ACTpath =  False ;
extern unsigned char  CSLeftVentricular_ACTcell1 ;//Events
static double  CSLeftVentricular_t  =  0 ; //the continuous vars
static double  CSLeftVentricular_t_u ; // and their updates
static double  CSLeftVentricular_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states CSLeftVentricular (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (CSLeftVentricular_ACTcell1) {
      CSLeftVentricular_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (CSLeftVentricular_t > (400.0)) {
      CSLeftVentricular_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( CSLeftVentricular_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) CSLeftVentricular_t_init = CSLeftVentricular_t ;
      slope =  1 ;
      CSLeftVentricular_t_u = (slope * d) + CSLeftVentricular_t ;
      /* Possible Saturation */
      
      CSLeftVentricular_ACTpath  = False;
      CSLeftVentricular_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: CSLeftVentricular!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (CSLeftVentricular_t > (10.0)) {
      CSLeftVentricular_t_u = 0 ;
      CSLeftVentricular_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( CSLeftVentricular_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) CSLeftVentricular_t_init = CSLeftVentricular_t ;
      slope =  1 ;
      CSLeftVentricular_t_u = (slope * d) + CSLeftVentricular_t ;
      /* Possible Saturation */
      
      CSLeftVentricular_ACTpath  = False;
      CSLeftVentricular_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: CSLeftVentricular!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (CSLeftVentricular_t > (200.0)) {
      CSLeftVentricular_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( CSLeftVentricular_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) CSLeftVentricular_t_init = CSLeftVentricular_t ;
      slope =  1 ;
      CSLeftVentricular_t_u = (slope * d) + CSLeftVentricular_t ;
      /* Possible Saturation */
      
      CSLeftVentricular_ACTpath  = False;
      CSLeftVentricular_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: CSLeftVentricular!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (CSLeftVentricular_ACTcell1) {
      CSLeftVentricular_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (CSLeftVentricular_t > (100.0)) {
      CSLeftVentricular_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( CSLeftVentricular_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) CSLeftVentricular_t_init = CSLeftVentricular_t ;
      slope =  1 ;
      CSLeftVentricular_t_u = (slope * d) + CSLeftVentricular_t ;
      /* Possible Saturation */
      
      CSLeftVentricular_ACTpath  = False;
      CSLeftVentricular_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: CSLeftVentricular!\n");
      exit(1);
    }
    break;
  }
  CSLeftVentricular_t = CSLeftVentricular_t_u;
  return cstate;
}