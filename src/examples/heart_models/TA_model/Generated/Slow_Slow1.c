#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char Slow_Slow1_ACTcell =  False ;
extern unsigned char  Slow_Slow1_ACTpath ;//Events
static double  Slow_Slow1_t  =  0 ; //the continuous vars
static double  Slow_Slow1_t_u ; // and their updates
static double  Slow_Slow1_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states Slow_Slow1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (Slow_Slow1_ACTpath) {
      Slow_Slow1_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) Slow_Slow1_t_init = Slow_Slow1_t ;
      slope =  0 ;
      Slow_Slow1_t_u = (slope * d) + Slow_Slow1_t ;
      /* Possible Saturation */
      
      Slow_Slow1_ACTcell  = False;
      Slow_Slow1_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow_Slow1!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (Slow_Slow1_t > (30.0)) {
      Slow_Slow1_t_u = 0 ;
      Slow_Slow1_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( Slow_Slow1_t <= (30.0)     ) {
      if ((pstate != cstate) || force_init_update) Slow_Slow1_t_init = Slow_Slow1_t ;
      slope =  1 ;
      Slow_Slow1_t_u = (slope * d) + Slow_Slow1_t ;
      /* Possible Saturation */
      
      Slow_Slow1_ACTcell  = False;
      Slow_Slow1_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Slow_Slow1!\n");
      exit(1);
    }
    break;
  }
  Slow_Slow1_t = Slow_Slow1_t_u;
  return cstate;
}