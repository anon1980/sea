#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char RBB_RBB1_ACTcell =  False ;
extern unsigned char  RBB_RBB1_ACTpath ;//Events
static double  RBB_RBB1_t  =  0 ; //the continuous vars
static double  RBB_RBB1_t_u ; // and their updates
static double  RBB_RBB1_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states RBB_RBB1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (RBB_RBB1_ACTpath) {
      RBB_RBB1_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RBB_RBB1_t_init = RBB_RBB1_t ;
      slope =  0 ;
      RBB_RBB1_t_u = (slope * d) + RBB_RBB1_t ;
      /* Possible Saturation */
      
      RBB_RBB1_ACTcell  = False;
      RBB_RBB1_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB_RBB1!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (RBB_RBB1_t > (5.0)) {
      RBB_RBB1_t_u = 0 ;
      RBB_RBB1_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( RBB_RBB1_t <= (5.0)     ) {
      if ((pstate != cstate) || force_init_update) RBB_RBB1_t_init = RBB_RBB1_t ;
      slope =  1 ;
      RBB_RBB1_t_u = (slope * d) + RBB_RBB1_t ;
      /* Possible Saturation */
      
      RBB_RBB1_ACTcell  = False;
      RBB_RBB1_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RBB_RBB1!\n");
      exit(1);
    }
    break;
  }
  RBB_RBB1_t = RBB_RBB1_t_u;
  return cstate;
}