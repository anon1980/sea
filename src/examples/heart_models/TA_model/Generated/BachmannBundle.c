#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char BachmannBundle_ACTpath =  False ;
extern unsigned char  BachmannBundle_ACTcell1 ;//Events
static double  BachmannBundle_t  =  0 ; //the continuous vars
static double  BachmannBundle_t_u ; // and their updates
static double  BachmannBundle_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states BachmannBundle (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (BachmannBundle_ACTcell1) {
      BachmannBundle_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (BachmannBundle_t > (400.0)) {
      BachmannBundle_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( BachmannBundle_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) BachmannBundle_t_init = BachmannBundle_t ;
      slope =  1 ;
      BachmannBundle_t_u = (slope * d) + BachmannBundle_t ;
      /* Possible Saturation */
      
      BachmannBundle_ACTpath  = False;
      BachmannBundle_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: BachmannBundle!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (BachmannBundle_t > (10.0)) {
      BachmannBundle_t_u = 0 ;
      BachmannBundle_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( BachmannBundle_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) BachmannBundle_t_init = BachmannBundle_t ;
      slope =  1 ;
      BachmannBundle_t_u = (slope * d) + BachmannBundle_t ;
      /* Possible Saturation */
      
      BachmannBundle_ACTpath  = False;
      BachmannBundle_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: BachmannBundle!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (BachmannBundle_t > (200.0)) {
      BachmannBundle_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( BachmannBundle_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) BachmannBundle_t_init = BachmannBundle_t ;
      slope =  1 ;
      BachmannBundle_t_u = (slope * d) + BachmannBundle_t ;
      /* Possible Saturation */
      
      BachmannBundle_ACTpath  = False;
      BachmannBundle_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: BachmannBundle!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (BachmannBundle_ACTcell1) {
      BachmannBundle_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (BachmannBundle_t > (100.0)) {
      BachmannBundle_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( BachmannBundle_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) BachmannBundle_t_init = BachmannBundle_t ;
      slope =  1 ;
      BachmannBundle_t_u = (slope * d) + BachmannBundle_t ;
      /* Possible Saturation */
      
      BachmannBundle_ACTpath  = False;
      BachmannBundle_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: BachmannBundle!\n");
      exit(1);
    }
    break;
  }
  BachmannBundle_t = BachmannBundle_t_u;
  return cstate;
}