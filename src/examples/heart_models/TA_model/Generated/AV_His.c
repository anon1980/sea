#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char AV_His_ACTcell =  False ;
extern unsigned char  AV_His_ACTpath ;//Events
static double  AV_His_t  =  0 ; //the continuous vars
static double  AV_His_t_u ; // and their updates
static double  AV_His_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states AV_His (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (AV_His_ACTpath) {
      AV_His_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) AV_His_t_init = AV_His_t ;
      slope =  0 ;
      AV_His_t_u = (slope * d) + AV_His_t ;
      /* Possible Saturation */
      
      AV_His_ACTcell  = False;
      AV_His_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: AV_His!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (AV_His_t > (30.0)) {
      AV_His_t_u = 0 ;
      AV_His_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( AV_His_t <= (30.0)     ) {
      if ((pstate != cstate) || force_init_update) AV_His_t_init = AV_His_t ;
      slope =  1 ;
      AV_His_t_u = (slope * d) + AV_His_t ;
      /* Possible Saturation */
      
      AV_His_ACTcell  = False;
      AV_His_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: AV_His!\n");
      exit(1);
    }
    break;
  }
  AV_His_t = AV_His_t_u;
  return cstate;
}