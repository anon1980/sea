#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char RA1_CS_ACTcell =  False ;
extern unsigned char  RA1_CS_ACTpath ;//Events
static double  RA1_CS_t  =  0 ; //the continuous vars
static double  RA1_CS_t_u ; // and their updates
static double  RA1_CS_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states RA1_CS (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (RA1_CS_ACTpath) {
      RA1_CS_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RA1_CS_t_init = RA1_CS_t ;
      slope =  0 ;
      RA1_CS_t_u = (slope * d) + RA1_CS_t ;
      /* Possible Saturation */
      
      RA1_CS_ACTcell  = False;
      RA1_CS_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RA1_CS!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (RA1_CS_t > (20.0)) {
      RA1_CS_t_u = 0 ;
      RA1_CS_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( RA1_CS_t <= (20.0)     ) {
      if ((pstate != cstate) || force_init_update) RA1_CS_t_init = RA1_CS_t ;
      slope =  1 ;
      RA1_CS_t_u = (slope * d) + RA1_CS_t ;
      /* Possible Saturation */
      
      RA1_CS_ACTcell  = False;
      RA1_CS_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RA1_CS!\n");
      exit(1);
    }
    break;
  }
  RA1_CS_t = RA1_CS_t_u;
  return cstate;
}