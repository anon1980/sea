#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char RightAtrium_ACTpath =  False ;
extern unsigned char  RightAtrium_ACTcell1 ;//Events
static double  RightAtrium_t  =  0 ; //the continuous vars
static double  RightAtrium_t_u ; // and their updates
static double  RightAtrium_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states RightAtrium (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (RightAtrium_ACTcell1) {
      RightAtrium_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (RightAtrium_t > (400.0)) {
      RightAtrium_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( RightAtrium_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) RightAtrium_t_init = RightAtrium_t ;
      slope =  1 ;
      RightAtrium_t_u = (slope * d) + RightAtrium_t ;
      /* Possible Saturation */
      
      RightAtrium_ACTpath  = False;
      RightAtrium_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightAtrium!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (RightAtrium_t > (10.0)) {
      RightAtrium_t_u = 0 ;
      RightAtrium_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( RightAtrium_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) RightAtrium_t_init = RightAtrium_t ;
      slope =  1 ;
      RightAtrium_t_u = (slope * d) + RightAtrium_t ;
      /* Possible Saturation */
      
      RightAtrium_ACTpath  = False;
      RightAtrium_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightAtrium!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (RightAtrium_t > (200.0)) {
      RightAtrium_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( RightAtrium_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) RightAtrium_t_init = RightAtrium_t ;
      slope =  1 ;
      RightAtrium_t_u = (slope * d) + RightAtrium_t ;
      /* Possible Saturation */
      
      RightAtrium_ACTpath  = False;
      RightAtrium_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightAtrium!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (RightAtrium_ACTcell1) {
      RightAtrium_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (RightAtrium_t > (100.0)) {
      RightAtrium_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( RightAtrium_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) RightAtrium_t_init = RightAtrium_t ;
      slope =  1 ;
      RightAtrium_t_u = (slope * d) + RightAtrium_t ;
      /* Possible Saturation */
      
      RightAtrium_ACTpath  = False;
      RightAtrium_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightAtrium!\n");
      exit(1);
    }
    break;
  }
  RightAtrium_t = RightAtrium_t_u;
  return cstate;
}