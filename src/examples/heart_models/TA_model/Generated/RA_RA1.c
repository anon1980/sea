#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char RA_RA1_ACTcell =  False ;
extern unsigned char  RA_RA1_ACTpath ;//Events
static double  RA_RA1_t  =  0 ; //the continuous vars
static double  RA_RA1_t_u ; // and their updates
static double  RA_RA1_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states RA_RA1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (RA_RA1_ACTpath) {
      RA_RA1_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) RA_RA1_t_init = RA_RA1_t ;
      slope =  0 ;
      RA_RA1_t_u = (slope * d) + RA_RA1_t ;
      /* Possible Saturation */
      
      RA_RA1_ACTcell  = False;
      RA_RA1_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RA_RA1!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (RA_RA1_t > (20.0)) {
      RA_RA1_t_u = 0 ;
      RA_RA1_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( RA_RA1_t <= (20.0)     ) {
      if ((pstate != cstate) || force_init_update) RA_RA1_t_init = RA_RA1_t ;
      slope =  1 ;
      RA_RA1_t_u = (slope * d) + RA_RA1_t ;
      /* Possible Saturation */
      
      RA_RA1_ACTcell  = False;
      RA_RA1_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RA_RA1!\n");
      exit(1);
    }
    break;
  }
  RA_RA1_t = RA_RA1_t_u;
  return cstate;
}