#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char LBB_LBB1_ACTcell =  False ;
extern unsigned char  LBB_LBB1_ACTpath ;//Events
static double  LBB_LBB1_t  =  0 ; //the continuous vars
static double  LBB_LBB1_t_u ; // and their updates
static double  LBB_LBB1_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { IDLE , ACT }; // state declarations

enum states LBB_LBB1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( IDLE ):
    if (True == False) {;}
    else if  (LBB_LBB1_ACTpath) {
      LBB_LBB1_t_u = 0 ;
      cstate =  ACT ;
      force_init_update = False;
    }

    else if ( True     ) {
      if ((pstate != cstate) || force_init_update) LBB_LBB1_t_init = LBB_LBB1_t ;
      slope =  0 ;
      LBB_LBB1_t_u = (slope * d) + LBB_LBB1_t ;
      /* Possible Saturation */
      
      LBB_LBB1_ACTcell  = False;
      LBB_LBB1_ACTpath  = False;
      cstate =  IDLE ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB_LBB1!\n");
      exit(1);
    }
    break;
  case ( ACT ):
    if (True == False) {;}
    else if  (LBB_LBB1_t > (5.0)) {
      LBB_LBB1_t_u = 0 ;
      LBB_LBB1_ACTcell = True;
      cstate =  IDLE ;
      force_init_update = False;
    }

    else if ( LBB_LBB1_t <= (5.0)     ) {
      if ((pstate != cstate) || force_init_update) LBB_LBB1_t_init = LBB_LBB1_t ;
      slope =  1 ;
      LBB_LBB1_t_u = (slope * d) + LBB_LBB1_t ;
      /* Possible Saturation */
      
      LBB_LBB1_ACTcell  = False;
      LBB_LBB1_ACTpath  = False;
      cstate =  ACT ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LBB_LBB1!\n");
      exit(1);
    }
    break;
  }
  LBB_LBB1_t = LBB_LBB1_t_u;
  return cstate;
}