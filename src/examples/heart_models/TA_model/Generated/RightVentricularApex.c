#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char RightVentricularApex_ACTpath =  False ;
extern unsigned char  RightVentricularApex_ACTcell1 ;//Events
static double  RightVentricularApex_t  =  0 ; //the continuous vars
static double  RightVentricularApex_t_u ; // and their updates
static double  RightVentricularApex_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states RightVentricularApex (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (RightVentricularApex_ACTcell1) {
      RightVentricularApex_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (RightVentricularApex_t > (400.0)) {
      RightVentricularApex_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( RightVentricularApex_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularApex_t_init = RightVentricularApex_t ;
      slope =  1 ;
      RightVentricularApex_t_u = (slope * d) + RightVentricularApex_t ;
      /* Possible Saturation */
      
      RightVentricularApex_ACTpath  = False;
      RightVentricularApex_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularApex!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (RightVentricularApex_t > (10.0)) {
      RightVentricularApex_t_u = 0 ;
      RightVentricularApex_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( RightVentricularApex_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularApex_t_init = RightVentricularApex_t ;
      slope =  1 ;
      RightVentricularApex_t_u = (slope * d) + RightVentricularApex_t ;
      /* Possible Saturation */
      
      RightVentricularApex_ACTpath  = False;
      RightVentricularApex_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularApex!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (RightVentricularApex_t > (200.0)) {
      RightVentricularApex_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( RightVentricularApex_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularApex_t_init = RightVentricularApex_t ;
      slope =  1 ;
      RightVentricularApex_t_u = (slope * d) + RightVentricularApex_t ;
      /* Possible Saturation */
      
      RightVentricularApex_ACTpath  = False;
      RightVentricularApex_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularApex!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (RightVentricularApex_ACTcell1) {
      RightVentricularApex_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (RightVentricularApex_t > (100.0)) {
      RightVentricularApex_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( RightVentricularApex_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) RightVentricularApex_t_init = RightVentricularApex_t ;
      slope =  1 ;
      RightVentricularApex_t_u = (slope * d) + RightVentricularApex_t ;
      /* Possible Saturation */
      
      RightVentricularApex_ACTpath  = False;
      RightVentricularApex_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightVentricularApex!\n");
      exit(1);
    }
    break;
  }
  RightVentricularApex_t = RightVentricularApex_t_u;
  return cstate;
}