#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char RightBundleBranch_ACTpath =  False ;
extern unsigned char  RightBundleBranch_ACTcell1 ;//Events
static double  RightBundleBranch_t  =  0 ; //the continuous vars
static double  RightBundleBranch_t_u ; // and their updates
static double  RightBundleBranch_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states RightBundleBranch (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (RightBundleBranch_ACTcell1) {
      RightBundleBranch_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (RightBundleBranch_t > (400.0)) {
      RightBundleBranch_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( RightBundleBranch_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) RightBundleBranch_t_init = RightBundleBranch_t ;
      slope =  1 ;
      RightBundleBranch_t_u = (slope * d) + RightBundleBranch_t ;
      /* Possible Saturation */
      
      RightBundleBranch_ACTpath  = False;
      RightBundleBranch_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightBundleBranch!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (RightBundleBranch_t > (10.0)) {
      RightBundleBranch_t_u = 0 ;
      RightBundleBranch_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( RightBundleBranch_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) RightBundleBranch_t_init = RightBundleBranch_t ;
      slope =  1 ;
      RightBundleBranch_t_u = (slope * d) + RightBundleBranch_t ;
      /* Possible Saturation */
      
      RightBundleBranch_ACTpath  = False;
      RightBundleBranch_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightBundleBranch!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (RightBundleBranch_t > (200.0)) {
      RightBundleBranch_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( RightBundleBranch_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) RightBundleBranch_t_init = RightBundleBranch_t ;
      slope =  1 ;
      RightBundleBranch_t_u = (slope * d) + RightBundleBranch_t ;
      /* Possible Saturation */
      
      RightBundleBranch_ACTpath  = False;
      RightBundleBranch_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightBundleBranch!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (RightBundleBranch_ACTcell1) {
      RightBundleBranch_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (RightBundleBranch_t > (100.0)) {
      RightBundleBranch_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( RightBundleBranch_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) RightBundleBranch_t_init = RightBundleBranch_t ;
      slope =  1 ;
      RightBundleBranch_t_u = (slope * d) + RightBundleBranch_t ;
      /* Possible Saturation */
      
      RightBundleBranch_ACTpath  = False;
      RightBundleBranch_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: RightBundleBranch!\n");
      exit(1);
    }
    break;
  }
  RightBundleBranch_t = RightBundleBranch_t_u;
  return cstate;
}