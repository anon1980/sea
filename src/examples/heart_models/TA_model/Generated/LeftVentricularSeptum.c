#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0



unsigned char LeftVentricularSeptum_ACTpath =  False ;
extern unsigned char  LeftVentricularSeptum_ACTcell1 ;//Events
static double  LeftVentricularSeptum_t  =  0 ; //the continuous vars
static double  LeftVentricularSeptum_t_u ; // and their updates
static double  LeftVentricularSeptum_t_init ; // and their inits
static unsigned char force_init_update;
extern double d; // the time step
static double slope; // the slope
enum states { REST , ST , ERP , RRP }; // state declarations

enum states LeftVentricularSeptum (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( REST ):
    if (True == False) {;}
    else if  (LeftVentricularSeptum_ACTcell1) {
      LeftVentricularSeptum_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (LeftVentricularSeptum_t > (400.0)) {
      LeftVentricularSeptum_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }

    else if ( LeftVentricularSeptum_t <= (400.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum_t_init = LeftVentricularSeptum_t ;
      slope =  1 ;
      LeftVentricularSeptum_t_u = (slope * d) + LeftVentricularSeptum_t ;
      /* Possible Saturation */
      
      LeftVentricularSeptum_ACTpath  = False;
      LeftVentricularSeptum_ACTcell1  = False;
      cstate =  REST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularSeptum!\n");
      exit(1);
    }
    break;
  case ( ST ):
    if (True == False) {;}
    else if  (LeftVentricularSeptum_t > (10.0)) {
      LeftVentricularSeptum_t_u = 0 ;
      LeftVentricularSeptum_ACTpath = True;
      cstate =  ERP ;
      force_init_update = False;
    }

    else if ( LeftVentricularSeptum_t <= (10.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum_t_init = LeftVentricularSeptum_t ;
      slope =  1 ;
      LeftVentricularSeptum_t_u = (slope * d) + LeftVentricularSeptum_t ;
      /* Possible Saturation */
      
      LeftVentricularSeptum_ACTpath  = False;
      LeftVentricularSeptum_ACTcell1  = False;
      cstate =  ST ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularSeptum!\n");
      exit(1);
    }
    break;
  case ( ERP ):
    if (True == False) {;}
    else if  (LeftVentricularSeptum_t > (200.0)) {
      LeftVentricularSeptum_t_u = 0 ;
      cstate =  RRP ;
      force_init_update = False;
    }

    else if ( LeftVentricularSeptum_t <= (200.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum_t_init = LeftVentricularSeptum_t ;
      slope =  1 ;
      LeftVentricularSeptum_t_u = (slope * d) + LeftVentricularSeptum_t ;
      /* Possible Saturation */
      
      LeftVentricularSeptum_ACTpath  = False;
      LeftVentricularSeptum_ACTcell1  = False;
      cstate =  ERP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularSeptum!\n");
      exit(1);
    }
    break;
  case ( RRP ):
    if (True == False) {;}
    else if  (LeftVentricularSeptum_ACTcell1) {
      LeftVentricularSeptum_t_u = 0 ;
      cstate =  ST ;
      force_init_update = False;
    }
    else if  (LeftVentricularSeptum_t > (100.0)) {
      LeftVentricularSeptum_t_u = 0 ;
      cstate =  REST ;
      force_init_update = False;
    }

    else if ( LeftVentricularSeptum_t <= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) LeftVentricularSeptum_t_init = LeftVentricularSeptum_t ;
      slope =  1 ;
      LeftVentricularSeptum_t_u = (slope * d) + LeftVentricularSeptum_t ;
      /* Possible Saturation */
      
      LeftVentricularSeptum_ACTpath  = False;
      LeftVentricularSeptum_ACTcell1  = False;
      cstate =  RRP ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: LeftVentricularSeptum!\n");
      exit(1);
    }
    break;
  }
  LeftVentricularSeptum_t = LeftVentricularSeptum_t_u;
  return cstate;
}