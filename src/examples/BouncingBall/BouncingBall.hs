module BouncingBall where
import Language
import qualified Hshac as H
import Prelude hiding ((==),(/=), LT, GT, (<=), (>=), (<), (>), (&&), not)
import Text.PrettyPrint.ANSI.Leijen
import GHC.IO.Handle
import System.IO

dvdt :: Diff
dvdt = DiffT (S "v")

dhdt :: Diff
dhdt = DiffT (S "h")

odev :: Ode
odev = Ode dvdt (-9.81) 10

odeh :: Ode
odeh = Ode dhdt (A(S "v")) 100

assignh :: Update (External Output V)
assignh = OutputVar (S "h") := A (S "h")

assignv :: Update (External Output V)
assignv = OutputVar (S "v") := A (S "v")

t1 :: Loc
t1 = Loc (L "t1") [odev, odeh] 
                  [InvariantLoc dhdt (T(S "h") > TR 0), InvariantLoc dvdt (TTrue)]
                  ([], [assignh, assignv])

e1 :: Edge
e1 = Edge t1 t1 [InvariantEdge ((T(S "h") == TR 0) && (T(S "v") <= TR 0))] 
                ([(S "h") := (A (S "h") + 0.00001), 
                  (S "v") := (A (S "v") * (-0.5))], 
                 [(OutputVar (S "h")) := A (S "h"),
                  (OutputVar (S "v")) := A (S "v")], []) 
                []

bouncingBall :: Ha
bouncingBall = Ha (L "BouncingBall") [t1] t1 [e1] 
               [SymbolDecl (S "v") 10, SymbolDecl (S "h") 100] [] [OutputVarDecl (S "h") 100, OutputVarDecl (S "v") 10]
               [] []

main :: IO ()
main = do 
       h <- openFile "bouncingBall.c" WriteMode
       x <- openFile "Main.c" WriteMode
       let (dd, others) = H.compileReaction bouncingBall
           (oo1, oo2) = others
       hPutDoc h dd
       hPutDoc x (H.mkMain (LastHa bouncingBall) [] [oo1] [oo2])
       hClose x
       hClose h

