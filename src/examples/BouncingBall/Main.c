#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>

#define True 1
#define False 0
extern void readInput();
extern void writeOutput();
extern int BouncingBall(int, int);

int main (void){
  int BouncingBall_cstate = 0 ;
  int BouncingBall_pstate = -1;
  
  while(True) {
    readInput();
    
    int BouncingBall_rstate = BouncingBall (BouncingBall_cstate, BouncingBall_pstate);
    BouncingBall_pstate = BouncingBall_cstate;
    BouncingBall_cstate = BouncingBall_rstate;
    writeOutput();
  }
  return 0;
}