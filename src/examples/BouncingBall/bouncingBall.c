#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0


double BouncingBall_h = 100.0;
double BouncingBall_v = 10.0;


static double  v  =  10 ,  h  =  100 ; //the continuous vars
static double  v_u , h_u ; // and their updates
static double  v_init , h_init ; // and their inits
static double  slope_v , slope_h ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { t1 }; // state declarations

enum states BouncingBall (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( t1 ):
    if (True == False) {;}
    else if  (h == (0.0) && (v <= (0.0))) {
      h_u = (h + 1.0e-5) ;
      v_u = (v * (- 0.5)) ;
      BouncingBall_h = h ;
      BouncingBall_v = v ;
      cstate =  t1 ;
      force_init_update = True;
    }

    else if ( h > (0.0) && 
              True     ) {
      if ((pstate != cstate) || force_init_update) v_init = v ;
      slope_v = (- 9.81) ;
      v_u = (slope_v * d) + v ;
      if ((pstate != cstate) || force_init_update) h_init = h ;
      slope_h = v ;
      h_u = (slope_h * d) + h ;
      /* Possible Saturation */
      
      h_u = (h_u <  0.0  && signbit(slope_h ) &&  h_init >  0.0)  ?  0.0 : h_u ;
      h_u = (h_u >  0.0  && !signbit(slope_h ) &&  h_init <  0.0)  ?  0.0 : h_u ;
      
      
      cstate =  t1 ;
      force_init_update = False;
      
      BouncingBall_h = h ;
      BouncingBall_v = v ;
    }
    else {
      fprintf(stderr, "Unreachable state in: BouncingBall!\n");
      exit(1);
    }
    break;
  }
  v = v_u;
  h = h_u;
  return cstate;
}