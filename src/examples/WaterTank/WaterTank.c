#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0


double waterTank_x = 20.0;

extern unsigned char  waterTank_ON , waterTank_OFF ;//Events
static double  x  =  20.0 ; //the continuous vars
static double  x_u ; // and their updates
static double  x_init ; // and their inits
static double  slope_x ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { t1 , t2 , t3 , t4 }; // state declarations

enum states waterTank (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( t1 ):
    if (True == False) {;}
    else if  (True && 
              waterTank_OFF) {
      x_u = x ;
      waterTank_x = x ;
      cstate =  t3 ;
      force_init_update = False;
    }
    else if  (x == (100.0) && 
              waterTank_ON) {
      x_u = x ;
      waterTank_x = x ;
      cstate =  t2 ;
      force_init_update = False;
    }

    else if ( x >= (20.0) && (x <= (100.0))     ) {
      if ((pstate != cstate) || force_init_update) x_init = x ;
      slope_x = (7.5e-2 * (150 + (- x))) ;
      x_u = (slope_x * d) + x ;
      /* Possible Saturation */
      x_u = (x_u <  20.0  && signbit(slope_x ) &&  x_init >  20.0)  ?  20.0 : x_u ;
      x_u = (x_u >  100.0  && !signbit(slope_x ) &&  x_init <  100.0)  ?  100.0 : x_u ;
      
      waterTank_ON  = False;
      waterTank_OFF  = False;
      cstate =  t1 ;
      force_init_update = False;
      
      waterTank_x = x ;
    }
    else {
      fprintf(stderr, "Unreachable state in: waterTank!\n");
      exit(1);
    }
    break;
  case ( t2 ):
    if (True == False) {;}
    else if  (True && 
              waterTank_OFF) {
      x_u = x ;
      waterTank_x = x ;
      cstate =  t3 ;
      force_init_update = False;
    }

    else if ( x == (100.0)     ) {
      if ((pstate != cstate) || force_init_update) x_init = x ;
      slope_x = 0 ;
      x_u = (slope_x * d) + x ;
      /* Possible Saturation */
      x_u = (x_u <  100.0  && signbit(slope_x ) &&  x_init >  100.0)  ?  100.0 : x_u ;
      x_u = (x_u >  100.0  && !signbit(slope_x ) &&  x_init <  100.0)  ?  100.0 : x_u ;
      
      waterTank_ON  = False;
      waterTank_OFF  = False;
      cstate =  t2 ;
      force_init_update = False;
      
      waterTank_x = x ;
    }
    else {
      fprintf(stderr, "Unreachable state in: waterTank!\n");
      exit(1);
    }
    break;
  case ( t3 ):
    if (True == False) {;}
    else if  (x == (20.0)) {
      x_u = x ;
      waterTank_x = x ;
      cstate =  t4 ;
      force_init_update = False;
    }
    else if  (True && 
              waterTank_ON) {
      x_u = x ;
      waterTank_x = x ;
      cstate =  t1 ;
      force_init_update = False;
    }

    else if ( x >= (20.0) && (x <= (100.0))     ) {
      if ((pstate != cstate) || force_init_update) x_init = x ;
      slope_x = (- (7.5e-2 * x)) ;
      x_u = (slope_x * d) + x ;
      /* Possible Saturation */
      x_u = (x_u <  20.0  && signbit(slope_x ) &&  x_init >  20.0)  ?  20.0 : x_u ;
      x_u = (x_u >  100.0  && !signbit(slope_x ) &&  x_init <  100.0)  ?  100.0 : x_u ;
      
      waterTank_ON  = False;
      waterTank_OFF  = False;
      cstate =  t3 ;
      force_init_update = False;
      
      waterTank_x = x ;
    }
    else {
      fprintf(stderr, "Unreachable state in: waterTank!\n");
      exit(1);
    }
    break;
  case ( t4 ):
    if (True == False) {;}
    else if  (True && 
              waterTank_ON) {
      x_u = x ;
      waterTank_x = x ;
      cstate =  t1 ;
      force_init_update = False;
    }

    else if ( x == (20.0)     ) {
      if ((pstate != cstate) || force_init_update) x_init = x ;
      slope_x = 0 ;
      x_u = (slope_x * d) + x ;
      /* Possible Saturation */
      x_u = (x_u <  20.0  && signbit(slope_x ) &&  x_init >  20.0)  ?  20.0 : x_u ;
      x_u = (x_u >  20.0  && !signbit(slope_x ) &&  x_init <  20.0)  ?  20.0 : x_u ;
      
      waterTank_ON  = False;
      waterTank_OFF  = False;
      cstate =  t4 ;
      force_init_update = False;
      
      waterTank_x = x ;
    }
    else {
      fprintf(stderr, "Unreachable state in: waterTank!\n");
      exit(1);
    }
    break;
  }
  x = x_u;
  return cstate;
}