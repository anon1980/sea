int px = 20 ;
ltl p {[]!(px >= 100)};
c_decl {\#include<math.h>};
c_decl{extern double d;};
bool controller_tick = false;
bool waterTank_tick = false;

mtype controller_cstate , controller_pstate ;
bool controller_force_init_update ;

mtype waterTank_cstate , waterTank_pstate ;
bool waterTank_force_init_update ;


c_decl {


};
c_decl { double x_u, x_slope, x_init, x =  20.0 ;};
c_track "&x" "sizeof(double)";
c_track "&x_slope" "sizeof(double)";
c_track "&x_u" "sizeof(double)";
c_track "&x_init" "sizeof(double)";
c_decl {double  waterTank_x = 20.0 ;};
c_track "&waterTank_x" "sizeof(double)";


bool waterTank_ON ;
bool waterTank_OFF ;
mtype { waterTank_t1, waterTank_t2, waterTank_t3, waterTank_t4 };
proctype waterTank () {
T1:
  if
    :: (c_expr { ((x >= 20.0) && (x <= 100.0)) }  &&  (! waterTank_ON)
	&& 
	(! waterTank_OFF)  &&  waterTank_cstate == waterTank_t1) ->
       if
	 :: waterTank_pstate != waterTank_cstate || waterTank_force_init_update ->
	    c_code { x_init = x ; };
	 :: else -> true
       fi;
       d_step {
	 c_code {
	   x_slope =  (7.5e-2 * (150 + (- x))) ;
	   x_u = (x_slope * d) + x ;
	   x_u = (x_u <  20.0  && signbit(x_slope) &&  x_init >  20.0)  ?  20.0 : x_u ;
	   x_u = (x_u >  100.0  && !signbit(x_slope) &&  x_init <  100.0)  ?  100.0 : x_u ;
	   
	   waterTank_x = x ;
	 };
	 waterTank_pstate = waterTank_cstate;
	 waterTank_cstate = waterTank_t1;
	 waterTank_force_init_update = false;
	 
	 waterTank_ON  = false;
	 waterTank_OFF  = false;
	 waterTank_tick = true;
       };
       do
	 :: waterTank_tick -> true
	 :: else -> goto  T1
       od;
    ::  (c_expr { 1 } && 
	 waterTank_OFF && 
	 waterTank_cstate == waterTank_t1)  -> 
       d_step {
	 c_code {  x_u = x ; };
	 c_code {  waterTank_x = x ; };
	 waterTank_pstate =  waterTank_cstate ;
	 waterTank_cstate =  waterTank_t3 ;
	 waterTank_force_init_update = false;
	 waterTank_tick = true;
       };
       do
	 :: waterTank_tick -> true
	 :: else -> goto  T3
       od;
    ::  (c_expr { (x == 100.0) }
	 && waterTank_ON && 
	 waterTank_cstate == waterTank_t1)  -> 
       d_step {
	 c_code {  x_u = x ; };
	 c_code {  waterTank_x = x ; };
	 waterTank_pstate =  waterTank_cstate ;
	 waterTank_cstate =  waterTank_t2 ;
	 waterTank_force_init_update = false;
	 waterTank_tick = true;
       };
       do
	 :: waterTank_tick -> true
	 :: else -> goto  T2
       od;

    :: else -> true
  fi;
T2:
  if
    :: (c_expr { (x == 100.0) }  &&  (! waterTank_ON)
	&& 
	(! waterTank_OFF)  &&  waterTank_cstate == waterTank_t2) ->
       if
	 :: waterTank_pstate != waterTank_cstate || waterTank_force_init_update ->
	    c_code { x_init = x ; };
	 :: else -> true
       fi;
       d_step {
	 c_code {
	   x_slope =  0 ;
	   x_u = (x_slope * d) + x ;
	   x_u = (x_u <  100.0  && signbit(x_slope) &&  x_init >  100.0)  ?  100.0 : x_u ;
	   x_u = (x_u >  100.0  && !signbit(x_slope) &&  x_init <  100.0)  ?  100.0 : x_u ;
	   
	   waterTank_x = x ;
	 };
	 waterTank_pstate = waterTank_cstate;
	 waterTank_cstate = waterTank_t2;
	 waterTank_force_init_update = false;
	 
	 waterTank_ON  = false;
	 waterTank_OFF  = false;
	 waterTank_tick = true;
       };
       do
	 :: waterTank_tick -> true
	 :: else -> goto  T2
       od;
    ::  (c_expr { 1 } && 
	 waterTank_OFF && 
	 waterTank_cstate == waterTank_t2)  -> 
       d_step {
	 c_code {  x_u = x ; };
	 c_code {  waterTank_x = x ; };
	 waterTank_pstate =  waterTank_cstate ;
	 waterTank_cstate =  waterTank_t3 ;
	 waterTank_force_init_update = false;
	 waterTank_tick = true;
       };
       do
	 :: waterTank_tick -> true
	 :: else -> goto  T3
       od;

    :: else -> true
  fi;
T3:
  if
    :: (c_expr { ((x >= 20.0) && (x <= 100.0)) }  &&  (! waterTank_ON)
	&& 
	(! waterTank_OFF)  &&  waterTank_cstate == waterTank_t3) ->
       if
	 :: waterTank_pstate != waterTank_cstate || waterTank_force_init_update ->
	    c_code { x_init = x ; };
	 :: else -> true
       fi;
       d_step {
	 c_code {
	   x_slope =  (- (7.5e-2 * x)) ;
	   x_u = (x_slope * d) + x ;
	   x_u = (x_u <  20.0  && signbit(x_slope) &&  x_init >  20.0)  ?  20.0 : x_u ;
	   x_u = (x_u >  100.0  && !signbit(x_slope) &&  x_init <  100.0)  ?  100.0 : x_u ;
	   
	   waterTank_x = x ;
	 };
	 waterTank_pstate = waterTank_cstate;
	 waterTank_cstate = waterTank_t3;
	 waterTank_force_init_update = false;
	 
	 waterTank_ON  = false;
	 waterTank_OFF  = false;
	 waterTank_tick = true;
       };
       do
	 :: waterTank_tick -> true
	 :: else -> goto  T3
       od;
    ::  (c_expr { (x == 20.0) }
	 && 
	 waterTank_cstate == waterTank_t3)  -> 
       d_step {
	 c_code {  x_u = x ; };
	 c_code {  waterTank_x = x ; };
	 waterTank_pstate =  waterTank_cstate ;
	 waterTank_cstate =  waterTank_t4 ;
	 waterTank_force_init_update = false;
	 waterTank_tick = true;
       };
       do
	 :: waterTank_tick -> true
	 :: else -> goto  T4
       od;
    ::  (c_expr { 1 } && 
	 waterTank_ON && 
	 waterTank_cstate == waterTank_t3)  -> 
       d_step {
	 c_code {  x_u = x ; };
	 c_code {  waterTank_x = x ; };
	 waterTank_pstate =  waterTank_cstate ;
	 waterTank_cstate =  waterTank_t1 ;
	 waterTank_force_init_update = false;
	 waterTank_tick = true;
       };
       do
	 :: waterTank_tick -> true
	 :: else -> goto  T1
       od;

    :: else -> true
  fi;
T4:
  if
    :: (c_expr { (x == 20.0) }  &&  (! waterTank_ON)
	&& 
	(! waterTank_OFF)  &&  waterTank_cstate == waterTank_t4) ->
       if
	 :: waterTank_pstate != waterTank_cstate || waterTank_force_init_update ->
	    c_code { x_init = x ; };
	 :: else -> true
       fi;
       d_step {
	 c_code {
	   x_slope =  0 ;
	   x_u = (x_slope * d) + x ;
	   x_u = (x_u <  20.0  && signbit(x_slope) &&  x_init >  20.0)  ?  20.0 : x_u ;
	   x_u = (x_u >  20.0  && !signbit(x_slope) &&  x_init <  20.0)  ?  20.0 : x_u ;
	   
	   waterTank_x = x ;
	 };
	 waterTank_pstate = waterTank_cstate;
	 waterTank_cstate = waterTank_t4;
	 waterTank_force_init_update = false;
	 
	 waterTank_ON  = false;
	 waterTank_OFF  = false;
	 waterTank_tick = true;
       };
       do
	 :: waterTank_tick -> true
	 :: else -> goto  T4
       od;
    ::  (c_expr { 1 } && 
	 waterTank_ON && 
	 waterTank_cstate == waterTank_t4)  -> 
       d_step {
	 c_code {  x_u = x ; };
	 c_code {  waterTank_x = x ; };
	 waterTank_pstate =  waterTank_cstate ;
	 waterTank_cstate =  waterTank_t1 ;
	 waterTank_force_init_update = false;
	 waterTank_tick = true;
       };
       do
	 :: waterTank_tick -> true
	 :: else -> goto  T1
       od;

    :: else -> true
  fi;
}
c_decl {


};



c_decl {double  controller_x ;};
c_track "&controller_x" "sizeof(double)";
bool controller_ON = 1 ;
bool controller_OFF = 0 ;

mtype { controller_c1, controller_c2 };
proctype controller () {
C1:
  if
    :: (c_expr { 1 && (controller_x < 93.0) }  &&  true  &&  controller_cstate == controller_c1) ->
       if
	 :: controller_pstate != controller_cstate || controller_force_init_update ->
	    c_code {  };
	 :: else -> true
       fi;
       d_step {
	 c_code {
	   
	   
	   
	   
	 };
	 controller_pstate = controller_cstate;
	 controller_cstate = controller_c1;
	 controller_force_init_update = false;
	 controller_ON  = false;
	 controller_OFF  = false;
	 
	 controller_tick = true;
       };
       do
	 :: controller_tick -> true
	 :: else -> goto  C1
       od;
    ::  (c_expr { (controller_x >= 93.0) }
	 && 
	 controller_cstate == controller_c1)  -> 
       d_step {
	 controller_OFF = 1;
	 controller_pstate =  controller_cstate ;
	 controller_cstate =  controller_c2 ;
	 controller_force_init_update = false;
	 controller_tick = true;
       };
       do
	 :: controller_tick -> true
	 :: else -> goto  C2
       od;

    :: else -> true
  fi;
C2:
  if
    :: (c_expr { 1 && (controller_x >= 93.0) }  &&  true  &&  controller_cstate == controller_c2) ->
       if
	 :: controller_pstate != controller_cstate || controller_force_init_update ->
	    c_code {  };
	 :: else -> true
       fi;
       d_step {
	 c_code {
	   
	   
	   
	   
	 };
	 controller_pstate = controller_cstate;
	 controller_cstate = controller_c2;
	 controller_force_init_update = false;
	 controller_ON  = false;
	 controller_OFF  = false;
	 
	 controller_tick = true;
       };
       do
	 :: controller_tick -> true
	 :: else -> goto  C2
       od;
    ::  (c_expr { (controller_x < 93.0) }
	 && 
	 controller_cstate == controller_c2)  -> 
       d_step {
	 controller_ON = 1;
	 controller_pstate =  controller_cstate ;
	 controller_cstate =  controller_c1 ;
	 controller_force_init_update = false;
	 controller_tick = true;
       };
       do
	 :: controller_tick -> true
	 :: else -> goto  C1
       od;

    :: else -> true
  fi;
}
proctype main () {
  do
    ::  (waterTank_tick && 
	 controller_tick)  -> 
       d_step {
	 c_code { x = x_u; };
	 c_code { now.px = (int)waterTank_x; };
	 waterTank_ON = controller_ON;
	 waterTank_OFF = controller_OFF;
	 c_code { controller_x = waterTank_x; }
	 controller_tick = false;
	 waterTank_tick = false;
	 
       }
    :: else -> true
  od
}
init {
  d_step {
    c_code {
      
    };
    c_code { now.px = (int)waterTank_x; };
    waterTank_ON = controller_ON;
    waterTank_OFF = controller_OFF;
    c_code { controller_x = waterTank_x; }
    waterTank_cstate = waterTank_t4;
    controller_cstate = controller_c1;
  }
  atomic {
    run main();
    run controller();
    run waterTank();
    
  }
}