#define rand	pan_rand
#define pthread_equal(a,b)	((a)==(b))
#if defined(HAS_CODE) && defined(VERBOSE)
	#ifdef BFS_PAR
		bfs_printf("Pr: %d Tr: %d\n", II, t->forw);
	#else
		cpu_printf("Pr: %d Tr: %d\n", II, t->forw);
	#endif
#endif
	switch (t->forw) {
	default: Uerror("bad forward move");
	case 0:	/* if without executable clauses */
		continue;
	case 1: /* generic 'goto' or 'skip' */
		IfNotBlocked
		_m = 3; goto P999;
	case 2: /* generic 'else' */
		IfNotBlocked
		if (trpt->o_pm&1) continue;
		_m = 3; goto P999;

		 /* CLAIM p */
	case 3: // STATE 1 - _spin_nvr.tmp:3 - [(!(!((px>=100))))] (6:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[4][1] = 1;
		if (!( !( !((now.px>=100)))))
			continue;
		/* merge: assert(!(!(!((px>=100)))))(0, 2, 6) */
		reached[4][2] = 1;
		spin_assert( !( !( !((now.px>=100)))), " !( !( !((px>=100))))", II, tt, t);
		/* merge: .(goto)(0, 7, 6) */
		reached[4][7] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 4: // STATE 10 - _spin_nvr.tmp:8 - [-end-] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported10 = 0;
			if (verbose && !reported10)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported10 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported10 = 0;
			if (verbose && !reported10)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported10 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[4][10] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC :init: */
	case 5: // STATE 8 - WaterTank.pml:397 - [D_STEP397]
		/* default 262 */
		

		reached[3][8] = 1;
		reached[3][t->st] = 1;
		reached[3][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_309_0: /* 2 */
		/* c_code49 */
		{ 

     }
;
S_310_0: /* 2 */
		/* c_code50 */
		{  now.px = (int)waterTank_x;  }
;
S_311_0: /* 2 */
		now.waterTank_ON = ((int)now.controller_ON);
#ifdef VAR_RANGES
		logval("waterTank_ON", ((int)now.waterTank_ON));
#endif
		;
S_312_0: /* 2 */
		now.waterTank_OFF = ((int)now.controller_OFF);
#ifdef VAR_RANGES
		logval("waterTank_OFF", ((int)now.waterTank_OFF));
#endif
		;
S_313_0: /* 2 */
		/* c_code51 */
		{  controller_x = waterTank_x;  }
;
S_314_0: /* 2 */
		now.waterTank_cstate = 1;
#ifdef VAR_RANGES
		logval("waterTank_cstate", now.waterTank_cstate);
#endif
		;
S_315_0: /* 2 */
		now.controller_cstate = 6;
#ifdef VAR_RANGES
		logval("controller_cstate", now.controller_cstate);
#endif
		;
		goto S_320_0;
S_320_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 6: // STATE 9 - WaterTank.pml:409 - [(run main())] (0:0:0 - 1)
		IfNotBlocked
		reached[3][9] = 1;
		if (!(addproc(II, 1, 2)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 7: // STATE 10 - WaterTank.pml:410 - [(run controller())] (0:0:0 - 1)
		IfNotBlocked
		reached[3][10] = 1;
		if (!(addproc(II, 1, 1)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 8: // STATE 11 - WaterTank.pml:411 - [(run waterTank())] (0:0:0 - 1)
		IfNotBlocked
		reached[3][11] = 1;
		if (!(addproc(II, 1, 0)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 9: // STATE 13 - WaterTank.pml:414 - [-end-] (0:0:0 - 1)
		IfNotBlocked
		reached[3][13] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC main */
	case 10: // STATE 1 - WaterTank.pml:382 - [((waterTank_tick&&controller_tick))] (0:0:0 - 1)
		IfNotBlocked
		reached[2][1] = 1;
		if (!((((int)now.waterTank_tick)&&((int)now.controller_tick))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 11: // STATE 9 - WaterTank.pml:383 - [D_STEP383]
		/* default 262 */
		

		reached[2][9] = 1;
		reached[2][t->st] = 1;
		reached[2][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_295_0: /* 2 */
		/* c_code46 */
		{  x = x_u;  }
;
S_296_0: /* 2 */
		/* c_code47 */
		{  now.px = (int)waterTank_x;  }
;
S_297_0: /* 2 */
		now.waterTank_ON = ((int)now.controller_ON);
#ifdef VAR_RANGES
		logval("waterTank_ON", ((int)now.waterTank_ON));
#endif
		;
S_298_0: /* 2 */
		now.waterTank_OFF = ((int)now.controller_OFF);
#ifdef VAR_RANGES
		logval("waterTank_OFF", ((int)now.waterTank_OFF));
#endif
		;
S_299_0: /* 2 */
		/* c_code48 */
		{  controller_x = waterTank_x;  }
;
S_300_0: /* 2 */
		now.controller_tick = 0;
#ifdef VAR_RANGES
		logval("controller_tick", ((int)now.controller_tick));
#endif
		;
S_301_0: /* 2 */
		now.waterTank_tick = 0;
#ifdef VAR_RANGES
		logval("waterTank_tick", ((int)now.waterTank_tick));
#endif
		;
		goto S_306_0;
S_306_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 12: // STATE 15 - WaterTank.pml:395 - [-end-] (0:0:0 - 1)
		IfNotBlocked
		reached[2][15] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC controller */
	case 13: // STATE 1 - WaterTank.pml:288 - [(({c_code38}&&(controller_cstate==controller_c1)))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][1] = 1;
		if (!((( (controller_x >= 93.0) )&&(now.controller_cstate==6))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 14: // STATE 7 - WaterTank.pml:289 - [D_STEP289]
		IfNotBlocked

		reached[1][7] = 1;
		reached[1][t->st] = 1;
		reached[1][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_208_0: /* 2 */
		now.controller_OFF = 1;
#ifdef VAR_RANGES
		logval("controller_OFF", ((int)now.controller_OFF));
#endif
		;
S_209_0: /* 2 */
		now.controller_pstate = now.controller_cstate;
#ifdef VAR_RANGES
		logval("controller_pstate", now.controller_pstate);
#endif
		;
S_210_0: /* 2 */
		now.controller_cstate = 5;
#ifdef VAR_RANGES
		logval("controller_cstate", now.controller_cstate);
#endif
		;
S_211_0: /* 2 */
		now.controller_force_init_update = 0;
#ifdef VAR_RANGES
		logval("controller_force_init_update", ((int)now.controller_force_init_update));
#endif
		;
S_212_0: /* 2 */
		now.controller_tick = 1;
#ifdef VAR_RANGES
		logval("controller_tick", ((int)now.controller_tick));
#endif
		;
		goto S_219_0;
S_219_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 15: // STATE 8 - WaterTank.pml:297 - [(controller_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[1][8] = 1;
		if (!(((int)now.controller_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 16: // STATE 16 - WaterTank.pml:303 - [((({c_code39}&&1)&&(controller_cstate==controller_c1)))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][16] = 1;
		if (!(((( 1 && (controller_x < 93.0) )&&1)&&(now.controller_cstate==6))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 17: // STATE 17 - WaterTank.pml:305 - [(((controller_pstate!=controller_cstate)||controller_force_init_update))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][17] = 1;
		if (!(((now.controller_pstate!=now.controller_cstate)||((int)now.controller_force_init_update))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 18: // STATE 18 - WaterTank.pml:306 - [{c_code40}] (0:0:0 - 1)
		IfNotBlocked
		reached[1][18] = 1;
		/* c_code40 */
		{ 
		sv_save();  }

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
;
		_m = 3; goto P999; /* 0 */
	case 19: // STATE 30 - WaterTank.pml:309 - [D_STEP309]
		/* default 262 */
		

		reached[1][30] = 1;
		reached[1][t->st] = 1;
		reached[1][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_229_0: /* 2 */
		/* c_code41 */
		{ 




        }
;
S_230_0: /* 2 */
		now.controller_pstate = now.controller_cstate;
#ifdef VAR_RANGES
		logval("controller_pstate", now.controller_pstate);
#endif
		;
S_231_0: /* 2 */
		now.controller_cstate = 6;
#ifdef VAR_RANGES
		logval("controller_cstate", now.controller_cstate);
#endif
		;
S_232_0: /* 2 */
		now.controller_force_init_update = 0;
#ifdef VAR_RANGES
		logval("controller_force_init_update", ((int)now.controller_force_init_update));
#endif
		;
S_233_0: /* 2 */
		now.controller_ON = 0;
#ifdef VAR_RANGES
		logval("controller_ON", ((int)now.controller_ON));
#endif
		;
S_234_0: /* 2 */
		now.controller_OFF = 0;
#ifdef VAR_RANGES
		logval("controller_OFF", ((int)now.controller_OFF));
#endif
		;
S_235_0: /* 2 */
		now.controller_tick = 1;
#ifdef VAR_RANGES
		logval("controller_tick", ((int)now.controller_tick));
#endif
		;
		goto S_242_0;
S_242_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 20: // STATE 31 - WaterTank.pml:325 - [(controller_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[1][31] = 1;
		if (!(((int)now.controller_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 21: // STATE 44 - WaterTank.pml:335 - [(({c_code42}&&(controller_cstate==controller_c2)))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][44] = 1;
		if (!((( (controller_x < 93.0) )&&(now.controller_cstate==5))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 22: // STATE 50 - WaterTank.pml:336 - [D_STEP336]
		IfNotBlocked

		reached[1][50] = 1;
		reached[1][t->st] = 1;
		reached[1][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_251_0: /* 2 */
		now.controller_ON = 1;
#ifdef VAR_RANGES
		logval("controller_ON", ((int)now.controller_ON));
#endif
		;
S_252_0: /* 2 */
		now.controller_pstate = now.controller_cstate;
#ifdef VAR_RANGES
		logval("controller_pstate", now.controller_pstate);
#endif
		;
S_253_0: /* 2 */
		now.controller_cstate = 6;
#ifdef VAR_RANGES
		logval("controller_cstate", now.controller_cstate);
#endif
		;
S_254_0: /* 2 */
		now.controller_force_init_update = 0;
#ifdef VAR_RANGES
		logval("controller_force_init_update", ((int)now.controller_force_init_update));
#endif
		;
S_255_0: /* 2 */
		now.controller_tick = 1;
#ifdef VAR_RANGES
		logval("controller_tick", ((int)now.controller_tick));
#endif
		;
		goto S_262_0;
S_262_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 23: // STATE 51 - WaterTank.pml:344 - [(controller_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[1][51] = 1;
		if (!(((int)now.controller_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 24: // STATE 59 - WaterTank.pml:350 - [((({c_code43}&&1)&&(controller_cstate==controller_c2)))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][59] = 1;
		if (!(((( 1 && (controller_x >= 93.0) )&&1)&&(now.controller_cstate==5))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 25: // STATE 60 - WaterTank.pml:352 - [(((controller_pstate!=controller_cstate)||controller_force_init_update))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][60] = 1;
		if (!(((now.controller_pstate!=now.controller_cstate)||((int)now.controller_force_init_update))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 26: // STATE 61 - WaterTank.pml:353 - [{c_code44}] (0:0:0 - 1)
		IfNotBlocked
		reached[1][61] = 1;
		/* c_code44 */
		{ 
		sv_save();  }

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
;
		_m = 3; goto P999; /* 0 */
	case 27: // STATE 73 - WaterTank.pml:356 - [D_STEP356]
		/* default 262 */
		

		reached[1][73] = 1;
		reached[1][t->st] = 1;
		reached[1][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_272_0: /* 2 */
		/* c_code45 */
		{ 




        }
;
S_273_0: /* 2 */
		now.controller_pstate = now.controller_cstate;
#ifdef VAR_RANGES
		logval("controller_pstate", now.controller_pstate);
#endif
		;
S_274_0: /* 2 */
		now.controller_cstate = 5;
#ifdef VAR_RANGES
		logval("controller_cstate", now.controller_cstate);
#endif
		;
S_275_0: /* 2 */
		now.controller_force_init_update = 0;
#ifdef VAR_RANGES
		logval("controller_force_init_update", ((int)now.controller_force_init_update));
#endif
		;
S_276_0: /* 2 */
		now.controller_ON = 0;
#ifdef VAR_RANGES
		logval("controller_ON", ((int)now.controller_ON));
#endif
		;
S_277_0: /* 2 */
		now.controller_OFF = 0;
#ifdef VAR_RANGES
		logval("controller_OFF", ((int)now.controller_OFF));
#endif
		;
S_278_0: /* 2 */
		now.controller_tick = 1;
#ifdef VAR_RANGES
		logval("controller_tick", ((int)now.controller_tick));
#endif
		;
		goto S_285_0;
S_285_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 28: // STATE 74 - WaterTank.pml:372 - [(controller_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[1][74] = 1;
		if (!(((int)now.controller_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 29: // STATE 87 - WaterTank.pml:378 - [-end-] (0:0:0 - 5)
		IfNotBlocked
		reached[1][87] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC waterTank */
	case 30: // STATE 1 - WaterTank.pml:36 - [((({c_code6}&&waterTank_OFF)&&(waterTank_cstate==waterTank_t1)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][1] = 1;
		if (!(((( 1 )&&((int)now.waterTank_OFF))&&(now.waterTank_cstate==4))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 31: // STATE 8 - WaterTank.pml:37 - [D_STEP37]
		/* default 262 */
		

		reached[0][8] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_001_0: /* 2 */
		/* c_code7 */
		{  x_u = x ;  }
;
S_002_0: /* 2 */
		/* c_code8 */
		{  waterTank_x = x ;  }
;
S_003_0: /* 2 */
		now.waterTank_pstate = now.waterTank_cstate;
#ifdef VAR_RANGES
		logval("waterTank_pstate", now.waterTank_pstate);
#endif
		;
S_004_0: /* 2 */
		now.waterTank_cstate = 2;
#ifdef VAR_RANGES
		logval("waterTank_cstate", now.waterTank_cstate);
#endif
		;
S_005_0: /* 2 */
		now.waterTank_force_init_update = 0;
#ifdef VAR_RANGES
		logval("waterTank_force_init_update", ((int)now.waterTank_force_init_update));
#endif
		;
S_006_0: /* 2 */
		now.waterTank_tick = 1;
#ifdef VAR_RANGES
		logval("waterTank_tick", ((int)now.waterTank_tick));
#endif
		;
		goto S_013_0;
S_013_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 32: // STATE 9 - WaterTank.pml:46 - [(waterTank_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][9] = 1;
		if (!(((int)now.waterTank_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 33: // STATE 16 - WaterTank.pml:51 - [((({c_code9}&&waterTank_ON)&&(waterTank_cstate==waterTank_t1)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][16] = 1;
		if (!(((( (x == 100.0) )&&((int)now.waterTank_ON))&&(now.waterTank_cstate==4))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 34: // STATE 23 - WaterTank.pml:52 - [D_STEP52]
		/* default 262 */
		

		reached[0][23] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_016_0: /* 2 */
		/* c_code10 */
		{  x_u = x ;  }
;
S_017_0: /* 2 */
		/* c_code11 */
		{  waterTank_x = x ;  }
;
S_018_0: /* 2 */
		now.waterTank_pstate = now.waterTank_cstate;
#ifdef VAR_RANGES
		logval("waterTank_pstate", now.waterTank_pstate);
#endif
		;
S_019_0: /* 2 */
		now.waterTank_cstate = 3;
#ifdef VAR_RANGES
		logval("waterTank_cstate", now.waterTank_cstate);
#endif
		;
S_020_0: /* 2 */
		now.waterTank_force_init_update = 0;
#ifdef VAR_RANGES
		logval("waterTank_force_init_update", ((int)now.waterTank_force_init_update));
#endif
		;
S_021_0: /* 2 */
		now.waterTank_tick = 1;
#ifdef VAR_RANGES
		logval("waterTank_tick", ((int)now.waterTank_tick));
#endif
		;
		goto S_028_0;
S_028_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 35: // STATE 24 - WaterTank.pml:61 - [(waterTank_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][24] = 1;
		if (!(((int)now.waterTank_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 36: // STATE 32 - WaterTank.pml:69 - [(((({c_code12}&&!(waterTank_ON))&&!(waterTank_OFF))&&(waterTank_cstate==waterTank_t1)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][32] = 1;
		if (!((((( ((x >= 20.0) && (x <= 100.0)) )&& !(((int)now.waterTank_ON)))&& !(((int)now.waterTank_OFF)))&&(now.waterTank_cstate==4))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 37: // STATE 33 - WaterTank.pml:71 - [(((waterTank_pstate!=waterTank_cstate)||waterTank_force_init_update))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][33] = 1;
		if (!(((now.waterTank_pstate!=now.waterTank_cstate)||((int)now.waterTank_force_init_update))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 38: // STATE 34 - WaterTank.pml:72 - [{c_code13}] (0:0:0 - 1)
		IfNotBlocked
		reached[0][34] = 1;
		/* c_code13 */
		{ 
		sv_save(); x_init = x ;  }

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
;
		_m = 3; goto P999; /* 0 */
	case 39: // STATE 46 - WaterTank.pml:75 - [D_STEP75]
		/* default 262 */
		

		reached[0][46] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_038_0: /* 2 */
		/* c_code14 */
		{ 
  x_slope = (7.5e-2 * (150 + (- x))) ;
  x_u = (x_slope * d) + x ;
  x_u = (x_u < 20.0 && signbit(x_slope) && x_init > 20.0) ? 20.0 : x_u ;
  x_u = (x_u > 100.0 && !signbit(x_slope) && x_init < 100.0) ? 100.0 : x_u ;

  waterTank_x = x ;
        }
;
S_039_0: /* 2 */
		now.waterTank_pstate = now.waterTank_cstate;
#ifdef VAR_RANGES
		logval("waterTank_pstate", now.waterTank_pstate);
#endif
		;
S_040_0: /* 2 */
		now.waterTank_cstate = 4;
#ifdef VAR_RANGES
		logval("waterTank_cstate", now.waterTank_cstate);
#endif
		;
S_041_0: /* 2 */
		now.waterTank_force_init_update = 0;
#ifdef VAR_RANGES
		logval("waterTank_force_init_update", ((int)now.waterTank_force_init_update));
#endif
		;
S_042_0: /* 2 */
		now.waterTank_ON = 0;
#ifdef VAR_RANGES
		logval("waterTank_ON", ((int)now.waterTank_ON));
#endif
		;
S_043_0: /* 2 */
		now.waterTank_OFF = 0;
#ifdef VAR_RANGES
		logval("waterTank_OFF", ((int)now.waterTank_OFF));
#endif
		;
S_044_0: /* 2 */
		now.waterTank_tick = 1;
#ifdef VAR_RANGES
		logval("waterTank_tick", ((int)now.waterTank_tick));
#endif
		;
		goto S_051_0;
S_051_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 40: // STATE 47 - WaterTank.pml:93 - [(waterTank_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][47] = 1;
		if (!(((int)now.waterTank_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 41: // STATE 60 - WaterTank.pml:103 - [((({c_code15}&&waterTank_OFF)&&(waterTank_cstate==waterTank_t2)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][60] = 1;
		if (!(((( 1 )&&((int)now.waterTank_OFF))&&(now.waterTank_cstate==3))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 42: // STATE 67 - WaterTank.pml:104 - [D_STEP104]
		/* default 262 */
		

		reached[0][67] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_060_0: /* 2 */
		/* c_code16 */
		{  x_u = x ;  }
;
S_061_0: /* 2 */
		/* c_code17 */
		{  waterTank_x = x ;  }
;
S_062_0: /* 2 */
		now.waterTank_pstate = now.waterTank_cstate;
#ifdef VAR_RANGES
		logval("waterTank_pstate", now.waterTank_pstate);
#endif
		;
S_063_0: /* 2 */
		now.waterTank_cstate = 2;
#ifdef VAR_RANGES
		logval("waterTank_cstate", now.waterTank_cstate);
#endif
		;
S_064_0: /* 2 */
		now.waterTank_force_init_update = 0;
#ifdef VAR_RANGES
		logval("waterTank_force_init_update", ((int)now.waterTank_force_init_update));
#endif
		;
S_065_0: /* 2 */
		now.waterTank_tick = 1;
#ifdef VAR_RANGES
		logval("waterTank_tick", ((int)now.waterTank_tick));
#endif
		;
		goto S_072_0;
S_072_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 43: // STATE 68 - WaterTank.pml:113 - [(waterTank_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][68] = 1;
		if (!(((int)now.waterTank_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 44: // STATE 76 - WaterTank.pml:121 - [(((({c_code18}&&!(waterTank_ON))&&!(waterTank_OFF))&&(waterTank_cstate==waterTank_t2)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][76] = 1;
		if (!((((( (x == 100.0) )&& !(((int)now.waterTank_ON)))&& !(((int)now.waterTank_OFF)))&&(now.waterTank_cstate==3))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 45: // STATE 77 - WaterTank.pml:123 - [(((waterTank_pstate!=waterTank_cstate)||waterTank_force_init_update))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][77] = 1;
		if (!(((now.waterTank_pstate!=now.waterTank_cstate)||((int)now.waterTank_force_init_update))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 46: // STATE 78 - WaterTank.pml:124 - [{c_code19}] (0:0:0 - 1)
		IfNotBlocked
		reached[0][78] = 1;
		/* c_code19 */
		{ 
		sv_save(); x_init = x ;  }

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
;
		_m = 3; goto P999; /* 0 */
	case 47: // STATE 90 - WaterTank.pml:127 - [D_STEP127]
		/* default 262 */
		

		reached[0][90] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_082_0: /* 2 */
		/* c_code20 */
		{ 
  x_slope = 0 ;
  x_u = (x_slope * d) + x ;
  x_u = (x_u < 100.0 && signbit(x_slope) && x_init > 100.0) ? 100.0 : x_u ;
  x_u = (x_u > 100.0 && !signbit(x_slope) && x_init < 100.0) ? 100.0 : x_u ;

  waterTank_x = x ;
        }
;
S_083_0: /* 2 */
		now.waterTank_pstate = now.waterTank_cstate;
#ifdef VAR_RANGES
		logval("waterTank_pstate", now.waterTank_pstate);
#endif
		;
S_084_0: /* 2 */
		now.waterTank_cstate = 3;
#ifdef VAR_RANGES
		logval("waterTank_cstate", now.waterTank_cstate);
#endif
		;
S_085_0: /* 2 */
		now.waterTank_force_init_update = 0;
#ifdef VAR_RANGES
		logval("waterTank_force_init_update", ((int)now.waterTank_force_init_update));
#endif
		;
S_086_0: /* 2 */
		now.waterTank_ON = 0;
#ifdef VAR_RANGES
		logval("waterTank_ON", ((int)now.waterTank_ON));
#endif
		;
S_087_0: /* 2 */
		now.waterTank_OFF = 0;
#ifdef VAR_RANGES
		logval("waterTank_OFF", ((int)now.waterTank_OFF));
#endif
		;
S_088_0: /* 2 */
		now.waterTank_tick = 1;
#ifdef VAR_RANGES
		logval("waterTank_tick", ((int)now.waterTank_tick));
#endif
		;
		goto S_095_0;
S_095_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 48: // STATE 91 - WaterTank.pml:145 - [(waterTank_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][91] = 1;
		if (!(((int)now.waterTank_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 49: // STATE 104 - WaterTank.pml:154 - [(({c_code21}&&(waterTank_cstate==waterTank_t3)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][104] = 1;
		if (!((( (x == 20.0) )&&(now.waterTank_cstate==2))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 50: // STATE 111 - WaterTank.pml:155 - [D_STEP155]
		/* default 262 */
		

		reached[0][111] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_104_0: /* 2 */
		/* c_code22 */
		{  x_u = x ;  }
;
S_105_0: /* 2 */
		/* c_code23 */
		{  waterTank_x = x ;  }
;
S_106_0: /* 2 */
		now.waterTank_pstate = now.waterTank_cstate;
#ifdef VAR_RANGES
		logval("waterTank_pstate", now.waterTank_pstate);
#endif
		;
S_107_0: /* 2 */
		now.waterTank_cstate = 1;
#ifdef VAR_RANGES
		logval("waterTank_cstate", now.waterTank_cstate);
#endif
		;
S_108_0: /* 2 */
		now.waterTank_force_init_update = 0;
#ifdef VAR_RANGES
		logval("waterTank_force_init_update", ((int)now.waterTank_force_init_update));
#endif
		;
S_109_0: /* 2 */
		now.waterTank_tick = 1;
#ifdef VAR_RANGES
		logval("waterTank_tick", ((int)now.waterTank_tick));
#endif
		;
		goto S_116_0;
S_116_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 51: // STATE 112 - WaterTank.pml:164 - [(waterTank_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][112] = 1;
		if (!(((int)now.waterTank_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 52: // STATE 119 - WaterTank.pml:169 - [((({c_code24}&&waterTank_ON)&&(waterTank_cstate==waterTank_t3)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][119] = 1;
		if (!(((( 1 )&&((int)now.waterTank_ON))&&(now.waterTank_cstate==2))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 53: // STATE 126 - WaterTank.pml:170 - [D_STEP170]
		/* default 262 */
		

		reached[0][126] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_119_0: /* 2 */
		/* c_code25 */
		{  x_u = x ;  }
;
S_120_0: /* 2 */
		/* c_code26 */
		{  waterTank_x = x ;  }
;
S_121_0: /* 2 */
		now.waterTank_pstate = now.waterTank_cstate;
#ifdef VAR_RANGES
		logval("waterTank_pstate", now.waterTank_pstate);
#endif
		;
S_122_0: /* 2 */
		now.waterTank_cstate = 4;
#ifdef VAR_RANGES
		logval("waterTank_cstate", now.waterTank_cstate);
#endif
		;
S_123_0: /* 2 */
		now.waterTank_force_init_update = 0;
#ifdef VAR_RANGES
		logval("waterTank_force_init_update", ((int)now.waterTank_force_init_update));
#endif
		;
S_124_0: /* 2 */
		now.waterTank_tick = 1;
#ifdef VAR_RANGES
		logval("waterTank_tick", ((int)now.waterTank_tick));
#endif
		;
		goto S_131_0;
S_131_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 54: // STATE 127 - WaterTank.pml:179 - [(waterTank_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][127] = 1;
		if (!(((int)now.waterTank_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 55: // STATE 135 - WaterTank.pml:187 - [(((({c_code27}&&!(waterTank_ON))&&!(waterTank_OFF))&&(waterTank_cstate==waterTank_t3)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][135] = 1;
		if (!((((( ((x >= 20.0) && (x <= 100.0)) )&& !(((int)now.waterTank_ON)))&& !(((int)now.waterTank_OFF)))&&(now.waterTank_cstate==2))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 56: // STATE 136 - WaterTank.pml:189 - [(((waterTank_pstate!=waterTank_cstate)||waterTank_force_init_update))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][136] = 1;
		if (!(((now.waterTank_pstate!=now.waterTank_cstate)||((int)now.waterTank_force_init_update))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 57: // STATE 137 - WaterTank.pml:190 - [{c_code28}] (0:0:0 - 1)
		IfNotBlocked
		reached[0][137] = 1;
		/* c_code28 */
		{ 
		sv_save(); x_init = x ;  }

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
;
		_m = 3; goto P999; /* 0 */
	case 58: // STATE 149 - WaterTank.pml:193 - [D_STEP193]
		/* default 262 */
		

		reached[0][149] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_141_0: /* 2 */
		/* c_code29 */
		{ 
  x_slope = (- (7.5e-2 * x)) ;
  x_u = (x_slope * d) + x ;
  x_u = (x_u < 20.0 && signbit(x_slope) && x_init > 20.0) ? 20.0 : x_u ;
  x_u = (x_u > 100.0 && !signbit(x_slope) && x_init < 100.0) ? 100.0 : x_u ;

  waterTank_x = x ;
        }
;
S_142_0: /* 2 */
		now.waterTank_pstate = now.waterTank_cstate;
#ifdef VAR_RANGES
		logval("waterTank_pstate", now.waterTank_pstate);
#endif
		;
S_143_0: /* 2 */
		now.waterTank_cstate = 2;
#ifdef VAR_RANGES
		logval("waterTank_cstate", now.waterTank_cstate);
#endif
		;
S_144_0: /* 2 */
		now.waterTank_force_init_update = 0;
#ifdef VAR_RANGES
		logval("waterTank_force_init_update", ((int)now.waterTank_force_init_update));
#endif
		;
S_145_0: /* 2 */
		now.waterTank_ON = 0;
#ifdef VAR_RANGES
		logval("waterTank_ON", ((int)now.waterTank_ON));
#endif
		;
S_146_0: /* 2 */
		now.waterTank_OFF = 0;
#ifdef VAR_RANGES
		logval("waterTank_OFF", ((int)now.waterTank_OFF));
#endif
		;
S_147_0: /* 2 */
		now.waterTank_tick = 1;
#ifdef VAR_RANGES
		logval("waterTank_tick", ((int)now.waterTank_tick));
#endif
		;
		goto S_154_0;
S_154_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 59: // STATE 150 - WaterTank.pml:211 - [(waterTank_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][150] = 1;
		if (!(((int)now.waterTank_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 60: // STATE 163 - WaterTank.pml:221 - [((({c_code30}&&waterTank_ON)&&(waterTank_cstate==waterTank_t4)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][163] = 1;
		if (!(((( 1 )&&((int)now.waterTank_ON))&&(now.waterTank_cstate==1))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 61: // STATE 170 - WaterTank.pml:222 - [D_STEP222]
		/* default 262 */
		

		reached[0][170] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_163_0: /* 2 */
		/* c_code31 */
		{  x_u = x ;  }
;
S_164_0: /* 2 */
		/* c_code32 */
		{  waterTank_x = x ;  }
;
S_165_0: /* 2 */
		now.waterTank_pstate = now.waterTank_cstate;
#ifdef VAR_RANGES
		logval("waterTank_pstate", now.waterTank_pstate);
#endif
		;
S_166_0: /* 2 */
		now.waterTank_cstate = 4;
#ifdef VAR_RANGES
		logval("waterTank_cstate", now.waterTank_cstate);
#endif
		;
S_167_0: /* 2 */
		now.waterTank_force_init_update = 0;
#ifdef VAR_RANGES
		logval("waterTank_force_init_update", ((int)now.waterTank_force_init_update));
#endif
		;
S_168_0: /* 2 */
		now.waterTank_tick = 1;
#ifdef VAR_RANGES
		logval("waterTank_tick", ((int)now.waterTank_tick));
#endif
		;
		goto S_175_0;
S_175_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 62: // STATE 171 - WaterTank.pml:231 - [(waterTank_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][171] = 1;
		if (!(((int)now.waterTank_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 63: // STATE 179 - WaterTank.pml:239 - [(((({c_code33}&&!(waterTank_ON))&&!(waterTank_OFF))&&(waterTank_cstate==waterTank_t4)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][179] = 1;
		if (!((((( (x == 20.0) )&& !(((int)now.waterTank_ON)))&& !(((int)now.waterTank_OFF)))&&(now.waterTank_cstate==1))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 64: // STATE 180 - WaterTank.pml:241 - [(((waterTank_pstate!=waterTank_cstate)||waterTank_force_init_update))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][180] = 1;
		if (!(((now.waterTank_pstate!=now.waterTank_cstate)||((int)now.waterTank_force_init_update))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 65: // STATE 181 - WaterTank.pml:242 - [{c_code34}] (0:0:0 - 1)
		IfNotBlocked
		reached[0][181] = 1;
		/* c_code34 */
		{ 
		sv_save(); x_init = x ;  }

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
;
		_m = 3; goto P999; /* 0 */
	case 66: // STATE 193 - WaterTank.pml:245 - [D_STEP245]
		/* default 262 */
		

		reached[0][193] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_185_0: /* 2 */
		/* c_code35 */
		{ 
  x_slope = 0 ;
  x_u = (x_slope * d) + x ;
  x_u = (x_u < 20.0 && signbit(x_slope) && x_init > 20.0) ? 20.0 : x_u ;
  x_u = (x_u > 20.0 && !signbit(x_slope) && x_init < 20.0) ? 20.0 : x_u ;

  waterTank_x = x ;
        }
;
S_186_0: /* 2 */
		now.waterTank_pstate = now.waterTank_cstate;
#ifdef VAR_RANGES
		logval("waterTank_pstate", now.waterTank_pstate);
#endif
		;
S_187_0: /* 2 */
		now.waterTank_cstate = 1;
#ifdef VAR_RANGES
		logval("waterTank_cstate", now.waterTank_cstate);
#endif
		;
S_188_0: /* 2 */
		now.waterTank_force_init_update = 0;
#ifdef VAR_RANGES
		logval("waterTank_force_init_update", ((int)now.waterTank_force_init_update));
#endif
		;
S_189_0: /* 2 */
		now.waterTank_ON = 0;
#ifdef VAR_RANGES
		logval("waterTank_ON", ((int)now.waterTank_ON));
#endif
		;
S_190_0: /* 2 */
		now.waterTank_OFF = 0;
#ifdef VAR_RANGES
		logval("waterTank_OFF", ((int)now.waterTank_OFF));
#endif
		;
S_191_0: /* 2 */
		now.waterTank_tick = 1;
#ifdef VAR_RANGES
		logval("waterTank_tick", ((int)now.waterTank_tick));
#endif
		;
		goto S_198_0;
S_198_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 67: // STATE 194 - WaterTank.pml:263 - [(waterTank_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][194] = 1;
		if (!(((int)now.waterTank_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 68: // STATE 207 - WaterTank.pml:269 - [-end-] (0:0:0 - 5)
		IfNotBlocked
		reached[0][207] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */
	case  _T5:	/* np_ */
		if (!((!(trpt->o_pm&4) && !(trpt->tau&128))))
			continue;
		/* else fall through */
	case  _T2:	/* true */
		_m = 3; goto P999;
#undef rand
	}

