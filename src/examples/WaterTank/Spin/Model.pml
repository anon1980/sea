/* The variable that we have in the promela model */
int myx = 20;

/* The bad state variable */
bool bad = false;

/* The properties we want to test */
/* ltl p1 {[] (20 <= myx && myx < 100)} */
/* ltl p2 {[] (!bad)} */

/* include files */
c_decl {
  \#include <math.h>
}


/* The ticks needed to synchronize this thing */
bool lt1 = false;
bool lt2 = false;

/* The state variables for all processes */
mtype cstate[1];
mtype pstate[1];

/* force_init_update */
bool force_init_update[2];

/* These are declarations global to everything */
c_decl {double d = 0.1;}

/* This is the watertank FSM */
bool waterTank_ON, waterTank_OFF; 
c_decl {double x = 20.0, x_u, waterTank_x = 20.0, x_init, w_slope;};
c_track "&waterTank_x" "sizeof(double)";
c_track "&x" "sizeof(double)";
c_track "&x_u" "sizeof(double)";
c_track "&x_init" "sizeof(double)";

/* The states of the waterTank */
mtype = {T_ONE, T_TWO, T_THREE, T_FOUR, UNDEF};

proctype WaterTank () {
T1:
  if
    :: (!waterTank_ON && !waterTank_OFF && c_expr {x  >= 20.0 && x <= 100.0}
	&& cstate[0] == T_ONE) ->
       d_step {
	 if
	   ::((pstate[0] != cstate[0]) || (force_init_update[0])) -> c_code {x_init = x;}
	   :: else -> true
	 fi;
	 c_code {
	   w_slope =  (7.5e-2 * (150 + (- x))) ;
	   x_u = (w_slope * d) + x ;
	   /* Possible Saturation */
	   x_u = (x_u <  20.0  && signbit(w_slope) &&  x_init >  20.0)  ?  20.0 : x_u ;
	   x_u = (x_u >  100.0  && !signbit(w_slope) &&  x_init <  100.0)  ?  100.0 : x_u ;
	   waterTank_x = x;
	   x = x_u;
	   printf ("waterTank_x: %f\n", waterTank_x);
	 };
	 waterTank_ON = false;
	 waterTank_OFF = false;
	 force_init_update[0] = false;
	 pstate[0] = cstate[0];
	 cstate[0] = T_ONE;
	 lt1 = true;
       }
       do
	 :: lt1 -> true
	 :: else -> goto T1
       od;
       
    :: waterTank_OFF && cstate[0] == T_ONE ->
       d_step {
	 c_code {
	   x_u = x ;
	   waterTank_x = x ;
	   x = x_u;
	   printf("x_u: %f, x:%f\n",x_u, x);
	 };
	 pstate[0] = cstate[0];
	 cstate[0] =  T_THREE ;
	 force_init_update[0] = false;
	 lt1 = true
       };
       do
	 :: lt1 -> true
	 :: else -> atomic{
	   c_code {printf("x: %f\n",x);};
	   printf("cs: %d, woff:%d, won:%d\n",cstate[0], waterTank_OFF, waterTank_ON);
	   goto T3
	 }
       od;

    :: (waterTank_ON && c_expr {x == 100.0} && cstate[0] == T_ONE) ->
       d_step {
	 c_code {
	   x_u = x ;
	   waterTank_x = x ;
	   x = x_u;
	 };
	 force_init_update[0] = false;
	 cstate[0] = T_TWO;
	 lt1 = true
       };
       do
	 :: lt1 -> true
	 :: else -> atomic {lt1 = false; goto T2}
       od;

    /* :: cstate[0] == T_ONE -> bad = true */
    :: else -> true
  fi;

T2:
  if
    :: (!waterTank_ON && !waterTank_OFF && (c_expr{x == 100.0})
	&& cstate[0] == T_TWO) ->
       d_step {
	 if
	   ::((pstate[0] != cstate[0]) || (force_init_update[0])) -> c_code {x_init = x;}
	   :: else -> true
	 fi;
	 c_code {
	   w_slope =  0 ;
	   x_u = (w_slope * d) + x ;
	   /* Possible Saturation */
	   x_u = (x_u <  100.0  && signbit(w_slope) &&  x_init >  100.0)  ?  100.0 : x_u ;
	   x_u = (x_u >  100.0  && !signbit(w_slope) &&  x_init <  100.0)  ?  100.0 : x_u ;
	   waterTank_x = x;
	   x = x_u;
	 };
	 waterTank_ON  = false;
	 waterTank_OFF  = false;
	 pstate[0] = cstate[0];
	 cstate[0] =  T_TWO ;
	 force_init_update[0] = false;
	 lt1 = true;
       };
       do
	 :: lt1 -> true
	 :: else -> atomic {lt1 = false; goto T2}
       od;

    :: (waterTank_OFF && cstate[0] == T_TWO) ->
       d_step {
	 c_code {
	   x_u = x;
	   waterTank_x = x ;
	   x = x_u;
	 };
	 pstate[0] = cstate[0];
	 cstate[0] = T_THREE;
	 force_init_update[0] = false;
	 lt1 = true
       }
       do
	 :: lt1 -> true
	 :: else -> atomic {lt1 = false; goto T3}
       od;

    /* :: cstate[0] == T_TWO -> bad = true */
    :: else -> true

  fi;

T3:
  if
    :: (!waterTank_OFF && !waterTank_ON && c_expr{x >= 20.0 && x <= 100.0}
	&& cstate[0] == T_THREE) ->
       d_step {
	 if
	   ::((pstate[0] != cstate[0]) || (force_init_update[0])) -> c_code {x_init = x;}
	   :: else -> true
	 fi;
	 c_code {
	   w_slope =  (- (7.5e-2 * x)) ;
	   x_u = (w_slope * d) + x ;
	   /* Possible Saturation */
	   x_u = (x_u <  20.0  && signbit(w_slope) &&  x_init >  20.0)  ?  20.0 : x_u ;
	   x_u = (x_u >  100.0  && !signbit(w_slope) &&  x_init <  100.0)  ?  100.0 : x_u ;
	   waterTank_x = x;
	   x = x_u;
	 };
	 waterTank_ON  = false;
	 waterTank_OFF  = false;
	 pstate[0] = cstate[0];
	 cstate[0] =  T_THREE ;
	 force_init_update[0] = false;
	 lt1 = true
       };
       do
	 :: lt1 -> true
	 :: else -> atomic {lt1 = false; goto T3}
       od;

    :: waterTank_OFF && cstate[0] == T_THREE-> 
       d_step {
	 c_code {
	   x_u = x ;
	   waterTank_x = x ;
	   x = x_u;
	 };
	 pstate[0] = cstate[0];
	 cstate[0] =  T_FOUR ;
	 force_init_update[0] = false;
	 lt1 = true
       };
       do
	 :: lt1 -> true
	 :: else -> atomic {lt1 = false; goto T4}
       od;

    :: waterTank_ON && cstate[0] == T_THREE ->
       d_step {
	 c_code {
	   x_u = x ;
	   waterTank_x = x ;
	   x = x_u;
	 };
	 pstate[0] = cstate[0];
	 cstate[0] = T_ONE;
	 force_init_update[0] = false;
	 lt1 = true
       };
       do
	 :: lt1-> true
	 :: else -> atomic {lt1 = false; goto T1}
       od;

    /* :: cstate[0] == T_THREE -> bad = true */
    :: else -> true
  fi;

T4:
  if
    :: (!waterTank_ON && !waterTank_OFF && c_expr {x == 20.0}
	&& cstate[0] == T_FOUR) ->
       d_step {
	 printf("Here\n");
	 if
	   ::((pstate[0] != cstate[0]) || (force_init_update[0])) -> c_code {x_init = x;}
	   :: else -> true
	 fi;
	 c_code {
	   w_slope =  0 ;
	   x_u = (w_slope * d) + x ;
	   /* Possible Saturation */
	   x_u = (x_u <  20.0  && signbit(w_slope) &&  x_init >  20.0)  ?  20.0 : x_u ;
	   x_u = (x_u >  20.0  && !signbit(w_slope) &&  x_init <  20.0)  ?  20.0 : x_u ;
	   waterTank_x = x ;
	   x = x_u;
	 };
	 waterTank_ON = false;
	 waterTank_OFF = false;
	 pstate[0] = cstate[0];
	 cstate[0] = T_FOUR;
	 force_init_update[0] = false;
	 lt1 = true
       };
       do
	 :: lt1 -> true
	 :: else -> atomic {lt1 = false; goto T4}
       od;

    :: waterTank_ON && cstate[0] == T_FOUR ->
       d_step {
	 c_code {
	   x_u = x ;
	   waterTank_x = x ;
	   x = x_u;
	 };
	 pstate[0] = cstate[0];
	 cstate[0] = T_ONE;
	 force_init_update[0] = false;
	 lt1 = true
       };
       do
	 :: lt1 -> true
	 :: else -> goto T1
       od;

    /* :: cstate[0] == T_FOUR -> bad = true */
    :: else -> true
  fi
}

/* Controller signals */
bool controller_ON = true, controller_OFF = false;
/* ltl p4 {[](controller_ON -> <> controller_OFF)} */
/* ltl p5 {[] controller_ON} */
ltl p6 {[] (<>controller_ON)}
/* ltl p3 {[](controller_OFF -> <> controller_ON)} */

/* Controller variables */
c_decl {double controller_x;}
c_track "&controller_x" "sizeof(double)";

/* This is the controller FSM */
proctype Controller () {
C1:
  if
    :: c_expr {controller_x < 93} -> 
       d_step {
	 controller_ON  = false;
	 controller_OFF  = false;
	 force_init_update[1] = false;
	 lt2 = true;
       };
       do
	 :: lt2 -> true 	/* Loop doing nothing */
	 :: else -> goto C1
       od;

    :: c_expr {controller_x >= 93} ->
       d_step {
	 controller_OFF = true;
	 controller_ON = false;
	 force_init_update[1] = false;
	 printf("Controller is now OFF!");
	 lt2 = true;
       };
       do
	 :: lt2 -> true
	 :: else -> goto C2
       od;

    :: else -> bad = true
  fi;

C2:
  if
    :: c_expr {controller_x <= 95.0 && controller_x >= 93.0} ->
       d_step {
	 controller_OFF = false;
	 controller_ON = false;
	 force_init_update[1] = false;
	 c_code {printf("Controller is OFF and waiting: %f\n", controller_x);};
	 lt2 = true
       };
       do
	 :: lt2 -> true
	 :: else -> goto C2
       od;

    :: c_expr{controller_x < 93.0} ->
       d_step {
	 controller_ON = true;
	 controller_OFF = false;
	 force_init_update[1] = false;
	 lt2 = true;
	 printf("Controller is now ON!\n")
       };
       do
	 :: lt2 -> true
	 :: else -> goto C1
       od;

    :: else -> bad = true
  fi
}

/* This is the Main FSM */
proctype Main () {
  do
    :: lt1 && lt2 ->
       d_step{
	 waterTank_ON = controller_ON;
	 waterTank_OFF = controller_OFF;
	 c_code{
	   controller_x = waterTank_x;
	   now.myx = (int)waterTank_x;
	   printf("cx: %f, wx: %f\n", controller_x, waterTank_x);
	 };
	 printf("myx: %d\n", myx);
	 lt1 = false;
	 lt2 = false;
       }

    :: else -> true
  od
}

init {
  d_step {
    c_code {
      /* Just once initially! */
      now.myx = (int) x;
      controller_x = waterTank_x;
    };
    cstate[0] = T_FOUR;
    waterTank_ON = controller_ON;
    waterTank_OFF = controller_OFF;
  };
  atomic {
    run Main ();
    run WaterTank();
    run Controller()
  }
}