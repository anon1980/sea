#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<string.h>
#include<assert.h>

/* The files are in csv format */
#define OFILE "wout_from_spin.csv"

extern double waterTank_x, controller_x;

/* The step size */
/* double d = 0.5; */
/* double d = 0.7; */
double d = 1.0;

/* The tick counter */
/* It is static to hide it inside this file */
static size_t tick = 0;

/* The output file pointer */
FILE *fo = NULL;

/* Write output x to file */
void writeOutput(){
  static unsigned char count = 0;
  if (0 == count){
    fo = fopen(OFILE, "w");
    if (fo == NULL){
      perror(OFILE);
      exit(1);
    }
    ++count;
    fprintf(fo, "title_x = %s\ntitle = %s\ntitle_y = %s\n",
	    "Ticks","Evolution of water temperature", "Water tank temperature");
  }
  fprintf(fo, "%zu,%f,%f\n", tick++, waterTank_x, controller_x);
}

