module WaterTank where
import Language
import qualified Hshac as H
import qualified Promela as P
import Prelude hiding ((==),(/=), LT, GT, (<=), (>=), (<), (>), (&&), not, min, max, (^))
import Text.PrettyPrint.ANSI.Leijen
import GHC.IO.Handle
import System.IO


-- The watertank example
dvar :: Diff
dvar = DiffT (S "x")

-- The ODE
ode1 :: Ode
ode1 = Ode dvar (0.075 * (150 - A(S "x"))) 20

ode2 :: Ode
ode2 = Ode dvar 0 100.0

ode3 :: Ode
ode3 = Ode dvar (-0.075 * A(S "x")) 100

ode4 :: Ode
ode4 = Ode dvar 0 20

-- Locations in the hybrid automata
-- XXX: Some assignment statements to OutputVar
assign :: Update (External Output V)
assign = OutputVar (S "x") := A (S "x")

t1 :: Loc
t1 = Loc (L "t1") [ode1] 
                  [InvariantLoc dvar ((T(S "x") >= TR 20) && (T(S "x") <= TR 100))]
                  ([], [assign])

t2 :: Loc
t2 = Loc (L "t2") [ode2] 
                  [InvariantLoc dvar (T(S "x") == TR 100)]
                  ([], [assign])

t3 :: Loc
t3 = Loc (L "t3") [ode3] 
                [InvariantLoc dvar ((T(S "x") >= TR 20) && (T(S "x") <= TR 100))]
                ([],  [assign])

t4 :: Loc
t4 = Loc (L "t4") [ode4] 
                  [InvariantLoc dvar (T(S "x") == TR 20)]
                  ([],  [assign])

-- Edges
e1 :: Edge
e1 = Edge t1 t2 [InvariantEdge (T(S "x") == TR 100)] ([(S "x") := (A (S "x"))],
                                                       [(OutputVar (S "x")) := (A (S "x"))], []) 
                                                       [Event (InputEvent "ON")]

e2 :: Edge
e2 = Edge t2 t3 [InvariantEdge TTrue]  ([(S "x") := (A (S "x"))],
                                        [(OutputVar (S "x")) := (A (S "x"))], [])
                                        [Event (InputEvent "OFF")]

e3 :: Edge
e3 = Edge t1 t3 [InvariantEdge TTrue] ([(S "x") := (A (S "x"))],
                                       [(OutputVar (S "x")) := (A (S "x"))], []) 
                                       [Event (InputEvent "OFF")]

e4 :: Edge
e4 = Edge t3 t1 [InvariantEdge TTrue]  ([(S "x") := (A (S "x"))],
                                        [(OutputVar (S "x")) := (A (S "x"))], [])
                                        [Event (InputEvent "ON")]

e5 :: Edge
e5 = Edge t3 t4 [InvariantEdge (T(S "x") == TR 20)] ([(S "x") := (A (S "x"))],
                                                     [(OutputVar (S "x")) := (A (S "x"))], [])
                                                     []

e6 :: Edge
e6 = Edge t4 t1 [InvariantEdge TTrue]  ([(S "x") := (A (S "x"))],
                                        [(OutputVar (S "x")) := (A (S "x"))], [])
                                        [Event (InputEvent "ON")]

-- The watertank Ha
waterTank :: Ha
waterTank = Ha (L "waterTank") [t1,t2,t3,t4] t4 
            [e1,e2,e3,e4,e5,e6] [SymbolDecl (S "x") 20.0] [] [(OutputVarDecl (S "x") 20.0)]
            [(InputEventDecl "ON"), (InputEventDecl "OFF")] []


-- XXX: The controller from Raskin
c1 :: Loc
c1 = Loc (L "c1") [] [InvariantLoc (InputVar (S "x")) (T (InputVar (S "x")) < TR 93)] ([], [])

c2 :: Loc
c2 = Loc  (L "c2") [] [InvariantLoc (InputVar (S "x")) (T (InputVar (S "x")) >= TR 93)] ([], [])

ce1 :: Edge
ce1 = Edge c1 c2 [InvariantEdge (T (InputVar (S "x")) >= TR 93)] ([], [], [(:!)(OutputEvent "OFF")]) 
                                                                 []

ce2 :: Edge
ce2 = Edge c2 c1 [InvariantEdge (T (InputVar (S "x")) < TR 93)] ([], [], [(:!) (OutputEvent "ON")]) 
                                                                []

-- Compile into 'C' file
-- waterTankC :: Doc
-- waterTankC = H.compileReaction waterTank 

controller :: Ha
controller = Ha (L "controller") [c1, c2] c1 [ce1, ce2] [] 
             [(InputVarDecl (S "x"))] [] [] 
             [(OutputEventDecl "ON" TTrue), (OutputEventDecl "OFF" TFalse)]

-- XXX: Now bind the Input to Output
b1 :: Bind
b1 = (InputEvent "ON") :< (OutputEvent "ON")

b2 :: Bind
b2 = (InputEvent "OFF") :< (OutputEvent "OFF")

x1 :: Bind
x1 = InputVar (S "x") :< (OutputVar (S "x"))

-- XXX: Now bind the HAs
bs :: BindHa
bs = [(waterTank, (b1 :<< (LastBind b2)), controller),
      (controller, (LastBind x1) , waterTank)] 

-- XXX: Now make the HA series
hase :: HaSeries
hase = waterTank ::: (LastHa controller)

writeHa :: (String, Doc) -> IO ()
writeHa (x, y) = do
                 handle <- openFile x WriteMode
                 hPutDoc handle y
                 hClose handle
main :: IO ()
main = do 
       let (dd, others) = unzip $ H.compileReactions hase bs
       let model = P.compileReactions hase bs [(waterTank, OutputVar (S "x"), (IntVar (S ("px")) 20))]
           (oo1, oo2) = unzip others
       mapM_ writeHa $ zip ["WaterTank.c", "Controller.c"] dd
       x <- openFile "Main.c" WriteMode
       hPutDoc x $ H.mkMain hase bs oo1 oo2
       hClose x
       -- XXX: use safety property: (px >= 20 && px < 100), works until
       -- some "d", but fails for some "d" Needs 1Million depth
       -- ltl p {[]!(px >= 100)};
       -- XXX: Use this liveness property: ltl p2 {[] ((waterTank_ON
       -- -> <> waterTank_OFF))}. Enable fairness -f in pan
       h <- openFile "Spin/WaterTank.pml" WriteMode
       hPutDoc h $ model
       hClose x
