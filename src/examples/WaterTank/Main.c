#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>


#define True 1
#define False 0
extern void readInput();
extern void writeOutput();
extern int waterTank(int, int);
extern int controller(int, int);
extern unsigned char waterTank_ON;
extern unsigned char controller_ON;
extern unsigned char waterTank_OFF;
extern unsigned char controller_OFF;
extern double controller_x;
extern double waterTank_x;
int main (void){
  int waterTank_cstate = 3 ;
  int waterTank_pstate = -1;
  int controller_cstate = 0 ;
  int controller_pstate = -1;
  
  
  while(True) {
    readInput();
    waterTank_ON = controller_ON;
    waterTank_OFF = controller_OFF;
    controller_x = waterTank_x;
    int waterTank_rstate = waterTank (waterTank_cstate, waterTank_pstate);
    waterTank_pstate = waterTank_cstate;
    waterTank_cstate = waterTank_rstate;
    int controller_rstate = controller (controller_cstate, controller_pstate);
    controller_pstate = controller_cstate;
    controller_cstate = controller_rstate;
    writeOutput();
  }
  return 0;
}