module TrainGate where
import Language
import qualified Hshac as H
import qualified Promela as P
import Prelude hiding ((==),(/=), LT, GT, (<=), (>=), (<), (>), (&&), not)
import Text.PrettyPrint.ANSI.Leijen
import GHC.IO.Handle
import System.IO

-- This is the train description

odey :: Ode
odey = Ode (DiffT (S "y")) (1) 0

assigny :: Update (External Output V)
assigny = OutputVar (S "y") := A (S "y")

t1 :: Loc
t1 =  Loc (L "t1") [odey] 
                   [InvariantLoc (DiffT (S "y")) (T (S "y") < TR 5)]
                   ([], [assigny])

t2 :: Loc
t2 = Loc (L "t2") [odey] 
                  [InvariantLoc (DiffT (S "y")) ((T (S "y") < TR 15) && (T (S "y") >= TR 5))]
                  ([], [assigny])

t3 :: Loc
t3 = Loc (L "t3") [odey] 
                  [InvariantLoc (DiffT (S "y")) ((T (S "y") < TR 25) && (T (S "y") >= TR 15))]
                  ([], [assigny])

e1 :: Edge
e1 = Edge t1 t2  [InvariantEdge (T (S "y") == TR 5)] 
                 ([(S "y") := A (S "y")], 
                   [(OutputVar (S "signal")) := 1,
                   (OutputVar (S "y")) := A (S "y")] , [])
                 []

e2 :: Edge
e2 =  Edge t2 t3 [InvariantEdge (T (S "y") == TR 15)]
                 ([(S "y") := A (S "y")],
                  [(OutputVar (S "signal")) := 0,
                   (OutputVar (S "y")) := A (S "y")], [])
                 []

e3 :: Edge
e3 = Edge t3 t1 [InvariantEdge  (T (S "y") == TR 25)]
                ([(S "y") := 0], 
                 [(OutputVar (S "signal")) := 0,
                  (OutputVar (S "y")) := A (S "y")] ,[])
                []


train :: Ha
train = Ha (L "Train") [t1, t2, t3] t1
                       [e1, e2, e3] 
                       [SymbolDecl (S "y") 0] 
                       [InputVarDecl (S "x")] 
                       [OutputVarDecl (S "signal") 0,
                        OutputVarDecl (S "y")  0] [] []

--  Now starts the Gate description
dxdt :: Diff
dxdt = DiffT (S "x")

xodeDown :: Ode
xodeDown = Ode dxdt (-A (S "x")/2) 10

xodeUp :: Ode
xodeUp = Ode dxdt (11-A (S ("x"))/2) 1

assignx :: Update (External Output V)
assignx = OutputVar (S "x") := A (S "x")

g1 :: Loc
g1 = Loc (L "g1") [xodeDown] 
                  [InvariantLoc dxdt ((T (S "x") >= TR 1) && (T (S "x") <= TR 10))]
                  ([], [assignx])

g2 :: Loc
g2 = Loc (L "g2") [xodeUp] 
                  [InvariantLoc dxdt ((T (S "x") >= TR 1) && (T (S "x") <= TR 10))]
                  ([], [assignx])

eg1 :: Edge
eg1 = Edge g1 g2 [InvariantEdge TTrue] 
                 ([(S "x") := A (S "x")], 
                  [(OutputVar (S "x")) := A (S "x")], [])
                 [Event (InternalEvent "UP")]

eg2 :: Edge
eg2 = Edge g2 g1 [InvariantEdge TTrue] 
                 ([(S "x") := A (S "x")], 
                  [(OutputVar (S "x")) := A (S "x")], [])
                 [Event (InternalEvent "DOWN")]
                 

gate :: Ha
gate =  Ha (L "Gate") [g1, g2] g2
           [eg1, eg2] [SymbolDecl (S "x") 1] [] 
           [OutputVarDecl (S "x") 0] [] []


-- XXX: Declaring the HA execution order
halist :: HaSeries
halist = train ::: (LastHa gate) -- This is the order in which the HAs
                                   -- will be executed.

-- XXX: Now bind the HAs
habind :: BindHa
habind = [(train, LastBind ((InputVar (S "x")) :< (OutputVar (S "x"))), gate)] 

writeToFile :: (String, Doc) -> IO ()
writeToFile (name, d) = do
                        handle <- openFile name WriteMode
                        hPutDoc handle d
                        hClose handle
main :: IO ()
main = do 
       let (reactions, others) = unzip $ H.compileReactions halist habind
           (oo1, oo2) = unzip others
           cFileNames = ["Train.c", "Gate.c"]
           mdoc = H.mkMain halist habind oo1 oo2
           -- XXX: The promela code gen
           promelaReactions = P.compileReactions halist habind []
       mapM_ writeToFile (zip cFileNames reactions)
       handle <- openFile "Main.c" WriteMode
       hPutDoc handle mdoc
       hClose handle
       h <- openFile "Spin/TrainGate.pml" WriteMode
       hPutDoc h promelaReactions
       hClose h
