#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0


double Gate_x = 10.0;

extern unsigned char  Gate_DOWN , Gate_UP ;//Events
static double  x  =  10 ; //the continuous vars
static double  x_u ; // and their updates
static double  x_init ; // and their inits
static double  slope_x ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { g1 , g2 , bottom , top }; // state declarations

enum states Gate (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( g1 ):
    if (True == False) {;}
    else if  (x == (10.0)) {
      x_u = x ;
      Gate_x = x ;
      cstate =  top ;
      force_init_update = False;
    }
    else if  (True && 
              Gate_DOWN) {
      x_u = x ;
      Gate_x = x ;
      cstate =  g2 ;
      force_init_update = False;
    }

    else if ( x >= (1.0) && (x < (10.0))     ) {
      if ((pstate != cstate) || force_init_update) x_init = x ;
      slope_x = (11 + (- (x / 2))) ;
      x_u = (slope_x * d) + x ;
      /* Possible Saturation */
      x_u = (x_u <  1.0  && signbit(slope_x ) &&  x_init >  1.0)  ?  1.0 : x_u ;
      x_u = (x_u >  10.0  && !signbit(slope_x ) &&  x_init <  10.0)  ?  10.0 : x_u ;
      
      Gate_UP  = False;
      Gate_DOWN  = False;
      cstate =  g1 ;
      force_init_update = False;
      
      Gate_x = x ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Gate!\n");
      exit(1);
    }
    break;
  case ( g2 ):
    if (True == False) {;}
    else if  (x == (1.0)) {
      x_u = x ;
      Gate_x = x ;
      cstate =  bottom ;
      force_init_update = False;
    }
    else if  (True && Gate_UP) {
      x_u = x ;
      Gate_x = x ;
      cstate =  g1 ;
      force_init_update = False;
    }

    else if ( x > (1.0) && (x <= (10.0))     ) {
      if ((pstate != cstate) || force_init_update) x_init = x ;
      slope_x = (- (x / 2)) ;
      x_u = (slope_x * d) + x ;
      /* Possible Saturation */
      x_u = (x_u <  1.0  && signbit(slope_x ) &&  x_init >  1.0)  ?  1.0 : x_u ;
      x_u = (x_u >  10.0  && !signbit(slope_x ) &&  x_init <  10.0)  ?  10.0 : x_u ;
      
      Gate_UP  = False;
      Gate_DOWN  = False;
      cstate =  g2 ;
      force_init_update = False;
      
      Gate_x = x ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Gate!\n");
      exit(1);
    }
    break;
  case ( bottom ):
    if (True == False) {;}
    else if  (True && 
              Gate_DOWN) {
      x_u = x ;
      Gate_x = x ;
      cstate =  bottom ;
      force_init_update = True;
    }
    else if  (True && Gate_UP) {
      x_u = x ;
      Gate_x = x ;
      cstate =  g1 ;
      force_init_update = False;
    }

    else if ( x == (1.0)     ) {
      if ((pstate != cstate) || force_init_update) x_init = x ;
      slope_x = 0 ;
      x_u = (slope_x * d) + x ;
      /* Possible Saturation */
      x_u = (x_u <  1.0  && signbit(slope_x ) &&  x_init >  1.0)  ?  1.0 : x_u ;
      x_u = (x_u >  1.0  && !signbit(slope_x ) &&  x_init <  1.0)  ?  1.0 : x_u ;
      
      Gate_UP  = False;
      Gate_DOWN  = False;
      cstate =  bottom ;
      force_init_update = False;
      
      Gate_x = x ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Gate!\n");
      exit(1);
    }
    break;
  case ( top ):
    if (True == False) {;}
    else if  (True && Gate_UP) {
      x_u = x ;
      Gate_x = x ;
      cstate =  top ;
      force_init_update = True;
    }
    else if  (True && 
              Gate_DOWN) {
      x_u = x ;
      Gate_x = x ;
      cstate =  g2 ;
      force_init_update = False;
    }

    else if ( x == (10.0)     ) {
      if ((pstate != cstate) || force_init_update) x_init = x ;
      slope_x = 0 ;
      x_u = (slope_x * d) + x ;
      /* Possible Saturation */
      x_u = (x_u <  10.0  && signbit(slope_x ) &&  x_init >  10.0)  ?  10.0 : x_u ;
      x_u = (x_u >  10.0  && !signbit(slope_x ) &&  x_init <  10.0)  ?  10.0 : x_u ;
      
      Gate_UP  = False;
      Gate_DOWN  = False;
      cstate =  top ;
      force_init_update = False;
      
      Gate_x = x ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Gate!\n");
      exit(1);
    }
    break;
  }
  x = x_u;
  return cstate;
}