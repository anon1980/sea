#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0


double Train_y = 0.0;


static double  y  =  0 ; //the continuous vars
static double  y_u ; // and their updates
static double  y_init ; // and their inits
static double  slope_y ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { t1 , t2 , t3 }; // state declarations

enum states Train (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( t1 ):
    if (True == False) {;}
    else if  (y == (5.0)) {
      y_u = y ;
      Train_y = y ;
      cstate =  t2 ;
      force_init_update = False;
    }

    else if ( y <= (5.0) && (y >= (0.0))     ) {
      if ((pstate != cstate) || force_init_update) y_init = y ;
      slope_y = 1 ;
      y_u = (slope_y * d) + y ;
      /* Possible Saturation */
      y_u = (y_u <  0.0  && signbit(slope_y ) &&  y_init >  0.0)  ?  0.0 : y_u ;
      y_u = (y_u >  5.0  && !signbit(slope_y ) &&  y_init <  5.0)  ?  5.0 : y_u ;
      
      
      cstate =  t1 ;
      force_init_update = False;
      
      Train_y = y ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Train!\n");
      exit(1);
    }
    break;
  case ( t2 ):
    if (True == False) {;}
    else if  (y == (15.0)) {
      y_u = y ;
      Train_y = y ;
      cstate =  t3 ;
      force_init_update = False;
    }

    else if ( y < (15.0) && (y >= (5.0))     ) {
      if ((pstate != cstate) || force_init_update) y_init = y ;
      slope_y = 1 ;
      y_u = (slope_y * d) + y ;
      /* Possible Saturation */
      y_u = (y_u <  5.0  && signbit(slope_y ) &&  y_init >  5.0)  ?  5.0 : y_u ;
      y_u = (y_u >  15.0  && !signbit(slope_y ) &&  y_init <  15.0)  ?  15.0 : y_u ;
      
      
      cstate =  t2 ;
      force_init_update = False;
      
      Train_y = y ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Train!\n");
      exit(1);
    }
    break;
  case ( t3 ):
    if (True == False) {;}
    else if  (y == (25.0)) {
      y_u = 0 ;
      Train_y = y ;
      cstate =  t1 ;
      force_init_update = False;
    }

    else if ( y < (25.0) && (y >= (15.0))     ) {
      if ((pstate != cstate) || force_init_update) y_init = y ;
      slope_y = 1 ;
      y_u = (slope_y * d) + y ;
      /* Possible Saturation */
      y_u = (y_u <  15.0  && signbit(slope_y ) &&  y_init >  15.0)  ?  15.0 : y_u ;
      y_u = (y_u >  25.0  && !signbit(slope_y ) &&  y_init <  25.0)  ?  25.0 : y_u ;
      
      
      cstate =  t3 ;
      force_init_update = False;
      
      Train_y = y ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Train!\n");
      exit(1);
    }
    break;
  }
  y = y_u;
  return cstate;
}