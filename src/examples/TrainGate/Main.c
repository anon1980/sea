#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>



#define True 1
#define False 0
extern void readInput();
extern void writeOutput();
extern int Gate(int, int);
extern int Train(int, int);
extern int Controller(int, int);
extern double Controller_y;
extern double Train_y;
extern unsigned char Gate_DOWN;
extern unsigned char Controller_DOWN;
extern unsigned char Gate_UP;
extern unsigned char Controller_UP;
int main (void){
  int Gate_cstate = 3 ;
  int Gate_pstate = -1;
  int Train_cstate = 0 ;
  int Train_pstate = -1;
  int Controller_cstate = 0 ;
  int Controller_pstate = -1;
  
  
  
  while(True) {
    readInput();
    Controller_y = Train_y;
    Gate_DOWN = Controller_DOWN;
    Gate_UP = Controller_UP;
    int Gate_rstate = Gate (Gate_cstate, Gate_pstate);
    Gate_pstate = Gate_cstate;
    Gate_cstate = Gate_rstate;
    int Train_rstate = Train (Train_cstate, Train_pstate);
    Train_pstate = Train_cstate;
    Train_cstate = Train_rstate;
    int Controller_rstate = Controller (Controller_cstate, Controller_pstate);
    Controller_pstate = Controller_cstate;
    Controller_cstate = Controller_rstate;
    writeOutput();
  }
  return 0;
}