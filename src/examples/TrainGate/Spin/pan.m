#define rand	pan_rand
#define pthread_equal(a,b)	((a)==(b))
#if defined(HAS_CODE) && defined(VERBOSE)
	#ifdef BFS_PAR
		bfs_printf("Pr: %d Tr: %d\n", II, t->forw);
	#else
		cpu_printf("Pr: %d Tr: %d\n", II, t->forw);
	#endif
#endif
	switch (t->forw) {
	default: Uerror("bad forward move");
	case 0:	/* if without executable clauses */
		continue;
	case 1: /* generic 'goto' or 'skip' */
		IfNotBlocked
		_m = 3; goto P999;
	case 2: /* generic 'else' */
		IfNotBlocked
		if (trpt->o_pm&1) continue;
		_m = 3; goto P999;

		 /* CLAIM p1 */
	case 3: // STATE 1 - _spin_nvr.tmp:3 - [((!(!(Gate_DOWN))&&!(Gate_UP)))] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[5][1] = 1;
		if (!(( !( !(((int)now.Gate_DOWN)))&& !(((int)now.Gate_UP)))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 4: // STATE 3 - _spin_nvr.tmp:4 - [((!(!(Gate_UP))&&!(Gate_DOWN)))] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported3 = 0;
			if (verbose && !reported3)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported3 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported3 = 0;
			if (verbose && !reported3)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported3 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[5][3] = 1;
		if (!(( !( !(((int)now.Gate_UP)))&& !(((int)now.Gate_DOWN)))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 5: // STATE 10 - _spin_nvr.tmp:9 - [(!(Gate_UP))] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported10 = 0;
			if (verbose && !reported10)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported10 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported10 = 0;
			if (verbose && !reported10)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported10 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[5][10] = 1;
		if (!( !(((int)now.Gate_UP))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 6: // STATE 15 - _spin_nvr.tmp:13 - [(!(Gate_DOWN))] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported15 = 0;
			if (verbose && !reported15)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported15 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported15 = 0;
			if (verbose && !reported15)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported15 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[5][15] = 1;
		if (!( !(((int)now.Gate_DOWN))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 7: // STATE 20 - _spin_nvr.tmp:15 - [-end-] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported20 = 0;
			if (verbose && !reported20)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported20 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported20 = 0;
			if (verbose && !reported20)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported20 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[5][20] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC :init: */
	case 8: // STATE 9 - TrainGateController.pml:690 - [D_STEP690]
		/* default 262 */
		

		reached[4][9] = 1;
		reached[4][t->st] = 1;
		reached[4][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_551_0: /* 2 */
		/* c_code84 */
		{ 

     }
;
S_552_0: /* 2 */
		/* c_code85 */
		{  now.train_pos = (int)Train_y;
             now.gate_pos = (int)Gate_x;  }
;
S_553_0: /* 2 */
		/* c_code86 */
		{  Controller_y = Train_y;  }
;
S_554_0: /* 2 */
		now.Gate_DOWN = ((int)now.Controller_DOWN);
#ifdef VAR_RANGES
		logval("Gate_DOWN", ((int)now.Gate_DOWN));
#endif
		;
S_555_0: /* 2 */
		now.Gate_UP = ((int)now.Controller_UP);
#ifdef VAR_RANGES
		logval("Gate_UP", ((int)now.Gate_UP));
#endif
		;
S_556_0: /* 2 */
		now.Gate_cstate = 1;
#ifdef VAR_RANGES
		logval("Gate_cstate", now.Gate_cstate);
#endif
		;
S_557_0: /* 2 */
		now.Train_cstate = 7;
#ifdef VAR_RANGES
		logval("Train_cstate", now.Train_cstate);
#endif
		;
S_558_0: /* 2 */
		now.Controller_cstate = 11;
#ifdef VAR_RANGES
		logval("Controller_cstate", now.Controller_cstate);
#endif
		;
		goto S_564_0;
S_564_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 9: // STATE 10 - TrainGateController.pml:704 - [(run main())] (0:0:0 - 1)
		IfNotBlocked
		reached[4][10] = 1;
		if (!(addproc(II, 1, 3)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 10: // STATE 11 - TrainGateController.pml:705 - [(run Controller())] (0:0:0 - 1)
		IfNotBlocked
		reached[4][11] = 1;
		if (!(addproc(II, 1, 2)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 11: // STATE 12 - TrainGateController.pml:706 - [(run Train())] (0:0:0 - 1)
		IfNotBlocked
		reached[4][12] = 1;
		if (!(addproc(II, 1, 1)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 12: // STATE 13 - TrainGateController.pml:707 - [(run Gate())] (0:0:0 - 1)
		IfNotBlocked
		reached[4][13] = 1;
		if (!(addproc(II, 1, 0)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 13: // STATE 15 - TrainGateController.pml:710 - [-end-] (0:0:0 - 1)
		IfNotBlocked
		reached[4][15] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC main */
	case 14: // STATE 1 - TrainGateController.pml:672 - [(((Gate_tick&&Train_tick)&&Controller_tick))] (0:0:0 - 1)
		IfNotBlocked
		reached[3][1] = 1;
		if (!(((((int)now.Gate_tick)&&((int)now.Train_tick))&&((int)now.Controller_tick))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 15: // STATE 10 - TrainGateController.pml:673 - [D_STEP673]
		/* default 262 */
		

		reached[3][10] = 1;
		reached[3][t->st] = 1;
		reached[3][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_536_0: /* 2 */
		/* c_code81 */
		{  x = x_u;
                 y = y_u;  }
;
S_537_0: /* 2 */
		/* c_code82 */
		{  now.train_pos = (int)Train_y;
                 now.gate_pos = (int)Gate_x;  }
;
S_538_0: /* 2 */
		/* c_code83 */
		{  Controller_y = Train_y;  }
;
S_539_0: /* 2 */
		now.Gate_DOWN = ((int)now.Controller_DOWN);
#ifdef VAR_RANGES
		logval("Gate_DOWN", ((int)now.Gate_DOWN));
#endif
		;
S_540_0: /* 2 */
		now.Gate_UP = ((int)now.Controller_UP);
#ifdef VAR_RANGES
		logval("Gate_UP", ((int)now.Gate_UP));
#endif
		;
S_541_0: /* 2 */
		now.Controller_tick = 0;
#ifdef VAR_RANGES
		logval("Controller_tick", ((int)now.Controller_tick));
#endif
		;
S_542_0: /* 2 */
		now.Train_tick = 0;
#ifdef VAR_RANGES
		logval("Train_tick", ((int)now.Train_tick));
#endif
		;
S_543_0: /* 2 */
		now.Gate_tick = 0;
#ifdef VAR_RANGES
		logval("Gate_tick", ((int)now.Gate_tick));
#endif
		;
		goto S_548_0;
S_548_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 16: // STATE 16 - TrainGateController.pml:688 - [-end-] (0:0:0 - 1)
		IfNotBlocked
		reached[3][16] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC Controller */
	case 17: // STATE 1 - TrainGateController.pml:483 - [(({c_code65}&&(Controller_cstate==Controller_cloc1)))] (0:0:0 - 1)
		IfNotBlocked
		reached[2][1] = 1;
		if (!((( ((Controller_y >= 5.0) && (Controller_y < 15.0)) )&&(now.Controller_cstate==11))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 18: // STATE 7 - TrainGateController.pml:484 - [D_STEP484]
		IfNotBlocked

		reached[2][7] = 1;
		reached[2][t->st] = 1;
		reached[2][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_365_0: /* 2 */
		now.Controller_DOWN = 1;
#ifdef VAR_RANGES
		logval("Controller_DOWN", ((int)now.Controller_DOWN));
#endif
		;
S_366_0: /* 2 */
		now.Controller_pstate = now.Controller_cstate;
#ifdef VAR_RANGES
		logval("Controller_pstate", now.Controller_pstate);
#endif
		;
S_367_0: /* 2 */
		now.Controller_cstate = 10;
#ifdef VAR_RANGES
		logval("Controller_cstate", now.Controller_cstate);
#endif
		;
S_368_0: /* 2 */
		now.Controller_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Controller_force_init_update", ((int)now.Controller_force_init_update));
#endif
		;
S_369_0: /* 2 */
		now.Controller_tick = 1;
#ifdef VAR_RANGES
		logval("Controller_tick", ((int)now.Controller_tick));
#endif
		;
		goto S_376_0;
S_376_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 19: // STATE 8 - TrainGateController.pml:492 - [(Controller_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[2][8] = 1;
		if (!(((int)now.Controller_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 20: // STATE 16 - TrainGateController.pml:498 - [((({c_code66}&&1)&&(Controller_cstate==Controller_cloc1)))] (0:0:0 - 1)
		IfNotBlocked
		reached[2][16] = 1;
		if (!(((( 1 && (Controller_y < 5.0) )&&1)&&(now.Controller_cstate==11))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 21: // STATE 17 - TrainGateController.pml:500 - [(((Controller_pstate!=Controller_cstate)||Controller_force_init_update))] (0:0:0 - 1)
		IfNotBlocked
		reached[2][17] = 1;
		if (!(((now.Controller_pstate!=now.Controller_cstate)||((int)now.Controller_force_init_update))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 22: // STATE 18 - TrainGateController.pml:501 - [{c_code67}] (0:0:0 - 1)
		IfNotBlocked
		reached[2][18] = 1;
		/* c_code67 */
		{ 
		sv_save();  }

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
;
		_m = 3; goto P999; /* 0 */
	case 23: // STATE 30 - TrainGateController.pml:504 - [D_STEP504]
		/* default 262 */
		

		reached[2][30] = 1;
		reached[2][t->st] = 1;
		reached[2][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_386_0: /* 2 */
		/* c_code68 */
		{ 




             }
;
S_387_0: /* 2 */
		now.Controller_pstate = now.Controller_cstate;
#ifdef VAR_RANGES
		logval("Controller_pstate", now.Controller_pstate);
#endif
		;
S_388_0: /* 2 */
		now.Controller_cstate = 11;
#ifdef VAR_RANGES
		logval("Controller_cstate", now.Controller_cstate);
#endif
		;
S_389_0: /* 2 */
		now.Controller_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Controller_force_init_update", ((int)now.Controller_force_init_update));
#endif
		;
S_390_0: /* 2 */
		now.Controller_UP = 0;
#ifdef VAR_RANGES
		logval("Controller_UP", ((int)now.Controller_UP));
#endif
		;
S_391_0: /* 2 */
		now.Controller_DOWN = 0;
#ifdef VAR_RANGES
		logval("Controller_DOWN", ((int)now.Controller_DOWN));
#endif
		;
S_392_0: /* 2 */
		now.Controller_tick = 1;
#ifdef VAR_RANGES
		logval("Controller_tick", ((int)now.Controller_tick));
#endif
		;
		goto S_399_0;
S_399_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 24: // STATE 31 - TrainGateController.pml:520 - [(Controller_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[2][31] = 1;
		if (!(((int)now.Controller_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 25: // STATE 44 - TrainGateController.pml:530 - [(({c_code69}&&(Controller_cstate==Controller_cloc2)))] (0:0:0 - 1)
		IfNotBlocked
		reached[2][44] = 1;
		if (!((( ((Controller_y >= 15.0) && (Controller_y < 25.0)) )&&(now.Controller_cstate==10))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 26: // STATE 50 - TrainGateController.pml:531 - [D_STEP531]
		IfNotBlocked

		reached[2][50] = 1;
		reached[2][t->st] = 1;
		reached[2][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_408_0: /* 2 */
		now.Controller_UP = 1;
#ifdef VAR_RANGES
		logval("Controller_UP", ((int)now.Controller_UP));
#endif
		;
S_409_0: /* 2 */
		now.Controller_pstate = now.Controller_cstate;
#ifdef VAR_RANGES
		logval("Controller_pstate", now.Controller_pstate);
#endif
		;
S_410_0: /* 2 */
		now.Controller_cstate = 9;
#ifdef VAR_RANGES
		logval("Controller_cstate", now.Controller_cstate);
#endif
		;
S_411_0: /* 2 */
		now.Controller_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Controller_force_init_update", ((int)now.Controller_force_init_update));
#endif
		;
S_412_0: /* 2 */
		now.Controller_tick = 1;
#ifdef VAR_RANGES
		logval("Controller_tick", ((int)now.Controller_tick));
#endif
		;
		goto S_419_0;
S_419_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 27: // STATE 51 - TrainGateController.pml:539 - [(Controller_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[2][51] = 1;
		if (!(((int)now.Controller_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 28: // STATE 59 - TrainGateController.pml:545 - [((({c_code70}&&1)&&(Controller_cstate==Controller_cloc2)))] (0:0:0 - 1)
		IfNotBlocked
		reached[2][59] = 1;
		if (!(((( 1 && (Controller_y < 15.0) )&&1)&&(now.Controller_cstate==10))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 29: // STATE 60 - TrainGateController.pml:547 - [(((Controller_pstate!=Controller_cstate)||Controller_force_init_update))] (0:0:0 - 1)
		IfNotBlocked
		reached[2][60] = 1;
		if (!(((now.Controller_pstate!=now.Controller_cstate)||((int)now.Controller_force_init_update))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 30: // STATE 61 - TrainGateController.pml:548 - [{c_code71}] (0:0:0 - 1)
		IfNotBlocked
		reached[2][61] = 1;
		/* c_code71 */
		{ 
		sv_save();  }

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
;
		_m = 3; goto P999; /* 0 */
	case 31: // STATE 73 - TrainGateController.pml:551 - [D_STEP551]
		/* default 262 */
		

		reached[2][73] = 1;
		reached[2][t->st] = 1;
		reached[2][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_429_0: /* 2 */
		/* c_code72 */
		{ 




             }
;
S_430_0: /* 2 */
		now.Controller_pstate = now.Controller_cstate;
#ifdef VAR_RANGES
		logval("Controller_pstate", now.Controller_pstate);
#endif
		;
S_431_0: /* 2 */
		now.Controller_cstate = 10;
#ifdef VAR_RANGES
		logval("Controller_cstate", now.Controller_cstate);
#endif
		;
S_432_0: /* 2 */
		now.Controller_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Controller_force_init_update", ((int)now.Controller_force_init_update));
#endif
		;
S_433_0: /* 2 */
		now.Controller_UP = 0;
#ifdef VAR_RANGES
		logval("Controller_UP", ((int)now.Controller_UP));
#endif
		;
S_434_0: /* 2 */
		now.Controller_DOWN = 0;
#ifdef VAR_RANGES
		logval("Controller_DOWN", ((int)now.Controller_DOWN));
#endif
		;
S_435_0: /* 2 */
		now.Controller_tick = 1;
#ifdef VAR_RANGES
		logval("Controller_tick", ((int)now.Controller_tick));
#endif
		;
		goto S_442_0;
S_442_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 32: // STATE 74 - TrainGateController.pml:567 - [(Controller_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[2][74] = 1;
		if (!(((int)now.Controller_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 33: // STATE 87 - TrainGateController.pml:577 - [(({c_code73}&&(Controller_cstate==Controller_cloc3)))] (0:0:0 - 1)
		IfNotBlocked
		reached[2][87] = 1;
		if (!((( (Controller_y >= 25.0) )&&(now.Controller_cstate==9))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 34: // STATE 92 - TrainGateController.pml:578 - [D_STEP578]
		IfNotBlocked

		reached[2][92] = 1;
		reached[2][t->st] = 1;
		reached[2][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_451_0: /* 2 */
		now.Controller_pstate = now.Controller_cstate;
#ifdef VAR_RANGES
		logval("Controller_pstate", now.Controller_pstate);
#endif
		;
S_452_0: /* 2 */
		now.Controller_cstate = 8;
#ifdef VAR_RANGES
		logval("Controller_cstate", now.Controller_cstate);
#endif
		;
S_453_0: /* 2 */
		now.Controller_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Controller_force_init_update", ((int)now.Controller_force_init_update));
#endif
		;
S_454_0: /* 2 */
		now.Controller_tick = 1;
#ifdef VAR_RANGES
		logval("Controller_tick", ((int)now.Controller_tick));
#endif
		;
		goto S_461_0;
S_461_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 35: // STATE 93 - TrainGateController.pml:586 - [(Controller_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[2][93] = 1;
		if (!(((int)now.Controller_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 36: // STATE 101 - TrainGateController.pml:592 - [((({c_code74}&&1)&&(Controller_cstate==Controller_cloc3)))] (0:0:0 - 1)
		IfNotBlocked
		reached[2][101] = 1;
		if (!(((( 1 && (Controller_y < 25.0) )&&1)&&(now.Controller_cstate==9))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 37: // STATE 102 - TrainGateController.pml:594 - [(((Controller_pstate!=Controller_cstate)||Controller_force_init_update))] (0:0:0 - 1)
		IfNotBlocked
		reached[2][102] = 1;
		if (!(((now.Controller_pstate!=now.Controller_cstate)||((int)now.Controller_force_init_update))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 38: // STATE 103 - TrainGateController.pml:595 - [{c_code75}] (0:0:0 - 1)
		IfNotBlocked
		reached[2][103] = 1;
		/* c_code75 */
		{ 
		sv_save();  }

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
;
		_m = 3; goto P999; /* 0 */
	case 39: // STATE 115 - TrainGateController.pml:598 - [D_STEP598]
		/* default 262 */
		

		reached[2][115] = 1;
		reached[2][t->st] = 1;
		reached[2][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_471_0: /* 2 */
		/* c_code76 */
		{ 




             }
;
S_472_0: /* 2 */
		now.Controller_pstate = now.Controller_cstate;
#ifdef VAR_RANGES
		logval("Controller_pstate", now.Controller_pstate);
#endif
		;
S_473_0: /* 2 */
		now.Controller_cstate = 9;
#ifdef VAR_RANGES
		logval("Controller_cstate", now.Controller_cstate);
#endif
		;
S_474_0: /* 2 */
		now.Controller_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Controller_force_init_update", ((int)now.Controller_force_init_update));
#endif
		;
S_475_0: /* 2 */
		now.Controller_UP = 0;
#ifdef VAR_RANGES
		logval("Controller_UP", ((int)now.Controller_UP));
#endif
		;
S_476_0: /* 2 */
		now.Controller_DOWN = 0;
#ifdef VAR_RANGES
		logval("Controller_DOWN", ((int)now.Controller_DOWN));
#endif
		;
S_477_0: /* 2 */
		now.Controller_tick = 1;
#ifdef VAR_RANGES
		logval("Controller_tick", ((int)now.Controller_tick));
#endif
		;
		goto S_484_0;
S_484_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 40: // STATE 116 - TrainGateController.pml:614 - [(Controller_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[2][116] = 1;
		if (!(((int)now.Controller_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 41: // STATE 129 - TrainGateController.pml:624 - [(({c_code77}&&(Controller_cstate==Controller_cloc4)))] (0:0:0 - 1)
		IfNotBlocked
		reached[2][129] = 1;
		if (!((( (Controller_y <= 0.0) )&&(now.Controller_cstate==8))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 42: // STATE 134 - TrainGateController.pml:625 - [D_STEP625]
		IfNotBlocked

		reached[2][134] = 1;
		reached[2][t->st] = 1;
		reached[2][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_493_0: /* 2 */
		now.Controller_pstate = now.Controller_cstate;
#ifdef VAR_RANGES
		logval("Controller_pstate", now.Controller_pstate);
#endif
		;
S_494_0: /* 2 */
		now.Controller_cstate = 11;
#ifdef VAR_RANGES
		logval("Controller_cstate", now.Controller_cstate);
#endif
		;
S_495_0: /* 2 */
		now.Controller_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Controller_force_init_update", ((int)now.Controller_force_init_update));
#endif
		;
S_496_0: /* 2 */
		now.Controller_tick = 1;
#ifdef VAR_RANGES
		logval("Controller_tick", ((int)now.Controller_tick));
#endif
		;
		goto S_503_0;
S_503_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 43: // STATE 135 - TrainGateController.pml:633 - [(Controller_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[2][135] = 1;
		if (!(((int)now.Controller_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 44: // STATE 143 - TrainGateController.pml:639 - [((({c_code78}&&1)&&(Controller_cstate==Controller_cloc4)))] (0:0:0 - 1)
		IfNotBlocked
		reached[2][143] = 1;
		if (!(((( 1 && (Controller_y >= 25.0) )&&1)&&(now.Controller_cstate==8))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 45: // STATE 144 - TrainGateController.pml:641 - [(((Controller_pstate!=Controller_cstate)||Controller_force_init_update))] (0:0:0 - 1)
		IfNotBlocked
		reached[2][144] = 1;
		if (!(((now.Controller_pstate!=now.Controller_cstate)||((int)now.Controller_force_init_update))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 46: // STATE 145 - TrainGateController.pml:642 - [{c_code79}] (0:0:0 - 1)
		IfNotBlocked
		reached[2][145] = 1;
		/* c_code79 */
		{ 
		sv_save();  }

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
;
		_m = 3; goto P999; /* 0 */
	case 47: // STATE 157 - TrainGateController.pml:645 - [D_STEP645]
		/* default 262 */
		

		reached[2][157] = 1;
		reached[2][t->st] = 1;
		reached[2][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_513_0: /* 2 */
		/* c_code80 */
		{ 




             }
;
S_514_0: /* 2 */
		now.Controller_pstate = now.Controller_cstate;
#ifdef VAR_RANGES
		logval("Controller_pstate", now.Controller_pstate);
#endif
		;
S_515_0: /* 2 */
		now.Controller_cstate = 8;
#ifdef VAR_RANGES
		logval("Controller_cstate", now.Controller_cstate);
#endif
		;
S_516_0: /* 2 */
		now.Controller_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Controller_force_init_update", ((int)now.Controller_force_init_update));
#endif
		;
S_517_0: /* 2 */
		now.Controller_UP = 0;
#ifdef VAR_RANGES
		logval("Controller_UP", ((int)now.Controller_UP));
#endif
		;
S_518_0: /* 2 */
		now.Controller_DOWN = 0;
#ifdef VAR_RANGES
		logval("Controller_DOWN", ((int)now.Controller_DOWN));
#endif
		;
S_519_0: /* 2 */
		now.Controller_tick = 1;
#ifdef VAR_RANGES
		logval("Controller_tick", ((int)now.Controller_tick));
#endif
		;
		goto S_526_0;
S_526_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 48: // STATE 158 - TrainGateController.pml:661 - [(Controller_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[2][158] = 1;
		if (!(((int)now.Controller_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 49: // STATE 171 - TrainGateController.pml:667 - [-end-] (0:0:0 - 5)
		IfNotBlocked
		reached[2][171] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC Train */
	case 50: // STATE 1 - TrainGateController.pml:323 - [(({c_code45}&&(Train_cstate==Train_t1)))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][1] = 1;
		if (!((( (y == 5.0) )&&(now.Train_cstate==7))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 51: // STATE 8 - TrainGateController.pml:324 - [D_STEP324]
		/* default 262 */
		

		reached[1][8] = 1;
		reached[1][t->st] = 1;
		reached[1][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_238_0: /* 2 */
		/* c_code46 */
		{  y_u = y ;  }
;
S_239_0: /* 2 */
		/* c_code47 */
		{  Train_y = y ;  }
;
S_240_0: /* 2 */
		now.Train_pstate = now.Train_cstate;
#ifdef VAR_RANGES
		logval("Train_pstate", now.Train_pstate);
#endif
		;
S_241_0: /* 2 */
		now.Train_cstate = 6;
#ifdef VAR_RANGES
		logval("Train_cstate", now.Train_cstate);
#endif
		;
S_242_0: /* 2 */
		now.Train_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Train_force_init_update", ((int)now.Train_force_init_update));
#endif
		;
S_243_0: /* 2 */
		now.Train_tick = 1;
#ifdef VAR_RANGES
		logval("Train_tick", ((int)now.Train_tick));
#endif
		;
		goto S_250_0;
S_250_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 52: // STATE 9 - TrainGateController.pml:333 - [(Train_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[1][9] = 1;
		if (!(((int)now.Train_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 53: // STATE 17 - TrainGateController.pml:339 - [((({c_code48}&&1)&&(Train_cstate==Train_t1)))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][17] = 1;
		if (!(((( ((y <= 5.0) && (y >= 0.0)) )&&1)&&(now.Train_cstate==7))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 54: // STATE 18 - TrainGateController.pml:341 - [(((Train_pstate!=Train_cstate)||Train_force_init_update))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][18] = 1;
		if (!(((now.Train_pstate!=now.Train_cstate)||((int)now.Train_force_init_update))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 55: // STATE 19 - TrainGateController.pml:342 - [{c_code49}] (0:0:0 - 1)
		IfNotBlocked
		reached[1][19] = 1;
		/* c_code49 */
		{ 
		sv_save(); y_init = y ;  }

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
;
		_m = 3; goto P999; /* 0 */
	case 56: // STATE 29 - TrainGateController.pml:345 - [D_STEP345]
		/* default 262 */
		

		reached[1][29] = 1;
		reached[1][t->st] = 1;
		reached[1][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_260_0: /* 2 */
		/* c_code50 */
		{ 
             y_slope = 1 ;
             y_u = (y_slope * d) + y ;
             y_u = (y_u < 0.0 && signbit(y_slope) && y_init > 0.0) ? 0.0 : y_u ;
             y_u = (y_u > 5.0 && !signbit(y_slope) && y_init < 5.0) ? 5.0 : y_u ;

             Train_y = y ;
             }
;
S_261_0: /* 2 */
		now.Train_pstate = now.Train_cstate;
#ifdef VAR_RANGES
		logval("Train_pstate", now.Train_pstate);
#endif
		;
S_262_0: /* 2 */
		now.Train_cstate = 7;
#ifdef VAR_RANGES
		logval("Train_cstate", now.Train_cstate);
#endif
		;
S_263_0: /* 2 */
		now.Train_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Train_force_init_update", ((int)now.Train_force_init_update));
#endif
		;
S_264_0: /* 2 */
		now.Train_tick = 1;
#ifdef VAR_RANGES
		logval("Train_tick", ((int)now.Train_tick));
#endif
		;
		goto S_271_0;
S_271_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 57: // STATE 30 - TrainGateController.pml:362 - [(Train_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[1][30] = 1;
		if (!(((int)now.Train_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 58: // STATE 43 - TrainGateController.pml:371 - [(({c_code51}&&(Train_cstate==Train_t2)))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][43] = 1;
		if (!((( (y == 15.0) )&&(now.Train_cstate==6))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 59: // STATE 50 - TrainGateController.pml:372 - [D_STEP372]
		/* default 262 */
		

		reached[1][50] = 1;
		reached[1][t->st] = 1;
		reached[1][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_280_0: /* 2 */
		/* c_code52 */
		{  y_u = y ;  }
;
S_281_0: /* 2 */
		/* c_code53 */
		{  Train_y = y ;  }
;
S_282_0: /* 2 */
		now.Train_pstate = now.Train_cstate;
#ifdef VAR_RANGES
		logval("Train_pstate", now.Train_pstate);
#endif
		;
S_283_0: /* 2 */
		now.Train_cstate = 5;
#ifdef VAR_RANGES
		logval("Train_cstate", now.Train_cstate);
#endif
		;
S_284_0: /* 2 */
		now.Train_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Train_force_init_update", ((int)now.Train_force_init_update));
#endif
		;
S_285_0: /* 2 */
		now.Train_tick = 1;
#ifdef VAR_RANGES
		logval("Train_tick", ((int)now.Train_tick));
#endif
		;
		goto S_292_0;
S_292_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 60: // STATE 51 - TrainGateController.pml:381 - [(Train_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[1][51] = 1;
		if (!(((int)now.Train_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 61: // STATE 59 - TrainGateController.pml:387 - [((({c_code54}&&1)&&(Train_cstate==Train_t2)))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][59] = 1;
		if (!(((( ((y < 15.0) && (y >= 5.0)) )&&1)&&(now.Train_cstate==6))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 62: // STATE 60 - TrainGateController.pml:389 - [(((Train_pstate!=Train_cstate)||Train_force_init_update))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][60] = 1;
		if (!(((now.Train_pstate!=now.Train_cstate)||((int)now.Train_force_init_update))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 63: // STATE 61 - TrainGateController.pml:390 - [{c_code55}] (0:0:0 - 1)
		IfNotBlocked
		reached[1][61] = 1;
		/* c_code55 */
		{ 
		sv_save(); y_init = y ;  }

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
;
		_m = 3; goto P999; /* 0 */
	case 64: // STATE 71 - TrainGateController.pml:393 - [D_STEP393]
		/* default 262 */
		

		reached[1][71] = 1;
		reached[1][t->st] = 1;
		reached[1][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_302_0: /* 2 */
		/* c_code56 */
		{ 
             y_slope = 1 ;
             y_u = (y_slope * d) + y ;
             y_u = (y_u < 5.0 && signbit(y_slope) && y_init > 5.0) ? 5.0 : y_u ;
             y_u = (y_u > 15.0 && !signbit(y_slope) && y_init < 15.0) ? 15.0 : y_u ;

             Train_y = y ;
             }
;
S_303_0: /* 2 */
		now.Train_pstate = now.Train_cstate;
#ifdef VAR_RANGES
		logval("Train_pstate", now.Train_pstate);
#endif
		;
S_304_0: /* 2 */
		now.Train_cstate = 6;
#ifdef VAR_RANGES
		logval("Train_cstate", now.Train_cstate);
#endif
		;
S_305_0: /* 2 */
		now.Train_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Train_force_init_update", ((int)now.Train_force_init_update));
#endif
		;
S_306_0: /* 2 */
		now.Train_tick = 1;
#ifdef VAR_RANGES
		logval("Train_tick", ((int)now.Train_tick));
#endif
		;
		goto S_313_0;
S_313_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 65: // STATE 72 - TrainGateController.pml:410 - [(Train_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[1][72] = 1;
		if (!(((int)now.Train_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 66: // STATE 85 - TrainGateController.pml:419 - [(({c_code57}&&(Train_cstate==Train_t3)))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][85] = 1;
		if (!((( (y == 25.0) )&&(now.Train_cstate==5))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 67: // STATE 92 - TrainGateController.pml:420 - [D_STEP420]
		/* default 262 */
		

		reached[1][92] = 1;
		reached[1][t->st] = 1;
		reached[1][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_322_0: /* 2 */
		/* c_code58 */
		{  y_u = 0 ;  }
;
S_323_0: /* 2 */
		/* c_code59 */
		{  Train_y = y ;  }
;
S_324_0: /* 2 */
		now.Train_pstate = now.Train_cstate;
#ifdef VAR_RANGES
		logval("Train_pstate", now.Train_pstate);
#endif
		;
S_325_0: /* 2 */
		now.Train_cstate = 7;
#ifdef VAR_RANGES
		logval("Train_cstate", now.Train_cstate);
#endif
		;
S_326_0: /* 2 */
		now.Train_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Train_force_init_update", ((int)now.Train_force_init_update));
#endif
		;
S_327_0: /* 2 */
		now.Train_tick = 1;
#ifdef VAR_RANGES
		logval("Train_tick", ((int)now.Train_tick));
#endif
		;
		goto S_334_0;
S_334_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 68: // STATE 93 - TrainGateController.pml:429 - [(Train_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[1][93] = 1;
		if (!(((int)now.Train_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 69: // STATE 101 - TrainGateController.pml:435 - [((({c_code60}&&1)&&(Train_cstate==Train_t3)))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][101] = 1;
		if (!(((( ((y < 25.0) && (y >= 15.0)) )&&1)&&(now.Train_cstate==5))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 70: // STATE 102 - TrainGateController.pml:437 - [(((Train_pstate!=Train_cstate)||Train_force_init_update))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][102] = 1;
		if (!(((now.Train_pstate!=now.Train_cstate)||((int)now.Train_force_init_update))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 71: // STATE 103 - TrainGateController.pml:438 - [{c_code61}] (0:0:0 - 1)
		IfNotBlocked
		reached[1][103] = 1;
		/* c_code61 */
		{ 
		sv_save(); y_init = y ;  }

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
;
		_m = 3; goto P999; /* 0 */
	case 72: // STATE 113 - TrainGateController.pml:441 - [D_STEP441]
		/* default 262 */
		

		reached[1][113] = 1;
		reached[1][t->st] = 1;
		reached[1][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_344_0: /* 2 */
		/* c_code62 */
		{ 
             y_slope = 1 ;
             y_u = (y_slope * d) + y ;
             y_u = (y_u < 15.0 && signbit(y_slope) && y_init > 15.0) ? 15.0 : y_u ;
             y_u = (y_u > 25.0 && !signbit(y_slope) && y_init < 25.0) ? 25.0 : y_u ;

             Train_y = y ;
             }
;
S_345_0: /* 2 */
		now.Train_pstate = now.Train_cstate;
#ifdef VAR_RANGES
		logval("Train_pstate", now.Train_pstate);
#endif
		;
S_346_0: /* 2 */
		now.Train_cstate = 5;
#ifdef VAR_RANGES
		logval("Train_cstate", now.Train_cstate);
#endif
		;
S_347_0: /* 2 */
		now.Train_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Train_force_init_update", ((int)now.Train_force_init_update));
#endif
		;
S_348_0: /* 2 */
		now.Train_tick = 1;
#ifdef VAR_RANGES
		logval("Train_tick", ((int)now.Train_tick));
#endif
		;
		goto S_355_0;
S_355_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 73: // STATE 114 - TrainGateController.pml:458 - [(Train_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[1][114] = 1;
		if (!(((int)now.Train_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 74: // STATE 127 - TrainGateController.pml:464 - [-end-] (0:0:0 - 5)
		IfNotBlocked
		reached[1][127] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC Gate */
	case 75: // STATE 1 - TrainGateController.pml:40 - [(({c_code6}&&(Gate_cstate==Gate_g1)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][1] = 1;
		if (!((( (x == 10.0) )&&(now.Gate_cstate==4))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 76: // STATE 8 - TrainGateController.pml:41 - [D_STEP41]
		/* default 262 */
		

		reached[0][8] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_001_0: /* 2 */
		/* c_code7 */
		{  x_u = x ;  }
;
S_002_0: /* 2 */
		/* c_code8 */
		{  Gate_x = x ;  }
;
S_003_0: /* 2 */
		now.Gate_pstate = now.Gate_cstate;
#ifdef VAR_RANGES
		logval("Gate_pstate", now.Gate_pstate);
#endif
		;
S_004_0: /* 2 */
		now.Gate_cstate = 1;
#ifdef VAR_RANGES
		logval("Gate_cstate", now.Gate_cstate);
#endif
		;
S_005_0: /* 2 */
		now.Gate_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Gate_force_init_update", ((int)now.Gate_force_init_update));
#endif
		;
S_006_0: /* 2 */
		now.Gate_tick = 1;
#ifdef VAR_RANGES
		logval("Gate_tick", ((int)now.Gate_tick));
#endif
		;
		goto S_013_0;
S_013_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 77: // STATE 9 - TrainGateController.pml:50 - [(Gate_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][9] = 1;
		if (!(((int)now.Gate_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 78: // STATE 16 - TrainGateController.pml:55 - [((({c_code9}&&Gate_DOWN)&&(Gate_cstate==Gate_g1)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][16] = 1;
		if (!(((( 1 )&&((int)now.Gate_DOWN))&&(now.Gate_cstate==4))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 79: // STATE 23 - TrainGateController.pml:56 - [D_STEP56]
		/* default 262 */
		

		reached[0][23] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_016_0: /* 2 */
		/* c_code10 */
		{  x_u = x ;  }
;
S_017_0: /* 2 */
		/* c_code11 */
		{  Gate_x = x ;  }
;
S_018_0: /* 2 */
		now.Gate_pstate = now.Gate_cstate;
#ifdef VAR_RANGES
		logval("Gate_pstate", now.Gate_pstate);
#endif
		;
S_019_0: /* 2 */
		now.Gate_cstate = 3;
#ifdef VAR_RANGES
		logval("Gate_cstate", now.Gate_cstate);
#endif
		;
S_020_0: /* 2 */
		now.Gate_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Gate_force_init_update", ((int)now.Gate_force_init_update));
#endif
		;
S_021_0: /* 2 */
		now.Gate_tick = 1;
#ifdef VAR_RANGES
		logval("Gate_tick", ((int)now.Gate_tick));
#endif
		;
		goto S_028_0;
S_028_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 80: // STATE 24 - TrainGateController.pml:65 - [(Gate_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][24] = 1;
		if (!(((int)now.Gate_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 81: // STATE 32 - TrainGateController.pml:73 - [(((({c_code12}&&!(Gate_UP))&&!(Gate_DOWN))&&(Gate_cstate==Gate_g1)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][32] = 1;
		if (!((((( ((x >= 1.0) && (x < 10.0)) )&& !(((int)now.Gate_UP)))&& !(((int)now.Gate_DOWN)))&&(now.Gate_cstate==4))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 82: // STATE 33 - TrainGateController.pml:75 - [(((Gate_pstate!=Gate_cstate)||Gate_force_init_update))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][33] = 1;
		if (!(((now.Gate_pstate!=now.Gate_cstate)||((int)now.Gate_force_init_update))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 83: // STATE 34 - TrainGateController.pml:76 - [{c_code13}] (0:0:0 - 1)
		IfNotBlocked
		reached[0][34] = 1;
		/* c_code13 */
		{ 
		sv_save(); x_init = x ;  }

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
;
		_m = 3; goto P999; /* 0 */
	case 84: // STATE 46 - TrainGateController.pml:79 - [D_STEP79]
		/* default 262 */
		

		reached[0][46] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_038_0: /* 2 */
		/* c_code14 */
		{ 
             x_slope = (11 + (- (x / 2))) ;
             x_u = (x_slope * d) + x ;
             x_u = (x_u < 1.0 && signbit(x_slope) && x_init > 1.0) ? 1.0 : x_u ;
             x_u = (x_u > 10.0 && !signbit(x_slope) && x_init < 10.0) ? 10.0 : x_u ;

             Gate_x = x ;
             }
;
S_039_0: /* 2 */
		now.Gate_pstate = now.Gate_cstate;
#ifdef VAR_RANGES
		logval("Gate_pstate", now.Gate_pstate);
#endif
		;
S_040_0: /* 2 */
		now.Gate_cstate = 4;
#ifdef VAR_RANGES
		logval("Gate_cstate", now.Gate_cstate);
#endif
		;
S_041_0: /* 2 */
		now.Gate_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Gate_force_init_update", ((int)now.Gate_force_init_update));
#endif
		;
S_042_0: /* 2 */
		now.Gate_UP = 0;
#ifdef VAR_RANGES
		logval("Gate_UP", ((int)now.Gate_UP));
#endif
		;
S_043_0: /* 2 */
		now.Gate_DOWN = 0;
#ifdef VAR_RANGES
		logval("Gate_DOWN", ((int)now.Gate_DOWN));
#endif
		;
S_044_0: /* 2 */
		now.Gate_tick = 1;
#ifdef VAR_RANGES
		logval("Gate_tick", ((int)now.Gate_tick));
#endif
		;
		goto S_051_0;
S_051_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 85: // STATE 47 - TrainGateController.pml:97 - [(Gate_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][47] = 1;
		if (!(((int)now.Gate_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 86: // STATE 60 - TrainGateController.pml:106 - [(({c_code15}&&(Gate_cstate==Gate_g2)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][60] = 1;
		if (!((( (x == 1.0) )&&(now.Gate_cstate==3))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 87: // STATE 67 - TrainGateController.pml:107 - [D_STEP107]
		/* default 262 */
		

		reached[0][67] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_060_0: /* 2 */
		/* c_code16 */
		{  x_u = x ;  }
;
S_061_0: /* 2 */
		/* c_code17 */
		{  Gate_x = x ;  }
;
S_062_0: /* 2 */
		now.Gate_pstate = now.Gate_cstate;
#ifdef VAR_RANGES
		logval("Gate_pstate", now.Gate_pstate);
#endif
		;
S_063_0: /* 2 */
		now.Gate_cstate = 2;
#ifdef VAR_RANGES
		logval("Gate_cstate", now.Gate_cstate);
#endif
		;
S_064_0: /* 2 */
		now.Gate_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Gate_force_init_update", ((int)now.Gate_force_init_update));
#endif
		;
S_065_0: /* 2 */
		now.Gate_tick = 1;
#ifdef VAR_RANGES
		logval("Gate_tick", ((int)now.Gate_tick));
#endif
		;
		goto S_072_0;
S_072_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 88: // STATE 68 - TrainGateController.pml:116 - [(Gate_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][68] = 1;
		if (!(((int)now.Gate_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 89: // STATE 75 - TrainGateController.pml:121 - [((({c_code18}&&Gate_UP)&&(Gate_cstate==Gate_g2)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][75] = 1;
		if (!(((( 1 )&&((int)now.Gate_UP))&&(now.Gate_cstate==3))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 90: // STATE 82 - TrainGateController.pml:122 - [D_STEP122]
		/* default 262 */
		

		reached[0][82] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_075_0: /* 2 */
		/* c_code19 */
		{  x_u = x ;  }
;
S_076_0: /* 2 */
		/* c_code20 */
		{  Gate_x = x ;  }
;
S_077_0: /* 2 */
		now.Gate_pstate = now.Gate_cstate;
#ifdef VAR_RANGES
		logval("Gate_pstate", now.Gate_pstate);
#endif
		;
S_078_0: /* 2 */
		now.Gate_cstate = 4;
#ifdef VAR_RANGES
		logval("Gate_cstate", now.Gate_cstate);
#endif
		;
S_079_0: /* 2 */
		now.Gate_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Gate_force_init_update", ((int)now.Gate_force_init_update));
#endif
		;
S_080_0: /* 2 */
		now.Gate_tick = 1;
#ifdef VAR_RANGES
		logval("Gate_tick", ((int)now.Gate_tick));
#endif
		;
		goto S_087_0;
S_087_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 91: // STATE 83 - TrainGateController.pml:131 - [(Gate_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][83] = 1;
		if (!(((int)now.Gate_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 92: // STATE 91 - TrainGateController.pml:139 - [(((({c_code21}&&!(Gate_UP))&&!(Gate_DOWN))&&(Gate_cstate==Gate_g2)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][91] = 1;
		if (!((((( ((x > 1.0) && (x <= 10.0)) )&& !(((int)now.Gate_UP)))&& !(((int)now.Gate_DOWN)))&&(now.Gate_cstate==3))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 93: // STATE 92 - TrainGateController.pml:141 - [(((Gate_pstate!=Gate_cstate)||Gate_force_init_update))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][92] = 1;
		if (!(((now.Gate_pstate!=now.Gate_cstate)||((int)now.Gate_force_init_update))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 94: // STATE 93 - TrainGateController.pml:142 - [{c_code22}] (0:0:0 - 1)
		IfNotBlocked
		reached[0][93] = 1;
		/* c_code22 */
		{ 
		sv_save(); x_init = x ;  }

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
;
		_m = 3; goto P999; /* 0 */
	case 95: // STATE 105 - TrainGateController.pml:145 - [D_STEP145]
		/* default 262 */
		

		reached[0][105] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_097_0: /* 2 */
		/* c_code23 */
		{ 
             x_slope = (- (x / 2)) ;
             x_u = (x_slope * d) + x ;
             x_u = (x_u < 1.0 && signbit(x_slope) && x_init > 1.0) ? 1.0 : x_u ;
             x_u = (x_u > 10.0 && !signbit(x_slope) && x_init < 10.0) ? 10.0 : x_u ;

             Gate_x = x ;
             }
;
S_098_0: /* 2 */
		now.Gate_pstate = now.Gate_cstate;
#ifdef VAR_RANGES
		logval("Gate_pstate", now.Gate_pstate);
#endif
		;
S_099_0: /* 2 */
		now.Gate_cstate = 3;
#ifdef VAR_RANGES
		logval("Gate_cstate", now.Gate_cstate);
#endif
		;
S_100_0: /* 2 */
		now.Gate_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Gate_force_init_update", ((int)now.Gate_force_init_update));
#endif
		;
S_101_0: /* 2 */
		now.Gate_UP = 0;
#ifdef VAR_RANGES
		logval("Gate_UP", ((int)now.Gate_UP));
#endif
		;
S_102_0: /* 2 */
		now.Gate_DOWN = 0;
#ifdef VAR_RANGES
		logval("Gate_DOWN", ((int)now.Gate_DOWN));
#endif
		;
S_103_0: /* 2 */
		now.Gate_tick = 1;
#ifdef VAR_RANGES
		logval("Gate_tick", ((int)now.Gate_tick));
#endif
		;
		goto S_110_0;
S_110_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 96: // STATE 106 - TrainGateController.pml:163 - [(Gate_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][106] = 1;
		if (!(((int)now.Gate_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 97: // STATE 119 - TrainGateController.pml:173 - [((({c_code24}&&Gate_DOWN)&&(Gate_cstate==Gate_bottom)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][119] = 1;
		if (!(((( 1 )&&((int)now.Gate_DOWN))&&(now.Gate_cstate==2))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 98: // STATE 126 - TrainGateController.pml:174 - [D_STEP174]
		/* default 262 */
		

		reached[0][126] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_119_0: /* 2 */
		/* c_code25 */
		{  x_u = x ;  }
;
S_120_0: /* 2 */
		/* c_code26 */
		{  Gate_x = x ;  }
;
S_121_0: /* 2 */
		now.Gate_pstate = now.Gate_cstate;
#ifdef VAR_RANGES
		logval("Gate_pstate", now.Gate_pstate);
#endif
		;
S_122_0: /* 2 */
		now.Gate_cstate = 2;
#ifdef VAR_RANGES
		logval("Gate_cstate", now.Gate_cstate);
#endif
		;
S_123_0: /* 2 */
		now.Gate_force_init_update = 1;
#ifdef VAR_RANGES
		logval("Gate_force_init_update", ((int)now.Gate_force_init_update));
#endif
		;
S_124_0: /* 2 */
		now.Gate_tick = 1;
#ifdef VAR_RANGES
		logval("Gate_tick", ((int)now.Gate_tick));
#endif
		;
		goto S_131_0;
S_131_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 99: // STATE 127 - TrainGateController.pml:183 - [(Gate_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][127] = 1;
		if (!(((int)now.Gate_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 100: // STATE 134 - TrainGateController.pml:188 - [((({c_code27}&&Gate_UP)&&(Gate_cstate==Gate_bottom)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][134] = 1;
		if (!(((( 1 )&&((int)now.Gate_UP))&&(now.Gate_cstate==2))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 101: // STATE 141 - TrainGateController.pml:189 - [D_STEP189]
		/* default 262 */
		

		reached[0][141] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_134_0: /* 2 */
		/* c_code28 */
		{  x_u = x ;  }
;
S_135_0: /* 2 */
		/* c_code29 */
		{  Gate_x = x ;  }
;
S_136_0: /* 2 */
		now.Gate_pstate = now.Gate_cstate;
#ifdef VAR_RANGES
		logval("Gate_pstate", now.Gate_pstate);
#endif
		;
S_137_0: /* 2 */
		now.Gate_cstate = 4;
#ifdef VAR_RANGES
		logval("Gate_cstate", now.Gate_cstate);
#endif
		;
S_138_0: /* 2 */
		now.Gate_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Gate_force_init_update", ((int)now.Gate_force_init_update));
#endif
		;
S_139_0: /* 2 */
		now.Gate_tick = 1;
#ifdef VAR_RANGES
		logval("Gate_tick", ((int)now.Gate_tick));
#endif
		;
		goto S_146_0;
S_146_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 102: // STATE 142 - TrainGateController.pml:198 - [(Gate_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][142] = 1;
		if (!(((int)now.Gate_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 103: // STATE 150 - TrainGateController.pml:206 - [(((({c_code30}&&!(Gate_UP))&&!(Gate_DOWN))&&(Gate_cstate==Gate_bottom)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][150] = 1;
		if (!((((( (x == 1.0) )&& !(((int)now.Gate_UP)))&& !(((int)now.Gate_DOWN)))&&(now.Gate_cstate==2))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 104: // STATE 151 - TrainGateController.pml:208 - [(((Gate_pstate!=Gate_cstate)||Gate_force_init_update))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][151] = 1;
		if (!(((now.Gate_pstate!=now.Gate_cstate)||((int)now.Gate_force_init_update))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 105: // STATE 152 - TrainGateController.pml:209 - [{c_code31}] (0:0:0 - 1)
		IfNotBlocked
		reached[0][152] = 1;
		/* c_code31 */
		{ 
		sv_save(); x_init = x ;  }

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
;
		_m = 3; goto P999; /* 0 */
	case 106: // STATE 164 - TrainGateController.pml:212 - [D_STEP212]
		/* default 262 */
		

		reached[0][164] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_156_0: /* 2 */
		/* c_code32 */
		{ 
             x_slope = 0 ;
             x_u = (x_slope * d) + x ;
             x_u = (x_u < 1.0 && signbit(x_slope) && x_init > 1.0) ? 1.0 : x_u ;
             x_u = (x_u > 1.0 && !signbit(x_slope) && x_init < 1.0) ? 1.0 : x_u ;

             Gate_x = x ;
             }
;
S_157_0: /* 2 */
		now.Gate_pstate = now.Gate_cstate;
#ifdef VAR_RANGES
		logval("Gate_pstate", now.Gate_pstate);
#endif
		;
S_158_0: /* 2 */
		now.Gate_cstate = 2;
#ifdef VAR_RANGES
		logval("Gate_cstate", now.Gate_cstate);
#endif
		;
S_159_0: /* 2 */
		now.Gate_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Gate_force_init_update", ((int)now.Gate_force_init_update));
#endif
		;
S_160_0: /* 2 */
		now.Gate_UP = 0;
#ifdef VAR_RANGES
		logval("Gate_UP", ((int)now.Gate_UP));
#endif
		;
S_161_0: /* 2 */
		now.Gate_DOWN = 0;
#ifdef VAR_RANGES
		logval("Gate_DOWN", ((int)now.Gate_DOWN));
#endif
		;
S_162_0: /* 2 */
		now.Gate_tick = 1;
#ifdef VAR_RANGES
		logval("Gate_tick", ((int)now.Gate_tick));
#endif
		;
		goto S_169_0;
S_169_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 107: // STATE 165 - TrainGateController.pml:230 - [(Gate_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][165] = 1;
		if (!(((int)now.Gate_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 108: // STATE 178 - TrainGateController.pml:240 - [((({c_code33}&&Gate_UP)&&(Gate_cstate==Gate_top)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][178] = 1;
		if (!(((( 1 )&&((int)now.Gate_UP))&&(now.Gate_cstate==1))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 109: // STATE 185 - TrainGateController.pml:241 - [D_STEP241]
		/* default 262 */
		

		reached[0][185] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_178_0: /* 2 */
		/* c_code34 */
		{  x_u = x ;  }
;
S_179_0: /* 2 */
		/* c_code35 */
		{  Gate_x = x ;  }
;
S_180_0: /* 2 */
		now.Gate_pstate = now.Gate_cstate;
#ifdef VAR_RANGES
		logval("Gate_pstate", now.Gate_pstate);
#endif
		;
S_181_0: /* 2 */
		now.Gate_cstate = 1;
#ifdef VAR_RANGES
		logval("Gate_cstate", now.Gate_cstate);
#endif
		;
S_182_0: /* 2 */
		now.Gate_force_init_update = 1;
#ifdef VAR_RANGES
		logval("Gate_force_init_update", ((int)now.Gate_force_init_update));
#endif
		;
S_183_0: /* 2 */
		now.Gate_tick = 1;
#ifdef VAR_RANGES
		logval("Gate_tick", ((int)now.Gate_tick));
#endif
		;
		goto S_190_0;
S_190_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 110: // STATE 186 - TrainGateController.pml:250 - [(Gate_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][186] = 1;
		if (!(((int)now.Gate_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 111: // STATE 193 - TrainGateController.pml:255 - [((({c_code36}&&Gate_DOWN)&&(Gate_cstate==Gate_top)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][193] = 1;
		if (!(((( 1 )&&((int)now.Gate_DOWN))&&(now.Gate_cstate==1))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 112: // STATE 200 - TrainGateController.pml:256 - [D_STEP256]
		/* default 262 */
		

		reached[0][200] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_193_0: /* 2 */
		/* c_code37 */
		{  x_u = x ;  }
;
S_194_0: /* 2 */
		/* c_code38 */
		{  Gate_x = x ;  }
;
S_195_0: /* 2 */
		now.Gate_pstate = now.Gate_cstate;
#ifdef VAR_RANGES
		logval("Gate_pstate", now.Gate_pstate);
#endif
		;
S_196_0: /* 2 */
		now.Gate_cstate = 3;
#ifdef VAR_RANGES
		logval("Gate_cstate", now.Gate_cstate);
#endif
		;
S_197_0: /* 2 */
		now.Gate_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Gate_force_init_update", ((int)now.Gate_force_init_update));
#endif
		;
S_198_0: /* 2 */
		now.Gate_tick = 1;
#ifdef VAR_RANGES
		logval("Gate_tick", ((int)now.Gate_tick));
#endif
		;
		goto S_205_0;
S_205_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 113: // STATE 201 - TrainGateController.pml:265 - [(Gate_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][201] = 1;
		if (!(((int)now.Gate_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 114: // STATE 209 - TrainGateController.pml:273 - [(((({c_code39}&&!(Gate_UP))&&!(Gate_DOWN))&&(Gate_cstate==Gate_top)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][209] = 1;
		if (!((((( (x == 10.0) )&& !(((int)now.Gate_UP)))&& !(((int)now.Gate_DOWN)))&&(now.Gate_cstate==1))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 115: // STATE 210 - TrainGateController.pml:275 - [(((Gate_pstate!=Gate_cstate)||Gate_force_init_update))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][210] = 1;
		if (!(((now.Gate_pstate!=now.Gate_cstate)||((int)now.Gate_force_init_update))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 116: // STATE 211 - TrainGateController.pml:276 - [{c_code40}] (0:0:0 - 1)
		IfNotBlocked
		reached[0][211] = 1;
		/* c_code40 */
		{ 
		sv_save(); x_init = x ;  }

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
;
		_m = 3; goto P999; /* 0 */
	case 117: // STATE 223 - TrainGateController.pml:279 - [D_STEP279]
		/* default 262 */
		

		reached[0][223] = 1;
		reached[0][t->st] = 1;
		reached[0][tt] = 1;

		if (TstOnly) return 1;

		sv_save();
		S_215_0: /* 2 */
		/* c_code41 */
		{ 
             x_slope = 0 ;
             x_u = (x_slope * d) + x ;
             x_u = (x_u < 10.0 && signbit(x_slope) && x_init > 10.0) ? 10.0 : x_u ;
             x_u = (x_u > 10.0 && !signbit(x_slope) && x_init < 10.0) ? 10.0 : x_u ;

             Gate_x = x ;
             }
;
S_216_0: /* 2 */
		now.Gate_pstate = now.Gate_cstate;
#ifdef VAR_RANGES
		logval("Gate_pstate", now.Gate_pstate);
#endif
		;
S_217_0: /* 2 */
		now.Gate_cstate = 1;
#ifdef VAR_RANGES
		logval("Gate_cstate", now.Gate_cstate);
#endif
		;
S_218_0: /* 2 */
		now.Gate_force_init_update = 0;
#ifdef VAR_RANGES
		logval("Gate_force_init_update", ((int)now.Gate_force_init_update));
#endif
		;
S_219_0: /* 2 */
		now.Gate_UP = 0;
#ifdef VAR_RANGES
		logval("Gate_UP", ((int)now.Gate_UP));
#endif
		;
S_220_0: /* 2 */
		now.Gate_DOWN = 0;
#ifdef VAR_RANGES
		logval("Gate_DOWN", ((int)now.Gate_DOWN));
#endif
		;
S_221_0: /* 2 */
		now.Gate_tick = 1;
#ifdef VAR_RANGES
		logval("Gate_tick", ((int)now.Gate_tick));
#endif
		;
		goto S_228_0;
S_228_0: /* 1 */

#if defined(C_States) && (HAS_TRACK==1)
		c_update((uchar *) &(now.c_state[0]));
#endif
		_m = 3; goto P999;

	case 118: // STATE 224 - TrainGateController.pml:297 - [(Gate_tick)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][224] = 1;
		if (!(((int)now.Gate_tick)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 119: // STATE 237 - TrainGateController.pml:303 - [-end-] (0:0:0 - 6)
		IfNotBlocked
		reached[0][237] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */
	case  _T5:	/* np_ */
		if (!((!(trpt->o_pm&4) && !(trpt->tau&128))))
			continue;
		/* else fall through */
	case  _T2:	/* true */
		_m = 3; goto P999;
#undef rand
	}

