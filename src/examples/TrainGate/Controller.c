#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

double Controller_y;

unsigned char Controller_UP =  False ;
unsigned char Controller_DOWN =  False ;

static double  ; //the continuous vars
static double  ; // and their updates
static double  ; // and their inits
static double  ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { cloc1 , cloc2 , cloc3 , cloc4 }; // state declarations

enum states Controller (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( cloc1 ):
    if (True == False) {;}
    else if  (Controller_y >= (5.0) && (Controller_y < (15.0))) {
      Controller_DOWN = True;
      cstate =  cloc2 ;
      force_init_update = False;
    }

    else if ( Controller_y < (5.0)     ) {
      
      /* Possible Saturation */
      
      Controller_UP  = False;
      Controller_DOWN  = False;
      
      cstate =  cloc1 ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Controller!\n");
      exit(1);
    }
    break;
  case ( cloc2 ):
    if (True == False) {;}
    else if  (Controller_y >= (15.0) && (Controller_y < (25.0))) {
      Controller_UP = True;
      cstate =  cloc3 ;
      force_init_update = False;
    }

    else if ( Controller_y < (15.0)     ) {
      
      /* Possible Saturation */
      
      Controller_UP  = False;
      Controller_DOWN  = False;
      
      cstate =  cloc2 ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Controller!\n");
      exit(1);
    }
    break;
  case ( cloc3 ):
    if (True == False) {;}
    else if  (Controller_y >= (25.0)) {
      
      cstate =  cloc4 ;
      force_init_update = False;
    }

    else if ( Controller_y < (25.0)     ) {
      
      /* Possible Saturation */
      
      Controller_UP  = False;
      Controller_DOWN  = False;
      
      cstate =  cloc3 ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Controller!\n");
      exit(1);
    }
    break;
  case ( cloc4 ):
    if (True == False) {;}
    else if  (Controller_y <= (0.0)) {
      
      cstate =  cloc1 ;
      force_init_update = False;
    }

    else if ( Controller_y >= (25.0)     ) {
      
      /* Possible Saturation */
      
      Controller_UP  = False;
      Controller_DOWN  = False;
      
      cstate =  cloc4 ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Controller!\n");
      exit(1);
    }
    break;
  }
  
  return cstate;
}