module TrainGateController where
import Language
import qualified Hshac as H
import qualified Promela as P
import Prelude hiding ((==),(/=), LT, GT, (<=), (>=), (<), (>), (&&), not)
import Text.PrettyPrint.ANSI.Leijen
import GHC.IO.Handle
import System.IO

-- This is the train description

odey :: Ode
odey = Ode (DiffT (S "y")) (1) 0

assigny :: Update (External Output V)
assigny = OutputVar (S "y") := A (S "y")

t1 :: Loc
t1 =  Loc (L "t1") [odey] 
                   [InvariantLoc (DiffT (S "y")) ((T (S "y") <= TR 5) && (T (S "y") >= TR 0))]
                   ([], [assigny])

t2 :: Loc
t2 = Loc (L "t2") [odey] 
                  [InvariantLoc (DiffT (S "y")) ((T (S "y") < TR 15) && (T (S "y") >= TR 5))]
                  ([], [assigny])

t3 :: Loc
t3 = Loc (L "t3") [odey] 
                  [InvariantLoc (DiffT (S "y")) ((T (S "y") < TR 25) && (T (S "y") >= TR 15))]
                  ([], [assigny])

e1 :: Edge
e1 = Edge t1 t2  [InvariantEdge (T (S "y") == TR 5)] 
                 ([(S "y") := A (S "y")], 
                   [(OutputVar (S "y")) := A (S "y")] , [])
                 []

e2 :: Edge
e2 =  Edge t2 t3 [InvariantEdge (T (S "y") == TR 15)]
                 ([(S "y") := A (S "y")],
                  [(OutputVar (S "y")) := A (S "y")], [])
                 []

e3 :: Edge
e3 = Edge t3 t1 [InvariantEdge  (T (S "y") == TR 25)]
                ([(S "y") := 0], 
                 [(OutputVar (S "y")) := A (S "y")] ,[])
                []


train :: Ha
train = Ha (L "Train") [t1, t2, t3] t1
                       [e1, e2, e3] 
                       [SymbolDecl (S "y") 0] 
                       [] 
                       [OutputVarDecl (S "y")  0] 
                       [] []

--  Now starts the Gate description
dxdt :: Diff
dxdt = DiffT (S "x")

xodeDown :: Ode
xodeDown = Ode dxdt (-A (S "x")/2) 10

xodeUp :: Ode
xodeUp = Ode dxdt (11-A (S ("x"))/2) 1

assignx :: Update (External Output V)
assignx = OutputVar (S "x") := A (S "x")

top :: Loc
top = Loc (L "top") [Ode dxdt 0 10] 
                    [InvariantLoc dxdt (T (S "x") == TR 10)]
                    ([], [assignx])

bottom :: Loc
bottom = Loc (L "bottom") [Ode dxdt 0 1] 
                          [InvariantLoc dxdt (T (S "x") == TR 1)]
                          ([], [assignx])

g1 :: Loc
g1 = Loc (L "g1") [xodeUp] 
                  [InvariantLoc dxdt ((T (S "x") >= TR 1) && (T (S "x") < TR 10))]
                  ([], [assignx])

g2 :: Loc
g2 = Loc (L "g2") [xodeDown] 
                  [InvariantLoc dxdt ((T (S "x") > TR 1) && (T (S "x") <= TR 10))]
                  ([], [assignx])

eg1 :: Edge
eg1 = Edge g1 g2 [InvariantEdge TTrue] 
                 ([(S "x") := A (S "x")], 
                  [(OutputVar (S "x")) := A (S "x")], [])
                  [Event (InputEvent "DOWN")]

eg2 :: Edge
eg2 = Edge g2 g1 [InvariantEdge TTrue] 
                 ([(S "x") := A (S "x")], 
                  [(OutputVar (S "x")) := A (S "x")], [])
                 [Event (InputEvent "UP")]

eg6 :: Edge
eg6 = Edge bottom g1 [InvariantEdge TTrue] 
                          ([(S "x") := A (S "x")], 
                           [(OutputVar (S "x")) := A (S "x")], [])
                          [Event (InputEvent "UP")]

eg7 :: Edge
eg7 = Edge bottom bottom [InvariantEdge TTrue] 
                         ([(S "x") := A (S "x")]
                         ,[(OutputVar (S "x")) := A (S "x")], [])
                         [Event (InputEvent "DOWN")]
                         

eg8 :: Edge
eg8 = Edge top top [InvariantEdge TTrue] 
                   ([(S "x") := A (S "x")]
                   ,[(OutputVar (S "x")) := A (S "x")], [])
                   [Event (InputEvent "UP")]

eg5 :: Edge
eg5 = Edge top g2 [InvariantEdge TTrue] 
                    ([(S "x") := A (S "x")], 
                     [(OutputVar (S "x")) := A (S "x")], [])
                    [Event (InputEvent "DOWN")]
eg3 :: Edge
eg3 = Edge g1 top [InvariantEdge (T (S "x") == TR 10)] 
                    ([(S "x") := A (S "x")], 
                     [(OutputVar (S "x")) := A (S "x")], [])
                    []

eg4 :: Edge
eg4 = Edge g2 bottom [InvariantEdge (T (S "x") == TR 1)] 
                       ([(S "x") := A (S "x")], 
                        [(OutputVar (S "x")) := A (S "x")], [])
                       []
gate :: Ha
gate =  Ha (L "Gate") [g1, g2, bottom, top] top
           [eg1, eg2, eg3, eg4, eg5, eg6, eg7, eg8] [SymbolDecl (S "x") 10] [] 
           [OutputVarDecl (S "x") 10] 
           [InputEventDecl "UP", InputEventDecl "DOWN"] 
           []
           
-- XXX: Controller between the train and the gate. This controller logic
-- is wrong. Property [](<> Gate_DOWN) fails.  Notice that edge ce2
-- takes controller back to cloc1, but the invariant in cloc1 does not
-- hold.
cloc1 :: Loc
cloc1 = Loc (L "cloc1") [] [InvariantLoc (InputVar (S "y")) 
                                         (T (InputVar (S "y")) < TR 5)] ([], [])

cloc2 :: Loc
cloc2 = Loc (L "cloc2") [] [InvariantLoc (InputVar (S "y")) (T (InputVar (S "y")) < TR 15)] ([], [])

cloc3 :: Loc
cloc3 = Loc (L "cloc3") [] [InvariantLoc (InputVar (S "y")) (T (InputVar (S "y")) < TR 25)] ([], [])

cloc4 :: Loc
cloc4 = Loc (L "cloc4") [] [InvariantLoc (InputVar (S "y")) (T (InputVar (S "y")) >= TR 25)] ([], [])

ce1 :: Edge
ce1 = Edge cloc1 cloc2 [InvariantEdge (((T (InputVar (S "y"))) >= TR 5) && 
                                       ((T (InputVar (S "y"))) < TR 15))] 
                       ([], [], [(:!) (OutputEvent "DOWN")]) []

ce2 :: Edge
ce2 = Edge cloc2 cloc3 [InvariantEdge (((T (InputVar (S "y"))) >= TR 15) && 
                                       ((T (InputVar (S "y"))) < TR 25))] 
                       ([], [], [(:!) (OutputEvent "UP")]) []
      
ce3 :: Edge
ce3 = Edge cloc3 cloc4 [InvariantEdge (T (InputVar (S "y")) >= TR 25)] 
                       ([], [], []) []
ce4 :: Edge
ce4 = Edge cloc4 cloc1 [InvariantEdge (T (InputVar (S "y")) <= TR 0)] 
                       ([], [], []) []

controller :: Ha
controller = Ha (L "Controller") [cloc1, cloc2, cloc3, cloc4] cloc1 [ce1, ce2, ce3, ce4]
                [] [InputVarDecl (S "y")] [] 
                [] [OutputEventDecl "UP" TFalse, OutputEventDecl "DOWN" TFalse]

-- XXX: Declaring the HA execution order
halist :: HaSeries
halist = gate ::: (train ::: (LastHa controller)) -- This is the order
                                                  -- in which the HAs
                                                  -- will be executed.

-- XXX: Now bind the HAs
habind :: BindHa
habind = [(controller, LastBind ((InputVar (S "y")) :< (OutputVar (S "y"))), train)
         , (gate, ((InputEvent "DOWN") :< (OutputEvent "DOWN")) 
                       :<< (LastBind ((InputEvent "UP") :< (OutputEvent "UP"))), controller)]

writeToFile :: (String, Doc) -> IO ()
writeToFile (name, d) = do
                        handle <- openFile name WriteMode
                        hPutDoc handle d
                        hClose handle
-- ltl l {[]((Gate_DOWN -> <> Gate_UP) && (Gate_UP -> <> Gate_DOWN))};
-- ltl s {[] !(train_pos == 0 && gate_pos == 1)}
main :: IO ()
main = do 
       let (reactions , others) = unzip $ H.compileReactions halist habind
           (oo1, oo2) = unzip others
           cFileNames = ["Gate.c", "Train.c", "Controller.c"]
           mdoc = H.mkMain halist habind oo1 oo2
           -- XXX: The promela code gen
           promelaReactions = P.compileReactions halist habind [(train
                                                                ,OutputVar (S "y"),
                                                                IntVar (S "train_pos") 0)
                                                               ,(gate,
                                                                 OutputVar (S "x"), 
                                                                 IntVar (S "gate_pos") 10)]
       mapM_ writeToFile (zip cFileNames reactions)
       handle <-  openFile "Main.c" WriteMode
       hPutDoc handle mdoc
       hClose handle
       h <- openFile "Spin/TrainGateController.pml" WriteMode
       hPutDoc h promelaReactions
       hClose h
