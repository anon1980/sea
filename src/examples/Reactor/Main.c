#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>

#define True 1
#define False 0
extern void readInput();
extern void writeOutput();
extern int ReactorPlant(int, int);

int main (void){
  int ReactorPlant_cstate = 0 ;
  int ReactorPlant_pstate = -1;
  
  while(True) {
    readInput();
    
    int ReactorPlant_rstate = ReactorPlant (ReactorPlant_cstate, ReactorPlant_pstate);
    ReactorPlant_pstate = ReactorPlant_cstate;
    ReactorPlant_cstate = ReactorPlant_rstate;
    writeOutput();
  }
  return 0;
}