#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0


double ReactorPlant_x = 510.0;

extern unsigned char  add1 , remove1 , add2 , remove2 ;//Events
static double  x  =  510 ; //the continuous vars
static double  x_u ; // and their updates
static double  x_init ; // and their inits
static double  slope_x ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { t0 , t1 , t2 }; // state declarations

enum states ReactorPlant (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( t0 ):
    if (True == False) {;}
    else if  (x >= (550.0) && 
              add2) {
      x_u = x ;
      ReactorPlant_x = x ;
      cstate =  t2 ;
      force_init_update = False;
    }
    else if  (x >= (550.0) && 
              add1) {
      x_u = x ;
      ReactorPlant_x = x ;
      cstate =  t1 ;
      force_init_update = False;
    }

    else if ( x <= (550.0)     ) {
      if ((pstate != cstate) || force_init_update) x_init = x ;
      slope_x = ((0.1 * x) + 50) ;
      x_u = (slope_x * d) + x ;
      /* Possible Saturation */
      x_u = (x_u <  550.0  && signbit(slope_x ) &&  x_init >  550.0)  ?  550.0 : x_u ;
      x_u = (x_u >  550.0  && !signbit(slope_x ) &&  x_init <  550.0)  ?  550.0 : x_u ;
      
      
      cstate =  t0 ;
      force_init_update = False;
      
      ReactorPlant_x = x ;
    }
    else {
      fprintf(stderr, "Unreachable state in: ReactorPlant!\n");
      exit(1);
    }
    break;
  case ( t1 ):
    if (True == False) {;}
    else if  (x <= (550.0) && 
              remove1) {
      x_u = x ;
      ReactorPlant_x = x ;
      cstate =  t0 ;
      force_init_update = False;
    }

    else if ( x >= (510.0)     ) {
      if ((pstate != cstate) || force_init_update) x_init = x ;
      slope_x = ((0.1 * x) + 56) ;
      x_u = (slope_x * d) + x ;
      /* Possible Saturation */
      x_u = (x_u <  510.0  && signbit(slope_x ) &&  x_init >  510.0)  ?  510.0 : x_u ;
      x_u = (x_u >  510.0  && !signbit(slope_x ) &&  x_init <  510.0)  ?  510.0 : x_u ;
      
      
      cstate =  t1 ;
      force_init_update = False;
      
      ReactorPlant_x = x ;
    }
    else {
      fprintf(stderr, "Unreachable state in: ReactorPlant!\n");
      exit(1);
    }
    break;
  case ( t2 ):
    if (True == False) {;}
    else if  (x <= (550.0) && 
              remove2) {
      x_u = x ;
      ReactorPlant_x = x ;
      cstate =  t0 ;
      force_init_update = False;
    }

    else if ( x >= (510.0)     ) {
      if ((pstate != cstate) || force_init_update) x_init = x ;
      slope_x = ((0.1 * x) + 60) ;
      x_u = (slope_x * d) + x ;
      /* Possible Saturation */
      x_u = (x_u <  510.0  && signbit(slope_x ) &&  x_init >  510.0)  ?  510.0 : x_u ;
      x_u = (x_u >  510.0  && !signbit(slope_x ) &&  x_init <  510.0)  ?  510.0 : x_u ;
      
      
      cstate =  t2 ;
      force_init_update = False;
      
      ReactorPlant_x = x ;
    }
    else {
      fprintf(stderr, "Unreachable state in: ReactorPlant!\n");
      exit(1);
    }
    break;
  }
  x = x_u;
  return cstate;
}