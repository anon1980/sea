#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

double Controller_x1;
double Controller_x2;

unsigned char Controller_ON1 =  True ;
unsigned char Controller_OFF1 =  False ;
unsigned char Controller_ON2 =  False ;
unsigned char Controller_OFF2 =  False ;

static double  ; //the continuous vars
static double  ; // and their updates
static double  ; // and their inits
static double  ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { C1 , C2 }; // state declarations

enum states Controller (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( C1 ):
    if (True == False) {;}
    else if  (Controller_x2 <= (100.0)) {
      Controller_OFF1 = True;
      Controller_ON2 = True;
      cstate =  C2 ;
      force_init_update = False;
    }

    else if ( Controller_x2 > (100.0)     ) {
      
      /* Possible Saturation */
      
      Controller_ON1  = False;
      Controller_OFF1  = False;
      Controller_ON2  = False;
      Controller_OFF2  = False;
      
      cstate =  C1 ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Controller!\n");
      exit(1);
    }
    break;
  case ( C2 ):
    if (True == False) {;}
    else if  (Controller_x2 >= (100.1)) {
      Controller_OFF2 = True;
      Controller_ON1 = True;
      cstate =  C1 ;
      force_init_update = False;
    }

    else if ( Controller_x2 <= (100.1)     ) {
      
      /* Possible Saturation */
      
      Controller_ON1  = False;
      Controller_OFF1  = False;
      Controller_ON2  = False;
      Controller_OFF2  = False;
      
      cstate =  C2 ;
      force_init_update = False;
      
      
    }
    else {
      fprintf(stderr, "Unreachable state in: Controller!\n");
      exit(1);
    }
    break;
  }
  
  return cstate;
}