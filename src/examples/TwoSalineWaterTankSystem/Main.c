#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>



#define True 1
#define False 0
extern void readInput();
extern void writeOutput();
extern int Tank1(int, int);
extern int Tank2(int, int);
extern int Controller(int, int);
extern unsigned char Tank1_ON1;
extern unsigned char Controller_ON1;
extern unsigned char Tank1_OFF1;
extern unsigned char Controller_OFF1;
extern double Controller_x1;
extern double Tank1_ox1;
extern double Controller_x2;
extern double Tank2_ox2;
extern double Tank2_q1;
extern double Tank1_oq1;
extern double Tank2_px1;
extern double Tank1_opx1;
extern double Tank1_q2;
extern double Tank2_oq2;
extern double Tank1_px2;
extern double Tank2_opx2;
extern unsigned char Tank2_ON2;
extern unsigned char Controller_ON2;
extern unsigned char Tank2_OFF2;
extern unsigned char Controller_OFF2;
int main (void){
  int Tank1_cstate = 0 ;
  int Tank1_pstate = -1;
  int Tank2_cstate = 0 ;
  int Tank2_pstate = -1;
  int Controller_cstate = 0 ;
  int Controller_pstate = -1;
  
  
  
  while(True) {
    readInput();
    Tank1_ON1 = Controller_ON1;
    Tank1_OFF1 = Controller_OFF1;
    Controller_x1 = Tank1_ox1;
    Controller_x2 = Tank2_ox2;
    Tank2_q1 = Tank1_oq1;
    Tank2_px1 = Tank1_opx1;
    Tank1_q2 = Tank2_oq2;
    Tank1_px2 = Tank2_opx2;
    Tank2_ON2 = Controller_ON2;
    Tank2_OFF2 = Controller_OFF2;
    int Tank1_rstate = Tank1 (Tank1_cstate, Tank1_pstate);
    Tank1_pstate = Tank1_cstate;
    Tank1_cstate = Tank1_rstate;
    int Tank2_rstate = Tank2 (Tank2_cstate, Tank2_pstate);
    Tank2_pstate = Tank2_cstate;
    Tank2_cstate = Tank2_rstate;
    int Controller_rstate = Controller (Controller_cstate, Controller_pstate);
    Controller_pstate = Controller_cstate;
    Controller_cstate = Controller_rstate;
    writeOutput();
  }
  return 0;
}