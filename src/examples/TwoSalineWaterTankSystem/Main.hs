module Main where
import Language
import qualified Hshac as H
import Prelude hiding ((==),(/=), LT, GT, (<=), (>=), (<), (>), (&&), not, min, max, (^))
import Text.PrettyPrint.ANSI.Leijen
import GHC.IO.Handle
import System.IO

  
-- The Tank 1 HA
assignx1 :: Update (External Output V)
assignx1 = OutputVar (S "ox1") := A (S "x1")

assignq1 :: Update (External Output V)
assignq1 = OutputVar (S "oq1") := A (S "q1")

t10odex :: Ode
t10odex = Ode  (DiffT (S "x1")) (10 + 10 - (5 + 10)) 75

-- Try changing px1 and px2 to x1 and x2 to see what happens with time varying system!
t10odeq :: Ode
t10odeq = Ode  (DiffT (S "q1")) (10/2 + ((10*A (InputVar (S "q2")))/A (InputVar (S "px2"))) 
                                 - ((5 + 10)/A (S "px1"))*A (S "q1")) 100

t11odex :: Ode
t11odex = Ode  (DiffT (S "x1")) (-(5 + 10)) 100

-- Try changing px1 and px2 to x1 and x2 to see what happens with time varying system!
t11odeq :: Ode
t11odeq = Ode  (DiffT (S "q1")) (- ((5 + 10)/A (S "px1"))*A (S "q1")) (400/3)

t10 :: Loc
t10 = Loc (L "t10") [t10odex, t10odeq] [InvariantLoc (DiffT (S "x1")) 
                                        ((T (S ("x1")) >= TR 50) && (T (S ("x1")) < TR 100))] 
                                        ([S ("px1") := A (S "px1")], [assignq1, assignx1])


t11 :: Loc
t11 = Loc (L "t11") [t11odeq, t11odex] 
                    [InvariantLoc (DiffT (S "x1")) ((T (S ("x1")) >= TR 50) && (T (S ("x1")) <= TR 100))] 
                    ([S "px1" := A (S "px1")], [assignq1, assignx1])

t10t11 :: Edge
t10t11 = Edge t10 t11 [] ([S "x1" := 100, S "q1" := (400/3), S ("px1") := 100]
                         ,[OutputVar (S "ox1") := 100, OutputVar (S "oq1") := (400/3)]
                         ,[]) [Event (InputEvent "OFF1")]

t11t10 :: Edge
t11t10 = Edge t11 t10 [] ([S "x1" := 75, S "q1" := 100, S ("px1") := 75]
                         ,[OutputVar (S "ox1")  := 75, OutputVar (S "oq1") := 100]
                         ,[]) [Event (InputEvent "ON1")]
                         
-- The HA
t1 :: Ha
t1 = Ha (L "Tank1") [t10, t11] t10 
                               [t10t11, t11t10] 
                               [SymbolDecl (S "x1") 75,
                                SymbolDecl (S "q1") 100,
                                SymbolDecl (S "px1") 75] 
                               [InputVarDecl (S "q2"), InputVarDecl (S "px2")] 
                               [OutputVarDecl (S "ox1") 75,
                                OutputVarDecl (S "oq1") 100,
                                OutputVarDecl (S "opx1") 75] 
                               [InputEventDecl "ON1", InputEventDecl "OFF1"] []

-- Tank 2 HA
assignx2 :: Update (External Output V)
assignx2 = OutputVar (S "ox2") := A (S "x2")

assignq2 :: Update (External Output V)
assignq2 = OutputVar (S "oq2") := A (S "q2")

v2 :: ArithExpr Rational
v2 = 10

t20odex :: Ode
t20odex = Ode  (DiffT (S "x2")) (5 - (10 + v2)) 150

-- Try changing px1 and px2 to x1 and x2 to see what happens with time varying system!
t20odeq :: Ode
t20odeq = Ode  (DiffT (S "q2")) (((10*A (InputVar (S "q1")))/A (InputVar (S "px1"))) 
                                 - ((10 + v2)/A (S "px2"))*A (S "q2")) 100

t21odex :: Ode
t21odex = Ode  (DiffT (S "x2")) (10 + 5 - v2) 75

-- Try changing px1 and px2 to x1 and x2 to see what happens with time varying system!
t21odeq :: Ode
t21odeq = Ode  (DiffT (S "q2")) (10/2 + ((5/A (InputVar (S "px1")))*A (InputVar (S "q1"))) 
                                 - (v2/A (S "px2"))*A (S "q2")) 100

t20 :: Loc
t20 = Loc (L "t20") [t20odex, t20odeq] [InvariantLoc (DiffT (S "x2")) 
                                       (T (S ("x2")) >= TR 100)] 
                                       ([S ("px2") := A (S "px2")], [assignq2, assignx2])


t21 :: Loc
t21 = Loc (L "t21") [t21odeq, t21odex] 
                    [InvariantLoc (DiffT (S "x2")) (T (S ("x2")) <= TR 100.1 && (T (S "x2") >= TR 50))] 
                    ([S "px2" := A (S "px2")], [assignq2, assignx2])

t20t21 :: Edge
t20t21 = Edge t20 t21 [] ([S "x2" := 97, S "q2" := (97*6/5), S "px2" := 97]
                         ,[OutputVar (S "ox2") := 97, OutputVar (S "oq2") := (97*6/5)]
                         ,[]) [Event (InputEvent "ON2")]

t21t20 :: Edge
t21t20 = Edge t21 t20 [] ([S "x2" := 150, S "q2" := 180, S "px2" := 150]
                         ,[OutputVar (S "ox2")  := 150, OutputVar (S "oq2") := 180]
                         ,[]) [Event (InputEvent "OFF2")]

t2 :: Ha
t2 = Ha (L "Tank2") [t20, t21] t20 
                               [t20t21, t21t20] 
                               [SymbolDecl (S "x2") 150,
                                SymbolDecl (S "q2") 180,
                                SymbolDecl (S "px2") 150] 
                               [InputVarDecl (S "px1"), InputVarDecl (S "q1")] 
                               [OutputVarDecl (S "ox2") 150, OutputVarDecl (S "oq2") 180,
                                OutputVarDecl (S "opx2")  150] 
                               [InputEventDecl "ON2", InputEventDecl "OFF2"] []
  
                                                                             
-- The controller
  
c1 :: Loc
c1 = Loc (L "C1") [] [InvariantLoc (InputVar (S "x2")) (T (InputVar (S "x2")) > TR 100)] ([], [])

c2 :: Loc
c2 = Loc (L "C2") [] [InvariantLoc (InputVar (S "x2")) (T (InputVar (S "x2")) <= TR 100.1)] ([], [])

c1c2 :: Edge
c1c2 = Edge c1 c2 [InvariantEdge (T (InputVar (S "x2")) <= TR 100)] ([], [], [(:!) (OutputEvent "OFF1"),
                                                                               (:!) (OutputEvent "ON2")]) []

c2c1 :: Edge
c2c1 = Edge c2 c1 [InvariantEdge (T (InputVar (S "x2")) >= TR 100.1)] ([], [], [(:!) (OutputEvent "OFF2"),
                                                                                (:!) (OutputEvent "ON1")]) []

controller :: Ha
controller = Ha (L "Controller") [c1, c2] c1 [c1c2, c2c1]
             [] [InputVarDecl (S "x1"), InputVarDecl (S "x2")] [] 
             [] [OutputEventDecl "ON1" TTrue,
                 OutputEventDecl "OFF1" TFalse,
                 OutputEventDecl "ON2" TFalse,
                 OutputEventDecl "OFF2" TFalse]
                
                
-- Bind
b1 :: Bind
b1 = (InputEvent "ON1") :< (OutputEvent "ON1")

b2 :: Bind
b2 = (InputEvent "OFF1") :< (OutputEvent "OFF1")

b3 :: Bind
b3 = (InputEvent "ON2") :< (OutputEvent "ON2")

b4 :: Bind
b4 = (InputEvent "OFF2") :< (OutputEvent "OFF2")

x1 :: Bind
x1 = InputVar (S "x1") :< OutputVar (S "ox1")

px1 :: Bind
px1 = InputVar (S "px1") :< OutputVar (S "opx1")

px2 :: Bind
px2 = InputVar (S "px2") :< OutputVar (S "opx2")

q1 :: Bind
q1 = InputVar (S "q1") :< OutputVar (S "oq1")

q2 :: Bind
q2 = InputVar (S "q2") :< OutputVar (S "oq2")

x2 :: Bind
x2 = InputVar (S "x2") :< OutputVar (S "ox2")
  
                                    
-- BindHa
bs :: BindHa
bs = [(t1, (b1 :<< (LastBind b2)), controller)
     ,(controller, (LastBind x1), t1)
     ,(controller, (LastBind x2), t2)
     ,(t2, q1 :<< (LastBind px1), t1)
     ,(t1, q2 :<< (LastBind px2), t2)
     ,(t2, (b3 :<< (LastBind b4)), controller)]


-- XXX: Now make the HA series
hase :: HaSeries
hase = t1 ::: (t2 ::: (LastHa controller))

writeHa :: (String, Doc) -> IO ()
writeHa (x, y) = do
                 handle <- openFile x WriteMode
                 hPutDoc handle y
                 hClose handle
main :: IO ()
main = do 
       let (dd, others) = unzip $ H.compileReactions hase bs
       let (oo1, oo2) = unzip others
       mapM_ writeHa $ zip ["Tank1.c", "Tank2.c", "Controller.c"] dd
       x <- openFile "Main.c" WriteMode
       hPutDoc x $ H.mkMain hase bs oo1 oo2
       hClose x
