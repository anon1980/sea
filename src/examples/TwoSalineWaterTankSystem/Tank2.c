#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

double Tank2_px1;
double Tank2_q1;
double Tank2_ox2 = 150.0;
double Tank2_oq2 = 180.0;
double Tank2_opx2 = 150.0;

extern unsigned char  Tank2_ON2 , Tank2_OFF2 ;//Events
static double  x2  =  150 ,  q2  =  180 ,  px2  =  150 ; //the continuous vars
static double  x2_u , q2_u , px2_u ; // and their updates
static double  x2_init , q2_init , px2_init ; // and their inits
static double  slope_x2 , slope_q2 , slope_px2 ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { t20 , t21 }; // state declarations

enum states Tank2 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( t20 ):
    if (True == False) {;}
    else if  (Tank2_ON2) {
      x2_u = 97 ;
      q2_u = ((97 * 6) / 5) ;
      px2_u = 97 ;
      Tank2_ox2 = 97 ;
      Tank2_oq2 = ((97 * 6) / 5) ;
      cstate =  t21 ;
      force_init_update = False;
    }

    else if ( x2 >= (100.0)     ) {
      if ((pstate != cstate) || force_init_update) x2_init = x2 ;
      slope_x2 = (5 + (- (10 + 10))) ;
      x2_u = (slope_x2 * d) + x2 ;
      if ((pstate != cstate) || force_init_update) q2_init = q2 ;
      slope_q2 = (((10 * Tank2_q1) / Tank2_px1) + (- (((10 + 10) / px2) * q2))) ;
      q2_u = (slope_q2 * d) + q2 ;
      /* Possible Saturation */
      x2_u = (x2_u <  100.0  && signbit(slope_x2 ) &&  x2_init >  100.0)  ?  100.0 : x2_u ;
      x2_u = (x2_u >  100.0  && !signbit(slope_x2 ) &&  x2_init <  100.0)  ?  100.0 : x2_u ;
      
      
      Tank2_ON2  = False;
      Tank2_OFF2  = False;
      cstate =  t20 ;
      force_init_update = False;
      px2_u = px2 ;
      Tank2_oq2 = q2 ;
      Tank2_ox2 = x2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Tank2!\n");
      exit(1);
    }
    break;
  case ( t21 ):
    if (True == False) {;}
    else if  (Tank2_OFF2) {
      x2_u = 150 ;
      q2_u = 180 ;
      px2_u = 150 ;
      Tank2_ox2 = 150 ;
      Tank2_oq2 = 180 ;
      cstate =  t20 ;
      force_init_update = False;
    }

    else if ( x2 <= (100.1) && (x2 >= (50.0))     ) {
      if ((pstate != cstate) || force_init_update) q2_init = q2 ;
      slope_q2 = (((10 / 2) + ((5 / Tank2_px1) * Tank2_q1)) + (- ((10 / px2) * q2))) ;
      q2_u = (slope_q2 * d) + q2 ;
      if ((pstate != cstate) || force_init_update) x2_init = x2 ;
      slope_x2 = ((10 + 5) + (- 10)) ;
      x2_u = (slope_x2 * d) + x2 ;
      /* Possible Saturation */
      
      x2_u = (x2_u <  50.0  && signbit(slope_x2 ) &&  x2_init >  50.0)  ?  50.0 : x2_u ;
      x2_u = (x2_u >  100.1  && !signbit(slope_x2 ) &&  x2_init <  100.1)  ?  100.1 : x2_u ;
      
      Tank2_ON2  = False;
      Tank2_OFF2  = False;
      cstate =  t21 ;
      force_init_update = False;
      px2_u = px2 ;
      Tank2_oq2 = q2 ;
      Tank2_ox2 = x2 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Tank2!\n");
      exit(1);
    }
    break;
  }
  x2 = x2_u;
  q2 = q2_u;
  px2 = px2_u;
  return cstate;
}