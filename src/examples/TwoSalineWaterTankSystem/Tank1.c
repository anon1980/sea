#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define True 1
#define False 0

double Tank1_q2;
double Tank1_px2;
double Tank1_ox1 = 75.0;
double Tank1_oq1 = 100.0;
double Tank1_opx1 = 75.0;

extern unsigned char  Tank1_OFF1 , Tank1_ON1 ;//Events
static double  x1  =  75 ,  q1  =  100 ,  px1  =  75 ; //the continuous vars
static double  x1_u , q1_u , px1_u ; // and their updates
static double  x1_init , q1_init , px1_init ; // and their inits
static double  slope_x1 , slope_q1 , slope_px1 ; // and their slopes
static unsigned char force_init_update;
extern double d; // the time step
enum states { t10 , t11 }; // state declarations

enum states Tank1 (enum states cstate, enum states pstate){
  switch (cstate) {
  case ( t10 ):
    if (True == False) {;}
    else if  (Tank1_OFF1) {
      x1_u = 100 ;
      q1_u = (400 / 3) ;
      px1_u = 100 ;
      Tank1_ox1 = 100 ;
      Tank1_oq1 = (400 / 3) ;
      cstate =  t11 ;
      force_init_update = False;
    }

    else if ( x1 >= (50.0) && (x1 < (100.0))     ) {
      if ((pstate != cstate) || force_init_update) x1_init = x1 ;
      slope_x1 = ((10 + 10) + (- (5 + 10))) ;
      x1_u = (slope_x1 * d) + x1 ;
      if ((pstate != cstate) || force_init_update) q1_init = q1 ;
      slope_q1 = (((10 / 2) + ((10 * Tank1_q2) / Tank1_px2)) + (- (((5 + 10) / px1) * q1))) ;
      q1_u = (slope_q1 * d) + q1 ;
      /* Possible Saturation */
      x1_u = (x1_u <  50.0  && signbit(slope_x1 ) &&  x1_init >  50.0)  ?  50.0 : x1_u ;
      x1_u = (x1_u >  100.0  && !signbit(slope_x1 ) &&  x1_init <  100.0)  ?  100.0 : x1_u ;
      
      
      Tank1_ON1  = False;
      Tank1_OFF1  = False;
      cstate =  t10 ;
      force_init_update = False;
      px1_u = px1 ;
      Tank1_oq1 = q1 ;
      Tank1_ox1 = x1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Tank1!\n");
      exit(1);
    }
    break;
  case ( t11 ):
    if (True == False) {;}
    else if  (Tank1_ON1) {
      x1_u = 75 ;
      q1_u = 100 ;
      px1_u = 75 ;
      Tank1_ox1 = 75 ;
      Tank1_oq1 = 100 ;
      cstate =  t10 ;
      force_init_update = False;
    }

    else if ( x1 >= (50.0) && (x1 <= (100.0))     ) {
      if ((pstate != cstate) || force_init_update) q1_init = q1 ;
      slope_q1 = (- (((5 + 10) / px1) * q1)) ;
      q1_u = (slope_q1 * d) + q1 ;
      if ((pstate != cstate) || force_init_update) x1_init = x1 ;
      slope_x1 = (- (5 + 10)) ;
      x1_u = (slope_x1 * d) + x1 ;
      /* Possible Saturation */
      
      x1_u = (x1_u <  50.0  && signbit(slope_x1 ) &&  x1_init >  50.0)  ?  50.0 : x1_u ;
      x1_u = (x1_u >  100.0  && !signbit(slope_x1 ) &&  x1_init <  100.0)  ?  100.0 : x1_u ;
      
      Tank1_ON1  = False;
      Tank1_OFF1  = False;
      cstate =  t11 ;
      force_init_update = False;
      px1_u = px1 ;
      Tank1_oq1 = q1 ;
      Tank1_ox1 = x1 ;
    }
    else {
      fprintf(stderr, "Unreachable state in: Tank1!\n");
      exit(1);
    }
    break;
  }
  x1 = x1_u;
  q1 = q1_u;
  px1 = px1_u;
  return cstate;
}