module SwitchTank where
import Language
import qualified Hshac as H
import Prelude hiding ((==),(/=), LT, GT, (<=), (>=), (<), (>), (&&), not)
import Text.PrettyPrint.ANSI.Leijen
import GHC.IO.Handle
import System.IO

dx1dt :: Diff
dx1dt = DiffT (S "x1")

dx2dt :: Diff
dx2dt = DiffT (S "x2")

odex1f :: Ode
odex1f = Ode dx1dt (-0.3+1) 0.5

odex1e :: Ode
odex1e = Ode dx1dt (-0.3) 0.5

odex2f :: Ode
odex2f = Ode dx2dt (-0.4+1) 0.5

odex2e :: Ode
odex2e = Ode dx2dt (-0.4) 0.5

t1 :: Loc
t1 = Loc (L "t1") [odex1f, odex2e] [InvariantLoc dx1dt (T (S "x1") <= TR 1),
                                    InvariantLoc dx2dt (T (S "x2") > TR 0.25)] 
                                    ([], [OutputVar (S "x1") := A (S "x1"),
                                          OutputVar (S "x2") := A (S "x2")])

t2 :: Loc
t2 = Loc (L "t2") [odex2f, odex1e] [InvariantLoc dx1dt (T (S "x1") > TR 0.25),
                                    InvariantLoc dx2dt (T (S "x2") <= TR 1)] 
                                    ([], [OutputVar (S "x1") := A (S "x1"),
                                          OutputVar (S "x2") := A (S "x2")])


e1 :: Edge
e1 = Edge t1 t2 [InvariantEdge (T (S "x2") <= TR 0.25)] 
                ([(S "x1") := A (S "x1"), 
                 (S "x2") := A (S "x2")]
                ,[(OutputVar (S "x1")) := A (S "x1"),
                 (OutputVar (S "x2")) := A (S "x2")]
                ,[]) 
                []

e2 :: Edge
e2 = Edge t2 t1 [InvariantEdge (T (S "x1") <= TR 0.25)] 
                 ([(S "x1") := A (S "x1"), 
                 (S "x2") := A (S "x2")]
                ,[(OutputVar (S "x1")) := A (S "x1"),
                 (OutputVar (S "x2")) := A (S "x2")]
                ,[])
                []

switchTank :: Ha
switchTank = Ha (L "SwitchTank") [t1, t2] t1 [e1, e2]
                  [SymbolDecl (S "x1") 0.5, SymbolDecl (S "x2") 0.5] [] 
                  [OutputVarDecl (S "x1") 0, OutputVarDecl (S "x2") 0]
                  [] []

main :: IO ()
main = do 
       x <- openFile "SwitchTank.c" WriteMode
       let (dd, (oo1, oo2)) = H.compileReaction switchTank
       hPutDoc x dd
       y <- openFile "Main.c" WriteMode
       hPutDoc y $ H.mkMain (LastHa switchTank) [] [oo1] [oo2]
       hClose y
       hClose x
